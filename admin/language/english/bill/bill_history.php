<?php
// Heading
$_['heading_title']     = 'Bill History';
$_['heading_title_mail'] = 'Send Mail';


$_['button_filter'] = 'Print Invoice';
$_['button_filter_normal'] = 'Filter';
$_['button_filter_receipt'] = 'Print Receipt';
$_['button_mail'] = 'Send Mail';

$_['text_print'] = 'Print Invoice';
$_['text_mail'] = 'Send Mail';
$_['text_print_receipt'] = 'Print Receipt';

// Column
$_['column_name'] = 'Horse Name';
$_['column_month']   = 'Month';
$_['column_year']     = 'Year';

$_['column_sr_no']     = 'Sr.No';
$_['column_bill_no']     = 'Bill Id';
$_['column_horse_name']     = 'Horse Name';
$_['column_owner_name']     = 'Owner Name';
$_['column_trainer_name']   = 'Trainer Name';
$_['column_total']     = 'Total';
$_['column_action']     = 'Action';

$_['entry_name'] = 'Horse Name';
$_['entry_owner'] = 'Owner Name';
$_['entry_bill_id'] = 'Bill Id';
$_['entry_trainer'] = 'Trainer Name';
$_['entry_month']   = 'Month';
$_['entry_year']     = 'Year';
$_['entry_doctor'] = 'Doctor';
$_['entry_date_start']   = 'Date Start';
$_['entry_date_end']   = 'Date End';
$_['entry_transaction_type'] = 'Clinic';
$_['entry_email'] = 'E-Mail';

?>
