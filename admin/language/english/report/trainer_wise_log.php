<?php
// Heading
$_['heading_title']     = 'Trainer Wise Log Report';

// Text

// Column
$_['column_dot'] 		= 'Date';
$_['column_horse_name']   = 'Horse Name';
$_['column_medicine_name']     = 'Medicine Name';
$_['column_trainer']      = 'Trainer Name';
$_['text_all']  = 'All';



$_['button_export'] = 'Export';
$_['button_filter'] = 'Search';

// Entry
$_['entry_date_start']  = 'Date:';
$_['entry_doctor']     = 'Doctor';
?>