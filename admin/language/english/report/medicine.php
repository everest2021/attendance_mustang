<?php
// Heading
$_['heading_title']     = 'Medicine Report';

// Text

// Column
$_['column_name'] 		= 'Medicine Name';
$_['column_price']   = 'Unit price';
$_['column_quantity']     = 'Quantity';
$_['column_total']   = 'Total Sale';

$_['button_filter']   = 'Filter';


// Entry
$_['entry_date_start']  = 'Date From:';
$_['entry_date_end']    = 'Date TO:';
?>
