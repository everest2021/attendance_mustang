<?php
// Heading
$_['heading_title']     = 'Bonus Report Per Day';

// Text

// Column
$_['column_dot'] 		= 'Date Of Treatment';
$_['column_horse_name']   = 'Horse Name';
$_['column_medicine_name']     = 'Medicine Name';
$_['column_medicine_quantity']   = 'Qty';
$_['column_total']        = 'Total';
$_['column_trainer']      = 'Trainer';
$_['column_owner']      = 'Owner';

$_['button_export'] = 'Export';

// Entry
$_['entry_date_start']  = 'Date From:';
$_['entry_date_end']    = 'Date TO:';
$_['entry_name']       = 'Owner Name:';
$_['entry_doctor']     = 'Doctor';
$_['entry_transaction_type'] = 'Transaction type';
?>