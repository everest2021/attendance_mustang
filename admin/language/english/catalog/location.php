<?php
// Heading
$_['heading_title']     = 'Location';

// Text
$_['text_success']      = 'Success: You have modified Location!';
$_['text_default']      = 'Default';

// Column
$_['column_title']      = 'Location Title';
$_['column_sort_order']	= 'Sort Order';
$_['column_action']     = 'Action';

// Entry
$_['entry_title']       = 'Location Title:';
$_['entry_description'] = 'Description:';
$_['entry_store']       = 'Stores:';
$_['entry_keyword']     = 'SEO Keyword:<br /><span class="help">Do not use spaces instead replace spaces with - and make sure the keyword is globally unique.</span>';
$_['entry_bottom']      = 'Bottom:<br/><span class="help">Display in the bottom footer.</span>';
$_['entry_status']      = 'Status:';
$_['entry_sort_order']  = 'Sort Order:';
$_['entry_layout']      = 'Layout Override:';

// Error 
$_['error_warning']     = 'Warning: Please check the form carefully for errors!';
$_['error_permission']  = 'Warning: You do not have permission to modify Location!';
$_['error_title']       = 'Location Title must be between 3 and 64 characters!';
$_['error_description'] = 'Description must be more than 3 characters!';
$_['error_account']     = 'Warning: This Location page cannot be deleted as it is currently assigned as the store account terms!';
$_['error_checkout']    = 'Warning: This Location page cannot be deleted as it is currently assigned as the store checkout terms!';
$_['error_affiliate']   = 'Warning: This Location page cannot be deleted as it is currently assigned as the store affiliate terms!';
$_['error_store']       = 'Warning: This Location page cannot be deleted as its currently used by %s stores!';
?>