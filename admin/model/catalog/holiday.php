<?php
class ModelCatalogholiday extends Model {
	public function addholiday($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "holiday SET `name` = '" . $this->db->escape($data['name']) . "', `date` = '" . $this->db->escape($data['date']) . "', `department_mumbai` = '" . $this->db->escape((isset($data['dept_holiday_mumbai'])) ? serialize($data['dept_holiday_mumbai']) : '') . "', `department_pune` = '" . $this->db->escape((isset($data['dept_holiday_pune'])) ? serialize($data['dept_holiday_pune']) : '') . "', `department_delhi` = '" . $this->db->escape((isset($data['dept_holiday_delhi'])) ? serialize($data['dept_holiday_delhi']) : '') . "', `department_chennai` = '" . $this->db->escape((isset($data['dept_holiday_chennai'])) ? serialize($data['dept_holiday_chennai']) : '') . "', `department_bangalore` = '" . $this->db->escape((isset($data['dept_holiday_bangalore'])) ? serialize($data['dept_holiday_bangalore']) : '') . "', `department_ahmedabad` = '" . $this->db->escape((isset($data['dept_holiday_ahmedabad'])) ? serialize($data['dept_holiday_ahmedabad']) : '') . "' ");
		$holiday_id = $this->db->getLastId();	
		$day_date = date('j', strtotime($data['date']));
		$holiday_ids = 'H_'.$holiday_id;

		$month = date('n', strtotime($data['date']));
		$year = date('Y', strtotime($data['date']));
		
		if (isset($data['dept_holiday_mumbai'])) {
			foreach ($data['dept_holiday_mumbai'] as $dkey => $dvalue) {
				$dvalue = html_entity_decode(strtolower(trim($dvalue)));
				$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$dvalue."' AND LOWER(`unit`) = 'mumbai' ");
				foreach ($emp_codes->rows as $ekey => $evalue) {
					//$this->db->query("INSERT INTO " . DB_PREFIX . "employee_meta_week SET `employee_code` = '".$evalue['emp_code']."', `week_id` = '" . $week_id . "' ");
					$current_shifts = $this->db->query("SELECT `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ");	
					$current_shift = 'S_1';
					if($current_shifts->num_rows > 0){
						$current_shift = $current_shifts->row[$day_date];
					}
					$current_shift_exp = explode('_', $current_shift);
					$holiday_idss = $holiday_ids;
					if($current_shift_exp[0] == 'S'){
						$holiday_idss = $holiday_ids.'_'.$current_shift_exp[1];
					}
					$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$holiday_idss."' WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ");
				}
			}
		}
		if (isset($data['dept_holiday_pune'])) {
			foreach ($data['dept_holiday_pune'] as $dkey => $dvalue) {
				$dvalue = html_entity_decode(strtolower(trim($dvalue)));
				$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$dvalue."' AND LOWER(`unit`) = 'pune' ");
				foreach ($emp_codes->rows as $ekey => $evalue) {
					//$this->db->query("INSERT INTO " . DB_PREFIX . "employee_meta_week SET `employee_code` = '".$evalue['emp_code']."', `week_id` = '" . $week_id . "' ");
					$current_shifts = $this->db->query("SELECT `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ");	
					$current_shift = 'S_1';
					if($current_shifts->num_rows > 0){
						$current_shift = $current_shifts->row[$day_date];
					}
					$current_shift_exp = explode('_', $current_shift);
					$holiday_idss = $holiday_ids;
					if($current_shift_exp[0] == 'S'){
						$holiday_idss = $holiday_ids.'_'.$current_shift_exp[1];
					}
					$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$holiday_idss."' WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ");
				}
			}
		}
		if (isset($data['dept_holiday_delhi'])) {
			foreach ($data['dept_holiday_delhi'] as $dkey => $dvalue) {
				$dvalue = html_entity_decode(strtolower(trim($dvalue)));
				$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$dvalue."' AND LOWER(`unit`) = 'delhi' ");
				foreach ($emp_codes->rows as $ekey => $evalue) {
					//$this->db->query("INSERT INTO " . DB_PREFIX . "employee_meta_week SET `employee_code` = '".$evalue['emp_code']."', `week_id` = '" . $week_id . "' ");
					$current_shifts = $this->db->query("SELECT `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ");	
					$current_shift = 'S_1';
					if($current_shifts->num_rows > 0){
						$current_shift = $current_shifts->row[$day_date];
					}
					$current_shift_exp = explode('_', $current_shift);
					$holiday_idss = $holiday_ids;
					if($current_shift_exp[0] == 'S'){
						$holiday_idss = $holiday_ids.'_'.$current_shift_exp[1];
					}
					$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$holiday_idss."' WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ");
				}
			}
		}
		if (isset($data['dept_holiday_chennai'])) {
			foreach ($data['dept_holiday_chennai'] as $dkey => $dvalue) {
				$dvalue = html_entity_decode(strtolower(trim($dvalue)));
				$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$dvalue."' AND LOWER(`unit`) = 'chennai' ");
				foreach ($emp_codes->rows as $ekey => $evalue) {
					//$this->db->query("INSERT INTO " . DB_PREFIX . "employee_meta_week SET `employee_code` = '".$evalue['emp_code']."', `week_id` = '" . $week_id . "' ");
					$current_shifts = $this->db->query("SELECT `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ");	
					$current_shift = 'S_1';
					if($current_shifts->num_rows > 0){
						$current_shift = $current_shifts->row[$day_date];
					}
					$current_shift_exp = explode('_', $current_shift);
					$holiday_idss = $holiday_ids;
					if($current_shift_exp[0] == 'S'){
						$holiday_idss = $holiday_ids.'_'.$current_shift_exp[1];
					}
					$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$holiday_idss."' WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ");
				}
			}
		}
		if (isset($data['dept_holiday_bangalore'])) {
			foreach ($data['dept_holiday_bangalore'] as $dkey => $dvalue) {
				$dvalue = html_entity_decode(strtolower(trim($dvalue)));
				$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$dvalue."' AND LOWER(`unit`) = 'bangalore' ");
				foreach ($emp_codes->rows as $ekey => $evalue) {
					//$this->db->query("INSERT INTO " . DB_PREFIX . "employee_meta_week SET `employee_code` = '".$evalue['emp_code']."', `week_id` = '" . $week_id . "' ");
					$current_shifts = $this->db->query("SELECT `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ");	
					$current_shift = 'S_1';
					if($current_shifts->num_rows > 0){
						$current_shift = $current_shifts->row[$day_date];
					}
					$current_shift_exp = explode('_', $current_shift);
					$holiday_idss = $holiday_ids;
					if($current_shift_exp[0] == 'S'){
						$holiday_idss = $holiday_ids.'_'.$current_shift_exp[1];
					}
					$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$holiday_idss."' WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ");
				}
			}
		}
		if (isset($data['dept_holiday_ahmedabad'])) {
			foreach ($data['dept_holiday_ahmedabad'] as $dkey => $dvalue) {
				$dvalue = html_entity_decode(strtolower(trim($dvalue)));
				$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$dvalue."' AND LOWER(`unit`) = 'ahmedabad' ");
				foreach ($emp_codes->rows as $ekey => $evalue) {
					//$this->db->query("INSERT INTO " . DB_PREFIX . "employee_meta_week SET `employee_code` = '".$evalue['emp_code']."', `week_id` = '" . $week_id . "' ");
					$current_shifts = $this->db->query("SELECT `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ");	
					$current_shift = 'S_1';
					if($current_shifts->num_rows > 0){
						$current_shift = $current_shifts->row[$day_date];
					}
					$current_shift_exp = explode('_', $current_shift);
					$holiday_idss = $holiday_ids;
					if($current_shift_exp[0] == 'S'){
						$holiday_idss = $holiday_ids.'_'.$current_shift_exp[1];
					}
					$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$holiday_idss."' WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ");
				}
			}
		}
	}

	public function editholiday($holiday_id, $data) {
		$week_data = $this->getholiday($holiday_id);
		$dept_holiday_mumbai = unserialize($week_data['department_mumbai']);
		if($dept_holiday_mumbai){
			foreach ($dept_holiday_mumbai as $key => $value) {
				$dept_holiday_mumbai[$key] = html_entity_decode(strtolower(trim($value)));
			}
		} else {
			$dept_holiday_mumbai = array();
		}

		$dept_holiday_pune = unserialize($week_data['department_pune']);
		if($dept_holiday_pune){
			foreach ($dept_holiday_pune as $key => $value) {
				$dept_holiday_pune[$key] = html_entity_decode(strtolower(trim($value)));
			}
		} else {
			$dept_holiday_pune = array();
		}

		$dept_holiday_delhi = unserialize($week_data['department_delhi']);
		if($dept_holiday_delhi){
			foreach ($dept_holiday_delhi as $key => $value) {
				$dept_holiday_delhi[$key] = html_entity_decode(strtolower(trim($value)));
			}
		} else {
			$dept_holiday_delhi = array();
		}

		$dept_holiday_chennai = unserialize($week_data['department_chennai']);
		if($dept_holiday_chennai){
			foreach ($dept_holiday_chennai as $key => $value) {
				$dept_holiday_chennai[$key] = html_entity_decode(strtolower(trim($value)));
			}
		} else {
			$dept_holiday_chennai = array();
		}

		$dept_holiday_bangalore = unserialize($week_data['department_bangalore']);
		if($dept_holiday_bangalore){
			foreach ($dept_holiday_bangalore as $key => $value) {
				$dept_holiday_bangalore[$key] = html_entity_decode(strtolower(trim($value)));
			}
		} else {
			$dept_holiday_bangalore = array();
		}

		$dept_holiday_ahmedabad = unserialize($week_data['department_ahmedabad']);
		if($dept_holiday_ahmedabad){
			foreach ($dept_holiday_ahmedabad as $key => $value) {
				$dept_holiday_ahmedabad[$key] = html_entity_decode(strtolower(trim($value)));
			}
		} else {
			$dept_holiday_ahmedabad = array();
		}

		$this->db->query("UPDATE " . DB_PREFIX . "holiday SET `name` = '" . $this->db->escape($data['name']) . "', `date` = '" . $this->db->escape($data['date']) . "', `department_mumbai` = '" . $this->db->escape((isset($data['dept_holiday_mumbai'])) ? serialize($data['dept_holiday_mumbai']) : '') . "', `department_pune` = '" . $this->db->escape((isset($data['dept_holiday_pune'])) ? serialize($data['dept_holiday_pune']) : '') . "', `department_delhi` = '" . $this->db->escape((isset($data['dept_holiday_delhi'])) ? serialize($data['dept_holiday_delhi']) : '') . "', `department_chennai` = '" . $this->db->escape((isset($data['dept_holiday_chennai'])) ? serialize($data['dept_holiday_chennai']) : '') . "', `department_bangalore` = '" . $this->db->escape((isset($data['dept_holiday_bangalore'])) ? serialize($data['dept_holiday_bangalore']) : '') . "', `department_ahmedabad` = '" . $this->db->escape((isset($data['dept_holiday_ahmedabad'])) ? serialize($data['dept_holiday_ahmedabad']) : '') . "' WHERE holiday_id = '" . (int)$holiday_id . "'");
		//$this->db->query("DELETE FROM " . DB_PREFIX . "employee_meta WHERE holiday_id = '" . (int)$holiday_id . "'");
		$holiday_ids = 'H_'.$holiday_id;
		$day_date = date('j', strtotime($data['date']));
		$month = date('n', strtotime($data['date']));
		$year = date('Y', strtotime($data['date']));

		$this->load->model('report/attendance');
		$department_datas = $this->model_report_attendance->getdepartment_list();
		$department_data = array();
		//$department_data['0'] = 'All';
		foreach ($department_datas as $dkey => $dvalue) {
			$department_data[strtolower(trim($dvalue['department']))] = strtolower(trim($dvalue['department']));
		}

		if (isset($data['dept_holiday_mumbai'])) {
			if($dept_holiday_mumbai){
				$d_dept = array();
				foreach ($dept_holiday_mumbai as $d1key => $d1value) {
					foreach ($data['dept_holiday_mumbai'] as $dkey => $dvalue) {
						$dvalue = html_entity_decode(strtolower(trim($dvalue)));
						if(!in_array($dvalue, $d_dept)){
							$d_dept[] = $dvalue;
							$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$dvalue."' AND LOWER(`unit`) = 'mumbai' ");
							foreach ($emp_codes->rows as $ekey => $evalue) {
								// echo "UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$holiday_ids."' WHERE `emp_code` = '".$evalue['emp_code']."' ";
								// echo '<br />';
								$current_shifts = $this->db->query("SELECT `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ");	
								$current_shift = 'S_1';
								if($current_shifts->num_rows > 0){
									$current_shift = $current_shifts->row[$day_date];
								}
								$current_shift_exp = explode('_', $current_shift);
								$holiday_idss = $holiday_ids;
								if(isset($current_shift_exp[2])){
									$holiday_idss = $holiday_ids.'_'.$current_shift_exp[2];
								} else {
									if($current_shift_exp[0] == 'S'){
										$holiday_idss = $holiday_ids.'_'.$current_shift_exp[1];
									}
								}
								$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$holiday_idss."' WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ");
							}
						} 
					}
				}
				foreach ($dept_holiday_mumbai as $d1key => $d1value) {
					$d1value = html_entity_decode(strtolower(trim($d1value)));
					if(!in_array($d1value, $d_dept)){
						$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$d1value."' AND LOWER(`unit`) = 'mumbai' ");
						foreach ($emp_codes->rows as $ekey => $evalue) {
							// echo "UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = 'S_1' WHERE `emp_code` = '".$evalue['emp_code']."' ";
							// echo '<br />';
							$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = 'S_1' WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ");
						}
					}
				}
			} else {
				foreach ($data['dept_holiday_mumbai'] as $dkey => $dvalue) {
					$dvalue = html_entity_decode(strtolower(trim($dvalue)));
					$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$dvalue."' AND LOWER(`unit`) = 'mumbai' ");
					foreach ($emp_codes->rows as $ekey => $evalue) {
						$current_shifts = $this->db->query("SELECT `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ");	
						$current_shift = 'S_1';
						if($current_shifts->num_rows > 0){
							$current_shift = $current_shifts->row[$day_date];
						}
						$current_shift_exp = explode('_', $current_shift);
						$holiday_idss = $holiday_ids;
						if(isset($current_shift_exp[2])){
							$holiday_idss = $holiday_ids.'_'.$current_shift_exp[2];
						} else {
							if($current_shift_exp[0] == 'S'){
								$holiday_idss = $holiday_ids.'_'.$current_shift_exp[1];
							}
						}
						$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$holiday_idss."' WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ");
					}
				}
			}
		} else {
			$d_dept = array();
			foreach ($dept_holiday_mumbai as $d1key => $d1value) {
				$d1value = html_entity_decode(strtolower(trim($d1value)));
				if(!in_array($d1value, $d_dept)){
					$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$d1value."' AND LOWER(`unit`) = 'mumbai' ");
					foreach ($emp_codes->rows as $ekey => $evalue) {
						// echo "UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = 'S_1' WHERE `emp_code` = '".$evalue['emp_code']."' ";
						// echo '<br />';
						$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = 'S_1' WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ");
					}
				}
			}
		}

		
		if (isset($data['dept_holiday_pune'])) {
			if($dept_holiday_pune){
				$d_dept = array();
				foreach ($dept_holiday_pune as $d1key => $d1value) {
					foreach ($data['dept_holiday_pune'] as $dkey => $dvalue) {
						$dvalue = html_entity_decode(strtolower(trim($dvalue)));
						if(!in_array($dvalue, $d_dept)){
							$d_dept[] = $dvalue;
							$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$dvalue."' AND LOWER(`unit`) = 'pune' ");
							foreach ($emp_codes->rows as $ekey => $evalue) {
								// echo "UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$holiday_ids."' WHERE `emp_code` = '".$evalue['emp_code']."' ";
								// echo '<br />';
								$current_shifts = $this->db->query("SELECT `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ");	
								$current_shift = 'S_1';
								if($current_shifts->num_rows > 0){
									$current_shift = $current_shifts->row[$day_date];
								}
								$current_shift_exp = explode('_', $current_shift);
								$holiday_idss = $holiday_ids;
								if(isset($current_shift_exp[2])){
									$holiday_idss = $holiday_ids.'_'.$current_shift_exp[2];
								} else {
									if($current_shift_exp[0] == 'S'){
										$holiday_idss = $holiday_ids.'_'.$current_shift_exp[1];
									}
								}
								$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$holiday_idss."' WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ");
							}
						} 
					}
				}
				foreach ($dept_holiday_pune as $d1key => $d1value) {
					$d1value = html_entity_decode(strtolower(trim($d1value)));
					if(!in_array($d1value, $d_dept)){
						$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$d1value."' AND LOWER(`unit`) = 'pune' ");
						foreach ($emp_codes->rows as $ekey => $evalue) {
							// echo "UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = 'S_1' WHERE `emp_code` = '".$evalue['emp_code']."' ";
							// echo '<br />';
							$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = 'S_1' WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ");
						}
					}
				}
			} else {
				foreach ($data['dept_holiday_pune'] as $dkey => $dvalue) {
					$dvalue = html_entity_decode(strtolower(trim($dvalue)));
					$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$dvalue."' AND LOWER(`unit`) = 'pune' ");
					foreach ($emp_codes->rows as $ekey => $evalue) {
						$current_shifts = $this->db->query("SELECT `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ");	
						$current_shift = 'S_1';
						if($current_shifts->num_rows > 0){
							$current_shift = $current_shifts->row[$day_date];
						}
						$current_shift_exp = explode('_', $current_shift);
						$holiday_idss = $holiday_ids;
						if(isset($current_shift_exp[2])){
							$holiday_idss = $holiday_ids.'_'.$current_shift_exp[2];
						} else {
							if($current_shift_exp[0] == 'S'){
								$holiday_idss = $holiday_ids.'_'.$current_shift_exp[1];
							}
						}
						$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$holiday_idss."' WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ");
					}
				}
			}
		} else {
			$d_dept = array();
			foreach ($dept_holiday_pune as $d1key => $d1value) {
				$d1value = html_entity_decode(strtolower(trim($d1value)));
				if(!in_array($d1value, $d_dept)){
					$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$d1value."' AND LOWER(`unit`) = 'pune' ");
					foreach ($emp_codes->rows as $ekey => $evalue) {
						// echo "UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = 'S_1' WHERE `emp_code` = '".$evalue['emp_code']."' ";
						// echo '<br />';
						$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = 'S_1' WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ");
					}
				}
			}
		}

		if (isset($data['dept_holiday_delhi'])) {
			if($dept_holiday_delhi){
				$d_dept = array();
				foreach ($dept_holiday_delhi as $d1key => $d1value) {
					foreach ($data['dept_holiday_delhi'] as $dkey => $dvalue) {
						$dvalue = html_entity_decode(strtolower(trim($dvalue)));
						if(!in_array($dvalue, $d_dept)){
							$d_dept[] = $dvalue;
							$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$dvalue."' AND LOWER(`unit`) = 'delhi' ");
							foreach ($emp_codes->rows as $ekey => $evalue) {
								// echo "UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$holiday_ids."' WHERE `emp_code` = '".$evalue['emp_code']."' ";
								// echo '<br />';
								$current_shifts = $this->db->query("SELECT `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ");	
								$current_shift = 'S_1';
								if($current_shifts->num_rows > 0){
									$current_shift = $current_shifts->row[$day_date];
								}
								$current_shift_exp = explode('_', $current_shift);
								$holiday_idss = $holiday_ids;
								if(isset($current_shift_exp[2])){
									$holiday_idss = $holiday_ids.'_'.$current_shift_exp[2];
								} else {
									if($current_shift_exp[0] == 'S'){
										$holiday_idss = $holiday_ids.'_'.$current_shift_exp[1];
									}
								}
								$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$holiday_idss."' WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ");
							}
						} 
					}
				}
				foreach ($dept_holiday_delhi as $d1key => $d1value) {
					$d1value = html_entity_decode(strtolower(trim($d1value)));
					if(!in_array($d1value, $d_dept)){
						$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$d1value."' AND LOWER(`unit`) = 'delhi' ");
						foreach ($emp_codes->rows as $ekey => $evalue) {
							// echo "UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = 'S_1' WHERE `emp_code` = '".$evalue['emp_code']."' ";
							// echo '<br />';
							$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = 'S_1' WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ");
						}
					}
				}
			} else {
				foreach ($data['dept_holiday_delhi'] as $dkey => $dvalue) {
					$dvalue = html_entity_decode(strtolower(trim($dvalue)));
					$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$dvalue."' AND LOWER(`unit`) = 'delhi' ");
					foreach ($emp_codes->rows as $ekey => $evalue) {
						$current_shifts = $this->db->query("SELECT `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ");	
						$current_shift = 'S_1';
						if($current_shifts->num_rows > 0){
							$current_shift = $current_shifts->row[$day_date];
						}
						$current_shift_exp = explode('_', $current_shift);
						$holiday_idss = $holiday_ids;
						if(isset($current_shift_exp[2])){
							$holiday_idss = $holiday_ids.'_'.$current_shift_exp[2];
						} else {
							if($current_shift_exp[0] == 'S'){
								$holiday_idss = $holiday_ids.'_'.$current_shift_exp[1];
							}
						}
						$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$holiday_idss."' WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ");
					}
				}
			}
		} else {
			$d_dept = array();
			foreach ($dept_holiday_delhi as $d1key => $d1value) {
				$d1value = html_entity_decode(strtolower(trim($d1value)));
				if(!in_array($d1value, $d_dept)){
					$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$d1value."' AND LOWER(`unit`) = 'delhi' ");
					foreach ($emp_codes->rows as $ekey => $evalue) {
						// echo "UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = 'S_1' WHERE `emp_code` = '".$evalue['emp_code']."' ";
						// echo '<br />';
						$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = 'S_1' WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ");
					}
				}
			}
		}

		if (isset($data['dept_holiday_chennai'])) {
			if($dept_holiday_chennai){
				$d_dept = array();
				foreach ($dept_holiday_chennai as $d1key => $d1value) {
					foreach ($data['dept_holiday_chennai'] as $dkey => $dvalue) {
						$dvalue = html_entity_decode(strtolower(trim($dvalue)));
						if(!in_array($dvalue, $d_dept)){
							$d_dept[] = $dvalue;
							$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$dvalue."' AND LOWER(`unit`) = 'chennai' ");
							foreach ($emp_codes->rows as $ekey => $evalue) {
								// echo "UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$holiday_ids."' WHERE `emp_code` = '".$evalue['emp_code']."' ";
								// echo '<br />';
								$current_shifts = $this->db->query("SELECT `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ");	
								$current_shift = 'S_1';
								if($current_shifts->num_rows > 0){
									$current_shift = $current_shifts->row[$day_date];
								}
								$current_shift_exp = explode('_', $current_shift);
								$holiday_idss = $holiday_ids;
								if(isset($current_shift_exp[2])){
									$holiday_idss = $holiday_ids.'_'.$current_shift_exp[2];
								} else {
									if($current_shift_exp[0] == 'S'){
										$holiday_idss = $holiday_ids.'_'.$current_shift_exp[1];
									}
								}
								$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$holiday_idss."' WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ");
							}
						} 
					}
				}
				foreach ($dept_holiday_chennai as $d1key => $d1value) {
					$d1value = html_entity_decode(strtolower(trim($d1value)));
					if(!in_array($d1value, $d_dept)){
						$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$d1value."' AND LOWER(`unit`) = 'chennai' ");
						foreach ($emp_codes->rows as $ekey => $evalue) {
							// echo "UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = 'S_1' WHERE `emp_code` = '".$evalue['emp_code']."' ";
							// echo '<br />';
							$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = 'S_1' WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ");
						}
					}
				}
			} else {
				foreach ($data['dept_holiday_chennai'] as $dkey => $dvalue) {
					$dvalue = html_entity_decode(strtolower(trim($dvalue)));
					$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$dvalue."' AND LOWER(`unit`) = 'chennai' ");
					foreach ($emp_codes->rows as $ekey => $evalue) {
						$current_shifts = $this->db->query("SELECT `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ");	
						$current_shift = 'S_1';
						if($current_shifts->num_rows > 0){
							$current_shift = $current_shifts->row[$day_date];
						}
						$current_shift_exp = explode('_', $current_shift);
						$holiday_idss = $holiday_ids;
						if(isset($current_shift_exp[2])){
							$holiday_idss = $holiday_ids.'_'.$current_shift_exp[2];
						} else {
							if($current_shift_exp[0] == 'S'){
								$holiday_idss = $holiday_ids.'_'.$current_shift_exp[1];
							}
						}
						$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$holiday_idss."' WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ");
					}
				}
			}
		} else {
			$d_dept = array();
			foreach ($dept_holiday_chennai as $d1key => $d1value) {
				$d1value = html_entity_decode(strtolower(trim($d1value)));
				if(!in_array($d1value, $d_dept)){
					$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$d1value."' AND LOWER(`unit`) = 'chennai' ");
					foreach ($emp_codes->rows as $ekey => $evalue) {
						// echo "UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = 'S_1' WHERE `emp_code` = '".$evalue['emp_code']."' ";
						// echo '<br />';
						$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = 'S_1' WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ");
					}
				}
			}
		}

		if (isset($data['dept_holiday_bangalore'])) {
			if($dept_holiday_bangalore){
				$d_dept = array();
				foreach ($dept_holiday_bangalore as $d1key => $d1value) {
					foreach ($data['dept_holiday_bangalore'] as $dkey => $dvalue) {
						$dvalue = html_entity_decode(strtolower(trim($dvalue)));
						if(!in_array($dvalue, $d_dept)){
							$d_dept[] = $dvalue;
							$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$dvalue."' AND LOWER(`unit`) = 'bangalore' ");
							foreach ($emp_codes->rows as $ekey => $evalue) {
								// echo "UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$holiday_ids."' WHERE `emp_code` = '".$evalue['emp_code']."' ";
								// echo '<br />';
								$current_shifts = $this->db->query("SELECT `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ");	
								$current_shift = 'S_1';
								if($current_shifts->num_rows > 0){
									$current_shift = $current_shifts->row[$day_date];
								}
								$current_shift_exp = explode('_', $current_shift);
								$holiday_idss = $holiday_ids;
								if(isset($current_shift_exp[2])){
									$holiday_idss = $holiday_ids.'_'.$current_shift_exp[2];
								} else {
									if($current_shift_exp[0] == 'S'){
										$holiday_idss = $holiday_ids.'_'.$current_shift_exp[1];
									}
								}
								$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$holiday_idss."' WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ");
							}
						} 
					}
				}
				foreach ($dept_holiday_bangalore as $d1key => $d1value) {
					$d1value = html_entity_decode(strtolower(trim($d1value)));
					if(!in_array($d1value, $d_dept)){
						$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$d1value."' AND LOWER(`unit`) = 'bangalore' ");
						foreach ($emp_codes->rows as $ekey => $evalue) {
							// echo "UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = 'S_1' WHERE `emp_code` = '".$evalue['emp_code']."' ";
							// echo '<br />';
							$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = 'S_1' WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ");
						}
					}
				}
			} else {
				foreach ($data['dept_holiday_bangalore'] as $dkey => $dvalue) {
					$dvalue = html_entity_decode(strtolower(trim($dvalue)));
					$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$dvalue."' AND LOWER(`unit`) = 'bangalore' ");
					foreach ($emp_codes->rows as $ekey => $evalue) {
						$current_shifts = $this->db->query("SELECT `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ");	
						$current_shift = 'S_1';
						if($current_shifts->num_rows > 0){
							$current_shift = $current_shifts->row[$day_date];
						}
						$current_shift_exp = explode('_', $current_shift);
						$holiday_idss = $holiday_ids;
						if(isset($current_shift_exp[2])){
							$holiday_idss = $holiday_ids.'_'.$current_shift_exp[2];
						} else {
							if($current_shift_exp[0] == 'S'){
								$holiday_idss = $holiday_ids.'_'.$current_shift_exp[1];
							}
						}
						$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$holiday_idss."' WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ");
					}
				}
			}
		} else {
			$d_dept = array();
			foreach ($dept_holiday_bangalore as $d1key => $d1value) {
				$d1value = html_entity_decode(strtolower(trim($d1value)));
				if(!in_array($d1value, $d_dept)){
					$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$d1value."' AND LOWER(`unit`) = 'bangalore' ");
					foreach ($emp_codes->rows as $ekey => $evalue) {
						// echo "UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = 'S_1' WHERE `emp_code` = '".$evalue['emp_code']."' ";
						// echo '<br />';
						$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = 'S_1' WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ");
					}
				}
			}
		}

		if (isset($data['dept_holiday_ahmedabad'])) {
			if($dept_holiday_ahmedabad){
				$d_dept = array();
				foreach ($dept_holiday_ahmedabad as $d1key => $d1value) {
					foreach ($data['dept_holiday_ahmedabad'] as $dkey => $dvalue) {
						$dvalue = html_entity_decode(strtolower(trim($dvalue)));
						if(!in_array($dvalue, $d_dept)){
							$d_dept[] = $dvalue;
							$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$dvalue."' AND LOWER(`unit`) = 'ahmedabad' ");
							foreach ($emp_codes->rows as $ekey => $evalue) {
								// echo "UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$holiday_ids."' WHERE `emp_code` = '".$evalue['emp_code']."' ";
								// echo '<br />';
								$current_shifts = $this->db->query("SELECT `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ");	
								$current_shift = 'S_1';
								if($current_shifts->num_rows > 0){
									$current_shift = $current_shifts->row[$day_date];
								}
								$current_shift_exp = explode('_', $current_shift);
								$holiday_idss = $holiday_ids;
								if(isset($current_shift_exp[2])){
									$holiday_idss = $holiday_ids.'_'.$current_shift_exp[2];
								} else {
									if($current_shift_exp[0] == 'S'){
										$holiday_idss = $holiday_ids.'_'.$current_shift_exp[1];
									}
								}
								$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$holiday_idss."' WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ");
							}
						} 
					}
				}
				foreach ($dept_holiday_ahmedabad as $d1key => $d1value) {
					$d1value = html_entity_decode(strtolower(trim($d1value)));
					if(!in_array($d1value, $d_dept)){
						$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$d1value."' AND LOWER(`unit`) = 'ahmedabad' ");
						foreach ($emp_codes->rows as $ekey => $evalue) {
							// echo "UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = 'S_1' WHERE `emp_code` = '".$evalue['emp_code']."' ";
							// echo '<br />';
							$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = 'S_1' WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ");
						}
					}
				}
			} else {
				foreach ($data['dept_holiday_ahmedabad'] as $dkey => $dvalue) {
					$dvalue = html_entity_decode(strtolower(trim($dvalue)));
					$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$dvalue."' AND LOWER(`unit`) = 'ahmedabad' ");
					foreach ($emp_codes->rows as $ekey => $evalue) {
						$current_shifts = $this->db->query("SELECT `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ");	
						$current_shift = 'S_1';
						if($current_shifts->num_rows > 0){
							$current_shift = $current_shifts->row[$day_date];
						}
						$current_shift_exp = explode('_', $current_shift);
						$holiday_idss = $holiday_ids;
						if(isset($current_shift_exp[2])){
							$holiday_idss = $holiday_ids.'_'.$current_shift_exp[2];
						} else {
							if($current_shift_exp[0] == 'S'){
								$holiday_idss = $holiday_ids.'_'.$current_shift_exp[1];
							}
						}
						$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = '".$holiday_idss."' WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ");
					}
				}
			}
		} else {
			$d_dept = array();
			foreach ($dept_holiday_ahmedabad as $d1key => $d1value) {
				$d1value = html_entity_decode(strtolower(trim($d1value)));
				if(!in_array($d1value, $d_dept)){
					$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE (`shift_type` = 'F' OR `shift_type` = '') AND `department` = '".$d1value."' AND LOWER(`unit`) = 'ahmedabad' ");
					foreach ($emp_codes->rows as $ekey => $evalue) {
						// echo "UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = 'S_1' WHERE `emp_code` = '".$evalue['emp_code']."' ";
						// echo '<br />';
						$this->db->query("UPDATE " . DB_PREFIX . "shift_schedule SET `".$day_date."` = 'S_1' WHERE `emp_code` = '".$evalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ");
					}
				}
			}
		}
		
	}

	public function deleteholiday($holiday_id) {
		//$this->db->query("DELETE FROM " . DB_PREFIX . "employee_meta WHERE holiday_id = '" . (int)$holiday_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "holiday WHERE holiday_id = '" . (int)$holiday_id . "'");
	}	

	public function getholiday($holiday_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "holiday WHERE holiday_id = '" . (int)$holiday_id . "'");
		return $query->row;
	}

	public function getholidays($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "holiday WHERE 1=1 ";

		if (!empty($data['filter_name'])) {
			$data['filter_name'] = html_entity_decode($data['filter_name']);
			$sql .= " AND LOWER(name) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}

		$sort_data = array(
			'name',
		);	

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY name";	
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}					

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}				

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalholidays($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "holiday WHERE 1=1 ";
		
		if (!empty($data['filter_name'])) {
			$sql .= " AND LOWER(name) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
		}

		$query = $this->db->query($sql);
		return $query->row['total'];
	}
}
?>
