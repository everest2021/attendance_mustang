<?php
class ModelCatalogLeave extends Model {
	public function addleave($data) {
		// $this->db->query("INSERT INTO " . DB_PREFIX . "employee SET name = '" . $this->db->escape($data['name']) . "', trainer = '" . $this->db->escape($data['trainer_id']) . "' ");
		// $employee_id = $this->db->getLastId();
		// if(isset($data['owners'])){
		// 	foreach ($data['owners'] as $okey => $ovalue) {
		// 		$this->db->query("INSERT INTO " . DB_PREFIX . "employee_owner SET  employee_id = '" . (int)$employee_id . "', owner = '" . $this->db->escape($ovalue['o_name_id']) . "', share = '" . (float)$ovalue['o_share'] . "' ");	
		// 	}
		// }
	}

	public function insert_leavedata($data) {
		// echo '<pre>';
		// print_r($data);
		// exit;
		if($data['leave_id'] == '0'){	
			$this->db->query("INSERT INTO " . DB_PREFIX . "leave SET 
								emp_id = '" . $this->db->escape($data['e_name_id']) . "', 
								emp_name = '" . $this->db->escape($data['e_name']) . "', 
								emp_doj = '" . $this->db->escape($data['emp_doj']) . "', 
								emp_grade = '" . $this->db->escape($data['emp_grade']) . "', 
								emp_designation = '" . $this->db->escape($data['emp_designation']) . "', 
								pl_acc = '" . $this->db->escape($data['pl_acc']) . "', 
								bl_acc = '" . $this->db->escape($data['bl_acc']) . "', 
								sl_acc = '" . $this->db->escape($data['sl_acc']) . "',
								lwp_acc = '" . $this->db->escape($data['pl_acc']) . "', 
								ml_acc = '" . $this->db->escape($data['ml_acc']) . "', 
								mal_acc = '" . $this->db->escape($data['mal_acc']) . "', 
								pal_acc = '" . $this->db->escape($data['pal_acc']) . "',
								cof_acc = '" . $this->db->escape($data['cof_acc']) . "',
								pl_bal = '" . $this->db->escape($data['pl_acc']) . "', 
								bl_bal = '" . $this->db->escape($data['bl_acc']) . "', 
								sl_bal = '" . $this->db->escape($data['sl_acc']) . "',
								lwp_bal = '" . $this->db->escape($data['lwp_acc']) . "', 
								ml_bal = '" . $this->db->escape($data['ml_acc']) . "', 
								mal_bal = '" . $this->db->escape($data['mal_acc']) . "', 
								pal_bal = '" . $this->db->escape($data['pal_acc']) . "',
								cof_bal = '" . $this->db->escape($data['cof_acc']) . "', 
								year = '" . $this->db->escape($data['year']) . "' ");
		} else {
			$this->db->query("UPDATE " . DB_PREFIX . "leave SET 
								emp_id = '" . $this->db->escape($data['e_name_id']) . "',
								emp_name = '" . $this->db->escape($data['e_name']) . "',  
								emp_doj = '" . $this->db->escape($data['emp_doj']) . "', 
								emp_grade = '" . $this->db->escape($data['emp_grade']) . "',
								emp_designation = '" . $this->db->escape($data['emp_designation']) . "', 
								pl_acc = '" . $this->db->escape($data['pl_acc']) . "', 
								bl_acc = '" . $this->db->escape($data['bl_acc']) . "', 
								sl_acc = '" . $this->db->escape($data['sl_acc']) . "',
								lwp_acc = '" . $this->db->escape($data['lwp_acc']) . "', 
								ml_acc = '" . $this->db->escape($data['ml_acc']) . "', 
								mal_acc = '" . $this->db->escape($data['mal_acc']) . "', 
								pal_acc = '" . $this->db->escape($data['pal_acc']) . "',
								cof_acc = '" . $this->db->escape($data['cof_acc']) . "',
								year = '" . $this->db->escape($data['year']) . "'
								WHERE leave_id = '" . (int)$data['leave_id'] . "'");
		}
	}

	public function deleteleave($leave_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "leave WHERE leave_id = '" . (int)$leave_id . "'");
	}	

	public function getleave($emp_code) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "leave WHERE emp_id = '" . (int)$emp_code . "' AND `close_status` = '0' ");
		return $query->rows;
	}

	public function getEmployees_dat($emp_code) {
		$sql = "SELECT * FROM `" . DB_PREFIX . "employee` WHERE `emp_code` = '".$emp_code."'";
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function getEmployeeLeaveData($emp_id) {
		$sql = "SELECT * FROM ".DB_PREFIX."leave WHERE `emp_id` = '".$emp_id."' AND `close_status` = '0' ORDER BY 'year' DESC";	
		$query = $this->db->query($sql);
		return $query->row;	
	}

	public function insertBlankEmpData($data) {
		$sql = "INSERT INTO ".DB_PREFIX."leave SET `emp_id` = '".$data['emp_code']."', `emp_name` = '".$data['name']."', `emp_doj` = '".$data['doj']."', `emp_grade` = '".$data['grade']."', `year` = '".$data['new_year']."', `close_status` = '0'";
		$this->db->query($sql);
	}

	public function copyLeaveEmpData($data) {
		$sql = "INSERT INTO ".DB_PREFIX."leave SET `emp_id` = '".$data['emp_id']."', `emp_name` = '".$data['emp_name']."', `emp_doj` = '".$data['emp_doj']."', `emp_grade` = '".$data['emp_grade']."', `pl_acc` = '".$data['pl_acc']."', `cl_acc` = '".$data['cl_acc']."', `sl_acc` = '".$data['sl_acc']."', `cof_acc` = '0', `pl_bal` = '".$data['pl_bal']."', `cl_bal` = '".$data['cl_bal']."', `sl_bal` = '".$data['sl_bal']."', `cof_bal` = '0', `year` = '".$data['new_year']."', `close_status` = '0'";
		//echo $sql;exit;
		$this->db->query($sql);
	}

	public function changeStatus($year) {
		$sql = "UPDATE ".DB_PREFIX."leave SET `close_status` = '1' WHERE `year` = '".$year."'";
		$this->db->query($sql);
	}

	public function getEmployeeLeaveYearData() {
		$sql = "SELECT `year` FROM ".DB_PREFIX."leave WHERE `close_status` = '0' ORDER BY 'year' DESC";	
		$query = $this->db->query($sql);
		return $query->row['year'];	
	}
}

?>
