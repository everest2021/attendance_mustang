<?php
class ModelCatalogEmployee extends Model {
	public function addemployee($data) {
		
		$location_datas = $this->db->query("SELECT * FROM " . DB_PREFIX . "unit WHERE `unit_id` = '" . (int)$data['unit']. "'")->row;
		// echo'<pre>';
		// print_r($data);
		// exit;
		$chunk_id = 0;
		if($data['shift_type'] == 'R'){
			$chunk_1_count = $this->db->query("SELECT COUNT(*) AS chunk_1 FROM oc_employee WHERE chunk_id = 1 ");
			$chunk_2_count = $this->db->query("SELECT COUNT(*) AS chunk_2 FROM oc_employee WHERE chunk_id = 2 ");
			$chunk_3_count = $this->db->query("SELECT COUNT(*) AS chunk_3 FROM oc_employee WHERE chunk_id = 3 ");

			$chunk_1_counts = 0;
			if ($chunk_1_count->num_rows > 0) {
				$chunk_1_counts = $chunk_1_count->row['chunk_1'];
			}

			$chunk_2_counts = 0;
			if ($chunk_2_count->num_rows > 0) {
				$chunk_2_counts = $chunk_2_count->row['chunk_2'];
			}

			$chunk_3_counts = 0;
			if ($chunk_3_count->num_rows > 0) {
				$chunk_3_counts = $chunk_3_count->row['chunk_3'];
			}

			$minimum_count = min($chunk_1_counts, $chunk_2_counts, $chunk_3_counts);
			// echo "<pre>";
			// print_r($minimum_count);
			// exit;

			if ($minimum_count == $chunk_1_count) {
				$chunk_id = 1;
			} elseif ($minimum_count == $chunk_2_count) {
				$chunk_id = 2;
			} else {
				$chunk_id = 3;
			}
		}
		$data['dept_head_list'] = array();
		$this->db->query("INSERT INTO " . DB_PREFIX . "employee SET 
							name = '" . $this->db->escape($data['name']) . "', 
							card_number = '" . $this->db->escape($data['emp_code']) . "', 
							emp_code = '" . $this->db->escape($data['emp_code']) . "', 
							gender = '" . $this->db->escape($data['gender']) . "', 
							designation = '" . $this->db->escape($data['designation']) . "', 
							designation_id = '" . $this->db->escape($data['designation_id']) . "',
							department = '" . $this->db->escape($data['department']) . "', 
							department_id = '" . $this->db->escape($data['department_id']) . "',
							grade = '" . $this->db->escape($data['grade']) . "', 
							grade_id = '" . $this->db->escape($data['grade_id']) . "',
							chunk_id = '" . $this->db->escape($chunk_id) . "',
							dob = '" . $this->db->escape($data['dob']) . "', 
							doj = '" . $this->db->escape($data['doj']) . "',
							location_id = '" . $this->db->escape($data['locations_id']) . "',  
							doc = '" . $this->db->escape($data['doc']) . "',  
							dol = '" . $this->db->escape($data['dol']) . "',  
							unit = '" . $this->db->escape($location_datas['unit']) . "',
							unit_id = '".$this->db->escape($data['unit'])."'  ,
							esic_no = '" . $this->db->escape($data['esic_no']) . "',  
							pfuan_no = '" . $this->db->escape($data['pfuan_no']) . "',  
							basic = '" . $this->db->escape($data['basic']) . "',  
							incentive = '" . $this->db->escape($data['incentive']) . "',  
							perdaysalary = '" . $this->db->escape($data['perdaysalary']) . "',  
							perdaysalary2 = '" . $this->db->escape($data['perdaysalary2']) . "',  
							shift_type = '".$data['shift_type']."',
							ot_calculate = '".$this->db->escape($data['ot_calculate'])."',  
							work_hour = '".$this->db->escape($data['work_hour'])."',  
							work_hour_cut = '".$this->db->escape($data['work_hour_cut'])."',  
							is_lunch = '".$this->db->escape($data['is_lunch'])."',  
							ot_hours = '".$this->db->escape($data['ot_hours'])."',
							ot_days = '".$this->db->escape($data['ot_days'])."',  
							dept_head_list = '" . $this->db->escape(implode(',', $data['dept_head_list'])) . "',
							status = '" . $this->db->escape($data['status']) . "',
							reporting_to = '".$this->db->escape($data['reporting_to'])."',
							reporting_to_name = '".$this->db->escape($data['reporting_to_name'])."'
							 ");
		$employee_id = $this->db->getLastId();
		$emp_code = $data['emp_code'];
		$user_name = $emp_code;
		$salt = substr(md5(uniqid(rand(), true)), 0, 9);
		$password = sha1($salt . sha1($salt . sha1($emp_code)));
		$sql = "UPDATE `oc_employee` set `username` = '".$this->db->escape($emp_code)."', `password` = '".$this->db->escape($password)."', `salt` = '".$this->db->escape($salt)."', `is_set` = '0', `user_group_id` = '11' WHERE `employee_id` = '".$employee_id."' ";
		$this->db->query($sql);
		
		$months_array = array(
			'1' => '1',
			'2' => '2',
			'3' => '3',
			'4' => '4',
			'5' => '5',
			'6' => '6',
			'7' => '7',
			'8' => '8',
			'9' => '9',
			'10' => '10',
			'11' => '11',
			'12' => '12',
		);
		
		if($data['shift_type'] == 'F'){
			$shift_id = '6';
		} else {
			$shift_id = '1';
		}

		$shift_id_data = 'S_'.$shift_id;
		/*
		$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE `unit` = '".$data['unit']."' AND `emp_code` <> '".$data['emp_code']."' LIMIT 1");
		if($emp_codes->num_rows > 0){ 
			$emp_codes = $emp_codes->row;
			$emp_code = $emp_codes['emp_code'];
		
			$exist_schedules = $this->db->query("SELECT * FROM `oc_shift_schedule` WHERE `emp_code` = '".$employee_id."' AND `year` = '".date('Y')."' ");
			if($exist_schedules->num_rows > 0){
				$exist_schedule = $exist_schedules->rows;
				foreach($exist_schedule as $ekeys => $evalues){
					$sql = "INSERT INTO `oc_shift_schedule` SET `1` = '".$evalues['1']."', `2` = '".$evalues['2']."', `3` = '".$evalues['3']."', `4` = '".$evalues['4']."', `5` = '".$evalues['5']."', `6` = '".$evalues['6']."', `7` = '".$evalues['7']."', `8` = '".$evalues['8']."', `9` = '".$evalues['9']."', `10` = '".$evalues['10']."', `11` = '".$evalues['11']."', `12` = '".$evalues['12']."', `13` = '".$evalues['13']."', `14` = '".$evalues['14']."', `15` = '".$evalues['15']."', `16` = '".$evalues['16']."', `17` = '".$evalues['17']."', `18` = '".$evalues['18']."', `19` = '".$evalues['19']."', `20` = '".$evalues['20']."', `21` = '".$evalues['21']."', `22` = '".$evalues['22']."', `23` = '".$evalues['23']."', `24` = '".$evalues['24']."', `25` = '".$evalues['25']."', `26` = '".$evalues['26']."', `27` = '".$evalues['27']."', `28` = '".$evalues['28']."', `29` = '".$evalues['29']."', `30` = '".$evalues['30']."', `31` = '".$evalues['31']."', `emp_code` = '".$employee_id."', `status` = '1', `month` = '".$evalues['month']."', `year` = '".$evalues['year']."', `unit` = '".$evalues['unit']."' "; 
					$this->db->query($sql);
				}
			}
		} else {
		*/
			foreach ($months_array as $key => $value) {
				$insert1 = "INSERT INTO `oc_shift_schedule` SET 
					`emp_code` = '".$employee_id."',
					`1` = '".$shift_id_data."',
					`2` = '".$shift_id_data."',
					`3` = '".$shift_id_data."',
					`4` = '".$shift_id_data."',
					`5` = '".$shift_id_data."',
					`6` = '".$shift_id_data."',
					`7` = '".$shift_id_data."',
					`8` = '".$shift_id_data."',
					`9` = '".$shift_id_data."',
					`10` = '".$shift_id_data."',
					`11` = '".$shift_id_data."',
					`12` = '".$shift_id_data."', 
					`13` = '".$shift_id_data."', 
					`14` = '".$shift_id_data."', 
					`15` = '".$shift_id_data."', 
					`16` = '".$shift_id_data."', 
					`17` = '".$shift_id_data."', 
					`18` = '".$shift_id_data."', 
					`19` = '".$shift_id_data."', 
					`20` = '".$shift_id_data."', 
					`21` = '".$shift_id_data."', 
					`22` = '".$shift_id_data."', 
					`23` = '".$shift_id_data."', 
					`24` = '".$shift_id_data."', 
					`25` = '".$shift_id_data."', 
					`26` = '".$shift_id_data."', 
					`27` = '".$shift_id_data."', 
					`28` = '".$shift_id_data."', 
					`29` = '".$shift_id_data."', 
					`30` = '".$shift_id_data."', 
					`31` = '".$shift_id_data."',  
					`month` = '".$key."',
					`year` = '".date('Y')."',
					`status` = '1' ,
					`unit` = '".$data['unit']."' "; 
					// echo "<pre>";
					// echo $insert1;
				$this->db->query($insert1);
			}
		//}
		/*
		$filter_year = date('Y');
		$current_year = $filter_year;
		$next_year = $filter_year + 1;
		$start = new DateTime($current_year.'-01-01');
		$end   = new DateTime($next_year.'-01-01');
		$interval = DateInterval::createFromDateString('1 day');
		$period = new DatePeriod($start, $interval, $end);
		$week_array = array();
		foreach ($period as $dt) {
		    if ($dt->format('N') == 6 || $dt->format('N') == 7) {
		    	$week_array[$dt->format('Y-m-d')]['date'] = $dt->format('Y-m-d');
		    	$week_array[$dt->format('Y-m-d')]['day'] = $dt->format('j');
		    	$week_array[$dt->format('Y-m-d')]['month'] = $dt->format('n');
		    	$week_array[$dt->format('Y-m-d')]['year'] = $dt->format('Y');
		    }
		}

		$weekly_off = 'W_1_1';
		foreach ($week_array as $wkey => $wvalue) {
			$sql = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = '".$weekly_off."' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$employee_id."' ";	
        	$this->db->query($sql);
		}
		*/
	}

	public function editemployee($employee_id, $data) {
		$location_datas = $this->db->query("SELECT * FROM " . DB_PREFIX . "unit WHERE `unit_id` = '" . (int)$data['unit']. "'")->row;
		/*echo '<pre>';
		print_r($data);
		exit;*/
		$chunk_id = 0;
		if($data['shift_type'] == 'R'){
			$chunk_1_count = $this->db->query("SELECT COUNT(*) AS chunk_1 FROM oc_employee WHERE chunk_id = 1 ");
			$chunk_2_count = $this->db->query("SELECT COUNT(*) AS chunk_2 FROM oc_employee WHERE chunk_id = 2 ");
			$chunk_3_count = $this->db->query("SELECT COUNT(*) AS chunk_3 FROM oc_employee WHERE chunk_id = 3 ");

			$chunk_1_counts = 0;
			if ($chunk_1_count->num_rows > 0) {
				$chunk_1_counts = $chunk_1_count->row['chunk_1'];
			}

			$chunk_2_counts = 0;
			if ($chunk_2_count->num_rows > 0) {
				$chunk_2_counts = $chunk_2_count->row['chunk_2'];
			}

			$chunk_3_counts = 0;
			if ($chunk_3_count->num_rows > 0) {
				$chunk_3_counts = $chunk_3_count->row['chunk_3'];
			}

			$minimum_count = min($chunk_1_counts, $chunk_2_counts, $chunk_3_counts);
			// echo "<pre>";
			// print_r($minimum_count);
			// exit;

			if ($minimum_count == $chunk_1_count) {
				$chunk_id = 1;
			} elseif ($minimum_count == $chunk_2_count) {
				$chunk_id = 2;
			} else {
				$chunk_id = 3;
			}
		}
		$data['dept_head_list'] = array();
		$this->db->query("UPDATE " . DB_PREFIX . "employee SET 
							name = '" . $this->db->escape($data['name']) . "',
							card_number = '" . $this->db->escape($data['emp_code']) . "', 
							emp_code = '" . $this->db->escape($data['emp_code']) . "', 
							shift_type = '".$data['shift_type']."',
							gender = '" . $this->db->escape($data['gender']) . "', 
							designation = '" . $this->db->escape($data['designation']) . "', 
							designation_id = '" . $this->db->escape($data['designation_id']) . "',
							department = '" . $this->db->escape($data['department']) . "', 
							department_id = '" . $this->db->escape($data['department_id']) . "',
							grade = '" . $this->db->escape($data['grade']) . "', 
							grade_id = '" . $this->db->escape($data['grade_id']) . "',
							chunk_id = '" . $this->db->escape($chunk_id) . "',
							dob = '" . $this->db->escape($data['dob']) . "', 
							doj = '" . $this->db->escape($data['doj']) . "', 
							location_id = '" . $this->db->escape($data['locations_id']) . "',  
							doc = '" . $this->db->escape($data['doc']) . "',  
							dol = '" . $this->db->escape($data['dol']) . "', 
							unit_id = '".$this->db->escape($data['unit'])."', 
							unit = '" . $this->db->escape($location_datas['unit']) . "',
							esic_no = '" . $this->db->escape($data['esic_no']) . "',  
							pfuan_no = '" . $this->db->escape($data['pfuan_no']) . "',  
							basic = '" . $this->db->escape($data['basic']) . "',
							incentive = '" . $this->db->escape($data['incentive']) . "',  
							perdaysalary = '" . $this->db->escape($data['perdaysalary']) . "',  
							perdaysalary2 = '" . $this->db->escape($data['perdaysalary2']) . "',
							ot_calculate = '".$this->db->escape($data['ot_calculate'])."',
							work_hour = '".$this->db->escape($data['work_hour'])."',  
							work_hour_cut = '".$this->db->escape($data['work_hour_cut'])."',
							is_lunch = '".$this->db->escape($data['is_lunch'])."',  
							ot_hours = '".$this->db->escape($data['ot_hours'])."',
							ot_days = '".$this->db->escape($data['ot_days'])."',
							dept_head_list = '" . $this->db->escape(implode(',', $data['dept_head_list'])) . "',
							status = '" . $this->db->escape($data['status']) . "',
							reporting_to = '".$this->db->escape($data['reporting_to'])."',
							reporting_to_name = '".$this->db->escape($data['reporting_to_name'])."'
							WHERE employee_id = '" . (int)$employee_id . "'");
		
		if($data['password_reset'] == '1'){
			$emp_code = $data['emp_code'];
			$user_name = $emp_code;
			$salt = substr(md5(uniqid(rand(), true)), 0, 9);
			$password = sha1($salt . sha1($salt . sha1($emp_code)));

			$sql = "UPDATE `oc_employee` set `username` = '".$this->db->escape($emp_code)."', `password` = '".$this->db->escape($password)."', `salt` = '".$this->db->escape($salt)."', `is_set` = '0' WHERE `employee_id` = '".$employee_id."' ";
			$this->db->query($sql);
		}
		/*
		if($data['emp_code'] != $data['hidden_emp_code']){
			$shift_schedule_sql = $this->db->query("UPDATE `oc_shift_schedule` SET `emp_code` = '".$data['emp_code']."' WHERE `emp_code` = '".$data['hidden_emp_code']."' ");
			$leave_sql = $this->db->query("UPDATE `oc_leave` SET `emp_id` = '".$data['emp_code']."' WHERE `emp_id` = '".$data['hidden_emp_code']."' ");
			$leave_emp_sql = $this->db->query("UPDATE `oc_leave_employee` SET `emp_id` = '".$data['emp_code']."' WHERE `emp_id` = '".$data['hidden_emp_code']."' ");
			$leave_transaction_sql = $this->db->query("UPDATE `oc_leave_transaction` SET `emp_id` = '".$data['emp_code']."' WHERE `emp_id` = '".$data['hidden_emp_code']."' ");
			$transaction_sql = $this->db->query("UPDATE `oc_transaction` SET `emp_id` = '".$data['emp_code']."' WHERE `emp_id` = '".$data['hidden_emp_code']."' ");
		}
		*/
		if($data['name'] != $data['hidden_name']){
			$leave_sql = $this->db->query("UPDATE `oc_leave` SET `emp_name` = '".$data['name']."' WHERE `emp_id` = '".$employee_id."' ");
			$transaction_sql = $this->db->query("UPDATE `oc_transaction` SET `emp_name` = '".$this->db->escape($data['name'])."' WHERE `emp_id` = '".$employee_id."' ");
		}
		//$this->db->query("UPDATE " . DB_PREFIX . "employee SET name = '" . $this->db->escape($data['name']) . "', grade = '" . $this->db->escape($data['grade']) . "', branch = '" . $this->db->escape($data['branch']) . "', department = '" . $this->db->escape($data['department']) . "', division = '" . $this->db->escape($data['division']) . "', group = '" . $this->db->escape($data['group']) . "', category = '" . $this->db->escape($data['category']) . "', unit = '" . $this->db->escape($data['unit']) . "', designation = '" . $this->db->escape($data['designation']) . "', dob = '" . $this->db->escape($data['dob']) . "', doj = '" . $this->db->escape($data['doj']) . "', doc = '" . $this->db->escape($data['doc']) . "', dol = '" . $this->db->escape($data['dol']) . "', emp_code = '" . $this->db->escape($data['emp_code']) . "', shift = '" . $this->db->escape($data['shift_id']) . "', card_number = '" . $this->db->escape($data['card_number']) . "', status = '" . $this->db->escape($data['status']) . "', daily_punch = '" . $this->db->escape($data['daily_punch']) . "', weekly_off = '" . $this->db->escape($data['weekly_off']) . "', gender = '" . $this->db->escape($data['gender']) . "' WHERE employee_id = '" . (int)$employee_id . "'");
		//echo "UPDATE " . DB_PREFIX . "employee SET card_number = '" . $this->db->escape($data['card_number']) . "', status = '" . $this->db->escape($data['status']) . "', daily_punch = '" . $this->db->escape($data['daily_punch']) . "', weekly_off = '" . $this->db->escape($data['weekly_off']) . "' WHERE employee_id = '" . (int)$employee_id . "'";
		//exit;
		//$this->log->write("UPDATE " . DB_PREFIX . "employee SET name = '" . $this->db->escape($data['name']) . "', location = '" . $this->db->escape($data['location_id']) . "', category = '" . $this->db->escape($data['category_id']) . "', emp_code = '" . $this->db->escape($data['emp_code']) . "', department = '" . $this->db->escape($data['department_id']) . "', shift = '" . $this->db->escape($data['shift_id']) . "', card_number = '" . $this->db->escape($data['card_number']) . "', status = '" . $this->db->escape($data['status']) . "', daily_punch = '" . $this->db->escape($data['daily_punch']) . "', weekly_off = '" . $this->db->escape($data['weekly_off']) . "', gender = '" . $this->db->escape($data['gender']) . "' WHERE employee_id = '" . (int)$employee_id . "'");
		//location = '" . $this->db->escape($data['location_id']) . "', category = '" . $this->db->escape($data['category_id']) . "', 
	}

	public function deleteemployee($employee_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "employee WHERE employee_id = '" . (int)$employee_id . "'");
	}	

	public function getemployee($employee_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "employee WHERE employee_id = '" . (int)$employee_id . "'");
		return $query->row;
	}

	public function getcatname($category_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "categories WHERE id = '" . (int)$category_id . "'");
		if($query->num_rows > 0){
			return $query->row['name'];
		} else {
			return '';
		}
	}

	public function getdeptname($department_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "department WHERE id = '" . (int)$department_id . "'");
		if($query->num_rows > 0){
			return $query->row['name'];
		} else {
			return '';
		}
	}

	public function getshiftname($shift_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "shift WHERE shift_id = '" . (int)$shift_id . "'");
		if($query->num_rows > 0){
			return $query->row['name'];
		} else {
			return '';
		}
	}

	public function getweekname($week_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "week WHERE week_id = '" . (int)$week_id . "'");
		if($query->num_rows > 0){
			return $query->row['name'];
		} else {
			return '';
		}
	}

	public function getholidayname($holiday_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "holiday WHERE holiday_id = '" . (int)$holiday_id . "'");
		if($query->num_rows > 0){
			return $query->row['name'];
		} else {
			return '';
		}
	}

	public function getshift_id($emp_code, $day_date, $month, $year) {
		$query = $this->db->query("SELECT `".$day_date."` FROM " . DB_PREFIX . "shift_schedule WHERE emp_code = '" . (int)$emp_code . "' ");
		if($query->num_rows > 0){
			return $query->row[$day_date];
		} else {
			return '';
		}
	}

	public function getlocname($location_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "location WHERE id = '" . (int)$location_id . "'");
		if($query->num_rows > 0){
			return $query->row['name'];
		} else {
			return '';
		}
	}

	public function getemployees($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "employee WHERE 1=1 ";

		if (!empty($data['filter_name'])) {
			$data['filter_name'] = html_entity_decode($data['filter_name']);
			//if(ctype_digit($data['filter_name'])){
			if(is_numeric(substr($data['filter_name'], -1, 1))){
				$sql .= " AND emp_code = '" . $data['filter_name'] . "' ";	
			} else {
				$sql .= " AND LOWER(name) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
			}
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}

		if (isset($data['filter_emp_code']) && !empty($data['filter_emp_code'])) {
			$filter_emp_code = $data['filter_emp_code'];
			$sql .= " AND `reporting_to` = '" . $this->db->escape($filter_emp_code) . "' ";
		}

		if (isset($data['filter_name_id']) && !empty($data['filter_name_id'])) {
			$sql .= " AND emp_code = '" . $data['filter_name_id'] . "' ";
		}

		if (isset($data['filter_code']) && !empty($data['filter_code'])) {
			$sql .= " AND `emp_code` = '" . $this->db->escape($data['filter_code']) . "' ";
		}

		if (isset($data['filter_department']) && !empty($data['filter_department'])) {
			$sql .= " AND LOWER(department) = '" . $this->db->escape(strtolower($data['filter_department'])) . "' ";
		}

		if (isset($data['filter_departments']) && !empty($data['filter_departments'])) {
			$sql .= " AND LOWER(`department`) IN (" . strtolower($data['filter_departments']) . ") ";
		}

		if (isset($data['filter_unit']) && !empty($data['filter_unit'])) {
			$sql .= " AND LOWER(unit) = '" . $this->db->escape(strtolower($data['filter_unit'])) . "' ";
		}

		if (isset($data['filter_shift_type'])) {
			$sql .= " AND shift_type = 'R' ";
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}

		if($this->user->getUnitId() != '0'){
			$sql .= " AND `unit_id` = '".$this->user->getUnitId()."' ";
		}

		$sort_data = array(
			'name',
		);	

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY name";	
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}					

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}	
		//$this->log->write($sql);			
		//echo $sql;exit;
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalemployees($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "employee WHERE 1=1 ";
		
		if (!empty($data['filter_name'])) {
			$sql .= " AND LOWER(name) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
		}

		if (isset($data['filter_code']) && !empty($data['filter_code'])) {
			$sql .= " AND `emp_code` = '" . $this->db->escape($data['filter_code']) . "' ";
		}

		if (isset($data['filter_emp_code']) && !empty($data['filter_emp_code'])) {
			$filter_emp_code = $data['filter_emp_code'];
			$sql .= " AND `reporting_to` = '" . $this->db->escape($filter_emp_code) . "' ";
		}

		if (isset($data['filter_department']) && !empty($data['filter_department'])) {
			$sql .= " AND LOWER(department) = '" . $this->db->escape(strtolower($data['filter_department'])) . "' ";
		}

		if (isset($data['filter_departments']) && !empty($data['filter_departments'])) {
			$sql .= " AND LOWER(`department`) IN (" . strtolower($data['filter_departments']) . ") ";
		}

		if (isset($data['filter_unit']) && !empty($data['filter_unit'])) {
			$sql .= " AND LOWER(unit) = '" . $this->db->escape(strtolower($data['filter_unit'])) . "' ";
		}

		if($this->user->getUnitId() != '0'){
			$sql .= " AND `unit_id` = '".$this->user->getUnitId()."' ";
		}

		if (isset($data['filter_shift_type'])) {
			$sql .= " AND shift_type = 'R' ";
			//$sql .= " AND LOWER(name) REGEXP '^" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}

		$query = $this->db->query($sql);
		return $query->row['total'];
	}

	public function getActiveEmployee() {
		$sql = "SELECT `emp_code`, `name`, `doj`, `grade`, `group` FROM ".DB_PREFIX."employee WHERE `status` = '1'";	
		$query = $this->db->query($sql);
		return $query->rows;	
	}
}
?>