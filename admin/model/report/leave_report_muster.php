<?php
class ModelReportLeavereportmuster extends Model {
	public function getAttendance($data) {
		//print_r($data);
		$sql = "SELECT * FROM `oc_transaction` WHERE 1=1";
		if (!empty($data['filter_date_start'])) {
			$sql .= " AND DATE(`date`) >= '" . $this->db->escape($data['filter_date_start']) . "'";
		}

		if (!empty($data['filter_date_end'])) {
			$sql .= " AND DATE(`date`) <= '" . $this->db->escape($data['filter_date_end']) . "'";
		}

		if (!empty($data['filter_month'])) {
			$sql .= " AND MONTH(`date`) = '" . $this->db->escape($data['filter_month']) . "'";
		}

		if (!empty($data['filter_year'])) {
			$sql .= " AND YEAR(`date`) = '" . $this->db->escape($data['filter_year']) . "'";
		}

		if (!empty($data['filter_name'])) {
			$sql .= " AND `emp_name` = '" . $this->db->escape($data['filter_name']) . "'";
		}

		if (!empty($data['filter_name_id'])) {
			$sql .= " AND `emp_id` = '" . $this->db->escape($data['filter_name_id']) . "'";
		}
		//$sql .= " AND `emp_id` = '21463' ";
		$sql .= ' ORDER BY `date` ';	
		// echo $sql;
		// echo '<br />';
		//exit;	
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getEmployees($data) {
		//print_r($data);
		$sql = "SELECT * FROM `oc_employee` WHERE 1=1";
		if (!empty($data['filter_name_id'])) {
			$sql .= " AND `emp_code` = '" . $this->db->escape($data['filter_name_id']) . "'";
		}
		if (!empty($data['filter_unit'])) {
			$sql .= " AND `unit` = '" . $this->db->escape($data['filter_unit']) . "'";
		}
		if (!empty($data['filter_gender'])) {
			if($data['filter_gender'] == '1'){
				$sql .= " AND (`gender` = 'Male' OR `gender` = '') ";
			} elseif($data['filter_gender'] == '2'){
				$sql .= " AND (`gender` = 'Female') ";
			}
		}
		$sql .= " AND `status` = '1' ORDER BY `emp_code` ";
		//$sql .= " AND `emp_id` = '21463' ";
		//echo $sql;exit;	
		$query = $this->db->query($sql);
		return $query->rows;
	}
}
?>