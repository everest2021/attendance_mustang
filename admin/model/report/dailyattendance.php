<?php
class ModelReportDailyAttendance extends Model {
	public function getAttendance($data) {
		//print_r($data);
		$sql = "SELECT * FROM `oc_transaction` WHERE 1=1";
		if (!empty($data['filter_date_start'])) {
			$sql .= " AND DATE(`date`) >= '" . $this->db->escape($data['filter_date_start']) . "'";
		}

		if (!empty($data['filter_date_end'])) {
			$sql .= " AND DATE(`date`) <= '" . $this->db->escape($data['filter_date_end']) . "'";
		}

		if (!empty($data['filter_name'])) {
			$sql .= " AND `emp_name` = '" . $this->db->escape($data['filter_name']) . "'";
		}

		if (!empty($data['filter_name_id'])) {
			$sql .= " AND `emp_id` = '" . $this->db->escape($data['filter_name_id']) . "'";
		}

		/*if (!empty($data['unit'])) {
			$sql .= " AND LOWER(`unit`) = '" . $this->db->escape(strtolower($data['unit'])) . "'";
		}
		if (!empty($data['department'])) {
			$sql .= " AND LOWER(`department`) = '" . $this->db->escape(strtolower($data['department'])) . "'";
		}
		if (!empty($data['group'])) {
			$sql .= " AND LOWER(`group`) = '" . $this->db->escape(strtolower($data['group'])) . "'";
		}

		if ($data['status'] == 1) {
			$sql .= " AND (`present_status` = '1' OR `present_status` = '0.5' OR `weekly_off` <> '0' OR `holiday_id` <> '0' OR (`halfday_status` <> '0' AND `absent_status` = '0') OR `leave_status` <> '0' OR `compli_status` <> '0')  ";
		} elseif($data['status'] == 2) {
			$sql .= " AND (`absent_status` = '1')";
			//$sql .= " AND (`absent_status` = '1' OR `absent_status` = '0.5')";
		} else {
			//$sql .= " AND (`present_status` = '1' OR `present_status` = '0.5' OR  `weekly_off` <> '0' OR `holiday_id` <> '0' OR `halfday_status` <> '0' OR `leave_status` <> '0')  ";
		}*/
		//$sql .= " AND `emp_id` = '21463' ";
		$sql .= ' ORDER BY `date` ';	
		//echo $sql;exit;	
		$query = $this->db->query($sql);//print_r($query);
		//echo $sql;exit;
		return $query->rows;
	}

	public function getAttendance1($data) {
		//print_r($data);
		$sql = "SELECT SEC_TO_TIME( SUM( TIME_TO_SEC( `working_time` ) ) ) As `total_time` FROM `oc_transaction` WHERE 1=1";
		if (!empty($data['filter_date_start'])) {
			$sql .= " AND DATE(`date`) >= '" . $this->db->escape($data['filter_date_start']) . "'";
		}

		if (!empty($data['filter_date_end'])) {
			$sql .= " AND DATE(`date`) <= '" . $this->db->escape($data['filter_date_end']) . "'";
		}

		if (!empty($data['filter_name'])) {
			$sql .= " AND `emp_name` = '" . $this->db->escape($data['filter_name']) . "'";
		}

		if (!empty($data['filter_name_id'])) {
			$sql .= " AND `emp_id` = '" . $this->db->escape($data['filter_name_id']) . "'";
		}

		$sql .= ' ORDER BY `date` ';	
		//echo $sql;exit;	
		$query = $this->db->query($sql);//print_r($query);
		//echo $sql;//exit;
		return $query->rows;
	}
	public function getclose_status($data) {
		$sql = "SELECT * FROM `oc_transaction` WHERE 1=1";
		if (!empty($data['filter_date_start'])) {
			$sql .= " AND DATE(`date`) = '" . $this->db->escape($data['filter_date_start']) . "'";
		}
		if (!empty($data['unit'])) {
			$sql .= " AND LOWER(`unit`) = '" . $this->db->escape(strtolower($data['unit'])) . "'";
		}
		if (!empty($data['department'])) {
			$sql .= " AND LOWER(`department`) = '" . $this->db->escape(strtolower($data['department'])) . "'";
		}
		if (!empty($data['group'])) {
			$sql .= " AND LOWER(`group`) = '" . $this->db->escape(strtolower($data['group'])) . "'";
		}
		$sql .= " AND `day_close_status` = 1";
		$sql .= " ORDER BY `act_intime` DESC";	
		//echo $sql;exit;	
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getTodaysAttendance($data) {
		$sql = "SELECT * FROM `oc_transaction` WHERE 1=1";
		$sql .= " AND DATE(`date`) = '".date('Y-m-d')."' AND `act_intime` <> '00:00:00' ";
		
		if (!empty($data['unit'])) {
			$sql .= " AND LOWER(`unit`) = '" . $this->db->escape(strtolower($data['unit'])) . "'";
		}
		if (!empty($data['department'])) {
			$sql .= " AND LOWER(`department`) = '" . $this->db->escape(strtolower($data['department'])) . "'";
		}
		if (!empty($data['group'])) {
			$sql .= " AND LOWER(`group`) = '" . $this->db->escape(strtolower($data['group'])) . "'";
		}

		$sql .= ' ORDER BY `act_intime` DESC';
		//echo $sql;exit;		
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getTodaysAttendance_new($data) {
		$sql = "SELECT * FROM `oc_attendance_new` WHERE 1=1";
		$sql .= " AND DATE(`punch_date`) = '".date('Y-m-d')."' AND `punch_time` <> '00:00:00' ";
		//$sql .= " AND DATE(`punch_date`) = '2016-12-28' AND `punch_time` <> '00:00:00' ";
		
		if (!empty($data['unit'])) {
			$sql .= " AND LOWER(`unit`) = '" . $this->db->escape(strtolower($data['unit'])) . "'";
		}
		if (!empty($data['department'])) {
			$sql .= " AND LOWER(`dept`) = '" . $this->db->escape(strtolower($data['department'])) . "'";
		}
		if (!empty($data['group'])) {
			$sql .= " AND LOWER(`group`) = '" . $this->db->escape(strtolower($data['group'])) . "'";
		}

		$sql .= ' ORDER BY `punch_time` DESC';
		//echo $sql;exit;		
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getdepartment_list() {
		$sql = "SELECT `department`,`department_id` FROM `oc_employee` WHERE `department` <> '' GROUP BY `department` ";
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getgroup_list() {
		$sql = "SELECT `group` FROM `oc_employee` GROUP BY `group` ";
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getNextDate($unit) {
		$sql = "SELECT `date` FROM " . DB_PREFIX . "transaction WHERE `day_close_status` = '1' ";
		if($unit){
			$sql .= " AND `unit` = '".$unit."' ";
		}
		$sql .= " GROUP BY `date` ORDER BY `date` DESC LIMIT 0,1";
		$query = $this->db->query($sql);
		if(isset($query->rows[0]['date'])) {
			return date('Y-m-d', strtotime($query->rows[0]['date'] .' +1 day'));	
		} else {
			return date('Y-m-d', strtotime('2016-10-01'));
		}
		
	}

	public function getNextDate_1() {
		$query = $this->db->query("SELECT `date` FROM " . DB_PREFIX . "transaction WHERE `month_close_status` = '1' GROUP BY `date` ORDER BY `date` DESC LIMIT 0,1");
		if(isset($query->rows[0]['date'])) {
			return date('Y-m-d', strtotime($query->rows[0]['date'] .' +1 day'));	
		} else {
			return date('Y-m-d', strtotime('2016-10-01'));
		}
		
	}
}
?>