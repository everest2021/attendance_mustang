<?php
class ControllerReportDailyAttendance extends Controller { 
	public function index() {  
		$this->language->load('report/dailyattendance');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			//$filter_date_start = date('Y-m-d');
			$from = date('Y-m-d');
			$filter_date_start = date('Y-m-d', strtotime($from . "-1 day"));
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			//$filter_date_start = date('Y-m-d');
			$from = date('Y-m-d');
			$filter_date_end = date('Y-m-d', strtotime($from . "+29 day"));
		}

		if (isset($this->request->get['unit'])) {
			$unit = $this->request->get['unit'];
			} else {
			if($this->user->getId() == 1) {
				$unit = '';
			} else if($this->user->getId() == 7) {
				$unit = 'Moving';
			} else if($this->user->getId() == 3) {
				$unit = 'Mumbai';
			} else if($this->user->getId() == 4) {
				$unit = 'Pune';
			} else {
				$unit = 'Mumbai';
			}
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = html_entity_decode($this->request->get['filter_name']);
		} else {
			$filter_name = "";
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = html_entity_decode($this->request->get['filter_name_id']);
		} else {
			$filter_name_id = "";
		}

		if (isset($this->request->get['department'])) {
			$department = html_entity_decode($this->request->get['department']);
		} else {
			$department ="";
		}

		if (isset($this->request->get['group'])) {
			$group = $this->request->get['group'];
		} else {
			$group = "";
		}

		if (isset($this->request->get['status'])) {
			$status = $this->request->get['status'];
		} else {
			$status = "";
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}
		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}
		if (isset($this->request->get['status'])) {
			$url .= '&status=' . $this->request->get['status'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('report/dailyattendance', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->load->model('report/dailyattendance');

		$data['attendace'] = array();

		$data = array(
			'filter_date_start'	     => $filter_date_start,
			'filter_date_end'	     => $filter_date_end,
			'filter_name'	    	 => $filter_name,
			//'filter_name_id'	     => $filter_name_id,
			'unit'					 => $unit,
			'department'			 => $department,
			'group'					 => $group,
			'status'			     => $status, 
			'start'                  => ($page - 1) * 7000,
			'limit'                  => 7000
		);
		//echo "<pre>"; print_r($data);

		if($url == ''){
			$this->data['generate'] = $this->url->link('transaction/transaction/generate_filter_date', 'token=' . $this->session->data['token'] . '&filter_date_start=' . $filter_date_start, 'SSL');
		} else {
			$this->data['generate'] = $this->url->link('transaction/transaction/generate_filter_date', 'token=' . $this->session->data['token'] . $url, 'SSL');
		}

		$this->data['export'] = $this->url->link('report/dailyattendance/export', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$start_time = strtotime($filter_date_start);
		$compare_time = strtotime(date('Y-m-d'));
		
		$results = $this->model_report_dailyattendance->getAttendance1($data);
		//echo "<pre>"; print_r($results);//exit;
		$this->data['workinghours'] = array();
		$total_hours='0';
		//echo'<pre>';
     //print_r($results);
     //exit();
		foreach ($results as $hour) {
			$this->data['workinghours'] = array(
				'total_hours'    => $hour['total_time'],
				);
					if($hour['total_time']){
			     		$time = array($hour['total_time']);
			 		}else{
			 			$time = array("00:00:00");
			 		}
				
     $hours=0;
     $min=0;
     $sec=0;


    
       foreach($time as $time_array)
    	{
       $time_exp=explode(':',$time_array); 
       $hours=$hours+$time_exp[0];
       $min=$min+$time_exp[1];
       $sec=$sec+$time_exp[2];
		

	    }
	     $time_output='';
	     $time_output=$hours.':'.$min.':'.$sec;
	   // echo '<br>',$time_output; //exit;


		}

		$this->data['hours']=$hours;
		$this->data['min'] =$min;
		$this->data['sec'] =$sec;
		//echo "<pre>"; print_r($this->data['workinghours']);exit;
	
		$results = $this->model_report_dailyattendance->getAttendance($data);

		$this->data['dailyreports'] = array();
		$counttime = '0'; $countoff = '0'; $countabsent = '0';$countpresent = '0'; $countleave = '0';
		$countholiday = '0';$total_time='0';$new_time ='0';
		foreach ($results as $result) {

			 if($result['leave_status'] == 1) { 
	            $Shift_type='NS';
	         } elseif($result['weekly_off'] != '0') { 
	            $Shift_type='NS';
	         } elseif($result['holiday_id'] != '0') {
	            $Shift_type='NS';
	         } elseif($result['present_status'] !='0') { 
	          	$Shift_type='GS';
	         } elseif($result['absent_status'] !='0') {
	           $Shift_type='NS';
	         }

			if ($result['weekly_off'] == '1') {
				$countoff = $countoff + 1;
			} elseif ($result['holiday_id'] == '1') {
				$countholiday = $countholiday + 1;
			} elseif ($result['leave_status'] == '1') {
				$countleave = $countleave + 1;
			} elseif ($result['leave_status'] == '0.5') {
				$countleave = $countleave + 0.5;
				if ($result['absent_status'] == '0.5') {
					$countabsent = $countabsent + 0.5;
				}else{
					$countpresent = $countpresent + 0.5;
				}
			} elseif ($result['present_status'] == '1' || $result['present_status'] == '0.5') {
				$countpresent = $countpresent + 1 ;
			} elseif ($result['absent_status'] == '1') {
				$countabsent = $countabsent + 1;
			}

			if($result['leave_status'] == 1) { 
				$status = 'leave';
			} elseif($result['weekly_off'] != '0') { 
				$status = 'Weekly Off';
            } elseif($result['holiday_id'] != '0') {
            	$status = 'Holiday';
            } elseif($result['halfday_status'] != '0') {
            	$status = 'Half day';
            } elseif($result['present_status'] !='0') {
            	$status = 'Present';
            } elseif($result['absent_status'] !='0') {
            	$status = '<b>Absent</b>';
            }

            	$this->data['dailyreports'][] = array(
				'emp_name'    => $result['emp_name'],
				'emp_id'   => $result['emp_id'],
				'department'   => $result['department'],
				'date'		 => $result['date'],
				'act_intime' => $result['act_intime'],
				'act_outtime' => $result['act_outtime'],
				'leave_status' => $result['leave_status'],
				'firsthalf_status' => $result['firsthalf_status'],
				'weekly_off' => $result['weekly_off'],
				'holiday_id' => $result['holiday_id'],
				'halfday_status' => $result['halfday_status'],
				'working_time' => $result['working_time'],
				'present_status' => $result['present_status'],
				'absent_status' => $result['absent_status'],
				'shift_name'	=> $Shift_type,
				'status_name'	=> $status,
			);

		}
		// echo "<pre>"; print_r($this->data['dailyreports']);
		//exit;
		$this->data['countpresent'] =$countpresent;
		$this->data['countleave'] =$countleave;
		$this->data['countholiday'] =$countholiday;
		$this->data['countabsent'] =$countabsent;
		$this->data['countoff'] =$countoff;
		//$this->data['emp_id'] =$emp_id;
		$this->data['department'] =$department;
		//$this->data['emp_name'] =$emp_name;

		
		$department_datas = $this->model_report_dailyattendance->getdepartment_list();
		$department_data = array();
		$department_data['0'] = 'All';
		foreach ($department_datas as $dkey => $dvalue) {
			$department_data[$dvalue['department']] = $dvalue['department'];
		}
		// echo '<pre>';
		// print_r($department_data);
		// exit;
		// $department_data = array(
		// 	'0' => 'All',
		// 	'DISPENSARY' => 'DISPENSARY',
		// 	'A.D.M.' => 'A.D.M.',
		// 	'ELECT.& MECH.' => 'ELECT.& MECH.'
		// );
		$this->data['department_data'] = $department_data;

		
		$group_datas = $this->model_report_dailyattendance->getgroup_list();
		$group_data = array();
		$group_data['0'] = 'All';
		foreach ($group_datas as $gkey => $gvalue) {
			$group_data[$gvalue['group']] = $gvalue['group'];
		}
		// echo '<pre>';
		// print_r($group_data);
		// exit;
		// $group_data = array(
		// 	'0' => 'All',
		// 	'OFFICIALS' => 'OFFICIALS',
		// 	'STAFF' => 'STAFF',
		// 	'WORKMEN' => 'WORKMEN'
		// 	);
		$this->data['group_data'] = $group_data;


		$statuses = array(
			'0' => 'All',
			'1' => 'Present',
			'2' => 'Absent'
		);
		$this->data['statuses'] = $statuses;
		
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_all_status'] = $this->language->get('text_all_status');

		
		$this->data['entry_date_start'] = $this->language->get('entry_date_start');
		$this->data['entry_date_end'] = $this->language->get('entry_date_end');
		
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['button_export'] = $this->language->get('button_export');

		$this->data['token'] = $this->session->data['token'];

		if(isset($this->data['warning'])){
			$this->data['error_warning'] = $this->data['warning'];
		} elseif(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}
		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}
		if (isset($this->request->get['status'])) {
			$url .= '&status=' . $this->request->get['status'];
		}

		$this->data['filter_date_start'] = $filter_date_start;
		$this->data['filter_date_end'] = $filter_date_end;
		$this->data['filter_name'] = $filter_name;
		$this->data['unit'] = $unit;
		$this->data['department'] = $department;
		$this->data['group'] = $group;
		$this->data['status'] = $status;

		$this->template = 'report/dailyattendance.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	public function export(){
		$this->language->load('report/dailyattendance');
		$this->load->model('report/common_report');
		$this->load->model('report/dailyattendance');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			//$filter_date_start = date('Y-m-d');
			$from = date('Y-m-d');
			$filter_date_start = date('Y-m-d', strtotime($from . "-1 day"));
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			//$filter_date_start = date('Y-m-d');
			$from = date('Y-m-d');
			$filter_date_end = date('Y-m-d', strtotime($from . "+29 day"));
		}

		if (isset($this->request->get['unit'])) {
			$unit = $this->request->get['unit'];
			} else {
			if($this->user->getId() == 1) {
				$unit = '';
			} else if($this->user->getId() == 7) {
				$unit = 'Moving';
			} else if($this->user->getId() == 3) {
				$unit = 'Mumbai';
			} else if($this->user->getId() == 4) {
				$unit = 'Pune';
			} else {
				$unit = 'Mumbai';
			}
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = html_entity_decode($this->request->get['filter_name']);
		} else {
			$filter_name = "";
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = html_entity_decode($this->request->get['filter_name_id']);
		} else {
			$filter_name_id = "";
		}

		if (isset($this->request->get['department'])) {
			$department = html_entity_decode($this->request->get['department']);
		} else {
			$department ="";
		}

		if (isset($this->request->get['group'])) {
			$group = $this->request->get['group'];
		} else {
			$group = "";
		}

		if (isset($this->request->get['status'])) {
			$status = $this->request->get['status'];
		} else {
			$status = "";
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}
		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}
		if (isset($this->request->get['status'])) {
			$url .= '&status=' . $this->request->get['status'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('report/dailyattendance', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->load->model('report/dailyattendance');

		$data['attendace'] = array();

		$data = array(
			'filter_date_start'	     => $filter_date_start,
			'filter_date_end'	     => $filter_date_end,
			'filter_name'	    	 => $filter_name,
			'filter_name_id'	     => $filter_name_id,
			'unit'					 => $unit,
			'department'			 => $department,
			'group'					 => $group,
			'status'			     => $status, 
			'start'                  => ($page - 1) * 7000,
			'limit'                  => 7000
		);
		//echo "<pre>"; print_r($data);

		$start_time = strtotime($filter_date_start);
		$compare_time = strtotime(date('Y-m-d'));
		
		$results = $this->model_report_dailyattendance->getAttendance1($data);
		//echo "<pre>"; print_r($results);//exit;
		$this->data['workinghours'] = array();
		$total_hours='0';
		foreach ($results as $hour) {
			$this->data['workinghours'] = array(
				'total_hours'    => $hour['Time( SUM(Time( `working_time` ) ) )'],
				);

			     $time = array($hour['Time( SUM(Time( `working_time` ) ) )']);
		     $hours=0;
		     $min=0;
		     $sec=0;
		    
		       foreach($time as $time_array)
		    	{
		       $time_exp=explode(':',$time_array); 
		       $hours=$hours+$time_exp[0];
		       $min=$min+$time_exp[1];
		       $sec=$sec+$time_exp[2];
				
			    }
			     $time_output='';
			     $time_output=$hours.':'.$min.':'.$sec;
		}

		$this->data['hours']=$hours;
		$this->data['min'] =$min;
		$this->data['sec'] =$sec;
	
		$results = $this->model_report_dailyattendance->getAttendance($data);

		$dailyreports= array();
		$counttime = '0'; $countoff = '0'; $countabsent = '0';$countpresent = '0'; $countleave = '0';
		$countholiday = '0';$total_time='0';$new_time ='0';
		$emp_id="";
		$department="";
		$emp_name="";
		foreach ($results as $result) {

			 if($result['leave_status'] == 1) { 
	            $Shift_type='NS';
	         } elseif($result['weekly_off'] != '0') { 
	            $Shift_type='NS';
	         } elseif($result['holiday_id'] != '0') {
	            $Shift_type='NS';
	         } elseif($result['present_status'] !='0') { 
	          	$Shift_type='GS';
	         } elseif($result['absent_status'] !='0') {
	           $Shift_type='NS';
	         }

			if ($result['weekly_off'] == '1') {
				$countoff = $countoff + 1;
			} elseif ($result['holiday_id'] == '1') {
				$countholiday = $countholiday + 1;
			} elseif ($result['leave_status'] == '1') {
				$countleave = $countleave + 1;
			} elseif ($result['leave_status'] == '0.5') {
				$countleave = $countleave + 0.5;
				if ($result['absent_status'] == '0.5') {
					$countabsent = $countabsent + 0.5;
				}else{
					$countpresent = $countpresent + 0.5;
				}
			} elseif ($result['present_status'] == '1' || $result['present_status'] == '0.5') {
				$countpresent = $countpresent + 1 ;
			} elseif ($result['absent_status'] == '1') {
				$countabsent = $countabsent + 1;
			}

			if($result['leave_status'] == 1) { 
				$status = 'leave';
			} elseif($result['weekly_off'] != '0') { 
				$status = 'Weekly Off';
            } elseif($result['holiday_id'] != '0') {
            	$status = 'Holiday';
            } elseif($result['halfday_status'] != '0') {
            	$status = 'Half day';
            } elseif($result['present_status'] !='0') {
            	$status = 'Present';
            } elseif($result['absent_status'] !='0') {
            	$status = '<b>Absent</b>';
            }

            	$dailyreports[] = array(
				'emp_name'    => $result['emp_name'],
				'emp_id'   => $result['emp_id'],
				'department'   => $result['department'],
				'date'		 => $result['date'],
				'act_intime' => $result['act_intime'],
				'act_outtime' => $result['act_outtime'],
				'leave_status' => $result['leave_status'],
				'firsthalf_status' => $result['firsthalf_status'],
				'weekly_off' => $result['weekly_off'],
				'holiday_id' => $result['holiday_id'],
				'halfday_status' => $result['halfday_status'],
				'working_time' => $result['working_time'],
				'present_status' => $result['present_status'],
				'absent_status' => $result['absent_status'],
				'shift_name'	=> $Shift_type,
				'status_name'	=> $status,
			);

		}
		// echo "<pre>"; print_r($this->data['dailyreports']);
		//exit;
		$this->data['countpresent'] =$countpresent;
		$this->data['countleave'] =$countleave;
		$this->data['countholiday'] =$countholiday;
		$this->data['countabsent'] =$countabsent;
		$this->data['countoff'] =$countoff;
		$this->data['emp_id'] =$emp_id;
		$this->data['department'] =$department;
		$this->data['emp_name'] =$emp_name;
		
		$department_datas = $this->model_report_dailyattendance->getdepartment_list();
		$department_data = array();
		$department_data['0'] = 'All';
		foreach ($department_datas as $dkey => $dvalue) {
			$department_data[$dvalue['department']] = $dvalue['department'];
		}
		// echo '<pre>';
		// print_r($department_data);
		// exit;
		// $department_data = array(
		// 	'0' => 'All',
		// 	'DISPENSARY' => 'DISPENSARY',
		// 	'A.D.M.' => 'A.D.M.',
		// 	'ELECT.& MECH.' => 'ELECT.& MECH.'
		// );
		$this->data['department_data'] = $department_data;

		
		$group_datas = $this->model_report_dailyattendance->getgroup_list();
		$group_data = array();
		$group_data['0'] = 'All';
		foreach ($group_datas as $gkey => $gvalue) {
			$group_data[$gvalue['group']] = $gvalue['group'];
		}
		// echo '<pre>';
		// print_r($group_data);
		// exit;
		// $group_data = array(
		// 	'0' => 'All',
		// 	'OFFICIALS' => 'OFFICIALS',
		// 	'STAFF' => 'STAFF',
		// 	'WORKMEN' => 'WORKMEN'
		// 	);
		$this->data['group_data'] = $group_data;


		$statuses = array(
			'0' => 'All',
			'1' => 'Present',
			'2' => 'Absent'
		);
		$this->data['statuses'] = $statuses;
		
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_all_status'] = $this->language->get('text_all_status');

		
		$this->data['entry_date_start'] = $this->language->get('entry_date_start');
		$this->data['entry_date_end'] = $this->language->get('entry_date_end');
		
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['button_export'] = $this->language->get('button_export');

		$this->data['token'] = $this->session->data['token'];

		if(isset($this->data['warning'])){
			$this->data['error_warning'] = $this->data['warning'];
		} elseif(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}
		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}
		if (isset($this->request->get['status'])) {
			$url .= '&status=' . $this->request->get['status'];
		}

		
		if($results){
			//$month = date("F", mktime(0, 0, 0, $filter_month, 10));
			if($status == 1 || $status == 0){
				$statuss = 'Present Report';
				$statusss = 'Present_Report';
			} else {
				$statuss = 'Absent Report';
				$statusss = 'Absent_Report';
			}
			$template = new Template();		
			$template->data['dailyreports'] = $dailyreports;
			//$template->data['workinghours'] = $workinghours;
			$template->data['filter_date_start'] = $filter_date_start;
			$template->data['filter_date_end'] = $filter_date_end;
			$template->data['countpresent'] =$countpresent;
			$template->data['countleave'] =$countleave;
			$template->data['countholiday'] =$countholiday;
			$template->data['countabsent'] =$countabsent;
			$template->data['countoff'] =$countoff;
			$template->data['hours']=$hours;
			$template->data['min'] =$min;
			$template->data['sec'] =$sec;
			$template->data['emp_id'] =$emp_id;
			$template->data['department'] =$department;
			$template->data['emp_name'] =$emp_name;
	
				$template->data['title'] = $statuss;
			if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
				$template->data['base'] = HTTPS_SERVER;
			} else {
				$template->data['base'] = HTTP_SERVER;
			}
			$html = $template->fetch('report/dailyattendance_html.tpl');
			//echo $html;exit;
			// header('Content-type: text/html');
			// header('Content-Disposition: attachment; filename='.$filename);
			//echo $html;exit;
			$filename = $filter_name."_".$filter_date_start;
			//echo $filename;exit();
			header("Content-Type: application/vnd.ms-excel; charset=utf-8");
			header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false);
			echo $html;
			exit;		
		} else {
			$this->session->data['warning'] = 'No Data Found';
			$this->redirect($this->url->link('report/dailyattendance', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}
	}

	public function remove_transaction(){
		$this->language->load('report/attendance');
		$this->load->model('report/common_report');
		$this->load->model('report/attendance');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			//$filter_date_start = date('Y-m-d');
			$from = date('Y-m-d');
			$filter_date_start = date('Y-m-d', strtotime($from . "-1 day"));
		}

		if (isset($this->request->get['unit'])) {
			$unit = $this->request->get['unit'];
		} else {
			$unit = 0;
		}

		if (isset($this->request->get['department'])) {
			$department = html_entity_decode($this->request->get['department']);
		} else {
			$department = 0;
		}

		if (isset($this->request->get['group'])) {
			$group = $this->request->get['group'];
		} else {
			$group = 0;
		}

		if (isset($this->request->get['status'])) {
			$status = $this->request->get['status'];
		} else {
			$status = 1;
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		if (isset($this->request->get['transaction_id'])) {
			$transaction_id = $this->request->get['transaction_id'];
		} else {
			$transaction_id = 0;
		}

		$sql = "DELETE FROM `oc_transaction` WHERE `transaction_id` = '".$transaction_id."'";
		//echo $sql;exit;
		$this->db->query($sql);

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}
		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}
		if (isset($this->request->get['status'])) {
			$url .= '&status=' . $this->request->get['status'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->session->data['success'] = 'Transaction Removed Succesfully';
		$this->redirect($this->url->link('report/attendance', 'token=' . $this->session->data['token'].$url, 'SSL'));
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/employee');

			if(isset($this->session->data['dept_names'])){
				$filter_departments = html_entity_decode($this->session->data['dept_names']);
				$filter_departments = "'" . str_replace(",", "','", html_entity_decode($filter_departments)) . "'";
			} else {
				$filter_departments = '';
			}

			if($filter_departments == ''){
				if(isset($this->session->data['dept_name'])){
					$filter_department = $this->session->data['dept_name'];
				} else {
					$filter_department = '';
				}
			} else {
				$filter_department = '';
			}

			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				'filter_department' => $filter_department,
				'filter_departments' => $filter_departments,
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_employee->getemployees($data);

			foreach ($results as $result) {
				$json[] = array(
					'employee_id' => $result['employee_id'],
					'emp_code' => $result['emp_code'], 
					'name'            => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}
}
?>
