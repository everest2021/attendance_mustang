<?php
class ModelReportCommonReport extends Model {
	public function gettransaction_data($employee_id, $data) {
		$sql = "SELECT * FROM `oc_transaction` WHERE 1=1";
		
		if (isset($data['filter_month']) && !empty($data['filter_month'])) {
			$sql .= " AND `month` = '" . $this->db->escape($data['filter_month']) . "'";
		}

		if (isset($data['filter_year']) && !empty($data['filter_year'])) {
			$sql .= " AND `year` = '" . $this->db->escape($data['filter_year']) . "'";
		}

		if (isset($data['filter_date_start']) && !empty($data['filter_date_start'])) {
			$sql .= " AND DATE(`date`) >= '" . $this->db->escape($data['filter_date_start']) . "'";
		}

		if (isset($data['filter_date_end']) && !empty($data['filter_date_end'])) {
			$sql .= " AND DATE(`date`) <= '" . $this->db->escape($data['filter_date_end']) . "'";
		}

		if (isset($data['filter_date']) && !empty($data['filter_date'])) {
			$sql .= " AND DATE(`date`) = '" . $this->db->escape($data['filter_date']) . "'";
		}

		if (isset($data['filter_unit']) && !empty($data['filter_unit'])) {
			$sql .= " AND LOWER(`unit_id`) = '" . $this->db->escape($data['filter_unit']) . "'";
		}

		if (isset($data['filter_department']) && !empty($data['filter_department'])) {
			$sql .= " AND LOWER(`department_id`) = '" . $this->db->escape($data['filter_department']) . "'";
		}
		if (isset($data['filter_designation']) && !empty($data['filter_designation'])) {
			$sql .= " AND LOWER(`designation_id`) = '" . $this->db->escape($data['filter_designation']) . "'";
		}


		if (isset($data['group']) && !empty($data['group'])) {
			if($data['group'] == '1'){
				$sql .= " AND LOWER(`group`) <> 'officials'";
			} else {
				$sql .= " AND LOWER(`group`) = '" . $this->db->escape(strtolower($data['group'])) . "'";
			}
		}

		if (isset($data['month_close']) && !empty($data['month_close'])) {
			$sql .= " AND `month_close_status` = '0' ";
		}

		if (isset($data['day_close']) && !empty($data['day_close'])) {
			//$sql .= " AND `day_close_status` = '1' ";
		}

		$sql .= " AND emp_id = '".$employee_id."' ";

		if(isset($data['filter_limit'])){
			$sql .= " LIMIT 0, 10";
		}
		//echo $sql;exit;
		//$sql .= ' ORDER BY `act_intime` DESC';		
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getemployees($data = array()) {
		$sql = "SELECT * FROM `oc_employee` WHERE 1=1  ";
		
		if (!empty($data['filter_name_id'])) {
			$sql .= " AND `employee_id` = '" . $this->db->escape(strtolower($data['filter_name_id'])) . "'";
		}

		if (!empty($data['filter_name'])) {
			$sql .= " AND `name` = '" . $this->db->escape(strtolower($data['filter_name'])) . "'";
		}


		if (!empty($data['unit'])) {
			$sql .= " AND LOWER(`unit`) = '" . $this->db->escape(strtolower($data['unit'])) . "'";
		}

		if (!empty($data['department'])) {
			$sql .= " AND LOWER(`department`) = '" . $this->db->escape(strtolower($data['department'])) . "'";
		}
			if (!empty($data['designation'])) {
			$sql .= " AND LOWER(`designation`) = '" . $this->db->escape(strtolower($data['designation'])) . "'";
		}

		if (isset($data['filter_departments']) && !empty($data['filter_departments'])) {
			$sql .= " AND LOWER(`department`) IN (" . strtolower($data['filter_departments']) . ") ";
		}

		if (!empty($data['group'])) {
			if($data['group'] == '1'){
				$sql .= " AND LOWER(`group`) <> 'officials' ";
			} else {
				$sql .= " AND LOWER(`group`) = '" . $this->db->escape(strtolower($data['group'])) . "'";
			}
		}

		//$sql .= " AND `status` = '1' AND `emp_code` = '22848' ";
		$sql .= " AND `status` = '1' ";
		//echo $sql;exit;
		$sql .= " ORDER BY CAST(`emp_code` AS INT)";		
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getemployees_reporting($data = array()) {
		$sql = "SELECT * FROM  `oc_employee` WHERE 1=1 ";
		
		if (!empty($data['filter_name_id'])) {
			$sql .= " AND `reporting_to` = '" . $this->db->escape(strtolower($data['filter_name_id'])) . "'";
		}

		if (!empty($data['unit'])) {
			$sql .= " AND LOWER(`unit`) = '" . $this->db->escape(strtolower($data['unit'])) . "'";
		}

		if (!empty($data['department'])) {
			$sql .= " AND LOWER(`department`) = '" . $this->db->escape(strtolower($data['department'])) . "'";
		}
		if (!empty($data['designation'])) {
			$sql .= " AND LOWER(`designation`) = '" . $this->db->escape(strtolower($data['designation'])) . "'";
		}


		if (isset($data['filter_departments']) && !empty($data['filter_departments'])) {
			$sql .= " AND LOWER(`department`) IN (" . strtolower($data['filter_departments']) . ") ";
		}

		if (!empty($data['group'])) {
			if($data['group'] == '1'){
				$sql .= " AND LOWER(`group`) <> 'officials' ";
			} else {
				$sql .= " AND LOWER(`group`) = '" . $this->db->escape(strtolower($data['group'])) . "'";
			}
		}

		//$sql .= " AND `status` = '1' AND `emp_code` = '22848' ";
		$sql .= " AND `status` = '1' ";
		//echo $sql;exit;
		//$sql .= " ORDER BY `shift_type` ";		
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getemployees_muster($data = array()) {
		$sql = "SELECT `emp_code`, `name`, `department`, `designation`, `unit`, `grade`, `status` FROM " . DB_PREFIX . "employee WHERE 1=1 ";
		
		if (!empty($data['filter_name_id'])) {
			$sql .= " AND `emp_code` = '" . $this->db->escape(strtolower($data['filter_name_id'])) . "'";
		}

		if (!empty($data['unit'])) {
			$sql .= " AND LOWER(`unit`) = '" . $this->db->escape(strtolower($data['unit'])) . "'";
		}

		if (!empty($data['department'])) {
			$sql .= " AND LOWER(`department`) = '" . $this->db->escape(strtolower($data['department'])) . "'";
		}

		if (!empty($data['group'])) {
			if($data['group'] == '1'){
				$sql .= " AND LOWER(`group`) <> 'officials' ";
			} else {
				$sql .= " AND LOWER(`group`) = '" . $this->db->escape(strtolower($data['group'])) . "'";
			}
		}

		//$sql .= " AND `status` = '1' AND `emp_code` = '11923' ";
		//$sql .= " AND `status` = '1'";

		$sql .= " ORDER BY `department`, `grade`, `emp_code` ";		
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getshiftdata($shift_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "shift WHERE `shift_id` = '".$shift_id."' ");
		if($query->num_rows > 0){
			return $query->row;
		} else {
			return array();
		}
	}

	public function getattendance_exist($date) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "attendance WHERE `punch_date` = '".$date."' ");
		if($query->num_rows > 0){
			return 1;
		} else {
			return 0;
		}
	}

	public function gettransaction_leave_data($emp_code) {
		$sql = "SELECT * FROM `oc_leave_transaction` WHERE 1=1";
		$sql .= " AND emp_id = '".$emp_code."' AND `a_status` = '1' ORDER BY id DESC";

		if(isset($data['filter_limit'])){
			$sql .= " LIMIT 0, 10";
		}
		//echo $sql;exit;
		//$sql .= ' ORDER BY `act_intime` DESC';		
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getNextDate() {
		$query = $this->db->query("SELECT `date` FROM " . DB_PREFIX . "transaction  GROUP BY `date` ORDER BY `date` DESC LIMIT 0,1");
		if(isset($query->rows[0]['date'])) {
			return date('Y-m-d', strtotime($query->rows[0]['date']));	
		} else {
			return date('Y-m-d', strtotime('2016-10-01'));
		}
		
	}

	public function getfilter_date_end($filter_date_start, $filter_name_id, $unit) {
		$sql = "SELECT `date` FROM " . DB_PREFIX . "transaction WHERE DATE(`date`) >= '" . $this->db->escape($filter_date_start) . "' AND `day_close_status` = '1' ";
		$in = 0;
		if($filter_name_id){
			$in = 1;
			$units = "SELECT `unit` FROM " . DB_PREFIX . "employee WHERE `employee_id` = '" . $this->db->escape($filter_name_id) . "' ";
			$unit = $this->db->query($units)->row['unit'];
			$sql .= " AND LOWER(`unit`) = '" . $this->db->escape(strtolower($unit)) . "'";
		}
		if($in == 0){
			if($unit) {
				$sql .= " AND LOWER(`unit`) = '" . $this->db->escape(strtolower($unit)) . "'";		
			}
		}
		$sql .= " GROUP BY `date` ORDER BY `date` DESC LIMIT 0,1";
		//echo $sql;exit;
		$query = $this->db->query($sql);
		if(isset($query->rows[0]['date'])) {
			return date('Y-m-d', strtotime($query->rows[0]['date']));	
		} else {
			return date('Y-m-d', strtotime('2018-8-30'));
		}
	}

	public function getleave_transaction_data_group($emp_code, $data) {
		$sql = "SELECT * FROM `oc_leave_transaction` WHERE 1=1";
		if (!empty($data['filter_date_start'])) {
			$sql .= " AND DATE(`date`) >= '" . $this->db->escape($data['filter_date_start']) . "'";
		}
		if (!empty($data['filter_date_end'])) {
			$sql .= " AND DATE(`date`) <= '" . $this->db->escape($data['filter_date_end']) . "'";
		}
		if (!empty($data['unit'])) {
			$sql .= " AND LOWER(`unit`) = '" . $this->db->escape(strtolower($data['unit'])) . "'";
		}
		if (isset($data['filter_leave']) && !empty($data['filter_leave'])) {
			$sql .= " AND LOWER(`leave_type`) = '" . $this->db->escape(strtolower($data['filter_leave'])) . "'";
		}
		$sql .= " AND emp_id = '".$emp_code."' AND `p_status` = '1' AND `a_status` = '1' AND (`leave_type` = 'PL' OR `leave_type` = 'ML' OR `leave_type` = 'SL' OR `leave_type` = 'BL' OR `leave_type` = 'MAL' OR `leave_type` = 'PAL' OR `leave_type` = 'LWP') GROUP BY `batch_id` ";
		//$sql .= " AND emp_id = '".$emp_code."' AND `a_status` = '1' GROUP BY `batch_id` ";
		//echo $sql;exit;
		//$sql .= ' ORDER BY `act_intime` DESC';		
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getencash_data($emp_code, $data) {
		$sql = "SELECT * FROM `oc_leave_transaction` WHERE 1=1";
		if (!empty($data['filter_date_start'])) {
			$sql .= " AND DATE(`dot`) >= '" . $this->db->escape($data['filter_date_start']) . "'";
		}
		if (!empty($data['filter_date_end'])) {
			$sql .= " AND DATE(`dot`) <= '" . $this->db->escape($data['filter_date_end']) . "'";
		}
		if (!empty($data['unit'])) {
			$sql .= " AND LOWER(`unit`) = '" . $this->db->escape(strtolower($data['unit'])) . "'";
		}
		$sql .= " AND emp_id = '".$emp_code."' AND `p_status` = '1' AND `a_status` = '1' AND `encash` <> '' GROUP BY `batch_id` ";
		//$sql .= " AND emp_id = '".$emp_code."' AND `a_status` = '1' AND `encash` <> '' GROUP BY `batch_id` ";
		//echo $sql;exit;
		//$sql .= ' ORDER BY `act_intime` DESC';		
		$query = $this->db->query($sql);
		$encash_days = 0;
		foreach ($query->rows as $key => $value) {
			$encash_days = $encash_days + $value['encash'];
		}
		return $encash_days;
	}

	public function getencash_data_un($emp_code, $data) {
		$sql = "SELECT * FROM `oc_leave_transaction` WHERE 1=1";
		$sql .= " AND emp_id = '".$emp_code."' AND `p_status` = '0' AND `a_status` = '1' AND `encash` <> '' ";
		$sql .= " AND `dot` >= '".$data['filter_date_start']."' AND `dot` <= '".$data['filter_date_end']."' GROUP BY `batch_id` ";

		//$sql .= " AND emp_id = '".$emp_code."' AND `a_status` = '1' AND `encash` <> '' GROUP BY `batch_id` ";
		//echo $sql;exit;
		//$sql .= ' ORDER BY `act_intime` DESC';		
		$query = $this->db->query($sql);
		$encash_days = 0;
		foreach ($query->rows as $key => $value) {
			$sql1 = "SELECT * FROM `oc_leave_transaction` WHERE 1=1";
			$sql1 .= " AND emp_id = '".$emp_code."' AND `batch_id` = '".$value['batch_id']."' ";	
			$query1 = $this->db->query($sql1);
			$p_status = 0;
			foreach($query1->rows as $qkey => $qvalue){
				if($qvalue['p_status'] == 1){
					$p_status = 1;
					break;
				}
			}
			if($p_status == 0){
				$encash_days = $encash_days + $value['encash'];
			}
		}
		return $encash_days;
	}

	public function getleave_transaction_data($batch_id) {
		$sql = "SELECT * FROM `oc_leave_transaction` WHERE 1=1";
		$sql .= " AND batch_id = '".$batch_id."' ";
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getleave_data($emp_code, $from_year) {
		$sql = "SELECT * FROM `oc_leave` WHERE 1=1";
		//$sql .= " AND emp_id = '".$emp_code."' AND `close_status` = '0' ";
		$sql .= " AND emp_id = '".$emp_code."' AND `year` = '".$from_year."' ";
		//echo $sql;exit;
		//$sql .= ' ORDER BY `act_intime` DESC';		
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function getsum_data($emp_id, $month, $year, $key) {
		$sql = "SELECT COUNT(*) as total FROM `oc_transaction` WHERE `month` = '".$month."' AND `year` = '".$year."' AND `emp_id` = '".$emp_id."' AND (`firsthalf_status` = '".$key."' OR `secondhalf_status` = '".$key."') ";
		//echo $sql;exit;
		$query = $this->db->query($sql);
		return $query->row['total'];
	}

	public function getleav_data($emp_id, $month, $year, $key) {
		$sql = "SELECT * FROM `oc_transaction` WHERE `month` = '".$month."' AND `year` = '".$year."' AND `emp_id` = '".$emp_id."' AND (`firsthalf_status` = '".$key."' OR `secondhalf_status` = '".$key."') ";
		//echo $sql;exit;
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getleave_transaction_data_group_ess($emp_code, $data) {
		$sql = "SELECT * FROM `oc_leave_transaction_temp` WHERE 1=1";
		if (!empty($data['filter_date_start'])) {
			$sql .= " AND DATE(`date`) >= '" . $this->db->escape($data['filter_date_start']) . "'";
		}
		if (!empty($data['filter_date_end'])) {
			$sql .= " AND DATE(`date`) <= '" . $this->db->escape($data['filter_date_end']) . "'";
		}
		if (!empty($data['unit'])) {
			$sql .= " AND LOWER(`unit`) = '" . $this->db->escape(strtolower($data['unit'])) . "'";
		}
		if (isset($data['filter_leave']) && !empty($data['filter_leave'])) {
			$sql .= " AND LOWER(`leave_type`) = '" . $this->db->escape(strtolower($data['filter_leave'])) . "'";
		}
		$sql .= " AND emp_id = '".$emp_code."' AND `p_status` = '1' AND `a_status` = '1' GROUP BY `batch_id` ";
		//$sql .= " AND emp_id = '".$emp_code."' AND `p_status` = '1' AND `a_status` = '1' AND (`leave_type` = 'PL' OR `leave_type` = 'ML' OR `leave_type` = 'SL' OR `leave_type` = 'BL' OR `leave_type` = 'MAL' OR `leave_type` = 'PAL' OR `leave_type` = 'LWP') GROUP BY `batch_id` ";
		//$sql .= " AND emp_id = '".$emp_code."' AND `p_status` = '1' AND `a_status` = '1' GROUP BY `batch_id` ";
		//$sql .= " AND emp_id = '".$emp_code."' AND `a_status` = '1' GROUP BY `batch_id` ";
		//echo $sql;exit;
		//$sql .= ' ORDER BY `act_intime` DESC';		
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getleave_transaction_data_ess($batch_id) {
		$sql = "SELECT * FROM `oc_leave_transaction_temp` WHERE 1=1";
		$sql .= " AND batch_id = '".$batch_id."' ";
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function get_setting($key) {
		$sql = "SELECT `value` FROM `oc_setting_new`WHERE `key` = '".$key."'";
		$setting_datas = $this->db->query($sql);
		if($setting_datas->num_rows > 0){
			$setting_data = $setting_datas->row;
			$value = $setting_data['value'];
		} else {
			$value = '';
		}
		return $value;
	}

	public function get_shift_schedule_data($employee_id, $filter_date_start){
		$day_date = date('j', strtotime($filter_date_start));
		$month = date('n', strtotime($filter_date_start));
		$year = date('Y', strtotime($filter_date_start));
		$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$employee_id."' AND `month` = '".$month."' AND `year` = '".$year."' ";
		$shift_schedules = $this->db->query($update3)->row;
		if($shift_schedules->num_rows > 0){
			$schedule_raw = explode('_', $shift_schedule[$day_date]);
		} else {
			$schedule_raw = explode('_', 'S_1');
		}
		return $schedule_raw;
	}	
}
?>
