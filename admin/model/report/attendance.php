<?php
class ModelReportAttendance extends Model {
	public function getAttendance($data) {
		$sql = "SELECT * FROM `oc_transaction` t LEFT JOIN `oc_employee` e ON (e.`employee_id` = t.`emp_id`) WHERE 1=1";
		
		if (!empty($data['filter_name'])) {
			$sql .= " AND LOWER(e.`name`) = '" . $this->db->escape($data['filter_name']) . "'";
		}

		if (!empty($data['filter_name_id'])) {
			$sql .= " AND e.`employee_id` = '" . $this->db->escape($data['filter_name_id']) . "'";
		}

		if (!empty($data['filter_date_start'])) {
			$sql .= " AND DATE(`date`) = '" . $this->db->escape($data['filter_date_start']) . "'";
		}

		if (!empty($data['filter_unit'])) {
			$sql .= " AND LOWER(e.`unit_id`) = '" . $this->db->escape(strtolower($data['filter_unit'])) . "'";
		}
		if (!empty($data['filter_department'])) {
			$sql .= " AND LOWER(e.`department_id`) = '" . $this->db->escape(strtolower($data['filter_department'])) . "'";
		}

		if (!empty($data['filter_designation'])) {
			$sql .= " AND LOWER(e.`designation_id`) = '" . $this->db->escape(strtolower($data['filter_designation'])) . "'";
		}
		if (!empty($data['group'])) {
			$sql .= " AND LOWER(e.`group`) = '" . $this->db->escape(strtolower($data['group'])) . "'";
		}

		if ($data['status'] == 1) {
			$sql .= " AND (`present_status` = '1' OR `present_status` = '0.5' OR t.`weekly_off` <> '0' OR `holiday_id` <> '0' OR (`halfday_status` <> '0' AND `absent_status` = '0') OR `leave_status` <> '0' OR `compli_status` <> '0')  ";
		} elseif($data['status'] == 2) {
			$sql .= " AND (`absent_status` = '1')";
			//$sql .= " AND (`absent_status` = '1' OR `absent_status` = '0.5')";
		} else {
			//$sql .= " AND (`present_status` = '1' OR `present_status` = '0.5' OR  `weekly_off` <> '0' OR `holiday_id` <> '0' OR `halfday_status` <> '0' OR `leave_status` <> '0')  ";
		}
		//$sql .= " AND `emp_id` = '21463' ";
		$sql .= ' ORDER BY `act_intime` DESC';	
		//echo $sql;exit;	
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getclose_status($data) {
		$sql = "SELECT * FROM `oc_transaction` WHERE 1=1";
		if (!empty($data['filter_date_start'])) {
			$sql .= " AND DATE(`date`) = '" . $this->db->escape($data['filter_date_start']) . "'";
		}
		if (!empty($data['unit'])) {
			$sql .= " AND LOWER(`unit`) = '" . $this->db->escape(strtolower($data['unit'])) . "'";
		}
		if (!empty($data['department'])) {
			$sql .= " AND LOWER(`department`) = '" . $this->db->escape(strtolower($data['department'])) . "'";
		}
		if (!empty($data['group'])) {
			$sql .= " AND LOWER(`group`) = '" . $this->db->escape(strtolower($data['group'])) . "'";
		}
		$sql .= " AND `day_close_status` = 1";
		$sql .= " ORDER BY `act_intime` DESC";	
		//echo $sql;exit;	
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getTodaysAttendance($data) {
		$sql = "SELECT * FROM `oc_transaction` WHERE 1=1 AND DATE(`date`) = '".date('Y-m-d')."' AND `act_intime` <> '00:00:00'";
		if (!empty($data['department'])) {
			$sql .= " AND `department_id` = '" . $this->db->escape($data['department']) . "'";
		}
		if (!empty($data['designation'])) {
			$sql .= " AND `designation_id` = '" . $this->db->escape($data['designation']) . "'";
		}
		if (!empty($data['unit'])) {
			$sql .= " AND `unit_id` = '" . $this->db->escape($data['unit']) . "'";
		}
		if($this->user->getUnitId() != '0'){
			$sql .= " AND `unit_id` = '".$this->user->getUnitId()."' ";
		}
		$sql .= ' ORDER BY `act_intime` DESC';
		//echo $sql;exit;		
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getTodaysAttendance_new($data) {
		$sql = "SELECT * FROM `oc_transaction` WHERE 1=1 AND DATE(`date`) = '".date('Y-m-d')."' AND `act_intime` <> '00:00:00'";
		if (!empty($data['department'])) {
			$sql .= " AND `department_id` = '" . $this->db->escape($data['department']) . "'";
		}
		if (!empty($data['designation'])) {
			$sql .= " AND `designation_id` = '" . $this->db->escape($data['designation']) . "'";
		}
		if (!empty($data['unit'])) {
			$sql .= " AND `unit_id` = '" . $this->db->escape($data['unit']) . "'";
		}
		if($this->user->getUnitId() != '0'){
			$sql .= " AND `unit_id` = '".$this->user->getUnitId()."' ";
		}
		$sql .= ' ORDER BY `act_intime` DESC';
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}		

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		//echo $sql;exit;		
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getdepartment_list() {
		$sql = "SELECT `department`,`department_id` FROM `oc_employee` WHERE `department` <> '' AND `department_id` <> '0'  GROUP BY `department` ";
		$query = $this->db->query($sql);
		//echo "<pre>";print_r($query->rows);exit;
		return $query->rows;
	}

	public function getlocation_list() {
		$sql = "SELECT `unit`,`unit_id` FROM `oc_employee` WHERE `unit_id` <> '' GROUP BY `unit_id` ";
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getdesignation_list() {
		$sql = "SELECT `designation`,`designation_id` FROM `oc_employee` WHERE `designation` <> '' AND `designation_id` <> '0' GROUP BY `designation` ";
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getgroup_list() {
		$sql = "SELECT `group` FROM `oc_employee` GROUP BY `group` ";
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getNextDate($unit) {
		$sql = "SELECT `date` FROM " . DB_PREFIX . "transaction WHERE `day_close_status` = '1' ";
		if($unit){
			$sql .= " AND `unit` = '".$unit."' ";
		}
		$sql .= " GROUP BY `date` ORDER BY `date` DESC LIMIT 0,1";
		$query = $this->db->query($sql);
		if(isset($query->rows[0]['date'])) {
			return date('Y-m-d', strtotime($query->rows[0]['date'] .' +1 day'));	
		} else {
			return date('Y-m-d', strtotime('2016-10-01'));
		}
		
	}

	public function getNextDate_1() {
		$query = $this->db->query("SELECT `date` FROM " . DB_PREFIX . "transaction WHERE `month_close_status` = '1' GROUP BY `date` ORDER BY `date` DESC LIMIT 0,1");
		if(isset($query->rows[0]['date'])) {
			return date('Y-m-d', strtotime($query->rows[0]['date'] .' +1 day'));	
		} else {
			return date('Y-m-d', strtotime('2016-10-01'));
		}
	}

	public function gettransaction_data($data) {
		$sql = "SELECT * FROM `oc_transaction` t LEFT JOIN `oc_employee` e ON(e.`employee_id` = t.`emp_id`) WHERE 1=1";
		if (!empty($data['filter_date_start'])) {
			$sql .= " AND DATE(t.`date`) >= '" . $this->db->escape($data['filter_date_start']) . "'";
		}
		if (!empty($data['filter_date_end'])) {
			$sql .= " AND DATE(t.`date`) <= '" . $this->db->escape($data['filter_date_end']) . "'";
		}
		if (!empty($data['department'])) {
			$sql .= " AND t.`department_id` = '" . $this->db->escape($data['department']) . "'";
		}
		if (!empty($data['designation'])) {
			$sql .= " AND t.`designation_id` = '" . $this->db->escape($data['designation']) . "'";
		}
		if (!empty($data['unit'])) {
			$sql .= " AND t.`unit_id` = '" . $this->db->escape($data['unit']) . "'";
		}
		if (!empty($data['filter_name'])) {
			$sql .= " AND t.`emp_name` = '" . $this->db->escape($data['filter_name']) . "'";
		}
		if (!empty($data['filter_name_id'])) {
			$sql .= " AND t.`emp_id` = '" . $this->db->escape($data['filter_name_id']) . "'";
		}
		if($this->user->getUnitId() != '0'){
			$sql .= " AND t.`unit_id` = '".$this->user->getUnitId()."' ";
		}

		$sql .= " AND e.`status` = '1' ";

		$sql .= ' ORDER BY t.`department_id`, t.`emp_id`, t.`date` ASC ';	
		//echo $sql;exit;	
		$query = $this->db->query($sql);
		return $query->rows;
	}
}
?>