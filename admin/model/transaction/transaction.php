<?php
class ModelTransactionTransaction extends Model {
	
	public function getrawattendance_group() {
		$query = $this->db->query("SELECT emp_id, punch_date, card_id FROM " . DB_PREFIX . "attendance WHERE `status` = '0' GROUP by `emp_id`, `punch_date` ");
		return $query->rows;
	}

	public function getrawattendance_group_date($date) {
		$query = $this->db->query("SELECT `emp_id`, `punch_date`, `card_id`, `manual_status`, `leave`, `holiday`, `weekly_off` FROM `" . DB_PREFIX . "attendance` WHERE `punch_date` = '".$date."' GROUP by `emp_id`, `punch_date` ");
		return $query->rows;
	}

	public function getrawattendance_group_date_custom($emp_code, $date) {
		$query = $this->db->query("SELECT `emp_id`, `punch_date`, `card_id`, `manual_status`, `leave`, `holiday`, `weekly_off`, `present`, absent, `device_id` FROM `" . DB_PREFIX . "attendance` WHERE `punch_date` = '".$date."' AND `emp_id` = '".$emp_code."' GROUP by `punch_date` ");
		//$this->log->write("SELECT `emp_id`, `punch_date`, `card_id`, `manual_status`, `leave`, `holiday`, `weekly_off`, `present`, absent FROM `" . DB_PREFIX . "attendance` WHERE `punch_date` = '".$date."' AND `emp_id` = '".$emp_code."' GROUP by `punch_date` ");
		return $query->row;
	}

	// public function getrawattendance_in_time($emp_id, $punch_date, $shift_intime) {
	// 	// echo $emp_id;
	// 	// echo '<br />';
	// 	//$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "attendance WHERE emp_id = '".$emp_id."' AND `punch_date` = '".$punch_date."' ORDER by STR_TO_DATE(punch_time,'%h:%i:%s %p') ASC LIMIT 1 ");
	// 	$prev_date = date('Y-m-d', strtotime($punch_date .' +1 day'));
	// 	$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "attendance WHERE emp_id = '".$emp_id."' AND (`punch_date` = '".$punch_date."' OR `punch_date` = '".$punch_date."') ORDER by `punch_date` ASC, time(`punch_time`) ASC ");
	// 	//echo "SELECT * FROM " . DB_PREFIX . "attendance WHERE emp_id = '".$emp_id."' AND `punch_date` = '".$punch_date."' ORDER by STR_TO_DATE(punch_time,'%h:%i:%s %p') ASC LIMIT 1 ";
	// 	//exit;
	// 	$array = $query->rows;
	// 	$comp_array = array();
	// 	$hour_array = array();
	// 	$act_intime = array();
		
	// 	// echo '<pre>';
	// 	// print_r($array);
	// 	// exit;
		
	// 	if(count($array) > 1){
	// 		foreach ($array as $akey => $avalue) {
	// 			$s_exp = explode(':', $shift_intime);
	// 			if($s_exp[0] == '00'){
	// 				$punch_date = date('Y-m-d', strtotime($punch_date .' +1 day'));
	// 			}
	// 			$start_date = new DateTime($punch_date.' '.$shift_intime);
	// 			$since_start = $start_date->diff(new DateTime($avalue['punch_date'].' '.$avalue['punch_time']));
				
	// 			// echo '<pre>';
	// 			// print_r($punch_date.' '.$shift_intime);
	// 			// echo '<pre>';
	// 			// print_r($avalue['punch_date'].' '.$avalue['punch_time']);
	// 			// echo '<pre>';
	// 			// print_r($since_start);
				
	// 			if($since_start->d == 0 && $since_start->h < 12 && $since_start->invert == 1){
	// 				$comp_array[] = $since_start->h.':'.$since_start->i.':'.$since_start->s;
	// 				$hour_array[] = $since_start->h;
	// 				$act_array[] = $avalue;
	// 			} elseif($since_start->d == 0 && $since_start->h < 12 && $since_start->invert == 0){
	// 				$comp_array[] = $since_start->h.':'.$since_start->i.':'.$since_start->s;
	// 				$hour_array[] = $since_start->h;
	// 				$act_array[] = $avalue;
	// 			}
	// 		}
	// 	}
		
	// 	// echo '<pre>';
	// 	// print_r($array);
	// 	// echo '<pre>';
	// 	// print_r($comp_array);
	// 	// echo '<pre>';
	// 	// print_r($hour_array);
	// 	// echo '<pre>';
	// 	// print_r($act_array);
	// 	// exit;
	// 	if($hour_array){
	// 		$hour_array = array_unique($hour_array);
	// 		asort($hour_array);
	// 		foreach ($hour_array as $key => $value) {
	// 			$act_intime = $act_array[$key];
	// 			$act_intime['status'] = 1;
	// 			break;
	// 		}
	// 	} else {
	// 		$act_intime['punch_time'] = $array[0]['punch_time'];
	// 		$act_intime['punch_date'] = $array[0]['punch_date'];
	// 		$act_intime['status'] = 0;
	// 	}
	// 	// echo '<pre>';
	// 	// print_r($act_intime);
	// 	// exit;
		
	// 	return $act_intime;
	// }

	public function getrawattendance_in_time($emp_id, $punch_date) {
		$future_date = date('Y-m-d', strtotime($punch_date .' +1 day'));
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "attendance WHERE emp_id = '".$emp_id."' AND (`punch_date` = '".$punch_date."' OR `punch_date` = '".$future_date."') AND `status` = '0' ORDER by date(`punch_date`) ASC, time(`punch_time`) ASC, `id` ASC ");
		$array = $query->row;
		return $array;
	}

	//public function getrawattendance_out_time($emp_id, $punch_date) {
		//$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "attendance WHERE emp_id = '".$emp_id."' AND (`punch_date` = '".$punch_date."') AND `status` = '0' ORDER by `id` DESC ");
		//$array = $query->row;
		//return $array;
	//}

	public function getrawattendance_out_time($emp_id, $punch_date, $act_intime, $act_punch_date) {
		$future_date = date('Y-m-d', strtotime($punch_date .' +1 day'));
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "attendance WHERE emp_id = '".$emp_id."' AND (`punch_date` = '".$punch_date."' OR `punch_date` = '".$future_date."') AND `status` = '0' ORDER by date(`punch_date`) ASC, time(`punch_time`) ASC");
		$act_outtime = array();
		if($query->num_rows > 0){
			$first_punch = $query->rows['0'];
			$array = $query->rows;
			$comp_array = array();
			$hour_array = array();
			
			// echo '<pre>';
			// print_r($array);

			foreach ($array as $akey => $avalue) {
				$start_date = new DateTime($act_punch_date.' '.$act_intime);
				$since_start = $start_date->diff(new DateTime($avalue['punch_date'].' '.$avalue['punch_time']));
				if($since_start->d == 0){
					$comp_array[] = $since_start->h.':'.$since_start->i.':'.$since_start->s;
					$hour_array[] = $since_start->h;
					$act_array[] = $avalue;
				}
			}

			// echo '<pre>';
			// print_r($hour_array);

			// echo '<pre>';
			// print_r($comp_array);

			foreach ($hour_array as $ckey => $cvalue) {
				if($cvalue > 18){
					unset($hour_array[$ckey]);
				}
			}

			// echo '<pre>';
			// print_r($hour_array);
			
			$act_outtimes = max($hour_array);

			// echo '<pre>';
			// print_r($act_outtimes);

			foreach ($hour_array as $akey => $avalue) {
				if($avalue == $act_outtimes){
					$act_outtime = $act_array[$akey];
				}
			}
		}
		// echo '<pre>';
		// print_r($act_outtime);
		// exit;		
		return $act_outtime;
	}

	public function getempdata($emp_id) {
		$query = $this->db->query("SELECT `name`, `shift`, `weekly_off`, `department`, `unit`, `group`, `shift_type`, `employee_id`, `emp_code` FROM " . DB_PREFIX . "employee WHERE `employee_id` = '".$emp_id."' ");
		if($query->num_rows > 0){
			return $query->row;
		} else {
			return array();
		}
	}

	public function getshiftdata($shift_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "shift WHERE `shift_id` = '".$shift_id."' ");
		if($query->num_rows > 0){
			return $query->row;
		} else {
			return array();
		}
	}

	public function getEmployees_dat($employee_id) {
		$sql = "SELECT `employee_id`, `name`, `department`, `department_id`, `unit`, `unit_id`, `designation`, `designation_id`, `shift_type` FROM `" . DB_PREFIX . "employee` WHERE `employee_id` = '".$employee_id."'";
		//echo $sql;exit;
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function getempid_by_empcode($emp_code){
		$sql = "SELECT `employee_id` FROM `" . DB_PREFIX . "employee` WHERE `emp_code` = '".$emp_code."'";
		//echo $sql;exit;
		$query = $this->db->query($sql);
		return $query->row;	
	}

	public function getpltakenyear($emp_code) {
		$sql = "SELECT `id` FROM `" . DB_PREFIX . "leave_transaction` WHERE `emp_id` = '".$emp_code."' AND `leave_type` = 'PL' AND `year` = '".date('Y')."' AND `encash` <> '' AND `a_status` = '1' ";
		//echo $sql;exit;
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function getleave_transaction_data($batch_id) {
		$sql = "SELECT * FROM `" . DB_PREFIX . "leave_transaction` WHERE `batch_id` = '".$batch_id."' ";
		//echo $sql;exit;
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function deleteorderhistory($date){
		$sql = "DELETE FROM `" . DB_PREFIX . "attendance` WHERE `punch_date` = '".$date."'";
		$this->db->query($sql);
	}

	public function deleteorderhistory_new($date){
		$sql = "DELETE FROM `" . DB_PREFIX . "attendance_new` WHERE DATE(`punch_date`) = '".$date."' OR DATE(`punch_date`) < '".$date."' ";
		$this->db->query($sql);
	}

	public function getorderhistory($data){
		$sql = "SELECT * FROM `" . DB_PREFIX . "attendance` WHERE `punch_date` = '".$data['punch_date']."' AND `punch_time` = '".$data['in_time']."' AND `emp_id` = '".$data['employee_id']."' ";
		$query = $this->db->query($sql);
		if($query->num_rows > 0){
			return 1;
		} else {
			return 0;
		}
	}

	public function insert_attendance_data($data) {
		//$this->log->write("INSERT INTO `oc_attendance` SET `emp_id` = '".$data['employee_id']."', `card_id` = '".$data['card_id']."', `punch_date` = '".$data['punch_date']."', `punch_time` = '".$data['in_time']."', `status` = '0' ");
		$sql = $this->db->query("INSERT INTO `oc_attendance` SET `emp_id` = '".$data['employee_id']."', `card_id` = '".$data['card_id']."', `punch_date` = '".$data['punch_date']."', `punch_time` = '".$data['in_time']."', `status` = '0' ");
		//$id = $this->db->getLastId();
		// echo $id;
		// echo '<br />';
		// if ($sql == true) {
		// 	echo 'true';
		// } else {
		// 	echo 'false';
		// }
		// exit;
		//echo 'done';exit;
	}

	public function insert_attendance_data_new($data) {
		//$this->log->write("INSERT INTO `oc_attendance_new` SET `emp_id` = '".$data['employee_id']."', `emp_name` = '".$this->db->escape($data['emp_name'])."', `punch_date` = '".$data['punch_date']."', `punch_time` = '".$data['in_time']."', `shift_intime` = '".$data['shift_intime']."', `status` = '0', `dept` = '".$this->db->escape($data['department'])."', `unit` = '".$this->db->escape($data['unit'])."', `group` = '".$this->db->escape($data['group'])."', `late_time` = '".$data['late_time']."'  ");
		$sql = $this->db->query("INSERT INTO `oc_attendance_new` SET `emp_id` = '".$data['employee_id']."', `emp_name` = '".$this->db->escape($data['emp_name'])."', `punch_date` = '".$data['punch_date']."', `punch_time` = '".$data['in_time']."', `shift_intime` = '".$data['shift_intime']."', `status` = '0', `dept` = '".$this->db->escape($data['department'])."', `unit` = '".$this->db->escape($data['unit'])."', `group` = '".$this->db->escape($data['group'])."', `late_time` = '".$data['late_time']."'  ");
		//$id = $this->db->getLastId();
		// echo $id;
		// echo '<br />';
		// if ($sql == true) {
		// 	echo 'true';
		// } else {
		// 	echo 'false';
		// }
		// exit;
		//echo 'done';exit;
	}

	public function getprevmonthtransdata($filter_month, $filter_year) {
		//$sql = "SELECT * FROM `" . DB_PREFIX . "shift_schedule` WHERE `month` = '".$this->db->escape($filter_month)."' AND `year` = '".$this->db->escape($filter_year)."' "; 
		$sql = "SELECT * FROM `" . DB_PREFIX . "shift_schedule` WHERE 1=1 "; 
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getexistdata($filter_month, $filter_year) {
		//$sql = "SELECT * FROM `" . DB_PREFIX . "shift_schedule` WHERE `month` = '".$this->db->escape($filter_month)."' AND `year` = '".$this->db->escape($filter_year)."' "; 
		$sql = "SELECT * FROM `" . DB_PREFIX . "shift_schedule` WHERE 1=1"; 
		//echo $sql;exit;
		$query = $this->db->query($sql);
		return $query->num_rows;
	}

	public function getemployee_exist($id) {
		$sql = "SELECT `status` FROM `" . DB_PREFIX . "employee` where `emp_code` = '".$id."' "; 
		//$this->log->write($sql);
		$query = $this->db->query($sql);
		$status = 0;
		if($query->num_rows > 0){
			$statuss = $query->row['status'];
			if($statuss == 1){
				$status = 1;
			} else {
				$status = 0;
			}
		}
		return $status;
	}

	public function getshift_schedule($emp_code, $day) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "shift_schedule WHERE emp_code = '" . (int)$emp_code . "' AND `month` = '".date('n')."' AND `year` = '".date('Y')."' ");
		return $query->row;
	}

	public function getempexist($emp_code) {
		$query = $this->db->query("SELECT count(*) AS total FROM " . DB_PREFIX . "employee WHERE emp_code = '" . (int)$emp_code . "' ");
		if(isset($query->row['total'])){
			return $query->row['total'];
		} else{
			return 0;
		}
	}

	public function gettransaction_data($transaction_id) {
		$sql = "SELECT * FROM `oc_transaction` WHERE `transaction_id` = '".$transaction_id."' ";
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getleavetransaction($data = array()) {
		$sql = "SELECT * FROM `oc_leave_transaction` WHERE 1=1 ";
		if (!empty($data['filter_name_id'])) {
			$sql .= " AND `emp_id` = '" . $this->db->escape($data['filter_name_id']) . "'";
		}

		if (!empty($data['filter_name_id_1'])) {
			$sql .= " AND `emp_id` = '" . $this->db->escape($data['filter_name_id_1']) . "'";
		}

		if (isset($this->session->data['emp_code'])) {
			$sql .= " AND `emp_id` = '" . $this->db->escape($this->session->data['emp_code']) . "'";
		}

		if (!empty($data['filter_date'])) {
			$sql .= " AND `dot` = '" . $this->db->escape($data['filter_date']) . "'";
		}

		if (!empty($data['filter_unit'])) {
			$sql .= " AND `unit` = '" . $this->db->escape($data['filter_unit']) . "'";
		}

		if (isset($data['filter_batch']) && !empty($data['filter_batch'])) {
			$sql .= " AND `batch_id` = '" . $this->db->escape($data['filter_batch']) . "'";
		}

		//$sql .= " AND `a_status` = '1' AND `p_status` = '0' GROUP BY batch_id ";
		$sql .= " GROUP BY batch_id ORDER BY batch_id DESC";

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}				

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);
		return $query->rows;
	}

	// public function gettotal_leave_days($batch_id) {
	// 	$sql = "SELECT COUNT(*) as days FROM `oc_leave_transaction` WHERE `batch_id` = '".$batch_id."' ";
	// 	$query = $this->db->query($sql);
	// 	return $query->row['days'];
	// }

	public function gettotal_leave_days($batch_id) {
		$sql = "SELECT `days`, `leave_amount` FROM `oc_leave_transaction` WHERE `batch_id` = '".$batch_id."' ";
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function getleave_from($batch_id) {
		$sql = "SELECT `date` FROM `oc_leave_transaction` WHERE `batch_id` = '".$batch_id."' ORDER by date(date) ASC LIMIT 1 ";
		$query = $this->db->query($sql);
		return $query->row['date'];
	}

	public function getleave_to($batch_id) {
		$sql = "SELECT `date` FROM `oc_leave_transaction` WHERE `batch_id` = '".$batch_id."' ORDER by date(date) DESC LIMIT 1 ";
		$query = $this->db->query($sql);
		return $query->row['date'];
	}

	public function getTotalleaveetransaction($data = array()) {
		$sql = "SELECT * FROM `oc_leave_transaction` WHERE 1=1 ";
		if (!empty($data['filter_name_id'])) {
			$sql .= " AND `emp_id` = '" . $this->db->escape($data['filter_name_id']) . "'";
		}

		if (isset($this->session->data['emp_code'])) {
			$sql .= " AND `emp_id` = '" . $this->db->escape($this->session->data['emp_code']) . "'";
		}

		if (!empty($data['filter_name_id_1'])) {
			$sql .= " AND `emp_id` = '" . $this->db->escape($data['filter_name_id_1']) . "'";
		}

		if (!empty($data['filter_date'])) {
			$sql .= " AND `dot` = '" . $this->db->escape($data['filter_date']) . "'";
		}

		if (!empty($data['filter_unit'])) {
			$sql .= " AND `unit` = '" . $this->db->escape($data['filter_unit']) . "'";
		}

		if (isset($data['filter_batch']) && !empty($data['filter_batch'])) {
			$sql .= " AND `batch_id` = '" . $this->db->escape($data['filter_batch']) . "'";
		}
		//$sql .= " AND `a_status` = '1' AND `p_status` = '0' GROUP BY batch_id ";
		$sql .= " GROUP BY batch_id ";
		$query = $this->db->query($sql);
		return $query->num_rows;
	}

	public function gettotal_bal($emp_code) {
		$sql = "SELECT `pl_bal`, `cl_bal`, `sl_bal`, `pl_acc`, `cl_acc`, `sl_acc`, `cof_acc` FROM `oc_leave` WHERE `emp_id` = '".$emp_code."' AND `close_status` = '0' ";
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function gettotal_bal_p($emp_code, $type) {
		$sql = "SELECT COUNT(*) as `bal_p`, encash FROM `oc_leave_transaction` WHERE `emp_id` = '" . $emp_code . "' AND `leave_amount`= '' AND `leave_type`= '".$type."' AND (`p_status` = '0') AND `a_status` = '1' ";
		//echo $sql;exit;
		//$sql .= " AND `a_status` = '1' AND `p_status` = '0' GROUP BY batch_id ";
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function gettotal_bal_p1($emp_code, $type) {
		$sql = "SELECT SUM(`leave_amount`) as leave_amount FROM `oc_leave_transaction` WHERE `emp_id` = '" . $emp_code . "' AND `days` = '' AND `leave_type`= '".$type."' AND (`p_status` = '0') AND `a_status` = '1' ";
		//echo $sql;exit;
		//$sql .= " AND `a_status` = '1' AND `p_status` = '0' GROUP BY batch_id ";
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function gettotal_bal_pro($emp_code, $type) {
		$sql = "SELECT `year` FROM `oc_leave` WHERE `emp_id` = '".$emp_code."' AND `close_status` = '0' ";
		//$this->log->write($sql);
		$query = $this->db->query($sql);
		$year = $query->row['year'];
		$comp_date = $year.'-01-01';
		$sql = "SELECT COUNT(*) as `bal_p`, `encash` FROM `oc_leave_transaction` WHERE `emp_id` = '" . $emp_code . "' AND `leave_amount` = '' AND `leave_type`= '".$type."' AND (`p_status` = '1') AND `a_status` = '1' AND date(`date`) >= '".$comp_date."' ";
		//echo $sql;exit;
		//$sql .= " AND `a_status` = '1' AND `p_status` = '0' GROUP BY batch_id ";
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function gettotal_bal_pro1($emp_code, $type) {
		$sql = "SELECT `year` FROM `oc_leave` WHERE `emp_id` = '".$emp_code."' AND `close_status` = '0' ";
		//$this->log->write($sql);
		$query = $this->db->query($sql);
		$year = $query->row['year'];
		$comp_date = $year.'-01-01';
		$sql = "SELECT SUM(`leave_amount`) as leave_amount FROM `oc_leave_transaction` WHERE `emp_id` = '" . $emp_code . "' AND `days` = '' AND `leave_type`= '".$type."' AND (`p_status` = '1') AND `a_status` = '1' AND date(`date`) >= '".$comp_date."' ";
		//echo $sql;exit;
		//$sql .= " AND `a_status` = '1' AND `p_status` = '0' GROUP BY batch_id ";
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function gettransaction_data_leave($emp_code, $data) {
		$sql = "SELECT * FROM `oc_transaction` WHERE 1=1";
		if (!empty($data['filter_date_start'])) {
			$sql .= " AND DATE(`date`) >= '" . $this->db->escape($data['filter_date_start']) . "'";
		}
		if (!empty($data['filter_date_end'])) {
			$sql .= " AND DATE(`date`) <= '" . $this->db->escape($data['filter_date_end']) . "'";
		}
		$sql .= " AND emp_id = '".$emp_code."' ";
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getTotalleaveetransaction_ess($data = array()) {
		$sql = "SELECT * FROM `oc_leave_transaction_temp` WHERE 1=1 ";
		if (!empty($data['filter_name_id'])) {
			$sql .= " AND `emp_id` = '" . $this->db->escape($data['filter_name_id']) . "'";
		}

		if (!empty($data['filter_name_id_1'])) {
			$sql .= " AND `emp_id` = '" . $this->db->escape($data['filter_name_id_1']) . "'";
		}

		if (isset($this->session->data['emp_code'])) {
			$sql .= " AND `emp_id` = '" . $this->db->escape($this->session->data['emp_code']) . "'";
		}

		// if (isset($this->session->data['dept_name'])) {
		// 	$sql .= " AND LOWER(dept_name) = '" . $this->db->escape(strtolower($this->session->data['dept_name'])) . "' ";
		// }

		if (!empty($data['filter_dept'])) {
			$sql .= " AND LOWER(dept_name) = '" . $this->db->escape(strtolower($data['filter_dept'])) . "' ";
		}

		if (isset($data['filter_depts']) && !empty($data['filter_depts'])) {
			$sql .= " AND LOWER(dept_name) IN (" . strtolower($data['filter_depts']) . ") ";
		}

		if (!empty($data['filter_group'])) {
			$sql .= " AND LOWER(`group`) = '" . $this->db->escape(strtolower($data['filter_group'])) . "' ";
		}

		if (!empty($data['filter_date'])) {
			$sql .= " AND `dot` = '" . $this->db->escape($data['filter_date']) . "'";
		}

		if (!empty($data['filter_unit'])) {
			$sql .= " AND `unit` = '" . $this->db->escape($data['filter_unit']) . "'";
		}

		if (!empty($data['filter_leavetype'])) {
			$sql .= " AND `leave_type` = '" . $this->db->escape($data['filter_leavetype']) . "'";
		}

		if (!empty($data['filter_approval_1_by'])) {
			$sql .= " AND `approved_1_by` = '" . $this->db->escape($data['filter_approval_1_by']) . "'";
		}

		if (!empty($data['filter_approval_1'])) {
			if($data['filter_approval_1'] == '1'){
				$sql .= " AND `approval_1` = '0'";
			} else {
				$sql .= " AND `approval_1` = '1'";	
			}
		}

		if (!empty($data['filter_approval_2'])) {
			if($data['filter_approval_2'] == '1'){
				$sql .= " AND `approval_2` = '0'";
			} else {
				$sql .= " AND `approval_2` = '1'";	
			}
		}

		if (!empty($data['filter_proc'])) {
			if($data['filter_proc'] == '1'){
				$sql .= " AND `p_status` = '0'";
			} else {
				$sql .= " AND `p_status` = '1'";	
			}
		}
		//$sql .= " AND `a_status` = '1' AND `p_status` = '0' GROUP BY batch_id ";
		$sql .= " GROUP BY batch_id ";
		//echo $sql;exit;
		$query = $this->db->query($sql);
		return $query->num_rows;
	}

	public function getleavetransaction_ess($data = array()) {
		$sql = "SELECT * FROM `oc_leave_transaction_temp` WHERE 1=1 ";
		if (!empty($data['filter_name_id'])) {
			$sql .= " AND `emp_id` = '" . $this->db->escape($data['filter_name_id']) . "'";
		}

		if (!empty($data['filter_name_id_1'])) {
			$sql .= " AND `emp_id` = '" . $this->db->escape($data['filter_name_id_1']) . "'";
		}

		if (isset($this->session->data['emp_code'])) {
			$sql .= " AND `emp_id` = '" . $this->db->escape($this->session->data['emp_code']) . "'";
		}

		// if (isset($this->session->data['dept_name'])) {
		// 	$sql .= " AND LOWER(dept_name) = '" . $this->db->escape(strtolower($this->session->data['dept_name'])) . "' ";
		// }

		if (!empty($data['filter_dept'])) {
			$sql .= " AND LOWER(dept_name) = '" . $this->db->escape(strtolower($data['filter_dept'])) . "' ";
		}

		if (isset($data['filter_depts']) && !empty($data['filter_depts'])) {
			$sql .= " AND LOWER(dept_name) IN (" . strtolower($data['filter_depts']) . ") ";
		}

		if (!empty($data['filter_date'])) {
			$sql .= " AND `dot` = '" . $this->db->escape($data['filter_date']) . "'";
		}

		if (!empty($data['filter_unit'])) {
			$sql .= " AND `unit` = '" . $this->db->escape($data['filter_unit']) . "'";
		}

		if (!empty($data['filter_group'])) {
			$sql .= " AND LOWER(`group`) = '" . $this->db->escape(strtolower($data['filter_group'])) . "' ";
		}

		if (!empty($data['filter_leavetype'])) {
			$sql .= " AND `leave_type` = '" . $this->db->escape($data['filter_leavetype']) . "'";
		}

		if (!empty($data['filter_approval_1_by'])) {
			$sql .= " AND `approved_1_by` = '" . $this->db->escape($data['filter_approval_1_by']) . "'";
		}

		if (!empty($data['filter_approval_1'])) {
			if($data['filter_approval_1'] == '1'){
				$sql .= " AND `approval_1` = '0'";
			} else if($data['filter_approval_1'] == '3') {
				$sql .= " AND `approval_1` = '2'";
			} else {
				$sql .= " AND `approval_1` = '1'";	
			}
		}

		if (!empty($data['filter_approval_2'])) {
			if($data['filter_approval_2'] == '1'){
				$sql .= " AND `approval_2` = '0'";
			} else {
				$sql .= " AND `approval_2` = '1'";	
			}
		}

		if (!empty($data['filter_proc'])) {
			if($data['filter_proc'] == '1'){
				$sql .= " AND `p_status` = '0'";
			} else {
				$sql .= " AND `p_status` = '1'";	
			}
		}

		//$sql .= " AND `a_status` = '1' AND `p_status` = '0' GROUP BY batch_id ";
		$sql .= " GROUP BY batch_id ORDER BY batch_id DESC";

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}				

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		//$this->log->write($sql);
		//echo $sql;exit;
		$query = $this->db->query($sql);
		//$this->log->write(print_r($query,true));
		return $query->rows;
	}

	public function gettotal_leave_days_ess($batch_id) {
		$sql = "SELECT `days`, `leave_amount` FROM `oc_leave_transaction_temp` WHERE `batch_id` = '".$batch_id."' ";
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function getleave_from_ess($batch_id) {
		$sql = "SELECT `date` FROM `oc_leave_transaction_temp` WHERE `batch_id` = '".$batch_id."' ORDER by date(date) ASC LIMIT 1 ";
		$query = $this->db->query($sql);
		if(isset($query->row['date'])){
			return $query->row['date'];
		} else {
			return '';
		}
	}

	public function getleave_to_ess($batch_id) {
		$sql = "SELECT `date` FROM `oc_leave_transaction_temp` WHERE `batch_id` = '".$batch_id."' ORDER by date(date) DESC LIMIT 1 ";
		$query = $this->db->query($sql);
		if(isset($query->row['date'])){
			return $query->row['date'];
		} else {
			return '';
		}
	}

	public function getleave_transaction_data_ess($batch_id) {
		$sql = "SELECT * FROM `oc_leave_transaction_temp` WHERE `batch_id` = '".$batch_id."' ";
		//echo $sql;exit;
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getpltakenyear_ess($emp_code) {
		$sql = "SELECT `id` FROM `oc_leave_transaction_temp` WHERE `emp_id` = '".$emp_code."' AND `leave_type` = 'PL' AND `year` = '".date('Y')."' AND `encash` <> '' AND `a_status` = '1' ";
		//echo $sql;exit;
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function gettotal_bal_ess($emp_code) {
		$sql = "SELECT `pl_bal`, `cl_bal`, `sl_bal`,`lwp_bal`, `ml_bal`, `pal_bal`, `pl_acc`, `cl_acc`, `sl_acc`,`lwp_acc`, `ml_acc`, `pal_acc`, `cof_acc` FROM `oc_leave` WHERE `emp_id` = '".$emp_code."' AND `close_status` = '0' ";
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function gettotal_bal_p_ess($emp_code, $type) {
		$sql = "SELECT COUNT(*) as `bal_p`, encash FROM `oc_leave_transaction_temp` WHERE `emp_id` = '" . $emp_code . "' AND `leave_amount`= '' AND `leave_type`= '".$type."' AND (`p_status` = '0') AND `a_status` = '1' ";
		//echo $sql;exit;
		//$sql .= " AND `a_status` = '1' AND `p_status` = '0' GROUP BY batch_id ";
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function gettotal_bal_p1_ess($emp_code, $type) {
		$sql = "SELECT SUM(`leave_amount`) as leave_amount FROM `oc_leave_transaction_temp` WHERE `emp_id` = '" . $emp_code . "' AND `days` = '' AND `leave_type`= '".$type."' AND (`p_status` = '0') AND `a_status` = '1' ";
		//echo $sql;exit;
		//$sql .= " AND `a_status` = '1' AND `p_status` = '0' GROUP BY batch_id ";
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function gettotal_bal_pro_ess($emp_code, $type) {
		$sql = "SELECT `year` FROM `oc_leave` WHERE `emp_id` = '".$emp_code."' AND `close_status` = '0' ";
		//$this->log->write($sql);
		$query = $this->db->query($sql);
		$year = $query->row['year'];
		$comp_date = $year.'-01-01';
		$sql = "SELECT COUNT(*) as `bal_p`, `encash` FROM `oc_leave_transaction_temp` WHERE `emp_id` = '" . $emp_code . "' AND `leave_amount` = '' AND `leave_type`= '".$type."' AND (`p_status` = '1') AND `a_status` = '1' AND date(`date`) >= '".$comp_date."' ";
		//echo $sql;exit;
		//$sql .= " AND `a_status` = '1' AND `p_status` = '0' GROUP BY batch_id ";
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function gettotal_bal_pro1_ess($emp_code, $type) {
		$sql = "SELECT `year` FROM `oc_leave` WHERE `emp_id` = '".$emp_code."' AND `close_status` = '0' ";
		//$this->log->write($sql);
		$query = $this->db->query($sql);
		$year = $query->row['year'];
		$comp_date = $year.'-01-01';
		$sql = "SELECT SUM(`leave_amount`) as leave_amount FROM `oc_leave_transaction_temp` WHERE `emp_id` = '" . $emp_code . "' AND `days` = '' AND `leave_type`= '".$type."' AND (`p_status` = '1') AND `a_status` = '1' AND date(`date`) >= '".$comp_date."' ";
		//echo $sql;exit;
		//$sql .= " AND `a_status` = '1' AND `p_status` = '0' GROUP BY batch_id ";
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function getTotalrequest($data = array()) {
		$sql = "SELECT * FROM `oc_requestform` WHERE 1=1 ";
		if (!empty($data['filter_name_id'])) {
			$sql .= " AND `emp_id` = '" . $this->db->escape($data['filter_name_id']) . "'";
		}

		if (!empty($data['filter_date'])) {
			$sql .= " AND `doi` = '" . $this->db->escape($data['filter_date']) . "'";
		}

		if (isset($data['filter_dept']) && !empty($data['filter_dept'])) {
			$sql .= " AND `dept_name` = '" . $this->db->escape($data['filter_dept']) . "'";
		}

		if (isset($data['filter_depts']) && !empty($data['filter_depts'])) {
			$sql .= " AND LOWER(dept_name) IN (" . strtolower($data['filter_depts']) . ") ";
		}

		if (isset($data['filter_unit']) && !empty($data['filter_unit'])) {
			$sql .= " AND `unit` = '" . $this->db->escape($data['filter_unit']) . "'";
		}

		if (!empty($data['filter_approval_1'])) {
			if($data['filter_approval_1'] == '1'){
				$sql .= " AND `approval_1` = '0'";
			} elseif($data['filter_approval_1'] == '2') {
				$sql .= " AND (`approval_1` = '1' OR `approval_1` = '3' OR `approval_1` = '4') ";	
			} elseif($data['filter_approval_1'] == '3') {
				$sql .= " AND `approval_1` = '2'";	
			}
		}
		
		//$sql .= " AND `a_status` = '1' AND `p_status` = '0' GROUP BY batch_id ";
		//$sql .= " GROUP BY batch_id ";
		$query = $this->db->query($sql);
		return $query->num_rows;
	}

	public function getrequests($data = array()) {
		$sql = "SELECT * FROM `oc_requestform` WHERE 1=1 ";
		if (!empty($data['filter_name_id'])) {
			$sql .= " AND `emp_id` = '" . $this->db->escape($data['filter_name_id']) . "'";
		}

		if (!empty($data['filter_date'])) {
			$sql .= " AND `doi` = '" . $this->db->escape($data['filter_date']) . "'";
		}

		if (!empty($data['filter_approval_1'])) {
			if($data['filter_approval_1'] == '1'){
				$sql .= " AND `approval_1` = '0'";
			} elseif($data['filter_approval_1'] == '2') {
				$sql .= " AND (`approval_1` = '1' OR `approval_1` = '3' OR `approval_1` = '4') ";
			} elseif($data['filter_approval_1'] == '3') {
				$sql .= " AND `approval_1` = '2'";	
			}
		}

		if (isset($data['filter_dept']) && !empty($data['filter_dept'])) {
			$sql .= " AND `dept_name` = '" . $this->db->escape($data['filter_dept']) . "'";
		}

		if (isset($data['filter_depts']) && !empty($data['filter_depts'])) {
			$sql .= " AND LOWER(dept_name) IN (" . strtolower($data['filter_depts']) . ") ";
		}

		if (isset($data['filter_unit']) && !empty($data['filter_unit'])) {
			$sql .= " AND `unit` = '" . $this->db->escape($data['filter_unit']) . "'";
		}
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}				

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getTotalrequest_unit($data = array()) {
		$sql = "SELECT * FROM `oc_requestform` WHERE 1=1 ";
		if (!empty($data['filter_name_id'])) {
			$sql .= " AND `emp_id` = '" . $this->db->escape($data['filter_name_id']) . "'";
		}

		if (!empty($data['filter_date'])) {
			$sql .= " AND `doi` = '" . $this->db->escape($data['filter_date']) . "'";
		}

		if (isset($data['filter_dept']) && !empty($data['filter_dept'])) {
			$sql .= " AND `dept_name` = '" . $this->db->escape($data['filter_dept']) . "'";
		}

		if (isset($data['filter_unit']) && !empty($data['filter_unit'])) {
			$sql .= " AND `unit` = '" . $this->db->escape($data['filter_unit']) . "'";
		}

		if (!empty($data['filter_approval_1'])) {
			if($data['filter_approval_1'] == '1'){
				$sql .= " AND `approval_1` = '1'";
			} elseif($data['filter_approval_1'] == '4') {
				$sql .= " AND `approval_1` = '4'";	
			}
		} else {
			$sql .= " AND (`approval_1` = '1' OR `approval_1` = '4') ";
		}
		
		//$sql .= " GROUP BY batch_id ";
		$query = $this->db->query($sql);
		return $query->num_rows;
	}

	public function getrequests_unit($data = array()) {
		$sql = "SELECT * FROM `oc_requestform` WHERE 1=1 ";
		if (!empty($data['filter_name_id'])) {
			$sql .= " AND `emp_id` = '" . $this->db->escape($data['filter_name_id']) . "'";
		}

		if (!empty($data['filter_date'])) {
			$sql .= " AND `doi` = '" . $this->db->escape($data['filter_date']) . "'";
		}

		if (isset($data['filter_dept']) && !empty($data['filter_dept'])) {
			$sql .= " AND `dept_name` = '" . $this->db->escape($data['filter_dept']) . "'";
		}

		if (isset($data['filter_unit']) && !empty($data['filter_unit'])) {
			$sql .= " AND `unit` = '" . $this->db->escape($data['filter_unit']) . "'";
		}

		if (!empty($data['filter_approval_1'])) {
			if($data['filter_approval_1'] == '1'){
				$sql .= " AND `approval_1` = '1'";
			} elseif($data['filter_approval_1'] == '4') {
				$sql .= " AND `approval_1` = '4'";	
			}
		} else {
			$sql .= " AND (`approval_1` = '1' OR `approval_1` = '4') ";
		}
		
		//$sql .= " GROUP BY batch_id ";
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getleave_data($batch_id) {
		$sql = "SELECT `leave_type` FROM `oc_leave_transaction_temp` WHERE `batch_id` = '".$batch_id."' LIMIT 1 ";
		$query = $this->db->query($sql);
		return $query->row;
	}

	

	public function getleavetransaction_ess_new($data = array(),$emp_code) {
		$sql = "SELECT * FROM `oc_leave_transaction_temp` WHERE 1=1 ";
		
		$sql .= " AND `reporting_to` = '".$emp_code."'";
		
		if (!empty($data['filter_name_id'])) {
			$sql .= " AND `emp_id` = '" . $this->db->escape($data['filter_name_id']) . "'";
		}

		if (!empty($data['filter_name_id_1'])) {
			$sql .= " AND `emp_id` = '" . $this->db->escape($data['filter_name_id_1']) . "'";
		}

		// if (isset($this->session->data['emp_code'])) {
		// 	$sql .= " AND `emp_id` = '" . $this->db->escape($this->session->data['emp_code']) . "'";
		// }

		// if (isset($this->session->data['dept_name'])) {
		// 	$sql .= " AND LOWER(dept_name) = '" . $this->db->escape(strtolower($this->session->data['dept_name'])) . "' ";
		// }

		if (!empty($data['filter_dept'])) {
			$sql .= " AND LOWER(dept_name) = '" . $this->db->escape(strtolower($data['filter_dept'])) . "' ";
		}

		if (isset($data['filter_depts']) && !empty($data['filter_depts'])) {
			$sql .= " AND LOWER(dept_name) IN (" . strtolower($data['filter_depts']) . ") ";
		}

		if (!empty($data['filter_date'])) {
			$sql .= " AND `dot` = '" . $this->db->escape($data['filter_date']) . "'";
		}

		if (!empty($data['filter_unit'])) {
			$sql .= " AND `unit` = '" . $this->db->escape($data['filter_unit']) . "'";
		}

		if (!empty($data['filter_group'])) {
			$sql .= " AND LOWER(`group`) = '" . $this->db->escape(strtolower($data['filter_group'])) . "' ";
		}

		if (!empty($data['filter_leavetype'])) {
			$sql .= " AND `leave_type` = '" . $this->db->escape($data['filter_leavetype']) . "'";
		}

		if (!empty($data['filter_approval_1_by'])) {
			$sql .= " AND `approved_1_by` = '" . $this->db->escape($data['filter_approval_1_by']) . "'";
		}

		if (!empty($data['filter_approval_1'])) {
			if($data['filter_approval_1'] == '1'){
				$sql .= " AND `approval_1` = '0'";
			} else if($data['filter_approval_1'] == '3') {
				$sql .= " AND `approval_1` = '2'";
			} else {
				$sql .= " AND `approval_1` = '1'";	
			}
		}

		if (!empty($data['filter_approval_2'])) {
			if($data['filter_approval_2'] == '1'){
				$sql .= " AND `approval_2` = '0'";
			} else {
				$sql .= " AND `approval_2` = '1'";	
			}
		}

		if (!empty($data['filter_proc'])) {
			if($data['filter_proc'] == '1'){
				$sql .= " AND `p_status` = '0'";
			} else {
				$sql .= " AND `p_status` = '1'";	
			}
		}

		//$sql .= " AND `a_status` = '1' AND `p_status` = '0' GROUP BY batch_id ";
		$sql .= " GROUP BY batch_id ORDER BY batch_id DESC";

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}				

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		//$this->log->write($sql);
		//echo $sql;exit;
		$query = $this->db->query($sql);
		//$this->log->write(print_r($query,true));
		return $query->rows;
	}
}	
?>