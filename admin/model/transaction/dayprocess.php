<?php
class ModelTransactionDayprocess extends Model {
	
	public function getNextDate($unit) {
		$sql = "SELECT `date` FROM " . DB_PREFIX . "transaction WHERE `day_close_status` = '1' ";
		if($unit){
			$sql .= " AND `unit` = '".$unit."' ";
		}
		$sql .= " GROUP BY `date` ORDER BY `date` DESC LIMIT 0,1 ";
		$query = $this->db->query($sql);
		if(isset($query->rows[0]['date'])) {
			return date('Y-m-d', strtotime($query->rows[0]['date'] .' +1 day'));	
		} else {
			return date('Y-m-d', strtotime('2016-10-01'));
		}
		
	}

	public function getData($date, $unit) {
		//echo "SELECT * FROM " . DB_PREFIX . "transaction WHERE `date` = '".$date."' AND `unit` = '".$unit."' AND `abnormal_status` = '1' ORDER BY `date` ASC";
		//exit; 
		$sql = "SELECT * FROM " . DB_PREFIX . "transaction WHERE `date` = '".$date."' AND `abnormal_status` = '1' ";
		if($unit){
			$sql .= " AND `unit` = '".$unit."' ";
		}
		$sql .= " ORDER BY `date` ASC";
		$query = $this->db->query($sql);
		if(isset($query->rows)) {
			return $query->rows;	
		} else {
			return 0;
		}
		
	}

	public function update_close_day($date, $unit) {
		$sql = "UPDATE " . DB_PREFIX . "transaction SET `day_close_status` = '1' WHERE `date` = '".$date."' ";
		if($unit){
			$sql .= " AND `unit` = '".$unit."' ";
		}
		$this->db->query($sql);
	}

	public function getAbsent($date, $unit) {
		//$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "transaction WHERE `date` = '".$date."' AND `unit` = '".$unit."' AND `emp_id` = '21463' AND `absent_status` = '1' ORDER BY `date` ASC");
		$sql = "SELECT * FROM " . DB_PREFIX . "transaction WHERE `date` = '".$date."' AND `absent_status` = '1' ";
		if($unit){
			$sql .= " AND `unit` = '".$unit."' ";
		}
		$sql .= " ORDER BY `date` ASC";
		$query = $this->db->query($sql);
		if(isset($query->rows)) {
			return $query->rows;	
		} else {
			return 0;
		}
	}

	public function is_closed_stat($date, $emp_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "transaction WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' AND `day_close_status` = '1' ");
		//echo "SELECT * FROM " . DB_PREFIX . "transaction WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' AND `day_close_status` = '1' ";
		if($query->num_rows > 0) {
			return 1;	
		} else {
			return 0;
		}
	}

	public function checkLeave($date, $emp_id) {
		$this->load->model('transaction/transaction');
		$query = $this->db->query("SELECT * FROM ".DB_PREFIX."leave_transaction WHERE `emp_id` = '".$emp_id."' AND `date` = '".$date."' AND `p_status` = '0' AND `a_status` = '1' ");
		if($query->num_rows > 0) {
			$this->log->write('----start---');
			if($query->row['type'] != ''){
				//echo 'aaaa';exit;
				$trans_data = $this->db->query("SELECT * FROM ".DB_PREFIX."transaction WHERE `emp_id` = '".$emp_id."' AND `date` = '".$date."' ")->row;
				if($query->row['type'] == 'F'){
					$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `firsthalf_status` = '".$query->row['leave_type']."', `secondhalf_status` = '".$query->row['leave_type']."', `leave_status` = '1', absent_status = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");		
					$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `firsthalf_status` = '".$query->row['leave_type']."', `secondhalf_status` = '".$query->row['leave_type']."', `leave_status` = '1', absent_status = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");		
					
					$this->db->query("UPDATE " . DB_PREFIX . "leave_transaction SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' AND `type` = '".$query->row['type']."' ");	
					$this->log->write("UPDATE " . DB_PREFIX . "leave_transaction SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' AND `type` = '".$query->row['type']."' ");
				
					$this->db->query("UPDATE " . DB_PREFIX . "leave_transaction_temp SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' AND `type` = '".$query->row['type']."' ");	
					$this->log->write("UPDATE " . DB_PREFIX . "leave_transaction_temp SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' AND `type` = '".$query->row['type']."' ");
					
				} elseif($query->row['type'] == '1'){
					$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `firsthalf_status` = '".$query->row['leave_type']."', `leave_status` = '0.5' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");			
					$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `firsthalf_status` = '".$query->row['leave_type']."', `leave_status` = '0.5' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");			
					if($trans_data['halfday_status'] != 0){
						$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `secondhalf_status` = 'HD', `absent_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
						$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `secondhalf_status` = 'HD', `absent_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
					} elseif($trans_data['secondhalf_status'] == 'COF'){
						$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0', `present_status` = '0', `leave_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
						$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0', `present_status` = '0', `leave_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
					} elseif($trans_data['secondhalf_status'] == 1){
						$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0', `present_status` = '0.5' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
						$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0', `present_status` = '0.5' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
					} elseif($trans_data['secondhalf_status'] == 0){
						$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0.5', `present_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
						$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0.5', `present_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
					} 
					$this->db->query("UPDATE " . DB_PREFIX . "leave_transaction SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' AND `type` = '".$query->row['type']."' ");	
					$this->log->write("UPDATE " . DB_PREFIX . "leave_transaction SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' AND `type` = '".$query->row['type']."' ");
					
					$this->db->query("UPDATE " . DB_PREFIX . "leave_transaction_temp SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' AND `type` = '".$query->row['type']."' ");	
					$this->log->write("UPDATE " . DB_PREFIX . "leave_transaction_temp SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' AND `type` = '".$query->row['type']."' ");
				
				} elseif($query->row['type'] == '2'){
					$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `secondhalf_status` = '".$query->row['leave_type']."', `leave_status` = '0.5' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");			
					$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `secondhalf_status` = '".$query->row['leave_type']."', `leave_status` = '0.5' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");			
					if($trans_data['halfday_status'] != 0){
						$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `firsthalf_status` = 'HD', `absent_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
						$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `firsthalf_status` = 'HD', `absent_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
					} elseif($trans_data['firsthalf_status'] == 'COF'){
						$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0', `present_status` = '0', `leave_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
						$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0', `present_status` = '0', `leave_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
					} elseif($trans_data['firsthalf_status'] == 1){
						$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0', `present_status` = '0.5' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
						$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0', `present_status` = '0.5' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
					} elseif($trans_data['firsthalf_status'] == 0){
						$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0.5', `present_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
						$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0.5', `present_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
					}
					$this->db->query("UPDATE " . DB_PREFIX . "leave_transaction SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' AND `type` = '".$query->row['type']."' ");	
					$this->log->write("UPDATE " . DB_PREFIX . "leave_transaction SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' AND `type` = '".$query->row['type']."' ");

					$this->db->query("UPDATE " . DB_PREFIX . "leave_transaction_temp SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' AND `type` = '".$query->row['type']."' ");	
					$this->log->write("UPDATE " . DB_PREFIX . "leave_transaction_temp SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' AND `type` = '".$query->row['type']."' ");
				}
					
				$sql = "SELECT * FROM `oc_leave` WHERE `emp_id` = '".$emp_id."' AND `close_status` = '0' ";
				$query1 = $this->db->query($sql);
				if($query1->num_rows > 0){
					if($query->row['leave_type'] == 'PL'){
						//$balance = $query1->row['pl_bal'];

						$total_bal = $this->model_transaction_transaction->gettotal_bal($emp_id);
						$total_bal_pro = $this->model_transaction_transaction->gettotal_bal_pro($emp_id, 'PL');
						$total_bal_pro1 = $this->model_transaction_transaction->gettotal_bal_pro1($emp_id, 'PL');
						$total_bal_pl = 0;
						if(isset($total_bal['pl_acc'])){
							if(isset($total_bal_pro['bal_p'])){
								$total_bal_pl = $total_bal['pl_acc'] - ($total_bal_pro['bal_p'] + $total_bal_pro['encash'] + $total_bal_pro1['leave_amount']);
							} else {
								$total_bal_pl = $total_bal['pl_acc'];
							}
						}

						$balance = $total_bal_pl - $query->row['leave_amount'];
						$upd_bal_sql = "UPDATE `oc_leave` SET `pl_bal` = '".$balance."' WHERE `emp_id` = '".$emp_id."' AND `close_status` = '0' ";
						$this->db->query($upd_bal_sql);
						$this->log->write($upd_bal_sql);
					} elseif($query->row['leave_type'] == 'CL'){
						//$balance = $query1->row['cl_bal'];
						
						$total_bal = $this->model_transaction_transaction->gettotal_bal($emp_id);
						$total_bal_pro = $this->model_transaction_transaction->gettotal_bal_pro($emp_id, 'CL');
						$total_bal_pro1 = $this->model_transaction_transaction->gettotal_bal_pro1($emp_id, 'CL');
						$total_bal_cl = 0;
						if(isset($total_bal['cl_acc'])){
							if(isset($total_bal_pro['bal_p'])){
								$total_bal_cl = $total_bal['cl_acc'] - ($total_bal_pro['bal_p'] + $total_bal_pro1['leave_amount']);
							} else {
								$total_bal_cl = $total_bal['cl_acc'];
							}
						}

						$balance = $total_bal_cl - $query->row['leave_amount'];
						$upd_bal_sql = "UPDATE `oc_leave` SET `cl_bal` = '".$balance."' WHERE `emp_id` = '".$emp_id."' AND `close_status` = '0' ";
						$this->db->query($upd_bal_sql);
						$this->log->write($upd_bal_sql);
					} elseif($query->row['leave_type'] == 'SL'){
						//$balance = $query1->row['sl_bal'];
						
						$total_bal = $this->model_transaction_transaction->gettotal_bal($emp_id);
						$total_bal_pro = $this->model_transaction_transaction->gettotal_bal_pro($emp_id, 'SL');
						$total_bal_pro1 = $this->model_transaction_transaction->gettotal_bal_pro1($emp_id, 'SL');
						$total_bal_sl = 0;
						if(isset($total_bal['sl_acc'])){
							if(isset($total_bal_pro['bal_p'])){
								$total_bal_sl = $total_bal['sl_acc'] - ($total_bal_pro['bal_p'] + $total_bal_pro1['leave_amount']);
							} else {
								$total_bal_sl = $total_bal['sl_acc'];
							}
						}

						$balance = $total_bal_sl - $query->row['leave_amount'];
						$upd_bal_sql = "UPDATE `oc_leave` SET `sl_bal` = '".$balance."' WHERE `emp_id` = '".$emp_id."' AND `close_status` = '0' ";
						$this->db->query($upd_bal_sql);
						$this->log->write($upd_bal_sql);
					} 

					// elseif($query->row['leave_type'] == 'COF'){
					// 	//$balance = $query1->row['cof_bal'];
						
					// 	$total_bal = $this->model_transaction_transaction->gettotal_bal($emp_id);
					// 	$total_bal_pro = $this->model_transaction_transaction->gettotal_bal_pro($emp_id, 'COF');
					// 	$total_bal_cof = 0;
					// 	if(isset($total_bal['cof_acc'])){
					// 		if(isset($total_bal_pro['bal_p'])){
					// 			$total_bal_cof = $total_bal['cof_acc'] - $total_bal_pro['bal_p'];
					// 		} else {
					// 			$total_bal_cof = $total_bal['cof_acc'];
					// 		}
					// 	}

					// 	$balance = $total_bal_cof - $query->row['leave_amount'];
					// 	$upd_bal_sql = "UPDATE `oc_leave` SET `cof_bal` = '".$balance."' WHERE `emp_id` = '".$emp_id."' AND `close_status` = '0' ";
					// 	$this->db->query($upd_bal_sql);
					// 	$this->log->write($upd_bal_sql);
					// }	
				}
			} else {
				$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `firsthalf_status` = '".$query->row['leave_type']."', `secondhalf_status` = '".$query->row['leave_type']."', `leave_status` = '1', absent_status = '0', `present_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");		
				$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `firsthalf_status` = '".$query->row['leave_type']."', `secondhalf_status` = '".$query->row['leave_type']."', `leave_status` = '1', absent_status = '0', `present_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");		
				
				$this->db->query("UPDATE " . DB_PREFIX . "leave_transaction SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");		
				$this->log->write("UPDATE " . DB_PREFIX . "leave_transaction SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");		
				
				$this->db->query("UPDATE " . DB_PREFIX . "leave_transaction_temp SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ");	
				$this->log->write("UPDATE " . DB_PREFIX . "leave_transaction_temp SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' ");

				$sql = "SELECT * FROM `oc_leave` WHERE `emp_id` = '".$emp_id."' AND `close_status` = '0' ";
				$query1 = $this->db->query($sql);
				if($query1->num_rows > 0){
					if($query->row['leave_type'] == 'PL'){
						//echo 'in pl';exit;
						//$balance = $query1->row['pl_bal'];
						
						$total_bal = $this->model_transaction_transaction->gettotal_bal($emp_id);
						$total_bal_pro = $this->model_transaction_transaction->gettotal_bal_pro($emp_id, 'PL');
						$total_bal_pro1 = $this->model_transaction_transaction->gettotal_bal_pro1($emp_id, 'PL');
						$total_bal_pl = 0;
						if(isset($total_bal['pl_acc'])){
							if(isset($total_bal_pro['bal_p'])){
								$total_bal_pl = $total_bal['pl_acc'] - ($total_bal_pro['bal_p'] + $total_bal_pro['encash'] + $total_bal_pro1['leave_amount']);
							} else {
								$total_bal_pl = $total_bal['pl_acc'];
							}
						}

						$balance = $total_bal_pl - 1;
						$upd_bal_sql = "UPDATE `oc_leave` SET `pl_bal` = '".$balance."' WHERE `emp_id` = '".$emp_id."' AND `close_status` = '0' ";
						$this->db->query($upd_bal_sql);
						$this->log->write($upd_bal_sql);
					} elseif($query->row['leave_type'] == 'CL'){
						//echo 'in cl';exit;
						//$balance = $query1->row['cl_bal'];
						
						$total_bal = $this->model_transaction_transaction->gettotal_bal($emp_id);
						$total_bal_pro = $this->model_transaction_transaction->gettotal_bal_pro($emp_id, 'CL');
						$total_bal_pro1 = $this->model_transaction_transaction->gettotal_bal_pro1($emp_id, 'CL');
						$total_bal_cl = 0;
						if(isset($total_bal['cl_acc'])){
							if(isset($total_bal_pro['bal_p'])){
								$total_bal_cl = $total_bal['cl_acc'] - ($total_bal_pro['bal_p'] - $total_bal_pro1['leave_amount']);
							} else {
								$total_bal_cl = $total_bal['cl_acc'];
							}
						}

						$balance = $total_bal_cl - 1;
						$upd_bal_sql = "UPDATE `oc_leave` SET `cl_bal` = '".$balance."' WHERE `emp_id` = '".$emp_id."' AND `close_status` = '0' ";
						$this->db->query($upd_bal_sql);
						$this->log->write($upd_bal_sql);
					} elseif($query->row['leave_type'] == 'SL'){
						//$balance = $query1->row['sl_bal'];
						
						$total_bal = $this->model_transaction_transaction->gettotal_bal($emp_id);
						$total_bal_pro = $this->model_transaction_transaction->gettotal_bal_pro($emp_id, 'SL');
						$total_bal_pro1 = $this->model_transaction_transaction->gettotal_bal_pro1($emp_id, 'SL');
						// echo '<pre>';
						// print_r($total_bal);

						// echo '<pre>';
						// print_r($total_bal_pro);


						$total_bal_sl = 0;
						if(isset($total_bal['sl_acc'])){
							if(isset($total_bal_pro['bal_p'])){
								$total_bal_sl = $total_bal['sl_acc'] - ($total_bal_pro['bal_p'] + $total_bal_pro1['leave_amount']);
							} else {
								$total_bal_sl = $total_bal['sl_acc'];
							}
						}

						// echo $total_bal_sl;
						// echo '<br />';
						

						$balance = $total_bal_sl - 1;

						// echo $balance;
						// exit;

						$upd_bal_sql = "UPDATE `oc_leave` SET `sl_bal` = '".$balance."' WHERE `emp_id` = '".$emp_id."' AND `close_status` = '0' ";
						$this->db->query($upd_bal_sql);
						$this->log->write($upd_bal_sql);
					} 

					// elseif($query->row['leave_type'] == 'COF'){
					// 	//$balance = $query1->row['cof_bal'];
						
					// 	$total_bal = $this->model_transaction_transaction->gettotal_bal($emp_id);
					// 	$total_bal_pro = $this->model_transaction_transaction->gettotal_bal_pro($emp_id, 'COF');
					// 	$total_bal_cof = 0;
					// 	if(isset($total_bal['cof_acc'])){
					// 		if(isset($total_bal_pro['bal_p'])){
					// 			$total_bal_cof = $total_bal['cof_acc'] - $total_bal_pro['bal_p'];
					// 		} else {
					// 			$total_bal_cof = $total_bal['cof_acc'];
					// 		}
					// 	}

					// 	$balance = $total_bal_cof - 1;
					// 	$upd_bal_sql = "UPDATE `oc_leave` SET `cof_bal` = '".$balance."' WHERE `emp_id` = '".$emp_id."' AND `close_status` = '0' ";
					// 	$this->db->query($upd_bal_sql);
					// 	$this->log->write($upd_bal_sql);
					// }	
				}	
			}
			$this->log->write('----end---');
			//echo 'out';exit;
			//return $query->rows;	
		} else {
			return 0;
		}	
	}

	public function checkLeave_1($date, $emp_id) {
		$query = $this->db->query("SELECT * FROM ".DB_PREFIX."leave_transaction WHERE `emp_id` = '".$emp_id."' AND `date` = '".$date."' AND `a_status` = '1' AND `p_status` = '1' ");
		if($query->num_rows > 0) {
			$this->log->write('----start---');
			if($query->row['type'] != ''){
				$trans_data = $this->db->query("SELECT * FROM ".DB_PREFIX."transaction WHERE `emp_id` = '".$emp_id."' AND `date` = '".$date."' ")->row;
				if($query->row['type'] == 'F'){
					$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `firsthalf_status` = '".$query->row['leave_type']."', `secondhalf_status` = '".$query->row['leave_type']."', `leave_status` = '1', absent_status = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");		
					$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `firsthalf_status` = '".$query->row['leave_type']."', `secondhalf_status` = '".$query->row['leave_type']."', `leave_status` = '1', absent_status = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");		
				} elseif($query->row['type'] == '1'){
					$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `firsthalf_status` = '".$query->row['leave_type']."', `leave_status` = '0.5' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");			
					$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `firsthalf_status` = '".$query->row['leave_type']."', `leave_status` = '0.5' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");			
					if($trans_data['halfday_status'] != 0){
						$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `secondhalf_status` = 'HD', `absent_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
						$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `secondhalf_status` = 'HD', `absent_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
					} elseif($trans_data['secondhalf_status'] == 'COF'){
						$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0', `present_status` = '0', `leave_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
						$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0', `present_status` = '0', `leave_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
					} elseif($trans_data['secondhalf_status'] == 1){
						$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0', `present_status` = '0.5' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
						$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0', `present_status` = '0.5' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
					} elseif($trans_data['secondhalf_status'] == 0){
						$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0.5', `present_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
						$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0.5', `present_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
					} 
				} elseif($query->row['type'] == '2'){
					$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `secondhalf_status` = '".$query->row['leave_type']."', `leave_status` = '0.5' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");			
					$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `secondhalf_status` = '".$query->row['leave_type']."', `leave_status` = '0.5' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");			
					if($trans_data['halfday_status'] != 0){
						$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `firsthalf_status` = 'HD', `absent_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
						$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `firsthalf_status` = 'HD', `absent_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
					} elseif($trans_data['firsthalf_status'] == 'COF'){
						$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0', `present_status` = '0', `leave_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
						$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0', `present_status` = '0', `leave_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
					} elseif($trans_data['firsthalf_status'] == 1){
						$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0', `present_status` = '0.5' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
						$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0', `present_status` = '0.5' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
					} elseif($trans_data['firsthalf_status'] == 0){
						$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0.5', `present_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
						$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `absent_status` = '0.5', `present_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");
					}
				}
			} else {
				$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `firsthalf_status` = '".$query->row['leave_type']."', `secondhalf_status` = '".$query->row['leave_type']."', `leave_status` = '1', absent_status = '0', `present_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");		
				$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `firsthalf_status` = '".$query->row['leave_type']."', `secondhalf_status` = '".$query->row['leave_type']."', `leave_status` = '1', absent_status = '0', `present_status` = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");		
			}
			$this->log->write('----end---');
			//echo 'out';exit;
			//return $query->rows;	
		} else {
			return 0;
		}
		//echo 'out';exit;	
	}

}	

?>