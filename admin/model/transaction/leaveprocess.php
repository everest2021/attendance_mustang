<?php
class ModelTransactionLeaveprocess extends Model {
	
	public function getNextDate($unit) {
		$sql = "SELECT `date` FROM " . DB_PREFIX . "transaction WHERE `day_close_status` = '0' ";
		if($unit){
			$sql .= " AND `unit` = '".$unit."' ";
		}
		$sql .= " ORDER BY `date` ASC LIMIT 0,1";
		$query = $this->db->query($sql);
		if(isset($query->rows[0]['date'])) {
			return $query->rows[0]['date'];	
		} else {
			return 0;
		}
		
	}

	public function getData($date, $unit) {
		$sql = "SELECT * FROM " . DB_PREFIX . "transaction WHERE `date` = '".$date."' AND `abnormal_status` = '1' ";
		if($unit){
			$sql .= " AND `unit` = '".$unit."' ";
		}
		$sql .= " ORDER BY `date` ASC";
		$query = $this->db->query($sql);
		if(isset($query->rows)) {
			return $query->rows;	
		} else {
			return 0;
		}
		
	}

	public function getUnprocessedLeaveTillDate($unit) {
		$sql = "SELECT lt.*, (SELECT e.name FROM `oc_employee` e WHERE e.emp_code = lt.emp_id ) AS ename FROM " . DB_PREFIX . "leave_transaction lt WHERE lt.p_status = '0' AND (lt.a_status = '1' OR lt.a_status = '0') ";
		if($unit){
			$sql .= " AND lt.`unit` = '".$unit."' ";
		}
		$sql .= " ORDER BY `a_status` ASC ";
		$data = $this->db->query($sql);
		if($data->rows) {
			return $data->rows;
		} else {
			return array();
		}

	}

	public function getUnprocessedLeaveTillDate_2($unit) {
		$sql = "SELECT lt.*, (SELECT e.name FROM `oc_employee` e WHERE e.emp_code = lt.emp_id ) AS ename FROM " . DB_PREFIX . "leave_transaction lt WHERE lt.p_status = '0' AND (lt.a_status = '1') ";
		if($unit){
			$sql .= " AND lt.`unit` = '".$unit."' ";
		}
		$data = $this->db->query($sql);
		if($data->rows) {
			return $data->rows;
		} else {
			return array();
		}

	}

	public function getUnprocessedLeaveTillDate_1() {
		$sql = "SELECT lt.*, (SELECT e.name FROM `oc_employee` e WHERE e.emp_code = lt.emp_id ) AS ename FROM " . DB_PREFIX . "leave_transaction lt WHERE lt.p_status = '1' AND lt.a_status = '1' ";
		$data = $this->db->query($sql);
		if($data->rows) {
			return $data->rows;
		} else {
			return array();
		}

	}

	public function is_closed_stat($date, $emp_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "transaction WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."' AND `day_close_status` = '1' ");
		if($unit){
			$sql .= " AND `unit` = '".$unit."' ";
		}
		if($query->num_rows > 0) {
			return 1;	
		} else {
			return 0;
		}
		
	}

	public function update_close_day($date, $unit) {
		$sql = "UPDATE " . DB_PREFIX . "transaction SET `day_close_status` = '1' WHERE `date` = '".$date."' ";
		if($unit){
			$sql .= " AND `unit` = '".$unit."' ";
		}
		$this->db->query($sql);
	}

	public function getAbsent($date, $unit) {
		$sql = "SELECT * FROM " . DB_PREFIX . "transaction WHERE `date` = '".$date."' AND `absent_status` = '1' ";
		if($unit){
			$sql .= " AND `unit` = '".$unit."' ";
		}
		$sql .= " ORDER BY `date` ASC ";
		$query = $this->db->query($sql);
		if(isset($query->rows)) {
			return $query->rows;	
		} else {
			return 0;
		}
	}

	public function checkLeave($date, $emp_id) {
		$query = $this->db->query("SELECT * FROM ".DB_PREFIX."leave_transaction WHERE `emp_id` = '".$emp_id."' AND `date` = '".$date."'");
		if($query->rows) {
			$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `leave_status` = '1', absent_status = '0' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");		
			$this->db->query("UPDATE " . DB_PREFIX . "leave_transaction SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");		
			$this->db->query("UPDATE " . DB_PREFIX . "leave_transaction_temp SET `p_status` = '1' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'");		
			//return $query->rows;	
		} else {
			return 0;
		}	
	}

}	

?>