ALTER TABLE `oc_transaction` CHANGE `present_status_manipulated` `present_status_manipulated` VARCHAR(255) NOT NULL DEFAULT '0';
ALTER TABLE `oc_transaction` CHANGE `absent_status_manipulated` `absent_status_manipulated` VARCHAR(255) NOT NULL DEFAULT '0';
ALTER TABLE `oc_employee` ADD `chunk_id` VARCHAR(255) NOT NULL DEFAULT '0' AFTER `ot_days`;
ALTER TABLE `oc_employee` ADD `location_id` INT(11) NOT NULL DEFAULT '0' AFTER `chunk_id`;
CREATE TABLE `oc_locations` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `oc_locations` ADD PRIMARY KEY (`id`);
ALTER TABLE `oc_locations`  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;