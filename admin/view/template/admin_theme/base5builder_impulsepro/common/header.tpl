<?php

if(isset($this->request->get['route'])){
	$current_location = explode("/", $this->request->get['route']);
	if($current_location[0] == "common"){
		$is_homepage = TRUE;
	}else{
		$is_homepage = FALSE;
	}
}else{
	$is_homepage = FALSE;
}

$get_url = explode("&", $_SERVER['QUERY_STRING']);

$get_route = substr($get_url[0], 6);

$get_route = explode("/", $get_route);

$page_name = array("shoppica2","journal_banner","journal_bgslider","journal_cp","journal_filter","journal_gallery","journal_menu","journal_product_slider","journal_product_tabs","journal_rev_slider","journal_slider");

// array_push($page_name, "EDIT-ME");

if(array_intersect($page_name, $get_route)){
	$is_custom_page = TRUE;
}else{
	$is_custom_page = FALSE;
}

?>
<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" xml:lang="<?php echo $lang; ?>">
<head>
	<meta charset="utf-8" />
	<title><?php echo $title; ?></title>
	<base href="<?php echo $base; ?>" />
	<?php if ($description) { ?>
	<meta name="description" content="<?php echo $description; ?>" />
	<?php } ?>
	<?php if ($keywords) { ?>
	<meta name="keywords" content="<?php echo $keywords; ?>" />
	<?php } ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="" />
	<meta name="author" content="" />
	<?php foreach ($links as $link) { ?>
	<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
	<?php } ?>

	<!-- Le styles -->
	<?php if(isset($this->request->get['route']) && $this->request->get['route'] == 'bill/bill_history/configuremail') { ?>
		<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
	<?php } ?>
	<?php if(!$is_custom_page){ ?>
	<link type="text/css" href="view/stylesheet/admin_theme/base5builder_impulsepro/bootstrap.css" rel="stylesheet" />
	<link type="text/css" href="view/stylesheet/admin_theme/base5builder_impulsepro/style.css" rel="stylesheet" />
	<link type="text/css" href="view/stylesheet/admin_theme/base5builder_impulsepro/bootstrap-responsive.css" rel="stylesheet" />
	<link type="text/css" href="view/stylesheet/admin_theme/base5builder_impulsepro/style-responsive.css" rel="stylesheet" />
	<?php }else{ ?>
	<link rel="stylesheet" type="text/css" href="view/stylesheet/stylesheet.css" />
	<link type="text/css" href="view/stylesheet/admin_theme/base5builder_impulsepro/style-custom-page.css" rel="stylesheet" />
	<?php
}
?>

<?php /*  ?>
<link type="text/css" href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' />
<?php */  ?>

<link type="text/css" href="view/javascript/admin_theme/base5builder_impulsepro/ui/themes/ui-lightness/jquery-ui-1.8.20.custom-min.css" rel="stylesheet" />
	<!--[if IE 7]>
	<link type="text/css" href="view/stylesheet/admin_theme/base5builder_impulsepro/style-ie7.css" rel="stylesheet">
	<![endif]-->
	<!--[if IE 8]>
	<link type="text/css" href="view/stylesheet/admin_theme/base5builder_impulsepro/style-ie8.css" rel="stylesheet">
	<![endif]-->
	<script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/jquery.js"></script>
	<script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/ui/jquery-ui-1.8.20.custom.min.js"></script>
	<script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/tabs.js"></script>
	<script type="text/javascript" src="view/javascript/jquery/ui/external/jquery.cookie.js"></script>
	<?php foreach ($styles as $style) { ?>
	<link rel="<?php echo $style['rel']; ?>" type="text/css" href="<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" />
	<?php } ?>
	<?php foreach ($scripts as $script) { ?>
	<script type="text/javascript" src="<?php echo $script; ?>"></script>
	<?php } ?>
	<?php if($this->user->getUserName() && $is_homepage){ ?>
	<script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/flot/jquery.flot.min.js"></script>
	<script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/flot/jquery.flot.pie.min.js"></script>
	<script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/flot/curvedLines.min.js"></script>
	<script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/flot/jquery.flot.tooltip.min.js"></script>
	<script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/modernizr.js"></script>

		<!--[if lte IE 8]>
		<script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/excanvas.min.js"></script>
		<![endif]-->
		<?php } ?>
	<script type="text/javascript">
		$(document).ready(function(){

		// Signin - Button

		$(".form-signin-body-right input").click(function(){
			$(".form-signin").submit();
		});

		// Signin - Enter Key

		$('.form-signin input').keydown(function(e) {
			if (e.keyCode == 13) {
				$('.form-signin').submit();
			}
		});

	    // Confirm Delete
	    $('#form').submit(function(){
	    	if ($(this).attr('action').indexOf('delete',1) != -1) {
	    		if (!confirm('<?php echo $text_confirm; ?>')) {
	    			return false;
	    		}
	    	}
	    });

		// Confirm Uninstall
		$('a').click(function(){
			if ($(this).attr('href') != null && $(this).attr('href').indexOf('uninstall', 1) != -1) {
				if (!confirm('<?php echo $text_confirm; ?>')) {
					return false;
				}
			}
		});
	});
	</script>

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  	<!--[if lt IE 9]>
  	<script type="text/javascript" src="view/javascript/admin_theme/base5builder_impulsepro/html5shiv.js"></script>
  	<![endif]-->

  	<!-- Fav and touch icons -->
  	<!--
  	<link rel="shortcut icon" href="view/image/admin_theme/base5builder_impulsepro/favicon.png" />-->
</head>

<body>
	<div class="container-fluid">
		<?php if ($logged) { ?>
		<div id="left-column">
			<div class="sidebar-logo" style="margin-left:10px !important;">
				<a href="<?php echo $home; ?>" style="color: #2382e4 !important;">
					<h4><?php echo COMPANY_NAME; ?></h4>
					<?php /* <img src="view/image/admin_theme/base5builder_impulsepro/logo1.png" style="width:150px; height:115px;"/> */ ?>
				</a>
			</div>
			<div id="mainnav">
				<?php if($this->user->getUserGroupId() == 1){ ?>
				<ul class="mainnav">
					<li id="menu-control">
						<div class="menu-control-outer">
							<div class="menu-control-inner">
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</div>
						</div>
					</li>
					
					<?php if($user_dept == 0){ ?>
						<li id="dashboard"><a href="<?php echo $home; ?>" class="top"><?php echo $text_dashboard; ?></a></li>
					<?php } ?>
					
					<?php if($user_dept == 0){ ?>
					<li id="catalog"><a class="top"><?php echo $text_master; ?></a>
						<ul>
							<li><a href="<?php echo $department; ?>"><?php echo "Department"; ?></a></li>
							<li><a href="<?php echo $designation; ?>"><?php echo "Designation"; ?></a></li>
							<li><a href="<?php echo $employee; ?>"><?php echo "Employee"; ?></a></li>
							<?php if($this->user->getUserGroupId() == 1){ ?>
								<!-- <li><a href="<?php echo $holiday; ?>"><?php echo "Holiday"; ?></a></li> -->
								<li><a href="<?php echo $location; ?>"><?php echo "Location"; ?></a></li>
								<li style="display: none;"><a href="<?php echo $shift; ?>"><?php echo "Shift"; ?></a></li>
							<?php } ?>
							<?php /* ?>
							<li style="display:none;"><a href="<?php echo $grade; ?>"><?php echo "Grade"; ?></a></li>
							<li style="display:none;"><a href="<?php echo $week; ?>"><?php echo "Week Off"; ?></a></li>
							<li style="display:none;"><a href="<?php echo $shift_schedule; ?>"><?php echo "Shift Schedule"; ?></a></li>
							<li style="display:none;"><a href="<?php echo $halfday; ?>"><?php echo "Half Day"; ?></a></li>
							<li style="display:none;"><a href="<?php echo $complimentary; ?>"><?php echo "Compensatory Off"; ?></a></li>
							<?php */ ?>
						</ul>
					</li>
					<?php } ?>	
					
					<?php if($this->user->getUserGroupId() == 1){ ?>
						<li id="extension"><a class="top"><?php echo $text_transaction; ?></a>
							<ul>
								<li><a href="<?php echo $manualpunch_daily; ?>"><?php echo 'Manual Punch Daily'; ?></a></li>
							<?php /* ?>
								<li style="display:none;"><a href="<?php echo $manualpunch; ?>"><?php echo 'Manual Punch'; ?></a></li>
							<?php */ ?>
							</ul>
						</li>
					<?php } ?>

					<?php if($user_dept == 0){ ?>
					<li id="extension"><a class="top"><?php echo "Reports"; ?></a>
						<ul>
							<?php if($this->user->getUserGroupId() == 1){ ?>
								<?php /* ?>
								<!-- <li><a style="display: none;" href="<?php echo $today_report; ?>"><?php echo "Today's Attendance"; ?></a></li>
								<li><a style="display: none;" href="<?php echo $attendance_report; ?>"><?php echo 'Daily Attendance'; ?></a></li>
								<li><a style="display: none;" href="<?php echo $performance_report; ?>"><?php echo 'Periodic Attendance'; ?></a></li>
								<li><a style="display: none;" href="<?php echo $dailyattendance_report; ?>"><?php echo 'Daily Attendance Report'; ?></a></li> -->
								<?php */ ?>
							<?php } ?>
							<li><a href="<?php echo $dailysummary_report; ?>"><?php echo 'Daily Summary'; ?></a></li>
							<li><a href="<?php echo $performance_report; ?>"><?php echo 'Periodic Attendance'; ?></a></li>
							<?php /* ?>
							<li style="display: none;"><a href="<?php echo $wages_report; ?>"><?php echo 'Wages Report Daily'; ?></a></li>
							<li style="display: none;"><a href="<?php echo $wages_report_muster; ?>"><?php echo 'Wages Report Muster'; ?></a></li>
							<li style="display: none;"><a href="<?php echo $slipdaily; ?>"><?php echo 'Salary Slip Daily'; ?></a></li>
							<li style="display: none;"><a href="<?php echo $slipmuster; ?>"><?php echo 'Salary Slip Muster'; ?></a></li>
							<li style="display: none;"><a href="<?php echo $leave_report_perday; ?>"><?php echo 'Leave Report Per Day'; ?></a></li>
							<li style="display: none;"><a href="<?php echo $leave_report_muster; ?>"><?php echo 'Leave Report Muster'; ?></a></li>
							<li style="display: none;"><a href="<?php echo $bonus_report_perday; ?>"><?php echo 'Bonus Report Per Day'; ?></a></li>
							<li style="display: none;"><a href="<?php echo $bonus_report_muster; ?>"><?php echo 'Bonus Report Muster'; ?></a></li>
							<li style="display:none;"><a href="<?php echo $deficit_report; ?>"><?php echo 'Deficit Report'; ?></a></li>
							<li style="display:none;"><a href="<?php echo $muster; ?>"><?php echo 'Muster'; ?></a></li>
							<li style="display:none;"><a href="<?php echo $lta; ?>"><?php echo 'LTA Register'; ?></a></li>
							<?php */ ?>
						</ul>
					</li>
					<?php } ?>

					<?php if($user_dept == 0){ ?>
						<li id="help"><a class="top"><?php echo $text_utility; ?></a>
							<ul>
								<li><a href="<?php echo $dataprocess_empty; ?>"><?php echo 'Generate Empty Attendance Data'; ?></a></li>
								<?php if($this->user->getUserGroupId() == 1){ ?>
								<li><a href="<?php echo $import_attendance_daily; ?>"><?php echo ' Employees Update Attendance'; ?></a></li>
								<li style="	"><a href="<?php echo $import_attendance_daily2; ?>"><?php echo ' Employees Update Attendance 2'; ?></a></li>
								<li><a href="<?php echo $user; ?>"><?php echo 'User'; ?></a></li>
								<li><a href="<?php //echo $resetprocess; ?>"><?php echo 'Reset Process'; ?></a></li>
								<?php } ?>
							</ul>
						</li>
					<?php } ?>
				</ul>
				</div>
				<?php } elseif($this->user->getUserGroupId() == 11) { ?>
				<ul class="mainnav">
					<li id="menu-control">
						<div class="menu-control-outer">
							<div class="menu-control-inner">
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</div>
						</div>
					</li>
					
					<?php if($user_dept == 0){ ?>
						<li id="dashboard"><a href="<?php echo $home; ?>" class="top"><?php echo $text_dashboard; ?></a></li>
					<?php } ?>
					
					<?php if($user_dept == 0){ ?>
					<li id="catalog"><a class="top"><?php echo $text_master; ?></a>
						<ul>
							<li><a href="<?php echo $department; ?>"><?php echo "Department"; ?></a></li>
							<li><a href="<?php echo $designation; ?>"><?php echo "Designation"; ?></a></li>
							<li><a href="<?php echo $employee; ?>"><?php echo "Employee"; ?></a></li>
							<?php if($this->user->getUserGroupId() == 1){ ?>
								<li><a href="<?php echo $holiday; ?>"><?php echo "Holiday"; ?></a></li>
								<li><a href="<?php echo $location; ?>"><?php echo "Location"; ?></a></li>
							<?php } ?>
						</ul>
					</li>
					<?php } ?>	
					
					<li id="extension"><a class="top"><?php echo $text_transaction; ?></a>
						<ul>
							<li><a href="<?php echo $manualpunch_daily; ?>"><?php echo 'Manual Punch Daily'; ?></a></li>
						<?php /* ?>
							<li style="display:none;"><a href="<?php echo $manualpunch; ?>"><?php echo 'Manual Punch'; ?></a></li>
						<?php */ ?>
						</ul>
					</li>

					<?php if($user_dept == 0){ ?>
					<li id="extension"><a class="top"><?php echo "Reports"; ?></a>
						<ul>
							<li><a href="<?php echo $dailysummary_report; ?>"><?php echo 'Daily Summary'; ?></a></li>
						</ul>
					</li>
					<?php } ?>

					<?php if($user_dept == 0){ ?>
						<li id="help"><a class="top"><?php echo $text_utility; ?></a>
							<ul>
								<li><a href="<?php echo $import_attendance_daily; ?>"><?php echo ' Employees Update Attendance'; ?></a></li>
							</ul>
						</li>
					<?php } ?>
				</ul>
			</div>
		
			<?php } elseif($this->user->getUserGroupId() == 10) { ?>
				<ul class="mainnav">
					<li id="menu-control">
						<div class="menu-control-outer">
							<div class="menu-control-inner">
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</div>
						</div>
					</li>
					
					<?php if($user_dept == 0){ ?>
						<li id="dashboard"><a href="<?php echo $home; ?>" class="top"><?php echo $text_dashboard; ?></a></li>
					<?php } ?>
					
					<?php if($user_dept == 0){ ?>
					<li id="catalog"><a class="top"><?php echo $text_master; ?></a>
						<ul>
							<li><a href="<?php echo $department; ?>"><?php echo "Department"; ?></a></li>
							<li><a href="<?php echo $designation; ?>"><?php echo "Designation"; ?></a></li>
							<li><a href="<?php echo $employee; ?>"><?php echo "Employee"; ?></a></li>
							<?php if($this->user->getUserGroupId() == 1){ ?>
								<li><a href="<?php echo $holiday; ?>"><?php echo "Holiday"; ?></a></li>
								<li><a href="<?php echo $location; ?>"><?php echo "Location"; ?></a></li>
							<?php } ?>
						</ul>
					</li>
					<?php } ?>	

					<?php if($user_dept == 0){ ?>
					<li id="extension"><a class="top"><?php echo "Reports"; ?></a>
						<ul>
							<li><a href="<?php echo $dailysummary_report; ?>"><?php echo 'Daily Summary'; ?></a></li>
						</ul>
					</li>
					<?php } ?>
				</ul>
			</div>
		<?php } ?>
			<?php /* ?>
			<div class="sidebar copyright" style="display:none;">
				<div class="sidebar-base5builder">
					<p>ImpulsePro Admin Template By <a href="http://base5builder.com/" target="_blank">Base5Builder.com</a>. Built with <a href="http://getbootstrap.com/" target="_blank">Bootstap</a> v2.3.2. <br />Icons by: <a href="http://iconsweets2.com/" target="_blank">iconSweets2</a></p>
				</div>
				<div class="sidebar-opencart"><?php echo $text_footer; ?></div>
			</div>
			<?php */ ?>
		</div>
		<div class="right-header-content clearfix">
			<div class="secondary-menu">
				<ul>
					<li id="store" style="display:none;">
						<a class="top"><span><?php echo $text_front; ?></span></a>
						<ul>
							<li><a  href="<?php echo $store; ?>" target="_blank" class="top"><?php echo $this->config->get('config_name'); ?></a></li>
							<?php foreach ($stores as $stores) { ?>
							<li><a href="<?php echo $stores['href']; ?>" target="_blank"><?php echo $stores['name']; ?></a></li>
							<?php } ?>
						</ul>
					</li>
					<li id="logout"><a class="top" href="<?php echo $logout; ?>"><span><?php echo $text_logout; ?></span></a></li>
				</ul>
			</div>
			<div class="admin-info"><?php echo $logged; ?></div>
		</div>
		<?php } ?>
