<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
</head>
<body>
  <div class="box" id="content";>
  <div style="page-break-after: always;">
    <h1 style="text-align:center;">
      <?php echo 'Daily Attendance Report (Summary Report)'; ?><br />
      <table class="product" style="border:hidden;">
        <tr>
          <td colspan="11" class="center" style="text-align: center;">
            <p style="display:inline;font-size:15px;">
              <?php $date=date_create($filter_date_start);    echo date_format($date,"M d Y ");?> To <?php $date=date_create($filter_date_end);  echo date_format($date,"M d Y ");?>
            </p>
          </td>
        </tr>
        <tr>
          <td colspan="5" class="left">
            <p style="font-size:15px;"><b >Company : </b><?php echo COMPANY_NAME; ?></p>
          </td>
          <td colspan="6" class="right">
            <p style="font-size:15px;"><b >Printed On :<?php echo date("M d Y") ," ". date("h:i") ;  ?></p>
         </td>
        </tr>
      </table>
    </h1>
    <?php if($dailyreports){ ?>
      <?php foreach($dailyreports as $dkeysss => $dvaluesss){ ?>
        <table class="product">
          <tr>
            <td class="left" colspan="2">
              <b><?php echo 'Employee Code:'; ?></b><?php echo $dvaluesss['summary_data']['emp_code']; ?>
            </td>
            <td class="left" colspan="2">
              <b><?php echo 'Employee Name:'; ?></b><?php echo $dvaluesss['summary_data']['emp_name']; ?>
            </td>
            <td class="left" colspan="2">
              <b><?php echo 'Department:'; ?></b><?php echo $dvaluesss['summary_data']['department']; ?>
            </td>
            <td class="left" colspan="2">
              <b><?php echo 'Location:'; ?></b><?php echo $dvaluesss['summary_data']['unit']; ?>
            </td>
            <td class="left" colspan="2">
              <b><?php echo 'Designation:'; ?></b><?php echo $dvaluesss['summary_data']['designation']; ?>
            </td>
          </tr>
        </table>
        <table  class="product">
          <thead>
            <tr>
              <td class="left"><?php echo 'Date'; ?></td>
              <td class="left"><?php echo 'InTime'; ?></td>
              <td class="left"><?php echo 'OutTime'; ?></td>
              <td class="left"><?php echo 'Shift'; ?></td>
              <td class="left"><?php echo 'Total Duration'; ?></td>
              <td class="left"><?php echo 'Total Duration Without Lunch'; ?></td>
              <td class="left"><?php echo 'Early Time'; ?></td>
              <td class="left"><?php echo 'Late Time'; ?></td>
              <td class="left"><?php echo 'Over Time'; ?></td>
              <td class="left"><?php echo 'Status'; ?></td>
              <td class="left"><?php echo 'Remarks'; ?></td>
            </tr>
          </thead>
          <tbody>
          <?php if ($dailyreports) { ?>
            <?php $i = 1; ?>
            <?php foreach ($dvaluesss['trans_data'] as $tkeys => $result) { ?>
              <tr>
                <td class="text-left"><?php echo $result['date']; ?></td>
                <td class="text-left"><?php echo $result['act_intime']; ?></td>
                <td class="text-left"><?php echo $result['act_outtime']; ?></td>
                <td class="text-left"><?php echo $result['shift_name']; ?> </td>
                <td class="text-left"><?php echo $result['working_time']; ?></td>
                <td class="text-left"><?php echo $result['working_time_lunch']; ?></td>
                <td class="text-left"><?php echo $result['early_time']; ?></td>
                <td class="text-left"><?php echo $result['late_time']; ?></td>
                <td class="text-left"><?php echo $result['overtime']; ?></td>
                <td class="text-left"><?php echo $result['status_name']; ?></td>
                <td></td>
              </tr>
            <?php } ?>
            <?php } ?>
          </tbody>
        </table>
        <table class="product" style="border-bottom: 2px solid #000000;">
          <tr>
            <td colspan="11">
              <b>
                <?php echo 'Total Duration:'; ?> <?php echo $dvaluesss['summary_data']['total_working_hours']  ,' Hrs '; ?><?php echo $dvaluesss['summary_data']['total_working_minutes'],' Min ' ;  ?>
                <?php echo 'Present Days:'; ?> <?php echo $dvaluesss['summary_data']['countpresent']; ?>
                <?php echo 'Leaves:'; ?> <?php echo $dvaluesss['summary_data']['countleave']; ?>
                <?php echo 'Holidays:'; ?> <?php echo $dvaluesss['summary_data']['countholiday']; ?>
                <?php echo 'Absent Days:'; ?> <?php echo $dvaluesss['summary_data']['countabsent']; ?>
                <?php echo 'Weekly Off:'; ?> <?php echo $dvaluesss['summary_data']['countoff']; ?>
              </b>
            </td>
          </tr>
          <tr>
            <td colspan="11">
              <b>
                <?php echo 'Total Duration Without Lunch:'; ?> <?php echo $dvaluesss['summary_data']['lunch_total_working_hours']  ,' Hrs '; ?><?php echo $dvaluesss['summary_data']['lunch_total_working_minutes'],' Min ' ;  ?>
              </b>
            </td>
          </tr>
        </table>
      <?php } ?>
    <?php } ?>
  </div>
  </div>
</body>
</html>