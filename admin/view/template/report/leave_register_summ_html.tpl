<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
</head>
<body style="width:100%;">
<?php //foreach($final_datass as $fkeyss => $final_datas) { ?>
<div style="page-break-after: always;">
  <h1 style="text-align:center;">
    <?php echo 'RWITC,MUMBAI'; ?><br /><?php echo 'LEAVE REGISTER'; ?><br />
    <p style="display:inline;font-size:15px;"><?php echo 'Period : '. $filter_date_start . ' - ' . $filter_date_end; ?></p>
  </h1>
  <table class="product" style="width:100% !important;">
    <tbody>
      <?php if($final_datas) { ?>
        <?php $i = 1; ?>
        <?php foreach($final_datas as $final_data) { ?>
          <tr>
            <td class="left" style="width:9%;font-weight:bold;font-size:12px;">From Date</td>
            <td class="left" style="width:9%;font-weight:bold;font-size:12px;">To Date</td>
            <td class="left" style="width:9%;font-weight:bold;font-size:12px;">Break</td>
            <td class="left" colspan="7" style="text-align:center;font-weight:bold;font-size:12px;">Leave Detail</td>
          </tr>
          <tr style="font-weight:bold;font-size:11px;">
            <td colspan = "10">
              <?php echo 'Employee Code & Name : ' . $final_data['basic_data']['emp_code']; ?> &nbsp;&nbsp;&nbsp; <?php echo $final_data['basic_data']['name']; ?> 
            </td>
          </tr>
          <tr>
            <td colspan="3">
            </td>
            <td style="padding: 0px 9px;font-weight:bold;font-size:12px;width: 7%;text-align: right;">
              BL
            </td>
            <td style="padding: 0px 9px;font-weight:bold;font-size:12px;width: 7%;text-align: right;">
              PL
            </td>
            <td style="padding: 0px 9px;font-weight:bold;font-size:12px;width: 7%;text-align: right;">
              SL
            </td>
            <td style="padding: 0px 9px;font-weight:bold;font-size:12px;width: 7%;text-align: right;">
              ML
            </td>
            <td style="padding: 0px 9px;font-weight:bold;font-size:12px;width: 7%;text-align: right;">
              MAL
            </td>
            <td style="padding: 0px 9px;font-weight:bold;font-size:12px;width: 7%;text-align: right;">
              PAL
            </td>
            <td style="padding: 0px 9px;font-weight:bold;font-size:12px;width: 7%;text-align: right;">
              LWP
            </td>
          </tr>
          <tr>
            <td class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
              <?php echo 'opening Bal'; ?>
            </td>
            <td style="padding: 0px 9px;font-weight:bold;font-size:12px;">
              <?php echo $final_data['basic_data']['open_date']; ?>
            </td>
            <td>
            </td>
            <td class="right" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
              <?php echo $final_data['basic_data']['bl_open']; ?>
            </td>
            <td class="right" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
              <?php echo $final_data['basic_data']['pl_open']; ?>
            </td>
            <td class="right" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
              <?php echo $final_data['basic_data']['sl_open']; ?>
            </td>
            <td class="right" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
              <?php echo $final_data['basic_data']['ml_open']; ?>
            </td>
            <td class="right" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
              <?php echo $final_data['basic_data']['mal_open']; ?>
            </td>
            <td class="right" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
              <?php echo $final_data['basic_data']['pal_open']; ?>
            </td>
            <td class="right" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
              <?php echo $final_data['basic_data']['lwp_open']; ?>
            </td>
          </tr>
          <tr>
            <td colspan="3" class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
              <?php echo 'Consumed Leave'; ?>
            </td>
            <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
              <?php echo $final_data['basic_data']['total_days_bl']; ?>
            </td>
            <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
              <?php echo $final_data['basic_data']['total_days_pl']; ?>
            </td>
            <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
              <?php echo $final_data['basic_data']['total_days_sl']; ?>
            </td>
            <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
              <?php echo $final_data['basic_data']['total_days_ml']; ?>
            </td>
            <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
              <?php echo $final_data['basic_data']['total_days_mal']; ?>
            </td>
            <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
              <?php echo $final_data['basic_data']['total_days_pal']; ?>
            </td>
            <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
              <?php echo $final_data['basic_data']['total_days_lwp']; ?>
            </td>
          </tr>
          <tr>
            <td colspan="3" class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
              <?php echo 'Closing Balance'; ?>
            </td>
            <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
              <?php echo $final_data['basic_data']['closing_bl']; ?>
            </td>
            <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
              <?php echo $final_data['basic_data']['closing_pl']; ?>
            </td>
            <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
              <?php echo $final_data['basic_data']['closing_sl']; ?>
            </td>
            <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
              <?php echo $final_data['basic_data']['closing_ml']; ?>
            </td>
            <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
              <?php echo $final_data['basic_data']['closing_mal']; ?>
            </td>
            <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
              <?php echo $final_data['basic_data']['closing_pal']; ?>
            </td>
            <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
              <?php echo $final_data['basic_data']['closing_lwp']; ?>
            </td>
          </tr>
          <?php if($filter_year == date('Y')) { ?>
            <tr>
              <td colspan="3" class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
                <?php echo 'UnProcessed Leave'; ?>
              </td>
              <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                <?php echo $final_data['basic_data']['un_total_days_bl']; ?>
              </td>
              <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                <?php echo $final_data['basic_data']['un_total_days_pl']; ?>
              </td>
              <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                <?php echo $final_data['basic_data']['un_total_days_sl']; ?>
              </td>
              <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                <?php echo $final_data['basic_data']['un_total_days_ml']; ?>
              </td>
              <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                <?php echo $final_data['basic_data']['un_total_days_mal']; ?>
              </td>
              <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                <?php echo $final_data['basic_data']['un_total_days_pal']; ?>
              </td>
              <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                <?php echo $final_data['basic_data']['un_total_days_lwp']; ?>
              </td>
            </tr>
            <tr>
              <td colspan="3" class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
                <?php echo 'UnProcessed Balance'; ?>
              </td>
              <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                <?php echo $final_data['basic_data']['un_total_bal_bl']; ?>
              </td>
              <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                <?php echo $final_data['basic_data']['un_total_bal_pl']; ?>
              </td>
              <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                <?php echo $final_data['basic_data']['un_total_bal_sl']; ?>
              </td>
              <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                <?php echo $final_data['basic_data']['un_total_bal_ml']; ?>
              </td>
              <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                <?php echo $final_data['basic_data']['un_total_bal_mal']; ?>
              </td>
              <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                <?php echo $final_data['basic_data']['un_total_bal_pal']; ?>
              </td>
              <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                <?php echo $final_data['basic_data']['un_total_bal_lwp']; ?>
              </td>
            </tr>
          <?php } ?>
          <tr style="border-bottom:2px solid black;">
            <td colspan = "10" >
            </td>
          </tr>
          <?php $i++; ?>
        <?php } ?>
      <?php } ?>
    </tbody>
  </table>
</div>
<?php //} ?>
</body>
</html>