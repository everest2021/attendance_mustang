  <?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
</head>
<body>
<div class="box" id="content">
  <div  style="page-break-after: always;">
    <h1 style="text-align:center;">
      <?php echo 'SPECTRUM SERVICES.'; ?><br />
      <?php echo 'Register Wages'; ?><br />
    </h1>
    <table class="product" style="border:0px solid #ffffff !important;">
      <tr>
        <td colspan="12" style="text-align: left;width: 50%;border:0px solid #ffffff !important;">
          <p style="font-size:12px;"><b>Name and address of Contractor: </b><?php echo 'PUSHPENDRA PANDEY'; ?></p>
        </td>
        <td colspan="12" style="text-align: left;width: 50%;border:0px solid #ffffff !important;">
          <p style="font-size:12px;"><b>Name and Address of Establishment/Under which Contract is carried on SPECTRUM SERVICES</b></p>
       </td>
      </tr>
      <tr>
        <td colspan="12" style="text-align: left;width: 50%;border:0px solid #ffffff !important;">
          <p style="font-size:12px;"><b>Nature of Location of Work - </b>Vasai</p>
        </td>
        <td colspan="12" style="text-align: left;width: 50%;border:0px solid #ffffff !important;">
          <p style="font-size:12px;"><b>Name and Address of Principal Employeer-Spectrum Scan</b></p>
       </td>
      </tr>
      <tr>
        <td colspan="12" style="text-align: left;width: 50%;border:0px solid #ffffff !important;">
          <p style="font-size:12px;"><b>For the Month of  - </b><?php echo $month ?></p>
        </td>
        <td colspan="12" style="text-align: left;width: 50%;border:0px solid #ffffff !important;">
          <p style="font-size:12px;"><b>Wage Period - </b>Monthly</p>
       </td>
      </tr>
    </table>
    <table class="product">
      <thead>
        <tr>
          <td class="left" style="text-align: left;"><?php echo 'Sr. No'; ?></td>
          <td class="left" style="text-align: left;"><?php echo 'Emp Code'; ?></td>
          <!-- <td class="left"><?php echo 'Designation'; ?></td> -->
          <td class="left" style="text-align: left;"><?php echo 'Joining Date'; ?></td>
          <!-- <td class="left"><?php echo 'Department'; ?></td> -->
          <!-- <td class="left"><?php echo 'Esic No'; ?></td> -->
          <!-- <td class="left"><?php echo 'PF No'; ?></td> -->
          <td class="left" style="text-align: left;"><?php echo 'Name'; ?></td>
          <td class="right" style="text-align: right;"><?php echo 'Basic'; ?></td>
          <td class="right" style="text-align: right;"><?php echo 'Incentive'; ?></td>
          <td class="right" style="text-align: right;"><?php echo 'Total'; ?></td>
          <td class="right" style="text-align: right;"><?php echo 'Absent'; ?></td>
          <td class="right" style="text-align: right;"><?php echo 'PL'; ?></td>
          <td class="right" style="text-align: right;"><?php echo 'Total'; ?></td>
          <td class="right" style="text-align: right;"><?php echo 'Earned Basic'; ?></td>
          <td class="right" style="text-align: right;"><?php echo 'HRA'; ?></td>
          <td class="right" style="text-align: right;"><?php echo 'Allowance'; ?></td>
          <td class="right" style="text-align: right;"><?php echo 'Gross Earned'; ?></td>
          <td class="right" style="text-align: right;"><?php echo 'PA'; ?></td>
          <td class="right" style="text-align: right;"><?php echo 'Con/Tea/Snacks'; ?></td>
          <td class="right" style="text-align: right;"><?php echo 'OT'; ?></td>
          <td class="right" style="text-align: right;"><?php echo 'OT Amount'; ?></td>
          <td class="right" style="text-align: right;"><?php echo 'Gross'; ?></td>
          <td class="right" style="text-align: right;"><?php echo 'ESIC'; ?></td>
          <td class="right" style="text-align: right;"><?php echo 'P.F'; ?></td>
          <td class="right" style="text-align: right;"><?php echo 'P.T'; ?></td>
          <td class="right" style="text-align: right;"><?php echo 'Net Paid'; ?></td>
        </tr>
      </thead>
      <tbody>
        <?php if ($dailyreports) { ?>
          <?php 
            $i = 1; 
            $total_fixed_basic = 0;
            $total_incentive = 0;
            $total_total = 0;
            $total_days_absent = 0;
            $total_days_leave = 0;
            $total_total_days_present = 0;
            $total_earned_basic = 0;
            $total_hra = 0;
            $total_other_allowance = 0;
            $total_pa = 0;
            $total_tea_snacks = 0;
            $total_gross_earned = 0;
            $total_calculated_overtime = 0;
            $total_ot_amount = 0;
            $total_gross = 0;
            $total_esic = 0;
            $total_pf = 0;
            $total_pt = 0;
            $total_net_paid = 0;
          ?>
          <?php foreach ($dailyreports as $result) { ?>
            <tr>
              <td class="text-left" style="text-align: left;"><?php echo $i; ?></td>
              <td class="text-left" style="text-align: left;"><?php echo $result['emp_code']; ?></td>
              <!-- <td class="text-left"><?php //echo $result['designation']; ?></td> -->
              <td class="text-left" style="text-align: left;"><?php echo $result['doj']; ?></td>
              <!-- <td class="text-left"><?php //echo $result['department']; ?> </td> -->
              <!-- <td class="text-left"><?php //echo $result['esic_no']; ?></td> -->
              <!-- <td class="text-left"><?php //echo $result['pf_no']; ?></td> -->
              <td class="text-left" style="text-align: left;"><?php echo $result['emp_name']; ?></td>
              <td class="text-right" style="text-align: right;"><?php echo $result['fixed_basic']; ?></td>
              <td class="text-right" style="text-align: right;"><?php echo $result['incentive']; ?></td>
              <td class="text-right" style="text-align: right;"><?php echo $result['total']; ?></td>
              <td class="text-right" style="text-align: right;"><?php echo $result['days_absent']; ?></td>
              <td class="text-right" style="text-align: right;"><?php echo $result['days_leave']; ?></td>
              <td class="text-right" style="text-align: right;"><?php echo $result['total_days_present']; ?></td>
              <td class="text-right" style="text-align: right;"><?php echo $result['earned_basic']; ?></td>
              <td class="text-right" style="text-align: right;"><?php echo $result['hra']; ?></td>
              <td class="text-right" style="text-align: right;"><?php echo $result['other_allowance']; ?></td>
              <td class="text-right" style="text-align: right;"><?php echo $result['gross_earned']; ?></td>
              <td class="text-right" style="text-align: right;"><?php echo $result['pa']; ?></td>
              <td class="text-right" style="text-align: right;"><?php echo $result['tea_snacks']; ?></td>
              <td class="text-right" style="text-align: right;"><?php echo $result['calculated_overtime']; ?></td>
              <td class="text-right" style="text-align: right;"><?php echo $result['ot_amount']; ?></td>
              <td class="text-right" style="text-align: right;"><?php echo $result['gross']; ?></td>
              <td class="text-right" style="text-align: right;"><?php echo $result['esic']; ?></td>
              <td class="text-right" style="text-align: right;"><?php echo $result['pf']; ?></td>
              <td class="text-right" style="text-align: right;"><?php echo $result['pt']; ?></td>
              <td class="text-right" style="text-align: right;"><?php echo $result['net_paid']; ?></td>
            </tr>
            <?php 
              $i ++; 
              $total_fixed_basic += $result['fixed_basic'];
              $total_incentive += $result['incentive'];
              $total_total += $result['total'];
              $total_days_absent += $result['days_absent'];
              $total_days_leave += $result['days_leave'];
              $total_total_days_present += $result['total_days_present'];
              $total_earned_basic += $result['earned_basic'];
              $total_hra += $result['hra'];
              $total_other_allowance += $result['other_allowance'];
              $total_pa += $result['pa'];
              $total_tea_snacks += $result['tea_snacks'];
              $total_gross_earned += $result['gross_earned'];
              $total_calculated_overtime += $result['calculated_overtime'];
              $total_ot_amount += $result['ot_amount'];
              $total_gross += $result['gross'];
              $total_esic += $result['esic'];
              $total_pf += $result['pf'];
              $total_pt += $result['pt'];
              $total_net_paid += $result['net_paid'];
            ?>
          <?php } ?>
          <tr>
            <td class="text-left">&nbsp;</td>
            <td class="text-left">&nbsp;</td>
            <td class="text-left">&nbsp;</td>
            <td class="text-left" style="text-align: right;font-weight: bold;">Total</td>
            <td class="text-right" style="text-align: right;font-weight: bold;"><?php echo $total_fixed_basic; ?></td>
            <td class="text-right" style="text-align: right;font-weight: bold;"><?php echo $total_incentive; ?></td>
            <td class="text-right" style="text-align: right;font-weight: bold;"><?php echo $total_total; ?></td>
            <td class="text-right" style="text-align: right;font-weight: bold;"><?php echo $total_days_absent; ?></td>
            <td class="text-right" style="text-align: right;font-weight: bold;"><?php echo $total_days_leave; ?></td>
            <td class="text-right" style="text-align: right;font-weight: bold;"><?php echo $total_total_days_present; ?></td>
            <td class="text-right" style="text-align: right;font-weight: bold;"><?php echo $total_earned_basic; ?></td>
            <td class="text-right" style="text-align: right;font-weight: bold;"><?php echo $total_hra; ?></td>
            <td class="text-right" style="text-align: right;font-weight: bold;"><?php echo $total_other_allowance; ?></td>
            <td class="text-right" style="text-align: right;font-weight: bold;"><?php echo $total_gross_earned; ?></td>
            <td class="text-right" style="text-align: right;font-weight: bold;"><?php echo $total_pa; ?></td>
            <td class="text-right" style="text-align: right;font-weight: bold;"><?php echo $total_tea_snacks; ?></td>
            <td class="text-right" style="text-align: right;font-weight: bold;"><?php echo $total_calculated_overtime; ?></td>
            <td class="text-right" style="text-align: right;font-weight: bold;"><?php echo $total_ot_amount; ?></td>
            <td class="text-right" style="text-align: right;font-weight: bold;"><?php echo $total_gross; ?></td>
            <td class="text-right" style="text-align: right;font-weight: bold;"><?php echo $total_esic; ?></td>
            <td class="text-right" style="text-align: right;font-weight: bold;"><?php echo $total_pf; ?></td>
            <td class="text-right" style="text-align: right;font-weight: bold;"><?php echo $total_pt; ?></td>
            <td class="text-right" style="text-align: right;font-weight: bold;"><?php echo $total_net_paid; ?></td>
          </tr>
        <?php } ?>
      </tbody>
    </table>
    <table class="product" style="border:0px solid #ffffff !important;">
      <tr>
        <td colspan="24" style="text-align: left;width: 100%;border:0px solid #ffffff !important;">
          <p style="font-size:12px;"><b>Certified that Payment of Wages has been made in the presense of an authorised representative of the Principal Employeer</b></p>
        </td>
      </tr>
      <tr>
        <td colspan="24" style="text-align: left;width: 100%;border:0px solid #ffffff !important;">
          <p style="font-size:12px;"><b>Date : <?php echo date('d-m-Y'); ?> </b></p>
        </td>
      </tr>
      <tr>
        <td colspan="24" style="text-align: left;width: 100%;border:0px solid #ffffff !important;">
          <p style="font-size:12px;"><b>Place : &nbsp;&nbsp;&nbsp;</b></p>
        </td>
      </tr>
    </table>
  </div>
</div></body></html>