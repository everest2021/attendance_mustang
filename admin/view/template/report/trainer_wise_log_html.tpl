<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
</head>
<body>
<div style="page-break-after: always;">
  <p style="display:inline;font-size:15px;"><?php echo 'Date : '; ?><b><?php echo $tdate; ?></b><p>
  <h2 style="text-align:left;">
    <?php echo $title; ?>
  </h2>
  <table class="product">
    <tr class="heading">
      <td class="left"><?php echo $column_trainer; ?></td>
      <td class="left"><?php echo $column_horse_name; ?></td>
      <td class="left"><?php echo $column_medicine_name; ?></td>
    </tr>
    <?php if ($final_datas) { ?>
      <?php foreach ($final_datas as $trainer_data) { ?>
        <tr>
          <td class="left" style="border:none !important;padding-bottom:15px;"><?php echo $trainer_data['trainer_name']; ?></td>
        </tr>
        <?php $cnt = 0; ?>
        <?php $cnt_log = count($trainer_data['horse_data']); ?>
        <?php foreach($trainer_data['horse_data'] as $hkey => $hvalue) { ?>
          <?php $cnt ++; ?>
          <?php if($cnt == $cnt_log) { ?>
            <tr style="border-bottom:1px solid;">
          <?php } else { ?>
            <tr style="border-bottom:1px dotted;">
          <?php } ?>
          <td class="left" style="border:none !important;padding-bottom:15px;"></td>
          <td class="left" style="border:none !important;padding-bottom:15px;"><?php echo $hvalue['horse_name']; ?></td>
          <td class="left" style="border:none !important;padding-bottom:15px;">
            <?php foreach($hvalue['medicine_data'] as $mkey => $mvalue) { ?>
            <?php echo $mvalue['medicine_name']; ?><br />
            <?php } ?>
          </td>
        </tr>
        <?php } ?>
      <?php } ?>
    <?php } else { ?>
    <tr>
      <td class="center" colspan="3"><?php echo $text_no_results; ?></td>
    </tr>
    <?php } ?>
  </table>
</div>
</body>
</html>