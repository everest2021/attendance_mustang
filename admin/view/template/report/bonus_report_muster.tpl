<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/report.png" alt="" /> <?php echo $heading_title; ?></h1>
    </div>
    <div class="content sales-report">
      <table class="form">
        <tr>
          <td style="width:13%;display: none;"><?php echo "Name"; ?>
            <input type="text" name="filter_name" value="<?php echo $filter_name;  ?>" id="filter_name" size="25" />
            <input type="hidden" name="filter_name_id" value="<?php echo $filter_name_id; ?>" id="filter_name_id" />
          </td>
          <td style="width:8%;display: none;"><?php echo "Date Start"; ?>
            <input type="text" name="filter_date_start" value="<?php echo $filter_date_start; ?>" id="date-start" size="10" />
          </td>
          <td style="width:8%;display: none;"><?php echo "Date End"; ?>
            <input type="text" name="filter_date_end" value="<?php echo $filter_date_end; ?>" id="date-end" size="10" />
          </td>
          <td style="width:8%;display: none;">Year
            <select name="filter_year">
              <?php foreach($years as $key => $ud) { ?>
                <?php if($key == $filter_year) { ?>
                  <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                <?php } else { ?>
                  <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </td>
          <td style="width:8%;display: none;">Unit
            <select name="filter_unit">
              <?php foreach($unit_data as $key => $ud) { ?>
                <?php if($key == $filter_unit) { ?>
                  <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                <?php } else { ?>
                  <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </td>
          <td style="width:8%;display: none;"><?php echo "Gender"; ?>
            <select name="filter_gender" id="filter_gender">
              <?php foreach($genders as $gkey => $gvalue){ ?>
                <?php if($gkey == $filter_gender){ ?>
                  <option value="<?php echo $gkey ?>" selected="selected"><?php echo $gvalue ?></option>
                <?php } else { ?>
                  <option value="<?php echo $gkey ?>"><?php echo $gvalue ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </td>
          <td style="text-align: right;">
            <a style="padding: 13px 25px;display: none;" onclick="filter();" id="filter" class="button"><?php echo $button_filter; ?></a>
            <a style="padding: 13px 25px;" href="<?php echo $export; ?>" id="filter_export" class="button"><?php echo 'Export'; ?></a>
          </td>
        </tr>
      </table>
      <table  class="list">
        <thead>
          <tr>
            <td class="left"><?php echo 'Sr. No'; ?></td>
            <td class="left"><?php echo 'Emp Code'; ?></td>
            <td class="left"><?php echo 'Name'; ?></td>
            <td class="right"><?php echo 'Bonus Amount'; ?></td>
          </tr>
        </thead>
        <tbody>
          <?php if ($dailyreports) { ?>
            <?php 
              $i = 1; 
            ?>
            <?php foreach ($dailyreports as $result) { ?>
              <tr>
                <td class="text-left"><?php echo $i; ?></td>
                <td class="text-left"><?php echo $result['emp_code']; ?></td>
                <td class="text-left"><?php echo $result['emp_name']; ?></td>
                <td class="text-right"><?php echo $result['bonus_amount']; ?></td>
              </tr>
              <?php 
                $i ++; 
              ?>
            <?php } ?>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
function filter() {
  url = 'index.php?route=report/bonus_report_muster&token=<?php echo $token; ?>';

  var filter_name = $('input[name=\'filter_name\']').attr('value');
  
  if (filter_name) {
    url += '&filter_name=' + encodeURIComponent(filter_name);
    var filter_name_id = $('input[name=\'filter_name_id\']').attr('value');
    if (filter_name_id) {
      url += '&filter_name_id=' + encodeURIComponent(filter_name_id);
    } else {
      alert('Please Enter Correct Employee Name');
      return false;
    }
  }
  
  var filter_date_start = $('input[name=\'filter_date_start\']').attr('value');
  
  if (filter_date_start) {
    url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  }

  var filter_gender = $('select[name=\'filter_gender\']').attr('value');
  if (filter_gender) {
    url += '&filter_gender=' + encodeURIComponent(filter_gender);
  }

  var filter_unit = $('select[name=\'filter_unit\']').attr('value');
  if (filter_unit) {
    url += '&filter_unit=' + encodeURIComponent(filter_unit);
  }

  var filter_year = $('select[name=\'filter_year\']').attr('value');
  if (filter_year) {
    url += '&filter_year=' + encodeURIComponent(filter_year);
  }

  var filter_date_end = $('input[name=\'filter_date_end\']').attr('value');
  if (filter_date_end) {
    url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
  }

  url += '&once=1';
  
  location = url;
  return false;
}

//--></script> 
<script type="text/javascript"><!--
$(document).ready(function() {
  
  //$('#date-start').datepicker({dateFormat: 'yy-mm-dd'});
  //$('#date-end').datepicker({dateFormat: 'yy-mm-dd'});

  $(function() {
      //$.datepicker.setDefaults($.datepicker.regional['en']);
      $('#date-start').datepicker({
            dateFormat: 'yy-mm-dd',
            onSelect: function(selectedDate) {
              var date = $(this).datepicker('getDate');
              $('#date-end').datepicker('option', 'maxDate', date); // Reset minimum date
              date.setDate(date.getDate() + 30); // Add 30 days
              $('#date-end').datepicker('setDate', date); // Set as default
            }
      });
      
      $('#date-end').datepicker({
            dateFormat: 'yy-mm-dd',
            onSelect: function(selectedDate) {
              $('#date-start').datepicker('option', 'maxDate', $(this).datepicker('getDate')); // Reset maximum date
            }
      });
      
  });
  
});

$('input[name=\'filter_name\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=report/dailyattendance/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.name,
            value: item.emp_code
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_name\']').val(ui.item.label);
    $('input[name=\'filter_name_id\']').val(ui.item.value);
    return false;
  },
  focus: function(event, ui) {
    return false;
  }
});

//$(document).ready(function() {
 // $('#date-start').datepicker({dateFormat: 'yy-mm-dd'});
  
//});
</script>
<?php echo $footer; ?>