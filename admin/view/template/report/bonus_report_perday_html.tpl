<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
</head>
<body>
<div class="box" id="content">
  <div  style="page-break-after: always;">
    <h1 style="text-align:center;">
      <?php echo 'SPECTRUM SERVICES.'; ?><br />
      <?php echo 'Bonus Report Perday'; ?><br />
    </h1>
    <table class="product">
      <thead>
        <tr>
          <td style="text-align: left;"><?php echo 'Sr. No'; ?></td>
          <td style="text-align: left;"><?php echo 'Emp Code'; ?></td>
          <td style="text-align: left;"><?php echo 'Name'; ?></td>
          <td style="text-align: right;"><?php echo 'Bonus Amount'; ?></td>
        </tr>
      </thead>
      <tbody>
        <?php if ($dailyreports) { ?>
          <?php 
            $i = 1; 
          ?>
          <?php foreach ($dailyreports as $result) { ?>
            <tr>
              <td style="text-align: left;"><?php echo $i; ?></td>
              <td style="text-align: left;"><?php echo $result['emp_code']; ?></td>
              <td style="text-align: left;"><?php echo $result['emp_name']; ?></td>
              <td style="text-align: right;"><?php echo $result['bonus_amount']; ?></td>
            </tr>
            <?php 
              $i ++; 
            ?>
          <?php } ?>
        <?php } ?>
      </tbody>
    </table>
  </div>
</div></body></html>