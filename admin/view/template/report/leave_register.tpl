<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/report.png" alt="" /> <?php echo $heading_title; ?></h1>
    </div>
    <div class="content sales-report">
      <table class="form">
        <tr>
          <td style="width:15%;"><?php echo "Name"; ?>
            <?php if(isset($this->session->data['emp_code'])) { ?>
              <input readonly="readonly" type="text" name="filter_name" value="<?php echo $filter_name; ?>" id="filter_name" size="25" />
            <?php } else { ?>
              <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" id="filter_name" size="25" />
            <?php } ?>
            <input type="hidden" name="filter_name_id" value="<?php echo $filter_name_id; ?>" id="filter_name_id" />
          </td>
          <td style="width:5%;">
            <?php echo 'Year'; ?>
            <select name="filter_year" id="filter_year">
              <?php for($i=2009; $i<=2020; $i++) { ?>
                <?php if($filter_year == $i) { ?>
                  <option value="<?php echo $i ?>" selected="selected"><?php echo $i; ?></option>
                <?php } else { ?>
                  <option value="<?php echo $i ?>"><?php echo $i; ?></option>
                <?php } ?>
              <?php } ?>
            </select>
            <input style="display:none;" type="text" name="filter_date_start" value="<?php echo $filter_date_start; ?>" id="date-start" size="8" />
          </td>
          <td style="width:7%;"><?php echo "Date End"; ?>
            <input type="text" name="filter_date_end" value="<?php echo $filter_date_end; ?>" id="date-end" size="8" />
          </td>
          
          <?php if($user_dept == '0' || $is_dept == '1') { ?>
          <td style="width:7%">Location
            <select name="unit" style="width:75px;">
              <?php foreach($unit_data as $key => $ud) { ?>
                <?php if($key === $unit) { ?>
                  <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                <?php } else { ?>
                  <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </td>
          <?php } ?>
          

          <?php if($user_dept == '0') { ?>
          <td style="width:10%">Dept
            <select name="department" style="width:115px;">
              <?php foreach($department_data as $key => $dd) { ?>
                <?php if($key === $department) { ?>
                  <option value='<?php echo $key; ?>' selected="selected"><?php echo $dd; ?></option> 
                <?php } else { ?>
                  <option value='<?php echo $key; ?>'><?php echo $dd; ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </td>
          <?php } ?>

          <?php if($user_dept == '0' || $is_dept == '1') { ?>
          <td style="width:7%;display: none;">Group
            <select name="group" style="width:110px;">
              <?php foreach($group_data as $key => $gd) { ?>
                <?php if($key === $group) { ?>
                  <option value='<?php echo $key; ?>' selected="selected"><?php echo $gd; ?></option> 
                <?php } else { ?>
                  <option value='<?php echo $key; ?>'><?php echo $gd; ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </td>
          <?php } ?>

          <td style="width:6%;">Type
            <select name="filter_leave" style="width:52px;">
              <?php foreach($leaves as $key => $dd) { ?>
                <?php if($key == $filter_leave) { ?>
                  <option value='<?php echo $key; ?>' selected="selected"><?php echo $dd; ?></option> 
                <?php } else { ?>
                  <option value='<?php echo $key; ?>'><?php echo $dd; ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </td>
    
          <td style="text-align: right;">
            <a style="padding: 5px 5px;" onclick="filter();" id="filter" class="button"><?php echo $button_filter; ?></a>
            <a style="padding: 5px 5px;" onclick="filter_export();" id="filter_export" class="button"><?php echo 'Export'; ?></a>
            <a style="padding: 5px 5px;display: none;" onclick="filter_export_summary();" id="filter_summ" class="button"><?php echo 'Export Summary'; ?></a>
            <a style="padding: 5px 5px;display: none;" onclick="filter_export_custom();" id="filter_custom" class="button"><?php echo 'Export Custom'; ?></a>
          </td>
        </tr>
      </table>
      <table class="list" style="width: 47%;">
        <tbody>
          <?php if($final_datas) { ?>
            <?php $i = 1; ?>
            <?php foreach($final_datas as $final_data) { ?>
              <tr>
                <td class="left" style="width:9%;font-weight:bold;font-size:12px;">From Date</td>
                <td class="left" style="width:9%;font-weight:bold;font-size:12px;">To Date</td>
                <td class="left" style="width:9%;font-weight:bold;font-size:12px;">Break</td>
                <td class="left" colspan="7" style="text-align:center;font-weight:bold;font-size:12px;">Leave Detail</td>
              </tr>
              <tr style="font-weight:bold;font-size:11px;">
                <td colspan = "10">
                  <?php echo 'Employee Code & Name : ' . $final_data['basic_data']['emp_code']; ?> &nbsp;&nbsp;&nbsp; <?php echo $final_data['basic_data']['name']; ?> 
                </td>
              </tr>
              <tr>
                <td colspan="3">
                </td>
                <td style="padding: 0px 9px;font-weight:bold;font-size:12px;width: 7%;text-align: right;">
                  BL
                </td>
                <td style="padding: 0px 9px;font-weight:bold;font-size:12px;width: 7%;text-align: right;">
                  PL
                </td>
                <td style="padding: 0px 9px;font-weight:bold;font-size:12px;width: 7%;text-align: right;">
                  SL
                </td>
                <td style="padding: 0px 9px;font-weight:bold;font-size:12px;width: 7%;text-align: right;">
                  ML
                </td>
                <td style="padding: 0px 9px;font-weight:bold;font-size:12px;width: 7%;text-align: right;">
                  MAL
                </td>
                <td style="padding: 0px 9px;font-weight:bold;font-size:12px;width: 7%;text-align: right;">
                  PAL
                </td>
                <td style="padding: 0px 9px;font-weight:bold;font-size:12px;width: 7%;text-align: right;">
                  LWP
                </td>
              </tr>
              <tr>
                <td class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
                  <?php echo 'opening Bal'; ?>
                </td>
                <td style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                  <?php echo $final_data['basic_data']['open_date']; ?>
                </td>
                <td>
                </td>
                <td class="right" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
                  <?php echo $final_data['basic_data']['bl_open']; ?>
                </td>
                <td class="right" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
                  <?php echo $final_data['basic_data']['pl_open']; ?>
                </td>
                <td class="right" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
                  <?php echo $final_data['basic_data']['sl_open']; ?>
                </td>
                <td class="right" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
                  <?php echo $final_data['basic_data']['ml_open']; ?>
                </td>
                <td class="right" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
                  <?php echo $final_data['basic_data']['mal_open']; ?>
                </td>
                <td class="right" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
                  <?php echo $final_data['basic_data']['pal_open']; ?>
                </td>
                <td class="right" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
                  <?php echo $final_data['basic_data']['lwp_open']; ?>
                </td>
              </tr>
              <?php if(isset($final_data['tran_data']['action'])) { ?>
                <?php foreach($final_data['tran_data']['action'] as $tkey => $tvalue) { ?>
                  <?php if($tvalue['performance_stat'] == 1){ ?>
                    <tr>
                      <td style="padding: 0px 9px;font-size:11px;">
                        <?php echo $tvalue['start_date']; ?>
                      </td>
                      <td style="padding: 0px 9px;font-size:11px;">
                        <?php echo $tvalue['end_date']; ?>
                      </td>
                      <td style="padding: 0px 9px;font-size:11px;">
                        <?php echo $tvalue['leave_types']; ?>
                      </td>
                      <td class="right" style="padding: 0px 9px;font-size:11px;">
                        <?php echo $tvalue['days_bl']; ?>
                      </td>
                      <td class="right" style="padding: 0px 9px;font-size:11px;">
                        <?php echo $tvalue['days_pl']; ?>
                      </td>
                      <td class="right" style="padding: 0px 9px;font-size:11px;">
                        <?php echo $tvalue['days_sl']; ?>
                      </td>
                      <td class="right" style="padding: 0px 9px;font-size:11px;">
                        <?php echo $tvalue['days_ml']; ?>
                      </td>
                      <td class="right" style="padding: 0px 9px;font-size:11px;">
                        <?php echo $tvalue['days_mal']; ?>
                      </td>
                      <td class="right" style="padding: 0px 9px;font-size:11px;">
                        <?php echo $tvalue['days_pal']; ?>
                      </td>
                      <td class="right" style="padding: 0px 9px;font-size:11px;">
                        <?php echo $tvalue['days_lwp']; ?>
                      </td>
                    </tr>
                  <?php } ?>
                  <?php if($tvalue['encash_status'] == 1){ ?>
                    <tr>
                      <td style="padding: 0px 9px;font-size:11px;">
                        <?php echo $tvalue['start_date']; ?>
                      </td>
                      <td style="padding: 0px 9px;font-size:11px;">
                        <?php echo $tvalue['end_date']; ?>
                      </td>
                      <td style="padding: 0px 9px;font-size:11px;">
                        <?php echo 'E'; ?>
                      </td>
                      <td class="right" style="padding: 0px 9px;font-size:11px;">
                        <?php echo 0; ?>
                      </td>
                      <td class="right" style="padding: 0px 9px;font-size:11px;">
                        <?php echo $tvalue['encash']; ?>
                      </td>
                      <td class="right" style="padding: 0px 9px;font-size:11px;">
                        <?php echo 0; ?>
                      </td>
                    </tr>
                  <?php } ?>
                <?php } ?>
              <?php } ?>
              <tr>
                <td colspan="3" class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
                  <?php echo 'Consumed Leave'; ?>
                </td>
                <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                  <?php echo $final_data['basic_data']['total_days_bl']; ?>
                </td>
                <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                  <?php echo $final_data['basic_data']['total_days_pl']; ?>
                </td>
                <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                  <?php echo $final_data['basic_data']['total_days_sl']; ?>
                </td>
                <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                  <?php echo $final_data['basic_data']['total_days_ml']; ?>
                </td>
                <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                  <?php echo $final_data['basic_data']['total_days_mal']; ?>
                </td>
                <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                  <?php echo $final_data['basic_data']['total_days_pal']; ?>
                </td>
                <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                  <?php echo $final_data['basic_data']['total_days_lwp']; ?>
                </td>
              </tr>
              <tr>
                <td colspan="3" class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
                  <?php echo 'Closing Balance'; ?>
                </td>
                <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                  <?php echo $final_data['basic_data']['closing_bl']; ?>
                </td>
                <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                  <?php echo $final_data['basic_data']['closing_pl']; ?>
                </td>
                <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                  <?php echo $final_data['basic_data']['closing_sl']; ?>
                </td>
                <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                  <?php echo $final_data['basic_data']['closing_ml']; ?>
                </td>
                <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                  <?php echo $final_data['basic_data']['closing_mal']; ?>
                </td>
                <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                  <?php echo $final_data['basic_data']['closing_pal']; ?>
                </td>
                <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                  <?php echo $final_data['basic_data']['closing_lwp']; ?>
                </td>
              </tr>
              <?php if($filter_year == date('Y')) { ?>
                <tr>
                  <td colspan="3" class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
                    <?php echo 'UnProcessed Leave'; ?>
                  </td>
                  <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                    <?php echo $final_data['basic_data']['un_total_days_bl']; ?>
                  </td>
                  <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                    <?php echo $final_data['basic_data']['un_total_days_pl']; ?>
                  </td>
                  <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                    <?php echo $final_data['basic_data']['un_total_days_sl']; ?>
                  </td>
                  <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                    <?php echo $final_data['basic_data']['un_total_days_ml']; ?>
                  </td>
                  <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                    <?php echo $final_data['basic_data']['un_total_days_mal']; ?>
                  </td>
                  <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                    <?php echo $final_data['basic_data']['un_total_days_pal']; ?>
                  </td>
                  <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                    <?php echo $final_data['basic_data']['un_total_days_lwp']; ?>
                  </td>
                </tr>
                <tr>
                  <td colspan="3" class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
                    <?php echo 'UnProcessed Balance'; ?>
                  </td>
                  <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                    <?php echo $final_data['basic_data']['un_total_bal_bl']; ?>
                  </td>
                  <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                    <?php echo $final_data['basic_data']['un_total_bal_pl']; ?>
                  </td>
                  <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                    <?php echo $final_data['basic_data']['un_total_bal_sl']; ?>
                  </td>
                  <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                    <?php echo $final_data['basic_data']['un_total_bal_ml']; ?>
                  </td>
                  <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                    <?php echo $final_data['basic_data']['un_total_bal_mal']; ?>
                  </td>
                  <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                    <?php echo $final_data['basic_data']['un_total_bal_pal']; ?>
                  </td>
                  <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                    <?php echo $final_data['basic_data']['un_total_bal_lwp']; ?>
                  </td>
                </tr>
              <?php } ?>
              <tr style="border-bottom:2px solid black;">
                <td colspan = "10" >
                </td>
              </tr>
              <?php $i++; ?>
            <?php } ?>
          <?php } else { ?>
          <tr>
            <td class="center" colspan = "1"><?php echo $text_no_results; ?></td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
      
    </div>
  </div>
</div>
<script type="text/javascript"><!--
function filter() {
  url = 'index.php?route=report/leave_register&token=<?php echo $token; ?>';
  
  var filter_name = $('input[name=\'filter_name\']').attr('value');
  
  if (filter_name) {
    url += '&filter_name=' + encodeURIComponent(filter_name);
    var filter_name_id = $('input[name=\'filter_name_id\']').attr('value');
    if (filter_name_id) {
      url += '&filter_name_id=' + encodeURIComponent(filter_name_id);
    } else {
      alert('Please Enter Correct Employee Name');
      return false;
    }
  }

  var filter_year = $('select[name=\'filter_year\']').attr('value');
  
  if (filter_year) {
    url += '&filter_year=' + encodeURIComponent(filter_year);
  }

  var filter_date_start = $('input[name=\'filter_date_start\']').attr('value');
  
  if (filter_date_start) {
    url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  }

  var filter_date_end = $('input[name=\'filter_date_end\']').attr('value');
  
  if (filter_date_end) {
    url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
  }

  /*
  var filter_month = $('select[name=\'filter_month\']').attr('value');
  
  if (filter_month) {
    url += '&filter_month=' + encodeURIComponent(filter_month);
  }

  var filter_year = $('select[name=\'filter_year\']').attr('value');
  
  if (filter_year) {
    url += '&filter_year=' + encodeURIComponent(filter_year);
  }
  */

  var unit = $('select[name=\'unit\']').attr('value');
  
  if (unit) {
    url += '&unit=' + encodeURIComponent(unit);
  }

  var department = $('select[name=\'department\']').attr('value');
  
  if (department) {
    url += '&department=' + encodeURIComponent(department);
  }

  var group = $('select[name=\'group\']').attr('value');
  
  if (group) {
    url += '&group=' + encodeURIComponent(group);
  }

  var filter_leave = $('select[name=\'filter_leave\']').attr('value');
  
  if (filter_leave) {
    url += '&filter_leave=' + encodeURIComponent(filter_leave);
  }

  url += '&once=1';
  
  location = url;
  return false;
}

function filter_export() {
  url = 'index.php?route=report/leave_register/export&token=<?php echo $token; ?>';
  
  var filter_name = $('input[name=\'filter_name\']').attr('value');
  
  if (filter_name) {
    url += '&filter_name=' + encodeURIComponent(filter_name);
    var filter_name_id = $('input[name=\'filter_name_id\']').attr('value');
    if (filter_name_id) {
      url += '&filter_name_id=' + encodeURIComponent(filter_name_id);
    } else {
      alert('Please Enter Correct Employee Name');
      return false;
    }
  }

  var filter_date_start = $('input[name=\'filter_date_start\']').attr('value');
  
  if (filter_date_start) {
    url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  }

  var filter_date_end = $('input[name=\'filter_date_end\']').attr('value');
  
  if (filter_date_end) {
    url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
  }

  var filter_year = $('select[name=\'filter_year\']').attr('value');
  
  if (filter_year) {
    url += '&filter_year=' + encodeURIComponent(filter_year);
  }

  /*
  var filter_month = $('select[name=\'filter_month\']').attr('value');
  
  if (filter_month) {
    url += '&filter_month=' + encodeURIComponent(filter_month);
  }

  var filter_year = $('select[name=\'filter_year\']').attr('value');
  
  if (filter_year) {
    url += '&filter_year=' + encodeURIComponent(filter_year);
  }
  */

  var unit = $('select[name=\'unit\']').attr('value');
  
  if (unit) {
    url += '&unit=' + encodeURIComponent(unit);
  }

  var department = $('select[name=\'department\']').attr('value');
  
  if (department) {
    url += '&department=' + encodeURIComponent(department);
  }

  var group = $('select[name=\'group\']').attr('value');
  
  if (group) {
    url += '&group=' + encodeURIComponent(group);
  }

  var filter_leave = $('select[name=\'filter_leave\']').attr('value');
  
  if (filter_leave) {
    url += '&filter_leave=' + encodeURIComponent(filter_leave);
  }

  location = url;
  return false;
}

function filter_export_summary() {
  url = 'index.php?route=report/leave_register/export_summary&token=<?php echo $token; ?>';
  
  var filter_name = $('input[name=\'filter_name\']').attr('value');
  
  if (filter_name) {
    url += '&filter_name=' + encodeURIComponent(filter_name);
    var filter_name_id = $('input[name=\'filter_name_id\']').attr('value');
    if (filter_name_id) {
      url += '&filter_name_id=' + encodeURIComponent(filter_name_id);
    } else {
      alert('Please Enter Correct Employee Name');
      return false;
    }
  }

  var filter_date_start = $('input[name=\'filter_date_start\']').attr('value');
  
  if (filter_date_start) {
    url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  }

  var filter_date_end = $('input[name=\'filter_date_end\']').attr('value');
  
  if (filter_date_end) {
    url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
  }

  var filter_year = $('select[name=\'filter_year\']').attr('value');
  
  if (filter_year) {
    url += '&filter_year=' + encodeURIComponent(filter_year);
  }

  /*
  var filter_month = $('select[name=\'filter_month\']').attr('value');
  
  if (filter_month) {
    url += '&filter_month=' + encodeURIComponent(filter_month);
  }

  var filter_year = $('select[name=\'filter_year\']').attr('value');
  
  if (filter_year) {
    url += '&filter_year=' + encodeURIComponent(filter_year);
  }
  */

  var unit = $('select[name=\'unit\']').attr('value');
  
  if (unit) {
    url += '&unit=' + encodeURIComponent(unit);
  }

  var department = $('select[name=\'department\']').attr('value');
  
  if (department) {
    url += '&department=' + encodeURIComponent(department);
  }

  var group = $('select[name=\'group\']').attr('value');
  
  if (group) {
    url += '&group=' + encodeURIComponent(group);
  }

  var filter_leave = $('select[name=\'filter_leave\']').attr('value');
  
  if (filter_leave) {
    url += '&filter_leave=' + encodeURIComponent(filter_leave);
  }

  location = url;
  return false;
}

function filter_export_custom() {
  url = 'index.php?route=report/leave_register/export_custom&token=<?php echo $token; ?>';
  
  var filter_name = $('input[name=\'filter_name\']').attr('value');
  
  if (filter_name) {
    url += '&filter_name=' + encodeURIComponent(filter_name);
    var filter_name_id = $('input[name=\'filter_name_id\']').attr('value');
    if (filter_name_id) {
      url += '&filter_name_id=' + encodeURIComponent(filter_name_id);
    } else {
      alert('Please Enter Correct Employee Name');
      return false;
    }
  }

  var filter_year = $('select[name=\'filter_year\']').attr('value');
  
  if (filter_year) {
    url += '&filter_year=' + encodeURIComponent(filter_year);
  }

  var filter_date_start = $('input[name=\'filter_date_start\']').attr('value');
  
  if (filter_date_start) {
    url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  }

  var filter_date_end = $('input[name=\'filter_date_end\']').attr('value');
  
  if (filter_date_end) {
    url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
  }

  var unit = $('select[name=\'unit\']').attr('value');
  
  if (unit) {
    url += '&unit=' + encodeURIComponent(unit);
  }

  var department = $('select[name=\'department\']').attr('value');
  
  if (department) {
    url += '&department=' + encodeURIComponent(department);
  }

  var group = $('select[name=\'group\']').attr('value');
  
  if (group) {
    url += '&group=' + encodeURIComponent(group);
  }

  var filter_leave = $('select[name=\'filter_leave\']').attr('value');
  
  if (filter_leave) {
    url += '&filter_leave=' + encodeURIComponent(filter_leave);
  }

  location = url;
  return false;
}

//--></script> 
<script type="text/javascript"><!--
$(document).ready(function() {
  
  //$('#date-start').datepicker({dateFormat: 'yy-mm-dd'});
  //$('#date-end').datepicker({dateFormat: 'yy-mm-dd'});

  $(function() {
      //$.datepicker.setDefaults($.datepicker.regional['en']);

      $("#filter_year").change(function() {
        year = $(this).val();
        $('#date-end').val(year+'-12-31');
      });

      $('#date-start').datepicker({
            dateFormat: 'yy-mm-dd',
            onSelect: function(selectedDate) {
              var date = $(this).datepicker('getDate');
              date_to = new Date(date.getFullYear(), 11, 31);
              var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
              a = date.getMonth();
              b = date_to.getMonth();
              var firstDate = new Date(date.getFullYear(), a, date.getDate());
              var secondDate = new Date(date_to.getFullYear(), b, date_to.getDate());
              var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
              var diffDays = diffDays;
              $('#date-end').datepicker('option', 'maxDate', date); // Reset minimum date
              date.setDate(date.getDate() + diffDays); // Add 30 days
              $('#date-end').datepicker('setDate', date_to); // Set as default
            }
      });
      
      $('#date-end').datepicker({
            dateFormat: 'yy-mm-dd',
            onSelect: function(selectedDate) {
              //$('#date-start').datepicker('option', 'maxDate', $(this).datepicker('getDate')); // Reset maximum date
            }
      });
      
  });
  
});
//--></script>
<script type="text/javascript"><!--

$.widget('custom.catcomplete', $.ui.autocomplete, {
  _renderMenu: function(ul, items) {
    var self = this, currentCategory = '';
    $.each(items, function(index, item) {
      if (item.category != currentCategory) {
        //ul.append('<li class="ui-autocomplete-category">' + item.category + '</li>');
        currentCategory = item.category;
      }
      self._renderItem(ul, item);
    });
  }
});

$('input[name=\'filter_name\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=report/leave_register/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.name,
            value: item.emp_code
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_name\']').val(ui.item.label);
    $('input[name=\'filter_name_id\']').val(ui.item.value);
    return false;
  },
  focus: function(event, ui) {
    return false;
  }
});
//--></script>
<?php echo $footer; ?>