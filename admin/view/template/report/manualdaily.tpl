<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
	<?php foreach ($breadcrumbs as $breadcrumb) { ?>
	<?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
	<?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
	<div class="heading">
	  <h1><img src="view/image/report.png" alt="" /> <?php echo $heading_title; ?></h1>
	</div>
	<div class="content sales-report">
	  <table class="form">
		<tr>
		  <td style="width:13%;"><?php echo "Name"; ?>
			<input type="text" name="filter_name" value="<?php echo $filter_name;  ?>" id="filter_name" size="25" />
			<input type="hidden" name="filter_name_id" value="<?php echo $filter_name_id; ?>" id="filter_name_id" />
		  </td>
		  <td style="width:8%;"><?php echo "Date Start"; ?>
			<input type="text" name="filter_date_start" value="<?php echo $filter_date_start; ?>" id="date-start" size="10" />
		  </td>
		  <td style="width:8%;"><?php echo "Date End"; ?>
			<input type="text" name="filter_date_end" value="<?php echo $filter_date_end; ?>" id="date-end" size="10" />
		  </td>
		  <td style="text-align: right;">
			<a style="padding: 13px 25px;" onclick="filter();" id="filter" class="button"><?php echo $button_filter; ?></a>
			<a onclick="$('#form').submit();" class="button"><?php echo 'Save'; ?></a>
		  </td>
		</tr>
	  </table>
	  <?php if($dailyreports){ ?>
		<table class="form">
			<tr>
			  <td colspan="3"><b><?php echo 'Department:'; ?></b>
			  <?php if (isset($dailyreports[0]['department'])) { ?>
			  <?php echo $dailyreports[0]['department']; ?>
			  <?php } else { echo " "; } ?>
			  </td>
			</tr>
			<tr>
			  <td class="left"><b><?php echo 'Employee Code:'; ?></b>
			  <?php if (isset($dailyreports[0]['emp_id'])) { ?>
			  <?php echo $dailyreports[0]['emp_id']; ?>
			  <?php } else { echo " "; } ?>
			  </td>
			  <td class="right"><b><?php echo 'Employee Name:'; ?></b>
			  <?php if (isset($dailyreports[0]['emp_name'])) { ?>
			  <?php echo $dailyreports[0]['emp_name']; ?>
			  <?php } else { echo " "; } ?></td>
			</tr>
		</table>

		<table  class="list">
		  <thead >
			<tr>
			  <td class="left"><?php echo 'Date'; ?></td>
			  <td class="left"><?php echo 'Shift'; ?></td>
			  <td class="left"><?php echo 'Shift InTime'; ?></td>
			  <td class="left"><?php echo 'Shift OutTime'; ?></td>
			  <td class="left"><?php echo 'Actual InTime'; ?></td>
			  <td class="left"><?php echo 'Actual OutTime'; ?></td>
			  <td class="left"><?php echo 'Status'; ?></td>
			</tr>
		  </thead>
		  <tbody>
			<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
			  <?php if ($dailyreports) { ?>
				<?php $i = 1; ?>
				<?php foreach ($dailyreports as $result) { ?>
				  <tr>
					<td class="text-left">
					  <?php echo $result['date']; ?>
					  <input id="date-<?php echo $result['transaction_id'] ?>" type="hidden" name="attendance_data[<?php echo $result['date'] ?>][dot]" value="<?php echo $result['date'] ?>" />
					  <input id="transaction_id-<?php echo $result['transaction_id'] ?>" type="hidden" name="attendance_data[<?php echo $result['date'] ?>][transaction_id]" value="<?php echo $result['transaction_id'] ?>" />
					  <input id="e_name_id-<?php echo $result['transaction_id'] ?>" type="hidden" name="attendance_data[<?php echo $result['date'] ?>][e_name_id]" value="<?php echo $result['emp_id'] ?>" />
					  <input id="e_name-<?php echo $result['transaction_id'] ?>" type="hidden" name="attendance_data[<?php echo $result['date'] ?>][e_name]" value="<?php echo $result['emp_name'] ?>" />
					</td>
					<td class="text-left">
					  <select name="attendance_data[<?php echo $result['date'] ?>][shift_id]" id="shift_id-<?php echo $result['transaction_id'] ?>" class="shift_class">
						<option value="0" selected="selected">Please Select</option>
						<?php foreach($shift_data as $skey => $svalue) { ?>
						  <?php if($svalue['shift_id'] === $result['shift_id']){ ?>
							<option value="<?php echo $svalue['shift_id']; ?>" selected="selected"><?php echo $svalue['shift_name']; ?></option>
						  <?php } else { ?>
							<option value="<?php echo $svalue['shift_id']; ?>"><?php echo $svalue['shift_name']; ?></option>
						  <?php } ?>
						<?php } ?>
					  </select>
					  <input id="hidden_shift_id-<?php echo $result['transaction_id'] ?>" type="hidden" name="attendance_data[<?php echo $result['date'] ?>][hidden_shift_id]" value="<?php echo $result['shift_id'] ?>" />
					</td>
					<td class="text-left">
					  <input id="shift_intime-<?php echo $result['transaction_id'] ?>" readonly="readonly" type="text" name="attendance_data[<?php echo $result['date'] ?>][shift_intime]" value="<?php echo $result['shift_intime'] ?>" />
					  <input id="hidden_shift_intime-<?php echo $result['transaction_id'] ?>" type="hidden" name="attendance_data[<?php echo $result['date'] ?>][hidden_shift_intime]" value="<?php echo $result['shift_intime'] ?>" />
					</td>
					<td class="text-left">
					  <input id="shift_outtime-<?php echo $result['transaction_id'] ?>" readonly="readonly" type="text" name="attendance_data[<?php echo $result['date'] ?>][shift_outtime]" value="<?php echo $result['shift_outtime'] ?>" />
					  <input id="hidden_shift_outtime-<?php echo $result['transaction_id'] ?>" type="hidden" name="attendance_data[<?php echo $result['date'] ?>][hidden_shift_outtime]" value="<?php echo $result['shift_outtime'] ?>" />
					</td>
					<td class="text-left">
					  <input class="time" type="text" name="attendance_data[<?php echo $result['date'] ?>][actual_intime]" value="<?php echo $result['act_intime'] ?>" />
					  <input type="hidden" name="attendance_data[<?php echo $result['date'] ?>][hidden_actual_intime]" value="<?php echo $result['act_intime'] ?>" />
					</td>
					<td class="text-left">
					  <input class="time" type="text" name="attendance_data[<?php echo $result['date'] ?>][actual_outtime]" value="<?php echo $result['act_outtime'] ?>" />
					  <input type="hidden" name="attendance_data[<?php echo $result['date'] ?>][hidden_actual_outtime]" value="<?php echo $result['act_outtime'] ?>" />
					</td>
					<td class="text-left">
					  <?php echo $result['status_name']; ?>
					</td>
				  </tr>
				<?php } ?>
			  <?php } ?>
			</form>
		  </tbody>
		</table>
	  <?php } ?>
	</div>
  </div>
</div>
<script type="text/javascript" src="view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript"><!--
function filter() {
  url = 'index.php?route=report/manualdaily&token=<?php echo $token; ?>';

  var filter_name = $('input[name=\'filter_name\']').attr('value');
  
  if (filter_name) {
	url += '&filter_name=' + encodeURIComponent(filter_name);
	var filter_name_id = $('input[name=\'filter_name_id\']').attr('value');
	if (filter_name_id) {
	  url += '&filter_name_id=' + encodeURIComponent(filter_name_id);
	} else {
	  alert('Please Enter Correct Employee Name');
	  return false;
	}
  }
  
  var filter_date_start = $('input[name=\'filter_date_start\']').attr('value');
  
  if (filter_date_start) {
	url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  }

  var filter_date_end = $('input[name=\'filter_date_end\']').attr('value');
  
  if (filter_date_end) {
	url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
  }

  url += '&once=1';

  location = url;
  return false;
}

//--></script> 
<script type="text/javascript"><!--
$(document).ready(function() {
  
  //$('#date-start').datepicker({dateFormat: 'yy-mm-dd'});
  //$('#date-end').datepicker({dateFormat: 'yy-mm-dd'});

  $(function() {
	  //$.datepicker.setDefaults($.datepicker.regional['en']);
	  $('#date-start').datepicker({
			dateFormat: 'yy-mm-dd',
			onSelect: function(selectedDate) {
			  var date = $(this).datepicker('getDate');
			  $('#date-end').datepicker('option', 'maxDate', date); // Reset minimum date
			  date.setDate(date.getDate() + 30); // Add 30 days
			  $('#date-end').datepicker('setDate', date); // Set as default
			}
	  });
	  
	  $('#date-end').datepicker({
			dateFormat: 'yy-mm-dd',
			onSelect: function(selectedDate) {
			  $('#date-start').datepicker('option', 'maxDate', $(this).datepicker('getDate')); // Reset maximum date
			}
	  });
	  
  });
  
});

$('input[name=\'filter_name\']').autocomplete({
  delay: 500,
  source: function(request, response) {
	$.ajax({
	  url: 'index.php?route=catalog/employee/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
	  dataType: 'json',
	  success: function(json) {   
		response($.map(json, function(item) {
		  return {
			label: item.name,
			value: item.emp_code
		  }
		}));
	  }
	});
  }, 
  select: function(event, ui) {
	$('input[name=\'filter_name\']').val(ui.item.label);
	$('input[name=\'filter_name_id\']').val(ui.item.value);
	return false;
  },
  focus: function(event, ui) {
	return false;
  }
});

$('.time').timepicker({timeFormat: 'hh:mm:ss'});

$('.shift_class').change(function(){
  idss = $(this).attr('id');
  s_id = idss.split('-');
  var id = s_id[1];
  shift_id = $('#shift_id-'+id).val();
  transaction_id = $('#transaction_id-'+id).val();
  dot = $('#date-'+id).val();
  emp_id = $('#e_name_id-'+id).val();
  $.ajax({
	url: 'index.php?route=transaction/manualpunch/getshiftdata&token=<?php echo $token; ?>&shift_id='+shift_id+'&transaction_id='+transaction_id+'&dot='+dot+'&emp_id='+emp_id,
	dataType: 'json',
	success: function(json) {  
	  if(json.status == 1){
		$('#shift_intime-'+id).val(json.shift_data['in_time']);
		$('#shift_outtime-'+id).val(json.shift_data['out_time']);
	  } else {
		$('#shift_intime-'+id).val('00:00:00');
		$('#shift_outtime-'+id).val('00:00:00');
	  } 
	}
  });
});

//$(document).ready(function() {
 // $('#date-start').datepicker({dateFormat: 'yy-mm-dd'});
  
//});
</script>
<?php echo $footer; ?>