<?php echo $header; ?>
<div id="content">
  	<div class="breadcrumb">
	<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
	<?php } ?>
  	</div>
  	<?php if ($error_warning) { ?>
  		<div class="warning"><?php echo $error_warning; ?></div>
  	<?php } ?>
  	<?php if ($success) { ?>
  		<div class="success"><?php echo $success; ?></div>
  	<?php } ?>
  	<div class="box">
		<div class="heading">
	  		<h1><img src="view/image/report.png" alt="" /> <?php echo $heading_title; ?></h1>
		</div>
		<div class="content sales-report">
	  	<table class="form">
		<tr>
		  	<td style="width:13%;"><?php echo "Name"; ?>
				<?php if(isset($this->session->data['employee_id'])) { ?>
			  		<input readonly="readonly" type="text" name="filter_name" value="<?php echo $filter_name; ?>" id="filter_name" size="25" />
				<?php } else { ?>
			  		<input type="text" name="filter_name" value="<?php echo $filter_name; ?>" id="filter_name" size="25" />
				<?php } ?>
					<input type="hidden" name="filter_name_id" value="<?php echo $filter_name_id; ?>" id="filter_name_id" />
		  	</td>
		  	<td style="width:8%;"><?php echo "Date Start"; ?>
				<input type="text" name="filter_date_start" value="<?php echo $filter_date_start; ?>" id="date-start" size="10" />
		  	</td>
		  	<td style="width:8%;"><?php echo "Date End"; ?>
				<input type="text" name="filter_date_end" value="<?php echo $filter_date_end; ?>" id="date-end" size="10" />
		  	</td>
		  	<td style="width:8%">Location
				<select name="filter_unit" style="width:100px">
			  	<?php foreach($unit_data as $key => $ud) { ?>
					<?php if($key == $filter_unit) { ?>
				  		<option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
					<?php } else { ?>
				  		<option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
					<?php } ?>
			  	<?php } ?>
				</select>
		  	</td>
		  	<td style="width:8%">Department
				<select name="filter_department" style="width:100px;">
			  	<?php foreach($department_data as $key => $dd) { ?>
					<?php if($key == $filter_department) { ?>
				  		<option value='<?php echo $key; ?>' selected="selected"><?php echo $dd; ?></option> 
					<?php } else { ?>
				  		<option value='<?php echo $key; ?>'><?php echo $dd; ?></option>
					<?php } ?>
			  	<?php } ?>
				</select>
		  	</td>
		  	<td style="width:8%">Designation
				<select name="filter_designation" style="width:100px">
			  	<?php foreach($designation_data as $dkey => $dg) { ?>
					<?php if($dkey == $filter_designation) { ?>
				  		<option value='<?php echo $dkey; ?>' selected="selected"><?php echo $dg; ?></option> 
					<?php } else { ?>
				  		<option value='<?php echo $dkey; ?>'><?php echo $dg; ?></option>
					<?php } ?>
			  	<?php } ?>
				</select>
		  	</td>
		  	<td style="text-align: right;">
				<a style="padding: 13px 25px;" onclick="filter();" id="filter" class="button"><?php echo $button_filter; ?></a>
				<a style="padding: 13px 25px;" href="<?php echo $export; ?>" class="button"><?php echo "Export"; ?></a>
		  	</td>
		</tr>
	  	</table>
	  	<table class="list">
		<tbody>
		  	<?php if($final_datas) { ?>
			<?php $i = 1; ?>
				<?php foreach($final_datas as $final_data) { ?>
			  	<tr>
					<td style="padding: 0px 9px;"></td>
					<?php foreach($days as $dkey => $dvalue) { ?>
				  	<td class="left" style="padding: 0px 9px;font-size:11px;"><?php echo $dvalue['day']; ?></td>
					<?php } ?>
			  	</tr>
			  	<tr style="font-weight:bold;font-size:11px;">
					<td colspan = "<?php echo count($final_data['tran_data']) + 1; ?> ">
				  		<?php echo $final_data['basic_data']['name']; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?php echo 'Emp Code : '.$final_data['basic_data']['emp_code']; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?php echo 'Department : '.$final_data['basic_data']['department']; ?>
					</td>
			  	</tr>
			  	<tr>
					<td class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;width:10%;">
				  		<?php echo 'Status'; ?>
					</td>
					<?php foreach($final_data['tran_data'] as $tkey => $tvalue) { ?>
				  	<td style="padding: 0px 9px;font-size:11px;">
						<?php echo $tvalue['firsthalf_status']; ?>
				  	</td>
					<?php } ?>
			  	</tr>
			  	<tr>
					<td class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
				 	 	<?php echo 'In Time'; ?>
					</td>
					<?php foreach($final_data['tran_data'] as $tkey => $tvalue) { ?>
				  		<td style="padding: 0px 9px;font-size:11px;">
							<?php echo $tvalue['act_intime']; ?>
				  		</td>
					<?php } ?>
			  	</tr>
			  	<tr>
					<td class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
				 	 	<?php echo 'Out Time'; ?>
					</td>
					<?php foreach($final_data['tran_data'] as $tkey => $tvalue) { ?>
				  	<td style="padding: 0px 9px;font-size:11px;">
						<?php echo $tvalue['act_outtime']; ?>
				  	</td>
					<?php } ?>
			  	</tr>
			  	<tr style="border-bottom:2px solid black;">
					<td class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
				  	<?php echo 'Total'; ?>
					</td>
					<?php foreach($final_data['tran_data'] as $tkey => $tvalue) { ?>
				  	<td style="padding: 0px 9px;font-size:11px;">
					<?php echo $tvalue['working_time']; ?>
				  	</td>
					<?php } ?>
			  	</tr>
			  	<?php if($filter_type == '3'){ ?>
			  	<tr style="border-bottom:2px solid black;">
					<td class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
				  	<?php echo 'Loss hours'; ?>
					</td>
					<?php foreach($final_data['tran_data'] as $tkey => $tvalue) { ?>
				  	<td style="padding: 0px 9px;font-size:11px;">
					<?php echo $tvalue['loss_hours']; ?>
				  	</td>
					<?php } ?>
			  	</tr>
			  	<?php } ?>
			  	<?php if($filter_type == '5'){ ?>
			  	<tr style="border-bottom:2px solid black;">
					<td class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
				  	<?php echo 'Exces hours'; ?>
					</td>
					<?php foreach($final_data['tran_data'] as $tkey => $tvalue) { ?>
				  	<td style="padding: 0px 9px;font-size:11px;">
					<?php echo $tvalue['excess_hours']; ?>
				 	</td>
					<?php } ?>
			  	</tr>
			  	<?php } ?>
			  	<tr style="border-bottom:2px solid black;">
				<td>
				</td>
				<td colspan = "<?php echo count($final_data['tran_data']) + 1; ?>" >
				</td>
			</tr>
			<?php $i++; ?>
			<?php } ?>
		  <?php } else { ?>
		  <tr>
			<td class="center" colspan = "<?php echo count($days) + 1; ?> "><?php echo $text_no_results; ?></td>
		  </tr>
		  <?php } ?>
		</tbody>
	  </table>
	  
	</div>
  </div>
</div>
<script type="text/javascript"><!--
function filter() {
  url = 'index.php?route=report/performance&token=<?php echo $token; ?>';
  
  var filter_name = $('input[name=\'filter_name\']').attr('value');
  
  if (filter_name) {
	url += '&filter_name=' + encodeURIComponent(filter_name);
	var filter_name_id = $('input[name=\'filter_name_id\']').attr('value');
	if (filter_name_id) {
	  url += '&filter_name_id=' + encodeURIComponent(filter_name_id);
	} else {
	  alert('Please Enter Correct Employee Name');
	  return false;
	}
  }

  var filter_date_start = $('input[name=\'filter_date_start\']').attr('value');
  
  if (filter_date_start) {
	url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  }

  var filter_date_end = $('input[name=\'filter_date_end\']').attr('value');
  
  if (filter_date_end) {
	url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
  }

  var filter_unit = $('select[name=\'filter_unit\']').attr('value');
  
  if (filter_unit) {
	url += '&filter_unit=' + encodeURIComponent(filter_unit);
  }

  var filter_department = $('select[name=\'filter_department\']').attr('value');
  
  if (filter_department) {
	url += '&filter_department=' + encodeURIComponent(filter_department);
  }
   var filter_designation = $('select[name=\'filter_designation\']').attr('value');
  
  if (filter_designation) {
	url += '&filter_designation=' + encodeURIComponent(filter_designation);
  }

  url += '&once=1';
  
  location = url;
  return false;
}

function filter_export() {
  url = 'index.php?route=report/performance/export&token=<?php echo $token; ?>';
  
 var filter_name = $('input[name=\'filter_name\']').attr('value');
  
  if (filter_name) {
	url += '&filter_name=' + encodeURIComponent(filter_name);
	var filter_name_id = $('input[name=\'filter_name_id\']').attr('value');
	if (filter_name_id) {
	  url += '&filter_name_id=' + encodeURIComponent(filter_name_id);
	} else {
	  alert('Please Enter Correct Employee Name');
	  return false;
	}
  }

  var filter_date_start = $('input[name=\'filter_date_start\']').attr('value');
  
  if (filter_date_start) {
	url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  }

  var filter_date_end = $('input[name=\'filter_date_end\']').attr('value');
  
  if (filter_date_end) {
	url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
  }

  var filter_unit = $('select[name=\'filter_unit\']').attr('value');
  
  if (filter_unit) {
	url += '&filter_unit=' + encodeURIComponent(filter_unit);
  }

  var filter_department = $('select[name=\'filter_department\']').attr('value');
  
  if (filter_department) {
	url += '&filter_department=' + encodeURIComponent(filter_department);
  }
   var filter_designation = $('select[name=\'filter_designation\']').attr('value');
  
  if (filter_designation) {
	url += '&filter_designation=' + encodeURIComponent(filter_designation);
  }
   
  location = url;
  return false;
}

//--></script> 
<script type="text/javascript"><!--
$(document).ready(function() {
  
  //$('#date-start').datepicker({dateFormat: 'yy-mm-dd'});
  //$('#date-end').datepicker({dateFormat: 'yy-mm-dd'});

  $(function() {
	  //$.datepicker.setDefaults($.datepicker.regional['en']);
	  $('#date-start').datepicker({
			dateFormat: 'yy-mm-dd',
			onSelect: function(selectedDate) {
			  var date = $(this).datepicker('getDate');
			  $('#date-end').datepicker('option', 'maxDate', date); // Reset minimum date
			  date.setDate(date.getDate() + 30); // Add 30 days
			  $('#date-end').datepicker('setDate', date); // Set as default
			}
	  });
	  
	  $('#date-end').datepicker({
			dateFormat: 'yy-mm-dd',
			onSelect: function(selectedDate) {
			  $('#date-start').datepicker('option', 'maxDate', $(this).datepicker('getDate')); // Reset maximum date
			}
	  });
	  
  });
  
});
//--></script>
<script type="text/javascript"><!--

$.widget('custom.catcomplete', $.ui.autocomplete, {
  _renderMenu: function(ul, items) {
	var self = this, currentCategory = '';
	$.each(items, function(index, item) {
	  if (item.category != currentCategory) {
		//ul.append('<li class="ui-autocomplete-category">' + item.category + '</li>');
		currentCategory = item.category;
	  }
	  self._renderItem(ul, item);
	});
  }
});

$('input[name=\'filter_name\']').autocomplete({
  delay: 500,
  source: function(request, response) {
	$.ajax({
	  url: 'index.php?route=catalog/employee/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
	  dataType: 'json',
	  success: function(json) {   
		response($.map(json, function(item) {
		  return {
			label: item.name,
			value: item.employee_id
		  }
		}));
	  }
	});
  }, 
  select: function(event, ui) {
	$('input[name=\'filter_name\']').val(ui.item.label);
	$('input[name=\'filter_name_id\']').val(ui.item.value);
	return false;
  },
  focus: function(event, ui) {
	return false;
  }
});
$('#region').on('change', function() {
  region = $(this).val();
  $.ajax({
  url: 'index.php?route=catalog/employee/getdivision_location&token=<?php echo $token; ?>&filter_region_id=' +  encodeURIComponent(region),
  dataType: 'json',
  success: function(json) {   
	$('#division').find('option').remove();
	if(json['division_datas']){
	$.each(json['division_datas'], function (i, item) {
	  $('#division').append($('<option>', { 
		value: item.division_id,
		text : item.division 
	  }));
	});
	}
	$('#unit').find('option').remove();
	if(json['unit_datas']){
	$.each(json['unit_datas'], function (i, item) {
	  $('#unit').append($('<option>', { 
		value: item.unit_id,
		text : item.unit 
	  }));
	});
	}
  }
  });
});

$('#division').on('change', function() {
  division = $(this).val();
  $.ajax({
  url: 'index.php?route=catalog/employee/getlocation&token=<?php echo $token; ?>&filter_division_id=' +  encodeURIComponent(division),
  dataType: 'json',
  success: function(json) {   
	$('#unit').find('option').remove();
	if(json['unit_datas']){
	$.each(json['unit_datas'], function (i, item) {
	  $('#unit').append($('<option>', { 
		value: item.unit_id,
		text : item.unit 
	  }));
	});
	}
  }
  });
});


//$(document).ready(function() {
 // $('#date-start').datepicker({dateFormat: 'yy-mm-dd'});
  
//});
</script>
<?php echo $footer; ?>