<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
</head>
<body>
<?php $i = 1; ?>
<div style="page-break-after: always;">
  <h1 style="text-align:center;">
	<?php echo COMPANY_NAME; ?><br />
	<?php echo $title; ?><br />
	<p style="display:inline;font-size:15px;"><?php echo 'Date : '. $date_start . ' &nbsp;&nbsp;&nbsp;&nbsp; Department : ' . $department; ?></p>
  </h1>
  <table class="product" style="width:100% !important;">
	<tbody>
	  <tr class="">
		<td class="center"><?php echo 'Sr.No'; ?></td>
		<td class="center"><?php echo 'Emp Code'; ?></td>
		<td class="center"><?php echo 'Name'; ?></td>
		<td class="center"><?php echo 'Shift Assigned'; ?></td>
		<td class="center"><?php echo 'Shift Attended'; ?></td>
		<td class="center"><?php echo 'Late time'; ?></td>
	  </tr>
	  <tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td class="center">In</td>
		<td class="center">In</td>
		<td>&nbsp;</td>
	  </tr>
	  <?php if($final_datass) { ?>
		<?php foreach($final_datass as $final_data) { ?>
		  <tr>
			<td class="left" style="font-size:11px;"><?php echo $i; ?></td>
			<td class="left" style="font-size:11px;"><?php echo $final_data['emp_code']; ?></td>
			<td class="left" style="font-size:11px;"><?php echo $final_data['emp_name']; ?></td>
			<td class="left" style="font-size:11px;"><?php echo $final_data['shift_intime']; ?></td>
			<td class="left" style="font-size:11px;"><?php echo $final_data['act_intime']; ?></td>
			<td class="left" style="font-size:11px;">
			  <?php if($final_data['late_time'] != '00:00:00') { ?>
				<span style="background-color:red;">
				  <?php echo $final_data['late_time']; ?>
				</span>
			  <?php } else { ?>
				<?php echo $final_data['late_time']; ?>
			  <?php } ?>
			</td>
		  </tr>
		  <?php $i++; ?>
		<?php } ?>
	  <?php } ?>
	</tbody>
  </table>
</div>

</body>
</html>