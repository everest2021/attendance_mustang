<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
</head>
<body>
  <div style="page-break-after: always;">
    <h1 style="text-align:center;">
      <?php echo COMPANY_NAME; ?><br />
      <?php echo $title; ?><br />
      <?php if($filter_department != '') { ?>
        <p style="display:inline;font-size:15px;"><?php echo 'Department : '. $filter_department; ?></p><br />
      <?php } ?>
      <p style="display:inline;font-size:15px;"><?php echo 'Date : '. $filter_date_start; ?></p>
    </h1>
    <table class="product" style="width:100% !important;">
      <thead>
        <tr>
          <td class="center"><?php echo 'Sr.No'; ?></td>
          <td class="center"><?php echo 'Emp Code'; ?></td>
          <td class="center"><?php echo 'Name'; ?></td>
          <td class="center" colspan='2'><?php echo 'Shift Assigned'; ?></td>
          <td class="center" colspan='4'><?php echo 'Shift Attended'; ?></td>
          <?php if($status == 0 || $status == 1) { ?>
            <td class="center"><?php echo 'Working Hours'; ?></td>
            <td class="center"><?php echo 'Late time'; ?></td>
            <td class="center"><?php echo 'Early time'; ?></td>
          <?php } else { ?>
          <td class="center" colspan='2'><?php echo 'Attendance Status'; ?></td>  
          <?php } ?>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td class="center">In</td>
          <td class="center">Out</td>
          <td class="center" style="width:8%;">In Date</td>
          <td class="center">In Time</td>
          <td class="center" style="width:8%;">Out Date</td>
          <td class="center">Out TIme</td>
          <?php if($status == 0 || $status == 1) { ?>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          <?php } else { ?>
            <td class="center">FH</td>
            <td class="center">SH</td>
          <?php } ?>
        </tr>
      </thead>
      <tbody>
        <?php if($results) { ?>
          <?php $i = 1; ?>
          <?php foreach($results as $result) { ?>
            <tr>
              <td>
                <?php echo $i; ?>
              </td>
              <td>
                <?php echo $result['emp_code']; ?>
              </td>
              <td>
                <?php echo $result['emp_name']; ?>
              </td>
              <td>
                <?php if($result['leave_status'] == 1) { ?>
                  <?php echo $result['firsthalf_status']; ?>
                <?php } elseif($result['weekly_off'] != '0') { ?>
                  <?php echo 'Weekly Off'; ?>
                <?php } elseif($result['holiday_id'] != '0') { ?>
                  <?php echo 'Holiday'; ?>
                <?php } elseif($result['halfday_status'] != '0') { ?>
                  <?php echo 'Half Day'; ?>
                <?php } elseif($result['compli_status'] != '0') { ?>
                  <?php echo 'Comp Off'; ?>
                <?php } else { ?>
                  <?php echo $result['shift_intime']; ?>
                <?php } ?>
              </td>
              <td>
                <?php if($result['leave_status'] == 1) { ?>
                  <?php echo $result['secondhalf_status']; ?>
                <?php } elseif($result['weekly_off'] != '0') { ?>
                  <?php echo 'Weekly Off'; ?>
                <?php } elseif($result['holiday_id'] != '0') { ?>
                  <?php echo 'Holiday'; ?>
                <?php } elseif($result['halfday_status'] != '0') { ?>
                  <?php echo 'Half Day'; ?>
                <?php } elseif($result['compli_status'] != '0') { ?>
                  <?php echo 'Comp Off'; ?>
                <?php } else { ?>
                  <?php echo $result['shift_outtime']; ?>
                <?php } ?>
              </td>
              
              <?php if($status == 0 || $status == 1) { ?>
                <td>
                  <?php echo $result['date']; ?>
                </td>
                <td>
                  <?php echo $result['act_intime']; ?>
                </td>
                <td>
                  <?php echo $result['date_out']; ?>
                </td>
                <td>
                  <?php echo $result['act_outtime']; ?>
                </td>
              <?php } else { ?>
                <td>
                  <?php echo $result['date']; ?>
                </td>
                <td>
                  <?php echo $result['act_intime']; ?>
                </td>
                <td>
                  <?php echo $result['date_out']; ?>
                </td>
                <td>
                  <?php echo $result['act_outtime']; ?>
                </td>
              <?php } ?>

              <?php if($status == 0 || $status == 1) { ?>
                <td>
                  <?php if($result['working_time'] == '08:00:00') { ?>
                    <span style="background-color:#13F113;">
                      <?php echo $result['working_time']; ?>
                    </span>
                  <?php } else { ?>
                      <?php echo $result['working_time']; ?>
                  <?php } ?> 
                </td>
                <td>
                  <?php if($result['late_time'] != '00:00:00') { ?>
                    <span style="background-color:red;">
                      <?php echo $result['late_time']; ?>
                    </span>
                  <?php } else { ?>
                    <?php echo $result['late_time']; ?>
                  <?php } ?>
                </td>
                <td>
                  <?php if($result['early_time'] != '00:00:00') { ?>
                    <span style="background-color:red;">
                      <?php echo $result['early_time']; ?>
                    </span>
                  <?php } else { ?>
                    <?php echo $result['early_time']; ?>
                  <?php } ?>
                </td>
              <?php } else { ?>
                <td>
                  <?php if($result['firsthalf_status'] == 1){ ?>
                    <?php echo 'P'; ?>
                  <?php } else { ?>
                    <?php echo 'A' ?>
                  <?php } ?>
                </td>
                <td>
                  <?php if($result['secondhalf_status'] == 1){ ?>
                    <?php echo 'P'; ?>
                  <?php } else { ?>
                    <?php echo 'A' ?>
                  <?php } ?>
                </td>
              <?php } ?>
            </tr>
            <?php $i++; ?>
          <?php } ?>
        <?php } else { ?>
        <tr>
          <td class="center" colspan="13"></td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
  </div>
</body>
</html>
