<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/report.png" alt="" /> <?php echo $heading_title; ?></h1>
    </div>
    <div class="content sales-report">
      <table class="form">
        <tr>
          <td style="width:13%;"><?php echo $entry_date_start; ?>
            <input type="text" name="filter_date_start" value="<?php echo $filter_date_start; ?>" id="date-start" size="12" /></td>
          <td style="width:13%;"><?php echo $entry_doctor; ?>
            <select name="filter_doctor" id="filter_doctor">
                <option value="0"><?php echo $text_all; ?></option>
              <?php foreach($doctors as $dkey => $dvalue) { ?>
                <?php if ($filter_doctor == $dvalue['doctor_id']) { ?>
                  <option value="<?php echo $dvalue['doctor_id'] ?>" selected="selected"><?php echo $dvalue['name']; ?></option>
                <?php } else { ?>
                  <option value="<?php echo $dvalue['doctor_id'] ?>"><?php echo $dvalue['name']; ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </td>
          <td style="text-align: right;">
            <a style="padding: 13px 25px;" onclick="filter();" id="filter" class="button"><?php echo $button_filter; ?></a>
            <a style="padding: 13px 25px;" onclick="filter_export();" id="export" class="button"><?php echo $button_export; ?></a>
          </td>
        </tr>
      </table>
      <h3><?php echo 'Date : ' . $filter_date_start; ?></h3>
      <table class="list">
        <thead>
          <tr>
            <td class="left"><?php echo $column_trainer; ?></td>
            <td class="left"><?php echo $column_horse_name; ?></td>
            <td class="left"><?php echo $column_medicine_name; ?></td>
          </tr>
        </thead>
        <tbody>
          <?php if ($trainer_datas) { ?>
            <?php $total = 0; ?>
            <?php foreach ($trainer_datas as $trainer_data) { ?>
              <tr>
                <td class="left" style="border:none !important;padding-bottom:15px;"><?php echo $trainer_data['trainer_name']; ?></td>
              </tr>
              <?php $cnt = 0; ?>
              <?php $cnt_log = count($trainer_data['horse_data']); ?>
              <?php foreach($trainer_data['horse_data'] as $hkey => $hvalue) { ?>
                <?php $cnt ++; ?>
                <?php if($cnt == $cnt_log) { ?>
                  <tr style="border-bottom:1px solid;">
                <?php } else { ?>
                  <tr style="border-bottom:1px dotted;">
                <?php } ?>
                <td class="left" style="border:none !important;padding-bottom:15px;"></td>
                <td class="left" style="border:none !important;padding-bottom:15px;"><?php echo $hvalue['horse_name']; ?></td>
                <td class="left" style="border:none !important;padding-bottom:15px;">
                  <?php foreach($hvalue['medicine_data'] as $mkey => $mvalue) { ?>
                  <?php echo $mvalue['medicine_name']; ?><br />
                  <?php } ?>
                </td>
              </tr>
              <?php } ?>
            <?php } ?>
          <?php } else { ?>
          <tr>
            <td class="center" colspan="3"><?php echo $text_no_results; ?></td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
      <?php /* ?>
      <div class="pagination"><?php echo $pagination; ?></div>
      <?php */ ?>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
function filter() {
	url = 'index.php?route=report/trainer_wise_log&token=<?php echo $token; ?>';
	
	var filter_date_start = $('input[name=\'filter_date_start\']').attr('value');
	
	if (filter_date_start) {
		url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
	}

	var filter_doctor = $('select[name=\'filter_doctor\']').attr('value');
  
  if (filter_doctor) {
    url += '&filter_doctor=' + encodeURIComponent(filter_doctor);
  }

  location = url;
  return false;
}

function filter_export() {
  url = 'index.php?route=report/trainer_wise_log/export&token=<?php echo $token; ?>';
  
  var filter_date_start = $('input[name=\'filter_date_start\']').attr('value');
  
  if (filter_date_start) {
    url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  }

  var filter_doctor = $('select[name=\'filter_doctor\']').attr('value');
  
  if (filter_doctor) {
    url += '&filter_doctor=' + encodeURIComponent(filter_doctor);
  }

  location = url;
  return false;
}
//--></script> 
<script type="text/javascript"><!--
$(document).ready(function() {
	$('#date-start').datepicker({dateFormat: 'yy-mm-dd'});
});
//--></script>
<?php echo $footer; ?>