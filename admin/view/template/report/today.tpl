<?php echo $header; ?>
<div id="content">
  	<div class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
		<?php } ?>
  	</div>
  	<?php if ($error_warning) { ?>
  	<div class="warning"><?php echo $error_warning; ?></div>
  	<?php } ?>
  	<?php if ($success) { ?>
  	<div class="success"><?php echo $success; ?></div>
  	<?php } ?>
  	<div class="box">
	<div class="heading">
	  	<h1><img src="view/image/report.png" alt="" /> <?php echo "Today's Attendance Report"; ?></h1>
	</div>
	<div class="content sales-report">
	  	<table class="form">
		<tr>
			<td style="width:13%;"><?php echo "Name"; ?>
            	<input type="text" name="filter_name" value="<?php echo $filter_name;  ?>" id="filter_name" size="25" />
            	<input type="hidden" name="filter_name_id" value="<?php echo $filter_name_id; ?>" id="filter_name_id" />
          	</td>
		  	<td style="width:8%">Location
				<select name="unit" style="width:100px">
			  	<?php foreach($unit_data as $key => $ud) { ?>
					<?php if($key == $unit) { ?>
				  		<option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
					<?php } else { ?>
				  		<option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
					<?php } ?>
			  	<?php } ?>
				</select>
		  	</td>
		  	<td style="width:8%">Department
			<select name="department" style="width:100px">
			  	<?php foreach($department_data as $key => $dd) { ?>
					<?php if($key == $department) { ?>
				  		<option value='<?php echo $key; ?>' selected="selected"><?php echo $dd; ?></option> 
					<?php } else { ?>
				  		<option value='<?php echo $key; ?>'><?php echo $dd; ?></option>
					<?php } ?>
			  	<?php } ?>
			</select>
		  	</td>
 			<td style="width:8%">Designation
				<select name="designation" style="width:100px">
			  	<?php foreach($designation_data as $dkey => $dg) { ?>
					<?php if($dkey == $designation) { ?>
				  		<option value='<?php echo $dkey; ?>' selected="selected"><?php echo $dg; ?></option> 
					<?php } else { ?>
				  		<option value='<?php echo $dkey; ?>'><?php echo $dg; ?></option>
					<?php } ?>
			  	<?php } ?>
				</select>
		  	</td>
		  	<td style="text-align: right;">
				<a style="padding: 13px 25px;" onclick="filter();" id="filter" class="button"><?php echo $button_filter; ?></a>
				<a  style="display:none;" href="<?php echo $generate_today; ?>" class="button"><?php echo "Refresh"; ?></a>
				<a style="padding: 13px 25px;" href="<?php echo $export; ?>" class="button"><?php echo "Export"; ?></a>
		  	</td>
		</tr>
	  	</table>
	  	<table class="list">
		<thead>
		  	<tr>
				<td class="center"><?php echo 'Sr.No'; ?></td>
				<td class="center"><?php echo 'Emp Code'; ?></td>
				<td class="center"><?php echo 'Name'; ?></td>
				<td class="center"><?php echo 'Shift Assigned'; ?></td>
				<td class="center"><?php echo 'Shift Attended'; ?></td>
				<td class="center"><?php echo 'Late time'; ?></td>
			</tr>
		  <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td class="center">In</td>
			<td class="center">In</td>
			<td>&nbsp;</td>
		  </tr>
		</thead>
		<tbody>
		  <?php if($results) { ?>
			<?php $i = 1; ?>
			<?php foreach($results as $result) { ?>
			  <tr>
				<td>
				  <?php echo $i; ?>
				</td>
				<td>
				  <?php echo $result['emp_code']; ?>
				</td>
				<td>
				  <?php echo $result['emp_name']; ?>
				</td>
				<td>
				  <?php echo $result['shift_intime']; ?>
				</td>
				<td>
				  <?php echo $result['act_intime']; ?>
				</td>
				<td>
				  <?php if($result['late_time'] != '00:00:00') { ?>
					<span style="background-color:red;">
					  <?php echo $result['late_time']; ?>
					</span>
				  <?php } else { ?>
					<?php echo $result['late_time']; ?>
				  <?php } ?>
				</td>
			  </tr>
			  <?php $i++; ?>
			<?php } ?>
		  <?php } else { ?>
		  <tr>
			<td class="center" colspan="6"><?php echo $text_no_results; ?></td>
		  </tr>
		  <?php } ?>
		</tbody>
	  </table>
	  
	</div>
  </div>
</div>
<script type="text/javascript"><!--
function filter() {
  url = 'index.php?route=report/today&token=<?php echo $token; ?>';
   var filter_name = $('input[name=\'filter_name\']').attr('value');
  
  if (filter_name) {
    url += '&filter_name=' + encodeURIComponent(filter_name);
    var filter_name_id = $('input[name=\'filter_name_id\']').attr('value');
    if (filter_name_id) {
      url += '&filter_name_id=' + encodeURIComponent(filter_name_id);
    } else {
      alert('Please Enter Correct Employee Name');
      return false;
    }
  }
 
  var unit = $('select[name=\'unit\']').attr('value');
  
  if (unit) {
	url += '&unit=' + encodeURIComponent(unit);
  }

  var department = $('select[name=\'department\']').attr('value');
  
  if (department) {
	url += '&department=' + encodeURIComponent(department);
  }

 var designation = $('select[name=\'designation\']').attr('value');
  
  if (designation) {
	url += '&designation=' + encodeURIComponent(designation);
  }
  
  location = url;
  return false;
}

//--></script>

<script type="text/javascript"><!--
$(document).ready(function() {
  
  //$('#date-start').datepicker({dateFormat: 'yy-mm-dd'});
  //$('#date-end').datepicker({dateFormat: 'yy-mm-dd'});

  $(function() {
      //$.datepicker.setDefaults($.datepicker.regional['en']);
      $('#date-start').datepicker({
            dateFormat: 'yy-mm-dd',
            onSelect: function(selectedDate) {
              var date = $(this).datepicker('getDate');
              $('#date-end').datepicker('option', 'maxDate', date); // Reset minimum date
              date.setDate(date.getDate() + 30); // Add 30 days
              $('#date-end').datepicker('setDate', date); // Set as default
            }
      });
      
      $('#date-end').datepicker({
            dateFormat: 'yy-mm-dd',
            onSelect: function(selectedDate) {
              $('#date-start').datepicker('option', 'maxDate', $(this).datepicker('getDate')); // Reset maximum date
            }
      });
      
  });
  
});

$('input[name=\'filter_name\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/employee/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.name,
            value: item.emp_code
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_name\']').val(ui.item.label);
    $('input[name=\'filter_name_id\']').val(ui.item.value);
    return false;
  },
  focus: function(event, ui) {
    return false;
  }
});

$('#region').on('change', function() {
  region = $(this).val();
  $.ajax({
	url: 'index.php?route=catalog/employee/getdivision_location&token=<?php echo $token; ?>&filter_region_id=' +  encodeURIComponent(region),
	dataType: 'json',
	success: function(json) {   
	  $('#division').find('option').remove();
	  if(json['division_datas']){
		$.each(json['division_datas'], function (i, item) {
		  $('#division').append($('<option>', { 
			  value: item.division_id,
			  text : item.division 
		  }));
		});
	  }
	  $('#unit').find('option').remove();
	  if(json['unit_datas']){
		$.each(json['unit_datas'], function (i, item) {
		  $('#unit').append($('<option>', { 
			  value: item.unit_id,
			  text : item.unit 
		  }));
		});
	  }
	}
  });
});

$('#division').on('change', function() {
  division = $(this).val();
  $.ajax({
	url: 'index.php?route=catalog/employee/getlocation&token=<?php echo $token; ?>&filter_division_id=' +  encodeURIComponent(division),
	dataType: 'json',
	success: function(json) {   
	  $('#unit').find('option').remove();
	  if(json['unit_datas']){
		$.each(json['unit_datas'], function (i, item) {
		  $('#unit').append($('<option>', { 
			  value: item.unit_id,
			  text : item.unit 
		  }));
		});
	  }
	}
  });
});


//$(document).ready(function() {
 // $('#date-start').datepicker({dateFormat: 'yy-mm-dd'});
  
//});
</script> 
<?php echo $footer; ?>