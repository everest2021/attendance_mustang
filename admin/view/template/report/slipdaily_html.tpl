<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
</head>
<body>
<div class="box" id="content">
  <?php if($dailyreportss){ ?>
  <?php foreach($dailyreportss as $dkeyss => $dailyreports){ ?>
    <div  style="page-break-after: always;">
      <?php if($dailyreports){ ?>
        <?php foreach($dailyreports as $dkey => $result){ ?>
          <h3 style="text-align:center;">
            <?php echo 'Salary Slip'; ?><br />
            Spectrum Services<br />
          </h3>
          <table class="product" style="margin-bottom: 0px !important;margin-left: auto;margin-right: auto;width: 100%;">
            <tr>
              <td style="text-align: left;font-weight: bold;width: 16.5%;border-right: 0px;border-bottom: 0px;padding: 2px 10px !important;">
                <?php echo 'PaySlip'; ?>
              </td>
              <td style="text-align: left;width: 16.5%;border-right: 0px;border-bottom: 0px;border-left: 0px;padding: 2px 10px !important;">
                <?php echo $result['pay_slip']; ?>
              </td>
              <td style="text-align: left;font-weight: bold;width: 17%;border-right: 0px;border-bottom: 0px;border-left: 0px;padding: 2px 10px !important;">
                <?php echo 'PaySlip for the Month'; ?>
              </td>
              <td style="text-align: left;width: 17%;border-right: 0px;border-bottom: 0px;border-left: 0px;padding: 2px 10px !important;">
                <?php echo $result['month_of']; ?>
              </td>
              <td style="text-align: left;width: 16.5%;border-right: 0px;border-bottom: 0px;border-left: 0px;padding: 2px 10px !important;">
                <?php echo 'Branch'; ?>
              </td>
              <td style="text-align: left;width: 16.5%;border-bottom: 0px;border-left: 0px;padding: 2px 10px !important;">
                <?php echo $result['branch']; ?>
              </td>
            </tr>
            <tr>
              <td style="text-align: left;font-weight: bold;width: 16.5%;border-top: 0px;border-right: 0px;border-bottom: 0px;padding: 2px 10px !important;">
                <?php echo 'Emp Code'; ?>
              </td>
              <td style="text-align: left;width: 16.5%;border-top: 0px;border-right: 0px;border-bottom: 0px;border-left: 0px;padding: 2px 10px !important;">
                <?php echo $result['emp_code']; ?>
              </td>
              <td style="text-align: left;font-weight: bold;width: 17%;border-top: 0px;border-right: 0px;border-bottom: 0px;border-left: 0px;padding: 2px 10px !important;">
                <?php echo 'Name'; ?>
              </td>
              <td style="text-align: left;width: 50%;border-top: 0px;border-bottom: 0px;border-left: 0px;padding: 2px 10px !important;" colspan="3">
                <?php echo $result['emp_name']; ?>
              </td>
            </tr>
            <tr>
              <td style="text-align: left;font-weight: bold;width: 16.5%;border-top: 0px;border-right: 0px;border-bottom: 0px;padding: 2px 10px !important;">
                <?php echo 'Grade'; ?>
              </td>
              <td style="text-align: left;width: 83.5%;border-top: 0px;border-bottom: 0px;border-left: 0px;padding: 2px 10px !important;" colspan="5">
                <?php echo $result['grade']; ?>
              </td>
            </tr>
            <tr>
              <td style="text-align: left;font-weight: bold;width: 16.5%;border-top: 0px;border-right: 0px;border-bottom: 0px;padding: 2px 10px !important;">
                <?php echo 'ESIC No'; ?>
              </td>
              <td style="text-align: left;width: 16.5%;border-top: 0px;border-right: 0px;border-bottom: 0px;border-left: 0px;padding: 2px 10px !important;">
                <?php echo $result['esic_no']; ?>
              </td>
              <td style="text-align: left;font-weight: bold;width: 16.5%;border-top: 0px;border-right: 0px;border-bottom: 0px;border-left: 0px;padding: 2px 10px !important;">
                <?php echo 'PF No'; ?>
              </td>
              <td style="text-align: left;width: 50%;border-top: 0px;border-bottom: 0px;border-left: 0px;padding: 2px 10px !important;" colspan="3">
                <?php echo $result['pf_no']; ?>
              </td>
            </tr>
            <tr>
              <td style="text-align: left;font-weight: bold;width: 16.5%;border-top: 0px;border-right: 0px;border-bottom: 0px;padding: 2px 10px !important;">
                <?php echo 'Joining Date'; ?>
              </td>
              <td style="text-align: left;width: 16.5%;border-top: 0px;border-right: 0px;border-bottom: 0px;border-left: 0px;padding: 2px 10px !important;">
                <?php echo $result['doj']; ?>
              </td>
              <td style="text-align: left;font-weight: bold;width: 16.5%;border-top: 0px;border-right: 0px;border-bottom: 0px;border-left: 0px;padding: 2px 10px !important;">
                <?php echo 'Emp PAN No'; ?>
              </td>
              <td style="text-align: left;width: 50%;border-top: 0px;border-bottom: 0px;border-left: 0px;padding: 2px 10px !important;" colspan="3">
                <?php echo $result['pan_no']; ?>
              </td>
            </tr>
          </table>
          <table class="product" style="margin-bottom: 20px !important;margin-left: auto;margin-right: auto;width: 100%;">
            <tr>
              <td style="text-align: left;width:25%;font-weight: bold;border-right: 0px;border-bottom: 2px solid;border-top: 2px solid;padding: 2px 10px !important;">
                <?php echo 'Earnings & Reimbursements'; ?>
              </td>
              <td style="text-align: right;width:12.5%;font-weight: bold;border-right: 0px;border-bottom: 2px solid;border-top: 2px solid;border-left: 0px;padding: 2px 10px !important;">
                <?php echo 'Gross Amt'; ?>
              </td>
              <td style="text-align: right;width:12.5%;font-weight: bold;border-right: 2px solid #000000;border-bottom: 2px solid;border-top: 2px solid;border-left: 0px;padding: 2px 10px !important;">
                <?php echo 'Actual Amt'; ?>
              </td>
              <td style="text-align: left;width:25%;font-weight: bold;border-right: 0px;border-bottom: 2px solid;border-top: 2px solid;padding: 2px 10px !important;">
                <?php echo 'Deduction & Recoveries'; ?>
              </td>
              <td style="text-align: right;width:12.5%;font-weight: bold;border-right: 0px;border-bottom: 2px solid;border-top: 2px solid;border-left: 0px;padding: 2px 10px !important;">
                <?php echo 'Gross Amt'; ?>
              </td>
              <td style="text-align: right;width:12.5%;font-weight: bold;border-bottom: 2px solid;border-top: 2px solid;border-left: 0px;border-right:1px solid #CDDDDD;padding: 2px 10px !important;">
                <?php echo 'Actual Amt'; ?>
              </td>
            </tr>
            <tr>
              <td style="text-align: left;width:25%;font-weight: bold;border-right: 0px;border-bottom: 0px;padding: 2px 10px !important;">
                <?php echo 'Basic'; ?>
              </td>
              <td style="text-align: right;width:12.5%;border-right: 0px;border-bottom: 0px;border-left: 0px;padding: 2px 10px !important;">
                <?php echo $result['basic']; ?>
              </td>
              <td style="text-align: right;width:12.5%;border-right: 2px solid #000000;border-bottom: 0px;border-left: 0px;padding: 2px 10px !important;">
                <?php echo $result['earned_basic']; ?>
              </td>
              <td style="text-align: left;width:25%;font-weight: bold;border-right: 0px;border-bottom: 0px;padding: 2px 10px !important;">
                <?php echo 'PROV.FUND'; ?>
              </td>
              <td style="text-align: right;width:12.5%;border-right: 0px;border-bottom: 0px;border-left: 0px;padding: 2px 10px !important;">
                <?php echo $result['pf']; ?>
              </td>
              <td style="text-align: right;width:12.5%;border-bottom: 0px;border-left: 0px;border-right:1px solid #CDDDDD;padding: 2px 10px !important;">
                <?php echo $result['earned_pf']; ?>
              </td>
            </tr>
            <tr>
              <td style="text-align: left;width:25%;font-weight: bold;border-right: 0px;border-bottom: 0px;border-top: 0px;padding: 2px 10px !important;">
                <?php echo 'Wages'; ?>
              </td>
              <td style="text-align: right;width:12.5%;border-right: 0px;border-bottom: 0px;border-top: 0px;border-left: 0px;padding: 2px 10px !important;">
                <?php echo $result['wages']; ?>
              </td>
              <td style="text-align: right;width:12.5%;border-right: 2px solid #000000;border-bottom: 0px;border-top: 0px;border-left: 0px;padding: 2px 10px !important;">
                <?php echo $result['earned_wages']; ?>
              </td>
              <td style="text-align: left;width:25%;font-weight: bold;border-right: 0px;border-bottom: 0px;border-top: 0px;padding: 2px 10px !important;">
                <?php echo 'P.Tax'; ?>
              </td>
              <td style="text-align: right;width:12.5%;border-right: 0px;border-bottom: 0px;border-top: 0px;border-left: 0px;padding: 2px 10px !important;">
                <?php echo $result['pt']; ?>
              </td>
              <td style="text-align: right;width:12.5%;border-bottom: 0px;border-top: 0px;border-left: 0px;border-right:1px solid #CDDDDD;padding: 2px 10px !important;">
                <?php echo $result['earned_pt']; ?>
              </td>
            </tr>
            <tr>
              <td style="text-align: left;width:25%;font-weight: bold;border-right: 0px;border-bottom: 0px;border-top: 0px;">
                <?php echo 'H.R.A'; ?>
              </td>
              <td style="text-align: right;width:12.5%;border-right: 0px;border-bottom: 0px;border-top: 0px;border-left: 0px;">
                <?php echo $result['hra']; ?>
              </td>
              <td style="text-align: right;width:12.5%;border-right: 2px solid #000000;border-bottom: 0px;border-top: 0px;border-left: 0px;">
                <?php echo $result['earned_hra']; ?>
              </td>
              <td style="text-align: left;width:25%;font-weight: bold;border-right: 0px;border-bottom: 0px;border-top: 0px;">
                <?php echo 'E.S.I.C'; ?>
              </td>
              <td style="text-align: right;width:12.5%;border-right: 0px;border-bottom: 0px;border-top: 0px;border-left: 0px;">
                <?php echo $result['esic']; ?>
              </td>
              <td style="text-align: right;width:12.5%;border-bottom: 0px;border-top: 0px;border-left: 0px;">
                <?php echo $result['earned_esic']; ?>
              </td>
            </tr>
            <tr>
              <td style="text-align: left;width:25%;font-weight: bold;border-right: 0px;border-bottom: 0px;border-top: 0px;">
                <?php echo 'Special Allw.'; ?>
              </td>
              <td style="text-align: right;width:12.5%;border-right: 0px;border-bottom: 0px;border-top: 0px;border-left: 0px;">
                <?php echo $result['other_allowance']; ?>
              </td>
              <td style="text-align: right;width:12.5%;border-right: 2px solid #000000;border-bottom: 0px;border-top: 0px;border-left: 0px;">
                <?php echo $result['earned_other_allowance']; ?>
              </td>
              <td style="text-align: left;width:25%;font-weight: bold;border-right: 0px;border-bottom: 0px;border-top: 0px;">
                &nbsp;
              </td>
              <td style="text-align: right;width:12.5%;border-right: 0px;border-bottom: 0px;border-top: 0px;border-left: 0px;">
                &nbsp;
              </td>
              <td style="text-align: right;width:12.5%;border-bottom: 0px;border-top: 0px;border-left: 0px;">
                &nbsp;
              </td>
            </tr>
            <tr>
              <td style="text-align: left;width:25%;font-weight: bold;border-right: 0px;border-bottom: 0px;border-top: 0px;padding: 2px 10px !important;">
                &nbsp;
              </td>
              <td style="text-align: left;width:12.5%;font-weight: bold;border-right: 0px;border-bottom: 0px;border-top: 0px;border-left: 0px;padding: 2px 10px !important;">
                &nbsp;
              </td>
              <td style="text-align: left;width:12.5%;font-weight: bold;border-right: 2px solid #000000;border-bottom: 0px;border-top: 0px;border-left: 0px;padding: 2px 10px !important;">
                &nbsp;
              </td>
              <td style="text-align: left;width:25%;font-weight: bold;border-right: 0px;border-bottom: 0px;border-top: 0px;padding: 2px 10px !important;">
                &nbsp;
              </td>
              <td style="text-align: left;width:12.5%;font-weight: bold;border-right: 0px;border-bottom: 0px;border-top: 0px;border-left: 0px;padding: 2px 10px !important;">
                &nbsp;
              </td>
              <td style="text-align: left;width:12.5%;font-weight: bold;border-bottom: 0px;border-top: 0px;border-left: 0px;border-right:1px solid #CDDDDD;padding: 2px 10px !important;">
                &nbsp;
              </td>
            </tr>
            <tr>
              <td style="text-align: left;width:25%;font-weight: bold;border-right: 0px;border-bottom: 0px;border-top: 0px;padding: 2px 10px !important;">
                &nbsp;
              </td>
              <td style="text-align: left;width:12.5%;font-weight: bold;border-right: 0px;border-bottom: 0px;border-top: 0px;border-left: 0px;padding: 2px 10px !important;">
                &nbsp;
              </td>
              <td style="text-align: left;width:12.5%;font-weight: bold;border-right: 2px solid #000000;border-bottom: 0px;border-top: 0px;border-left: 0px;padding: 2px 10px !important;">
                &nbsp;
              </td>
              <td style="text-align: left;width:25%;font-weight: bold;border-right: 0px;border-bottom: 0px;border-top: 0px;padding: 2px 10px !important;">
                &nbsp;
              </td>
              <td style="text-align: left;width:12.5%;font-weight: bold;border-right: 0px;border-bottom: 0px;border-top: 0px;border-left: 0px;padding: 2px 10px !important;">
                &nbsp;
              </td>
              <td style="text-align: left;width:12.5%;font-weight: bold;border-bottom: 0px;border-top: 0px;border-left: 0px;border-right:1px solid #CDDDDD;padding: 2px 10px !important;">
                &nbsp;
              </td>
            </tr>
            <tr>
              <td style="text-align: left;width:25%;font-weight: bold;border-right: 0px;border-bottom: 0px;border-top: 0px;padding: 2px 10px !important;">
                &nbsp;
              </td>
              <td style="text-align: left;width:12.5%;font-weight: bold;border-right: 0px;border-bottom: 0px;border-top: 0px;border-left: 0px;padding: 2px 10px !important;">
                &nbsp;
              </td>
              <td style="text-align: left;width:12.5%;font-weight: bold;border-right: 2px solid #000000;border-bottom: 0px;border-top: 0px;border-left: 0px;padding: 2px 10px !important;">
                &nbsp;
              </td>
              <td style="text-align: left;width:25%;font-weight: bold;border-right: 0px;border-bottom: 0px;border-top: 0px;padding: 2px 10px !important;">
                &nbsp;
              </td>
              <td style="text-align: left;width:12.5%;font-weight: bold;border-right: 0px;border-bottom: 0px;border-top: 0px;border-left: 0px;padding: 2px 10px !important;">
                &nbsp;
              </td>
              <td style="text-align: left;width:12.5%;font-weight: bold;border-bottom: 0px;border-top: 0px;border-left: 0px;border-right:1px solid #CDDDDD;padding: 2px 10px !important;">
                &nbsp;
              </td>
            </tr>
            <tr>
              <td style="text-align: left;width:25%;font-weight: bold;border-right: 0px;border-bottom: 0px;border-top: 2px solid;padding: 2px 10px !important;">
                <?php echo 'Total Earning'; ?>
              </td>
              <td style="text-align: right;width:12.5%;font-weight: bold;border-right: 0px;border-bottom: 0px;border-top: 2px solid;border-left: 0px;padding: 2px 10px !important;">
                <?php echo $result['earning_amt']; ?>
              </td>
              <td style="text-align: right;width:12.5%;font-weight: bold;border-right: 2px solid #000000;border-bottom: 0px;border-top: 2px solid;border-left: 0px;padding: 2px 10px !important;">
                <?php echo $result['earned_earning_amt']; ?>
              </td>
              <td style="text-align: left;width:25%;font-weight: bold;border-right: 0px;border-bottom: 0px;border-top: 2px solid;padding: 2px 10px !important;">
                <?php echo 'Total Deduction'; ?>
              </td>
              <td style="text-align: right;width:12.5%;font-weight: bold;border-right: 0px;border-bottom: 0px;border-top: 2px solid;border-left: 0px;padding: 2px 10px !important;">
                <?php echo $result['deduction']; ?>
              </td>
              <td style="text-align: right;width:12.5%;font-weight: bold;border-bottom: 0px;border-top: 2px solid;border-left: 0px;border-right:1px solid #CDDDDD;padding: 2px 10px !important;">
                <?php echo $result['earned_deduction']; ?>
              </td>
            </tr>
            <tr>
              <td style="text-align: left;width:25%;font-weight: bold;border-right: 0px;border-top: 0px;border-bottom: 2px solid;padding: 2px 10px !important;">
                &nbsp;
              </td>
              <td style="text-align: left;width:12.5%;font-weight: bold;border-right: 0px;border-top: 0px;border-left: 0px;border-bottom: 2px solid;padding: 2px 10px !important;">
                &nbsp;
              </td>
              <td style="text-align: left;width:12.5%;font-weight: bold;border-right: 2px solid #000000;border-top: 0px;border-left: 0px;border-bottom: 2px solid;padding: 2px 10px !important;">
                &nbsp;
              </td>
              <td style="text-align: left;width:25%;font-weight: bold;border-right: 0px;border-top: 0px;border-bottom: 2px solid;padding: 2px 10px !important;">
                <?php echo 'Net Pay'; ?>
              </td>
              <td style="text-align: left;width:12.5%;font-weight: bold;border-right: 0px;border-top: 0px;border-left: 0px;border-bottom: 2px solid;padding: 2px 10px !important;">
                <?php echo ''; ?>
              </td>
              <td style="text-align: right;width:12.5%;font-weight: bold;border-top: 0px;border-left: 0px;border-bottom: 2px solid;border-right:1px solid #CDDDDD;padding: 2px 10px !important;">
                <?php echo $result['net_paid']; ?>
              </td>
            </tr>
            <tr>
              <td style="text-align: left;width:100%;font-weight: bold;border-bottom: 2px solid;" colspan="6">
                Net Pay: Rupees <?php echo decimal_to_words($result['net_paid']) ?>
              </td>
            </tr>
            <tr>
              <td style="text-align: left;font-weight: bold;border-right:0px;border-bottom: 2px solid;" colspan="2">
                Checked By
              </td>
              <td style="text-align: left;font-weight: bold;border-left:0px;border-right:0px;border-bottom: 2px solid;" colspan="2">
                Approved By
              </td>
              <td style="text-align: left;font-weight: bold;border-left:0px;border-bottom: 2px solid;border-right:1px solid #CDDDDD;" colspan="2">
                Received By
              </td>
            </tr>
          </table>
          <?php if($dkey == '0'){ ?>
            <hr style="margin-bottom: 20px !important;border-top: dotted 2px;" />
          <?php } ?>
        <?php } ?>
      <?php } ?>
    </div>
  <?php } ?>
  <?php } ?>
</div></body></html>
<?php
  function decimal_to_words($x) {
    $x = str_replace(',','',$x);
    $pos = strpos((string)$x, ".");
    if ($pos !== false) { $decimalpart= substr($x, $pos+1, 2); $x = substr($x,0,$pos); }
    $tmp_str_rtn = number_to_words ($x);
    if(!empty($decimalpart) && $decimalpart != '00' && $decimalpart != '0') {
      $tmp_str_rtn .= ' and '  . number_to_words ($decimalpart) . ' paise only';
    } else {
      $tmp_str_rtn .= ' only';  
    }
    return   strtoupper($tmp_str_rtn);
  } 

  function number_to_words ($x) {
      $nwords = array(  "", "one", "two", "three", "four", "five", "six", 
            "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", 
            "fourteen", "fifteen", "sixteen", "seventeen", "eightteen", 
          "nineteen", "twenty", 30 => "thirty", 40 => "fourty",
                     50 => "fifty", 60 => "sixty", 70 => "seventy", 80 => "eigthy",
                     90 => "ninety" );
      //global $nwords; 
       if(!is_numeric($x))
       {
           $w = '#';
       }else if(fmod($x, 1) != 0)
       {
           $w = '#';
       }else{
           if($x < 0)
           {
               $w = 'minus ';
               $x = -$x;
           }else{
               $w = '';
           }
           if($x < 21)
           {
              if(!isset($nwords[$x])){
                  $x = ltrim($x, '0');
              }
        if($x != ''){ 
                $w .= $nwords[$x];
        }
           }else if($x < 100)
           {
               $w .= $nwords[10 * floor($x/10)];
               $r = fmod($x, 10);
               if($r > 0)
               {
                   $w .= ' '. $nwords[$r];
               }
           } else if($x < 1000)
           {
      
               $w .= $nwords[floor($x/100)] .' hundred';
               $r = fmod($x, 100);
               if($r > 0)
               {
                   $w .= ' '. number_to_words($r);
               }
           } else if($x < 100000)
           {
            $w .= number_to_words(floor($x/1000)) .' thousand';
               $r = fmod($x, 1000);
               if($r > 0)
               {
                   $w .= ' ';
                   if($r < 100)
                   {
                       $w .= ' ';
                   }
                   $w .= number_to_words($r);
               }
           } else {
               $w .= number_to_words(floor($x/100000)) .' lakh';
               $r = fmod($x, 100000);
               if($r > 0)
               {
                   $w .= ' ';
                   if($r < 100)
                   {
                       $word .= ' ';
                   }
                   $w .= number_to_words($r);
               }
           }
       }
       return $w;
  }
?>