<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/report.png" alt="" /> <?php echo $heading_title; ?></h1>
    </div>
    <div class="content sales-report">
      <table class="form">
        <tr>
          <td style="width:13%;"><?php echo "Name"; ?>
            <input type="text" name="filter_name" value="<?php echo $filter_name;  ?>" id="filter_name" size="25" />
            <input type="hidden" name="filter_name_id" value="<?php echo $filter_name_id; ?>" id="filter_name_id" />
          </td>
          <td style="width:8%;"><?php echo "Date Start"; ?>
            <input type="text" name="filter_date_start" value="<?php echo $filter_date_start; ?>" id="date-start" size="10" />
          </td>
          <td style="width:8%;"><?php echo "Date End"; ?>
            <input type="text" name="filter_date_end" value="<?php echo $filter_date_end; ?>" id="date-end" size="10" />
          </td>
          <td style="width:10%;"><?php echo "Gender"; ?>
            <select name="filter_gender" id="filter_gender">
              <?php foreach($genders as $gkey => $gvalue){ ?>
                <?php if($gkey == $filter_gender){ ?>
                  <option value="<?php echo $gkey ?>" selected="selected"><?php echo $gvalue ?></option>
                <?php } else { ?>
                  <option value="<?php echo $gkey ?>"><?php echo $gvalue ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </td>
          <td style="text-align: right;border-bottom: 0px;">
            <a style="padding: 13px 25px;" onclick="filter();" id="filter" class="button"><?php echo $button_filter; ?></a>
            <a style="padding: 13px 25px;" href="<?php echo $export; ?>" id="filter_export" class="button"><?php echo 'Export'; ?></a>
          </td>
        </tr>
      </table>
      <?php if($dailyreports){ ?>
        <?php foreach($dailyreports as $dkey => $result){ ?>
          <table class="list" style="width: 60%;">
            <tr>
              <td style="text-align: left;font-weight: bold;width: 16.5%;border-right: 0px;border-bottom: 0px;">
                <?php echo 'PaySlip'; ?>
              </td>
              <td style="text-align: left;width: 16.5%;border-right: 0px;border-bottom: 0px;border-left: 0px;">
                <?php echo $result['pay_slip']; ?>
              </td>
              <td style="text-align: left;font-weight: bold;width: 17%;border-right: 0px;border-bottom: 0px;border-left: 0px;">
                <?php echo 'PaySlip for the Month'; ?>
              </td>
              <td style="text-align: left;width: 17%;border-right: 0px;border-bottom: 0px;border-left: 0px;">
                <?php echo $result['month_of']; ?>
              </td>
              <td style="text-align: left;width: 16.5%;border-right: 0px;border-bottom: 0px;border-left: 0px;">
                <?php echo 'Branch'; ?>
              </td>
              <td style="text-align: left;width: 16.5%;border-bottom: 0px;border-left: 0px;">
                <?php echo $result['branch']; ?>
              </td>
            </tr>
            <tr>
              <td style="text-align: left;font-weight: bold;width: 16.5%;border-top: 0px;border-right: 0px;border-bottom: 0px;">
                <?php echo 'Emp Code'; ?>
              </td>
              <td style="text-align: left;width: 16.5%;border-top: 0px;border-right: 0px;border-bottom: 0px;border-left: 0px;">
                <?php echo $result['emp_code']; ?>
              </td>
              <td style="text-align: left;font-weight: bold;width: 17%;border-top: 0px;border-right: 0px;border-bottom: 0px;border-left: 0px;">
                <?php echo 'Name'; ?>
              </td>
              <td style="text-align: left;width: 50%;border-top: 0px;border-bottom: 0px;border-left: 0px;" colspan="3">
                <?php echo $result['emp_name']; ?>
              </td>
            </tr>
            <tr>
              <td style="text-align: left;font-weight: bold;width: 16.5%;border-top: 0px;border-right: 0px;border-bottom: 0px;">
                <?php echo 'Grade'; ?>
              </td>
              <td style="text-align: left;width: 83.5%;border-top: 0px;border-bottom: 0px;border-left: 0px;" colspan="5">
                <?php echo $result['grade']; ?>
              </td>
            </tr>
            <tr>
              <td style="text-align: left;font-weight: bold;width: 16.5%;border-top: 0px;border-right: 0px;border-bottom: 0px;">
                <?php echo 'ESIC No'; ?>
              </td>
              <td style="text-align: left;width: 16.5%;border-top: 0px;border-right: 0px;border-bottom: 0px;border-left: 0px;">
                <?php echo $result['esic_no']; ?>
              </td>
              <td style="text-align: left;font-weight: bold;width: 16.5%;border-top: 0px;border-right: 0px;border-bottom: 0px;border-left: 0px;">
                <?php echo 'PF No'; ?>
              </td>
              <td style="text-align: left;width: 50%;border-top: 0px;border-bottom: 0px;border-left: 0px;" colspan="3">
                <?php echo $result['pf_no']; ?>
              </td>
            </tr>
            <tr>
              <td style="text-align: left;font-weight: bold;width: 16.5%;border-top: 0px;border-right: 0px;border-bottom: 0px;">
                <?php echo 'Joining Date'; ?>
              </td>
              <td style="text-align: left;width: 16.5%;border-top: 0px;border-right: 0px;border-bottom: 0px;border-left: 0px;">
                <?php echo $result['doj']; ?>
              </td>
              <td style="text-align: left;font-weight: bold;width: 16.5%;border-top: 0px;border-right: 0px;border-bottom: 0px;border-left: 0px;">
                <?php echo 'Emp PAN No'; ?>
              </td>
              <td style="text-align: left;width: 50%;border-top: 0px;border-bottom: 0px;border-left: 0px;" colspan="3">
                <?php echo $result['pan_no']; ?>
              </td>
            </tr>
          </table>
          <table class="list" style="width: 60%;">
            <tr>
              <td style="text-align: left;width:25%;font-weight: bold;border-right: 0px;border-bottom: 2px solid;border-top: 2px solid;">
                <?php echo 'Earnings & Reimbursements'; ?>
              </td>
              <td style="text-align: right;width:12.5%;font-weight: bold;border-right: 0px;border-bottom: 2px solid;border-top: 2px solid;border-left: 0px;">
                <?php echo 'Gross Amt'; ?>
              </td>
              <td style="text-align: right;width:12.5%;font-weight: bold;border-right: 2px solid #000000;border-bottom: 2px solid;border-top: 2px solid;border-left: 0px;">
                <?php echo 'Actual Amt'; ?>
              </td>
              <td style="text-align: left;width:25%;font-weight: bold;border-right: 0px;border-bottom: 2px solid;border-top: 2px solid;">
                <?php echo 'Deduction & Recoveries'; ?>
              </td>
              <td style="text-align: right;width:12.5%;font-weight: bold;border-right: 0px;border-bottom: 2px solid;border-top: 2px solid;border-left: 0px;">
                <?php echo 'Gross Amt'; ?>
              </td>
              <td style="text-align: right;width:12.5%;font-weight: bold;border-bottom: 2px solid;border-top: 2px solid;border-left: 0px;">
                <?php echo 'Actual Amt'; ?>
              </td>
            </tr>
            <tr>
              <td style="text-align: left;width:25%;font-weight: bold;border-right: 0px;border-bottom: 0px;">
                <?php echo 'Basic'; ?>
              </td>
              <td style="text-align: right;width:12.5%;border-right: 0px;border-bottom: 0px;border-left: 0px;">
                <?php echo $result['basic']; ?>
              </td>
              <td style="text-align: right;width:12.5%;border-right: 2px solid #000000;border-bottom: 0px;border-left: 0px;">
                <?php echo $result['earned_basic']; ?>
              </td>
              <td style="text-align: left;width:25%;font-weight: bold;border-right: 0px;border-bottom: 0px;">
                <?php echo 'PROV.FUND'; ?>
              </td>
              <td style="text-align: right;width:12.5%;border-right: 0px;border-bottom: 0px;border-left: 0px;">
                <?php echo $result['pf']; ?>
              </td>
              <td style="text-align: right;width:12.5%;border-bottom: 0px;border-left: 0px;">
                <?php echo $result['earned_pf']; ?>
              </td>
            </tr>
            <tr>
              <td style="text-align: left;width:25%;font-weight: bold;border-right: 0px;border-bottom: 0px;border-top: 0px;">
                <?php echo 'Wages'; ?>
              </td>
              <td style="text-align: right;width:12.5%;border-right: 0px;border-bottom: 0px;border-top: 0px;border-left: 0px;">
                <?php echo $result['wages']; ?>
              </td>
              <td style="text-align: right;width:12.5%;border-right: 2px solid #000000;border-bottom: 0px;border-top: 0px;border-left: 0px;">
                <?php echo $result['earned_wages']; ?>
              </td>
              <td style="text-align: left;width:25%;font-weight: bold;border-right: 0px;border-bottom: 0px;border-top: 0px;">
                <?php echo 'P.Tax'; ?>
              </td>
              <td style="text-align: right;width:12.5%;border-right: 0px;border-bottom: 0px;border-top: 0px;border-left: 0px;">
                <?php echo $result['pt']; ?>
              </td>
              <td style="text-align: right;width:12.5%;border-bottom: 0px;border-top: 0px;border-left: 0px;">
                <?php echo $result['earned_pt']; ?>
              </td>
            </tr>
            <tr>
              <td style="text-align: left;width:25%;font-weight: bold;border-right: 0px;border-bottom: 0px;border-top: 0px;">
                <?php echo 'H.R.A'; ?>
              </td>
              <td style="text-align: right;width:12.5%;border-right: 0px;border-bottom: 0px;border-top: 0px;border-left: 0px;">
                <?php echo $result['hra']; ?>
              </td>
              <td style="text-align: right;width:12.5%;border-right: 2px solid #000000;border-bottom: 0px;border-top: 0px;border-left: 0px;">
                <?php echo $result['earned_hra']; ?>
              </td>
              <td style="text-align: left;width:25%;font-weight: bold;border-right: 0px;border-bottom: 0px;border-top: 0px;">
                <?php echo 'E.S.I.C'; ?>
              </td>
              <td style="text-align: right;width:12.5%;border-right: 0px;border-bottom: 0px;border-top: 0px;border-left: 0px;">
                <?php echo $result['esic']; ?>
              </td>
              <td style="text-align: right;width:12.5%;border-bottom: 0px;border-top: 0px;border-left: 0px;">
                <?php echo $result['earned_esic']; ?>
              </td>
            </tr>
            <tr>
              <td style="text-align: left;width:25%;font-weight: bold;border-right: 0px;border-bottom: 0px;border-top: 0px;">
                <?php echo 'Special Allw.'; ?>
              </td>
              <td style="text-align: right;width:12.5%;border-right: 0px;border-bottom: 0px;border-top: 0px;border-left: 0px;">
                <?php echo $result['other_allowance']; ?>
              </td>
              <td style="text-align: right;width:12.5%;border-right: 2px solid #000000;border-bottom: 0px;border-top: 0px;border-left: 0px;">
                <?php echo $result['earned_other_allowance']; ?>
              </td>
              <td style="text-align: left;width:25%;font-weight: bold;border-right: 0px;border-bottom: 0px;border-top: 0px;">
                &nbsp;
              </td>
              <td style="text-align: right;width:12.5%;border-right: 0px;border-bottom: 0px;border-top: 0px;border-left: 0px;">
                &nbsp;
              </td>
              <td style="text-align: right;width:12.5%;border-bottom: 0px;border-top: 0px;border-left: 0px;">
                &nbsp;
              </td>
            </tr>
            <tr>
              <td style="text-align: left;width:25%;font-weight: bold;border-right: 0px;border-bottom: 0px;border-top: 0px;">
                &nbsp;
              </td>
              <td style="text-align: left;width:12.5%;font-weight: bold;border-right: 0px;border-bottom: 0px;border-top: 0px;border-left: 0px;">
                &nbsp;
              </td>
              <td style="text-align: left;width:12.5%;font-weight: bold;border-right: 2px solid #000000;border-bottom: 0px;border-top: 0px;border-left: 0px;">
                &nbsp;
              </td>
              <td style="text-align: left;width:25%;font-weight: bold;border-right: 0px;border-bottom: 0px;border-top: 0px;">
                &nbsp;
              </td>
              <td style="text-align: left;width:12.5%;font-weight: bold;border-right: 0px;border-bottom: 0px;border-top: 0px;border-left: 0px;">
                &nbsp;
              </td>
              <td style="text-align: left;width:12.5%;font-weight: bold;border-bottom: 0px;border-top: 0px;border-left: 0px;">
                &nbsp;
              </td>
            </tr>
            <tr>
              <td style="text-align: left;width:25%;font-weight: bold;border-right: 0px;border-bottom: 0px;border-top: 0px;">
                &nbsp;
              </td>
              <td style="text-align: left;width:12.5%;font-weight: bold;border-right: 0px;border-bottom: 0px;border-top: 0px;border-left: 0px;">
                &nbsp;
              </td>
              <td style="text-align: left;width:12.5%;font-weight: bold;border-right: 2px solid #000000;border-bottom: 0px;border-top: 0px;border-left: 0px;">
                &nbsp;
              </td>
              <td style="text-align: left;width:25%;font-weight: bold;border-right: 0px;border-bottom: 0px;border-top: 0px;">
                &nbsp;
              </td>
              <td style="text-align: left;width:12.5%;font-weight: bold;border-right: 0px;border-bottom: 0px;border-top: 0px;border-left: 0px;">
                &nbsp;
              </td>
              <td style="text-align: left;width:12.5%;font-weight: bold;border-bottom: 0px;border-top: 0px;border-left: 0px;">
                &nbsp;
              </td>
            </tr>
            <tr>
              <td style="text-align: left;width:25%;font-weight: bold;border-right: 0px;border-bottom: 0px;border-top: 0px;">
                &nbsp;
              </td>
              <td style="text-align: left;width:12.5%;font-weight: bold;border-right: 0px;border-bottom: 0px;border-top: 0px;border-left: 0px;">
                &nbsp;
              </td>
              <td style="text-align: left;width:12.5%;font-weight: bold;border-right: 2px solid #000000;border-bottom: 0px;border-top: 0px;border-left: 0px;">
                &nbsp;
              </td>
              <td style="text-align: left;width:25%;font-weight: bold;border-right: 0px;border-bottom: 0px;border-top: 0px;">
                &nbsp;
              </td>
              <td style="text-align: left;width:12.5%;font-weight: bold;border-right: 0px;border-bottom: 0px;border-top: 0px;border-left: 0px;">
                &nbsp;
              </td>
              <td style="text-align: left;width:12.5%;font-weight: bold;border-bottom: 0px;border-top: 0px;border-left: 0px;">
                &nbsp;
              </td>
            </tr>
            <tr>
              <td style="text-align: left;width:25%;font-weight: bold;border-right: 0px;border-bottom: 0px;border-top: 2px solid;">
                <?php echo 'Total Earning'; ?>
              </td>
              <td style="text-align: right;width:12.5%;font-weight: bold;border-right: 0px;border-bottom: 0px;border-top: 2px solid;border-left: 0px;">
                <?php echo $result['earning_amt']; ?>
              </td>
              <td style="text-align: right;width:12.5%;font-weight: bold;border-right: 2px solid #000000;border-bottom: 0px;border-top: 2px solid;border-left: 0px;">
                <?php echo $result['earned_earning_amt']; ?>
              </td>
              <td style="text-align: left;width:25%;font-weight: bold;border-right: 0px;border-bottom: 0px;border-top: 2px solid;">
                <?php echo 'Total Deduction'; ?>
              </td>
              <td style="text-align: right;width:12.5%;font-weight: bold;border-right: 0px;border-bottom: 0px;border-top: 2px solid;border-left: 0px;">
                <?php echo $result['deduction']; ?>
              </td>
              <td style="text-align: right;width:12.5%;font-weight: bold;border-bottom: 0px;border-top: 2px solid;border-left: 0px;">
                <?php echo $result['earned_deduction']; ?>
              </td>
            </tr>
            <tr>
              <td style="text-align: left;width:25%;font-weight: bold;border-right: 0px;border-top: 0px;border-bottom: 2px solid;">
                &nbsp;
              </td>
              <td style="text-align: left;width:12.5%;font-weight: bold;border-right: 0px;border-top: 0px;border-left: 0px;border-bottom: 2px solid;">
                &nbsp;
              </td>
              <td style="text-align: left;width:12.5%;font-weight: bold;border-right: 2px solid #000000;border-top: 0px;border-left: 0px;border-bottom: 2px solid;">
                &nbsp;
              </td>
              <td style="text-align: left;width:25%;font-weight: bold;border-right: 0px;border-top: 0px;border-bottom: 2px solid;">
                <?php echo 'Net Pay'; ?>
              </td>
              <td style="text-align: left;width:12.5%;font-weight: bold;border-right: 0px;border-top: 0px;border-left: 0px;border-bottom: 2px solid;">
                <?php echo ''; ?>
              </td>
              <td style="text-align: right;width:12.5%;font-weight: bold;border-top: 0px;border-left: 0px;border-bottom: 2px solid;">
                <?php echo $result['net_paid']; ?>
              </td>
            </tr>
            <tr>
              <td style="text-align: left;width:100%;font-weight: bold;border-bottom: 2px solid;" colspan="6">
                Net Pay: Rupees <?php echo decimal_to_words($result['net_paid']) ?>
              </td>
            </tr>
            <tr>
              <td style="text-align: left;font-weight: bold;border-right:0px;border-bottom: 2px solid;" colspan="2">
                Checked By
              </td>
              <td style="text-align: left;font-weight: bold;border-left:0px;border-right:0px;border-bottom: 2px solid;" colspan="2">
                Approved By
              </td>
              <td style="text-align: left;font-weight: bold;border-left:0px;border-bottom: 2px solid;" colspan="2">
                Received By
              </td>
            </tr>
          </table>
        <?php } ?>
      <?php } ?>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
function filter() {
  url = 'index.php?route=report/slipdaily&token=<?php echo $token; ?>';

  var filter_name = $('input[name=\'filter_name\']').attr('value');
  
  if (filter_name) {
    url += '&filter_name=' + encodeURIComponent(filter_name);
    var filter_name_id = $('input[name=\'filter_name_id\']').attr('value');
    if (filter_name_id) {
      url += '&filter_name_id=' + encodeURIComponent(filter_name_id);
    } else {
      alert('Please Enter Correct Employee Name');
      return false;
    }
  }
  
  var filter_date_start = $('input[name=\'filter_date_start\']').attr('value');
  
  if (filter_date_start) {
    url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  }

  var filter_date_end = $('input[name=\'filter_date_end\']').attr('value');
  
  if (filter_date_end) {
    url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
  }

  var filter_gender = $('select[name=\'filter_gender\']').attr('value');
  if (filter_gender) {
    url += '&filter_gender=' + encodeURIComponent(filter_gender);
  }

  url += '&once=1';
  
  location = url;
  return false;
}

//--></script> 
<script type="text/javascript"><!--
$(document).ready(function() {
  
  //$('#date-start').datepicker({dateFormat: 'yy-mm-dd'});
  //$('#date-end').datepicker({dateFormat: 'yy-mm-dd'});

  $(function() {
      //$.datepicker.setDefaults($.datepicker.regional['en']);
      $('#date-start').datepicker({
            dateFormat: 'yy-mm-dd',
            onSelect: function(selectedDate) {
              var date = $(this).datepicker('getDate');
              $('#date-end').datepicker('option', 'maxDate', date); // Reset minimum date
              date.setDate(date.getDate() + 30); // Add 30 days
              $('#date-end').datepicker('setDate', date); // Set as default
            }
      });
      
      $('#date-end').datepicker({
            dateFormat: 'yy-mm-dd',
            onSelect: function(selectedDate) {
              $('#date-start').datepicker('option', 'maxDate', $(this).datepicker('getDate')); // Reset maximum date
            }
      });
      
  });
  
});

$('input[name=\'filter_name\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=report/dailyattendance/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.name,
            value: item.emp_code
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_name\']').val(ui.item.label);
    $('input[name=\'filter_name_id\']').val(ui.item.value);
    return false;
  },
  focus: function(event, ui) {
    return false;
  }
});

//$(document).ready(function() {
 // $('#date-start').datepicker({dateFormat: 'yy-mm-dd'});
  
//});
</script>
<?php
  function decimal_to_words($x) {
    $x = str_replace(',','',$x);
    $pos = strpos((string)$x, ".");
    if ($pos !== false) { $decimalpart= substr($x, $pos+1, 2); $x = substr($x,0,$pos); }
    $tmp_str_rtn = number_to_words ($x);
    if(!empty($decimalpart) && $decimalpart != '00' && $decimalpart != '0') {
      $tmp_str_rtn .= ' and '  . number_to_words ($decimalpart) . ' paise only';
    } else {
      $tmp_str_rtn .= ' only';  
    }
    return   strtoupper($tmp_str_rtn);
  } 

  function number_to_words ($x) {
      $nwords = array(  "", "one", "two", "three", "four", "five", "six", 
            "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", 
            "fourteen", "fifteen", "sixteen", "seventeen", "eightteen", 
          "nineteen", "twenty", 30 => "thirty", 40 => "fourty",
                     50 => "fifty", 60 => "sixty", 70 => "seventy", 80 => "eigthy",
                     90 => "ninety" );
      //global $nwords; 
       if(!is_numeric($x))
       {
           $w = '#';
       }else if(fmod($x, 1) != 0)
       {
           $w = '#';
       }else{
           if($x < 0)
           {
               $w = 'minus ';
               $x = -$x;
           }else{
               $w = '';
           }
           if($x < 21)
           {
              if(!isset($nwords[$x])){
                  $x = ltrim($x, '0');
              }
        if($x != ''){ 
                $w .= $nwords[$x];
        }
           }else if($x < 100)
           {
               $w .= $nwords[10 * floor($x/10)];
               $r = fmod($x, 10);
               if($r > 0)
               {
                   $w .= ' '. $nwords[$r];
               }
           } else if($x < 1000)
           {
      
               $w .= $nwords[floor($x/100)] .' hundred';
               $r = fmod($x, 100);
               if($r > 0)
               {
                   $w .= ' '. number_to_words($r);
               }
           } else if($x < 100000)
           {
            $w .= number_to_words(floor($x/1000)) .' thousand';
               $r = fmod($x, 1000);
               if($r > 0)
               {
                   $w .= ' ';
                   if($r < 100)
                   {
                       $w .= ' ';
                   }
                   $w .= number_to_words($r);
               }
           } else {
               $w .= number_to_words(floor($x/100000)) .' lakh';
               $r = fmod($x, 100000);
               if($r > 0)
               {
                   $w .= ' ';
                   if($r < 100)
                   {
                       $word .= ' ';
                   }
                   $w .= number_to_words($r);
               }
           }
       }
       return $w;
  }
?>
<?php echo $footer; ?>