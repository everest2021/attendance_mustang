<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/report.png" alt="" /> <?php echo $heading_title; ?></h1>
    </div>
    <div class="content sales-report">
      <table class="form">
        <tr>
          <td style="width:13%;"><?php echo "Name"; ?>
            <input type="text" name="filter_name" value="<?php echo $filter_name;  ?>" id="filter_name" size="25" />
            <input type="hidden" name="filter_name_id" value="<?php echo $filter_name_id; ?>" id="filter_name_id" />
          </td>
          <td style="width:8%;"><?php echo "Date Start"; ?>
            <input type="text" name="filter_date_start" value="<?php echo $filter_date_start; ?>" id="date-start" size="10" />
          </td>
          <td style="width:8%;"><?php echo "Date End"; ?>
            <input type="text" name="filter_date_end" value="<?php echo $filter_date_end; ?>" id="date-end" size="10" />
          </td>
          <td style="width:8%">Unit
            <select name="filter_unit">
              <?php foreach($unit_data as $key => $ud) { ?>
                <?php if($key == $filter_unit) { ?>
                  <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                <?php } else { ?>
                  <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </td>
          <td style="width:8%;"><?php echo "Gender"; ?>
            <select name="filter_gender" id="filter_gender">
              <?php foreach($genders as $gkey => $gvalue){ ?>
                <?php if($gkey == $filter_gender){ ?>
                  <option value="<?php echo $gkey ?>" selected="selected"><?php echo $gvalue ?></option>
                <?php } else { ?>
                  <option value="<?php echo $gkey ?>"><?php echo $gvalue ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </td>
          <td style="text-align: right;">
            <a style="padding: 13px 25px;" onclick="filter();" id="filter" class="button"><?php echo $button_filter; ?></a>
            <a style="padding: 13px 25px;" href="<?php echo $export; ?>" id="filter_export" class="button"><?php echo 'Export'; ?></a>
          </td>
        </tr>
      </table>
      <table  class="list">
        <thead>
          <tr>
            <td class="left"><?php echo 'Sr. No'; ?></td>
            <td class="left"><?php echo 'Emp Code'; ?></td>
            <!-- <td class="left"><?php echo 'Designation'; ?></td> -->
            <td class="left"><?php echo 'Joining Date'; ?></td>
            <!-- <td class="left"><?php echo 'Department'; ?></td> -->
            <!-- <td class="left"><?php echo 'Esic No'; ?></td> -->
            <!-- <td class="left"><?php echo 'PF No'; ?></td> -->
            <td class="left"><?php echo 'Name'; ?></td>
            <td class="right"><?php echo 'Basic'; ?></td>
            <td class="right"><?php echo 'Incentive'; ?></td>
            <td class="right"><?php echo 'Total'; ?></td>
            <td class="right"><?php echo 'Absent'; ?></td>
            <td class="right"><?php echo 'PL'; ?></td>
            <td class="right"><?php echo 'Total'; ?></td>
            <td class="right"><?php echo 'Earned Basic'; ?></td>
            <td class="right"><?php echo 'HRA'; ?></td>
            <td class="right"><?php echo 'Allowance'; ?></td>
            <td class="right"><?php echo 'Gross Earned'; ?></td>
            <td class="right"><?php echo 'PA'; ?></td>
            <td class="right"><?php echo 'Con/Tea/Snacks'; ?></td>
            <td class="right"><?php echo 'OT'; ?></td>
            <td class="right"><?php echo 'OT Amount'; ?></td>
            <td class="right"><?php echo 'Gross'; ?></td>
            <td class="right"><?php echo 'ESIC'; ?></td>
            <td class="right"><?php echo 'P.F'; ?></td>
            <td class="right"><?php echo 'P.T'; ?></td>
            <td class="right"><?php echo 'Net Paid'; ?></td>
          </tr>
        </thead>
        <tbody>
          <?php if ($dailyreports) { ?>
            <?php 
              $i = 1; 
              $total_fixed_basic = 0;
              $total_incentive = 0;
              $total_total = 0;
              $total_days_absent = 0;
              $total_days_leave = 0;
              $total_total_days_present = 0;
              $total_earned_basic = 0;
              $total_hra = 0;
              $total_other_allowance = 0;
              $total_pa = 0;
              $total_tea_snacks = 0;
              $total_gross_earned = 0;
              $total_calculated_overtime = 0;
              $total_ot_amount = 0;
              $total_gross = 0;
              $total_esic = 0;
              $total_pf = 0;
              $total_pt = 0;
              $total_net_paid = 0;
            ?>
            <?php foreach ($dailyreports as $result) { ?>
              <tr>
                <td class="text-left"><?php echo $i; ?></td>
                <td class="text-left"><?php echo $result['emp_code']; ?></td>
                <!-- <td class="text-left"><?php //echo $result['designation']; ?></td> -->
                <td class="text-left"><?php echo $result['doj']; ?></td>
                <!-- <td class="text-left"><?php //echo $result['department']; ?> </td> -->
                <!-- <td class="text-left"><?php //echo $result['esic_no']; ?></td> -->
                <!-- <td class="text-left"><?php //echo $result['pf_no']; ?></td> -->
                <td class="text-left"><?php echo $result['emp_name']; ?></td>
                <td class="text-right"><?php echo $result['fixed_basic']; ?></td>
                <td class="text-right"><?php echo $result['incentive']; ?></td>
                <td class="text-right"><?php echo $result['total']; ?></td>
                <td class="text-right"><?php echo $result['days_absent']; ?></td>
                <td class="text-right"><?php echo $result['days_leave']; ?></td>
                <td class="text-right"><?php echo $result['total_days_present']; ?></td>
                <td class="text-right"><?php echo $result['earned_basic']; ?></td>
                <td class="text-right"><?php echo $result['hra']; ?></td>
                <td class="text-right"><?php echo $result['other_allowance']; ?></td>
                <td class="text-right"><?php echo $result['gross_earned']; ?></td>
                <td class="text-right"><?php echo $result['pa']; ?></td>
                <td class="text-right"><?php echo $result['tea_snacks']; ?></td>
                <td class="text-right"><?php echo $result['calculated_overtime']; ?></td>
                <td class="text-right"><?php echo $result['ot_amount']; ?></td>
                <td class="text-right"><?php echo $result['gross']; ?></td>
                <td class="text-right"><?php echo $result['esic']; ?></td>
                <td class="text-right"><?php echo $result['pf']; ?></td>
                <td class="text-right"><?php echo $result['pt']; ?></td>
                <td class="text-right"><?php echo $result['net_paid']; ?></td>
              </tr>
              <?php 
                $i ++; 
                $total_fixed_basic += $result['fixed_basic'];
                $total_incentive += $result['incentive'];
                $total_total += $result['total'];
                $total_days_absent += $result['days_absent'];
                $total_days_leave += $result['days_leave'];
                $total_total_days_present += $result['total_days_present'];
                $total_earned_basic += $result['earned_basic'];
                $total_hra += $result['hra'];
                $total_other_allowance += $result['other_allowance'];
                $total_pa += $result['pa'];
                $total_tea_snacks += $result['tea_snacks'];
                $total_gross_earned += $result['gross_earned'];
                $total_calculated_overtime += $result['calculated_overtime'];
                $total_ot_amount += $result['ot_amount'];
                $total_gross += $result['gross'];
                $total_esic += $result['esic'];
                $total_pf += $result['pf'];
                $total_pt += $result['pt'];
                $total_net_paid += $result['net_paid'];
              ?>
            <?php } ?>
            <tr>
              <td class="text-left">&nbsp;</td>
              <td class="text-left">&nbsp;</td>
              <td class="text-left">&nbsp;</td>
              <td class="text-left" style="text-align: right;font-weight: bold;">Total</td>
              <td class="text-right" style="text-align: right;font-weight: bold;"><?php echo $total_fixed_basic; ?></td>
              <td class="text-right" style="text-align: right;font-weight: bold;"><?php echo $total_incentive; ?></td>
              <td class="text-right" style="text-align: right;font-weight: bold;"><?php echo $total_total; ?></td>
              <td class="text-right" style="text-align: right;font-weight: bold;"><?php echo $total_days_absent; ?></td>
              <td class="text-right" style="text-align: right;font-weight: bold;"><?php echo $total_days_leave; ?></td>
              <td class="text-right" style="text-align: right;font-weight: bold;"><?php echo $total_total_days_present; ?></td>
              <td class="text-right" style="text-align: right;font-weight: bold;"><?php echo $total_earned_basic; ?></td>
              <td class="text-right" style="text-align: right;font-weight: bold;"><?php echo $total_hra; ?></td>
              <td class="text-right" style="text-align: right;font-weight: bold;"><?php echo $total_other_allowance; ?></td>
              <td class="text-right" style="text-align: right;font-weight: bold;"><?php echo $total_gross_earned; ?></td>
              <td class="text-right" style="text-align: right;font-weight: bold;"><?php echo $total_pa; ?></td>
              <td class="text-right" style="text-align: right;font-weight: bold;"><?php echo $total_tea_snacks; ?></td>
              <td class="text-right" style="text-align: right;font-weight: bold;"><?php echo $total_calculated_overtime; ?></td>
              <td class="text-right" style="text-align: right;font-weight: bold;"><?php echo $total_ot_amount; ?></td>
              <td class="text-right" style="text-align: right;font-weight: bold;"><?php echo $total_gross; ?></td>
              <td class="text-right" style="text-align: right;font-weight: bold;"><?php echo $total_esic; ?></td>
              <td class="text-right" style="text-align: right;font-weight: bold;"><?php echo $total_pf; ?></td>
              <td class="text-right" style="text-align: right;font-weight: bold;"><?php echo $total_pt; ?></td>
              <td class="text-right" style="text-align: right;font-weight: bold;"><?php echo $total_net_paid; ?></td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
function filter() {
  url = 'index.php?route=report/wagesmuster&token=<?php echo $token; ?>';

  var filter_name = $('input[name=\'filter_name\']').attr('value');
  
  if (filter_name) {
    url += '&filter_name=' + encodeURIComponent(filter_name);
    var filter_name_id = $('input[name=\'filter_name_id\']').attr('value');
    if (filter_name_id) {
      url += '&filter_name_id=' + encodeURIComponent(filter_name_id);
    } else {
      alert('Please Enter Correct Employee Name');
      return false;
    }
  }
  
  var filter_date_start = $('input[name=\'filter_date_start\']').attr('value');
  
  if (filter_date_start) {
    url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  }

  var filter_date_end = $('input[name=\'filter_date_end\']').attr('value');
  
  if (filter_date_end) {
    url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
  }

  var filter_gender = $('select[name=\'filter_gender\']').attr('value');
  if (filter_gender) {
    url += '&filter_gender=' + encodeURIComponent(filter_gender);
  }

  var filter_unit = $('select[name=\'filter_unit\']').attr('value');
  if (filter_unit) {
    url += '&filter_unit=' + encodeURIComponent(filter_unit);
  }

  url += '&once=1';
  
  location = url;
  return false;
}

//--></script> 
<script type="text/javascript"><!--
$(document).ready(function() {
  
  //$('#date-start').datepicker({dateFormat: 'yy-mm-dd'});
  //$('#date-end').datepicker({dateFormat: 'yy-mm-dd'});

  $(function() {
      //$.datepicker.setDefaults($.datepicker.regional['en']);
      $('#date-start').datepicker({
            dateFormat: 'yy-mm-dd',
            onSelect: function(selectedDate) {
              var date = $(this).datepicker('getDate');
              $('#date-end').datepicker('option', 'maxDate', date); // Reset minimum date
              date.setDate(date.getDate() + 30); // Add 30 days
              $('#date-end').datepicker('setDate', date); // Set as default
            }
      });
      
      $('#date-end').datepicker({
            dateFormat: 'yy-mm-dd',
            onSelect: function(selectedDate) {
              $('#date-start').datepicker('option', 'maxDate', $(this).datepicker('getDate')); // Reset maximum date
            }
      });
      
  });
  
});

$('input[name=\'filter_name\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=report/dailyattendance/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.name,
            value: item.emp_code
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_name\']').val(ui.item.label);
    $('input[name=\'filter_name_id\']').val(ui.item.value);
    return false;
  },
  focus: function(event, ui) {
    return false;
  }
});

//$(document).ready(function() {
 // $('#date-start').datepicker({dateFormat: 'yy-mm-dd'});
  
//});
</script>
<?php echo $footer; ?>