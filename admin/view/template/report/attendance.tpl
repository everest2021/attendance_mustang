<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
	<?php foreach ($breadcrumbs as $breadcrumb) { ?>
	<?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
	<?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
	<div class="heading">
	  <h1><img src="view/image/report.png" alt="" /> <?php echo $heading_title; ?></h1>
	</div>
	<div class="content sales-report">
	  <table class="form">
		<tr>
			<td style="width:13%;"><?php echo "Name"; ?>
            	<input type="text" name="filter_name" value="<?php echo $filter_name;  ?>" id="filter_name" size="25" />
            	<input type="hidden" name="filter_name_id" value="<?php echo $filter_name_id; ?>" id="filter_name_id" />
          	</td>

		  <td style="width:8%;"><?php echo "Date"; ?>
			<input type="text" name="filter_date_start" value="<?php echo $filter_date_start; ?>" id="date-start" size="12" /></td>
		  <td style="width:8%">Location
			<select name="filter_unit">
			  <?php foreach($unit_data as $key => $ud) { ?>
				<?php if($key == $filter_unit) { ?>
				  <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
				<?php } else { ?>
				  <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
				<?php } ?>
			  <?php } ?>
			</select>
		  </td>

		  <td style="width:100px">Department
			<select name="filter_department" style="width:100px">
			  <?php foreach($department_data as $key => $dd) { ?>
				<?php if($key == $filter_department) { ?>
				  <option value='<?php echo $key; ?>' selected="selected"><?php echo $dd; ?></option> 
				<?php } else { ?>
				  <option value='<?php echo $key; ?>'><?php echo $dd; ?></option>
				<?php } ?>
			  <?php } ?>
			</select>
			
		  </td>

		  <td style="width:8%">Designation
			<select name="filter_designation" style="width:100px">
			  <?php foreach($designation_data as $gkey => $dg) { ?>
				<?php if($gkey == $filter_designation) { ?>
				  <option value='<?php echo $gkey; ?>' selected="selected"><?php echo $dg; ?></option> 
				<?php } else { ?>
				  <option value='<?php echo $gkey; ?>'><?php echo $dg; ?></option>
				<?php } ?>
			  <?php } ?>
			</select>
		  </td>

		  <td style="width:2%;display: none;">Group
			<select name="group">
			  <?php foreach($group_data as $key => $gd) { ?>
				<?php if($key == $group) { ?>
				  <option value='<?php echo $key; ?>' selected="selected"><?php echo $gd; ?></option> 
				<?php } else { ?>
				  <option value='<?php echo $key; ?>'><?php echo $gd; ?></option>
				<?php } ?>
			  <?php } ?>
			</select>
		  </td>

		  <td style="width:8%">Status
			<select name="status">
			  <?php foreach($statuses as $skey => $svalue) { ?>
				<?php if($skey == $status) { ?>
				  <option value='<?php echo $skey; ?>' selected="selected"><?php echo $svalue; ?></option> 
				<?php } else { ?>
				  <option value='<?php echo $skey; ?>'><?php echo $svalue; ?></option>
				<?php } ?>
			  <?php } ?>
			</select>
		  </td>
		  
		  <td style="text-align: right;">
			<a style="padding: 13px 25px;" onclick="filter();" id="filter" class="button"><?php echo $button_filter; ?></a>
			<a style="padding: 13px 25px;display: none;" href="<?php echo $generate; ?>" id="filter_refresh" class="button"><?php echo 'Refresh'; ?></a>
			<a style="padding: 13px 25px;" href="<?php echo $export; ?>" id="filter_export" class="button"><?php echo 'Export'; ?></a>
		  </td>
		</tr>
	  </table>
	  <table class="list">
		<thead>
		  <tr>
			<td class="center"><?php echo 'Sr.No'; ?></td>
			<td class="center"><?php echo 'Emp Code'; ?></td>
			<td class="center"><?php echo 'Name'; ?></td>
			<td class="center" colspan='2'><?php echo 'Shift Assigned'; ?></td>
			<td class="center" colspan='4'><?php echo 'Shift Attended'; ?></td>
			<?php if($status == 0 || $status == 1) { ?>
			  <td class="center"><?php echo 'Working Hours'; ?></td>
			  <td class="center"><?php echo 'Late time'; ?></td>
			  <td class="center"><?php echo 'Early time'; ?></td>
			<?php } ?>
			<?php if($this->user->getId() == 111111111){ ?>
			  <td class="center"><?php echo 'Action'; ?></td>
			<?php } ?>
		  </tr>
		  <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td class="center">In</td>
			<td class="center">Out</td>
			<td class="center" style="width:8%;">In Date</td>
			<td class="center">In Time</td>
			<td class="center" style="width:8%;">Out Date</td>
			<td class="center">Out TIme</td>
			<?php if($status == 0 || $status == 1) { ?>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			<?php } ?>
			<?php if($this->user->getId() == 111111111){ ?>
			  <td>&nbsp;</td>
			<?php } ?>
		  </tr>
		</thead>
		<tbody>
		  <?php if($results) { ?>
			<?php $i = 1; ?>
			<?php foreach($results as $result) { ?>
			  <tr>
				<td>
				  <?php echo $i; ?>
				</td>
				<td>
				  <?php echo $result['emp_code']; ?>
				</td>
				<td>
				  <?php echo $result['emp_name']; ?>
				</td>
				<td>
				  <?php if($result['leave_status'] == 1) { ?>
					<?php echo $result['firsthalf_status']; ?>
				  <?php } elseif($result['weekly_off'] != '0') { ?>
					<?php echo 'Weekly Off'; ?>
				  <?php } elseif($result['holiday_id'] != '0') { ?>
					<?php echo 'Holiday'; ?>
				  <?php } elseif($result['halfday_status'] != '0') { ?>
					<?php echo 'Half Day'; ?>
				  <?php } elseif($result['compli_status'] != '0') { ?>
					<?php echo 'Comp Off'; ?>
				  <?php } else { ?>
					<?php echo $result['shift_intime']; ?>
				  <?php } ?>
				</td>
				<td>
				  <?php if($result['leave_status'] == 1) { ?>
					<?php echo $result['secondhalf_status']; ?>
				  <?php } elseif($result['weekly_off'] != '0') { ?>
					<?php echo 'Weekly Off'; ?>
				  <?php } elseif($result['holiday_id'] != '0') { ?>
					<?php echo 'Holiday'; ?>
				  <?php } elseif($result['halfday_status'] != '0') { ?>
					<?php echo 'Half Day'; ?>
				  <?php } elseif($result['compli_status'] != '0') { ?>
					<?php echo 'Comp Off'; ?>
				  <?php } else { ?>
					<?php echo $result['shift_outtime']; ?>
				  <?php } ?>
				</td>
				
				<?php if($status == 0 || $status == 1) { ?>
				  <td>
					<?php echo $result['date']; ?>
				  </td>
				  <td>
					<?php echo $result['act_intime']; ?>
				  </td>
				  <td>
					<?php echo $result['date_out']; ?>
				  </td>
				  <td>
					<?php echo $result['act_outtime']; ?>
				  </td>
				<?php } else { ?>
				  <td>
					<?php echo $result['date']; ?>
				  </td>
				  <td>
					<?php echo $result['act_intime']; ?>
				  </td>
				  <td>
					<?php echo $result['date_out']; ?>
				  </td>
				  <td>
					<?php echo $result['act_outtime']; ?>
				  </td>
				<?php } ?>

				<?php if($status == 0 || $status == 1) { ?>
				  <td>
					<?php if($result['working_time'] == '08:00:00') { ?>
					  <span style="background-color:#13F113;">
						<?php echo $result['working_time']; ?>
					  </span>
					<?php } else { ?>
						<?php echo $result['working_time']; ?>
					<?php } ?> 
				  </td>
				  <td>
					<?php if($result['late_time'] != '00:00:00') { ?>
					  <span style="background-color:red;">
						<?php echo $result['late_time']; ?>
					  </span>
					<?php } else { ?>
					  <?php echo $result['late_time']; ?>
					<?php } ?>
				  </td>
				  <td>
					<?php if($result['early_time'] != '00:00:00') { ?>
					  <span style="background-color:red;">
						<?php echo $result['early_time']; ?>
					  </span>
					<?php } else { ?>
					  <?php echo $result['early_time']; ?>
					<?php } ?>
				  </td>
				<?php } ?>
				<?php if($this->user->getId() == 111111111){ ?>
				  <td>
					<a href="<?php echo $result['remove_href']; ?>">Delete</a>
				  </td>
				<?php } ?>
			  </tr>
			  <?php $i++; ?>
			<?php } ?>
		  <?php } else { ?>
		  <tr>
			<td class="center" colspan="13"><?php echo $text_no_results; ?></td>
		  </tr>
		  <?php } ?>
		</tbody>
	  </table>
	  
	</div>
  </div>
</div>
<script type="text/javascript"><!--
function filter() {
  url = 'index.php?route=report/attendance&token=<?php echo $token; ?>';
   var filter_name = $('input[name=\'filter_name\']').attr('value');
  
  if (filter_name) {
    url += '&filter_name=' + encodeURIComponent(filter_name);
    var filter_name_id = $('input[name=\'filter_name_id\']').attr('value');
    if (filter_name_id) {
      url += '&filter_name_id=' + encodeURIComponent(filter_name_id);
    } else {
      alert('Please Enter Correct Employee Name');
      return false;
    }
  }

  
  var filter_date_start = $('input[name=\'filter_date_start\']').attr('value');
  
  if (filter_date_start) {
	url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  }

  var filter_unit = $('select[name=\'filter_unit\']').attr('value');
  
  if (filter_unit) {
	url += '&filter_unit=' + encodeURIComponent(filter_unit);
  }

  var filter_department = $('select[name=\'filter_department\']').attr('value');
  
  if (filter_department) {
	url += '&filter_department=' + encodeURIComponent(filter_department);
  }

   var filter_designation = $('select[name=\'filter_designation\']').attr('value');
  
  if (filter_designation) {
	url += '&filter_designation=' + encodeURIComponent(filter_designation);
  }


 //  var group = $('select[name=\'group\']').attr('value');
  
 //  if (group) {
	// url += '&group=' + encodeURIComponent(group);
 //  }

  var status = $('select[name=\'status\']').attr('value');
  
  if (status) {
	url += '&status=' + encodeURIComponent(status);
  }  
  
  location = url;
  return false;
}

//--></script> 
<script type="text/javascript"><!--
$(document).ready(function() {
  $('#date-start').datepicker({dateFormat: 'yy-mm-dd'});
  
});
//--></script>
<script type="text/javascript"><!--
$(document).ready(function() {
  
  //$('#date-start').datepicker({dateFormat: 'yy-mm-dd'});
  //$('#date-end').datepicker({dateFormat: 'yy-mm-dd'});

  $(function() {
      //$.datepicker.setDefaults($.datepicker.regional['en']);
      $('#date-start').datepicker({
            dateFormat: 'yy-mm-dd',
            onSelect: function(selectedDate) {
              var date = $(this).datepicker('getDate');
              $('#date-end').datepicker('option', 'maxDate', date); // Reset minimum date
              date.setDate(date.getDate() + 30); // Add 30 days
              $('#date-end').datepicker('setDate', date); // Set as default
            }
      });
      
      $('#date-end').datepicker({
            dateFormat: 'yy-mm-dd',
            onSelect: function(selectedDate) {
              $('#date-start').datepicker('option', 'maxDate', $(this).datepicker('getDate')); // Reset maximum date
            }
      });
      
  });
  
});

$('input[name=\'filter_name\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/employee/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.name,
            value: item.emp_code
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_name\']').val(ui.item.label);
    $('input[name=\'filter_name_id\']').val(ui.item.value);
    return false;
  },
  focus: function(event, ui) {
    return false;
  }
});

//$(document).ready(function() {
 // $('#date-start').datepicker({dateFormat: 'yy-mm-dd'});
  
//});
$('#region').on('change', function() {
  region = $(this).val();
  $.ajax({
  url: 'index.php?route=catalog/employee/getdivision_location&token=<?php echo $token; ?>&filter_region_id=' +  encodeURIComponent(region),
  dataType: 'json',
  success: function(json) {   
	$('#division').find('option').remove();
	if(json['division_datas']){
	$.each(json['division_datas'], function (i, item) {
	  $('#division').append($('<option>', { 
		value: item.division_id,
		text : item.division 
	  }));
	});
	}
	$('#unit').find('option').remove();
	if(json['unit_datas']){
	$.each(json['unit_datas'], function (i, item) {
	  $('#unit').append($('<option>', { 
		value: item.unit_id,
		text : item.unit 
	  }));
	});
	}
  }
  });
});

$('#division').on('change', function() {
  division = $(this).val();
  $.ajax({
  url: 'index.php?route=catalog/employee/getlocation&token=<?php echo $token; ?>&filter_division_id=' +  encodeURIComponent(division),
  dataType: 'json',
  success: function(json) {   
	$('#unit').find('option').remove();
	if(json['unit_datas']){
	$.each(json['unit_datas'], function (i, item) {
	  $('#unit').append($('<option>', { 
		value: item.unit_id,
		text : item.unit 
	  }));
	});
	}
  }
  });
});


//$(document).ready(function() {
 // $('#date-start').datepicker({dateFormat: 'yy-mm-dd'});
  
//});
</script> 
<?php echo $footer; ?>