<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
</head>
<body style="width:100%;">
<?php //foreach($final_datass as $fkeyss => $final_datas) { ?>
<div style="page-break-after: always;">
  <h1 style="text-align:center;">
    <?php echo 'India Factoring & Finance Solutions Pvt.Ltd.'; ?><br /><?php echo 'LEAVE REGISTER'; ?><br />
    <p style="display:inline;font-size:15px;"><?php echo 'Period : '. $filter_date_start . ' - ' . $filter_date_end; ?></p>
  </h1>
  <table class="product" style="width:100% !important;">
    <tbody>
      <?php if($final_datas) { ?>
        <?php $i = 1; ?>
        <?php foreach($final_datas as $final_data) { ?>
          <tr>
            <td class="left" style="width:9%;font-weight:bold;font-size:12px;">From Date</td>
            <td class="left" style="width:9%;font-weight:bold;font-size:12px;">To Date</td>
            <td class="left" style="width:9%;font-weight:bold;font-size:12px;">Break</td>
            <td class="left" colspan="7" style="text-align:center;font-weight:bold;font-size:12px;">Leave Detail</td>
          </tr>
          <tr style="font-weight:bold;font-size:11px;">
            <td colspan = "10">
              <?php echo 'Employee Code & Name : ' . $final_data['basic_data']['emp_code']; ?> &nbsp;&nbsp;&nbsp; <?php echo $final_data['basic_data']['name']; ?> 
            </td>
          </tr>
          <tr>
            <td colspan="3">
            </td>
            <?php if($filter_leave == 'BL' || $filter_leave == '0'){ ?>
              <td style="padding: 0px 9px;font-weight:bold;font-size:12px;width: 7%;text-align: right;">
                BL
              </td>
            <?php } ?>
            <?php if($filter_leave == 'PL' || $filter_leave == '0'){ ?>
              <td style="padding: 0px 9px;font-weight:bold;font-size:12px;width: 7%;text-align: right;">
                PL
              </td>
            <?php } ?>
            <?php if($filter_leave == 'SL' || $filter_leave == '0'){ ?>
              <td style="padding: 0px 9px;font-weight:bold;font-size:12px;width: 7%;text-align: right;">
                SL
              </td>
            <?php } ?>
            <?php if($filter_leave == 'ML' || $filter_leave == '0'){ ?>
            <td style="padding: 0px 9px;font-weight:bold;font-size:12px;width: 7%;text-align: right;">
              ML
            </td>
            <?php } ?>
            <?php if($filter_leave == 'MAL' || $filter_leave == '0'){ ?>
            <td style="padding: 0px 9px;font-weight:bold;font-size:12px;width: 7%;text-align: right;">
              MAL
            </td>
            <?php } ?>
            <?php if($filter_leave == 'PAL' || $filter_leave == '0'){ ?>
            <td style="padding: 0px 9px;font-weight:bold;font-size:12px;width: 7%;text-align: right;">
              PAL
            </td>
            <?php } ?>
            <?php if($filter_leave == 'LWP' || $filter_leave == '0'){ ?>
            <td style="padding: 0px 9px;font-weight:bold;font-size:12px;width: 7%;text-align: right;">
              LWP
            </td>
            <?php } ?>
          </tr>
          <tr>
            <td class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
              <?php echo 'opening Bal'; ?>
            </td>
            <td style="padding: 0px 9px;font-weight:bold;font-size:12px;">
              <?php echo $final_data['basic_data']['open_date']; ?>
            </td>
            <td>
            </td>
            <?php if($filter_leave == 'BL' || $filter_leave == '0'){ ?>
              <td class="right" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
                <?php echo $final_data['basic_data']['bl_open']; ?>
              </td>
            <?php } ?>
            <?php if($filter_leave == 'PL' || $filter_leave == '0'){ ?>
              <td class="right" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
                <?php echo $final_data['basic_data']['pl_open']; ?>
              </td>
            <?php } ?>
            <?php if($filter_leave == 'SL' || $filter_leave == '0'){ ?>
              <td class="right" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
                <?php echo $final_data['basic_data']['sl_open']; ?>
              </td>
            <?php } ?>
            <?php if($filter_leave == 'ML' || $filter_leave == '0'){ ?>
              <td class="right" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
                <?php echo $final_data['basic_data']['ml_open']; ?>
              </td>
            <?php } ?>
            <?php if($filter_leave == 'MAL' || $filter_leave == '0'){ ?>
              <td class="right" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
                <?php echo $final_data['basic_data']['mal_open']; ?>
              </td>
            <?php } ?>
            <?php if($filter_leave == 'PAL' || $filter_leave == '0'){ ?>
              <td class="right" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
                <?php echo $final_data['basic_data']['pal_open']; ?>
              </td>
            <?php } ?>
            <?php if($filter_leave == 'LWP' || $filter_leave == '0'){ ?>
              <td class="right" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
                <?php echo $final_data['basic_data']['lwp_open']; ?>
              </td>
            <?php } ?>
          </tr>
          <?php if(isset($final_data['tran_data']['action'])) { ?>
            <?php foreach($final_data['tran_data']['action'] as $tkey => $tvalue) { ?>
              <?php if($tvalue['performance_stat'] == 1){ ?>
                <tr>
                  <td style="padding: 0px 9px;font-size:11px;">
                    <?php echo $tvalue['start_date']; ?>
                  </td>
                  <td style="padding: 0px 9px;font-size:11px;">
                    <?php echo $tvalue['end_date']; ?>
                  </td>
                  <td style="padding: 0px 9px;font-size:11px;">
                    <?php echo $tvalue['leave_types']; ?>
                  </td>
                  
                  <?php if($filter_leave == 'BL' || $filter_leave == '0'){ ?>
                    <td class="right" style="padding: 0px 9px;font-size:11px;">
                      <?php echo $tvalue['days_bl']; ?>
                    </td>
                  <?php } ?>
                  <?php if($filter_leave == 'PL' || $filter_leave == '0'){ ?>
                    <td class="right" style="padding: 0px 9px;font-size:11px;">
                      <?php echo $tvalue['days_pl']; ?>
                    </td>
                  <?php } ?>
                  <?php if($filter_leave == 'SL' || $filter_leave == '0'){ ?>
                    <td class="right" style="padding: 0px 9px;font-size:11px;">
                      <?php echo $tvalue['days_sl']; ?>
                    </td>
                  <?php } ?>
                  <?php if($filter_leave == 'ML' || $filter_leave == '0'){ ?>
                    <td class="right" style="padding: 0px 9px;font-size:11px;">
                      <?php echo $tvalue['days_ml']; ?>
                    </td>
                  <?php } ?>
                  <?php if($filter_leave == 'MAL' || $filter_leave == '0'){ ?>
                    <td class="right" style="padding: 0px 9px;font-size:11px;">
                      <?php echo $tvalue['days_mal']; ?>
                    </td>
                  <?php } ?>
                  <?php if($filter_leave == 'PAL' || $filter_leave == '0'){ ?>
                    <td class="right" style="padding: 0px 9px;font-size:11px;">
                      <?php echo $tvalue['days_pal']; ?>
                    </td>
                  <?php } ?>
                  <?php if($filter_leave == 'LWP' || $filter_leave == '0'){ ?>
                    <td class="right" style="padding: 0px 9px;font-size:11px;">
                      <?php echo $tvalue['days_lwp']; ?>
                    </td>
                  <?php } ?>
                </tr>
              <?php } ?>
              <?php if($tvalue['encash_status'] == 1){ ?>
                <tr>
                  <td style="padding: 0px 9px;font-size:11px;">
                    <?php echo $tvalue['start_date']; ?>
                  </td>
                  <td style="padding: 0px 9px;font-size:11px;">
                    <?php echo $tvalue['end_date']; ?>
                  </td>
                  <td style="padding: 0px 9px;font-size:11px;">
                    <?php echo 'E'; ?>
                  </td>
                  <td class="right" style="padding: 0px 9px;font-size:11px;">
                    <?php echo 0; ?>
                  </td>
                  <td class="right" style="padding: 0px 9px;font-size:11px;">
                    <?php echo $tvalue['encash']; ?>
                  </td>
                  <td class="right" style="padding: 0px 9px;font-size:11px;">
                    <?php echo 0; ?>
                  </td>
                </tr>
              <?php } ?>
            <?php } ?>
          <?php } ?>
          <tr>
            <td colspan="3" class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
              <?php echo 'Consumed Leave'; ?>
            </td>
            <?php if($filter_leave == 'BL' || $filter_leave == '0'){ ?>
              <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                <?php echo $final_data['basic_data']['total_days_bl']; ?>
              </td>
            <?php } ?>
            <?php if($filter_leave == 'PL' || $filter_leave == '0'){ ?>
              <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                <?php echo $final_data['basic_data']['total_days_pl']; ?>
              </td>
            <?php } ?>
            <?php if($filter_leave == 'SL' || $filter_leave == '0'){ ?>
              <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                <?php echo $final_data['basic_data']['total_days_sl']; ?>
              </td>
            <?php } ?>
            <?php if($filter_leave == 'ML' || $filter_leave == '0'){ ?>
              <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                <?php echo $final_data['basic_data']['total_days_ml']; ?>
              </td>
            <?php } ?>
            <?php if($filter_leave == 'MAL' || $filter_leave == '0'){ ?>
              <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                <?php echo $final_data['basic_data']['total_days_mal']; ?>
              </td>
            <?php } ?>
            <?php if($filter_leave == 'PAL' || $filter_leave == '0'){ ?>
              <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                <?php echo $final_data['basic_data']['total_days_pal']; ?>
              </td>
            <?php } ?>
            <?php if($filter_leave == 'LWP' || $filter_leave == '0'){ ?>
              <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                <?php echo $final_data['basic_data']['total_days_lwp']; ?>
              </td>
            <?php } ?>
          </tr>
          <tr>
            <td colspan="3" class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
              <?php echo 'Closing Balance'; ?>
            </td>
            <?php if($filter_leave == 'BL' || $filter_leave == '0'){ ?>
              <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                <?php echo $final_data['basic_data']['closing_bl']; ?>
              </td>
            <?php } ?>
            <?php if($filter_leave == 'PL' || $filter_leave == '0'){ ?>
              <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                <?php echo $final_data['basic_data']['closing_pl']; ?>
              </td>
            <?php } ?>
            <?php if($filter_leave == 'SL' || $filter_leave == '0'){ ?>
              <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                <?php echo $final_data['basic_data']['closing_sl']; ?>
              </td>
            <?php } ?>
            <?php if($filter_leave == 'ML' || $filter_leave == '0'){ ?>
              <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                <?php echo $final_data['basic_data']['closing_ml']; ?>
              </td>
            <?php } ?>
            <?php if($filter_leave == 'MAL' || $filter_leave == '0'){ ?>
              <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                <?php echo $final_data['basic_data']['closing_mal']; ?>
              </td>
            <?php } ?>
            <?php if($filter_leave == 'PAL' || $filter_leave == '0'){ ?>
              <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                <?php echo $final_data['basic_data']['closing_pal']; ?>
              </td>
            <?php } ?>
            <?php if($filter_leave == 'LWP' || $filter_leave == '0'){ ?>
              <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                <?php echo $final_data['basic_data']['closing_lwp']; ?>
              </td>
            <?php } ?>
          </tr>
          <?php if($filter_year == date('Y')) { ?>
            <tr>
              <td colspan="3" class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
                <?php echo 'UnProcessed Leave'; ?>
              </td>
              <?php if($filter_leave == 'BL' || $filter_leave == '0'){ ?>
                <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                  <?php echo $final_data['basic_data']['un_total_days_bl']; ?>
                </td>
              <?php } ?>
              <?php if($filter_leave == 'PL' || $filter_leave == '0'){ ?>
                <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                  <?php echo $final_data['basic_data']['un_total_days_pl']; ?>
                </td>
              <?php } ?>
              <?php if($filter_leave == 'SL' || $filter_leave == '0'){ ?>
                <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                  <?php echo $final_data['basic_data']['un_total_days_sl']; ?>
                </td>
              <?php } ?>
              <?php if($filter_leave == 'ML' || $filter_leave == '0'){ ?>
                <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                  <?php echo $final_data['basic_data']['un_total_days_ml']; ?>
                </td>
              <?php } ?>
              <?php if($filter_leave == 'MAL' || $filter_leave == '0'){ ?>
                <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                  <?php echo $final_data['basic_data']['un_total_days_mal']; ?>
                </td>
              <?php } ?>
              <?php if($filter_leave == 'PAL' || $filter_leave == '0'){ ?>
                <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                  <?php echo $final_data['basic_data']['un_total_days_pal']; ?>
                </td>
              <?php } ?>
              <?php if($filter_leave == 'LWP' || $filter_leave == '0'){ ?>
                <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                  <?php echo $final_data['basic_data']['un_total_days_lwp']; ?>
                </td>
              <?php } ?>
            </tr>
            <tr>
              <td colspan="3" class="left" style="padding: 0px 9px; font-weight:bold;font-size:12px;">
                <?php echo 'UnProcessed Balance'; ?>
              </td>
              <?php if($filter_leave == 'BL' || $filter_leave == '0'){ ?>
                <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                  <?php echo $final_data['basic_data']['un_total_bal_bl']; ?>
                </td>
              <?php } ?>
              <?php if($filter_leave == 'PL' || $filter_leave == '0'){ ?>
                <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                  <?php echo $final_data['basic_data']['un_total_bal_pl']; ?>
                </td>
              <?php } ?>
              <?php if($filter_leave == 'SL' || $filter_leave == '0'){ ?>
                <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                  <?php echo $final_data['basic_data']['un_total_bal_sl']; ?>
                </td>
              <?php } ?>
              <?php if($filter_leave == 'ML' || $filter_leave == '0'){ ?>
                <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                  <?php echo $final_data['basic_data']['un_total_bal_ml']; ?>
                </td>
              <?php } ?>
              <?php if($filter_leave == 'MAL' || $filter_leave == '0'){ ?>
                <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                  <?php echo $final_data['basic_data']['un_total_bal_mal']; ?>
                </td>
              <?php } ?>
              <?php if($filter_leave == 'PAL' || $filter_leave == '0'){ ?>
                <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                  <?php echo $final_data['basic_data']['un_total_bal_pal']; ?>
                </td>
              <?php } ?>
              <?php if($filter_leave == 'LWP' || $filter_leave == '0'){ ?>
                <td class="right" style="padding: 0px 9px;font-weight:bold;font-size:12px;">
                  <?php echo $final_data['basic_data']['un_total_bal_lwp']; ?>
                </td>
              <?php } ?>
            </tr>
          <?php } ?>
          <tr style="border-bottom:2px solid black;">
            <td colspan = "10" >
            </td>
          </tr>
          <?php $i++; ?>
        <?php } ?>
      <?php } ?>
    </tbody>
  </table>
</div>
<?php //} ?>
</body>
</html>