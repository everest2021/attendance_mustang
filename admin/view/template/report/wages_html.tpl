<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
</head>
<body>
<div class="box" id="content">
  <div  style="page-break-after: always;">
    <h1 style="text-align:center;">
      <?php echo 'Register Wages'; ?><br />
    </h1>
    <table class="product" style="border:0px solid #ffffff !important;">
      <tr>
        <td colspan="10" style="text-align: left;width: 50%;border:0px solid #ffffff !important;">
          <p style="font-size:12px;"><b>Name and address of Contractor: </b><?php echo 'PUSHPENDRA PANDEY'; ?></p>
        </td>
        <td colspan="9" style="text-align: left;width: 50%;border:0px solid #ffffff !important;">
          <p style="font-size:12px;"><b>Name and Address of Establishment/Under which Contract is carried on SPECTRUM SERVICES</b></p>
       </td>
      </tr>
      <tr>
        <td colspan="10" style="text-align: left;width: 50%;border:0px solid #ffffff !important;">
          <p style="font-size:12px;"><b>Nature of Location of Work - </b>Vasai</p>
        </td>
        <td colspan="9" style="text-align: left;width: 50%;border:0px solid #ffffff !important;">
          <p style="font-size:12px;"><b>Name and Address of Principal Employeer-Spectrum Scan</b></p>
       </td>
      </tr>
      <tr>
        <td colspan="10" style="text-align: left;width: 50%;border:0px solid #ffffff !important;">
          <p style="font-size:12px;"><b>For the Month of  - </b><?php echo $month ?></p>
        </td>
        <td colspan="9" style="text-align: left;width: 50%;border:0px solid #ffffff !important;">
          <p style="font-size:12px;"><b>Wage Period - </b>Monthly</p>
       </td>
      </tr>
    </table>
    <table class="product">
      <thead>
        <tr>
          <td style="text-align: left;"><?php echo 'Sr. No'; ?></td>
          <td style="text-align: left;"><?php echo 'Emp Code'; ?></td>
          <td style="text-align: left;"><?php echo 'Designation'; ?></td>
          <td style="text-align: left;"><?php echo 'DOJ'; ?></td>
          <td style="text-align: left;"><?php echo 'Department'; ?></td>
          <td style="text-align: left;"><?php echo 'Esic No'; ?></td>
          <td style="text-align: left;"><?php echo 'PF No'; ?></td>
          <td style="text-align: left;"><?php echo 'Name'; ?></td>
          <td style="text-align: right;"><?php echo 'Fixed Basic'; ?></td>
          <td style="text-align: right;"><?php echo 'Per Day Salary'; ?></td>
          <td style="text-align: right;"><?php echo 'Days Present'; ?></td>
          <td style="text-align: right;"><?php echo 'Earned Basic'; ?></td>
          <td style="text-align: right;"><?php echo 'Salary Earned'; ?></td>
          <td style="text-align: right;"><?php echo 'H.R.A'; ?></td>
          <td style="text-align: right;"><?php echo 'Other Allowance'; ?></td>
          <td style="text-align: right;"><?php echo 'Gross'; ?></td>
          <td style="text-align: right;"><?php echo 'P.F'; ?></td>
          <td style="text-align: right;"><?php echo 'ESIC'; ?></td>
          <td style="text-align: right;"><?php echo 'P.T'; ?></td>
          <td style="text-align: right;"><?php echo 'Net Paid'; ?></td>
        </tr>
      </thead>
      <tbody>
        <?php if ($dailyreports) { ?>
          <?php 
            $i = 1; 
            $total_fixed_basic = 0;
            $total_perdaysal = 0;
            $total_days_present = 0;
            $total_earned_basic = 0;
            $total_salary_earned = 0;
            $total_hra = 0;
            $total_other_allowance = 0;
            $total_gross = 0;
            $total_pf = 0;
            $total_esic = 0;
            $total_pt = 0;
            $total_net_paid = 0;
          ?>
          <?php foreach ($dailyreports as $result) { ?>
            <tr>
              <td style="text-align: left;"><?php echo $i; ?></td>
              <td style="text-align: left;"><?php echo $result['emp_code']; ?></td>
              <td style="text-align: left;"><?php echo $result['designation']; ?></td>
              <td style="text-align: left;"><?php echo $result['doj']; ?></td>
              <td style="text-align: left;"><?php echo $result['department']; ?> </td>
              <td style="text-align: left;"><?php echo $result['esic_no']; ?></td>
              <td style="text-align: left;"><?php echo $result['pf_no']; ?></td>
              <td style="text-align: left;"><?php echo $result['emp_name']; ?></td>
              <td style="text-align: right;"><?php echo $result['fixed_basic']; ?></td>
              <td style="text-align: right;"><?php echo $result['perdaysal']; ?></td>
              <td style="text-align: right;"><?php echo $result['days_present']; ?></td>
              <td style="text-align: right;"><?php echo $result['earned_basic']; ?></td>
              <td style="text-align: right;"><?php echo $result['salary_earned']; ?></td>
              <td style="text-align: right;"><?php echo $result['hra']; ?></td>
              <td style="text-align: right;"><?php echo $result['other_allowance']; ?></td>
              <td style="text-align: right;"><?php echo $result['gross']; ?></td>
              <td style="text-align: right;"><?php echo $result['pf']; ?></td>
              <td style="text-align: right;"><?php echo $result['esic']; ?></td>
              <td style="text-align: right;"><?php echo $result['pt']; ?></td>
              <td style="text-align: right;"><?php echo $result['net_paid']; ?></td>
            </tr>
            <?php 
              $i ++; 
              $total_fixed_basic += $result['fixed_basic'];
              $total_perdaysal += $result['perdaysal'];
              $total_days_present += $result['days_present'];
              $total_earned_basic += $result['earned_basic'];
              $total_salary_earned += $result['salary_earned'];
              $total_hra += $result['hra'];
              $total_other_allowance += $result['other_allowance'];
              $total_gross += $result['gross'];
              $total_pf += $result['pf'];
              $total_esic += $result['esic'];
              $total_pt += $result['pt'];
              $total_net_paid += $result['net_paid'];
            ?>
          <?php } ?>
          <tr>
            <td class="text-left">&nbsp;</td>
            <td class="text-left">&nbsp;</td>
            <td class="text-left">&nbsp;</td>
            <td class="text-left">&nbsp;</td>
            <td class="text-left">&nbsp;</td>
            <td class="text-left">&nbsp;</td>
            <td class="text-left">&nbsp;</td>
            <td class="text-left" style="text-align: right;font-weight: bold;">Total</td>
            <td class="text-right" style="text-align: right;font-weight: bold;"><?php echo $total_fixed_basic; ?></td>
            <td class="text-right" style="text-align: right;font-weight: bold;"><?php echo $total_perdaysal; ?></td>
            <td class="text-right" style="text-align: right;font-weight: bold;"><?php echo $total_days_present; ?></td>
            <td class="text-right" style="text-align: right;font-weight: bold;"><?php echo $total_earned_basic; ?></td>
            <td class="text-right" style="text-align: right;font-weight: bold;"><?php echo $total_salary_earned; ?></td>
            <td class="text-right" style="text-align: right;font-weight: bold;"><?php echo $total_hra; ?></td>
            <td class="text-right" style="text-align: right;font-weight: bold;"><?php echo $total_other_allowance; ?></td>
            <td class="text-right" style="text-align: right;font-weight: bold;"><?php echo $total_gross; ?></td>
            <td class="text-right" style="text-align: right;font-weight: bold;"><?php echo $total_pf; ?></td>
            <td class="text-right" style="text-align: right;font-weight: bold;"><?php echo $total_esic; ?></td>
            <td class="text-right" style="text-align: right;font-weight: bold;"><?php echo $total_pt; ?></td>
            <td class="text-right" style="text-align: right;font-weight: bold;"><?php echo $total_net_paid; ?></td>
          </tr>
        <?php } ?>
      </tbody>
    </table>
    <table class="product" style="border:0px solid #ffffff !important;">
      <tr>
        <td colspan="19" style="text-align: left;width: 100%;border:0px solid #ffffff !important;">
          <p style="font-size:12px;"><b>Certified that Payment of Wages has been made in the presense of an authorised representative of the Principal Employeer</b></p>
        </td>
      </tr>
      <tr>
        <td colspan="19" style="text-align: left;width: 100%;border:0px solid #ffffff !important;">
          <p style="font-size:12px;"><b>Date : <?php echo date('d-m-Y'); ?> </b></p>
        </td>
      </tr>
      <tr>
        <td colspan="19" style="text-align: left;width: 100%;border:0px solid #ffffff !important;">
          <p style="font-size:12px;"><b>Place : &nbsp;&nbsp;&nbsp;</b></p>
        </td>
      </tr>
    </table>
  </div>
</div></body></html>