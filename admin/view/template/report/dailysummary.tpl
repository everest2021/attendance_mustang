<?php echo $header; ?>
<div id="content">
  	<div class="breadcrumb">
	<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
	<?php } ?>
  	</div>
  	<?php if ($error_warning) { ?>
  		<div class="warning"><?php echo $error_warning; ?></div>
  	<?php } ?>
  	<?php if ($success) { ?>
  		<div class="success"><?php echo $success; ?></div>
  	<?php } ?>
  	<div class="box">
		<div class="heading">
	  		<h1><img src="view/image/report.png" alt="" /> <?php echo $heading_title; ?></h1>
		</div>
	<div class="content sales-report">
	  	<table class="form">
			<tr>
		  		<td style="width:13%;"><?php echo "Name"; ?>
					<input type="text" name="filter_name" value="<?php echo $filter_name;  ?>" id="filter_name" size="25" />
					<input type="hidden" name="filter_name_id" value="<?php echo $filter_name_id; ?>" id="filter_name_id" />
		 	 	</td>
		  		<td style="width:8%;"><?php echo "Date Start"; ?>
					<input type="text" name="filter_date_start" value="<?php echo $filter_date_start; ?>" id="date-start" size="10" />
		  		</td>
		  		<td style="width:8%;"><?php echo "Date End"; ?>
					<input type="text" name="filter_date_end" value="<?php echo $filter_date_end; ?>" id="date-end" size="10" />
		  		</td>

		  		<td style="width:8%">Department
				<select name="department" style="width:100px">
			  	<?php foreach($department_data as $key => $dd) { ?>
					<?php if($key == $department) { ?>
				  		<option value='<?php echo $key; ?>' selected="selected"><?php echo $dd; ?></option> 
					<?php } else { ?>
				  		<option value='<?php echo $key; ?>'><?php echo $dd; ?></option>
					<?php } ?>
			  	<?php } ?>
				</select>
		  		</td>

		  		<td style="width:8%">Designation
					<select name="designation" style="width:100px">
			  		<?php foreach($designation_data as $gkey => $dg) { ?>
						<?php if($gkey == $designation) { ?>
				  			<option value='<?php echo $gkey; ?>' selected="selected"><?php echo $dg; ?></option> 
						<?php } else { ?>
				  			<option value='<?php echo $gkey; ?>'><?php echo $dg; ?></option>
						<?php } ?>
			  		<?php } ?>
					</select>
		  		</td>
		   	
		   		<td style="width:8%">Location
					<select name="unit" style="width:100px">
			  		<?php foreach($unit_data as $key => $ud) { ?>
						<?php if($key == $unit) { ?>
				  			<option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
						<?php } else { ?>
				  			<option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
						<?php } ?>
			  		<?php } ?>
					</select>
		  		</td>

		  		<td style="text-align: right;">
					<a style="padding: 13px 25px;" onclick="filter();" id="filter" class="button"><?php echo $button_filter; ?></a>
					<a style="display: none; padding: 13px 25px;" href="<?php echo $generate; ?>" id="filter_refresh" class="button"><?php echo 'Refresh'; ?></a>
					<a style="padding: 13px 25px;" href="<?php echo $export; ?>" id="filter_export" class="button"><?php echo 'Export'; ?></a>
		  		</td>
			</tr>
	  	</table>
	  	<?php if($dailyreports){ ?>
			<?php foreach($dailyreports as $dkeysss => $dvaluesss){ ?>
		  		<table class="form">
					<tr>
					  	<td class="left">
							<b><?php echo 'Employee Code:'; ?></b><?php echo $dvaluesss['summary_data']['emp_code']; ?>
					  	</td>
					  	<td class="left">
							<b><?php echo 'Employee Name:'; ?></b><?php echo $dvaluesss['summary_data']['emp_name']; ?>
					  	</td>
					  	<td class="left">
							<b><?php echo 'Department:'; ?></b><?php echo $dvaluesss['summary_data']['department']; ?>
					  	</td>
					  	<td class="left">
							<b><?php echo 'Location:'; ?></b><?php echo $dvaluesss['summary_data']['unit']; ?>
					  	</td>
					  	<td class="left">
							<b><?php echo 'Designation:'; ?></b><?php echo $dvaluesss['summary_data']['designation']; ?>
					  	</td>
					</tr>
		  		</table>
		  		<table  class="list">
				<thead >
				  	<tr>
						<td class="left"><?php echo 'Date'; ?></td>
						<td class="left"><?php echo 'InTime'; ?></td>
						<td class="left"><?php echo 'OutTime'; ?></td>
						<td class="left"><?php echo 'Shift'; ?></td>
						<td class="left"><?php echo 'Total Duration'; ?></td>
						<td class="left"><?php echo 'Early Time'; ?></td>
						<td class="left"><?php echo 'Late Time'; ?></td>
						<td class="left"><?php echo 'Over Time'; ?></td>
						<td class="left"><?php echo 'Status'; ?></td>
						<td class="left"><?php echo 'Remarks'; ?></td>
				  	</tr>
				</thead>
				<tbody>
			  	<?php if ($dailyreports) { ?>
					<?php $i = 1; ?>
					<?php foreach ($dvaluesss['trans_data'] as $tkeys => $result) { ?>
			  			<tr>
							<td class="text-left"><?php echo $result['date']; ?></td>
							<td class="text-left"><?php echo $result['act_intime']; ?></td>
							<td class="text-left"><?php echo $result['outtime']; ?></td>
							<td class="text-left"><?php echo $result['shift_name']; ?> </td>
							<td class="text-left"><?php echo $result['duration']; ?> </td>
							<td class="text-left"><?php echo $result['early']; ?></td>
							<td class="text-left"><?php echo $result['late']; ?></td>
							<td class="text-left"> <?php if($result['over_time'] == null) { ?>
							 	<?php echo "00:00:00"; ?>
							<?php } else { ?>
							  	<?php echo $result['over_time']; ?>
							<?php } ?>
							</td>          
							<td class="text-left"><?php echo $result['status_name']; ?></td>
							<td></td>
						</tr>
					<?php } ?>
			  	<?php } ?>
				</tbody>
		  		</table>
		  		<table class="form" style="border-bottom: 2px solid #000000;">
					<tr>
			  			<td><b>
						  	<?php echo 'Total Duration:'; ?> <?php echo $dvaluesss['summary_data']['total_working_hours']  ,' Hrs '; ?><?php echo $dvaluesss['summary_data']['total_working_minutes'],' Min ' ;  ?>
						  	<?php echo 'Present Days:'; ?> <?php echo $dvaluesss['summary_data']['countpresent']; ?>
						  	<?php echo 'Leaves:'; ?> <?php echo $dvaluesss['summary_data']['countleave']; ?>
						  	<?php echo 'Holidays:'; ?> <?php echo $dvaluesss['summary_data']['countholiday']; ?>
						  	<?php echo 'Absent Days:'; ?> <?php echo $dvaluesss['summary_data']['countabsent']; ?>
						  	<?php echo 'Weekly Off:'; ?> <?php echo $dvaluesss['summary_data']['countoff']; ?>
						  	<?php echo 'Weekly Off Present:'; ?> <?php echo $dvaluesss['summary_data']['countoff_present']; ?>
						 </b></td>
					</tr>
					<tr>
			  			<td>
							<b>
				  				<?php echo 'Total OverTime Hours:'; ?> <?php echo $dvaluesss['summary_data']['total_overtime_hours']  ,' Hrs '; ?><?php echo $dvaluesss['summary_data']['total_overtime_minutes'],' Min ' ;  ?>
							</b>
			  			</td>
					</tr>
		  		</table>
			<?php } ?>
	  	<?php } ?>
	</div>
  	</div>
</div>
<script type="text/javascript"><!--
function filter() {
  url = 'index.php?route=report/dailysummary&token=<?php echo $token; ?>';

  var filter_name = $('input[name=\'filter_name\']').attr('value');
  
  if (filter_name) {
	url += '&filter_name=' + encodeURIComponent(filter_name);
	var filter_name_id = $('input[name=\'filter_name_id\']').attr('value');
	if (filter_name_id) {
	  url += '&filter_name_id=' + encodeURIComponent(filter_name_id);
	} else {
	  alert('Please Enter Correct Employee Name');
	  return false;
	}
  }
   var designation = $('select[name=\'designation\']').attr('value');
  
  if (designation) {
	url += '&designation=' + encodeURIComponent(designation);
  }


  
  var filter_date_start = $('input[name=\'filter_date_start\']').attr('value');
  
  if (filter_date_start) {
	url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
  }

  var filter_date_end = $('input[name=\'filter_date_end\']').attr('value');
  
  if (filter_date_end) {
	url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
  }

  var unit = $('select[name=\'unit\']').attr('value');
  
  if (unit) {
	url += '&unit=' + encodeURIComponent(unit);
  }

  var department = $('select[name=\'department\']').attr('value');
  
  if (department) {
	url += '&department=' + encodeURIComponent(department);
  }

  var group = $('select[name=\'group\']').attr('value');
  
  if (group) {
	url += '&group=' + encodeURIComponent(group);
  }

  var status = $('select[name=\'status\']').attr('value');
  
  if (status) {
	url += '&status=' + encodeURIComponent(status);
  }  

  url += '&once=1';
  
  location = url;
  return false;
}

//--></script> 
<script type="text/javascript"><!--
$(document).ready(function() {
  
  //$('#date-start').datepicker({dateFormat: 'yy-mm-dd'});
  //$('#date-end').datepicker({dateFormat: 'yy-mm-dd'});

  $(function() {
	  //$.datepicker.setDefaults($.datepicker.regional['en']);
	  $('#date-start').datepicker({
			dateFormat: 'yy-mm-dd',
			onSelect: function(selectedDate) {
			  var date = $(this).datepicker('getDate');
			  $('#date-end').datepicker('option', 'maxDate', date); // Reset minimum date
			  date.setDate(date.getDate() + 30); // Add 30 days
			  $('#date-end').datepicker('setDate', date); // Set as default
			}
	  });
	  
	  $('#date-end').datepicker({
			dateFormat: 'yy-mm-dd',
			onSelect: function(selectedDate) {
			  $('#date-start').datepicker('option', 'maxDate', $(this).datepicker('getDate')); // Reset maximum date
			}
	  });
	  
  });
  
});

$('input[name=\'filter_name\']').autocomplete({
  delay: 500,
  source: function(request, response) {
	$.ajax({
	  url: 'index.php?route=catalog/employee/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
	  dataType: 'json',
	  success: function(json) {   
		response($.map(json, function(item) {
		  return {
			label: item.name,
			value: item.emp_code
		  }
		}));
	  }
	});
  }, 
  select: function(event, ui) {
	$('input[name=\'filter_name\']').val(ui.item.label);
	$('input[name=\'filter_name_id\']').val(ui.item.value);
	return false;
  },
  focus: function(event, ui) {
	return false;
  }
});

//$(document).ready(function() {
 // $('#date-start').datepicker({dateFormat: 'yy-mm-dd'});
  
//});
$('#region').on('change', function() {
  region = $(this).val();
  $.ajax({
  url: 'index.php?route=catalog/employee/getdivision_location&token=<?php echo $token; ?>&filter_region_id=' +  encodeURIComponent(region),
  dataType: 'json',
  success: function(json) {   
	$('#division').find('option').remove();
	if(json['division_datas']){
	$.each(json['division_datas'], function (i, item) {
	  $('#division').append($('<option>', { 
		value: item.division_id,
		text : item.division 
	  }));
	});
	}
	$('#unit').find('option').remove();
	if(json['unit_datas']){
	$.each(json['unit_datas'], function (i, item) {
	  $('#unit').append($('<option>', { 
		value: item.unit_id,
		text : item.unit 
	  }));
	});
	}
  }
  });
});

$('#division').on('change', function() {
  division = $(this).val();
  $.ajax({
  url: 'index.php?route=catalog/employee/getlocation&token=<?php echo $token; ?>&filter_division_id=' +  encodeURIComponent(division),
  dataType: 'json',
  success: function(json) {   
	$('#unit').find('option').remove();
	if(json['unit_datas']){
	$.each(json['unit_datas'], function (i, item) {
	  $('#unit').append($('<option>', { 
		value: item.unit_id,
		text : item.unit 
	  }));
	});
	}
  }
  });
});
</script>
<?php echo $footer; ?>