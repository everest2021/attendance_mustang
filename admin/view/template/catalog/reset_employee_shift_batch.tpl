<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/report.png" alt="" /> <?php echo "Update Employee Shift"; ?></h1>
    </div>
    <div class="content sales-report">
      <table class="form">
        <tr>
          	
          	<td style="text-align: right;">
            	<a style="padding: 13px 25px;" onclick="reset_employee();" id="filter" class="button"><?php echo 'Process'; ?></a>
          	</td>
        </tr>
      </table>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
function reset_employee() {
  url = 'index.php?route=snippets/reset_employee_shift_batch&token=<?php echo $token; ?>';
  var filter_month = $('select[name=\'filter_month\']').attr('value');
  if (filter_month) {
    url += '&filter_month=' + encodeURIComponent(filter_month);
  }
  var filter_year = $('select[name=\'filter_year\']').attr('value');
  if (filter_year) {
    url += '&filter_year=' + encodeURIComponent(filter_year);
  }
  url += '&once=1';
  location = url;
  return false;
}

//--></script> 

<?php echo $footer; ?>