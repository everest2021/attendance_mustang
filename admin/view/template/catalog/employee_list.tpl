<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
	<?php foreach ($breadcrumbs as $breadcrumb) { ?>
	<?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
	<?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
	<div class="heading">
	  <h1><img src="view/image/shipping.png" alt="" /> <?php echo $heading_title; ?></h1>
	  <div class="buttons">
		<?php if($filter_emp_code == ''){ ?>
		  <a href="<?php echo $insert; ?>" class="button" style=""><?php echo $button_insert; ?></a>
		  <a href="<?php echo $export_depthead; ?>" class="button" style="display:none;"><?php echo 'Export Department head'; ?></a>
		  <a onclick="$('#form').submit();" class="button" style="display:none;"><?php echo $button_delete; ?></a>
		<?php } ?>
	  </div>
	</div>
	<div class="content">
		<?php if($reporting_to_name != ''){ ?>
		  <h4>Reporting To : <?php echo $reporting_to_name; ?></h4>
		<?php } ?>
		<table class="list">
		  <thead>
			<tr>
			  <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
			  <td class="left"><?php if ($sort == 'name') { ?>
				<a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
				<?php } else { ?>
				<a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
				<?php } ?></td>
			  <td>
				<a><?php echo "Code"; ?></a>
			  </td>
			  <td>
				<a><?php echo "Department"; ?></a>
			  </td>
			  <td>
				<a><?php echo "Location"; ?></a>
			  </td>
			  <td style="display: none;">
				<a><?php echo "Category"; ?></a>
			  </td>
			  <td>
				<a><?php echo "Shift"; ?></a>
			  </td>
			  <td style="display: none;">
				<a><?php echo "Department Head"; ?></a>
			  </td>
			  <?php if($filter_emp_code == ''){ ?>
			  <td class="right"><?php echo $column_action; ?></td>
			  <?php } ?>
			</tr>
		  </thead>
		  <tbody>
			<tr class="filter">
			  <td>&nbsp;</td>
			  <td><input type="text" id="filter_name" name="filter_name" value="<?php echo $filter_name; ?>"  style="width:210px;" /></td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td style="display: none;">&nbsp;</td>
			  <td style="display: none;">&nbsp;</td>
			  <?php if($filter_emp_code == ''){ ?>
				<td>&nbsp;</td>
				<td align="right"><a onclick="filter();" class="button"><?php echo $button_filter; ?></a></td>
			  <?php } else { ?>
				<td align="right"><a onclick="filter();" class="button"><?php echo $button_filter; ?></a></td>
			  <?php } ?>
			</tr>
			<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
			  <?php if ($employees) { ?>
			  <?php foreach ($employees as $employee) { ?>
			  <tr>
				<td style="text-align: center;"><?php if ($employee['selected']) { ?>
				  <input type="checkbox" name="selected[]" value="<?php echo $employee['employee_id']; ?>" checked="checked" />
				  <?php } else { ?>
				  <input type="checkbox" name="selected[]" value="<?php echo $employee['employee_id']; ?>" />
				  <?php } ?></td>
				<td class="left"><?php echo $employee['name']; ?></td>
				<td class="left"><?php echo $employee['code']; ?></td>
				<td class="left"><?php echo $employee['deartment']; ?></td>
				<td class="left"><?php echo $employee['location'] ?></td>
				<td class="left" style="display: none;"><?php echo $employee['category']; ?></td>
				<td class="left"><?php echo $employee['shift']; ?></td>
				<td class="left" style="display: none;"><?php echo $employee['dept_head']; ?></td>
				<?php if($filter_emp_code == ''){ ?>
				<td class="right"><?php foreach ($employee['action'] as $action) { ?>
				  [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
				  <?php } ?></td>
				<?php } ?>
			  </tr>
			  <?php } ?>
			  <?php } else { ?>
			  <tr>
				<td class="center" colspan="10"><?php echo $text_no_results; ?></td>
			  </tr>
			  <?php } ?>
			</form>
		  </tbody>
		</table>
	  <div class="pagination"><?php echo $pagination; ?></div>
	</div>
  </div>
</div>
<script type="text/javascript"><!--
function filter() {
  url = 'index.php?route=catalog/employee&token=<?php echo $token; ?>';
  
  var filter_name = $('input[name=\'filter_name\']').attr('value');
  
  if (filter_name) {
	url += '&filter_name=' + encodeURIComponent(filter_name);
  }

  var filter_trainer_id = $('input[name=\'filter_trainer_id\']').attr('value');
  var filter_trainer = $('input[name=\'filter_trainer\']').attr('value');
  if (filter_trainer) {
	url += '&filter_trainer_id=' + encodeURIComponent(filter_trainer_id);
	url += '&filter_trainer=' + encodeURIComponent(filter_trainer);
  }
  
  location = url;
  return false;
}
//--></script>
<script type="text/javascript"><!--
$('#filter_name, #filter_trainer').keydown(function(e) {
  if (e.keyCode == 13) {
	filter();
  }
});
//--></script> 
<script type="text/javascript"><!--
$('input[name=\'filter_name\']').autocomplete({
  delay: 500,
  source: function(request, response) {
	$.ajax({
	  url: 'index.php?route=catalog/employee/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
	  dataType: 'json',
	  success: function(json) {   
		response($.map(json, function(item) {
		  return {
			label: item.name,
			value: item.employee_id
		  }
		}));
	  }
	});
  }, 
  select: function(event, ui) {
	$('input[name=\'filter_name\']').val(ui.item.label);
			
	return false;
  },
  focus: function(event, ui) {
		return false;
	}
});
//--></script>
<script type="text/javascript"><!--
$('input[name=\'filter_trainer\']').autocomplete({
  delay: 500,
  source: function(request, response) {
	$.ajax({
	  url: 'index.php?route=catalog/employee/autocomplete_trainer&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
	  dataType: 'json',
	  success: function(json) {   
		response($.map(json, function(item) {
		  return {
			label: item.name,
			value: item.trainer_id
		  }
		}));
	  }
	});
  }, 
  select: function(event, ui) {
	$('input[name=\'filter_trainer\']').val(ui.item.label);
	$('input[name=\'filter_trainer_id\']').val(ui.item.value);
			
	return false;
  },
  focus: function(event, ui) {
		return false;
	}
});
//--></script>
<?php echo $footer; ?>