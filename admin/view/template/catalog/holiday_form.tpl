<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/shipping.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo 'Save'; ?></a><a href="<?php echo $cancel; ?>" class="button"><?php echo 'Cancel'; ?></a></div>
    </div>
    <div class="content">
      <div id="tabs" class="htabs"><a href="#tab-general"><?php echo 'General'; ?></a></div>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <div id="tab-general">
          <table class="form">
            <tr>
              <td><span class="required">*</span> <?php echo 'Name'; ?></td>
              <td><input type="text" name="name" value="<?php echo $name; ?>" size="100" />
                <?php if ($error_name) { ?>
                <span class="error"><?php echo $error_name; ?></span>
                <?php } ?></td>
            </tr>
            <tr>
              <td><span class="required">*</span> <?php echo 'Date'; ?></td>
              <td><input type="text" id="date" name="date" value="<?php echo $date; ?>" size="100" readonly="readonly" />
                <?php if ($error_date) { ?>
                <span class="error"><?php echo $error_date; ?></span>
                <?php } ?></td>
            </tr>
            <?php /* ?>
            <tr>
              <td><?php echo 'Location'; ?></td>
              <td><div class="scrollbox">
                  <?php $class = 'even'; ?>
                  <?php foreach ($unit_data as $ukey => $uvalue) { ?>
                  <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                  <div class="<?php echo $class; ?>">
                    <?php if (in_array($ukey, $loc_holiday)) { ?>
                    <input type="checkbox" name="loc_holiday[]" value="<?php echo $ukey; ?>" checked="checked" />
                    <?php echo $uvalue; ?>
                    <?php } else { ?>
                    <input type="checkbox" name="loc_holiday[]" value="<?php echo $ukey; ?>" />
                    <?php echo $uvalue; ?>
                    <?php } ?>
                  </div>
                  <?php } ?>
                </div>
              </td>
            </tr>
            <?php */ ?>
            <tr>
              <td><?php echo 'Department (Vasai)'; ?></td>
              <td><div class="scrollbox">
                  <?php $class = 'even'; ?>
                  <?php foreach ($department_data as $dkey => $dvalue) { ?>
                  <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                  <div class="<?php echo $class; ?>">
                    <?php if (in_array($dkey, $dept_holiday_mumbai)) { ?>
                    <input type="checkbox" name="dept_holiday_mumbai[]" value="<?php echo $dkey; ?>" checked="checked" />
                    <?php echo $dvalue; ?>
                    <?php } else { ?>
                    <input type="checkbox" name="dept_holiday_mumbai[]" value="<?php echo $dkey; ?>" />
                    <?php echo $dvalue; ?>
                    <?php } ?>
                  </div>
                  <?php } ?>
                </div>
                <a onclick="$(this).parent().find(':checkbox').attr('checked', true);"><?php echo 'Select All'; ?></a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);"><?php echo 'Unselect All'; ?></a>
              </td>
            </tr>
            <tr style="display: none;">
              <td><?php echo 'Department (Pune)'; ?></td>
              <td><div class="scrollbox">
                  <?php $class = 'even'; ?>
                  <?php foreach ($department_data as $dkey => $dvalue) { ?>
                  <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                  <div class="<?php echo $class; ?>">
                    <?php if (in_array($dkey, $dept_holiday_pune)) { ?>
                    <input type="checkbox" name="dept_holiday_pune[]" value="<?php echo $dkey; ?>" checked="checked" />
                    <?php echo $dvalue; ?>
                    <?php } else { ?>
                    <input type="checkbox" name="dept_holiday_pune[]" value="<?php echo $dkey; ?>" />
                    <?php echo $dvalue; ?>
                    <?php } ?>
                  </div>
                  <?php } ?>
                </div>
                <a onclick="$(this).parent().find(':checkbox').attr('checked', true);"><?php echo 'Select All'; ?></a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);"><?php echo 'Unselect All'; ?></a>
              </td>
            </tr>
            <tr style="display: none;">
              <td><?php echo 'Department (Delhi)'; ?></td>
              <td><div class="scrollbox">
                  <?php $class = 'even'; ?>
                  <?php foreach ($department_data as $dkey => $dvalue) { ?>
                  <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                  <div class="<?php echo $class; ?>">
                    <?php if (in_array($dkey, $dept_holiday_delhi)) { ?>
                    <input type="checkbox" name="dept_holiday_delhi[]" value="<?php echo $dkey; ?>" checked="checked" />
                    <?php echo $dvalue; ?>
                    <?php } else { ?>
                    <input type="checkbox" name="dept_holiday_delhi[]" value="<?php echo $dkey; ?>" />
                    <?php echo $dvalue; ?>
                    <?php } ?>
                  </div>
                  <?php } ?>
                </div>
                <a onclick="$(this).parent().find(':checkbox').attr('checked', true);"><?php echo 'Select All'; ?></a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);"><?php echo 'Unselect All'; ?></a>
              </td>
            </tr>
            <tr style="display: none;">
              <td><?php echo 'Department (Chennai)'; ?></td>
              <td><div class="scrollbox">
                  <?php $class = 'even'; ?>
                  <?php foreach ($department_data as $dkey => $dvalue) { ?>
                  <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                  <div class="<?php echo $class; ?>">
                    <?php if (in_array($dkey, $dept_holiday_chennai)) { ?>
                    <input type="checkbox" name="dept_holiday_chennai[]" value="<?php echo $dkey; ?>" checked="checked" />
                    <?php echo $dvalue; ?>
                    <?php } else { ?>
                    <input type="checkbox" name="dept_holiday_chennai[]" value="<?php echo $dkey; ?>" />
                    <?php echo $dvalue; ?>
                    <?php } ?>
                  </div>
                  <?php } ?>
                </div>
                <a onclick="$(this).parent().find(':checkbox').attr('checked', true);"><?php echo 'Select All'; ?></a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);"><?php echo 'Unselect All'; ?></a>
              </td>
            </tr>
            <tr style="display: none;">
              <td><?php echo 'Department (Bangalore)'; ?></td>
              <td><div class="scrollbox">
                  <?php $class = 'even'; ?>
                  <?php foreach ($department_data as $dkey => $dvalue) { ?>
                  <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                  <div class="<?php echo $class; ?>">
                    <?php if (in_array($dkey, $dept_holiday_bangalore)) { ?>
                    <input type="checkbox" name="dept_holiday_bangalore[]" value="<?php echo $dkey; ?>" checked="checked" />
                    <?php echo $dvalue; ?>
                    <?php } else { ?>
                    <input type="checkbox" name="dept_holiday_bangalore[]" value="<?php echo $dkey; ?>" />
                    <?php echo $dvalue; ?>
                    <?php } ?>
                  </div>
                  <?php } ?>
                </div>
                <a onclick="$(this).parent().find(':checkbox').attr('checked', true);"><?php echo 'Select All'; ?></a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);"><?php echo 'Unselect All'; ?></a>
              </td>
            </tr>
            <tr style="display: none;">
              <td><?php echo 'Department (Ahmedabad)'; ?></td>
              <td><div class="scrollbox">
                  <?php $class = 'even'; ?>
                  <?php foreach ($department_data as $dkey => $dvalue) { ?>
                  <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                  <div class="<?php echo $class; ?>">
                    <?php if (in_array($dkey, $dept_holiday_ahmedabad)) { ?>
                    <input type="checkbox" name="dept_holiday_ahmedabad[]" value="<?php echo $dkey; ?>" checked="checked" />
                    <?php echo $dvalue; ?>
                    <?php } else { ?>
                    <input type="checkbox" name="dept_holiday_ahmedabad[]" value="<?php echo $dkey; ?>" />
                    <?php echo $dvalue; ?>
                    <?php } ?>
                  </div>
                  <?php } ?>
                </div>
                <a onclick="$(this).parent().find(':checkbox').attr('checked', true);"><?php echo 'Select All'; ?></a> / <a onclick="$(this).parent().find(':checkbox').attr('checked', false);"><?php echo 'Unselect All'; ?></a>
              </td>
            </tr>
          </table>
        </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript" src="view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript"><!--
$('#tabs a').tabs(); 

$('.time').timepicker({timeFormat: 'h:m'});
//--></script> 
<script type="text/javascript"><!--
$(document).ready(function() {
  $('#date').datepicker({dateFormat: 'yy-mm-dd'});
  
});
//--></script>
<?php echo $footer; ?>