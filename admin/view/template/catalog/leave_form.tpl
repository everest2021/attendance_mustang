<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($error_warning1) { ?>
  <div class="warning"><?php echo $error_warning1; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/shipping.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons">
        <?php if($editable == 1) { ?>
          <a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a>
        <?php } ?>
        <a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a>
      </div>
    </div>
    <div class="content">
      <div id="tabs" class="htabs"><a href="#tab-general"><?php echo $tab_general; ?></a></div>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <div id="tab-general">
          <table class="form">
            <tr>
              <td><span class="required">*</span> <?php echo 'Employee Name'; ?></td>
              <td>
                <input type="text" readonly="readonly" name="e_name" id="e_name" value="<?php echo $e_name; ?>" size="100" />
                <input type="hidden" name="e_name_id" id="e_name_id" value="<?php echo $e_name_id; ?>" size="100" />
                <input type="hidden" name="leave_id" id="transaction_id" value="<?php echo $leave_id; ?>" />
                <input type="hidden" name="year" id="year" value="<?php echo $year; ?>" />
              </td>
            </tr>
            <tr>
              <td><?php echo 'Employee Code'; ?></td>
              <td>
                <input readonly="readonly" type="text" name="emp_code" value="<?php echo $emp_code; ?>" size="10" />
              </td>
            </tr>
            <tr>
              <td><?php echo 'Date Of Joining'; ?></td>
              <td>
                <input readonly="readonly" type="text" name="emp_doj" value="<?php echo $emp_doj; ?>" size="10" />
              </td>
            </tr>
            <tr style="display: none;">
              <td><?php echo 'Grade'; ?></td>
              <td>
                <input readonly="readonly" type="text" name="emp_grade" value="<?php echo $emp_grade; ?>" size="10" />
              </td>
            </tr>
            <tr>
              <td><?php echo 'Designation'; ?></td>
              <td>
                <input readonly="readonly" type="text" name="emp_designation" value="<?php echo $emp_designation; ?>" size="10" />
              </td>
            </tr>
            <tr>
              <td><?php echo 'PL Opening'; ?></td>
              <td>
                <input <?php echo ($editable == 1) ? '' : 'readonly="readonly"'; ?> type="text" name="pl_acc" value="<?php echo $pl_acc; ?>" size="10" />
              </td>
            </tr>
            
            <tr>
              <td><?php echo 'SL Opening'; ?></td>
              <td>
                <input <?php echo ($editable == 1) ? '' : 'readonly="readonly"'; ?> type="text" name="sl_acc" value="<?php echo $sl_acc; ?>" size="10" />
              </td>
            </tr>
            <tr>
              <td><?php echo 'BL Opening'; ?></td>
              <td>
                <input <?php echo ($editable == 1) ? '' : 'readonly="readonly"'; ?> type="text" name="bl_acc" value="<?php echo $bl_acc; ?>" size="10" />
              </td>
            </tr>

            <tr>
              <td><?php echo 'LWP Opening'; ?></td>
              <td>
                <input <?php echo ($editable == 1) ? '' : 'readonly="readonly"'; ?> type="text" name="lwp_acc" value="<?php echo $lwp_acc; ?>" size="10" />
              </td>
            </tr>
            <tr>
              <td><?php echo 'ML Opening'; ?></td>
              <td>
                <input <?php echo ($editable == 1) ? '' : 'readonly="readonly"'; ?> type="text" name="ml_acc" value="<?php echo $ml_acc; ?>" size="10" />
              </td>
            </tr>
            <tr>
              <td><?php echo 'MAL Opening'; ?></td>
              <td>
                <input <?php echo ($editable == 1) ? '' : 'readonly="readonly"'; ?> type="text" name="mal_acc" value="<?php echo $mal_acc; ?>" size="10" />
              </td>
            </tr>
            <tr>
              <td><?php echo 'PAL Opening'; ?></td>
              <td>
                <input <?php echo ($editable == 1) ? '' : 'readonly="readonly"'; ?> type="text" name="pal_acc" value="<?php echo $pal_acc; ?>" size="10" />
              </td>
            </tr>
            <tr style="display: none;">
              <td><?php echo 'COF Opening'; ?></td>
              <td>
                <input <?php echo ($editable == 1) ? '' : 'readonly="readonly"'; ?> type="text" name="cof_acc" value="<?php echo $cof_acc; ?>" size="10" />
              </td>
            </tr>
          </table>
        </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
$('#tabs a').tabs();
//--></script>
<?php echo $footer; ?>
