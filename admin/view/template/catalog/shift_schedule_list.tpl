<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/shipping.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons">
        <a href="<?php //echo $insert; ?>" class="button" style="display:none;"><?php //echo $button_insert; ?></a>
        <a onclick="$('#form').submit();" class="button" style="display:none;"><?php //echo $button_delete; ?></a>
      </div>
    </div>
    <div class="content">
        <table class="list">
          <thead>
            <tr>
              <td class="left"><?php if ($sort == 'name') { ?>
                <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Name'; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_name; ?>"><?php echo 'Name'; ?></a>
                <?php } ?></td>
              <td class="left"><?php if ($sort == 'emp_code') { ?>
                <a href="<?php echo $sort_code; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Emp Code'; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_code; ?>"><?php echo 'Emp Code'; ?></a>
                <?php } ?></td>
              <td class="left"><?php if ($sort == 'department') { ?>
                <a href="<?php echo $sort_department; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Department'; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_department; ?>"><?php echo 'Department'; ?></a>
                <?php } ?></td>
              <td class="left"><?php if ($sort == 'unit') { ?>
                <a href="<?php echo $sort_unit; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Location'; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_unit; ?>"><?php echo 'Location'; ?></a>
                <?php } ?></td>              
              <td class="right"><?php echo 'Action'; ?></td>
            </tr>
          </thead>
          <tbody>
            <tr class="filter">
              <td>
                <input type="text" id="filter_name" name="filter_name" value="<?php echo $filter_name; ?>"  style="width:210px;" />
              </td>
              <td>
                <input type="text" id="filter_code" name="filter_code" value="<?php echo $filter_code; ?>"  style="width:210px;" />
              </td>
              <td>
                <select name="filter_department" id="filter_department">
                  <?php foreach($department_data as $key => $ud) { ?>
                    <?php if($key === $filter_department) { ?>
                      <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                    <?php } else { ?>
                      <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
              <td>
                <select name="filter_unit" id="filter_unit">
                  <?php foreach($unit_data as $key => $ud) { ?>
                    <?php if($key === $filter_unit) { ?>
                      <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                    <?php } else { ?>
                      <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
              <td align="right"><a onclick="filter();" class="button"><?php echo 'Filter'; ?></a></td>
            </tr>
            <form action="" method="post" enctype="multipart/form-data" id="form">
              <?php if ($employees) { ?>
              <?php foreach ($employees as $employee) { ?>
              <tr>
                <td class="left"><?php echo $employee['name']; ?></td>
                <td class="left"><?php echo $employee['employee_code']; ?></td>
                <td class="left"><?php echo $employee['department']; ?></td>
                <td class="left"><?php echo $employee['unit']; ?></td>
                <td class="right"><?php foreach ($employee['action'] as $action) { ?>
                  [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                  <?php } ?></td>
              </tr>
              <?php } ?>
              <?php } else { ?>
              <tr>
                <td class="center" colspan="5"><?php echo $text_no_results; ?></td>
              </tr>
              <?php } ?>
            </form>
          </tbody>
        </table>
      <div class="pagination"><?php echo $pagination; ?></div>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
function filter() {
  url = 'index.php?route=catalog/shift_schedule&token=<?php echo $token; ?>';
  
  var filter_name = $('input[name=\'filter_name\']').attr('value');
  if (filter_name) {
    url += '&filter_name=' + encodeURIComponent(filter_name);
  }

  var filter_code = $('input[name=\'filter_code\']').attr('value');
  if (filter_code) {
    url += '&filter_code=' + encodeURIComponent(filter_code);
  }

  var filter_department = $('select[name=\'filter_department\']').attr('value');
  if (filter_department) {
    url += '&filter_department=' + encodeURIComponent(filter_department);
  }

  var filter_unit = $('select[name=\'filter_unit\']').attr('value');
  if (filter_unit) {
    url += '&filter_unit=' + encodeURIComponent(filter_unit);
  }

  location = url;
  return false;
}
//--></script>
<script type="text/javascript"><!--
$('#filter_name, #filter_code').keydown(function(e) {
  if (e.keyCode == 13) {
    filter();
  }
});
//--></script> 
<script type="text/javascript"><!--
$('input[name=\'filter_name\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/employee/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.name,
            value: item.employee_id
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_name\']').val(ui.item.label);
            
    return false;
  },
  focus: function(event, ui) {
        return false;
    }
});
//--></script>
<?php echo $footer; ?>