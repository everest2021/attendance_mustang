<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/shipping.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons">
        <a tabindex="5" onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a>
        <a tabindex="6" href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a>
      </div>
    </div>
    <div class="content">
      <div id="tabs" class="htabs">
        <a href="#tab-general"><?php echo $tab_general; ?></a>
      </div>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <div id="tab-general">
          <table class="form">
            <tr>
              <td><?php echo 'Name'; ?></td>
              <td><input tabindex="1" type="text" name="name" value="<?php echo $name; ?>" size="50" /></td>
            </tr>
            <tr>
              <td><?php echo 'Type'; ?></td>
              <td>
                <select tabindex="2" name="type" id="type">
                  <option value="0">None</option>
                  <?php foreach($types as $tkey => $tvalue) { ?>
                    <?php if ($tkey == $type) { ?>
                      <option value="<?php echo $tkey; ?>" selected="selected"><?php echo $tvalue; ?></option>
                    <?php } else { ?>
                      <option value="<?php echo $tkey; ?>"><?php echo $tvalue; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
            </tr>
            <tr>
              <td><?php echo 'Percent'; ?></td>
              <td><input tabindex="3" type="text" name="percent" value="<?php echo $percent; ?>" /></td>
            </tr>
            <tr>
              <td><?php echo $entry_status; ?></td>
              <td><select tabindex="4" name="status">
                  <?php if ($status) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select></td>
            </tr>
          </table>
        </div>
      </form>
    </div>
  </div>
</div>
<?php echo $footer; ?>