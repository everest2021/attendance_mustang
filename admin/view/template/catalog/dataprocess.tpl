<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/report.png" alt="" /> <?php echo "Muster Employee Update Attendance"; ?></h1>
    </div>
    <div class="content sales-report">
      <table class="form">
        <tr>
          	<td style="width:13%">Month
            	<select name="filter_month">
              		<?php foreach($months as $key => $ud) { ?>
                		<?php if($key == $filter_month) { ?>
                  			<option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                		<?php } else { ?>
                  			<option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                		<?php } ?>
              		<?php } ?>
            	</select>
          	</td>
			<td style="width:13%">Year
            	<select name="filter_year">
              		<?php foreach($years as $key => $dd) { ?>
		                <?php if($key == $filter_year) { ?>
		                  <option value='<?php echo $key; ?>' selected="selected"><?php echo $dd; ?></option> 
		                <?php } else { ?>
		                  <option value='<?php echo $key; ?>'><?php echo $dd; ?></option>
		                <?php } ?>
	              	<?php } ?>
            	</select>
          	</td>
          	<td style="text-align: right;">
            	<a style="padding: 13px 25px;" onclick="filter();" id="filter" class="button"><?php echo 'Process'; ?></a>
          	</td>
        </tr>
      </table>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
function filter() {
  url = 'index.php?route=snippets/dataprocess&token=<?php echo $token; ?>';
  var filter_month = $('select[name=\'filter_month\']').attr('value');
  if (filter_month) {
    url += '&filter_month=' + encodeURIComponent(filter_month);
  }
  var filter_year = $('select[name=\'filter_year\']').attr('value');
  if (filter_year) {
    url += '&filter_year=' + encodeURIComponent(filter_year);
  }
  url += '&once=1';
  location = url;
  return false;
}

//--></script> 
<script type="text/javascript">
//close_window();
//function close_window(){
  //var win = window.open("about:blank", "_self"); 
  //win.close();
  //window.open('', '_self', '');
  //window.close();
//}
</script>
<?php echo $footer; ?>