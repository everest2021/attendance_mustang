<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/shipping.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form1').submit();" class="button"><?php echo 'Save'; ?></a><a href="<?php echo $cancel; ?>" class="button"><?php echo 'Cancel'; ?></a></div>
    </div>
    <div class="content">
      <div id="tabs" class="htabs"><a href="#tab-general"><?php echo 'General'; ?></a></div>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form1">
        <div id="tab-general">
          <table class="form1">
            <tr>
              <td style="font-weight:bold;">
                Month
              </td>
              <td>
                <select name="filter_month" id="filter_month">
                  <?php foreach($months as $mkey => $mvalue){ ?>
                    <?php if($mkey == $filter_month) { ?>
                      <option value='<?php echo $mkey; ?>' selected="selected"><?php echo $mvalue; ?></option> 
                    <?php } else { ?>
                      <option value='<?php echo $mkey; ?>'><?php echo $mvalue; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
              <td style="font-weight:bold;">
                Year
              </td>
              <td>
                <select name="filter_year" id="filter_year">
                  <?php foreach($years as $mkey => $mvalue){ ?>
                    <?php if($mkey == $filter_year) { ?>
                      <option value='<?php echo $mkey; ?>' selected="selected"><?php echo $mvalue; ?></option> 
                    <?php } else { ?>
                      <option value='<?php echo $mkey; ?>'><?php echo $mvalue; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
              <td style="text-align: left;">
                <a style="padding: 13px 25px;" onclick="filter();" id="filter" class="button"><?php echo 'Filter'; ?></a>
              </td>
            </tr>
            <tr>
              <td style="width:15%;font-weight:bold;">
                Employee Name
              </td>
              <td>
                <?php echo $emp_name ?>
              </td>
            </tr>
            <tr>
              <td><?php echo 'Day'; ?></td>
              <td style="width:16%;"><?php echo 'Shift'; ?></td>
              <td style="width:9%;"><?php echo 'Weekly Off'; ?></td>
              <td style="width:9%;"><?php echo 'Holiday'; ?></td>
              <td style="width:9%; display:none;"><?php echo 'Halfday'; ?></td>
              <td style="display:none;"><?php echo 'Compensatory'; ?></td>
            </tr>
            <?php for($i = 1; $i <= 31; $i ++) { ?>
              <tr>
                <td><?php echo $i; ?></td>
                <td>
                  <select name="shift_id[]" id="shift_<?php echo $i ?>" <?php echo ($i == 1 || $i == 16) ? 'class="changesel"' : '' ?> >
                    <?php foreach($shift_data as $skey => $svalue) { ?>
                      <?php if($skey == $shift_day_data[$i]) { ?>
                        <option value="<?php echo $skey; ?>" selected="selected"><?php echo $svalue ?></option>
                      <?php } else { ?>
                        <option value="<?php echo $skey; ?>"><?php echo $svalue ?></option>
                      <?php } ?>
                    <?php } ?>
                  </select>
                </td>
                <td>
                  <select name="week_id[]" id="wo_<?php echo $i ?>" <?php echo ($i == 1 || $i == 16) ? 'class="wochange"' : '' ?> >
                    <?php foreach($week_data as $wkey => $wvalue) { ?>
                      <?php if($week_day_data[$i] != '') { ?>
                        <option value="<?php echo $wkey; ?>" selected="selected"><?php echo $wvalue ?></option>
                      <?php } else { ?>
                        <option value="<?php echo $wkey; ?>"><?php echo $wvalue ?></option>
                      <?php } ?>
                    <?php } ?>
                  </select>
                </td>
                <td>
                  <select name="holiday_id[]" id="hld_<?php echo $i ?>" <?php echo ($i == 1 || $i == 16) ? 'class="hldchange"' : '' ?> >
                    <?php foreach($holiday_data as $hkey => $hvalue) { ?>
                      <?php if($holiday_day_data[$i] != '') { ?>
                        <option value="<?php echo $hkey; ?>" selected="selected"><?php echo $hvalue ?></option>
                      <?php } else { ?>
                        <option value="<?php echo $hkey; ?>"><?php echo $hvalue ?></option>
                      <?php } ?>
                    <?php } ?>
                  </select>
                </td>
                <td style="display:none;">
                  <select name="halfday_id[]" id="hd_<?php echo $i ?>" <?php echo ($i == 1 || $i == 16) ? 'class="hdchange"' : '' ?> >
                    <?php foreach($halfday_data as $hkey => $hvalue) { ?>
                      <?php if($halfday_day_data[$i] != '') { ?>
                        <option value="<?php echo $hkey; ?>" selected="selected"><?php echo $hvalue ?></option>
                      <?php } else { ?>
                        <option value="<?php echo $hkey; ?>"><?php echo $hvalue ?></option>
                      <?php } ?>
                    <?php } ?>
                  </select>
                </td>
                <td style="display:none;">
                  <select name="compli_id[]" id="co_<?php echo $i ?>" <?php echo ($i == 1 || $i == 16) ? 'class="cochange"' : '' ?> >
                    <?php foreach($compli_data as $hkey => $hvalue) { ?>
                      <?php if($compli_day_data[$i] != '') { ?>
                        <option value="<?php echo $hkey; ?>" selected="selected"><?php echo $hvalue ?></option>
                      <?php } else { ?>
                        <option value="<?php echo $hkey; ?>"><?php echo $hvalue ?></option>
                      <?php } ?>
                    <?php } ?>
                  </select>
                </td>
              </tr>
            <?php } ?>
          </table>
        </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript" src="view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript"><!--
function filter() {
  url = 'index.php?route=catalog/shift_schedule/getForm&token=<?php echo $token; ?>';
  
  var filter_month = $('select[name=\'filter_month\']').attr('value');
  if (filter_month) {
  url += '&filter_month=' + encodeURIComponent(filter_month);
  }

  var filter_year = $('select[name=\'filter_year\']').attr('value');
  if (filter_year && filter_year != '0') {
  url += '&filter_year=' + encodeURIComponent(filter_year);
  }

  emp_code = "<?php echo $this->request->get['employee_code'] ?>";
  url += '&employee_code=' +encodeURIComponent(emp_code);

  location = url;
  return false;
}

$('#tabs a').tabs(); 

$('.time').timepicker({timeFormat: 'h:m'});
//--></script> 
<script type="text/javascript"><!--
$(document).ready(function() {
  $('#date').datepicker({dateFormat: 'yy-mm-dd'});
  
});

$(".changesel").change(function() {
  var ID = $(this).attr('id');
  var index = ID.split('_');
  console.log(ID);
  console.log(index[1]);
  if(index[1] == 1){
    for(i=1;i<=31;i++){
      value = $('#'+ID).val();
      $('#shift_'+i).val(value);    
    }
  } else {
    for(i=16;i<=31;i++){
      value = $('#'+ID).val();
      $('#shift_'+i).val(value);    
    }
  }
});

$(".wochange, .hldchange, .hdchange, .cochange").change(function() {
  var ID = $(this).attr('id');
  var index = ID.split('_');
  if(index[1] == 1){
    if($(this).val() == 1){
      //$('#shift_1').val(0);  
    } else {
      //$('#shift_1').val(17);
    } 
  } else {
    if($(this).val() == 1){
      //$('#shift_16').val(0);  
    } else {
      //$('#shift_16').val(17);
    }
  }
});


//--></script>
<?php echo $footer; ?>