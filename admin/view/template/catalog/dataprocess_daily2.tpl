<?php echo $header; ?>
<div id="content">
  	<div class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
			<?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
		<?php } ?>
  	</div>
  	<?php if ($error_warning) { ?>
  		<div class="warning"><?php echo $error_warning; ?></div>
  	<?php } ?>
  	<?php if ($success) { ?>
  		<div class="success"><?php echo $success; ?></div>
  	<?php } ?>
  	<div class="box">
		<div class="heading">
	  		<h1><img src="view/image/report.png" alt="" /> <?php echo "Daily Employee Update Attendance 2"; ?></h1>
		</div>
	<div class="content sales-report">
	  	<table class="form">
		<tr>
			<td style="width:13%">Month
				<select name="filter_month">
					<?php foreach($months as $key => $ud) { ?>
						<?php if($key == $filter_month) { ?>
							<option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
						<?php } else { ?>
							<option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
						<?php } ?>
					<?php } ?>
				</select>
			</td>
			<td style="width:13%">Year
				<select name="filter_year">
					<?php foreach($years as $key => $dd) { ?>
						<?php if($key == $filter_year) { ?>
						  <option value='<?php echo $key; ?>' selected="selected"><?php echo $dd; ?></option> 
						<?php } else { ?>
						  <option value='<?php echo $key; ?>'><?php echo $dd; ?></option>
						<?php } ?>
					<?php } ?>
				</select>
			</td>
			<td style="">Location
				<select name="filter_location">
					<?php foreach($unit_datas as $lkey => $ldd) { ?>
						<?php if($lkey == $filter_location) { ?>
						  <option value='<?php echo $lkey; ?>' selected="selected"><?php echo $ldd; ?></option> 
						<?php } else { ?>
						  <option value='<?php echo $lkey; ?>'><?php echo $ldd; ?></option>
						<?php } ?>
					<?php } ?>
				</select>
			</td>
			<td style="text-align: right;">
				<a style="padding: 13px 25px;" onclick="filter();" id="filter" class="button"><?php echo 'Process'; ?></a>
			</td>
		</tr>
	  </table>
	</div>
  </div>
</div>
<script type="text/javascript"><!--
function filter() {
  url = 'index.php?route=snippets/dataprocess_daily2&token=<?php echo $token; ?>';
  var filter_month = $('select[name=\'filter_month\']').attr('value');
  if (filter_month) {
	url += '&filter_month=' + encodeURIComponent(filter_month);
  }
  var filter_year = $('select[name=\'filter_year\']').attr('value');
  if (filter_year) {
	url += '&filter_year=' + encodeURIComponent(filter_year);
  }
   var filter_location = $('select[name=\'filter_location\']').attr('value');
  if (filter_location) {
	url += '&filter_location=' + encodeURIComponent(filter_location);
  }
  url += '&once=1';
  location = url;
  return false;
}

//--></script> 
<script type="text/javascript">
//close_window();
//function close_window(){
  //var win = window.open("about:blank", "_self"); 
  //win.close();
  //window.open('', '_self', '');
  //window.close();
//}
</script>
<?php echo $footer; ?>