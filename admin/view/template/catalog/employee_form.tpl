<?php echo $header; ?>
<div id="content">
	<div class="breadcrumb">
			<?php foreach ($breadcrumbs as $breadcrumb) { ?>
			<?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
			<?php } ?>
	</div>
	<?php if ($error_warning) { ?>
	<div class="warning"><?php echo $error_warning; ?></div>
	<?php } ?>
	<div class="box">
		<div class="heading">
			<h1><img src="view/image/shipping.png" alt="" /> <?php echo $heading_title; ?></h1>
			<div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a></div>
		</div>
		<div class="content">
			<div id="tabs" class="htabs"><a href="#tab-general"><?php echo $tab_general; ?></a></div>
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
					<div id="tab-general">
						<table class="form">
								<tr>
									<td><span class="required">*</span> <?php echo $entry_name; ?></td>
									<td>
										<input type="text" name="name" value="<?php echo $name; ?>" size="100" />
										<input type="hidden" name="hidden_name" value="<?php echo $hidden_name; ?>" size="100" />
										<?php if ($error_name) { ?>
										<span class="error"><?php echo $error_name; ?></span>
										<?php } ?>
									</td>
								</tr>
								<tr>
									<td><?php echo "Emp Code"; ?></td>
									<td>
										<input type="text" name="emp_code" value="<?php echo $emp_code; ?>" size="100" />
										<input type="hidden" name="hidden_emp_code" value="<?php echo $hidden_emp_code; ?>" size="50" />
									</td>
								</tr>
								<tr >
									<td><span class="required">*</span> <?php echo 'Shift Type'; ?></td>
									<td>
										<select name="shift_type">
											<?php if($shift_type == 'F' || $shift_type == ''){ ?>
													<option value = "F" selected="selected"><?php echo 'Fixed'; ?></option>
													<option value = "R"><?php echo 'Rotate'; ?></option>
											<?php } else { ?>
													<option value = "R" selected="selected"><?php echo 'Rotate'; ?></option>
													<option value = "F"><?php echo 'Fixed'; ?></option>
											<?php } ?>
										</select>
									</td>
								</tr>

								<tr >
									<td><span class="required">*</span> <?php echo 'Location'; ?></td>
									<td>
										<select name="locations_id">
											<?php foreach ($locations_array as $lkey => $lvalue) { ?>
												<?php if ($lkey == $locations_id) { ?>
													<option value="<?php echo $lkey; ?>" selected="selected"><?php echo $lvalue; ?></option>
												<?php } else { ?>
													<option value="<?php echo $lkey; ?>" ><?php echo $lvalue; ?></option>
												<?php } ?>
											<?php } ?>
										</select>
									</td>
								</tr>

								<tr>
									<td><?php echo "Designation"; ?></td>
									<td>
										<input type="text" name="designation" id="designation" value="<?php echo $designation; ?>" size="100" />
										<input type="hidden" name="designation_id" id="designation_id" value="<?php echo $designation_id; ?>" size="100" />
									</td>
								</tr>
								<tr style="display: none;">
									<td><span class="required">*</span> <?php echo "Grade"; ?></td>
									<td>
											<input type="text" name="grade" id="grade" value="<?php echo $grade; ?>" size="100" />
											<input type="hidden" name="grade_id" id="grade_id" value="<?php echo $grade_id; ?>" size="100" />
									</td>
								</tr>
								<tr>
									<td> <?php echo "Department"; ?></td>
									<td>
										<input type="text" name="department" id="department" value="<?php echo $department; ?>" size="100" />
										<input type="hidden" name="department_id" id="department_id" value="<?php echo $department_id; ?>" />
									</td>
								</tr>
								<tr>
									<td><?php echo "Gender"; ?></td>
									<td>
										<select name="gender">
												<option value="0">None</option>
												<?php foreach($genders as $gkey => $gvalue) { ?>
														<?php if ($gkey == $gender) { ?>
																<option value="<?php echo $gkey; ?>" selected="selected"><?php echo $gvalue; ?></option>
														<?php } else { ?>
																<option value="<?php echo $gkey; ?>"><?php echo $gvalue; ?></option>
														<?php } ?>
												<?php } ?>
										</select>
									</td>
								</tr>

								<tr style="display: none;">
									<td><span class="required">*</span> <?php echo "Reporting To"; ?></td>
									<td>
										<input type="text" name="reporting_to_name" id="reporting_to_name" value="<?php echo $reporting_to_name; ?>" size="100" />
										<input type="hidden" name="reporting_to" id="reporting_to" value="<?php echo $reporting_to; ?>" />
									</td>
								</tr>
								
								<tr>
									<td> <?php echo "Date Of Birth"; ?></td>
									<td>
										<input type="text" name="dob" value="<?php echo $dob; ?>" size="100" class="date" />
									</td>
								</tr>
								<tr>
									<td> <?php echo "Date Of Joining"; ?></td>
									<td>
										<input type="text" name="doj" value="<?php echo $doj; ?>" size="100" class="date" />
									</td>
								</tr>
								<tr>
									<td><?php echo "Date Of Confirmation"; ?></td>
									<td>
										<input type="text" name="doc" value="<?php echo $doc; ?>" size="100" class="date" />
									</td>
								</tr>
								<tr>
									<td> <?php echo "Date Of Left"; ?></td>
									<td>
										<input type="text" name="dol" value="<?php echo $dol; ?>" size="100" class="date" />
									</td>
								</tr>
								<tr>
									<td><?php echo "Unit"; ?></td>
									<td>
										<select name="unit" id="unit">
											<option value="0">None</option>
											<?php foreach($loc_datas as $gkey => $gvalue) { ?>
												<?php if ($gkey == $unit_id) { ?>
														<option value="<?php echo $gkey; ?>" selected="selected"><?php echo $gvalue; ?></option>
												<?php } else { ?>
														<option value="<?php echo $gkey; ?>"><?php echo $gvalue; ?></option>
												<?php } ?>
											<?php } ?>
										</select>
									</td>
								</tr>
								<tr>
									<td> <?php echo "ESIC Number"; ?></td>
									<td>
										<input type="text" name="esic_no" value="<?php echo $esic_no; ?>" size="100" />
									</td>
								</tr>
								<tr>
									<td> <?php echo "UAN Number"; ?></td>
									<td><input type="text" name="pfuan_no" value="<?php echo $pfuan_no; ?>" size="100" />
									</td>
								</tr>
								<tr>
									<td> <?php echo "Basic"; ?></td>
									<td>
										<input type="text" name="basic" value="<?php echo $basic; ?>" size="100" />
									</td>
								</tr>
								<tr>
									<td> <?php echo "Incentive"; ?></td>
									<td>
										<input type="text" name="incentive" value="<?php echo $incentive; ?>" size="100" />
									</td>
								</tr>
								<tr>
									<td> <?php echo "Per Day Salary 1"; ?></td>
									<td>
										<input type="text" name="perdaysalary" value="<?php echo $perdaysalary; ?>" size="100" />
									</td>
								</tr>
								<tr>
									<td> <?php echo "Per Day Salary 2"; ?></td>
									<td>
										<input type="text" name="perdaysalary2" value="<?php echo $perdaysalary2; ?>" size="100" />
									</td>
								</tr>
								<tr>
									<td><?php echo 'OT Calculate'; ?></td>
									<td>
										<select name="ot_calculate">
											<?php if($ot_calculate == '1'){ ?>
												<option value = "1" selected="selected"><?php echo 'Yes'; ?></option>
												<option value = "0"><?php echo 'No'; ?></option>
											<?php } else { ?>
												<option value = "0" selected="selected"><?php echo 'No'; ?></option>
												<option value = "1"><?php echo 'Yes'; ?></option>
											<?php } ?>
										</select>
									</td>
								</tr>
								<tr>
									<td><?php echo "Working Hours"; ?></td>
									<td>
										<select name="work_hour" id="work_hour">
											<?php foreach($work_hours as $wkey => $wvalue) { ?>
												<?php if ($wkey == $work_hour) { ?>
													<option value="<?php echo $wkey; ?>" selected="selected"><?php echo $wvalue; ?></option>
												<?php } else { ?>
													<option value="<?php echo $wkey; ?>"><?php echo $wvalue; ?></option>
												<?php } ?>
											<?php } ?>
										</select>
									</td>
								</tr>
								<tr>
									<td><?php echo 'Lunch Deduct'; ?></td>
									<td>
										<select name="is_lunch">
											<?php if($is_lunch == '1'){ ?>
												<option value = "1" selected="selected"><?php echo 'Yes'; ?></option>
												<option value = "0"><?php echo 'No'; ?></option>
											<?php } else { ?>
												<option value = "0" selected="selected"><?php echo 'No'; ?></option>
												<option value = "1"><?php echo 'Yes'; ?></option>
											<?php } ?>
										</select>
									</td>
								</tr>
								<tr>
									<td><?php echo '3 Working Hours Deduct'; ?></td>
									<td>
										<select name="work_hour_cut">
											<?php if($work_hour_cut == '1'){ ?>
												<option value = "1" selected="selected"><?php echo 'Yes'; ?></option>
												<option value = "0"><?php echo 'No'; ?></option>
											<?php } else { ?>
												<option value = "0" selected="selected"><?php echo 'No'; ?></option>
												<option value = "1"><?php echo 'Yes'; ?></option>
											<?php } ?>
										</select>
									</td>
								</tr>	
								<tr>
									<td> <?php echo "OT Hours"; ?></td>
									<td>
										<input type="text" name="ot_hours" value="<?php echo $ot_hours; ?>" size="100" />
									</td>
								</tr>
								<tr>
									<td> <?php echo "OT Days"; ?></td>
									<td>
										<input type="text" name="ot_days" value="<?php echo $ot_days; ?>" size="100" />
									</td>
								</tr>							
								<tr style="display:none;">
										<td><?php echo 'Department List'; ?></td>
										<td><div class="scrollbox">
														<?php $class = 'even'; ?>
														<?php foreach ($department_data as $dkey => $dvalue) { ?>
														<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
														<div class="<?php echo $class; ?>">
																<?php if (in_array($dkey, $dept_head_list)) { ?>
																<input type="checkbox" name="dept_head_list[]" value="<?php echo $dkey; ?>" checked="checked" />
																<?php echo $dvalue; ?>
																<?php } else { ?>
																<input type="checkbox" name="dept_head_list[]" value="<?php echo $dkey; ?>" />
																<?php echo $dvalue; ?>
																<?php } ?>
														</div>
														<?php } ?>
												</div>
										</td>
								</tr>
								<tr style="display: none;">
										<td><?php echo 'Password Reset'; ?></td>
										<td>
												<select name="password_reset">
														<?php if($password_reset == '1'){ ?>
																<option value = "1" selected="selected"><?php echo 'Yes'; ?></option>
																<option value = "0"><?php echo 'No'; ?></option>
														<?php } else { ?>
																<option value = "0" selected="selected"><?php echo 'No'; ?></option>
																<option value = "1"><?php echo 'Yes'; ?></option>
														<?php } ?>
												</select>
										</td>
								</tr>
								<tr>
										<td> <?php echo 'Status'; ?></td>
										<td>
												<select name="status">
														<?php if($status == 1){ ?>
																<option value = "1" selected="selected"><?php echo 'Active'; ?></option>
																<option value = "0"><?php echo 'Inactive'; ?></option>
														<?php } else { ?>
																<option value = "0" selected="selected"><?php echo 'Inactive'; ?></option>
																<option value = "1"><?php echo 'Active'; ?></option>
														<?php } ?>
												</select>
												<?php if ($error_status) { ?>
												<span class="error"><?php echo $error_status; ?></span>
												<?php } ?>
										</td>
								</tr>
								<tr style="display: none;">
									<td> <?php echo "Shift"; ?></td>
									<td>
										<input type="text" name="shift_name" value="<?php echo $shift_name; ?>" size="100" readonly="readonly" />
										<input type="hidden" name="shift_id" value="<?php echo $shift_id; ?>" />
									</td>
								</tr>
								<tr style="display: none;">
									<td><span class="required">*</span> <?php echo 'Card Number'; ?></td>
									<td>
										<input type="text" name="card_number" value="<?php echo $card_number; ?>" size="100" />
										<?php if ($error_card_number) { ?>
										<span class="error"><?php echo $error_card_number; ?></span>
										<?php } ?>
									</td>
								</tr>
								<tr style="display: none;">
									<td><span class="required">*</span> <?php echo 'Daily Punch'; ?></td>
									<td>
										<select name="daily_punch">
											<?php if($daily_punch == 1){ ?>
													<option value = "1" selected="selected"><?php echo 'Yes'; ?></option>
													<option value = "0"><?php echo 'No'; ?></option>
											<?php } else { ?>
													<option value = "0" selected="selected"><?php echo 'No'; ?></option>
													<option value = "1"><?php echo 'Yes'; ?></option>
											<?php } ?>
										</select>
										<?php if ($error_daily_punch) { ?>
											<span class="error"><?php echo $error_daily_punch; ?></span>
										<?php } ?>
									</td>
								</tr>
								<tr style="display: none;">
									<td><span class="required">*</span> <?php echo 'Weekly Off'; ?></td>
									<td>
										<select name="weekly_off">
											<option value="0">None</option>
											<?php foreach($weeks as $wkey => $wvalue) { ?>
												<?php if ($wkey == $weekly_off) { ?>
														<option value="<?php echo $wkey; ?>" selected="selected"><?php echo $wvalue; ?></option>
												<?php } else { ?>
														<option value="<?php echo $wkey; ?>"><?php echo $wvalue; ?></option>
												<?php } ?>
											<?php } ?>
										</select>
										<?php if ($error_weekly_off) { ?>
										<span class="error"><?php echo $error_weekly_off; ?></span>
										<?php } ?>
									</td>
								</tr>
								<tr style="display: none;">
									<td><?php echo 'Super User'; ?></td>
									<td>
										<select name="is_super">
											<?php if($is_super == '1'){ ?>
												<option value = "1" selected="selected"><?php echo 'Yes'; ?></option>
												<option value = "0"><?php echo 'No'; ?></option>
											<?php } else { ?>
												<option value = "0" selected="selected"><?php echo 'No'; ?></option>
												<option value = "1"><?php echo 'Yes'; ?></option>
											<?php } ?>
										</select>
									</td>
								</tr>
								<tr style="display: none;">
									<td><span class="required">*</span> <?php echo "Branch"; ?></td>
									<td>
										<input type="text" name="branch" value="<?php echo $branch; ?>" size="100" readonly="readonly" />
									</td>
								</tr>
								<tr style="display: none;">
									<td><span class="required">*</span> <?php echo "Division"; ?></td>
									<td>
										<input type="text" name="division" value="<?php echo $division; ?>" size="100" readonly="readonly" />
									</td>
								</tr>
								<tr style="display: none;">
									<td><span class="required">*</span> <?php echo "Group"; ?></td>
									<td>
										<input type="text" name="group" value="<?php echo $group; ?>" size="100" readonly="readonly" />
									</td>
								</tr>
								<tr style="display: none;">
									<td><span class="required">*</span> <?php echo "Category"; ?></td>
									<td>
										<input type="text" name="category" value="<?php echo $category; ?>" size="100" readonly="readonly" />
									</td>
								</tr>
						</table>
					</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript"><!--
$('input[name=\'designation\']').autocomplete({
		delay: 500,
		source: function(request, response) {
				$.ajax({
						url: 'index.php?route=catalog/designation/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
						dataType: 'json',
						success: function(json) {   
								response($.map(json, function(item) {
										return {
												label: item.d_name,
												value: item.designation_id
										}
								}));
						}
				});
		}, 
		select: function(event, ui) {
				$('input[name=\'designation\']').val(ui.item.label);
				$('input[name=\'designation_id\']').val(ui.item.value);
				return false;
		},
		focus: function(event, ui) {
				return false;
		}
});

$('input[name=\'grade\']').autocomplete({
		delay: 500,
		source: function(request, response) {
				$.ajax({
						url: 'index.php?route=catalog/grade/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
						dataType: 'json',
						success: function(json) {   
								response($.map(json, function(item) {
										return {
												label: item.g_name,
												value: item.grade_id
										}
								}));
						}
				});
		}, 
		select: function(event, ui) {
				$('input[name=\'grade\']').val(ui.item.label);
				$('input[name=\'grade_id\']').val(ui.item.value);
				return false;
		},
		focus: function(event, ui) {
				return false;
		}
});

$('input[name=\'department\']').autocomplete({
		delay: 500,
		source: function(request, response) {
				$.ajax({
						url: 'index.php?route=catalog/department/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
						dataType: 'json',
						success: function(json) {   
								response($.map(json, function(item) {
										return {
												label: item.d_name,
												value: item.department_id
										}
								}));
						}
				});
		}, 
		select: function(event, ui) {
				$('input[name=\'department\']').val(ui.item.label);
				$('input[name=\'department_id\']').val(ui.item.value);
				return false;
		},
		focus: function(event, ui) {
				return false;
		}
});

$('input[name=\'reporting_to_name\']').autocomplete({
		delay: 500,
		source: function(request, response) {
				$.ajax({
						url: 'index.php?route=catalog/employee/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
						dataType: 'json',
						success: function(json) {   
								response($.map(json, function(item) {
										return {
												label: item.name,
												value: item.emp_code
										}
								}));
						}
				});
		}, 
		select: function(event, ui) {
				$('input[name=\'reporting_to_name\']').val(ui.item.label);
				$('input[name=\'reporting_to\']').val(ui.item.value);
				return false;
		},
		focus: function(event, ui) {
				return false;
		}
});
//--></script>
<script type="text/javascript"><!--
$(document).ready(function() {
		$('.date').datepicker({dateFormat: 'yy-mm-dd'});
});
//--></script>
<?php echo $footer; ?>