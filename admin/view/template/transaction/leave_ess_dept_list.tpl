<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/shipping.png" alt="" /> 
        <?php if(isset($this->session->data['is_dept'])){ ?>
          <?php echo 'Dept Leave Approval'; ?>
        <?php } else { ?>
          <?php echo 'Leave Approval'; ?>
        <?php } ?>
      </h1>
      <div class="buttons">
        <?php if($filter_name_id != '' || $filter_name_id_1 != '') { ?>
          <a href="<?php echo $insert; ?>" class="button" style=""><?php echo $button_insert; ?></a>
        <?php } ?>
        <a onclick="approve_fun();" class="button" style="display: none;"><?php echo 'Approve'; ?></a>
        <a onclick="$('#form').submit();" class="button" style="display: none;"><?php echo 'Delete'; ?></a>
        <a style="" onclick="exportss();" class="button" style=""><?php echo 'Export'; ?></a>
      </div>
    </div>
    <?php if(isset($this->session->data['is_dept'])){ ?>
    <div style="margin-left: 40%;">
      <div style="text-align: left;">
        <span style="font-size: 16px;">Department</span> : <b style="font-size: 16px;"><?php echo strtoupper($this->session->data['dept_names']); ?></b>
      </div>
    </div>
    <?php } ?>
    <div class="content">
        <table class="list">
          <thead>
            <tr>
              <td class="right" style="width:135px;"><?php echo 'Action'; ?></td>
              <td width="1" style="display:none;text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
              <td class="left" style="">
                <?php echo 'Emp Name'; ?>
              </td>
              <td class="left" style="display: none;">
                <?php echo 'Emp Id'; ?>
              </td>
              <?php if(isset($this->session->data['is_super']) || $dept_head_count > 1){ ?>
              <td class="left">
                <?php echo 'Department'; ?>
              </td>
              <?php } ?>
              <td class="left" style="display: none;">
                <?php echo 'Group'; ?>
              </td>
              <td class="left">
                <?php echo 'DOI'; ?>
              </td>
              <td class="left" style="width:60px;">
                <?php echo 'Lve Tpe'; ?>
              </td>
              <td>
                <a><?php echo "Days"; ?></a>
              </td>
              <td style="display: none;">
                <a><?php echo "PLE"; ?></a>
              </td>
              <td style="width:65px;">
                <a><?php echo "Leave From"; ?></a>
              </td>
              <td style="width:65px;">
                <a><?php echo "Leave To"; ?></a>
              </td>
              <td>
                <a><?php echo "Leave Reason"; ?></a>
              </td>
              <td style="width: 95px;">
                <a><?php echo "Status"; ?></a>
              </td>
              <td style="display: none;">
                <a><?php echo "Dept Appr by"; ?></a>
              </td>
              <td style="display: none;">
                <a><?php echo "Secy. Appr"; ?></a>
              </td>
              <td style="width: 75px;">
                <a><?php echo "Reject Reason"; ?></a>
              </td>
              <td>
                <a><?php echo "Process"; ?></a>
              </td>
            </tr>
          </thead>
          <tbody>
            <tr class="filter">
              <td align="right">
                <a onclick="filter();" class="button"><?php echo $button_filter; ?></a>
              </td>
              <td style="display: none;">&nbsp;</td>
              <td>
                <input type="text" id="filter_name" name="filter_name" value="<?php echo $filter_name; ?>"  style="width:150px;" />
                <input type="hidden" id="filter_name_id" name="filter_name_id" value="<?php echo $filter_name_id; ?>" />
              </td>
              <td style="display: none;">
                <input type="text" id="filter_name_id_1" name="filter_name_id_1" value="<?php echo $filter_name_id_1; ?>"  style="width:31px;" />
              </td>
              <?php if(isset($this->session->data['is_super']) || $dept_head_count > 1){ ?>
                <td>
                  <select name="filter_dept" style="width:60px;">
                    <?php foreach($dept_data as $key => $ud) { ?>
                      <?php if($key == $filter_dept) { ?>
                        <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                      <?php } else { ?>
                        <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                      <?php } ?>
                    <?php } ?>
                  </select>
                </td>
              <?php } ?>
              <td style="width:7%;display: none;">
                <select name="filter_group" style="width:75px;">
                  <?php foreach($group_data as $key => $gd) { ?>
                    <?php if($key == $filter_group) { ?>
                      <option value='<?php echo $key; ?>' selected="selected"><?php echo $gd; ?></option> 
                    <?php } else { ?>
                      <option value='<?php echo $key; ?>'><?php echo $gd; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
              <td><input type="text" id="filter_date" name="filter_date" value="<?php echo $filter_date; ?>" class="date"  style="width:67px;" /></td>
              <td>
                <select name="filter_leavetype">
                  <?php foreach($leavess as $key => $ud) { ?>
                    <?php if($key == $filter_leavetype) { ?>
                      <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                    <?php } else { ?>
                      <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
              <td>&nbsp;</td>
              <td style="display: none;">&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>
                <select name="filter_approval_1">
                  <?php foreach($approves as $key => $ud) { ?>
                    <?php if($key == $filter_approval_1) { ?>
                      <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                    <?php } else { ?>
                      <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
              <td style="display: none;">
                <select name="filter_approval_1_by" style="width:60px;">
                  <?php foreach($dept_heads as $key => $ud) { ?>
                    <?php if($key == $filter_approval_1_by) { ?>
                      <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                    <?php } else { ?>
                      <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
              <td style="display: none;">
                <select name="filter_approval_2" style="width:65px;">
                  <?php foreach($approves as $key => $ud) { ?>
                    <?php if($key == $filter_approval_2) { ?>
                      <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                    <?php } else { ?>
                      <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
              <td>&nbsp;</td>
              <td>
                <select name="filter_proc" style="width:60px;">
                  <?php foreach($process as $key => $ud) { ?>
                    <?php if($key == $filter_proc) { ?>
                      <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                    <?php } else { ?>
                      <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
            </tr>
            <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
              <?php if ($leaves) { ?>
              <?php foreach ($leaves as $employee) { ?>
              <tr>
                <td class="right">
                  <?php foreach ($employee['action'] as $action) { ?>
                    <?php if($action['href'] == '') { ?>
                      <p style="cursor:default;"><?php echo $action['text']; ?></p>
                    <?php } else { ?>
                      [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                    <?php } ?>
                  <?php } ?>
                </td>
                <td style="display:none;text-align: center;"><?php if ($employee['selected']) { ?>
                  <input type="checkbox" name="selected[]" value="<?php echo $employee['batch_id']; ?>" checked="checked" />
                  <?php } else { ?>
                  <input type="checkbox" name="selected[]" value="<?php echo $employee['batch_id']; ?>" />
                  <?php } ?></td>
                <td class="left"><?php echo $employee['name']; ?></td>
                <td class="left" style="display: none;"><?php echo $employee['emp_id']; ?></td>
                <?php if(isset($this->session->data['is_super']) || $dept_head_count > 1){ ?>
                  <td class="left"><?php echo $employee['dept']; ?></td>
                <?php } ?>
                <td class="left" style="display: none;"><?php echo $employee['group']; ?></td>
                <td class="left"><?php echo $employee['dot']; ?></td>
                <td class="left"><?php echo $employee['leave_type']; ?></td>
                <td class="left"><?php echo $employee['total_leave_days']; ?></td>
                <td class="left" style="display: none;"><?php echo $employee['encash']; ?></td>
                <td class="left"><?php echo $employee['leave_from']; ?></td>
                <td class="left"><?php echo $employee['leave_to']; ?></td>
                <td class="left"><?php echo $employee['leave_reason']; ?></td>
                <td class="left"><?php echo $employee['approval_1']; ?></td>
                <td class="left" style="display: none;"><?php echo $employee['approval_1_by']; ?></td>
                <td class="left" style="display: none;"><?php echo $employee['approval_2']; ?></td>
                <td class="left"><?php echo $employee['reject_reason']; ?></td>
                <td class="left"><?php echo $employee['proc_stat']; ?></td>
              </tr>
              <?php } ?>
              <?php } else { ?>
              <tr>
                <td class="center" colspan="18"><?php echo $text_no_results; ?></td>
              </tr>
              <?php } ?>
            </form>
          </tbody>
        </table>
      
    </div>
  </div>
</div>
<script type="text/javascript"><!--
function filter() {
  url = 'index.php?route=transaction/leave_ess_dept&token=<?php echo $token; ?>';

  var filter_name = $('input[name=\'filter_name\']').attr('value');
  var filter_name_id = $('input[name=\'filter_name_id\']').attr('value');
  if (filter_name) {
    url += '&filter_name=' + encodeURIComponent(filter_name);
    url += '&filter_name_id=' + encodeURIComponent(filter_name_id);
  }

  var filter_name_id_1 = $('input[name=\'filter_name_id_1\']').attr('value');
  if(filter_name_id_1){
    url += '&filter_name_id_1=' + encodeURIComponent(filter_name_id_1);
  }

  var filter_dept = $('select[name=\'filter_dept\']').attr('value');
  if (filter_dept) {
    url += '&filter_dept=' + encodeURIComponent(filter_dept);
  }

  var filter_unit = $('select[name=\'filter_unit\']').attr('value');
  if (filter_unit) {
    url += '&filter_unit=' + encodeURIComponent(filter_unit);
  }

  var filter_group = $('select[name=\'filter_group\']').attr('value');
  if (filter_group) {
    url += '&filter_group=' + encodeURIComponent(filter_group);
  }
  
  var filter_date = $('input[name=\'filter_date\']').attr('value');
  if (filter_date) {
    url += '&filter_date=' + encodeURIComponent(filter_date);
  }

  var filter_approval_1 = $('select[name=\'filter_approval_1\']').attr('value');
  if (filter_approval_1) {
    url += '&filter_approval_1=' + encodeURIComponent(filter_approval_1);
  }

  var filter_approval_1_by = $('select[name=\'filter_approval_1_by\']').attr('value');
  if (filter_approval_1_by) {
    url += '&filter_approval_1_by=' + encodeURIComponent(filter_approval_1_by);
  }

  var filter_approval_2 = $('select[name=\'filter_approval_2\']').attr('value');
  if (filter_approval_2) {
    url += '&filter_approval_2=' + encodeURIComponent(filter_approval_2);
  }

  var filter_proc = $('select[name=\'filter_proc\']').attr('value');
  if (filter_proc) {
    url += '&filter_proc=' + encodeURIComponent(filter_proc);
  }

  var filter_leavetype = $('select[name=\'filter_leavetype\']').attr('value');
  if (filter_leavetype) {
    url += '&filter_leavetype=' + encodeURIComponent(filter_leavetype);
  }
  
  location = url;
  return false;
}

function exportss() {
  url = 'index.php?route=transaction/leave_ess_dept/export&token=<?php echo $token; ?>';

  var filter_name = $('input[name=\'filter_name\']').attr('value');
  var filter_name_id = $('input[name=\'filter_name_id\']').attr('value');
  if (filter_name) {
    url += '&filter_name=' + encodeURIComponent(filter_name);
    url += '&filter_name_id=' + encodeURIComponent(filter_name_id);
  }

  var filter_name_id_1 = $('input[name=\'filter_name_id_1\']').attr('value');
  if(filter_name_id_1){
    url += '&filter_name_id_1=' + encodeURIComponent(filter_name_id_1);
  }

  var filter_dept = $('select[name=\'filter_dept\']').attr('value');
  if (filter_dept) {
    url += '&filter_dept=' + encodeURIComponent(filter_dept);
  }

  var filter_unit = $('select[name=\'filter_unit\']').attr('value');
  if (filter_unit) {
    url += '&filter_unit=' + encodeURIComponent(filter_unit);
  }

  var filter_group = $('select[name=\'filter_group\']').attr('value');
  if (filter_group) {
    url += '&filter_group=' + encodeURIComponent(filter_group);
  }
  
  var filter_date = $('input[name=\'filter_date\']').attr('value');
  if (filter_date) {
    url += '&filter_date=' + encodeURIComponent(filter_date);
  }

  var filter_approval_1 = $('select[name=\'filter_approval_1\']').attr('value');
  if (filter_approval_1) {
    url += '&filter_approval_1=' + encodeURIComponent(filter_approval_1);
  }

  var filter_approval_1_by = $('select[name=\'filter_approval_1_by\']').attr('value');
  if (filter_approval_1_by) {
    url += '&filter_approval_1_by=' + encodeURIComponent(filter_approval_1_by);
  }

  var filter_approval_2 = $('select[name=\'filter_approval_2\']').attr('value');
  if (filter_approval_2) {
    url += '&filter_approval_2=' + encodeURIComponent(filter_approval_2);
  }

  var filter_proc = $('select[name=\'filter_proc\']').attr('value');
  if (filter_proc) {
    url += '&filter_proc=' + encodeURIComponent(filter_proc);
  }

  var filter_leavetype = $('select[name=\'filter_leavetype\']').attr('value');
  if (filter_leavetype) {
    url += '&filter_leavetype=' + encodeURIComponent(filter_leavetype);
  }
  
  location = url;
  return false;
}
//--></script>
<script type="text/javascript"><!--
$('#filter_name, #filter_date').keydown(function(e) {
  if (e.keyCode == 13) {
    filter();
  }
});

$.widget('custom.catcomplete', $.ui.autocomplete, {
  _renderMenu: function(ul, items) {
    var self = this, currentCategory = '';
    $.each(items, function(index, item) {
      if (item.category != currentCategory) {
        //ul.append('<li class="ui-autocomplete-category">' + item.category + '</li>');
        currentCategory = item.category;
      }
      self._renderItem(ul, item);
    });
  }
});
//--></script> 
<script type="text/javascript"><!--
$('input[name=\'filter_name\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=transaction/leave_ess_dept/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.name,
            value: item.employee_id,
            emp_code: item.emp_code
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_name\']').val(ui.item.label);
    $('input[name=\'filter_name_id\']').val(ui.item.emp_code);
            
    return false;
  },
  focus: function(event, ui) {
        return false;
    }
});

function approve_fun(){
  approve_var = '<?php echo $approve; ?>';
  approve_var = approve_var.replace('&amp;', '&');
  approve_var = approve_var.replace('&amp;', '&');
  approve_var = approve_var.replace('&amp;', '&');
  approve_var = approve_var.replace('&amp;', '&');
  approve_var = approve_var.replace('&amp;', '&');
  approve_var = approve_var.replace('&amp;', '&');
  approve_var = approve_var.replace('&amp;', '&');
  approve_var = approve_var.replace('&amp;', '&');
  approve_var = approve_var.replace('&amp;', '&');
  $('#form').attr('action', approve_var);
  $('#form').submit();
}
//--></script>
<script type="text/javascript" src="view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript"><!--
$('.date').datepicker({
    dateFormat: 'yy-mm-dd'
});
//--></script>
<?php echo $footer; ?>