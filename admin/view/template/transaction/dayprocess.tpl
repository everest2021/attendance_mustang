<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/report.png" alt="" /> <?php echo $heading_title; ?></h1>
    </div>
    <div class="content sales-report">
      <table class="form">
        <tr>
          <td style="width:10%;"><?php echo "Date Of Processing"; ?>
            <input readonly type="text" name="filter_date" value="<?php echo $filter_date; ?>" id="date" size="9" />
          </td>
          
          <td style="width:10%">Location
            <select name="unit">
              <?php foreach($unit_data as $key => $ud) { ?>
                <?php if($key === $unit) { ?>
                  <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                <?php } else { ?>
                  <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </td>
          
          <td style="text-align: right;">
            <a style="padding: 13px 25px;" onclick="filter();" id="filter" class="button"><?php echo $button_filter; ?></a>
            <?php 
              if($exceptional || $count_mismatch == '1') {
            ?>
              <a style="padding: 13px 25px;" onclick="filter_export();" id="filter" class="button"><?php echo 'Export'; ?></a>
              <a style="padding: 13px 25px;" onclick="close_day();" id="closeday" class="button"><?php echo 'Close Day'; ?></a>
            <?php 
              } else { 
            ?>
              <a style="padding: 13px 25px;" onclick="close_day();" id="closeday" class="button"><?php echo 'Close Day'; ?></a>
            <?php 
              }
            ?>
          </td>
        </tr>
      </table>
      <table class="list">
        <tbody>
          
          <?php 
            if($filter_date == 0) {
          ?>
            <tr>
              <td class="center"><?php echo "No data to process"; ?></td>
            </tr>
          <?php 
            } else {
          ?>
            <?php 
              if($exceptional || $count_mismatch == '1') {
            ?>
              <tr>
                  <td colspan="5">
                      Before closing the day you will need to manually settle the abnormal entries and Match the Records in Transaction and Master
                  </td>
              </tr>
              <tr>
                <td>
                    Sr No
                </td>
                <td>
                    Name
                </td>
                <td>
                    In time
                </td>
                <td>
                    Out time
                </td>
                <td>
                    Action
                </td>
              </tr>
              <?php 
                $i = 1;
                foreach($exceptional as $data) {
              ?>
                <tr>
                  <td>
                    <?php echo $data['emp_id']; ?>
                  </td>
                  <td>
                    <?php echo $data['emp_name']; ?>
                  </td>
                  <td>
                    <?php echo $data['act_intime']; ?>
                  </td>
                  <td>
                    <?php echo $data['act_outtime']; ?>
                  </td>
                  <td>
                    <a href="<?php echo $data['man_href']; ?>">Manual Punch</a>
                  </td> 
                </tr>
              <?php
                }
              ?>
            <?php 
              } else {
            ?>
              <tr>
                <td>
                  Now you can press close day button
                </td>
              </tr>
            <?php 
              }
            ?>
          <?php } ?>
        </tbody>
      </table>
      
    </div>
  </div>
</div>
<script type="text/javascript"><!--
function filter() {
  url = 'index.php?route=transaction/dayprocess&token=<?php echo $token; ?>';
  
  var unit = $('select[name=\'unit\']').attr('value');
  
  if (unit) {
    url += '&unit=' + encodeURIComponent(unit);
  }
  
  location = url;
  return false;
}

function filter_export() {
  url = 'index.php?route=transaction/dayprocess/export&token=<?php echo $token; ?>';
  
  var unit = $('select[name=\'unit\']').attr('value');
  
  if (unit) {
    url += '&unit=' + encodeURIComponent(unit);
  }

  location = url;
  return false;
}

function close_day() {
  url = 'index.php?route=transaction/dayprocess/close_day&token=<?php echo $token; ?>';
  
  var unit = $('select[name=\'unit\']').attr('value');
  
  if (unit) {
    url += '&unit=' + encodeURIComponent(unit);
  }

  location = url;
  return false;
}

//--></script> 
<?php echo $footer; ?>