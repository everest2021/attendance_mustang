<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
</head>
<body>
<div style="page-break-after: always;">
  <h1 style="text-align:center;">
    <?php echo $title; ?><br />
    <p style="display:inline;font-size:15px;"><?php echo 'Date : ' . date('Y-m-d H:i:s'); ?></p>
  </h1>
  <table class="product" style="width:100% !important;">
    <tbody>
      <tr>
        <td>
          Emp Name
        </td>
        <td>
          Emp Id 
        </td>
        <td>
          Loc
        </td>
        <td>
          Dte of Input
        </td>
        <td>
          Lve type
        </td>
        <td>
          Days
        </td>
        <td>
          Encash
        </td>
        <td>
          From
        </td>
        <td>
          To
        </td>
        <td>
          Stat
        </td>
        <td>
          Proc
        </td>
      </tr>
      <?php if($final_datas) { ?>
        <?php foreach($final_datas as $final_data) { ?>
          <tr style="font-weight:bold;font-size:11px;">
            <td>
              <?php echo $final_data['name']; ?>
            </td>
            <td>
              <?php echo $final_data['emp_id']; ?>
            </td>
            <td>
              <?php echo $final_data['unit']; ?>
            </td>
            <td>
              <?php echo $final_data['dot']; ?>
            </td>
            <td>
              <?php echo $final_data['leave_type']; ?>
            </td>
            <td>
              <?php echo $final_data['total_leave_days']; ?>
            </td>
            <td>
              <?php echo $final_data['encash']; ?>
            </td>
            <td>
              <?php echo $final_data['leave_from']; ?>
            </td>
            <td>
              <?php echo $final_data['leave_to']; ?>
            </td>
            <td>
              <?php echo $final_data['status']; ?>
            </td>
            <td>
              <?php echo $final_data['p_status']; ?>
            </td>
          </tr>
        <?php } ?>
      <?php } ?>
    </tbody>
  </table>
</div>
</body>
</html>