<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/shipping.png" alt="" /> <?php echo 'Self Leave Entry'; ?></h1>
      <div class="buttons">
        <?php if($filter_name_id != '' || $filter_name_id_1 != '') { ?>
          <a href="<?php echo $insert; ?>" class="button" style=""><?php echo $button_insert; ?></a>
        <?php } ?>
        <a onclick="$('#form').submit();" class="button" style=""><?php echo 'Delete'; ?></a>
        <a style="display: none;" href="<?php echo $export; ?>" class="button" style=""><?php echo 'Export'; ?></a>
      </div>
    </div>
    <div style="margin-left: 10%;">
      <div style="text-align: left;">
        <span style="font-size: 16px;">Name</span> : <b style="font-size: 16px;"><?php echo $filter_name; ?></b> &nbsp;&nbsp; | &nbsp;&nbsp;
        <span style="font-size: 16px;">Id</span> : <b style="font-size: 16px;"><?php echo $filter_name_id; ?></b> &nbsp;&nbsp; | &nbsp;&nbsp;
        <span style="font-size: 16px;">Department</span> : <b style="font-size: 16px;"><?php echo $filter_dept; ?></b> &nbsp;&nbsp; | &nbsp;&nbsp;
        <span style="font-size: 16px;">Unit</span> : <b style="font-size: 16px;"><?php echo $filter_unit; ?></b>
      </div>
    </div>
    <div class="content">
        <table class="list">
          <thead>
            <tr>
              <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
              <td class="left" style="width:80px;">
                <?php echo 'DOI'; ?>
              </td>
              <td class="left" style="width: 105px;">
                <?php echo 'Lve Tpe'; ?>
              </td>
              <td>
                <a><?php echo "Days"; ?></a>
              </td>
              <td style="display:none;">
                <a><?php echo "PLE"; ?></a>
              </td>
              <td style="width:65px;">
                <a><?php echo "Leave From"; ?></a>
              </td>
              <td style="width:65px;">
                <a><?php echo "Leave To"; ?></a>
              </td>
              <td>
                <a><?php echo "Leave Reason"; ?></a>
              </td>
              <td style="width:70px;">
                <a><?php echo "Approval Status"; ?></a>
              </td>
              <td style="width:105px; display:none;">
                <a><?php echo "Approved By"; ?></a>
              </td>
              <td style="width: 155px;display:none;">
                <a><?php echo "Secy. Appr"; ?></a>
              </td>
              <td>
                <a><?php echo "Reject Reason"; ?></a>
              </td>
              <td>
                <a><?php echo "Process"; ?></a>
              </td>
              <td class="right"><?php echo 'Action'; ?></td>
            </tr>
          </thead>
          <tbody>
            <tr class="filter">
              <td>&nbsp;</td>
              <td><input type="text" id="filter_date" name="filter_date" value="<?php echo $filter_date; ?>" class="date"  style="width:67px;" /></td>
              <td>
                <select name="filter_leavetype">
                  <?php foreach($leavess as $key => $ud) { ?>
                    <?php if($key == $filter_leavetype) { ?>
                      <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                    <?php } else { ?>
                      <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
              <td>&nbsp;</td>
              <td style="display:none;">&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>
                <select name="filter_approval_1">
                  <?php foreach($approves as $key => $ud) { ?>
                    <?php if($key == $filter_approval_1) { ?>
                      <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                    <?php } else { ?>
                      <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
              <td style="display:none;">
                <select name="filter_approval_1_by" style="width:100px;">
                  <?php foreach($dept_heads as $key => $ud) { ?>
                    <?php if($key == $filter_approval_1_by) { ?>
                      <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                    <?php } else { ?>
                      <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
              <td style="display:none;">
                <select name="filter_approval_2">
                  <?php foreach($approves as $key => $ud) { ?>
                    <?php if($key == $filter_approval_2) { ?>
                      <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                    <?php } else { ?>
                      <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
              <td>&nbsp;</td>
              <td>
                <select name="filter_proc">
                  <?php foreach($process as $key => $ud) { ?>
                    <?php if($key == $filter_proc) { ?>
                      <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                    <?php } else { ?>
                      <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
              <td align="right">
                <a onclick="filter();" class="button"><?php echo $button_filter; ?></a>
              </td>
            </tr>
            <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
              <?php if ($leaves) { ?>
              <?php foreach ($leaves as $employee) { ?>
              <tr>
                <td style="text-align: center;"><?php if ($employee['selected']) { ?>
                  <input type="checkbox" name="selected[]" value="<?php echo $employee['batch_id']; ?>" checked="checked" />
                  <?php } else { ?>
                  <input type="checkbox" name="selected[]" value="<?php echo $employee['batch_id']; ?>" />
                  <?php } ?></td>
                <td class="left"><?php echo $employee['dot']; ?></td>
                <td class="left"><?php echo $employee['leave_type']; ?></td>
                <td class="left"><?php echo $employee['total_leave_days']; ?></td>
                <td class="left" style="display:none;"><?php echo $employee['encash']; ?></td>
                <td class="left"><?php echo $employee['leave_from']; ?></td>
                <td class="left"><?php echo $employee['leave_to']; ?></td>
                <td class="left"><?php echo $employee['leave_reason']; ?></td>
                <td class="left"><?php echo $employee['approval_1']; ?></td>
                <td class="left" style="display:none;"><?php echo $employee['approval_1_by']; ?></td>
                <td class="left" style="display:none;"><?php echo $employee['approval_2']; ?></td>
                <td class="left"><?php echo $employee['reject_reason']; ?></td>
                <td class="left"><?php echo $employee['proc_stat']; ?></td>
                <td class="right"><?php foreach ($employee['action'] as $action) { ?>
                  <?php if($action['href'] == '') { ?>
                    <p style="cursor:default;"><?php echo $action['text']; ?></p>
                  <?php } else { ?>
                    [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                  <?php } ?>
                  <?php } ?></td>
              </tr>
              <?php } ?>
              <?php } else { ?>
              <tr>
                <td class="center" colspan="15"><?php echo $text_no_results; ?></td>
              </tr>
              <?php } ?>
            </form>
          </tbody>
        </table>
      
    </div>
  </div>
</div>
<script type="text/javascript"><!--
function filter() {
  url = 'index.php?route=transaction/leave_ess&token=<?php echo $token; ?>';
  
  var filter_date = $('input[name=\'filter_date\']').attr('value');
  if (filter_date) {
    url += '&filter_date=' + encodeURIComponent(filter_date);
  }

  var filter_approval_1 = $('select[name=\'filter_approval_1\']').attr('value');
  if (filter_approval_1) {
    url += '&filter_approval_1=' + encodeURIComponent(filter_approval_1);
  }

  var filter_approval_1_by = $('select[name=\'filter_approval_1_by\']').attr('value');
  if (filter_approval_1_by) {
    url += '&filter_approval_1_by=' + encodeURIComponent(filter_approval_1_by);
  }

  var filter_approval_2 = $('select[name=\'filter_approval_2\']').attr('value');
  if (filter_approval_2) {
    url += '&filter_approval_2=' + encodeURIComponent(filter_approval_2);
  }

  var filter_proc = $('select[name=\'filter_proc\']').attr('value');
  if (filter_proc) {
    url += '&filter_proc=' + encodeURIComponent(filter_proc);
  }

  var filter_leavetype = $('select[name=\'filter_leavetype\']').attr('value');
  if (filter_leavetype) {
    url += '&filter_leavetype=' + encodeURIComponent(filter_leavetype);
  }
  
  location = url;
  return false;
}
//--></script>
<script type="text/javascript"><!--
$('#filter_name, #filter_date').keydown(function(e) {
  if (e.keyCode == 13) {
    filter();
  }
});

$.widget('custom.catcomplete', $.ui.autocomplete, {
  _renderMenu: function(ul, items) {
    var self = this, currentCategory = '';
    $.each(items, function(index, item) {
      if (item.category != currentCategory) {
        //ul.append('<li class="ui-autocomplete-category">' + item.category + '</li>');
        currentCategory = item.category;
      }
      self._renderItem(ul, item);
    });
  }
});
//--></script> 
<script type="text/javascript"><!--
$('input[name=\'filter_name\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=transaction/leave/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.name,
            value: item.employee_id,
            emp_code: item.emp_code
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_name\']').val(ui.item.label);
    $('input[name=\'filter_name_id\']').val(ui.item.emp_code);
            
    return false;
  },
  focus: function(event, ui) {
        return false;
    }
});
//--></script>
<script type="text/javascript" src="view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript"><!--
$('.date').datepicker({
    dateFormat: 'yy-mm-dd'
});
//--></script>
<?php echo $footer; ?>