<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/shipping.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons">
        <?php if(($filter_name_id != '' || $filter_name_id_1 != '') && $request_list == '') { ?>
          <a href="<?php echo $insert; ?>" class="button" style=""><?php echo $button_insert; ?></a>
        <?php } ?>
        <a style="display: none;" onclick="$('#form').submit();" class="button" style=""><?php echo 'Cancel'; ?></a>
        <a href="<?php echo $export; ?>" class="button" style=""><?php echo 'Export'; ?></a>
        <?php if($request_list != ''){ ?>
          <a href="<?php echo $request_list; ?>">Back to Request List</a>
        <?php } ?>
      </div>
    </div>
    <div class="content">
        <table class="list">
          <thead>
            <tr>
              <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
              <td class="left" style="width:1%;">
                <?php echo 'Emp Name'; ?>
              </td>
              <td class="left">
                <?php echo 'Emp Id'; ?>
              </td>
              <td class="left">
                <?php echo 'Loc'; ?>
              </td>
              <td class="left">
                <?php echo 'Dte of input'; ?>
              </td>
              <td class="left">
                <?php echo 'Lve Tpe'; ?>
              </td>
              <td>
                <a><?php echo "Days"; ?></a>
              </td>
              <td>
                <a><?php echo "Encash"; ?></a>
              </td>
              <td>
                <a><?php echo "From"; ?></a>
              </td>
              <td>
                <a><?php echo "To"; ?></a>
              </td>
              <td>
                <a><?php echo "Stat"; ?></a>
              </td>
              <td>
                <a><?php echo "Proc"; ?></a>
              </td>
              <?php /*<td class="right"><?php echo 'Action'; ?></td> */ ?>
            </tr>
          </thead>
          <tbody>
            <tr class="filter">
              <td>&nbsp;</td>
              <td>
                <input type="text" id="filter_name" name="filter_name" value="<?php echo $filter_name; ?>"  style="width:150px;" />
                <input type="hidden" id="filter_name_id" name="filter_name_id" value="<?php echo $filter_name_id; ?>" />
              </td>
              <td>
                <input type="text" id="filter_name_id_1" name="filter_name_id_1" value="<?php echo $filter_name_id_1; ?>"  style="width:31px;" />
              </td>
              <td>
                <select name="filter_unit">
                  <?php foreach($unit_data as $key => $ud) { ?>
                    <?php if($key === $filter_unit) { ?>
                      <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                    <?php } else { ?>
                      <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
              <td><input type="text" id="filter_date" name="filter_date" value="<?php echo $filter_date; ?>" class="date"  style="width:67px;" /></td>
              <td></td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <?php /* <td>&nbsp;</td> */ ?>
              <td align="left">
                <a onclick="filter();" class="button"><b><?php echo $button_filter; ?></b></a>
              </td>
            </tr>
            <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
              <?php if ($leaves) { ?>
              <?php foreach ($leaves as $employee) { ?>
              <tr>
                <td style="text-align: center;"><?php if ($employee['selected']) { ?>
                  <input type="checkbox" name="selected[]" value="<?php echo $employee['batch_id']; ?>" checked="checked" />
                  <?php } else { ?>
                  <input type="checkbox" name="selected[]" value="<?php echo $employee['batch_id']; ?>" />
                  <?php } ?></td>
                <td class="left"><?php echo $employee['name']; ?></td>
                <td class="left"><?php echo $employee['emp_id']; ?></td>
                <td class="left"><?php echo $employee['unit']; ?></td>
                <td class="left"><?php echo $employee['dot']; ?></td>
                <td class="left"><?php echo $employee['leave_type']; ?></td>
                <td class="left"><?php echo $employee['total_leave_days']; ?></td>
                <td class="left"><?php echo $employee['encash']; ?></td>
                <td class="left"><?php echo $employee['leave_from']; ?></td>
                <td class="left"><?php echo $employee['leave_to']; ?></td>
                <td class="left"><?php echo $employee['status']; ?></td>
                <td class="left"><?php echo $employee['p_status']; ?></td>
                <?php /*
                <td class="right"><?php foreach ($employee['action'] as $action) { ?>
                  <?php if($action['href'] == '') { ?>
                    <p style="cursor:default;"><?php echo $action['text']; ?></p>
                  <?php } else { ?>
                    [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                  <?php } ?>
                  <?php } ?></td> */ ?>
              </tr>
              <?php } ?>
              <?php } else { ?>
              <tr>
                <td class="center" colspan="12"><?php echo $text_no_results; ?></td>
              </tr>
              <?php } ?>
            </form>
          </tbody>
        </table>
      <div class="pagination"><?php echo $pagination; ?></div>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
function filter() {
  url = 'index.php?route=transaction/leave&token=<?php echo $token; ?>';
  
  var filter_name = $('input[name=\'filter_name\']').attr('value');
  var filter_name_id = $('input[name=\'filter_name_id\']').attr('value');
  if (filter_name) {
    url += '&filter_name=' + encodeURIComponent(filter_name);
    url += '&filter_name_id=' + encodeURIComponent(filter_name_id);
  }

  var filter_name_id_1 = $('input[name=\'filter_name_id_1\']').attr('value');
  if(filter_name_id_1){
    url += '&filter_name_id_1=' + encodeURIComponent(filter_name_id_1);
  }

  var filter_date = $('input[name=\'filter_date\']').attr('value');
  if (filter_date) {
    url += '&filter_date=' + encodeURIComponent(filter_date);
  }

  var filter_unit = $('select[name=\'filter_unit\']').attr('value');
  if (filter_unit) {
    url += '&filter_unit=' + encodeURIComponent(filter_unit);
  }

  location = url;
  return false;
}
//--></script>
<script type="text/javascript"><!--
$('#filter_name, #filter_date').keydown(function(e) {
  if (e.keyCode == 13) {
    filter();
  }
});

$.widget('custom.catcomplete', $.ui.autocomplete, {
  _renderMenu: function(ul, items) {
    var self = this, currentCategory = '';
    $.each(items, function(index, item) {
      if (item.category != currentCategory) {
        //ul.append('<li class="ui-autocomplete-category">' + item.category + '</li>');
        currentCategory = item.category;
      }
      self._renderItem(ul, item);
    });
  }
});
//--></script> 
<script type="text/javascript"><!--
$('input[name=\'filter_name\']').autocomplete({
  delay: 500,
  source: function(request, response) {
    $.ajax({
      url: 'index.php?route=catalog/employee/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
      dataType: 'json',
      success: function(json) {   
        response($.map(json, function(item) {
          return {
            label: item.name,
            value: item.employee_id,
            emp_code: item.emp_code
          }
        }));
      }
    });
  }, 
  select: function(event, ui) {
    $('input[name=\'filter_name\']').val(ui.item.label);
    $('input[name=\'filter_name_id\']').val(ui.item.emp_code);
            
    return false;
  },
  focus: function(event, ui) {
        return false;
    }
});
//--></script>
<script type="text/javascript" src="view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript"><!--
$('.date').datepicker({
    dateFormat: 'yy-mm-dd'
});
//--></script>
<?php echo $footer; ?>