<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/report.png" alt="" /> <?php echo $heading_title; ?></h1>
    </div>
    <div class="content sales-report">
      <table class="form">
        <tr>
          
          <td style="width:10%">Location
            <select name="unit">
              <?php foreach($unit_data as $key => $ud) { ?>
                <?php if($key === $unit) { ?>
                  <option value='<?php echo $key; ?>' selected="selected"><?php echo $ud; ?></option> 
                <?php } else { ?>
                  <option value='<?php echo $key; ?>'><?php echo $ud; ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </td>
          
          <td style="text-align: right;">
            <a style="padding: 13px 25px;" onclick="filter();" id="filter" class="button"><?php echo $button_filter; ?></a>
            <a style="padding: 13px 25px;" onclick="close_day();" id="closeday" class="button"><?php echo 'Leave Process'; ?></a>
            
          </td>
        </tr>
      </table>
      <table class="list">
        <tbody>
          
          <?php 
            if($unprocessed) {
          ?>
            <tr>
              <td>
                  Sr No
              </td>
              <td>
                  Date
              </td>
              <td>
                  Emp Name
              </td>
              <td>
                  Leave type
              </td>
              <td>
                  Action
              </td>
            </tr>
            <?php 
              $i = 1;
              foreach($unprocessed as $data) {
            ?>
              <tr>
                <td>
                  <?php echo $i; ?>
                </td>
                <td>
                  <?php echo $data['date']; ?>
                </td>
                <td>
                  <?php echo $data['ename']; ?>
                </td>
                <td>
                  <?php echo $data['leave_type']; ?>
                </td>
                <td class="right">
                  <?php foreach ($data['action'] as $action) { ?>
                    <?php if($action['href'] == '') { ?>
                      <p style="display:inline;cursor:default;"><?php echo $action['text']; ?></p>
                    <?php } else { ?>
                      [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                    <?php } ?>
                  <?php } ?>
                </td>
              </tr>
            <?php
            $i++;
              }
            ?>
          <?php 
            } else {
          ?>
            No data to process      
          <?php 
            }
          ?>
        
        </tbody>
      </table>
      
    </div>
  </div>
</div>
<script type="text/javascript"><!--
function filter() {
  url = 'index.php?route=transaction/leaveprocess&token=<?php echo $token; ?>';
  
  var unit = $('select[name=\'unit\']').attr('value');
  
  if (unit) {
    url += '&unit=' + encodeURIComponent(unit);
  }
  
  location = url;
  return false;
}

function close_day() {
  url = 'index.php?route=transaction/leaveprocess/close_day&token=<?php echo $token; ?>';
  
  var unit = $('select[name=\'unit\']').attr('value');
  
  if (unit) {
    url += '&unit=' + encodeURIComponent(unit);
  }

  location = url;
  return false;
}

//--></script> 
<?php echo $footer; ?>