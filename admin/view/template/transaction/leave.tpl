<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div style="display:none;" class="ajax_warning warning">Month Already Closed</div>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/shipping.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons">
        <?php if($show == '1'){ ?>
          <a onclick="$('#form').submit();" id="save" class="button"><?php echo $button_save; ?></a>
        <?php } else { ?>
          <a href="<?php echo $action_1; ?>" class="button"><?php echo 'Approved'; ?></a>
          <a onclick="$('#form').submit();" id="save" class="button"><?php echo 'Reject'; ?></a>
        <?php } ?>
        <a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a>
      </div>
    </div>
    </div>
    <div class="content">
      <div id="tabs" class="htabs"><a href="#tab-general"><?php echo $tab_general; ?></a></div>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <div id="tab-general">
          <table class="form">
            <tr>
              <td><span class="required">*</span> <?php echo 'Employee Name'; ?></td>
              <td colspan="2">
                <input readonly="readonly" type="text" name="e_name" id="e_name" value="<?php echo $e_name; ?>" size="100" />
                <input type="hidden" name="e_name_id" id="e_name_id" value="<?php echo $e_name_id; ?>" size="100" />
                <input type="hidden" name="insert" id="insert" value="<?php echo $insert; ?>" />
                <input type="hidden" name="batch_id" id="batch_id" value="<?php echo $batch_id; ?>" />
                <input type="hidden" name="diffDays" id="diffDays" value="<?php echo $diffDays; ?>" />
                <input type="hidden" name="multi_day" id="multi_day" value="<?php echo $multi_day; ?>" />
                <input type="hidden" name="group" id="group" value="<?php echo $group; ?>" />
                <input type="hidden" name="show" id="show" value="<?php echo $show; ?>" />
              </td>
            </tr>
            <tr>
              <td><span class="required">*</span> <?php echo 'Emp Code'; ?></td>
              <td colspan="2">
                <input readonly="readonly" type="text" name="emp_code" id="emp_code" value="<?php echo $emp_code; ?>" size="10" />
              </td>
            </tr>
            <tr>
              <td><span class="required">*</span> <?php echo 'Leave Type'; ?></td>
              <td>
                <?php if($show == '1'){ ?>
                  <select name="leave_type" id="leave_type">
                    <?php foreach($leaves as $lkey => $lvalue) { ?>
                      <?php if($lvalue['leave_id'] === $leave_type){ ?>
                        <option value="<?php echo $lvalue['leave_id']; ?>" selected="selected"><?php echo $lvalue['leave_name']; ?></option>
                      <?php } else { ?>
                        <option value="<?php echo $lvalue['leave_id']; ?>"><?php echo $lvalue['leave_name']; ?></option>
                      <?php } ?>
                    <?php } ?>
                  </select>
                <?php } else { ?>
                  <select disabled="disabled" name="leave_type" id="leave_type">
                  <?php foreach($leaves as $lkey => $lvalue) { ?>
                    <?php if($lvalue['leave_id'] === $leave_type){ ?>
                      <option value="<?php echo $lvalue['leave_id']; ?>" selected="selected"><?php echo $lvalue['leave_name']; ?></option>
                    <?php } else { ?>
                      <option value="<?php echo $lvalue['leave_id']; ?>"><?php echo $lvalue['leave_name']; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
                <?php } ?>
                <?php if ($error_leave_type) { ?>
                <span class="error"><?php echo $error_leave_type; ?></span>
                <?php } ?>
              </td>
              <td class="right" style="float:right;margin-right:55px;">
                <table border="1">
                  <tr>
                    <td colspan="4" style="font-weight:bold;text-align:center;">
                        Leave Status
                    </td>
                  </tr>
                  <tr>
                    <td style="padding:10px;text-align:left;">
                    </td>
                    <td style="padding:10px;text-align:left;">
                      PL
                    </td>
                    <td style="padding:10px;text-align:left;">
                      CL
                    </td>
                    <td style="padding:10px;text-align:left;">
                      SL
                    </td>
                  </tf>
                  <tr>
                    <td style="padding-right:40px;">
                      Balance Leave
                    </td>
                    <td style="text-align:center;">
                      <?php echo $total_bal_pl; ?>
                    </td>
                    <td style="text-align:center;">
                      <?php echo $total_bal_cl; ?>
                    </td>
                    <td style="text-align:center;">
                      <?php echo $total_bal_sl; ?>
                    </td>
                  </tr>
                  <tr>
                    <td style="padding-right:40px;">
                      Unprocessed Leave
                    </td>
                    <td style="text-align:center;">
                      <?php echo $total_bal_pl_p; ?>
                    </td>
                    <td style="text-align:center;">
                      <?php echo $total_bal_cl_p; ?>
                    </td>
                    <td style="text-align:center;">
                      <?php echo $total_bal_sl_p; ?>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr id="date_cof" style="display:none;">
              <td><span class="required">*</span> <?php echo 'COF Date'; ?></td>
              <td>
                <?php if($show == '1'){ ?>
                  <input class="date" type="text" name="date_cof" id="date_coff" value="<?php echo $date_cof ?>" size="10" />
                <?php } else { ?>
                  <input readonly="readonly" type="text" name="date_cof" id="date_coff" value="<?php echo $date_cof ?>" size="10" />
                <?php } ?>
              </td>
              <td id="cof_shift_sch" class="left" style="float:left;border-top:1px solid #EEEEEE;">
              </td>
            </tr>
            <tr>
              <td><span class="required">*</span> <?php echo 'From Date'; ?></td>
              <td colspan="2">
                <?php if($show == '1'){ ?>
                  <input class="date" type="text" name="from" id="from" value="<?php echo $from ?>" size="10" readonly="readonly" />
                  &nbsp;&nbsp;&nbsp;&nbsp;
                  <input class="date" type="text" name="to" id="to" value="<?php echo $to ?>" size="10" readonly="readonly" />
                <?php } else { ?>
                  <input type="text" name="from" id="from" value="<?php echo $from ?>" size="10" readonly="readonly" />
                  &nbsp;&nbsp;&nbsp;&nbsp;
                  <input type="text" name="to" id="to" value="<?php echo $to ?>" size="10" readonly="readonly" />
                <?php } ?>
              </td>
            </tr>
            <tr class="oneday">
              <td><span class="required">*</span> <?php echo 'Type'; ?></td>
              <td colspan="2">
                <?php if($show == '1'){ ?>
                  <select name="type" id="type">
                    <?php foreach($types as $tkey => $tvalue) { ?>
                      <?php if($type === $tkey) { ?>
                        <option value="<?php echo $tkey ?>" selected="selected"><?php echo $tvalue; ?></option>
                      <?php } else { ?>
                        <option value="<?php echo $tkey ?>"><?php echo $tvalue; ?></option>
                      <?php } ?>
                    <?php } ?>
                  </select>
                <?php } else { ?>
                  <select disabled="disabled" name="type" id="type">
                    <?php foreach($types as $tkey => $tvalue) { ?>
                      <?php if($type === $tkey) { ?>
                        <option value="<?php echo $tkey ?>" selected="selected"><?php echo $tvalue; ?></option>
                      <?php } else { ?>
                        <option value="<?php echo $tkey ?>"><?php echo $tvalue; ?></option>
                      <?php } ?>
                    <?php } ?>
                  </select>
                <?php } ?>
              </td>
            </tr>
            <tr class="oneday">
              <td><span class="required">*</span> <?php echo 'Leave Amount'; ?></td>
              <td colspan="2">
                <input readonly="readonly" type="text" id="leave_amount" name="leave_amount" value="<?php echo $leave_amount; ?>" size="10" />
                <?php if ($error_days) { ?>
                  <span class="error"><?php echo $error_days; ?></span>
                <?php } ?>
              </td>
            </tr>
            <tr class="multiday">
              <td><span class="required">*</span> <?php echo 'Days'; ?></td>
              <td colspan="2">
                <input readonly="readonly" type="text" id="days" name="days" value="<?php echo $days; ?>" size="10" />
                <?php if ($error_days) { ?>
                  <span class="error"><?php echo $error_days; ?></span>
                <?php } ?>
              </td>
            </tr>
            <tr class="multiday_2" id="enable_encash_tr" style="">
              <td><span class="required">*</span> <?php echo 'Enable Encash'; ?></td>
              <td colspan="2">
                <?php if($show == '1'){ ?>
                  <?php if($enable_encash == 1){ ?>
                    <input type="radio" class="enable_encash" name="enable_encash" value="1" checked="checked"/> Yes
                    <input type="radio" class="enable_encash" name="enable_encash" value="0" /> No
                  <?php } else { ?>
                    <input type="radio" class="enable_encash" name="enable_encash" value="1" /> Yes
                    <input type="radio" class="enable_encash" name="enable_encash" value="0" checked="checked" /> No
                  <?php } ?>
                <?php } else { ?>
                  <?php if($enable_encash == 1){ ?>
                    <input disabled="disabled" type="radio" class="enable_encash" name="enable_encash" value="1" checked="checked"/> Yes
                    <input disabled="disabled" type="radio" class="enable_encash" name="enable_encash" value="0" /> No
                  <?php } else { ?>
                    <input disabled="disabled" type="radio" class="enable_encash" name="enable_encash" value="1" /> Yes
                    <input disabled="disabled" type="radio" class="enable_encash" name="enable_encash" value="0" checked="checked" /> No
                  <?php } ?>
                <?php } ?>
              </td>
            </tr>
            <tr class="multiday_1" id="encash_tr">
              <td><span class="required">*</span> <?php echo 'Encash'; ?></td>
              <td colspan="2">
                <?php if($show == '1'){ ?>
                  <input type="text" id="encash" name="encash" value="<?php echo $encash; ?>" size="10" />
                <?php } else { ?>
                  <input readonly="readonly" type="text" id="encash" name="encash" value="<?php echo $encash; ?>" size="10" />
                <?php } ?>
                <span class="error er_encash" style="display:none;"></span>
                <?php if ($error_encash) { ?>
                  <span class="error"><?php echo $error_encash; ?></span>
                <?php } ?>
              </td>
            </tr>
            <tr style="display:none;">
              <td><span class="required">*</span> <?php echo 'Status'; ?></td>
              <td colspan="2">
                <select name="a_status" id="a_status">
                  <?php foreach($statuses as $akey => $avalue) { ?>
                    <?php if($a_status == $akey) { ?>
                      <option value="<?php echo $akey ?>" selected="selected"><?php echo $avalue; ?></option>
                    <?php } else { ?>
                      <option value="<?php echo $akey ?>"><?php echo $avalue; ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </td>
            </tr>
            <tr>
              <td><span class="required"></span> <?php echo 'Leave Reason'; ?></td>
              <td colspan="2">
                <?php if($show == '1'){ ?>
                  <input type="text" id="leave_reason" name="leave_reason" value="<?php echo $leave_reason; ?>" size="50" />
                <?php } else { ?>
                  <input readonly="readonly" type="text" id="leave_reason" name="leave_reason" value="<?php echo $leave_reason; ?>" size="50" />
                <?php } ?>
              </td>
            </tr>
            <?php if($show == '0'){ ?>
            <tr>
              <td><span class="required"></span> <?php echo 'Reject Reason Admin'; ?></td>
              <td colspan="2">
                <?php if(!isset($this->session->data['is_dept']) && !isset($this->session->data['is_super'])) { ?>
                  <input type="text" id="reject_reason" name="reject_reason" value="<?php echo $reject_reason; ?>" size="50" />
                <?php } else { ?>
                  <input readonly="readonly" type="text" id="reject_reason" name="reject_reason" value="<?php echo $reject_reason; ?>" size="50" />
                <?php } ?>
              </td>
            </tr>
            <tr>
              <td><span class="required"></span> <?php echo 'Reject Reason Dept'; ?></td>
              <td colspan="2">
                <?php if(isset($this->session->data['is_dept'])) { ?>
                  <input type="text" id="reject_reason_1" name="reject_reason_1" value="<?php echo $reject_reason_1; ?>" size="50" />
                <?php } else { ?>
                  <input readonly="readonly" type="text" id="reject_reason_1" name="reject_reason_1" value="<?php echo $reject_reason_1; ?>" size="50" />
                <?php } ?>
              </td>
            </tr>
            <tr>
              <td><span class="required"></span> <?php echo 'Reject Reason Secy'; ?></td>
              <td colspan="2">
                <?php if(isset($this->session->data['is_super'])) { ?>
                  <input type="text" id="reject_reason_2" name="reject_reason_2" value="<?php echo $reject_reason_2; ?>" size="50" />
                <?php } else { ?>
                  <input readonly="readonly" type="text" id="reject_reason_2" name="reject_reason_2" value="<?php echo $reject_reason_2; ?>" size="50" />
                <?php } ?>
              </td>
            </tr>
            <tr>
              <td><span class="required"></span> <?php echo 'Dept.Appr'; ?></td>
              <td colspan="2">
                <?php if(isset($this->session->data['is_dept'])) { ?>
                  <input readonly="readonly" type="text" id="approval_1" name="approval_1" value="<?php echo $approval_1; ?>" size="50" />
                <?php } else { ?>
                  <input readonly="readonly" type="text" id="approval_1" name="approval_1" value="<?php echo $approval_1; ?>" size="50" />
                <?php } ?>
              </td>
            </tr>
            <tr>
              <td><span class="required"></span> <?php echo 'Secy. Appr'; ?></td>
              <td colspan="2">
                <?php if(isset($this->session->data['is_super'])) { ?>
                  <input readonly="readonly" type="text" id="approval_2" name="approval_2" value="<?php echo $approval_2; ?>" size="50" />
                <?php } else { ?>
                  <input readonly="readonly" type="text" id="approval_2" name="approval_2" value="<?php echo $approval_2; ?>" size="50" />
                <?php } ?>
              </td>
            </tr>
            <?php } ?>
            <tr>
              <td colspan="3" id="shift_sch">

              </td>
            </tr>
          </table>
        </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript" src="view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript"><!--
$('#tabs a').tabs();
$('.date').datepicker({
    dateFormat: 'yy-mm-dd',
    onSelect: function(date, instance) {
      d_id = $(this).attr('id');
      if(d_id == 'date_coff'){
        emp_code = $('#emp_code').val();
        cof_date = $('#date_coff').val();
        $.ajax({
          url: 'index.php?route=transaction/leave/getshift_details&token=<?php echo $token; ?>&emp_code=' + emp_code + '&from=' + cof_date + '&to=' + cof_date + '&cof_stat=1',
          dataType: 'json',
          success: function(json) {  
            if(json.status == 1){
              $('#cof_shift_sch').html(json.html);
              $('#save').show();
              $('.ajax_warning').hide();
            } else if(json.status == 2){
              $('.ajax_warning').show();
              $('#save').hide();
            }   
          }
        });
      } else {
        if(d_id == 'from'){
          var date = $(this).datepicker('getDate');
          $('#to').datepicker('option', 'maxDate', date); // Reset minimum date
          date.setDate(date.getDate() + 30); // Add 30 days
        }
        $('#to').datepicker('setDate', date); // Set as default
        from_1 = $('#from').val();
        to = $('#to').val();
        emp_code = $('#emp_code').val();
        var f_mdy = from_1.split('-');
        f_mdy[1] = f_mdy[1] - 1;
        var t_mdy = to.split('-');
        t_mdy[1] = t_mdy[1] - 1;
        var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
        var firstDate = new Date(f_mdy[0],f_mdy[1],f_mdy[2]);
        var secondDate = new Date(t_mdy[0],t_mdy[1],t_mdy[2]);
        var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
        var diffDays = diffDays + 1;
        $('#diffDays').val(diffDays);
        if(diffDays > 1){
          leave_type = $('#leave_type').val();
          // if(leave_type == 'PL'){
          //   $('#encash_tr').show();
          // } else {
          //   $('#encash_tr').hide();
          // }
          encash_done_year = '<?php echo $encash_done_year; ?>';
          // if(leave_type == 'PL'){
          //   $('#encash_tr').show();
          // } else {
          //   $('#encash_tr').hide();
          // }
          $('.multiday').show();
          
          group = $('#group').val();
          if(group != 'OFFICIALS'){
            leave_type = $('#leave_type').val();
            if(leave_type == 'PL'){
              $('#date_cof').hide();
              $('.multiday_2').show();
            } else if(leave_type == 'COF'){
              $('#date_cof').show();
              $('.multiday_2').hide();
            } else {
              $('#date_cof').hide();
              $('.multiday_2').hide();  
            }
          } else {
            $('.multiday_2').hide();
          }
          
          $('#days').val(diffDays);
          $('#multi_day').val('1');
          encash = parseInt(diffDays) * 3.29;
          encash = Math.round(encash)
          $('#encash').val(encash);
          $('.oneday').hide();
          $('#leave_amount').val('0');
        } else {
          $('.multiday').hide();
          $('.multiday_2').hide();
          $('.oneday').show();
          $('#multi_day').val('0');
          $('#days').val('0');
          $('#leave_amount').val('1');
        }
        $.ajax({
          url: 'index.php?route=transaction/leave/getshift_details&token=<?php echo $token; ?>&emp_code=' + emp_code + '&from=' + from_1 + '&to=' + to,
          dataType: 'json',
          success: function(json) {  
            if(json.status == 1){
              $('#shift_sch').html(json.html);
              $('#save').show();
              $('.ajax_warning').hide();
            } else if(json.status == 2){
              $('.ajax_warning').show();
              $('#save').hide();
            }   
          }
        });
      }
    }
});

$('.date_1').datepicker({dateFormat: 'yy-mm-dd'});

$('.time').timepicker({timeFormat: 'hh:mm:ss'});

$.widget('custom.catcomplete', $.ui.autocomplete, {
  _renderMenu: function(ul, items) {
    var self = this, currentCategory = '';
    $.each(items, function(index, item) {
      if (item.category != currentCategory) {
        //ul.append('<li class="ui-autocomplete-category">' + item.category + '</li>');
        currentCategory = item.category;
      }
      self._renderItem(ul, item);
    });
  }
});

$(document).ready(function() {
  $('.multiday').hide();
  type = '<?php echo $type ?>';
  if(type == 'F'){
    $('#leave_amount').val('1');
  } else if(type == '1' || type == '2'){
    $('#leave_amount').val('0.5');
  }

  multi_day = '<?php echo $multi_day ?>';
  if(multi_day == '1'){
    $('.multiday').show();
    $('.multiday_1').show();
    group = $('#group').val();
    if(group != 'OFFICIALS'){
      $('.multiday_2').show();
    } else {
      $('.multiday_2').hide();
    }
    $('.oneday').hide();
  } else {
    $('.multiday_1').hide();
    $('.multiday_2').hide();
    $('.multiday').hide();
    $('.oneday').show();
  }

  leave_type = '<?php echo $leave_type ?>';
  if(leave_type == 'PL'){
    from_1 = $('#from').val();
    to = $('#to').val();
    var f_mdy = from_1.split('-');
    f_mdy[1] = f_mdy[1] - 1;
    var t_mdy = to.split('-');
    t_mdy[1] = t_mdy[1] - 1;
    var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
    var firstDate = new Date(f_mdy[0],f_mdy[1],f_mdy[2]);
    var secondDate = new Date(t_mdy[0],t_mdy[1],t_mdy[2]);
    var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
    var diffDays = diffDays + 1;
    $('#diffDays').val(diffDays);
    if(diffDays > 1){
      group = $('#group').val();
      if(group != 'OFFICIALS'){
        $('#encash_tr').show();
      } else {
        $('#encash_tr').hide();
      }
    } else {
      $('#encash_tr').hide();
    }
    $('#date_cof').hide();
  } else if(leave_type == 'COF'){ 
    $('#date_cof').show();
    $('#encash_tr').hide();
    $('#enable_encash_tr').hide();
  }else {
    $('#date_cof').hide();
    $('#encash_tr').hide();
    $('#enable_encash_tr').hide();
  }

  enable_encash = $('.enable_encash:checked').val()
  if(enable_encash == 1){
    $('#encash_tr').show();
  } else {
    $('#encash_tr').hide();
  }

  $('.date').datepicker({ 
      dateFormat: 'yy-mm-dd',
  });
  post_status = '<?php echo $post_status; ?>';
  edit_status = '<?php echo $edit_status; ?>';
  if(post_status == 0 && edit_status == 0){  
    to = '<?php echo $to ?>';
    var date = $('#from').datepicker('getDate');
    $('#to').datepicker('option', 'maxDate', date); // Reset minimum date
    date.setDate(date.getDate() + 30); // Add 30 days
  }

  if(edit_status == 1){
    emp_code = $('#emp_code').val();
    from_1 = $('#from').val();
    to = $('#to').val();
    $.ajax({
      url: 'index.php?route=transaction/leave/getshift_details&token=<?php echo $token; ?>&emp_code=' + emp_code + '&from=' + from_1 + '&to=' + to,
      dataType: 'json',
      success: function(json) {  
        if(json.status == 1){
          $('#shift_sch').html(json.html);
          $('#save').show();
          $('.ajax_warning').hide();
        } else if(json.status == 2){
          $('.ajax_warning').show();
          $('#save').hide();
        }   
      }
    });
  }

});

$('#type').change(function(){
  type = $(this).val();
  if(type == 'F'){
    $('#leave_amount').val('1');
  } else if(type == '1' || type == '2'){
    $('#leave_amount').val('0.5');
  }  
});


$('.enable_encash').change(function(){
  e_val = $(this).val();
  if(e_val == '1'){
    from_1 = $('#from').val();
    to = $('#to').val();
    emp_code = $('#emp_code').val();
    var f_mdy = from_1.split('-');
    f_mdy[1] = f_mdy[1] - 1;
    var t_mdy = to.split('-');
    t_mdy[1] = t_mdy[1] - 1;
    var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
    var firstDate = new Date(f_mdy[0],f_mdy[1],f_mdy[2]);
    var secondDate = new Date(t_mdy[0],t_mdy[1],t_mdy[2]);
    var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
    var diffDays = diffDays + 1;
    encash = parseInt(diffDays) * 3.29;
    encash = Math.round(encash)
    $('#encash').val(encash);
    $('#encash_tr').show();
  } else if(e_val == '0'){
    $('#encash_tr').hide();
    $('#encash').val('0')
  }  
});


$('#leave_type').change(function(){
  leave_type = $(this).val();
  if(leave_type == 'PL'){
    from_1 = $('#from').val();
    to = $('#to').val();
    var f_mdy = from_1.split('-');
    f_mdy[1] = f_mdy[1] - 1;
    var t_mdy = to.split('-');
    t_mdy[1] = t_mdy[1] - 1;
    var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
    var firstDate = new Date(f_mdy[0],f_mdy[1],f_mdy[2]);
    var secondDate = new Date(t_mdy[0],t_mdy[1],t_mdy[2]);
    var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
    var diffDays = diffDays + 1;
    if(diffDays > 1){
      group = $('#group').val();
      if(group != 'OFFICIALS'){
        e_val = $('.enable_encash:checked').val()
        if(e_val == 1){
          $('#encash_tr').show();
        } else {
          $('#encash_tr').hide();    
        }
        $('#enable_encash_tr').show();
      } else {
        $('#enable_encash_tr').hide();
        $('#encash_tr').hide();
      }
    }
    $('#date_cof').hide();
  } else if(leave_type == 'COF'){
    $('#date_cof').show();
    $('#encash_tr').hide();
    $('#enable_encash_tr').hide();
  } else {
    $('#date_cof').hide();
    $('#encash_tr').hide();
    $('#enable_encash_tr').hide();
  }  
});

$("#days").focusout(function() {
    days = $('#days').val();
    encash = parseInt(days) * 3.29;
    encash = Math.round(encash)
    encash_done_year = '<?php echo $encash_done_year; ?>';
    if(encash_done_year == 0){
      $("#encash").val(encash);
    } else{
      $('.er_encash').css('display', '');
    }
})

//--></script>
<?php echo $footer; ?>