<?php    
class ControllerCatalogEmployee extends Controller { 
	private $error = array();

	public function index() {
		$this->language->load('catalog/employee');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/employee');

		$this->getList();
	}

	public function insert() {
		$this->language->load('catalog/employee');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/employee');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_employee->addemployee($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/employee', 'token=' . $this->session->data['token'] . $url . '&filter_name=' . $this->request->post['name'], 'SSL'));
		}

		$this->getForm();
	}

	public function update() {
		$this->language->load('catalog/employee');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/employee');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			
			$this->model_catalog_employee->editemployee($this->request->get['employee_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			if(isset($this->request->get['return'])){
				$this->redirect($this->url->link('report/employee_data', 'token=' . $this->session->data['token'].'&h_name='.$this->request->post['name'].'&h_name_id='.$this->request->get['employee_id'], 'SSL'));
			} else {
				$this->redirect($this->url->link('catalog/employee', 'token=' . $this->session->data['token'] . $url , 'SSL'));
			}
		}

		$this->getForm();
	}

	public function delete() {
		$this->language->load('catalog/employee');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/employee');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $employee_id) {
				$this->model_catalog_employee->deleteemployee($employee_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/employee', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		} elseif(isset($this->request->get['employee_id']) && $this->validateDelete()){
			$this->model_catalog_employee->deleteemployee($this->request->get['employee_id']);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/employee', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	public function export_depthead(){
		$this->language->load('catalog/employee');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->data['attendace'] = array();

		
        $final_datas = array();
        $results = $this->db->query("SELECT * FROM `oc_employee` WHERE `is_dept` = '1' ")->rows;
        foreach ($results as $rkey => $rvalue) {
        	$dept = $rvalue['department'];
        	$final_datas[] = array(
				'employee_id' => $rvalue['employee_id'],
				'name'        => $rvalue['name'],
				'code' 	      => $rvalue['emp_code'],
				'department'   => $dept,
			);
		}
		
		//$final_datas = array_chunk($final_datas, 15);
		// echo '<pre>';
		// print_r($final_datas);
		// exit;
		
		if($final_datas){
			//$month = date("F", mktime(0, 0, 0, $filter_month, 10));
			$template = new Template();		
			$template->data['final_datas'] = $final_datas;
			$template->data['title'] = 'Department Head List';
			if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
				$template->data['base'] = HTTPS_SERVER;
			} else {
				$template->data['base'] = HTTP_SERVER;
			}
			$html = $template->fetch('catalog/export_depthead.tpl');
			//echo $html;exit;
			$filename = "Department_Head_List.html";
			header('Content-type: text/html');
			header('Content-Disposition: attachment; filename='.$filename);
			echo $html;
			exit;		
		} else {
			$this->session->data['warning'] = 'No Data Found';
			$this->redirect($this->url->link('catalog/employee', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}
	}

	protected function getList() {
		$this->data['reporting_to_name'] = '';
		if (isset($this->session->data['emp_code'])) {
			$filter_emp_code = $this->session->data['emp_code'];
			$reporting_to_datas = $this->db->query("SELECT `reporting_to`, `reporting_to_name` FROM `oc_employee` WHERE `emp_code` = '".$filter_emp_code."' "); 
			if($reporting_to_datas->num_rows > 0){
				$reporting_to_name = $reporting_to_datas->row['reporting_to_name'];
				$this->data['reporting_to_name'] = $reporting_to_name;
			}
		} else {
			$filter_emp_code = '';
			$this->data['reporting_to_name'] = '';
		}

		// echo '<pre>';
		// print_r($this->data['reporting_to_name']);
		// exit;

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/employee', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->data['insert'] = $this->url->link('catalog/employee/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('catalog/employee/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');	
		$this->data['export_depthead'] = $this->url->link('catalog/employee/export_depthead', 'token=' . $this->session->data['token'] . $url, 'SSL');	

		$this->data['employees'] = array();

		$data = array(
			'filter_name' => $filter_name,
			'filter_emp_code' => $filter_emp_code,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);
		$location_d ='';
		$employee_total = $this->model_catalog_employee->getTotalemployees($data);

		$results = $this->model_catalog_employee->getemployees($data);

		foreach ($results as $result) {

			$loc_ds = $this->db->query("SELECT * FROM `oc_unit` WHERE `unit_id` = '".$result['unit_id']."' ")->row;
			$location_d = $loc_ds['unit'];
			// echo'<pre>';
			// print_r($location_d);
			// exit;
			$action = array();

			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('catalog/employee/update', 'token=' . $this->session->data['token'] . '&employee_id=' . $result['employee_id'] . $url, 'SSL')
			);

			// $action[] = array(
			// 	'text' => $this->language->get('text_delete'),
			// 	'href' => $this->url->link('catalog/employee/delete', 'token=' . $this->session->data['token'] . '&employee_id=' . $result['employee_id'] . $url, 'SSL')
			// );

			$today_date = date('j');
			$month = date('n');
			$year = date('Y');
			$shift_data = $this->model_catalog_employee->getshift_id($result['employee_id'], $today_date, $month, $year);
			$shift_ids = explode('_', $shift_data);
			if(isset($shift_ids[1])){
				if($shift_ids[0] == 'S'){
					$shift_id = $shift_ids['1'];
					$shift_name = $this->model_catalog_employee->getshiftname($shift_id);
				} elseif($shift_ids[0] == 'W') {
					$shift_id = $shift_ids['1'];
					//$shift_name = $this->model_catalog_employee->getweekname($shift_id);
					$shift_name = 'Weekly Off';
				} elseif($shift_ids[0] == 'H'){
					$shift_id = $shift_ids['1'];
					//$shift_name = $this->model_catalog_employee->getholidayname($shift_id);
					$shift_name = 'Holiday';
				} elseif($shift_ids[0] == 'HD'){
					$shift_id = $shift_ids['1'];
					//$shift_name = $this->model_catalog_employee->getholidayname($shift_id);
					$shift_name = 'Half Day';
				} elseif($shift_ids[0] == 'C'){
					$shift_id = $shift_ids['1'];
					//$shift_name = $this->model_catalog_employee->getholidayname($shift_id);
					$shift_name = 'Complimentary Off';
				}
			} else {
				$shift_id = 0;
				$shift_name = '';
			}
			

			//$shift = $this->model_catalog_employee->getshiftname($result['shift']);
			$location = $result['unit'];//$this->model_catalog_employee->getlocname($result['location']); //'Mumbai'
			$category = $result['category'];//$this->model_catalog_employee->getcatname($result['category']); //'PLD';
 			$dept = $result['department'];//$this->model_catalog_employee->getdeptname($result['department']); //'Personal';

 			if($result['is_dept'] == '1'){
 				$dept_head = 'Department head';
 			} else {
 				$dept_head = '';
 			}

			$this->data['employees'][] = array(
				'employee_id' => $result['employee_id'],
				'name'        => $result['name'],
				'code' 	      => $result['emp_code'],
				'location' 	  => $location_d,
				'deartment'   => $dept,
				'category'   => $category,
				'shift'   => $shift_name,
				'dept_head'   => $dept_head,
				'selected'        => isset($this->request->post['selected']) && in_array($result['employee_id'], $this->request->post['selected']),
				'action'          => $action
			);
		}

		$this->data['token'] = $this->session->data['token'];	

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_delete'] = $this->language->get('text_delete');

		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_trainer'] = $this->language->get('column_trainer');
		$this->data['column_action'] = $this->language->get('column_action');		

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');

		$this->data['button_filter'] = $this->language->get('button_filter');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_trainer'])) {
			$url .= '&filter_trainer=' . $this->request->get['filter_trainer'];
		}

		if (isset($this->request->get['filter_trainer_id'])) {
			$url .= '&filter_trainer_id=' . $this->request->get['filter_trainer_id'];
		}

		$this->data['sort_name'] = $this->url->link('catalog/employee', 'token=' . $this->session->data['token'] . '&sort=name' . $url, 'SSL');
		$this->data['sort_trainer'] = $this->url->link('catalog/employee', 'token=' . $this->session->data['token'] . '&sort=trainer' . $url, 'SSL');

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_trainer'])) {
			$url .= '&filter_trainer=' . $this->request->get['filter_trainer'];
		}

		if (isset($this->request->get['filter_trainer_id'])) {
			$url .= '&filter_trainer_id=' . $this->request->get['filter_trainer_id'];
		}

		$pagination = new Pagination();
		$pagination->total = $employee_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('catalog/employee', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		$this->data['filter_name'] = $filter_name;
		$this->data['filter_emp_code'] = $filter_emp_code;
		///$this->data['filter_trainer'] = $filter_trainer;
		//$this->data['filter_trainer_id'] = $filter_trainer_id;

		$this->template = 'catalog/employee_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_default'] = $this->language->get('text_default');
		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_dob'] = $this->language->get('entry_dob');
		$this->data['entry_doj'] = $this->language->get('entry_doj');
		$this->data['entry_action'] = $this->language->get('entry_action');
		$this->data['entry_assign'] = $this->language->get('entry_assign');
		$this->data['entry_remove'] = $this->language->get('entry_remove');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		$this->data['text_no_results'] = $this->language->get('text_no_results');

		$this->data['tab_general'] = $this->language->get('tab_general');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$this->data['error_name'] = $this->error['name'];
		} else {
			$this->data['error_name'] = '';
		}

		if (isset($this->error['card_number'])) {
			$this->data['error_card_number'] = $this->error['card_number'];
		} else {
			$this->data['error_card_number'] = '';
		}

		if (isset($this->error['daily_punch'])) {
			$this->data['error_daily_punch'] = $this->error['daily_punch'];
		} else {
			$this->data['error_daily_punch'] = '';
		}

		if (isset($this->error['weekly_off'])) {
			$this->data['error_weekly_off'] = $this->error['weekly_off'];
		} else {
			$this->data['error_weekly_off'] = '';
		}

		if (isset($this->error['status'])) {
			$this->data['error_status'] = $this->error['status'];
		} else {
			$this->data['error_status'] = '';
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}


		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/employee', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		if (isset($this->request->get['employee_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$employee_info = $this->model_catalog_employee->getemployee($this->request->get['employee_id']);
		}

		$this->data['token'] = $this->session->data['token'];

		if (isset($this->request->post['name'])) {
			$this->data['name'] = $this->request->post['name'];
			$this->data['hidden_name'] = $this->request->post['hidden_name'];
		} elseif (!empty($employee_info)) {
			$this->data['name'] = $employee_info['name'];
			$this->data['hidden_name'] = $employee_info['name'];
		} else {	
			$this->data['name'] = '';
			$this->data['hidden_name'] = '';
		}

		if (isset($this->request->post['emp_code'])) {
			$this->data['emp_code'] = $this->request->post['emp_code'];
			$this->data['hidden_emp_code'] = $this->request->post['hidden_emp_code'];
		} elseif (!empty($employee_info)) {
			$this->data['emp_code'] = $employee_info['emp_code'];
			$this->data['hidden_emp_code'] = $employee_info['emp_code'];
		} else {	
			$emp_codes = $this->db->query("SELECT `emp_code` FROM `oc_employee` ORDER BY `emp_code` DESC LIMIT 1");
			if($emp_codes->num_rows > 0){
				$emp_code = $emp_codes->row['emp_code'] + 1;
			} else {
				$emp_code = '1000';
			}
			$this->data['emp_code'] = $emp_code;
			$this->data['hidden_emp_code'] = $emp_code;
		}

		if (isset($this->request->post['grade'])) {
			$this->data['grade'] = $this->request->post['grade'];
		} elseif (!empty($employee_info)) {
			$this->data['grade'] = $employee_info['grade'];
		} else {	
			$this->data['grade'] = '';
		}

		if (isset($this->request->post['grade_id'])) {
			$this->data['grade_id'] = $this->request->post['grade_id'];
		} elseif (!empty($employee_info)) {
			$this->data['grade_id'] = $employee_info['grade_id'];
		} else {	
			$this->data['grade_id'] = '0';
		}

		if (isset($this->request->post['branch'])) {
			$this->data['branch'] = $this->request->post['branch'];
		} elseif (!empty($employee_info)) {
			$this->data['branch'] = $employee_info['branch'];
		} else {	
			$this->data['branch'] = '';
		}

		if (isset($this->request->post['department'])) {
			$this->data['department'] = $this->request->post['department'];
		} elseif (!empty($employee_info)) {
			$this->data['department'] = $employee_info['department'];
		} else {	
			$this->data['department'] = '';
		}

		if (isset($this->request->post['department_id'])) {
			$this->data['department_id'] = $this->request->post['department_id'];
		} elseif (!empty($employee_info)) {
			$this->data['department_id'] = $employee_info['department_id'];
		} else {	
			$this->data['department_id'] = '0';
		}

		if (isset($this->request->post['reporting_to'])) {
			$this->data['reporting_to'] = $this->request->post['reporting_to'];
		} elseif (!empty($employee_info)) {
			$this->data['reporting_to'] = $employee_info['reporting_to'];
		} else {	
			$this->data['reporting_to'] = '0';
		}

		if (isset($this->request->post['reporting_to_name'])) {
			$this->data['reporting_to_name'] = $this->request->post['reporting_to_name'];
		} elseif (!empty($employee_info)) {
			$this->data['reporting_to_name'] = $employee_info['reporting_to_name'];
		} else {	
			$this->data['reporting_to_name'] = '';
		}

		if (isset($this->request->post['division'])) {
			$this->data['division'] = $this->request->post['division'];
		} elseif (!empty($employee_info)) {
			$this->data['division'] = $employee_info['division'];
		} else {	
			$this->data['division'] = '';
		}

		if (isset($this->request->post['group'])) {
			$this->data['group'] = $this->request->post['group'];
		} elseif (!empty($employee_info)) {
			$this->data['group'] = $employee_info['group'];
		} else {	
			$this->data['group'] = '';
		}

		if (isset($this->request->post['category'])) {
			$this->data['category'] = $this->request->post['category'];
		} elseif (!empty($employee_info)) {
			$this->data['category'] = $employee_info['category'];
		} else {	
			$this->data['category'] = '';
		}

		if (isset($this->request->post['unit'])) {
			$this->data['unit'] = $this->request->post['unit'];
		} elseif (!empty($employee_info)) {
			$this->data['unit'] = $employee_info['unit'];
		} else {	
			$this->data['unit'] = 'Mumbai';
		}

		$this->data['units'] = array(
			'Unit 31' => 'Unit 31',
			'Unit 64' => 'Unit 64',
			//'Mumbai' => 'Vasai',
			//'Pune' => 'Pune',
			//'Delhi' => 'Delhi',
			//'Chennai' => 'Chennai',
			//'Bangalore' => 'Bangalore',
			//'Ahmedabad' => 'Ahmedabad',
		);

		if (isset($this->request->post['unit_id'])) {
			$this->data['unit_id'] = $this->request->post['unit_id'];
		} elseif (!empty($employee_info)) {
			$this->data['unit_id'] = $employee_info['unit_id'];
		} else {
			$this->data['unit_id'] = '';
		}
			
		$this->load->model('catalog/location');
		$loc_datas = $this->model_catalog_location->getlocations();
		$loc_data = array();
		foreach ($loc_datas as $lkey => $lvalue) {
			$loc_data[strtolower(trim($lvalue['unit_id']))] = (trim($lvalue['unit']));
		}
		$this->data['loc_datas'] = $loc_data;

		if (isset($this->request->post['designation'])) {
			$this->data['designation'] = $this->request->post['designation'];
		} elseif (!empty($employee_info)) {
			$this->data['designation'] = $employee_info['designation'];
		} else {	
			$this->data['designation'] = '';
		}

		if (isset($this->request->post['designation_id'])) {
			$this->data['designation_id'] = $this->request->post['designation_id'];
		} elseif (!empty($employee_info)) {
			$this->data['designation_id'] = $employee_info['designation_id'];
		} else {	
			$this->data['designation_id'] = '';
		}

		$locations_query = $this->db->query("SELECT * FROM oc_locations WHERE 1=1");
		$this->data['locations_array'] = array(
			'1'  => 'HO',
			'2'  => 'Unit1',
		);
		if($locations_query->num_rows > 0){
			foreach ($locations_query->rows as $lkey => $lvalue) {
				$this->data['locations_array'][$lvalue['id']] = $lvalue['name'];
			}
		} 
		
		if (isset($this->request->post['locations_id'])) {
			$this->data['locations_id'] = $this->request->post['locations_id'];
		} elseif (!empty($employee_info)) {
			$this->data['locations_id'] = $employee_info['location_id'];
		} else {	
			$this->data['locations_id'] = '2';
		}
		if (isset($this->request->post['dob'])) {
			$this->data['dob'] = $this->request->post['dob'];
		} elseif (!empty($employee_info)) {
			$this->data['dob'] = $employee_info['dob'];
		} else {	
			$this->data['dob'] = '';
		}



		if (isset($this->request->post['doj'])) {
			$this->data['doj'] = $this->request->post['doj'];
		} elseif (!empty($employee_info)) {
			$this->data['doj'] = $employee_info['doj'];
		} else {	
			$this->data['doj'] = '';
		}

		if (isset($this->request->post['doc'])) {
			$this->data['doc'] = $this->request->post['doc'];
		} elseif (!empty($employee_info)) {
			$this->data['doc'] = $employee_info['doc'];
		} else {	
			$this->data['doc'] = '';
		}

		if (isset($this->request->post['dol'])) {
			$this->data['dol'] = $this->request->post['dol'];
		} elseif (!empty($employee_info)) {
			$this->data['dol'] = $employee_info['dol'];
		} else {	
			$this->data['dol'] = '';
		}

		if (isset($this->request->post['shift_name'])) {
			$this->data['shift_name'] = $this->request->post['shift_name'];
			$this->data['shift_id'] = $this->request->post['shift_id'];
		} elseif (!empty($employee_info)) {
			$today_date = date('j');
			$month = date('n');
			$year = date('Y');
			$shift_data = $this->model_catalog_employee->getshift_id($employee_info['emp_code'], $today_date, $month, $year);
			$shift_ids = explode('_', $shift_data);
			if(isset($shift_ids[1])){
				if($shift_ids[0] == 'S'){
					$shift_id = $shift_ids['1'];
					$shift_name = $this->model_catalog_employee->getshiftname($shift_id);
				} elseif($shift_ids[0] == 'W') {
					$shift_id = $shift_ids['1'];
					//$shift_name = $this->model_catalog_employee->getweekname($shift_id);
					$shift_name = 'Weekly Off';
				} elseif($shift_ids[0] == 'H'){
					$shift_id = $shift_ids['1'];
					//$shift_name = $this->model_catalog_employee->getholidayname($shift_id);
					$shift_name = 'Holiday';
				} elseif($shift_ids[0] == 'HD'){
					$shift_id = $shift_ids['1'];
					//$shift_name = $this->model_catalog_employee->getholidayname($shift_id);
					$shift_name = 'Half Day';
				} elseif($shift_ids[0] == 'C'){
					$shift_id = $shift_ids['1'];
					//$shift_name = $this->model_catalog_employee->getholidayname($shift_id);
					$shift_name = 'Complimentary Off';
				}
			} else {
				$shift_id = 0;
				$shift_name = '';
			}
			$this->data['shift_id'] = $shift_id;
			$this->data['shift_name'] = $shift_name;
		} else {	
			$this->data['shift_name'] = '';
			$this->data['shift_id'] = 0;
		}

		if (isset($this->request->post['status'])) {
			$this->data['status'] = $this->request->post['status'];
		} elseif (!empty($employee_info)) {
			$this->data['status'] = $employee_info['status'];
		} else {	
			$this->data['status'] = 1;
		}

		if (isset($this->request->post['gender'])) {
			$this->data['gender'] = $this->request->post['gender'];
		} elseif (!empty($employee_info)) {
			$this->data['gender'] = $employee_info['gender'];
		} else {	
			$this->data['gender'] = '';
		}

		$this->data['genders'] = array(
			'M' => 'Male',
			'F' => 'Female'
		);

		if (isset($this->request->post['esic_no'])) {
			$this->data['esic_no'] = $this->request->post['esic_no'];
		} elseif (!empty($employee_info)) {
			$this->data['esic_no'] = $employee_info['esic_no'];
		} else {	
			$this->data['esic_no'] = '';
		}

		if (isset($this->request->post['pfuan_no'])) {
			$this->data['pfuan_no'] = $this->request->post['pfuan_no'];
		} elseif (!empty($employee_info)) {
			$this->data['pfuan_no'] = $employee_info['pfuan_no'];
		} else {	
			$this->data['pfuan_no'] = '';
		}

		if (isset($this->request->post['basic'])) {
			$this->data['basic'] = $this->request->post['basic'];
		} elseif (!empty($employee_info)) {
			$this->data['basic'] = $employee_info['basic'];
		} else {	
			$this->data['basic'] = '';
		}

		if (isset($this->request->post['incentive'])) {
			$this->data['incentive'] = $this->request->post['incentive'];
		} elseif (!empty($employee_info)) {
			$this->data['incentive'] = $employee_info['incentive'];
		} else {	
			$this->data['incentive'] = '';
		}

		if (isset($this->request->post['perdaysalary'])) {
			$this->data['perdaysalary'] = $this->request->post['perdaysalary'];
		} elseif (!empty($employee_info)) {
			$this->data['perdaysalary'] = $employee_info['perdaysalary'];
		} else {	
			$this->data['perdaysalary'] = '';
		}

		if (isset($this->request->post['perdaysalary2'])) {
			$this->data['perdaysalary2'] = $this->request->post['perdaysalary2'];
		} elseif (!empty($employee_info)) {
			$this->data['perdaysalary2'] = $employee_info['perdaysalary2'];
		} else {	
			$this->data['perdaysalary2'] = '';
		}

		if (isset($this->request->post['daily_punch'])) {
			$this->data['daily_punch'] = $this->request->post['daily_punch'];
		} elseif (!empty($employee_info)) {
			$this->data['daily_punch'] = $employee_info['daily_punch'];
		} else {	
			$this->data['daily_punch'] = 1;
		}

		if (isset($this->request->post['weekly_off'])) {
			$this->data['weekly_off'] = $this->request->post['weekly_off'];
		} elseif (!empty($employee_info)) {
			$this->data['weekly_off'] = $employee_info['weekly_off'];
		} else {	
			$this->data['weekly_off'] = 1;
		}

		if (isset($this->request->post['card_number'])) {
			$this->data['card_number'] = $this->request->post['card_number'];
		} elseif (!empty($employee_info)) {
			$this->data['card_number'] = $employee_info['card_number'];
		} else {	
			$this->data['card_number'] = '';
		}

		if (isset($this->request->post['shift_type'])) {
			$this->data['shift_type'] = $this->request->post['shift_type'];
		} elseif (!empty($employee_info)) {
			$this->data['shift_type'] = $employee_info['shift_type'];
		} else {	
			$this->data['shift_type'] = 'F';
		}

		if (isset($this->request->post['is_dept'])) {
			$this->data['is_dept'] = $this->request->post['is_dept'];
		} elseif (!empty($employee_info)) {
			$this->data['is_dept'] = $employee_info['is_dept'];
		} else {	
			$this->data['is_dept'] = '0';
		}

		if (isset($this->request->post['dept_head_list'])) {
			$this->data['dept_head_list'] = $this->request->post['dept_head_list'];
		} elseif (!empty($employee_info)) {
			$dept_head_lists = explode(',', $employee_info['dept_head_list']);
			foreach($dept_head_lists as $dkey => $dvalue){
				$dept_head_list[html_entity_decode(strtolower(trim($dvalue)))] = html_entity_decode(strtolower(trim($dvalue)));
			}
			$this->data['dept_head_list'] = $dept_head_list;
		} else {	
			$this->data['dept_head_list'] = array();
		}

		$this->load->model('report/attendance');
		$department_datas = $this->model_report_attendance->getdepartment_list();
		$department_data = array();
		//$department_data['0'] = 'All';
		foreach ($department_datas as $dkey => $dvalue) {
			$department_data[strtolower(trim($dvalue['department']))] = (trim($dvalue['department']));
		}
		$this->data['department_data'] = $department_data;

		if (isset($this->request->post['is_super'])) {
			$this->data['is_super'] = $this->request->post['is_super'];
		} elseif (!empty($employee_info)) {
			$this->data['is_super'] = $employee_info['is_super'];
		} else {	
			$this->data['is_super'] = '0';
		}

		if (isset($this->request->post['ot_calculate'])) {
			$this->data['ot_calculate'] = $this->request->post['ot_calculate'];
		} elseif (!empty($employee_info)) {
			$this->data['ot_calculate'] = $employee_info['ot_calculate'];
		} else {	
			$this->data['ot_calculate'] = '1';
		}

		if (isset($this->request->post['work_hour'])) {
			$this->data['work_hour'] = $this->request->post['work_hour'];
		} elseif (!empty($employee_info)) {
			$this->data['work_hour'] = $employee_info['work_hour'];
		} else {	
			$this->data['work_hour'] = '1';
		}

		$this->data['work_hours'] = array(
			'1' => '12 Hours',
			'2' => '8 Hours',
		);

		if (isset($this->request->post['is_lunch'])) {
			$this->data['is_lunch'] = $this->request->post['is_lunch'];
		} elseif (!empty($employee_info)) {
			$this->data['is_lunch'] = $employee_info['is_lunch'];
		} else {	
			$this->data['is_lunch'] = '1';
		}

		if (isset($this->request->post['work_hour_cut'])) {
			$this->data['work_hour_cut'] = $this->request->post['work_hour_cut'];
		} elseif (!empty($employee_info)) {
			$this->data['work_hour_cut'] = $employee_info['work_hour_cut'];
		} else {	
			$this->data['work_hour_cut'] = '0';
		}

		if (isset($this->request->post['ot_hours'])) {
			$this->data['ot_hours'] = $this->request->post['ot_hours'];
		} elseif (!empty($employee_info)) {
			$this->data['ot_hours'] = $employee_info['ot_hours'];
		} else {	
			$this->data['ot_hours'] = '';
		}

		if (isset($this->request->post['ot_days'])) {
			$this->data['ot_days'] = $this->request->post['ot_days'];
		} elseif (!empty($employee_info)) {
			$this->data['ot_days'] = $employee_info['ot_days'];
		} else {	
			$this->data['ot_days'] = '';
		}

		if (isset($this->request->post['password_reset'])) {
			$this->data['password_reset'] = $this->request->post['password_reset'];
		} else {	
			$this->data['password_reset'] = '0';
		}

		// if (isset($this->request->post['category_name'])) {
		// 	$this->data['category_id'] = $this->request->post['category_id'];			
		// 	$this->data['category_name'] = $this->request->post['category_name'];
		// } elseif (!empty($employee_info)) {
		// 	$this->data['category_id'] = $employee_info['category'];
		// 	$category_name = $this->model_catalog_employee->getcatname($employee_info['category']);
		// 	$this->data['category_name'] = $category_name;
		// } else {	
		// 	$this->data['category_id'] = '';
		// 	$this->data['category_name'] = 0;
		// }

		// if (isset($this->request->post['department_name'])) {
		// 	$this->data['department_name'] = $this->request->post['department_name'];
		// 	$this->data['department_id'] = $this->request->post['department_id'];
		// } elseif (!empty($employee_info)) {
		// 	$this->data['department_id'] = $employee_info['department'];
		// 	$department_name = $this->model_catalog_employee->getdeptname($employee_info['department']);
		// 	$this->data['department_name'] = $department_name;
		// } else {	
		// 	$this->data['department_name'] = '';
		// 	$this->data['department_id'] = 0;
		// }

		// if (isset($this->request->post['location_name'])) {
		// 	$this->data['location_name'] = $this->request->post['location_name'];
		// 	$this->data['location_id'] = $this->request->post['location_id'];
		// } elseif (!empty($employee_info)) {
		// 	$this->data['location_id'] = $employee_info['location'];
		// 	$location_name = $this->model_catalog_employee->getlocname($employee_info['location']);
		// 	$this->data['location_name'] = $location_name;
		// } else {	
		// 	$this->data['location_name'] = '';
		// 	$this->data['location_id'] = 0;
		// }

		

		if (!isset($this->request->get['employee_id'])) {
			$this->data['action'] = $this->url->link('catalog/employee/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			if(isset($this->request->get['return'])){
				$this->data['action'] = $this->url->link('catalog/employee/update', 'token=' . $this->session->data['token'] . '&employee_id=' . $this->request->get['employee_id'] . '&return=1', 'SSL');
			} else {
				$this->data['action'] = $this->url->link('catalog/employee/update', 'token=' . $this->session->data['token'] . '&employee_id=' . $this->request->get['employee_id'] . $url, 'SSL');
			}
		}

		if(isset($this->request->get['return'])){
			$this->data['cancel'] = $this->url->link('report/employee_data', 'token=' . $this->session->data['token'] . '&h_name=' . $this->data['name'] . '&h_name_id=' . $this->request->get['employee_id'], 'SSL');
		} else {
			$this->data['cancel'] = $this->url->link('catalog/employee', 'token=' . $this->session->data['token'] . $url, 'SSL');
		}

		$this->data['weeks'] = array(
			'1' => 'Sunday',
			'2' => 'Monday',
			'3' => 'Tuesday',
			'4' => 'Wednesday',
			'5' => 'Thursday',
			'6' => 'Friday',
			'7' => 'Saturday',
		);
		
		$this->template = 'catalog/employee_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}  

	protected function validateForm() {
		$this->load->model('catalog/employee');

		if (!$this->user->hasPermission('modify', 'catalog/employee')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if(strlen(utf8_decode(trim($this->request->post['name']))) < 1 || strlen(utf8_decode(trim($this->request->post['name']))) > 255){
			$this->error['name'] = $this->language->get('error_name');
		}

		// echo '<pre>';
		// print_r($this->error);
		// exit;

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/employee')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('catalog/employee');

		// if(isset($this->request->post['selected'])){
		// 	foreach ($this->request->post['selected'] as $employee_id) {
		// 		$employee_total = $this->model_catalog_employee->getTotaltreatmentByemployeeId($employee_id);

		// 		if ($employee_total) {
		// 			$this->error['warning'] = sprintf($this->language->get('error_employee'), $employee_total);
		// 		}	
		// 	}
		// } elseif(isset($this->request->get['employee_id'])){
		// 	$employee_total = $this->model_catalog_employee->getTotaltreatmentByemployeeId($this->request->get['employee_id']);

		// 	if ($employee_total) {
		// 		$this->error['warning'] = sprintf($this->language->get('error_employee'), $employee_total);
		// 	}
		// }

		if (!$this->error) {
			return true;
		} else {
			return false;
		}  
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/employee');

			if (isset($this->session->data['emp_code'])) {
				$filter_emp_code = $this->session->data['emp_code'];
			} else {
				$filter_emp_code = '';
			}

			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				'filter_emp_code' => $filter_emp_code,
				'start'       => 0,
				'limit'       => 10
			);

			$results = $this->model_catalog_employee->getemployees($data);

			foreach ($results as $result) {
				$json[] = array(
					'employee_id' => $result['employee_id'],
					'emp_code' => $result['employee_id'], 
					'name'            => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}



	public function getlocation() {
		$json = array();
		$division_datas = array();
		$unit_datas = array();
		$device_datas = array();
		if (isset($this->request->get['filter_division_id']) || isset($this->request->get['filter_division_ids'])) {
			$this->load->model('catalog/unit');
			$this->load->model('catalog/device');
			if(isset($this->request->get['filter_division_id'])){
				$data = array(
					'filter_division_id' => $this->request->get['filter_division_id'],
				);
			} else {
				$data = array(
					'filter_division_ids' => $this->request->get['filter_division_ids'],
				);
			}
			if(isset($this->request->get['filter_state_id'])){
				//$data['filter_state_id'] = $this->request->get['filter_state_id'];
			}
			$results = $this->model_catalog_unit->getUnits($data);
			$unit_datas[] = array(
				'unit_id' => '',
				'unit' => 'All'
			);
			$device_datas[] = array(
				'device_id' => '',
				'device_name' => 'All'
			);
			$site_string = $this->user->getsite();
			$site_array = array();
			if($site_string != ''){
				$site_array = explode(',', $site_string);
			}
			foreach ($results as $result) {
				if(!empty($site_array)){
					if(in_array($result['unit_id'], $site_array)){
						$unit_datas[] = array(
							'unit_id' => $result['unit_id'],
							'unit' => strip_tags(html_entity_decode($result['unit'], ENT_QUOTES, 'UTF-8'))
						);
					}
				} else {
					$unit_datas[] = array(
						'unit_id' => $result['unit_id'],
						'unit' => strip_tags(html_entity_decode($result['unit'], ENT_QUOTES, 'UTF-8'))
					);
				}
			}
			if($unit_datas){
				foreach($unit_datas as $ukey => $uvalue){
					if($uvalue['unit_id'] != ''){
						$data = array(
							//'filter_unit' => $uvalue['unit'],
						);
						$results = $this->model_catalog_device->getDevices($data);
						foreach ($results as $result) {
							$device_datas[] = array(
								'device_id' => $result['device_id'],
								'device_name' => strip_tags(html_entity_decode($result['device_name'], ENT_QUOTES, 'UTF-8'))
							);
						}	
					}
				}
			}
		}
		$json['unit_datas'] = $unit_datas;
		$json['device_datas'] = $device_datas;
		// $sort_order = array();
		// foreach ($json as $key => $value) {
		// 	$sort_order[$key] = $value['name'];
		// }
		//array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}


	public function autocomplete_trainer() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/trainer');

			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_trainer->gettrainers($data);

			foreach ($results as $result) {
				$json[] = array(
					'trainer_id' => $result['trainer_id'], 
					'name'            => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}

	public function autocomplete_owner() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/owner');

			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_owner->getowners($data);

			foreach ($results as $result) {
				$json[] = array(
					'owner_id' => $result['owner_id'], 
					'name'            => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		//$this->log->write(print_r($json,true));

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}	
}
?>