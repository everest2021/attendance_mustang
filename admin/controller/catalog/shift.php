<?php    
class ControllerCatalogShift extends Controller { 
	private $error = array();

	public function get_in_times($time){
		date_default_timezone_set("Asia/Kolkata");
		$minus_start_time = date('H:i:s', strtotime($time . "-15 minutes"));
		$time_array = array();
		for($i=0; $i<15; $i++){
			$time_array[] = date('H:i:s', strtotime($minus_start_time . "+".$i." minutes"));
		}
		for($i=0; $i<=30; $i++){
			$time_array[] = date('H:i:s', strtotime($time . "+".$i." minutes"));
		}
		return $time_array;		
	}

	public function generate_random($length, $max_value){
		$randarray = array(); 
		for($i = 1; $i <= $length;) { 
		    unset($rand); 
		    $rand = rand(1, $max_value); 
		    if(!in_array($rand, $randarray)) { 
		        $randarray[] = $rand; 
		        $i++; 
		    } 
		}
		return $randarray;
	}

	public function index() {
		/*
		$shift_re_sql = "SELECT * FROM `oc_shift_change`";
		$shift_re = $this->db->query($shift_re_sql)->row;
		if(isset($shift_re['date'])){
			$shift_change_date = $shift_re['date'];
		} else {
			$shift_change_date = date('Y-m-d');
		}
		$filter_date_start = '2019-01-15';
		$times = '07:00:00';
		$start_date = new DateTime($shift_change_date.' '.$times);
		$since_start = $start_date->diff(new DateTime($filter_date_start.' '.$times));
		echo '<pre>';
		print_r($since_start);
		exit;

		$randarray = $this->get_in_times('07:00:00');
		echo '<pre>';
		print_r($randarray);
		exit;
		

		$a = 709;
		$b = 5;
		$c = (int)$a / $b;
		$d = $a % $b;;
		echo $c;
		echo '<br />';
		echo $d;
		echo '<br />';
		exit;
		*/
		$this->language->load('catalog/shift');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/shift');

		$this->getList();
	}

	public function insert() {
		$this->language->load('catalog/shift');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/shift');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_shift->addshift($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/shift', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function update() {
		$this->language->load('catalog/shift');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/shift');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_shift->editshift($this->request->get['shift_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/shift', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function export() {
		$this->language->load('catalog/shift');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/shift');

		if(1==1){
			$data['filter_name'] = '';
			$data['sort'] = 'shift_id';
			$shift_datas = $this->model_catalog_shift->getshifts($data);
			$final_datas = array();
			foreach($shift_datas as $skey => $svalue){
				$final_datas[$skey]['shift_name'] = $svalue['name'];
				$final_datas[$skey]['shift_intime'] = $svalue['in_time'];
				$final_datas[$skey]['shift_outtime'] = $svalue['out_time'];

				$dept_mumbai = unserialize($svalue['department_mumbai']);
				$dept_mum_data = '';
				
				if($dept_mumbai){
					foreach ($dept_mumbai as $dkey => $dvalue) {
						$dept_mum_data .= $dvalue . ', '; 
					}
				}

				$dept_pune = unserialize($svalue['department_pune']);
				$dept_pune_data = '';
				if($dept_pune){
					foreach ($dept_pune as $dkey => $dvalue) {
						$dept_pune_data .= $dvalue . ', '; 
					}
				}

				$dept_mov = unserialize($svalue['department_moving']);
				$dept_mov_data = '';
				if($dept_mov){
					foreach ($dept_mov as $dkey => $dvalue) {
						$dept_mov_data .= $dvalue . ', '; 
					}
				}	
				$final_datas[$skey]['dept_mum_data'] = $dept_mum_data;
				$final_datas[$skey]['dept_pune_data'] = $dept_pune_data;
				$final_datas[$skey]['dept_mov_data'] = $dept_mov_data;
			}
			// echo '<pre>';
			// print_r($final_datas);
			// exit;

			$template = new Template();		
			$template->data['final_datas'] = $final_datas;
			//$template->data['filter_year'] = $filter_year;
			$template->data['title'] = 'SHift Schedule';
			if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
				$template->data['base'] = HTTPS_SERVER;
			} else {
				$template->data['base'] = HTTP_SERVER;
			}
			$html = $template->fetch('catalog/shift_schedule_html.tpl');
			//echo $html;exit;
			$filename = "Shitt_Schedule";
			
			header("Content-Type: application/vnd.ms-excel; charset=utf-8");
			header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false);
			echo $html;
			exit;
		} else {
			$this->session->data['warning'] = 'No Data';
			//$this->redirect($this->url->link('catalog/shift', 'token=' . $this->session->data['token'], 'SSL'));
			$this->getList();
		}
	}

	public function delete() {
		$this->language->load('catalog/shift');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/shift');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $shift_id) {
				$this->model_catalog_shift->deleteshift($shift_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/shift', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		} elseif(isset($this->request->get['shift_id']) && $this->validateDelete()){
			$this->model_catalog_shift->deleteshift($this->request->get['shift_id']);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/shift', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'shift_id';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/shift', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->data['insert'] = $this->url->link('catalog/shift/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('catalog/shift/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');	
		
		$this->data['export'] = $this->url->link('catalog/shift/export', 'token=' . $this->session->data['token'] . $url, 'SSL');	

		$this->data['shifts'] = array();

		$data = array(
			'filter_name' => $filter_name,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);

		$shift_total = $this->model_catalog_shift->getTotalshifts($data);

		$results = $this->model_catalog_shift->getshifts($data);

		// echo '<pre>';
		// print_r($results);
		// exit;

		foreach ($results as $result) {
			$action = array();

			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('catalog/shift/update', 'token=' . $this->session->data['token'] . '&shift_id=' . $result['shift_id'] . $url, 'SSL')
			);

			// $action[] = array(
			// 	'text' => 'Delete',
			// 	'href' => $this->url->link('catalog/shift/delete', 'token=' . $this->session->data['token'] . '&shift_id=' . $result['shift_id'] . $url, 'SSL')
			// );
			// echo '<pre>';
			// print_r($result);exit;
			$this->data['shifts'][] = array(
				'shift_id' => $result['shift_id'],
				'shift_code'=>$result['shift_code'],
				'name'            => $result['name'],
				'selected'        => isset($this->request->post['selected']) && in_array($result['shift_id'], $this->request->post['selected']),
				'action'          => $action
			);
		}

		$this->data['token'] = $this->session->data['token'];	

		$this->data['heading_title'] = $this->language->get('heading_title');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		$this->data['sort_name'] = $this->url->link('catalog/shift', 'token=' . $this->session->data['token'] . '&sort=name' . $url, 'SSL');
		
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		$pagination = new Pagination();
		$pagination->total = $shift_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('catalog/shift', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		$this->data['filter_name'] = $filter_name;
		
		$this->template = 'catalog/shift_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$this->data['error_name'] = $this->error['name'];
		} else {
			$this->data['error_name'] = '';
		}

		if (isset($this->error['shift_code'])) {
			$this->data['error_shift_code'] = $this->error['shift_code'];
		} else {
			$this->data['error_shift_code'] = '';
		}

		if (isset($this->error['in_time'])) {
			$this->data['error_in_time'] = $this->error['in_time'];
		} else {
			$this->data['error_in_time'] = '';
		}

		if (isset($this->error['out_time'])) {
			$this->data['error_out_time'] = $this->error['out_time'];
		} else {
			$this->data['error_out_time'] = '';
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/shift', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		if (!isset($this->request->get['shift_id'])) {
			$this->data['action'] = $this->url->link('catalog/shift/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('catalog/shift/update', 'token=' . $this->session->data['token'] . '&shift_id=' . $this->request->get['shift_id'] . $url, 'SSL');
		}

		$this->data['cancel'] = $this->url->link('catalog/shift', 'token=' . $this->session->data['token'] . $url, 'SSL');

		if (isset($this->request->get['shift_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$shift_info = $this->model_catalog_shift->getshift($this->request->get['shift_id']);
		}

		$this->data['token'] = $this->session->data['token'];

		$this->load->model('report/attendance');

		$unit_data = array(
			'Mumbai' => 'Mumbai',
			'Pune' => 'Pune',
			'Moving' => 'Moving' 
		);

		$this->data['unit_data'] = $unit_data;
		
		$department_datas = $this->model_report_attendance->getdepartment_list();
		$department_data = array();
		//$department_data['0'] = 'All';
		foreach ($department_datas as $dkey => $dvalue) {
			$department_data[strtolower($dvalue['department'])] = $dvalue['department'];
		}
		$this->data['department_data'] = $department_data;

		if (isset($this->request->post['name'])) {
			$this->data['name'] = $this->request->post['name'];
		} elseif (!empty($shift_info)) {
			$this->data['name'] = $shift_info['name'];
		} else {	
			$this->data['name'] = '';
		}

		if (isset($this->request->post['shift_code'])) {
			$this->data['shift_code'] = $this->request->post['shift_code'];
		} elseif (!empty($shift_info)) {
			$this->data['shift_code'] = $shift_info['shift_code'];
		} else {	
			$this->data['shift_code'] = '';
		}
		
		if (isset($this->request->post['in_time'])) {
			$this->data['in_time'] = $this->request->post['in_time'];
		} elseif (!empty($shift_info)) {
			$this->data['in_time'] = $shift_info['in_time'];
		} else {	
			$this->data['in_time'] = '';
		}

		if (isset($this->request->post['out_time'])) {
			$this->data['out_time'] = $this->request->post['out_time'];
		} elseif (!empty($shift_info)) {
			$this->data['out_time'] = $shift_info['out_time'];
		} else {	
			$this->data['out_time'] = '';
		}

		if (isset($this->request->post['weekly_off_1'])) {
			$this->data['weekly_off_1'] = $this->request->post['weekly_off_1'];
		} elseif (!empty($shift_info)) {
			$this->data['weekly_off_1'] = $shift_info['weekly_off_1'];
		} else {	
			$this->data['weekly_off_1'] = '';
		}

		if (isset($this->request->post['weekly_off_2'])) {
			$this->data['weekly_off_2'] = $this->request->post['weekly_off_2'];
		} elseif (!empty($shift_info)) {
			$this->data['weekly_off_2'] = $shift_info['weekly_off_2'];
		} else {	
			$this->data['weekly_off_2'] = '';
		}

		if (isset($this->request->post['lunch'])) {
			$this->data['lunch'] = $this->request->post['lunch'];
		} elseif (!empty($shift_info)) {
			$this->data['lunch'] = $shift_info['lunch'];
		} else {	
			$this->data['lunch'] = 0;
		}

		$this->data['weeks'] = array(
			'1' => 'Sunday',
			'2' => 'Monday',
			'3' => 'Tuesday',
			'4' => 'Wednesday',
			'5' => 'Thursday',
			'6' => 'Friday',
			'7' => 'Saturday',
		);

		if (isset($this->request->post['dept_holiday_mumbai'])) {
			$this->data['dept_holiday_mumbai'] = $this->request->post['dept_holiday_mumbai'];
		} elseif (!empty($shift_info['department_mumbai'])) {
			$this->data['dept_holiday_mumbai'] = unserialize($shift_info['department_mumbai']);
			foreach ($this->data['dept_holiday_mumbai'] as $key => $value) {
				$this->data['dept_holiday_mumbai'][$key] = html_entity_decode(strtolower(trim($value)));
			}
			//$this->data['dept_holiday'] = array();
		} else {	
			$this->data['dept_holiday_mumbai'] = array();
		}

		if (isset($this->request->post['dept_holiday_pune'])) {
			$this->data['dept_holiday_pune'] = $this->request->post['dept_holiday_pune'];
		} elseif (!empty($shift_info['department_pune'])) {
			$this->data['dept_holiday_pune'] = unserialize($shift_info['department_pune']);
			foreach ($this->data['dept_holiday_pune'] as $key => $value) {
				$this->data['dept_holiday_pune'][$key] = html_entity_decode(strtolower(trim($value)));
			}
			//$this->data['dept_holiday'] = array();
		} else {	
			$this->data['dept_holiday_pune'] = array();
		}

		if (isset($this->request->post['dept_holiday_moving'])) {
			$this->data['dept_holiday_moving'] = $this->request->post['dept_holiday_moving'];
		} elseif (!empty($shift_info['department_moving'])) {
			$this->data['dept_holiday_moving'] = unserialize($shift_info['department_moving']);
			foreach ($this->data['dept_holiday_moving'] as $key => $value) {
				$this->data['dept_holiday_moving'][$key] = html_entity_decode(strtolower(trim($value)));
			}
			//$this->data['dept_holiday'] = array();
		} else {	
			$this->data['dept_holiday_moving'] = array();
		}

		// echo '<pre>'; 
		// print_r($this->data['dept_holiday_moving']);
		// exit;

		$this->template = 'catalog/shift_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}  

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/shift')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if ((utf8_strlen($this->request->post['name']) < 1) || (utf8_strlen($this->request->post['name']) > 64)) {
			$this->error['name'] = $this->language->get('error_name');
		}

		if ((utf8_strlen($this->request->post['shift_code']) < 1) || (utf8_strlen($this->request->post['shift_code']) > 64)) {
			$this->error['shift_code'] = 'Shift Code is Required';
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/shift')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		// $this->load->model('catalog/horse');

		// if(isset($this->request->post['selected'])){
		// 	foreach ($this->request->post['selected'] as $shift_id) {
		// 		$horse_total = $this->model_catalog_horse->getTotalhorsesByshiftId($shift_id);

		// 		if ($horse_total) {
		// 			$this->error['warning'] = sprintf($this->language->get('error_horse'), $horse_total);
		// 		}	
		// 	}
		// } elseif(isset($this->request->get['shift_id'])){
		// 	$horse_total = $this->model_catalog_horse->getTotalhorsesByshiftId($this->request->get['shift_id']);

		// 	if ($horse_total) {
		// 		$this->error['warning'] = sprintf($this->language->get('error_horse'), $horse_total);
		// 	}
		// }

		if (!$this->error) {
			return true;
		} else {
			return false;
		}  
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/shift');

			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_shift->getshifts($data);

			foreach ($results as $result) {
				$json[] = array(
					'shift_id' => $result['shift_id'],
					'shift_code'=>$result['shift_code'] ,
					'name'            => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}	
}
?>