<?php    
class ControllerCatalogLeave extends Controller { 
	private $error = array();

	public function index() {
		$this->language->load('catalog/leave');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('catalog/leave');
		$this->load->model('catalog/employee');

		$this->getList();
	}

	public function insert() {
		$this->language->load('catalog/leave');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/leave');
		$this->load->model('catalog/employee');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			//$emp_data = $this->model_transaction_transaction->getEmployees_dat($data['e_name_id']);
			$this->session->data['success'] = 'You have Inserted the Transaction';	
			//echo 'out';exit;
			$this->redirect($this->url->link('transaction/manualpunch', 'token=' . $this->session->data['token'].'&transaction_id='.$transaction_id, 'SSL'));
		}

		$this->getForm();
	}

	public function update() {
		$this->language->load('catalog/leave');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/leave');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_leave->insert_leavedata($this->request->post);
			$this->session->data['success'] = 'You have Inserted the Transaction';	
			//echo 'out';exit;
			$this->redirect($this->url->link('catalog/leave', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->getForm();
	}

	protected function getList() {
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/leave', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->data['insert'] = $this->url->link('catalog/leave/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('catalog/leave/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');	
		$this->data['change_year'] = $this->url->link('catalog/leavechangeyear', 'token=' . $this->session->data['token'] . $url, 'SSL');	

		$this->data['leaves'] = array();

		$data = array(
			'filter_name' => $filter_name,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);

		$leave_total = $this->model_catalog_employee->getTotalemployees($data);

		$results = $this->model_catalog_employee->getemployees($data);

		foreach ($results as $result) {
			$action = array();

			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('catalog/leave/update', 'token=' . $this->session->data['token'] . '&emp_code=' . $result['emp_code'] . $url, 'SSL')
			);

			$this->data['leaves'][] = array(
				'employee_id' => $result['employee_id'],
				'name'        => $result['name'],
				'selected'    => isset($this->request->post['selected']) && in_array($result['employee_id'], $this->request->post['selected']),
				'action'      => $action
			);
		}

		$this->data['token'] = $this->session->data['token'];	

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_delete'] = $this->language->get('text_delete');

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');

		$this->data['button_filter'] = $this->language->get('button_filter');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		if (isset($this->session->data['warning'])) {
			$this->data['error_warning'] = $this->session->data['warning'];

			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		$this->data['sort_name'] = $this->url->link('catalog/employee', 'token=' . $this->session->data['token'] . '&sort=name' . $url, 'SSL');

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		
		$pagination = new Pagination();
		$pagination->total = $leave_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('catalog/leave', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		$this->data['filter_name'] = $filter_name;
		///$this->data['filter_trainer'] = $filter_trainer;
		//$this->data['filter_trainer_id'] = $filter_trainer_id;

		$this->template = 'catalog/leave_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');

		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		$this->data['text_no_results'] = $this->language->get('text_no_results');

		$this->data['tab_general'] = $this->language->get('tab_general');

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {	
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['e_name'])) {
			$this->data['error_employee'] = $this->error['e_name'];
		} else {
			$this->data['error_employee'] = '';
		}

		if (isset($this->error['dot'])) {
			$this->data['error_dot'] = $this->error['dot'];
		} else {
			$this->data['error_dot'] = '';
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/leave', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$this->data['cancel'] = $this->url->link('catalog/leave', 'token=' . $this->session->data['token'], 'SSL');
		
		if(isset($this->request->get['leave_id'])){
			$this->data['action'] = $this->url->link('catalog/leave/update', 'token=' . $this->session->data['token'].'&leave_id='.$this->request->get['leave_id'], 'SSL');
		} else {
			$this->data['action'] = $this->url->link('catalog/leave/update', 'token=' . $this->session->data['token'], 'SSL');
		}
		
		$transaction_data = array();
		if (isset($this->request->get['emp_code']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$transaction_data = $this->model_catalog_leave->getleave($this->request->get['emp_code']);
		}

		$emp_data = $this->model_catalog_leave->getEmployees_dat($this->request->get['emp_code']);

		$this->data['token'] = $this->session->data['token'];

		if (isset($this->request->post['leave_id'])) {
			$this->data['leave_id'] = $this->request->post['leave_id'];
		} elseif(isset($transaction_data[0]['leave_id'])) {
			$this->data['leave_id'] = $transaction_data[0]['leave_id'];
		} else {	
			$this->data['leave_id'] = 0;
		}

		if (isset($this->request->post['year'])) {
			$this->data['year'] = $this->request->post['year'];
		} elseif(isset($transaction_data[0]['year'])) {
			$this->data['year'] = $transaction_data[0]['year'];
		} else {	
			$this->data['year'] = date('Y');
		}

		if (isset($this->request->post['e_name'])) {
			$this->data['e_name'] = $this->request->post['e_name'];
			$this->data['e_name_id'] = $this->request->post['e_name_id'];
			$this->data['emp_code'] = $this->request->post['e_name_id'];
			$this->data['emp_doj'] = $this->request->post['emp_doj'];
			$this->data['emp_grade'] = $this->request->post['emp_grade'];
			$this->data['emp_designation'] = $this->request->post['emp_designation'];
		} elseif (isset($transaction_data[0]['emp_name'])) {
			$this->data['e_name'] = $transaction_data[0]['emp_name'];
			$this->data['e_name_id'] = $transaction_data[0]['emp_id'];
			$this->data['emp_code'] = $transaction_data[0]['emp_id'];
			$this->data['emp_doj'] = $transaction_data[0]['emp_doj'];
			$this->data['emp_grade'] = $transaction_data[0]['emp_grade'];
			$this->data['emp_designation'] = $transaction_data[0]['emp_designation'];
		} else {	
			$this->data['e_name'] = $emp_data['name'];
			$this->data['e_name_id'] = $emp_data['emp_code'];
			$this->data['emp_code'] = $emp_data['emp_code'];
			$this->data['emp_doj'] = $emp_data['doj'];
			$this->data['emp_grade'] = $emp_data['grade'];
			$this->data['emp_designation'] = $emp_data['designation'];
		}

		$current_date = date('Y-m-d');
		$doj = $this->data['emp_doj'];//date('Y-m-d');//

		$start_date2 = new DateTime($current_date);
		$since_start2 = $start_date2->diff(new DateTime($doj));

		$this->data['error_warning1'] = '';
		$this->data['editable'] = 0;
		if($since_start2->y >= 1){
			if($this->user->getId() == '1'){
				$this->data['editable'] = 1;
			}
		} elseif($since_start2->m >= 6){
			if($this->user->getId() == '1'){
				$this->data['editable'] = 1;
			}
		} else{
			if($this->data['emp_grade'] == 'CONSULTANT'){
				if($this->user->getId() == '1'){				
					$this->data['editable'] = 1;
				}
			} else {
				if($this->user->getId() == '1'){				
					$this->data['editable'] = 1;
				}
				//$this->data['error_warning1'] = 'Empolyee Not Eligible For Leaves';
				//$this->data['editable'] = 0;
			}
		}
		// echo '<pre>';
		// print_r($this->data['editable']);
		// exit;

		if (isset($this->request->post['pl_acc'])) {
			$this->data['pl_acc'] = $this->request->post['pl_acc'];
		} elseif(isset($transaction_data[0]['pl_acc'])) {
			$this->data['pl_acc'] = $transaction_data[0]['pl_acc'];
		} else {	
			$this->data['pl_acc'] = 0;
		}

		if (isset($this->request->post['bl_acc'])) {
			$this->data['bl_acc'] = $this->request->post['bl_acc'];
		} elseif(isset($transaction_data[0]['bl_acc'])) {
			$this->data['bl_acc'] = $transaction_data[0]['bl_acc'];
		} else {	
			$this->data['bl_acc'] = 0;
		}

		if (isset($this->request->post['sl_acc'])) {
			$this->data['sl_acc'] = $this->request->post['sl_acc'];
		} elseif(isset($transaction_data[0]['sl_acc'])) {
			$this->data['sl_acc'] = $transaction_data[0]['sl_acc'];
		} else {	
			$this->data['sl_acc'] = 0;
		}

		if (isset($this->request->post['lwp_acc'])) {
			$this->data['lwp_acc'] = $this->request->post['lwp_acc'];
		} elseif(isset($transaction_data[0]['lwp_acc'])) {
			$this->data['lwp_acc'] = $transaction_data[0]['lwp_acc'];
		} else {	
			$this->data['lwp_acc'] = 0;
		}

		if (isset($this->request->post['ml_acc'])) {
			$this->data['ml_acc'] = $this->request->post['ml_acc'];
		} elseif(isset($transaction_data[0]['ml_acc'])) {
			$this->data['ml_acc'] = $transaction_data[0]['ml_acc'];
		} else {	
			$this->data['ml_acc'] = 0;
		}

		if (isset($this->request->post['mal_acc'])) {
			$this->data['mal_acc'] = $this->request->post['mal_acc'];
		} elseif(isset($transaction_data[0]['mal_acc'])) {
			$this->data['mal_acc'] = $transaction_data[0]['mal_acc'];
		} else {	
			$this->data['mal_acc'] = 0;
		}

		if (isset($this->request->post['pal_acc'])) {
			$this->data['pal_acc'] = $this->request->post['pal_acc'];
		} elseif(isset($transaction_data[0]['pal_acc'])) {
			$this->data['pal_acc'] = $transaction_data[0]['pal_acc'];
		} else {	
			$this->data['pal_acc'] = 0;
		}

		if (isset($this->request->post['cof_acc'])) {
			$this->data['cof_acc'] = $this->request->post['cof_acc'];
		} elseif(isset($transaction_data[0]['cof_acc'])) {
			$this->data['cof_acc'] = $transaction_data[0]['cof_acc'];
		} else {	
			$this->data['cof_acc'] = 0;
		}

		$this->template = 'catalog/leave_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}  

	protected function validateForm() {
		$this->load->model('transaction/transaction');

		if (!$this->user->hasPermission('modify', 'transaction/horse_wise')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if(strlen(utf8_decode(trim($this->request->post['e_name']))) < 1 || strlen(utf8_decode(trim($this->request->post['e_name']))) > 255){
			$this->error['e_name'] = 'Please Select Employee Name';
		} else {
			if($this->request->post['e_name_id'] == ''){
				$emp_id = $this->model_transaction_transaction->getempexist($this->request->post['e_name_id']);				
				if($emp_id == 0){
					$this->error['e_name'] = 'Employee Does Not Exist';
				}
			}
		}

		// echo '<pre>';
		// print_r($this->error);
		// exit;

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
}
?>
