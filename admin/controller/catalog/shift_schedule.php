<?php    
class ControllerCatalogShiftSchedule extends Controller { 
	private $error = array();

	public function index() {
		$this->language->load('catalog/shift_schedule');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/shift_schedule');
		$this->load->model('catalog/employee');

		$this->getList();
	}

	public function update() {
		$this->language->load('catalog/shift_schedule');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/shift_schedule');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			// echo '<pre>';
			// print_r($this->request->post);
			// exit;
			$this->model_catalog_shift_schedule->editshift_schedule($this->request->get['employee_code'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/shift_schedule', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	protected function getList() {
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_code'])) {
			$filter_code = $this->request->get['filter_code'];
		} else {
			$filter_code = '';
		}

		if (isset($this->request->get['filter_department'])) {
			$filter_department = $this->request->get['filter_department'];
		} else {
			$filter_department = '';
		}

		if (isset($this->request->get['filter_unit'])) {
			$filter_unit = $this->request->get['filter_unit'];
		} else {
			$filter_unit = '';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_code'])) {
			$url .= '&filter_code=' . $this->request->get['filter_code'];
		}

		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}

		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/shift_schedule', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->data['employees'] = array();

		$data = array(
			'filter_name' => $filter_name,
			'filter_code' => $filter_code,
			'filter_department' => $filter_department,
			'filter_unit' => $filter_unit,
			//'filter_shift_type' => 1,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);

		$shift_schedule_total = $this->model_catalog_employee->getTotalemployees($data);

		$results = $this->model_catalog_employee->getemployees($data);

		// echo '<pre>';
		// print_r($results);
		// exit;

		foreach ($results as $result) {
			$action = array();

			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('catalog/shift_schedule/update', 'token=' . $this->session->data['token'] . '&employee_code=' . $result['employee_id'] . $url, 'SSL')
			);

			$this->data['employees'][] = array(
				'employee_id' => $result['employee_id'],
				'employee_code' => $result['emp_code'],
				'name' => $result['name'],
				'department' => $result['department'],
				'unit' => $result['unit'],
				'action'          => $action
			);
		}

		$this->data['token'] = $this->session->data['token'];	

		$this->data['heading_title'] = $this->language->get('heading_title');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_code'])) {
			$url .= '&filter_code=' . $this->request->get['filter_code'];
		}

		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}

		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}

		$this->data['sort_name'] = $this->url->link('catalog/shift_schedule', 'token=' . $this->session->data['token'] . '&sort=name' . $url, 'SSL');
		$this->data['sort_code'] = $this->url->link('catalog/shift_schedule', 'token=' . $this->session->data['token'] . '&sort=emp_code' . $url, 'SSL');
		$this->data['sort_department'] = $this->url->link('catalog/shift_schedule', 'token=' . $this->session->data['token'] . '&sort=department' . $url, 'SSL');
		$this->data['sort_unit'] = $this->url->link('catalog/shift_schedule', 'token=' . $this->session->data['token'] . '&sort=unit' . $url, 'SSL');
		
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_code'])) {
			$url .= '&filter_code=' . $this->request->get['filter_code'];
		}

		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}

		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}

		$this->load->model('report/attendance');

		$unit_data = array(
			'0' => 'All',
			'Mumbai' => 'Mumbai',
			'Pune' => 'Pune',
			'Moving' => 'Moving' 
		);

		$this->data['unit_data'] = $unit_data;

		$department_datas = $this->model_report_attendance->getdepartment_list();
		$department_data = array();
		$department_data['0'] = 'All';
		foreach ($department_datas as $dkey => $dvalue) {
			$department_data[$dvalue['department']] = $dvalue['department'];
		}
		$this->data['department_data'] = $department_data;

		$pagination = new Pagination();
		$pagination->total = $shift_schedule_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('catalog/shift_schedule', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		$this->data['filter_name'] = $filter_name;
		$this->data['filter_code'] = $filter_code;
		$this->data['filter_department'] = $filter_department;
		$this->data['filter_unit'] = $filter_unit;
		
		$this->template = 'catalog/shift_schedule_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	public function getForm() {
		$this->language->load('catalog/shift_schedule');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/shift_schedule');
		$this->load->model('catalog/employee');
		
		$this->data['heading_title'] = $this->language->get('heading_title');

		if (isset($this->request->get['filter_month'])) {
			$filter_month = $this->request->get['filter_month'];
		} else {
			$filter_month = date('n');
		}

		if (isset($this->request->get['filter_year'])) {
			$filter_year = $this->request->get['filter_year'];
		} else {
			$filter_year = date('Y');
		}

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$this->data['error_name'] = $this->error['name'];
		} else {
			$this->data['error_name'] = '';
		}

		if (isset($this->error['date'])) {
			$this->data['error_date'] = $this->error['date'];
		} else {
			$this->data['error_date'] = '';
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_code'])) {
			$url .= '&filter_code=' . $this->request->get['filter_code'];
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . $this->request->get['filter_date'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/shift_schedule', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		if (!isset($this->request->get['employee_code'])) {
			$this->data['action'] = $this->url->link('catalog/shift_schedule/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('catalog/shift_schedule/update', 'token=' . $this->session->data['token'] . '&employee_code=' . $this->request->get['employee_code'] . $url, 'SSL');
		}

		$this->data['cancel'] = $this->url->link('catalog/shift_schedule', 'token=' . $this->session->data['token'] . $url, 'SSL');

		if (isset($this->request->get['employee_code']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$shift_schedule_info = $this->model_catalog_shift_schedule->getshift_schedule($this->request->get['employee_code'],$filter_month, $filter_year);
		}

		$this->data['emp_name'] = '';
		if(isset($shift_schedule_info['emp_code'])){
			$this->data['emp_name'] = $this->model_catalog_shift_schedule->getempname($shift_schedule_info['emp_code']);
		}

		$filter_name = '';
		$data = array(
			'filter_name' => $filter_name
		);
		$this->load->model('catalog/shift');
		$shift_datass = $this->model_catalog_shift->getshifts($data);
		$shift_data = array();
		//$shift_data[0] = 'No';
		foreach ($shift_datass as $skey => $svalue) {
			$shift_data[$svalue['shift_id']] = $svalue['name'];
		}
		$this->data['shift_data'] = $shift_data;

		$filter_name = '';
		$data = array(
			'filter_name' => $filter_name
		);
		$this->load->model('catalog/week');
		$week_datass = $this->model_catalog_week->getweeks($data);
		$week_data = array();
		$week_data[0] = 'No';
		$week_data[1] = 'Yes';
		// foreach ($week_datass as $wkey => $wvalue) {
		// 	$week_data[$wvalue['week_id']] = $wvalue['name'];
		// }
		$this->data['week_data'] = $week_data;

		$filter_name = '';
		$data = array(
			'filter_name' => $filter_name
		);
		$this->load->model('catalog/holiday');
		$holiday_datass = $this->model_catalog_holiday->getholidays($data);
		$holiday_data = array();
		$holiday_data[0] = 'No';
		$holiday_data[1] = 'Yes';
		// foreach ($holiday_datass as $hkey => $hvalue) {
		// 	$holiday_data[$hvalue['holiday_id']] = $hvalue['name'];
		// }
		$this->data['holiday_data'] = $holiday_data;

		$halfday_data = array();
		$halfday_data[0] = 'No';
		$halfday_data[1] = 'Yes';
		// foreach ($week_datass as $wkey => $wvalue) {
		// 	$week_data[$wvalue['week_id']] = $wvalue['name'];
		// }
		$this->data['halfday_data'] = $halfday_data;

		$compli_data = array();
		$compli_data[0] = 'No';
		$compli_data[1] = 'Yes';
		// foreach ($week_datass as $wkey => $wvalue) {
		// 	$week_data[$wvalue['week_id']] = $wvalue['name'];
		// }
		$this->data['compli_data'] = $compli_data;

		$shift_day_data = array();
		$week_day_data = array();
		$holiday_day_data = array();
		$halfday_day_data = array();
		$compli_day_data = array();
		// echo '<pre>';
		// print_r($shift_schedule_info);
		// exit;
		foreach ($shift_schedule_info as $skey => $svalue) {
			if(is_int($skey)){
				$explode = explode('_', $svalue);
				// if($explode[0] == 'S'){
				// 	$shift_day_data[$skey] = $explode[1];
				// 	$week_day_data[$skey] = '';
				// 	$holiday_day_data[$skey] = '';
				// 	$halfday_day_data[$skey] = '';
				// 	$compli_day_data[$skey] = '';
				// } else
				if(!isset($explode[2])){
					$explode[2] = '';
				}
				if ($explode[0] == 'W') {
					$shift_day_data[$skey] = $explode[2];
					$week_day_data[$skey] = $explode[1];
					$holiday_day_data[$skey] = '';
					$halfday_day_data[$skey] = '';
					$compli_day_data[$skey] = '';
				} elseif($explode[0] == 'H'){
					$shift_day_data[$skey] = $explode[2];
					$week_day_data[$skey] = '';
					$holiday_day_data[$skey] = $explode[1];
					$halfday_day_data[$skey] = '';
					$compli_day_data[$skey] = '';
				} elseif($explode[0] == 'HD'){
					$shift_day_data[$skey] = $explode[2];
					$week_day_data[$skey] = '';
					$holiday_day_data[$skey] = '';
					$compli_day_data[$skey] = '';
					$halfday_day_data[$skey] = $explode[1];
				} elseif($explode[0] == 'C'){
					$shift_day_data[$skey] = $explode[2];
					$week_day_data[$skey] = '';
					$holiday_day_data[$skey] = '';
					$halfday_day_data[$skey] = '';
					$compli_day_data[$skey] = $explode[1];
				} elseif($explode[0] == 'S'){
					$shift_day_data[$skey] = $explode[1];
					$week_day_data[$skey] = '';
					$holiday_day_data[$skey] = '';
					$halfday_day_data[$skey] = '';
					$compli_day_data[$skey] = '';
				}
			}
		}

		$this->data['holiday_day_data'] = $holiday_day_data;
		$this->data['shift_day_data'] = $shift_day_data;
		$this->data['week_day_data'] = $week_day_data;
		$this->data['halfday_day_data'] = $halfday_day_data;
		$this->data['compli_day_data'] = $compli_day_data;


		// echo '<pre>';
		// print_r($shift_day_data);
		// echo '<pre>';
		// print_r($week_day_data);
		// echo '<pre>';
		// print_r($holiday_day_data);
		// exit;


		$this->data['shift_schedule_info'] = $shift_schedule_info;
		$this->data['filter_month'] = $filter_month;
		$this->data['filter_year'] = $filter_year;

		$months = array(
			'1' => 'January',
			'2' => 'Feburary',
			'3' => 'March',
			'4' => 'April',
			'5' => 'May',
			'6' => 'June',
			'7' => 'July',
			'8' => 'August',
			'9' => 'September',
			'10' => 'October',
			'11' => 'November',
			'12' => 'December'
		);
		$this->data['months'] = $months;

		$years = array(
			date('Y') => date('Y'),
		);
		$this->data['years'] = $years;
		
		$this->data['token'] = $this->session->data['token'];

		// echo '<pre>';
		// print_r($shift_schedule_info);
		// exit;
		
		$this->template = 'catalog/shift_schedule_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}  

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/shift_schedule')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		// if ((utf8_strlen($this->request->post['name']) < 1) || (utf8_strlen($this->request->post['name']) > 64)) {
		// 	$this->error['name'] = $this->language->get('error_name');
		// }

		// if ((utf8_strlen($this->request->post['shift_schedule_code']) < 1) || (utf8_strlen($this->request->post['shift_schedule_code']) > 64)) {
		// 	$this->error['shift_schedule_code'] = 'shift_schedule Code is Required';
		// }

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/shift_schedule')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		// $this->load->model('catalog/horse');

		// if(isset($this->request->post['selected'])){
		// 	foreach ($this->request->post['selected'] as $shift_schedule_id) {
		// 		$horse_total = $this->model_catalog_horse->getTotalhorsesByshift_scheduleId($shift_schedule_id);

		// 		if ($horse_total) {
		// 			$this->error['warning'] = sprintf($this->language->get('error_horse'), $horse_total);
		// 		}	
		// 	}
		// } elseif(isset($this->request->get['shift_schedule_id'])){
		// 	$horse_total = $this->model_catalog_horse->getTotalhorsesByshift_scheduleId($this->request->get['shift_schedule_id']);

		// 	if ($horse_total) {
		// 		$this->error['warning'] = sprintf($this->language->get('error_horse'), $horse_total);
		// 	}
		// }

		if (!$this->error) {
			return true;
		} else {
			return false;
		}  
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/shift_schedule');

			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_shift_schedule->getshift_schedules($data);

			foreach ($results as $result) {
				$json[] = array(
					'shift_schedule_id' => $result['shift_schedule_id'], 
					'name'            => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}

	public function autocomplete_employee() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/employee');

			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				//'filter_shift_type' => 1,
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_employee->getemployees($data);

			foreach ($results as $result) {
				$json[] = array(
					'employee_id' => $result['employee_id'], 
					'name'            => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}	
}
?>