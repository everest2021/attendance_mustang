<?php    
class ControllerSnippetsResetEmployeeShiftBatch extends Controller { 
	private $error = array();

	public function index() {
		$this->load->model('transaction/transaction');
		$this->load->model('report/common_report');
		$this->load->model('catalog/employee');		
		
		if(isset($this->request->get['filter_month'])){
			$filter_month = $this->request->get['filter_month'];
		} else {
			$filter_month = date('n');
		}

		if(isset($this->request->get['filter_year'])){
			$filter_year = $this->request->get['filter_year'];
		} else {
			$filter_year = date('Y');
		}
		if(isset($this->request->get['filter_location'])){
			$filter_location = $this->request->get['filter_location'];
		} else {
			$filter_location = '';
		}


		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Update Employee Shift'),
			'href'      => $this->url->link('snippets/reset_employee_shift_batch', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$months = array(
			'01' => 'January',
			'02' => 'Feburary',
			'03' => 'March',
			'04' => 'April',
			'05' => 'May',
			'06' => 'June',
			'07' => 'July',
			'08' => 'August',
			'09' => 'September',
			'10' => 'October',
			'11' => 'November',
			'12' => 'December'
		);
		$this->data['months'] = $months;
		
		$years = array(
			'2018' => '2018',
			'2019' => '2019',
			'2020' => '2020',
			'2021' => '2021',
		);
		$this->data['years'] = $years;
		// echo'<pre>';
		// print_r($this->data);
		// exit;

		$this->load->model('catalog/location');



		$departmentss = $this->db->query("SELECT department_id FROM oc_employee WHERE 1=1 AND `shift_type` = 'R' AND `status` = '1' GROUP BY department_id ")->rows;

		foreach ($departmentss as $dkey => $dvalue) {
			
			$department_wise_employess = $this->db->query("SELECT emp_code , chunk_id, employee_id FROM oc_employee WHERE department_id = '".$dvalue['department_id']."' AND `shift_type` = 'R' AND `status` = '1' ");
			if($department_wise_employess->num_rows > 0) {

				$counts = ceil(count($department_wise_employess->rows) / 3) ;

				$chunk_datas = array_chunk($department_wise_employess->rows, $counts);

				// 		echo'<pre>';
				// print_r($chunk_datas);
				// exit;
				foreach ($chunk_datas as $ekey => $evalue) {
					if($ekey == 0) {
						foreach ($evalue as $eskey => $esvalue) {
							$this->db->query("UPDATE oc_employee SET chunk_id = '1' WHERE employee_id = '".$esvalue['employee_id']."' ");
						}
					} 

					if ($ekey == 1) {
						foreach ($evalue as $es1key => $es1value) {
							$this->db->query("UPDATE oc_employee SET chunk_id = '2' WHERE employee_id = '".$es1value['employee_id']."' ");
						}
					} 

					if ($ekey == 2) {
						foreach ($evalue as $es2key => $es2value) {
							$this->db->query("UPDATE oc_employee SET chunk_id = '3' WHERE employee_id = '".$es2value['employee_id']."' ");
						}
					}
				
				
				}

			}
		}


		//transaction table update 

		$all_rotae_emp = $this->db->query("SELECT * FROM oc_employee WHERE `shift_type` = 'R' AND `status` = '1'  ")->rows;
		foreach ($all_rotae_emp as $akey => $avalue) {

			if($avalue['chunk_id'] == 1) {
				$str = 'S1';
			} else if($avalue['chunk_id'] == 2) {
				$str = 'S2';
			} else if($avalue['chunk_id'] == 3) {
				$str = 'S3';

			}

			$exist_emp = $this->db->query("SELECT * FROM oc_transaction WHERE date = '2021-09-30' AND emp_id = '".$avalue['employee_id']."'  ");
			if($exist_emp->num_rows == 0) {
				if($avalue['shift_type'] == 'F'){
					$emp_type = 1;
				} else {
					$emp_type = 2;
				}
				$shift_datas = $this->db->query("SELECT * FROM oc_shift WHERE shift_id = '".$avalue['shift']."' ")->row;
				$this->db->query("INSERT INTO oc_transaction SET emp_type = '".$emp_type."', shift_code_manipulated = '".$str."', shift_id_manipulated = '".$avalue['chunk_id'] ."' , emp_id = '".$avalue['employee_id']."', emp_name = '".$avalue['name']."',  date = '2021-09-30', day = '30', month = '9', year = '2021',department_id = '".$avalue['department_id']."', department = '".$avalue['department']."', unit_id = '".$avalue['unit_id']."', unit = '".$avalue['unit']."', absent_status = '1', absent_status_manipulated = '1', designation = '".$avalue['designation']."', designation_id = '".$avalue['designation_id']."'");
			} else {
				$this->db->query("UPDATE oc_transaction SET shift_code_manipulated = '".$str."', shift_id_manipulated = '".$avalue['chunk_id'] ."' WHERE date = '2021-09-30' AND emp_id = '".$avalue['employee_id']."' ");

			}


		}


				// 	exit;

	

		if(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->data['token'] = $this->session->data['token'];

		$this->data['filter_month'] = $filter_month;
		$this->data['filter_year'] = $filter_year;
		$this->data['filter_location'] = $filter_location;
		
		$this->template = 'catalog/reset_employee_shift_batch.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}


	
}
?>