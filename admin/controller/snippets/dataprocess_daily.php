<?php    
class ControllerSnippetsDataprocessdaily extends Controller { 
	private $error = array();

	public function index() {
		
		/*
		$shift_id = '2';
		$shift_code = 'S2';
		$shift_intime = '16:00:00';
		$shift_outtime = '00:00:00';
		$act_in_punch_date = '2019-01-01';
		$act_out_punch_date = '2019-01-01';
		$svalue['act_intime'] = '08:15:00';
		if($svalue['act_intime'] != "00:00:00"){		
			$in_datetime = $this->get_all_times($shift_intime);
			$in_random = array_rand($in_datetime);
			$act_intime = $in_datetime[$in_random];
			
			$datatime = $this->get_all_timess("08:00:00");
			$random = array_rand($datatime);
			$addtime = $datatime[$random];
		
			$secs = strtotime($act_intime)-strtotime("00:00:00");
			$act_outtime = date("H:i:s",strtotime($addtime)+$secs);
			$act_outtime_hours = date("H",strtotime($addtime)+$secs);
			echo $act_outtime;
			echo '<br />';
			if($act_outtime >= '00' && $act_outtime <= '01'){
				$act_out_punch_date = date('Y-m-d', strtotime($act_out_punch_date . "+1 day"));		
			}
		}
		echo $act_in_punch_date;
		echo '<br />';
		echo $act_out_punch_date;
		echo '<br />';
		exit;

		$shift_id = '3';
		$shift_code = 'S3';
		$shift_intime = '00:00:00';
		$shift_outtime = '08:00:00';
		$act_in_punch_date = '2019-01-01';
		$act_out_punch_date = '2019-01-01';
		$svalue['act_intime'] = '08:15:00';
		if($svalue['act_intime'] != "00:00:00"){		
			$in_datetime = $this->get_all_times($shift_intime);
			$in_random = array_rand($in_datetime);
			$act_intime = $in_datetime[$in_random];
			echo $act_intime;
			echo '<br />';
			$act_intime_hour = date('H', strtotime($act_intime));
			if($act_intime_hour >= '23'){
				$act_in_punch_date = date('Y-m-d', strtotime($act_in_punch_date . "-1 day"));
			}
		}
		echo $act_in_punch_date;
		echo '<br />';
		echo $act_out_punch_date;
		echo '<br />';
		echo 'out';exit;
		*/

		$this->load->model('transaction/transaction');
		$this->load->model('report/common_report');
		$this->load->model('catalog/employee');		
		
		if(isset($this->request->get['filter_month'])){
			$filter_month = $this->request->get['filter_month'];
		} else {
			$filter_month = date('n');
		}

		if(isset($this->request->get['filter_year'])){
			$filter_year = $this->request->get['filter_year'];
		} else {
			$filter_year = date('Y');
		}
		if(isset($this->request->get['filter_location'])){
			$filter_location = $this->request->get['filter_location'];
		} else {
			$filter_location = '';
		}


		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Daily Employee Update Attendance'),
			'href'      => $this->url->link('snippets/dataprocess_daily', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$months = array(
			'01' => 'January',
			'02' => 'Feburary',
			'03' => 'March',
			'04' => 'April',
			'05' => 'May',
			'06' => 'June',
			'07' => 'July',
			'08' => 'August',
			'09' => 'September',
			'10' => 'October',
			'11' => 'November',
			'12' => 'December'
		);
		$this->data['months'] = $months;
		
		$years = array(
			'2018' => '2018',
			'2019' => '2019',
			'2020' => '2020',
			'2021' => '2021',
		);
		$this->data['years'] = $years;
		// echo'<pre>';
		// print_r($this->data);
		// exit;

		$this->load->model('catalog/location');
		$data = array();
		$unit_id = 0;
		$in = 0;
		if($this->user->getUserGroupId() != 1 && $this->user->getUnitId() != '0'){
			$in = 1;
			$unit_id = $this->user->getUnitId();
		}
		$results = $this->model_catalog_location->getLocations($data);
		$unit_datas = array();
		if($in == 0){
			$unit_datas['0'] = 'All';
		}
		foreach($results as $rkey => $rvalue){
			if($in == 1){
				if($unit_id == $rvalue['unit_id']){
					$unit_datas[$rvalue['unit']] = $rvalue['unit'];
				}
			} else {
				$unit_datas[$rvalue['unit']] = $rvalue['unit'];
			}
		}
		$sql = "UPDATE `oc_status` SET `process_status` = '0' ";
				$this->db->query($sql);
		$this->data['unit_datas'] = $unit_datas;
		if(isset($this->request->get['once']) && $this->request->get['once'] == '1'){
			//echo 'aaaa';exit;
			
			$stats_sql = "SELECT * FROM `oc_status` WHERE `process_status` = '1' ";
			$stats_data = $this->db->query($stats_sql);
			// echo '<pre>';
			// print_r($stats_data);
			// exit;
			if($stats_data->num_rows == 0){
				$sql = "UPDATE `oc_status` SET `process_status` = '1' ";
				$this->db->query($sql);
				$batch_ids = $this->db->query("SELECT `batch_id` FROM `oc_transaction` ORDER BY `batch_id` DESC LIMIT 1");
				if($batch_ids->num_rows > 0){
					$batch_id = $batch_ids->row['batch_id'] + 1;
				} else {
					$batch_id = 1;
				}
					
				$start_date = $filter_year.'-'.$filter_month.'-01';
				$end_date = date("Y-m-t", strtotime($start_date));

				//$start_date = '2019-10-01';
				//$end_date = '2019-10-10';
				//echo $start_date;
				// echo '<br />';
				//echo $end_date;
				//exit;
				$day = array();
				$days = $this->GetDays($start_date, $end_date);
				foreach ($days as $dkey => $dvalue) {
					if(strtotime($dvalue) <= strtotime(date('Y-m-d'))){
						$dates = explode('-', $dvalue);
						$day[$dkey]['day'] = $dates[2];
						$day[$dkey]['date'] = $dvalue;
					}
				}
				// echo '<pre>';
				// print_r($day);
				// exit;
				$shift_datas_sql = "SELECT * FROM `oc_shift` WHERE `status` = '1' AND `shift_change` = '1' ";
				$shift_datass = $this->db->query($shift_datas_sql);
				$shift_count = $shift_datass->num_rows;
				//echo $shift_count;exit;
				$shift_datas = $shift_datass->rows;
				$shift_data = array();
				foreach ($shift_datas as $skey => $svalue) {
					$shift_data[$svalue['shift_id']] = array(
						'shift_intime' => $svalue['in_time'],
						'shift_outtime' => $svalue['out_time'],
						'shift_code' => $svalue['shift_code'],
					);
				}

				$counter_shift_change = 1;

				$shift_random_1[0] = '0';
				$shift_random_1[1] = '1';
				$shift_random_1[2] = '2';
				$shift_random_1[3] = '0';

				$shift_random_2[0] = '3';
				$shift_random_2[1] = '1';
				$shift_random_2[2] = '2';
				$shift_random_2[3] = '3';

				$shift_random_3[0] = '2';
				$shift_random_3[1] = '3';
				$shift_random_3[2] = '1';
				$shift_random_3[3] = '2';

				$shift_random_4[0] = '1';
				$shift_random_4[1] = '2';
				$shift_random_4[2] = '3';
				$shift_random_4[3] = '1';

				$delete_re = "TRUNCATE `oc_shift_change`";
				$this->db->query($delete_re);
				
				//$update_empty = "UPDATE `oc_transaction` SET `shift_id` = '0', `shift_code` = '', `shift_intime` = '0', `shift_outtime` = '0', `act_intime` = '00:00:00', `act_outtime` = '00:00:00', `late_time` = '00:00:00', `early_time` = '00:00:00', `working_time` = '00:00:00', `over_time` = '00:00:00', `leave_status` = '0', `absent_status` = '1', `present_status` = '0', `abnormal_status` = '0', `batch_id` = '0', `weekly_off` = '0', `holiday_id` = '0', `halfday_status` = '0', `day_close_status` = '0', `manual_status` = '0', `firsthalf_status` = '0', `secondhalf_status` = '0' WHERE `date` >= '".$start_date."' AND `date` <= '".$end_date."' ";
				$update_empty = "UPDATE `oc_transaction` SET `date_out` = '0000-00-00', `shift_id` = '0', `shift_code` = '', `shift_intime` = '0', `shift_outtime` = '0', `act_intime` = '00:00:00', `act_outtime` = '00:00:00', `late_time` = '00:00:00', `early_time` = '00:00:00', `working_time` = '00:00:00', `over_time` = '00:00:00', `leave_status` = '0', `absent_status` = '1', `present_status` = '0', `abnormal_status` = '0', `batch_id` = '0', `weekly_off` = '0', `holiday_id` = '0', `halfday_status` = '0', `day_close_status` = '0', `manual_status` = '0', `firsthalf_status` = '0', `secondhalf_status` = '0', `act_intime_manipulated` = '00:00:00', `act_outtime_manipulated` = '00:00:00', `working_time_manipulated` = '00:00:00',
				over_time_manipulated = '00:00:00',`firsthalf_status_manipulated` = '0', `secondhalf_status_manipulated` = '0', `present_status_manipulated` = '0', `absent_status_manipulated` = '1', `shift_id_manipulated` = '0', `shift_code_manipulated` = '', `shift_intime_manipulated` = '00:00:00', `shift_outtime_manipulated` = '00:00:00', `date_out_manipulated` = '0000-00-00' WHERE `date` >= '".$start_date."' AND `date` <= '".$end_date."' ";
				//$update_empty = "UPDATE `oc_transaction` SET `act_intime_manipulated` = '00:00:00', `act_outtime_manipulated` = '00:00:00', `working_time_manipulated` = '00:00:00', `firsthalf_status_manipulated` = '0', `secondhalf_status_manipulated` = '0', `present_status_manipulated` = '0', `absent_status_manipulated` = '1', `shift_id_manipulated` = '0', `shift_intime_manipulated` = '00:00:00', `shift_outtime_manipulated` = '00:00:00' WHERE `date` >= '".$start_date."' AND `date` <= '".$end_date."' ";
				//$update_empty = "UPDATE `oc_transaction` SET `shift_id` = '0', `shift_code` = '', `shift_intime` = '0', `shift_outtime` = '0', `act_intime` = '00:00:00', `act_outtime` = '00:00:00', `late_time` = '00:00:00', `early_time` = '00:00:00', `working_time` = '00:00:00', `over_time` = '00:00:00', `leave_status` = '0', `absent_status` = '1', `present_status` = '0', `abnormal_status` = '0', `batch_id` = '0', `weekly_off` = '0', `holiday_id` = '0', `halfday_status` = '0', `day_close_status` = '0', `manual_status` = '0', `firsthalf_status` = '0', `secondhalf_status` = '0' WHERE `date` >= '".$start_date."' AND `date` <= '".$end_date."' AND `manual_status` = '0' AND `emp_id` = '83' ";
				if($filter_location > 0){
					$update_empty .= " AND `unit` = '".$filter_location."' ";
				}
				$this->db->query($update_empty);
				//$this->log->write($update_empty);
				
				/*$mdbFilename = 'D:\AVI INFOTECH\auto backup essl\eTimeTrackLite1.mdb';
				//$mdbFilename = DIR_DOWNLOAD.'eTimeTrackLite1.mdb';
				$user = '';
				$password = '';
				$connection = odbc_connect("Driver={Microsoft Access Driver (*.mdb)};Dbq=$mdbFilename", $user, $password);*/

				//MS SQL SERVER CONNECTION

				// $serverName = "DATABASE-ERP\SP2010";
				// $connectionInfo = array("Database"=>"SmartOfficedb", "UID"=>"avi", "PWD"=>"avi@123");
				// //$connectionInfo = array("Database"=>"SmartOffice");
				// $conn1 = sqlsrv_connect($serverName, $connectionInfo);
				// if($conn1) {
				// 	//echo "Connection established.<br />";exit;
				// }else{
				// 	//echo "Connection could not be established.<br />";
				// 	$dat['connection'] = 'Connection could not be established';
				// 	// echo '<pre>';
				// 	// print_r(sqlsrv_errors());
				// 	// exit;
				// 	// die( print_r( sqlsrv_errors(), true));
				// }

				$serverName = "DESKTOP-8RFC6LP\ERICSIR";
				//$connectionInfo = array("Database"=>"SmartOfficedb", "UID"=>"sa", "PWD"=>"eric@123");
				$connectionInfo = array("Database"=>"SmartOfficedb");
				$conn1 = sqlsrv_connect($serverName, $connectionInfo);
				if($conn1) {
					//echo "Connection established.<br />";exit;
				}else{
					//echo "Connection could not be established.<br />";
					// $dat['connection'] = 'Connection could not be established';
					// echo '<pre>';
					// print_r(sqlsrv_errors());
					// exit;
					// die( print_r( sqlsrv_errors(), true));
				}

				$sql_emp = "SELECT * FROM `" . DB_PREFIX . "employee` WHERE 1= 1";
				//$sql_emp = "SELECT * FROM `" . DB_PREFIX . "employee` WHERE 1= 1 AND emp_code = '270'";//main query imp
				$query = $this->db->query($sql_emp);
				$emp_data_masters = $query->rows;	
				$process_emp_master_data = array();
				$employee_lists_master = '';
				foreach($emp_data_masters as $emp_data_master) {
					$process_emp_master_data[$emp_data_master['emp_code']] = $emp_data_master;
					$employee_lists_master .= $emp_data_master['emp_code'] .',';
				}
				$all_employee_code = rtrim($employee_lists_master,',');
				//$sql = "SELECT * FROM Employees WHERE 1=1 AND Location = '8' AND CompanyId = '2' ";
				$emp_array = array();
				$sql = "SELECT * FROM Employees WHERE 1=1 AND Location IN ('8','7') AND CompanyId = '2' AND EmployeeCode IN (".$all_employee_code.")";
				$params = array();
				$options =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
				$stmt = sqlsrv_query($conn1, $sql, $params, $options);
				if( $stmt === false) {
				   // echo'<pre>';
				   // print_r(sqlsrv_errors());
				   // exit;
				}
				$employee_array_with_code = array();
				$employee_lists = '';
				while($rows = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)){
					$emp_array[] = $rows;
					$employee_array_with_code[$rows['EmployeeCode']] = $rows['EmployeeId'] ;
				}
				if(!empty($process_emp_master_data)){
					foreach ($process_emp_master_data as $ekey => $evalue) {
						if(isset($employee_array_with_code[$ekey])){
							$employee_lists .= $evalue['employee_id'] .',';
						}
					}
				}

				
				$employee_list = '0';
				$emp_transection_array = array();
				if($employee_lists != ''){
					$employee_list = rtrim($employee_lists,',');
					$trans_exist_sql = $this->db->query("SELECT * FROM `oc_transaction` WHERE `emp_id` IN (".$employee_list.") AND `date` >= '".$start_date."' AND `date` <= '".$end_date."' ");
					if($trans_exist_sql->num_rows > 0){
						foreach ($trans_exist_sql->rows as $tkey => $tvalue) {
							$emp_transection_array[$tvalue['emp_id']][$tvalue['date']] = $tvalue['transaction_id'];
						}
					}
				}


				//echo "<pre>";print_r($emp_transection_array);exit;
				//$sql1 = "SELECT * FROM AttendanceLogs WHERE EmployeeId = ".$emp_id." AND AttendanceDate = '".$filter_date_start."' ";
				$week_off_value = $this->model_report_common_report->get_setting('week_off');
				$holiday_value = $this->model_report_common_report->get_setting('holiday');
											
				$sql_multiple_query = '';
				$itr = 1;
				$sql1 = "SELECT * FROM AttendanceLogs WHERE CAST(AttendanceDate AS DAte) >= '".$start_date."' AND CAST(AttendanceDate AS DAte) <= '".$end_date."' ";
				$params1 = array();
				$options1 =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
				$stmt1 = sqlsrv_query($conn1, $sql1, $params1, $options1);
				if($stmt1 === false) {
				   //echo'<pre>';
				   //print_r(sqlsrv_errors());
				   //exit;
				}
				$attendance_array = array();
				while($atten_row = sqlsrv_fetch_array($stmt1, SQLSRV_FETCH_ASSOC)) {
					$attendance_dates_object = 	$atten_row['AttendanceDate']->format("Y-m-d");
					$attendance_date =  date("Y-m-d", strtotime($attendance_dates_object));  
					$attendance_array[$atten_row['EmployeeId']][$attendance_date] = $atten_row;
				}
				foreach($day as $dkeys => $dvalues){
					$filter_date_start = $dvalues['date'];
					/*$sql1 = "SELECT * FROM AttendanceLogs WHERE AttendanceDate = '".$filter_date_start."' ";
					$params1 = array();
					$options1 =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
					$stmt1 = sqlsrv_query($conn1, $sql1, $params1, $options1);
					if($stmt1 === false) {
					   //echo'<pre>';
					   //print_r(sqlsrv_errors());
					   //exit;
					}
					$attendance_array = array();
					while($atten_row = sqlsrv_fetch_array($stmt1, SQLSRV_FETCH_ASSOC)) {	
						$attendance_array[$atten_row['EmployeeId']] = $atten_row;
					}*/
										/*if(!empty($filter_location)){
						$sql .= "AND Location = '".$filter_location."' ";
					}*/


					foreach($emp_array as $row){
						$emp_id = $row['EmployeeId'];
						$emp_codes = $row["EmployeeCode"];
						$emp_code = $emp_codes;
						// echo $emp_id;
						// echo "<br>";
						// echo $emp_code;
						// echo "<pre>";
						// print_r($attendance_array);
						// exit;
						//echo $sql1;exit;
						
						$rev_exp_datas = array();
						$found_emp_code = array();
						if(isset($attendance_array[$emp_id][$filter_date_start])) {
							
							$row1 = $attendance_array[$emp_id][$filter_date_start];	
								// echo "<pre>";
								// print_r($row1);
								// exit;
								
								$atten_log_data = $row1['AttendanceDate'];
								$attend_date = $atten_log_data->format("Y-m-d");
								//echo $attend_date; exit;
								if($attend_date != '1900-01-01' && $attend_date != '1970-01-01' && $attend_date != '0000-00-00'){
									/*$emp_id = $row['EmployeeId'];
									$sql1 = "SELECT * FROM Employees WHERE EmployeeId = ".$emp_id." ";
									$params1 = array();
									$options1 =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
									$stmt1 = sqlsrv_query($conn1, $sql1, $params1, $options1);
									if($stmt1 === false) {
									   //echo'<pre>';
									   //print_r(sqlsrv_errors());
									   //exit;
									}
									$emp_code = 0;
									while($row1 = sqlsrv_fetch_array($stmt1, SQLSRV_FETCH_ASSOC)){
										$emp_codes = $row1['EmployeeCode'];
										$emp_code = $emp_codes;
									}
									if($emp_code != 0){
										// echo '<pre>';
										// print_r($row);
										// exit;
										$out_log_data = $row['OutTimeDateTime'];
										$out_time = $out_log_data->format("H:i:s");
										$out_date = $out_log_data->format("Y-m-d");
										if($out_date == '1900-01-01' || $out_date == '1970-01-01' || $out_date == '0000-00-00'){
											$out_date = $attend_date;
										}
										$in_log_data = $row['InTimeDateTime'];
										$in_time = $in_log_data->format("H:i:s");
										$in_date = $in_log_data->format("Y-m-d");*/

										$in_log_data = $row1['InTimeDateTime'];
										$in_date = $in_log_data->format("Y-m-d");
										$in_time = $in_log_data->format("H:i:s");
										if($in_date != '1900-01-01' && $in_date != '1970-01-01' && $in_date != '0000-00-00'){
										} else {
											$in_date = '0000-00-00';
											$in_time = '00:00:00';
										}

										$out_log_data = $row1['OutTimeDateTime'];
										$out_date = $out_log_data->format("Y-m-d");
										$out_time = $out_log_data->format("H:i:s");

										if($out_date != '1900-01-01' && $out_date != '1970-01-01' && $out_date != '0000-00-00'){
										} else {
											$out_date = '0000-00-00';
											$out_time = '00:00:00';
										}

										// $emp_datas = $this->model_transaction_transaction->getempid_by_empcode($emp_code);
										// if(isset($emp_datas['employee_id'])){
										// 	$employee_id = $emp_datas['employee_id'];
										// } else {
										// 	$employee_id = 0;
										// }
										// $result = $this->model_transaction_transaction->getEmployees_dat($employee_id);
										

										if(isset($process_emp_master_data[$emp_code])){
											$result = $process_emp_master_data[$emp_code];
											$sql2 = "SELECT * FROM Shifts WHERE ShiftId = ".$row1['ShiftId']." ";
											$params2 = array();
											$options2 =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
											$stmt2 = sqlsrv_query($conn1, $sql2, $params2, $options2);
											if($stmt2 === false) {
											   // echo'<pre>';
											   // print_r(sqlsrv_errors());
											   // exit;
											}
											$shift_intime = '00:00:00';
											$shift_outtime = '00:00:00';
											$shift_id = $row1['ShiftId'];
											$shift_code = 'NS';
											$in = 0;
											if($result['shift_type'] == 'F'){
												$emp_type = 1;
											} else {
												$emp_type = 2;
											}
											while($row2 = sqlsrv_fetch_array($stmt2, SQLSRV_FETCH_ASSOC)) {
												if($row2["BeginTime"] != '00:00'){
													$in = 1;
													$shift_intime = $row2["BeginTime"];
												}
												if($row2["BeginTime"] != '00:00'){
													$in = 1;
													$shift_outtime = $row2["EndTime"];
												}
												if($in == 1) {
													// if($emp_type == 1){
													// 	$is_exist = $this->db->query("SELECT * FROM `oc_shift` WHERE `in_time` = '".$shift_intime."' AND `out_time` = '".$shift_outtime."' ");
													// } else {
														$is_exist = $this->db->query("SELECT * FROM `oc_shift` WHERE `in_time` = '".$shift_intime."' AND `out_time` = '".$shift_outtime."' ");
													//}
													if($is_exist->num_rows == 0){
														$shift_name = $shift_intime.' : '.$shift_outtime;
														$shift_insert = "INSERT INTO `oc_shift` SET `name` = '".$shift_name."', `in_time` = '".$shift_intime."', `out_time` = '".$shift_outtime."', `shift_code` = 'GS', `status` = '0' ";
														$this->db->query($shift_insert);
														$shift_id = $this->db->getLastId(); 
														$shift_code = 'GS';
													} else {
														if($is_exist->num_rows == 1){
															$shift_id = $is_exist->row['shift_id'];
															$shift_code = $is_exist->row['shift_code'];
														} else {
															if($emp_type == 1){
																$is_exist = $this->db->query("SELECT * FROM `oc_shift` WHERE `in_time` = '".$shift_intime."' AND `out_time` = '".$shift_outtime."' AND `shift_change` = '0' ");
															} else {
																$is_exist = $this->db->query("SELECT * FROM `oc_shift` WHERE `in_time` = '".$shift_intime."' AND `out_time` = '".$shift_outtime."' AND `shift_change` = '1' ");
															}
															if($is_exist->num_rows > 0){			
																$shift_id = $is_exist->row['shift_id'];
																$shift_code = $is_exist->row['shift_code'];
															}
														}
													}
												}	
											}
											$weekly_off = 0;
											$holiday_id = 0;
											$leave_status = 0;
											$present_status = 0;
											$absent_status = 0;
											$halfday_status = 0;
											$first_half = '';
											$second_half = '';
											$found_emp_code[$emp_code] = $emp_code;
											$emp_name = $result['name'];
											$employee_id = $result['employee_id'];
											$department = $result['department'];
											$department_id = $result['department_id'];
											$unit = $result['unit'];
											$unit_id = $result['unit_id'];
											$designation = $result['designation'];
											$designation_id = $result['designation_id'];
											$act_intime = $in_time;
											
											$act_outtime = $out_time;
											$date_out = $out_date;
											$date = $in_date;
											$day = date('j', strtotime($attend_date));
											$month = date('n', strtotime($attend_date));
											$year = date('Y', strtotime($attend_date));
											$working_time = '00:00:00';
											if($act_outtime != '00:00:00'){//for getting working time
												// if($date != $date_out){
												// 	$start_date = new DateTime($date.' '.$act_intime);
												// 	$since_start = $start_date->diff(new DateTime($date_out.' '.$act_outtime));
												// 	$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
												// } else {
													$date_out =  date('Y-m-d', strtotime($date . ' +1 day'))	;
												    $start_date = new DateTime($date.' '.$act_intime);
													$since_start = $start_date->diff(new DateTime($date_out.' '.$act_outtime));
													$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
												//}
											} else {
												$working_time = $this->convertToHoursMins($row1['Duration']);
											}
											$late_time = $this->convertToHoursMins($row1['LateBy']);
											$early_time = $this->convertToHoursMins($row1['EarlyBy']);
											$over_time = $this->convertToHoursMins($row1['OverTime']);
											$present_status = $row1['Present'];
											$absent_status = $row1['Absent'];
											if($present_status == '1'){
												$first_half = '1';
												$second_half = '1';
											}
											if($absent_status == '1'){
												$first_half = '0';
												$second_half = '0';
											}

											if($row1['Status'] == '1/2Present' || $row1['Status'] == '1/2Present On WeeklyOff' || $row1['Status'] == '1/2Present On Leave(PL)' || $row1['Status'] == '1/2Present On Leave(AL)' || $row1['Status'] == 'on leave(½AL)' || $row1['Status'] == '½P' || $row1['Status'] == '1/2Present On Holiday' || $row1['Status'] == '1/2Present On Holiday'|| $row1['Status'] == '1/2Present On Leave(½AL)'){
												$first_half = '1/2P';
												$second_half = '1/2P';
												$halfday_status = 1;
											}
											
											if($week_off_value == 1){
												$shift_schedule_data = $this->model_report_common_report->get_shift_schedule_data($employee_id, $filter_date_start);
												if($shift_schedule_data[0] == 'W'){
													$weekly_off = 1;
													$first_half = 'WO';							
													$second_half = 'WO';
												}
											} else {
												if($row1['WeeklyOff'] != '0'){
													$weekly_off = 1;
													$first_half = 'WO';							
													$second_half = 'WO';
													// if($act_intime != '00:00:00' && $act_outtime != '00:00:00' && $department_id == '12'){
													// 	$first_half = 'WOFFP';							
													// 	$second_half = 'WOFFP';
													// }							
												}	
											}
											
											if($holiday_value == 1){
												$shift_schedule_data = $this->model_report_common_report->get_shift_schedule_data($employee_id, $filter_date_start);
												if($shift_schedule_data[0] == 'H'){
													$week_off = 0;
													$holiday_id = 1;
													$first_half = 'HO';							
													$second_half = 'HO';							
												}
											} else {
												if($row1['Holiday'] != '0'){
													$week_off = 0;
													$holiday_id = 1;
													$first_half = 'HO';							
													$second_half = 'HO';							
												}
											}
											if($row1['LeaveType'] != ''){
												if($row1['LeaveType'] == 'AL'){
													$holiday_id = 0;
													$week_off = 0;
													$leave_status = 1;
													$first_half = 'AL';							
													$second_half = 'AL';
												} else {
													$holiday_id = 0;
													$week_off = 0;
													$leave_status = 1;
													//$first_half = 'PL';							
													//$second_half = 'PL';
													$first_half = $row1['LeaveType'];
													$second_half = $row1['LeaveType'];
												}							
											}
											// echo "<pre>";
											// print_r('manupulated_codessssss');
											// echo "<pre>";
											// print_r('present_status_m');
											// print_r($present_status_m);
											// print_r('absent_status_m');
											// print_r($absent_status_m);
											// echo "<br>";


											
											if(isset($emp_transection_array[$employee_id][$attend_date])){	
												$transaction_id_master = $emp_transection_array[$employee_id][$attend_date];								
												$sql = "UPDATE `oc_transaction` SET `emp_id` = '".$employee_id."', `emp_name` = '".$emp_name."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', over_time = '".$over_time."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$attend_date."', `date_out` = '".$date_out."', `department` = '".$department."', `department_id` = '".$department_id."', `unit` = '".$unit."', `unit_id` = '".$unit_id."', `designation` = '".$designation."', `designation_id` = '".$designation_id."', `group` = '', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `weekly_off` = '".$weekly_off."', `holiday_id` = '".$holiday_id."', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '0', `emp_type` = '".$emp_type."', `leave_status` = '".$leave_status."' ,`halfday_status` = '".$halfday_status."' WHERE `transaction_id` = '".$transaction_id_master."' ";
													$itr++;
													$this->db->query($sql);
													//$sql_multiple_query = $sql_multiple_query.$sql;
													//echo $sql; exit;
												//$this->log->write($sql);
											} else {
													$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$employee_id."', `emp_name` = '".$emp_name."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', over_time = '".$over_time."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$attend_date."', `date_out` = '".$date_out."', `department` = '".$department."', `department_id` = '".$department_id."', `unit` = '".$unit."', `unit_id` = '".$unit_id."', `designation` = '".$designation."', `designation_id` = '".$designation_id."', `group` = '', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `weekly_off` = '".$weekly_off."', `holiday_id` = '".$holiday_id."', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '0', `emp_type` = '".$emp_type."', `leave_status` = '".$leave_status."'  ,`halfday_status` = '".$halfday_status."'";
													$itr++;
												//$sql_multiple_query = $sql_multiple_query.$sql;
												//echo $sql;
												$this->db->query($sql);
											}
											//echo $sql_multiple_query; exit;
										}
									//}
								}
							
						}	
						//$this->log->write('---Employee End EmployeeCode : '.$emp_code.'----'.' And Date : ' . $filter_date_start.'---');
					}
					
					//$filter_unit = '';
					//$this->model_transaction_dayprocess->update_close_day($filter_date_start, $filter_unit);
				}//exit;
				$sql = "UPDATE `oc_status` SET `process_status` = '0'; ";
				$sql_multiple_query = $sql_multiple_query.$sql;
				$con=mysqli_connect("localhost","root","","db_attendance_mustang");
				mysqli_multi_query($con,$sql_multiple_query);
				mysqli_close($con);
				//exit;
				
				//$this->db->query($sql);
				$this->session->data['success'] = 'Employee Attendance Process Done';
				$this->redirect($this->url->link('snippets/dataprocess_daily', 'token=' . $this->session->data['token'], 'SSL'));
			} else {
				$this->session->data['warning'] = 'Attendance Process Already Running';
				$this->redirect($this->url->link('snippets/dataprocess_daily', 'token=' . $this->session->data['token'], 'SSL'));
			}
			
		}

		if(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->data['token'] = $this->session->data['token'];

		$this->data['filter_month'] = $filter_month;
		$this->data['filter_year'] = $filter_year;
		$this->data['filter_location'] = $filter_location;
		
		$this->template = 'catalog/dataprocess_daily.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	public function get_all_times($time){
		date_default_timezone_set("Asia/Kolkata");
		$minus_start_time = date('H:i:s', strtotime($time . "-15 minutes"));
		$time_array = array();
		for($i=0; $i<15; $i++){
			$time_array[] = date('H:i:s', strtotime($minus_start_time . "+".$i." minutes"));
		}
		for($i=0; $i<=30; $i++){
			$time_array[] = date('H:i:s', strtotime($time . "+".$i." minutes"));
		}
		return $time_array;		
	}

	public function get_all_timess($time){
		for($i=0; $i<=15; $i++){
			$time_array[] = date('H:i:s', strtotime($time . "+".$i." minutes"));
		}
		return $time_array;
	}

	public function generate_random($length, $max_value){
		$randarray = array(); 
		for($i = 1; $i <= $length;) { 
		    unset($rand); 
		    $rand = rand(1, $max_value); 
		    $rand = $rand - 1;
		    if(!in_array($rand, $randarray)) { 
		        $randarray[] = $rand; 
		        $i++; 
		    } 
		}
		return $randarray;
	}

	public function convertToHoursMins($time, $format = '%02d:%02d') {
	    if ($time < 1) {
	        return;
	    }
	    $hours = floor($time / 60);
	    $minutes = ($time % 60);
	    return sprintf($format, $hours, $minutes);
	}

	public function sortByOrder($a, $b) {
		$v1 = strtotime($a['fdate']);
		$v2 = strtotime($b['fdate']);
		return $v1 - $v2; // $v2 - $v1 to reverse direction
		// if ($a['punch_date'] == $b['punch_date']) {
			//return ($a['in_time'] > $b['in_time']) ? -1 : 1;;
		//}
		//return $a['punch_date'] - $b['punch_date'];
	}

	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	public function array_sort($array, $on, $order=SORT_ASC){

		$new_array = array();
		$sortable_array = array();

		if (count($array) > 0) {
			foreach ($array as $k => $v) {
				if (is_array($v)) {
					foreach ($v as $k2 => $v2) {
						if ($k2 == $on) {
							$sortable_array[$k] = $v2;
						}
					}
				} else {
					$sortable_array[$k] = $v;
				}
			}

			switch ($order) {
				case SORT_ASC:
					asort($sortable_array);
					break;
				case SORT_DESC:
					arsort($sortable_array);
					break;
			}

			foreach ($sortable_array as $k => $v) {
				$new_array[$k] = $array[$k];
			}
		}

		return $new_array;
	}

	public function resetprocess(){
		$sql = "UPDATE `oc_status` SET `process_status` = '0' ";
		$this->db->query($sql);

		$this->session->data['success'] = 'Process Reset Sucessfully';
		$this->redirect($this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'));	
	}

	function create_time_range($start, $end, $interval = '30 mins', $format = '12') {
	    $startTime = strtotime($start); 
	    $endTime   = strtotime($end);
	    $returnTimeFormat = ($format == '12')?'g:i:s':'G:i:s';

	    $current   = time(); 
	    $addTime   = strtotime('+'.$interval, $current); 
	    $diff      = $addTime - $current;

	    $times = array(); 
	    while ($startTime < $endTime) { 
	        $times[] = date($returnTimeFormat, $startTime); 
	        $startTime += $diff; 
	    } 
	    $times[] = date($returnTimeFormat, $startTime); 
	    return $times; 
	}
}
?>