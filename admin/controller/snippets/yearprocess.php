<?php    
class ControllerSnippetsYearprocess extends Controller { 
	private $error = array();

	public function index() {
		$filter_year_1 = '2020';
		$filter_year = '2021';
		$year_start_date = $filter_year.'-01-01';
		$months_array = array(
			'1' => '1',
			'2' => '2',
			'3' => '3',
			'4' => '4',
			'5' => '5',
			'6' => '6',
			'7' => '7',
			'8' => '8',
			'9' => '9',
			'10' => '10',
			'11' => '11',
			'12' => '12',
		);
		$emp_datas = $this->db->query("SELECT `employee_id`, `emp_code`, `unit`, `doj`, `dol`, `department`, `name`, `shift_type` FROM `oc_employee` WHERE 1=1 AND `status` = '1' AND (`dol` = '0000-00-00' OR `dol` > '".$year_start_date."') ORDER BY `emp_code` ")->rows;
		// echo '<pre>';
		// print_r($emp_datas);
		// exit;
		foreach($emp_datas as $ekey => $evalue){
			if($evalue['shift_type'] == 'F'){
				$shift_id = '6';
			} else {
				$shift_id = '1';
			}
			$shift_id_data = 'S_'.$shift_id;
			$in = 0;
			foreach ($months_array as $key => $value) {
				if($key == 12){
					$is_exist = $this->db->query("SELECT `emp_code`, `id` FROM `oc_shift_schedule` WHERE `emp_code` = '".$evalue['employee_id']."' AND `month` = '".$key."' AND `year` = '".$filter_year_1."' ");
					if($is_exist->num_rows == 0){
						$in = 1;
						$insert = "INSERT INTO `oc_shift_schedule` SET 
							`emp_code` = '".$evalue['employee_id']."',
							`1` = '".$shift_id_data."',
							`2` = '".$shift_id_data."',
							`3` = '".$shift_id_data."',
							`4` = '".$shift_id_data."',
							`5` = '".$shift_id_data."',
							`6` = '".$shift_id_data."',
							`7` = '".$shift_id_data."',
							`8` = '".$shift_id_data."',
							`9` = '".$shift_id_data."',
							`10` = '".$shift_id_data."',
							`11` = '".$shift_id_data."',
							`12` = '".$shift_id_data."', 
							`13` = '".$shift_id_data."', 
							`14` = '".$shift_id_data."', 
							`15` = '".$shift_id_data."', 
							`16` = '".$shift_id_data."', 
							`17` = '".$shift_id_data."', 
							`18` = '".$shift_id_data."', 
							`19` = '".$shift_id_data."', 
							`20` = '".$shift_id_data."', 
							`21` = '".$shift_id_data."', 
							`22` = '".$shift_id_data."', 
							`23` = '".$shift_id_data."', 
							`24` = '".$shift_id_data."', 
							`25` = '".$shift_id_data."', 
							`26` = '".$shift_id_data."', 
							`27` = '".$shift_id_data."', 
							`28` = '".$shift_id_data."', 
							`29` = '".$shift_id_data."', 
							`30` = '".$shift_id_data."', 
							`31` = '".$shift_id_data."',  
							`month` = '".$key."',
							`year` = '".$filter_year."',
							`status` = '1' ,
							`unit` = '".$evalue['unit']."' "; 
							// echo $insert1;
							// echo '<br />';
						$this->db->query($insert);				
					}
				}

				$is_exist = $this->db->query("SELECT `emp_code`, `id` FROM `oc_shift_schedule` WHERE `emp_code` = '".$evalue['employee_id']."' AND `month` = '".$key."' AND `year` = '".$filter_year."' ");
				if($is_exist->num_rows == 0){
					$insert = "INSERT INTO `oc_shift_schedule` SET 
						`emp_code` = '".$evalue['employee_id']."',
						`1` = '".$shift_id_data."',
						`2` = '".$shift_id_data."',
						`3` = '".$shift_id_data."',
						`4` = '".$shift_id_data."',
						`5` = '".$shift_id_data."',
						`6` = '".$shift_id_data."',
						`7` = '".$shift_id_data."',
						`8` = '".$shift_id_data."',
						`9` = '".$shift_id_data."',
						`10` = '".$shift_id_data."',
						`11` = '".$shift_id_data."',
						`12` = '".$shift_id_data."', 
						`13` = '".$shift_id_data."', 
						`14` = '".$shift_id_data."', 
						`15` = '".$shift_id_data."', 
						`16` = '".$shift_id_data."', 
						`17` = '".$shift_id_data."', 
						`18` = '".$shift_id_data."', 
						`19` = '".$shift_id_data."', 
						`20` = '".$shift_id_data."', 
						`21` = '".$shift_id_data."', 
						`22` = '".$shift_id_data."', 
						`23` = '".$shift_id_data."', 
						`24` = '".$shift_id_data."', 
						`25` = '".$shift_id_data."', 
						`26` = '".$shift_id_data."', 
						`27` = '".$shift_id_data."', 
						`28` = '".$shift_id_data."', 
						`29` = '".$shift_id_data."', 
						`30` = '".$shift_id_data."', 
						`31` = '".$shift_id_data."',  
						`month` = '".$key."',
						`year` = '".$filter_year."',
						`status` = '1' ,
						`unit` = '".$evalue['unit']."' "; 
						// echo $insert1;
						// echo '<br />';
					$this->db->query($insert);				
				} else {
					$update = "UPDATE `oc_shift_schedule` SET 
						`emp_code` = '".$evalue['employee_id']."',
						`1` = '".$shift_id_data."',
						`2` = '".$shift_id_data."',
						`3` = '".$shift_id_data."',
						`4` = '".$shift_id_data."',
						`5` = '".$shift_id_data."',
						`6` = '".$shift_id_data."',
						`7` = '".$shift_id_data."',
						`8` = '".$shift_id_data."',
						`9` = '".$shift_id_data."',
						`10` = '".$shift_id_data."',
						`11` = '".$shift_id_data."',
						`12` = '".$shift_id_data."', 
						`13` = '".$shift_id_data."', 
						`14` = '".$shift_id_data."', 
						`15` = '".$shift_id_data."', 
						`16` = '".$shift_id_data."', 
						`17` = '".$shift_id_data."', 
						`18` = '".$shift_id_data."', 
						`19` = '".$shift_id_data."', 
						`20` = '".$shift_id_data."', 
						`21` = '".$shift_id_data."', 
						`22` = '".$shift_id_data."', 
						`23` = '".$shift_id_data."', 
						`24` = '".$shift_id_data."', 
						`25` = '".$shift_id_data."', 
						`26` = '".$shift_id_data."', 
						`27` = '".$shift_id_data."', 
						`28` = '".$shift_id_data."', 
						`29` = '".$shift_id_data."', 
						`30` = '".$shift_id_data."', 
						`31` = '".$shift_id_data."',   
						`month` = '".$key."',
						`year` = '".$filter_year."',
						`status` = '1' ,
						`unit` = '".$evalue['unit']."' 
						WHERE `id` = '".$is_exist->row['id']."' ";
						// echo $insert1;
						// echo '<br />';
					$this->db->query($update);
				}
			}
			/*
			$current_year = $filter_year;
			$next_year = $filter_year + 1;
			if($in == 1){
				$start = new DateTime($filter_year_1.'-12-01');
			} else {
				$start = new DateTime($current_year.'-01-01');
			}
			$end   = new DateTime($next_year.'-01-01');
			$interval = DateInterval::createFromDateString('1 day');
			$period = new DatePeriod($start, $interval, $end);
			$week_array = array();
			foreach ($period as $dt) {
			    if ($dt->format('N') == 6 || $dt->format('N') == 7) {
			    	$week_array[$dt->format('Y-m-d')]['date'] = $dt->format('Y-m-d');
			    	$week_array[$dt->format('Y-m-d')]['day'] = $dt->format('j');
			    	$week_array[$dt->format('Y-m-d')]['month'] = $dt->format('n');
			    	$week_array[$dt->format('Y-m-d')]['year'] = $dt->format('Y');
			    }
			}

			foreach ($week_array as $wkey => $wvalue) {
				$sql = "UPDATE `oc_shift_schedule` SET `".$wvalue['day']."` = 'W_1_1' WHERE `month` = '".$wvalue['month']."' AND `year` = '".$wvalue['year']."' AND `emp_code` = '".$evalue['employee_id']."' ";	
	        	$this->db->query($sql);
			}
			*/
			/*
			if($evalue['doj'] == '0000-00-00'){
				$doj_compare = $year_start_date;
			} else {
				$doj_compare = $evalue['doj'];
			}	
			$holiday_sql = "SELECT * FROM `oc_holiday` WHERE `date` >= '".$doj_compare."' AND `date` >= '".$year_start_date."' ";
			$holiday_datas = $this->db->query($holiday_sql);
			if($holiday_datas->num_rows > 0){
				$holiday_data = $holiday_datas->row;
				$location_value = '';
				if($evalue['unit'] == 'Unit 31'){
					$location_value = $holiday_data['department_mumbai'];
				}
				$location_value_1 = unserialize($location_value);
				$department = html_entity_decode(strtolower(trim($evalue['department'])));
				if(in_array($department, $location_value_1)){
					$date = $holiday_data['date'];
					$month = date('n', strtotime($date));
					$year = date('Y', strtotime($date));
					$day = date('j', strtotime($date));
					$holiday_string = 'H_'.$hvalue['holiday_id'].'_1';
					$update_shift_schedule = "UPDATE `oc_shift_schedule` SET `".$day."` = '".$holiday_string."' WHERE `month` = '".$month."' AND `year` = '".$year."' AND `emp_code` = '".$evalue['employee_id']."' ";
					$this->db->query($update_shift_schedule);
				}
			}
			*/
			/*
			$is_exist = $this->db->query("SELECT `emp_id`, `leave_id` FROM `oc_leave` WHERE `emp_id` = '".$evalue['employee_id']."' AND `year` = '".$filter_year."' ");
			if($is_exist->num_rows == 0){
				$update_sql = "UPDATE `oc_leave` SET `close_status` = '1' WHERE `emp_id` = '".$evalue['employee_id']."' AND `year` = '".$filter_year_1."' ";
				$this->db->query($update_sql);

				$insert2 = "INSERT INTO `oc_leave` SET 
							`emp_id` = '".$evalue['employee_id']."', 
							`emp_name` = '".$evalue['name']."', 
							`emp_doj` = '".$evalue['doj']."', 
							`year` = '".$filter_year."',
							`close_status` = '0' "; 
				$this->db->query($insert2);
			}
			*/
			//echo 'out';exit;
		}
		echo 'Done';
		exit;
	}

	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	public function array_sort($array, $on, $order=SORT_ASC){

	    $new_array = array();
	    $sortable_array = array();

	    if (count($array) > 0) {
	        foreach ($array as $k => $v) {
	            if (is_array($v)) {
	                foreach ($v as $k2 => $v2) {
	                    if ($k2 == $on) {
	                        $sortable_array[$k] = $v2;
	                    }
	                }
	            } else {
	                $sortable_array[$k] = $v;
	            }
	        }

	        switch ($order) {
	            case SORT_ASC:
	                asort($sortable_array);
	                break;
	            case SORT_DESC:
	                arsort($sortable_array);
	                break;
	        }

	        foreach ($sortable_array as $k => $v) {
	            $new_array[$k] = $array[$k];
	        }
	    }

	    return $new_array;
	}
}
?>