<?php    
class ControllerSnippetsDataprocess extends Controller { 
	private $error = array();

	public function index() {
		$this->load->model('transaction/transaction');
		$this->load->model('report/common_report');
		$this->load->model('catalog/employee');		
		
		if(isset($this->request->get['filter_month'])){
			$filter_month = $this->request->get['filter_month'];
		} else {
			$filter_month = date('n');
		}

		if(isset($this->request->get['filter_year'])){
			$filter_year = $this->request->get['filter_year'];
		} else {
			$filter_year = date('Y');
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('snippets/dataprocess', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$months = array(
			'01' => 'January',
			'02' => 'Feburary',
			'03' => 'March',
			'04' => 'April',
			'05' => 'May',
			'06' => 'June',
			'07' => 'July',
			'08' => 'August',
			'09' => 'September',
			'10' => 'October',
			'11' => 'November',
			'12' => 'December'
		);
		$this->data['months'] = $months;
		
		$years = array(
			'2017' => '2017',
			'2018' => '2018',
			'2019' => '2019',
		);
		$this->data['years'] = $years;

		if(isset($this->request->get['once']) && $this->request->get['once'] == '1'){
			$batch_ids = $this->db->query("SELECT `batch_id` FROM `oc_transaction` ORDER BY `batch_id` DESC LIMIT 1");
			if($batch_ids->num_rows > 0){
				$batch_id = $batch_ids->row['batch_id'] + 1;
			} else {
				$batch_id = 1;
			}
				
			// $device_last_dates = $this->db->query("SELECT `date` FROM `oc_transaction` WHERE `emp_type` = '1' AND `day_close_status` = '1' ORDER BY `date` DESC LIMIT 1");
			// if($device_last_dates->num_rows > 0){
			// 	$device_last_date = $device_last_dates->row['date'];
			// } else {
			// 	$device_last_date = '2017-03-31';
			// }
			$start_date = $filter_year.'-'.$filter_month.'-01';
			$end_date = date("Y-m-t", strtotime($start_date));
			// echo $start_date;
			// echo '<br />';
			// echo $end_date;
			// exit;
			$update_empty = "UPDATE `oc_transaction` SET `act_intime` = '00:00:00', `act_outtime` = '00:00:00', `late_time` = '00:00:00', `early_time` = '00:00:00', `working_time` = '00:00:00', `leave_status` = '0', `absent_status` = '1', `present_status` = '0', `abnormal_status` = '0', `batch_id` = '0', `outtime` = '00:00:00', `duration` = '00:00:00', `over_time` = '00:00:00', `overtime` = '00:00:00', `early_time_process` = '00:00:00', `weekly_off` = '0', `holiday_id` = '0', `halfday_status` = '0', `day_close_status` = '0', `manual_status` = '0', `firsthalf_status` = '0', `secondhalf_status` = '0' WHERE `emp_type` = '1' AND `date` >= '".$start_date."' ";
			//echo $update_empty;exit;
			$this->db->query($update_empty);

			$day = array();
			$days = $this->GetDays($start_date, $end_date);
			foreach ($days as $dkey => $dvalue) {
				$dates = explode('-', $dvalue);
				$day[$dkey]['day'] = $dates[2];
				$day[$dkey]['date'] = $dvalue;
			}
			/*
			if(strtotime($today_date) > strtotime($next_date)){
				$days = $this->GetDays($next_date, $today_date);
				foreach ($days as $dkey => $dvalue) {
					$dates = explode('-', $dvalue);
					$day[$dkey]['day'] = $dates[2];
					$day[$dkey]['date'] = $dvalue;
				}
			} else {
				if(strtotime($next_date) == strtotime($today_date)){
					$dates = explode('-', $today_date);
					$day[0]['day'] = $dates[2];
					$day[0]['date'] = $today_date;
				}
			}
			*/
			//$time = $this->convertToHoursMins('360');
			// echo '<pre>';
			// print_r($day);
			// exit;
			//$mdbFilename = 'Z:\Avi infotech Backup\eTimeTrackLite1.mdb';
			//$mdbFilename = DIR_DOWNLOAD.'eTimeTrackLite1.mdb';
			//$user = '';
			//$password = '';
			//$connection = odbc_connect("Driver={Microsoft Access Driver (*.mdb)};Dbq=$mdbFilename", $user, $password);
			$serverName = "DATABASE-ERP\SP2010";
			$connectionInfo = array("Database"=>"SmartOfficedb", "UID"=>"avi", "PWD"=>"avi@123");
			//$connectionInfo = array("Database"=>"eSSLSmartOfficeAVI");
			$conn = sqlsrv_connect($serverName, $connectionInfo);
			if($conn) {
				//echo "Connection established.<br />";exit;
			}else{
				//echo "Connection could not be established.<br />";exit;
				//echo '<pre>';
				//print_r(sqlsrv_errors());
				//exit;
				//die( print_r( sqlsrv_errors(), true));
			}
			//echo 'out';exit;

			foreach($day as $dkeys => $dvalues){
				$filter_date_start = $dvalues['date'];
				$sql = "SELECT * FROM dbo.AttendanceLogs WHERE CONVERT(date, AttendanceDate) = CONVERT(date, '".$filter_date_start."') ";
				//echo $sql;exit;
				$params = array();
				$options =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
				$stmt = sqlsrv_query($conn, $sql, $params, $options);
				if( $stmt === false) {
				   // echo'<pre>';
				   // print_r(sqlsrv_errors());
				   // exit;
				}
				$exp_datas = array();
				$rev_exp_datas = array();
				$found_emp_code = array();
				while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
					$atten_log_data = $row['AttendanceDate'];
					if($atten_log_data != null){	
						$attend_date = $atten_log_data->format("Y-m-d");
						$emp_id = $row['EmployeeId'];
						$sql1 = "SELECT * FROM dbo.Employees WHERE EmployeeId = ".$emp_id." ";
						$params1 = array();
						$options1 =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
						$stmt1 = sqlsrv_query($conn, $sql1, $params1, $options1);
						if($stmt1 === false) {
						   // echo'<pre>';
						   // print_r(sqlsrv_errors());
						   // exit;
						}
						$emp_code = 0;
						while($row1 = sqlsrv_fetch_array($stmt1, SQLSRV_FETCH_ASSOC)) {
							$emp_codes = $row1['EmployeeCode'];
							$emp_code = $emp_codes;
						}
						if($emp_code != 0){
							$out_time = $row['OutTime'];
							$out_date = $attend_date;
							
							$in_time = $row['InTime'];
							$in_date = $attend_date;
							$result = $this->model_transaction_transaction->getEmployees_dat($emp_code);
							if(isset($result['emp_code'])){
								$shift_intime = '09:00:00';
								$shift_outtime = '18:00:00';
								$weekly_off = 0;
								$holiday_id = 0;
								$leave_status = 0;
								$present_status = 0;
								$absent_status = 0;
								$first_half = '';
								$second_half = '';

								$found_emp_code[$emp_code] = $emp_code;
								$emp_name = 'Test Name';//$result['name'];
								$department = 'Test Dept';//$result['department'];
								$unit = 'Test Unit';//$result['unit'];
								$act_intime = $in_time;
								$act_outtime = $out_time;
								$date_out = $out_date;
								$date = $in_date;
								$day = date('j', strtotime($in_date));
								$month = date('n', strtotime($in_date));
								$year = date('Y', strtotime($in_date));
								$working_time = '00:00:00';
								if($act_outtime != '00:00:00'){//for getting working time
									$start_date = new DateTime($date.' '.$act_intime);
									$since_start = $start_date->diff(new DateTime($date_out.' '.$act_outtime));
									$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
								} else {
									$working_time = $this->convertToHoursMins($row['Duration']);
								}
								$late_time = $this->convertToHoursMins($row['LateBy']);
								$early_time = $this->convertToHoursMins($row['EarlyBy']);
								$overtime = $this->convertToHoursMins($row['OverTime']);
								$present_status = $row['Present'];
								$absent_status = $row['Absent'];
								if($present_status == '1'){
									$first_half = '1';
									$second_half = '1';
								}
								if($absent_status == '1'){
									$first_half = '0';
									$second_half = '0';
								}
								if($row['LeaveType'] != ''){
									$leave_status = 1;
									$first_half = 'PL';							
									$second_half = 'PL';							
								}
								if($row['WeeklyOff'] != '0'){
									$weekly_off = 1;
									$first_half = 'WO';							
									$second_half = 'WO';							
								}
								if($row['Holiday'] != '0'){
									$holiday_id = 1;
									$first_half = 'HLD';							
									$second_half = 'HLD';							
								}

								$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$emp_code."' AND `date` = '".$date."' ";
								$trans_exist = $this->db->query($trans_exist_sql);
								if($trans_exist->num_rows == 0){
									$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$emp_code."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$date."', `date_out` = '".$date_out."', `department` = '".$department."', `unit` = '".$unit."', `group` = '', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `weekly_off` = '".$weekly_off."', `holiday_id` = '".$holiday_id."', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '0', overtime = '".$overtime."', emp_type = '1'  ";
								} else {
									$sql = "UPDATE `oc_transaction` SET `emp_id` = '".$emp_code."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$date."', `date_out` = '".$date_out."', `department` = '".$department."', `unit` = '".$unit."', `group` = '', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `weekly_off` = '".$weekly_off."', `holiday_id` = '".$holiday_id."', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '0', overtime = '".$overtime."', emp_type = '1' WHERE `emp_id` = '".$emp_code."' AND `date` = '".$date."' ";			
								}
								//echo $sql;exit;
								$this->db->query($sql);
								//$this->log->write($sql);

								// $employees_proc[] = array(
								// 	'employee_id' => $emp_code,//$result['emp_code'],
								// 	'card_id' => $emp_code,//$result['card_number'],
								// 	'shift_intime' => $shift_intime,
								// 	'shift_outtime' => $shift_outtime,
								// 	'act_intime' => $in_time,
								// 	'act_outtime' => $out_time,
								// 	'weekly_off' => $weekly_off,
								// 	'holiday_id' => $holiday_id,
								// 	'late_time' => $late_time,
								// 	'early_time' => $early_time,
								// 	'working_time' => $working_time,
								// 	'day' => $day,
								// 	'month' => $month,
								// 	'year' => $year,
								// 	'date' => $in_date,
								// 	'department' => $department,
								// 	'unit' => $unit,
								// 	'first_half' => $first_half,
								// 	'second_half' => $second_half,
								// 	'leave_status' => $leave_status,
								// 	'present_status' => $present_status,
								// 	'absent_status' => $absent_status,
								// 	'date_out' => $out_date,
								// 	'overtime' => $overtime,
								// 	'out_time' => $out_time,
								// 	'out_time' => $out_time,
								// 	'out_time' => $out_time,
								// 	'out_time' => $out_time,
								// );
							}
						}
					}
				}
				//echo 'out';exit;
				$this->load->model('transaction/dayprocess');
				$filter_unit = '';
				$absent = $this->model_transaction_dayprocess->getAbsent($filter_date_start,$filter_unit);
				foreach($absent as $data) {
					$check_leave = $this->model_transaction_dayprocess->checkLeave($data['date'],$data['emp_id']);
					//$this->model_transaction_dayprocess->checkLeave_1($data['date'],$data['emp_id']);
				}
				$this->model_transaction_dayprocess->update_close_day($filter_date_start, $filter_unit, 1);
			}
			$this->load->model('transaction/leaveprocess');
			$this->load->model('transaction/dayprocess');
			if (isset($this->request->get['unit'])) {
				$unit = $this->request->get['unit'];
			} else {
				$unit = '';
			}
			$unprocessed = $this->model_transaction_leaveprocess->getUnprocessedLeaveTillDate_2($unit);
			foreach($unprocessed as $data) {
				$is_closed = $this->model_transaction_dayprocess->is_closed_stat($data['date'], $data['emp_id']); 
				if($is_closed == 1){
					$this->model_transaction_dayprocess->checkLeave($data['date'], $data['emp_id']);
				}
			}
			$this->session->data['success'] = 'Muster Employees Attendance Updated Sucessfully';
			$this->redirect($this->url->link('snippets/dataprocess', 'token=' . $this->session->data['token'], 'SSL'));
		//echo 'Done';exit;
		}

		if(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->data['token'] = $this->session->data['token'];

		$this->data['filter_month'] = $filter_month;
		$this->data['filter_year'] = $filter_year;

		$this->template = 'catalog/dataprocess.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	public function convertToHoursMins($time, $format = '%02d:%02d') {
	    if ($time < 1) {
	        return;
	    }
	    $hours = floor($time / 60);
	    $minutes = ($time % 60);
	    return sprintf($format, $hours, $minutes);
	}

	public function sortByOrder($a, $b) {
		$v1 = strtotime($a['fdate']);
		$v2 = strtotime($b['fdate']);
		return $v1 - $v2; // $v2 - $v1 to reverse direction
		// if ($a['punch_date'] == $b['punch_date']) {
			//return ($a['in_time'] > $b['in_time']) ? -1 : 1;;
		//}
		//return $a['punch_date'] - $b['punch_date'];
	}

	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	public function array_sort($array, $on, $order=SORT_ASC){

		$new_array = array();
		$sortable_array = array();

		if (count($array) > 0) {
			foreach ($array as $k => $v) {
				if (is_array($v)) {
					foreach ($v as $k2 => $v2) {
						if ($k2 == $on) {
							$sortable_array[$k] = $v2;
						}
					}
				} else {
					$sortable_array[$k] = $v;
				}
			}

			switch ($order) {
				case SORT_ASC:
					asort($sortable_array);
					break;
				case SORT_DESC:
					arsort($sortable_array);
					break;
			}

			foreach ($sortable_array as $k => $v) {
				$new_array[$k] = $array[$k];
			}
		}

		return $new_array;
	}
}
?>