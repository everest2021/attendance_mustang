<?php    
class ControllerSnippetsDataprocessdaily2 extends Controller { 
	private $error = array();

	public function index() {
		//echo date("h:i:s A");
		/*
		$shift_id = '2';
		$shift_code = 'S2';
		$shift_intime = '16:00:00';
		$shift_outtime = '00:00:00';
		$act_in_punch_date = '2019-01-01';
		$act_out_punch_date = '2019-01-01';
		$svalue['act_intime'] = '08:15:00';
		if($svalue['act_intime'] != "00:00:00"){		
			$in_datetime = $this->get_all_times($shift_intime);
			$in_random = array_rand($in_datetime);
			$act_intime = $in_datetime[$in_random];
			
			$datatime = $this->get_all_timess("08:00:00");
			$random = array_rand($datatime);
			$addtime = $datatime[$random];
		
			$secs = strtotime($act_intime)-strtotime("00:00:00");
			$act_outtime = date("H:i:s",strtotime($addtime)+$secs);
			$act_outtime_hours = date("H",strtotime($addtime)+$secs);
			echo $act_outtime;
			echo '<br />';
			if($act_outtime >= '00' && $act_outtime <= '01'){
				$act_out_punch_date = date('Y-m-d', strtotime($act_out_punch_date . "+1 day"));		
			}
		}
		echo $act_in_punch_date;
		echo '<br />';
		echo $act_out_punch_date;
		echo '<br />';
		exit;

		$shift_id = '3';
		$shift_code = 'S3';
		$shift_intime = '00:00:00';
		$shift_outtime = '08:00:00';
		$act_in_punch_date = '2019-01-01';
		$act_out_punch_date = '2019-01-01';
		$svalue['act_intime'] = '08:15:00';
		if($svalue['act_intime'] != "00:00:00"){		
			$in_datetime = $this->get_all_times($shift_intime);
			$in_random = array_rand($in_datetime);
			$act_intime = $in_datetime[$in_random];
			echo $act_intime;
			echo '<br />';
			$act_intime_hour = date('H', strtotime($act_intime));
			if($act_intime_hour >= '23'){
				$act_in_punch_date = date('Y-m-d', strtotime($act_in_punch_date . "-1 day"));
			}
		}
		echo $act_in_punch_date;
		echo '<br />';
		echo $act_out_punch_date;
		echo '<br />';
		echo 'out';exit;
		*/

		$this->load->model('transaction/transaction');
		$this->load->model('report/common_report');
		$this->load->model('catalog/employee');		
		
		if(isset($this->request->get['filter_month'])){
			$filter_month = $this->request->get['filter_month'];
		} else {
			$filter_month = date('n');
		}

		if(isset($this->request->get['filter_year'])){
			$filter_year = $this->request->get['filter_year'];
		} else {
			$filter_year = date('Y');
		}
		if(isset($this->request->get['filter_location'])){
			$filter_location = $this->request->get['filter_location'];
		} else {
			$filter_location = '';
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Daily Employee Update Attendance'),
			'href'      => $this->url->link('snippets/dataprocess_daily', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$months = array(
			'01' => 'January',
			'02' => 'Feburary',
			'03' => 'March',
			'04' => 'April',
			'05' => 'May',
			'06' => 'June',
			'07' => 'July',
			'08' => 'August',
			'09' => 'September',
			'10' => 'October',
			'11' => 'November',
			'12' => 'December'
		);
		$this->data['months'] = $months;
		
		$years = array(
			'2018' => '2018',
			'2019' => '2019',
			'2020' => '2020',
			'2021' => '2021',
		);
		$this->data['years'] = $years;
		// echo'<pre>';
		// print_r($this->data);
		// exit;

		$this->load->model('catalog/location');
		$data = array();
		$unit_id = 0;
		$in = 0;
		if($this->user->getUserGroupId() != 1 && $this->user->getUnitId() != '0'){
			$in = 1;
			$unit_id = $this->user->getUnitId();
		}
		$results = $this->model_catalog_location->getLocations($data);
		$unit_datas = array();
		if($in == 0){
			$unit_datas['0'] = 'All';
		}
		foreach($results as $rkey => $rvalue){
			if($in == 1){
				if($unit_id == $rvalue['unit_id']){
					$unit_datas[$rvalue['unit']] = $rvalue['unit'];
				}
			} else {
				$unit_datas[$rvalue['unit']] = $rvalue['unit'];
			}
		}
		$this->data['unit_datas'] = $unit_datas;
		if(isset($this->request->get['once']) && $this->request->get['once'] == '1'){
			//echo 'aaaa';exit;
			
			$stats_sql = "SELECT * FROM `oc_status` WHERE `process_status` = '1' ";
			$stats_data = $this->db->query($stats_sql);
			// echo '<pre>';
			// print_r($stats_data);
			// exit;
			if($stats_data->num_rows == 0){
				$sql = "UPDATE `oc_status` SET `process_status` = '0' ";

				
				$this->db->query($sql);
				
				$start_date = $filter_year.'-'.$filter_month.'-01';
				$end_date = date("Y-m-t", strtotime($start_date));

				//$start_date = '2019-10-01';
				//$end_date = '2019-10-10';
				// echo $start_date;
				// echo '<br />';
				// echo $end_date;
				
				$day = array();
				$days = $this->GetDays($start_date, $end_date);
				foreach ($days as $dkey => $dvalue) {
					if(strtotime($dvalue) <= strtotime(date('Y-m-d'))){
						$dates = explode('-', $dvalue);
						$day[$dkey]['day'] = $dates[2];
						$day[$dkey]['date'] = $dvalue;
					}
				}
				$this->db->query("UPDATE `oc_transaction` SET  `act_intime_manipulated` = '00:00:00', `act_outtime_manipulated` = '00:00:00', `working_time_manipulated` = '00:00:00',over_time_manipulated = '00:00:00',`firsthalf_status_manipulated` = '0', `secondhalf_status_manipulated` = '0', `present_status_manipulated` = '0', `absent_status_manipulated` = '1', `shift_id_manipulated` = '0', `shift_code_manipulated` = '', `shift_intime_manipulated` = '00:00:00', `shift_outtime_manipulated` = '00:00:00', `date_out_manipulated` = '0000-00-00' WHERE `date` >= '".$start_date."' AND `date` <= '".$end_date."' ");
				$all_shift[1]['shift_intime'] = '07:00:00';
				$all_shift[1]['shift_outtime'] = '15:30:00';
				$all_shift[1]['shift_code'] = 'S1';

				$all_shift[2]['shift_intime'] = '15:00:00';
				$all_shift[2]['shift_outtime'] = '23:30:00';
				$all_shift[2]['shift_code'] = 'S2';

				$all_shift[3]['shift_intime'] = '23:00:00';
				$all_shift[3]['shift_outtime'] = '07:30:00';
				$all_shift[3]['shift_code'] = 'S3';

				$emp_string_1 = '';
				$emp_string_2 = '';
				$emp_string_3 = '';
				$emp_chunk_1 = $this->db->query(" SELECT * FROM oc_employee WHERE chunk_id = 1 ")->rows;
				foreach ($emp_chunk_1 as $e1key => $evalue1) {
					$emp_string_1 .= $evalue1['employee_id'].',';
				}

				$emp_string_1 = rtrim($emp_string_1, ',');

				$emp_chunk_2 = $this->db->query(" SELECT * FROM oc_employee WHERE chunk_id = 2 ")->rows;
				foreach ($emp_chunk_2 as $e2key => $evalue2) {
					$emp_string_2 .= $evalue2['employee_id'].',';
				}

				$emp_string_2 = rtrim($emp_string_2, ',');

				$emp_chunk_3 = $this->db->query(" SELECT * FROM oc_employee WHERE chunk_id = 3 ")->rows;
				foreach ($emp_chunk_3 as $e3key => $evalue3) {
					$emp_string_3 .= $evalue3['employee_id'].',';
				}

				$emp_string_3 = rtrim($emp_string_3, ',');
				
				$shift_datas_sql = "SELECT * FROM `oc_shift` WHERE `status` = '1' AND `shift_change` = '1' ";
				$shift_datass = $this->db->query($shift_datas_sql);
				$shift_count = $shift_datass->num_rows;
				
											
				$sql_multiple_query = '';
				$itr = 1;
				$counter_shift_change = 1;
				foreach($day as $dkeys => $dvalues){
					$filter_date_start = $dvalues['date'];
					// echo "<br />";
					// echo'<pre>';
					// print_r($filter_date_start);
					// echo "<br />";
					
					$this->load->model('transaction/dayprocess');

					$shift_1_data = array();
					$tran_datas_sql_1 = $this->db->query("SELECT * FROM `oc_transaction` WHERE `date` = '".$filter_date_start."' AND emp_id IN (".$emp_string_1.") ORDER BY `emp_id` ASC");
					if($tran_datas_sql_1->num_rows > 0){
						$shift_1_data = $tran_datas_sql_1->rows;
					}

					$shift_2_data = array();
					$tran_datas_sql_2 = $this->db->query("SELECT * FROM `oc_transaction` WHERE `date` = '".$filter_date_start."' AND emp_id IN (".$emp_string_2.") ORDER BY `emp_id` ASC");
					if($tran_datas_sql_2->num_rows > 0){
						$shift_2_data = $tran_datas_sql_2->rows;
					}

					$shift_3_data = array();
					$tran_datas_sql_3 = $this->db->query("SELECT * FROM `oc_transaction` WHERE `date` = '".$filter_date_start."' AND emp_id IN (".$emp_string_3.") ORDER BY `emp_id` ASC");
					if($tran_datas_sql_3->num_rows > 0){
						$shift_3_data = $tran_datas_sql_3->rows;
					}
					$tran_datas_sql = "SELECT * FROM `oc_transaction` t LEFT JOIN `oc_employee` e ON (e.`employee_id` = t.`emp_id`) WHERE t.`date` = '".$filter_date_start."' AND t.`emp_type` = '2' AND e.`status` = '1'ORDER BY t.`emp_id` ASC";
					// echo'<pre>';
					// print_r("SELECT * FROM `oc_transaction` t LEFT JOIN `oc_employee` e ON (e.`employee_id` = t.`emp_id`) WHERE t.`date` = '".$filter_date_start."' AND t.`emp_type` = '2' AND e.`status` = '1' ORDER BY t.`emp_id` ASC");
					// exit;
					$tran_datass = $this->db->query($tran_datas_sql);
					$tran_datas = array();
					$tran_datas_num_rows = 0;
					$tran_datas_num_rows = $tran_datass->num_rows;				
					$tran_datas = $tran_datass->rows;
					// print_r('tran_datas');
					// echo'<pre>';
					// print_r($tran_datas);
					// exit;
					$shift_re_sql = "SELECT * FROM `oc_shift_change`";
					$shift_re = $this->db->query($shift_re_sql)->row;
					$first_in = 0;
					if(isset($shift_re['date'])){
						$shift_change_date = $shift_re['date'];
					} else {
						$first_in = 1;
						$shift_change_date = $filter_date_start;
					}
					$times = '07:00:00';
					$start_date = new DateTime($filter_date_start.' '.$times);
					$since_start = $start_date->diff(new DateTime($shift_change_date.' '.$times));
					$difference_days = $since_start->d;
					
					// $this->log->write('Start Date');
					// $this->log->write($filter_date_start);
					// $this->log->write('Diffrence Date');
					// $this->log->write($difference_days);

					//if($first_in == 1 || $difference_days >= 7){
					if(date('Y-m-d', strtotime($filter_date_start)) == date('Y-m-01', strtotime($filter_date_start)) || date('N', strtotime($filter_date_start)) == 6){
						$this->log->write('Inn Reset Shift');
						$this->log->write($difference_days);
						$tran_count = count($tran_datas);
						$divide_number = ceil($tran_count / $shift_count);
						$tran_datas_chunk = array_chunk($tran_datas, $divide_number);
						// $this->log->write('Trans Data Chunk');
						// $this->log->write($tran_datas_chunk);
						//$max_size = count($tran_datas_chunk);
						//$random_array = $this->generate_random($max_size, $shift_count);
						
						// $shift_1_data = array();
						// $shift_3_data = array();
						// $shift_2_data = array();
						// if(isset($tran_datas_chunk[0])){
						// 	if($counter_shift_change == 1){
						// 		$shift_1_data = $tran_datas_chunk[0];	
						// 	} else if($counter_shift_change == 2){
						// 		$shift_2_data = $tran_datas_chunk[0];	
						// 	} else if($counter_shift_change == 3){
						// 		$shift_3_data = $tran_datas_chunk[0];
						// 	} else if($counter_shift_change == 4){
						// 		$shift_1_data = $tran_datas_chunk[0];
						// 	} else if($counter_shift_change == 5){
						// 		$shift_2_data = $tran_datas_chunk[0];
						// 	} else if($counter_shift_change == 6){
						// 		$shift_3_data = $tran_datas_chunk[0];
						// 	}
						// 	//$shift_1_data = $tran_datas_chunk[$random_array[0]];	
						// }
						// if(isset($tran_datas_chunk[1])){
						// 	if($counter_shift_change == 1){
						// 		$shift_3_data = $tran_datas_chunk[1];	
						// 	} else if($counter_shift_change == 2){
						// 		$shift_1_data = $tran_datas_chunk[1];	
						// 	} else if($counter_shift_change == 3){
						// 		$shift_2_data = $tran_datas_chunk[1];
						// 	} else if($counter_shift_change == 4){
						// 		$shift_3_data = $tran_datas_chunk[1];
						// 	} else if($counter_shift_change == 5){
						// 		$shift_1_data = $tran_datas_chunk[1];
						// 	} else if($counter_shift_change == 6){
						// 		$shift_2_data = $tran_datas_chunk[1];
						// 	}
						// 	//$shift_2_data = $tran_datas_chunk[$random_array[1]];	
						// }
						// if(isset($tran_datas_chunk[2])){
						// 	if($counter_shift_change == 1){
						// 		$shift_2_data = $tran_datas_chunk[2];	
						// 	} else if($counter_shift_change == 2){
						// 		$shift_3_data = $tran_datas_chunk[2];	
						// 	} else if($counter_shift_change == 3){
						// 		$shift_1_data = $tran_datas_chunk[2];
						// 	} else if($counter_shift_change == 4){
						// 		$shift_2_data = $tran_datas_chunk[2];
						// 	} else if($counter_shift_change == 5){
						// 		$shift_3_data = $tran_datas_chunk[2];
						// 	} else if($counter_shift_change == 6){
						// 		$shift_1_data = $tran_datas_chunk[2];
						// 	}
						// 	//$shift_3_data = $tran_datas_chunk[$random_array[2]];	
						// }
						
						foreach ($shift_1_data as $skey => $svalue) {
							//$shift_id = '1';
							//$shift_code = 'S1';
							//$shift_intime = '07:00:00';
							//$shift_outtime = '15:00:00';
							//$act_in_punch_date = $svalue['date'];
							//$act_out_punch_date = $svalue['date'];

							if ($skey == 0) {
								if ($counter_shift_change == 1) {
									$date_starts = date("Y-m", strtotime($filter_date_start)).'-01';
									$date_start_minus_1 = date("Y-m-d", strtotime("-1 day", strtotime($date_starts)));
								} else {
									if (date('N', strtotime($filter_date_start)) == 6) {
										//$date_starts = date("Y-m", strtotime($filter_date_start)).'-16';
										$date_start_minus_1 = date("Y-m-d", strtotime("-1 day", strtotime($filter_date_start)));
									} 
								}
								$previous_shift = $this->db->query(" SELECT shift_id_manipulated FROM oc_transaction WHERE `date` = '".$date_start_minus_1."' AND emp_id = '".$svalue['emp_id']."' ");
								if ($previous_shift->num_rows > 0 && $previous_shift->row['shift_id_manipulated'] == 1) {
									$shift_id = 3;
								} elseif ($previous_shift->num_rows > 0 && $previous_shift->row['shift_id_manipulated'] == 2) {
									$shift_id = 1;
								} elseif ($previous_shift->num_rows > 0 && $previous_shift->row['shift_id_manipulated'] == 3) {
									$shift_id = 2;
								} else {
									$shift_id = 1;
								}
								//echo "<pre>";print_r('im _m' .$filter_date_start);
								//echo "<pre>";print_r('im _changing_thiShift from this shit' .$shift_id .'to shift '.$previous_shift->row['shift_id_manipulated']);
							}
							 
							//echo "<pre>";print_r('skey');
							//echo "<pre>";print_r($skey);
							//echo "<pre>";print_r('shift_id');
							//echo "<pre>";print_r($shift_id);

							$shift_code = $all_shift[$shift_id]['shift_code'];
							$shift_intime = $all_shift[$shift_id]['shift_intime'];
							$shift_outtime = $all_shift[$shift_id]['shift_outtime'];
							$act_in_punch_date = $svalue['date'];
							$act_out_punch_date = $svalue['date'];

							//if($svalue['act_intime'] != "00:00:00"  || (date('N', strtotime($svalue['date'])) == 5)){	
							if($svalue['act_intime'] != "00:00:00" ){		
								$in_datetime = $this->get_all_times($shift_intime);
								$in_random = array_rand($in_datetime);
								$act_intime = $in_datetime[$in_random];

								if($svalue['halfday_status'] == '1'){
									$datatime = $this->create_time_range('04:31', '05:59', '1 mins');
									// echo "<pre>";print_r('empid');
									// echo "<pre>";print_r($svalue['emp_name']);
									// echo "<pre>";print_r('daetime');
									// echo "<pre>";print_r($datatime);
								} else {

									$datatime = $this->get_all_timess("08:00:00");
									// echo "<pre>";print_r('empid');
									// echo "<pre>";print_r($svalue['emp_name']);
									// echo "<pre>";print_r('daetime');
									// echo "<pre>";print_r($datatime);
								}
								

								$random = array_rand($datatime);
								$addtime = $datatime[$random];
							
								$secs = strtotime($act_intime)-strtotime("00:00:00");
								$act_outtime = date("H:i:s",strtotime($addtime)+$secs);
							
								$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
								$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
								$late_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
								if($since_start->invert == 1){
									$late_time = '00:00:00';
								} else {
									$late_time = '00:00:00';
								}

								$start_date = new DateTime($act_out_punch_date.' '.$shift_outtime);
								$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
								$early_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;					
								$over_time = '00:00:00';//$since_start->h.':'.$since_start->i.':'.$since_start->s;					
								if($since_start->invert == 0){
									$early_time = '00:00:00';
								} else {
									$early_time = '00:00:00';
									$over_time = '00:00:00';
								}

								$early_time = '00:00:00';
								$late_time = '00:00:00';
								$over_time = '00:00:00';

								$first_half = 1;
								$second_half = 1;
								$present_status = 1;
								$absent_status = 0;

								$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
								$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
								$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
							} else {
								$act_intime = '00:00:00';	
								$act_outtime = '00:00:00';	
								$late_time = '00:00:00';	
								$early_time = '00:00:00';	
								$over_time = '00:00:00';	
								$working_time = '00:00:00';	
								$first_half = 0;
								$second_half = 0;
								$present_status = 0;
								$absent_status = 1;
							}
							if($act_in_punch_date == $act_out_punch_date){
								$act_out_punch_date =  date('Y-m-d', strtotime($act_in_punch_date . ' +1 day'))	;
							    $start_date = new DateTime($act_in_punch_date.' '.$act_intime);
								$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
								$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
							}
							$update_sql = 	"UPDATE `oc_transaction` SET 
											`shift_id_manipulated` = '".$shift_id."',
											`shift_code_manipulated` = '".$shift_code."',
											`shift_intime_manipulated` = '".$shift_intime."',
											`shift_outtime_manipulated` = '".$shift_outtime."',
											`manual_status` = '1',
											`date_out_manipulated` = '".$act_out_punch_date."',
											`act_intime_manipulated` = '".$act_intime."',
											`act_outtime_manipulated` = '".$act_outtime."',
											`late_time_manipulated` = '".$late_time."',
											`early_time_manipulated` = '".$early_time."',
											`over_time_manipulated` = '".$over_time."',
											`working_time_manipulated` = '".$working_time."',
											`firsthalf_status_manipulated` = '".$first_half."',
											`secondhalf_status_manipulated` = '".$second_half."',
											`present_status_manipulated` = '".$present_status."',
											`absent_status_manipulated` = '".$absent_status."' 
											WHERE `transaction_id` = '".$svalue['transaction_id']."'";
							//$sql_multiple_query = $sql_multiple_query.$update_sql;
							$this->db->query($update_sql);
							//echo $update_sql;
						}

						foreach ($shift_2_data as $skey => $svalue) {
							//$shift_id = '1';
							//$shift_code = 'S1';
							//$shift_intime = '07:00:00';
							//$shift_outtime = '15:00:00';
							//$act_in_punch_date = $svalue['date'];
							//$act_out_punch_date = $svalue['date'];

							if ($skey == 0) {
								if ($counter_shift_change == 1) {
									$date_starts = date("Y-m", strtotime($filter_date_start)).'-01';
									$date_start_minus_1 = date("Y-m-d", strtotime("-1 day", strtotime($date_starts)));
								} else {
									if (date('N', strtotime($filter_date_start)) == 6) {
										//$date_starts = date("Y-m", strtotime($filter_date_start)).'-16';
										$date_start_minus_1 = date("Y-m-d", strtotime("-1 day", strtotime($filter_date_start)));
									} 
								}
								$previous_shift = $this->db->query(" SELECT shift_id_manipulated FROM oc_transaction WHERE `date` = '".$date_start_minus_1."' AND emp_id = '".$svalue['emp_id']."' ");
								if ($previous_shift->num_rows > 0 && $previous_shift->row['shift_id_manipulated'] == 1) {
									$shift_id = 3;
								} elseif ($previous_shift->num_rows > 0 && $previous_shift->row['shift_id_manipulated'] == 2) {
									$shift_id = 1;
								} elseif ($previous_shift->num_rows > 0 && $previous_shift->row['shift_id_manipulated'] == 3) {
									$shift_id = 2;
								} else {
									$shift_id = 1;
								}
							}

							$shift_code = $all_shift[$shift_id]['shift_code'];
							$shift_intime = $all_shift[$shift_id]['shift_intime'];
							$shift_outtime = $all_shift[$shift_id]['shift_outtime'];
							$act_in_punch_date = $svalue['date'];
							$act_out_punch_date = $svalue['date'];

							//if($svalue['act_intime'] != "00:00:00"  || (date('N', strtotime($svalue['date'])) == 5)){	
							if($svalue['act_intime'] != "00:00:00" ){		
								$in_datetime = $this->get_all_times($shift_intime);
								$in_random = array_rand($in_datetime);
								$act_intime = $in_datetime[$in_random];

								if($svalue['halfday_status'] == '1'){
									$datatime = $this->create_time_range('04:31', '05:59', '1 mins');
								} else {
									$datatime = $this->get_all_timess("08:00:00");
								}
								$random = array_rand($datatime);
								$addtime = $datatime[$random];
							
								$secs = strtotime($act_intime)-strtotime("00:00:00");
								$act_outtime = date("H:i:s",strtotime($addtime)+$secs);
							
								$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
								$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
								$late_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
								if($since_start->invert == 1){
									$late_time = '00:00:00';
								} else {
									$late_time = '00:00:00';
								}

								$start_date = new DateTime($act_out_punch_date.' '.$shift_outtime);
								$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
								$early_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;					
								$over_time = '00:00:00';//$since_start->h.':'.$since_start->i.':'.$since_start->s;					
								if($since_start->invert == 0){
									$early_time = '00:00:00';
								} else {
									$early_time = '00:00:00';
									$over_time = '00:00:00';
								}

								$early_time = '00:00:00';
								$late_time = '00:00:00';
								$over_time = '00:00:00';

								$first_half = 1;
								$second_half = 1;
								$present_status = 1;
								$absent_status = 0;

								$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
								$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
								$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
							} else {
								$act_intime = '00:00:00';	
								$act_outtime = '00:00:00';	
								$late_time = '00:00:00';	
								$early_time = '00:00:00';	
								$over_time = '00:00:00';	
								$working_time = '00:00:00';	
								$first_half = 0;
								$second_half = 0;
								$present_status = 0;
								$absent_status = 1;
							}
							if($act_in_punch_date == $act_out_punch_date){
								$act_out_punch_date =  date('Y-m-d', strtotime($act_in_punch_date . ' +1 day'))	;
							    $start_date = new DateTime($act_in_punch_date.' '.$act_intime);
								$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
								$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
							}
							$update_sql = 	"UPDATE `oc_transaction` SET 
											`shift_id_manipulated` = '".$shift_id."',
											`shift_code_manipulated` = '".$shift_code."',
											`shift_intime_manipulated` = '".$shift_intime."',
											`shift_outtime_manipulated` = '".$shift_outtime."',
											`manual_status` = '1',
											`date_out_manipulated` = '".$act_out_punch_date."',
											`act_intime_manipulated` = '".$act_intime."',
											`act_outtime_manipulated` = '".$act_outtime."',
											`late_time_manipulated` = '".$late_time."',
											`early_time_manipulated` = '".$early_time."',
											`over_time_manipulated` = '".$over_time."',
											`working_time_manipulated` = '".$working_time."',
											`firsthalf_status_manipulated` = '".$first_half."',
											`secondhalf_status_manipulated` = '".$second_half."',
											`present_status_manipulated` = '".$present_status."',
											`absent_status_manipulated` = '".$absent_status."' 
											WHERE `transaction_id` = '".$svalue['transaction_id']."' ";
							//$sql_multiple_query = $sql_multiple_query.$update_sql;
							$this->db->query($update_sql);
							//echo $update_sql;
						}

						foreach ($shift_3_data as $skey => $svalue) {
							//$shift_id = '1';
							//$shift_code = 'S1';
							//$shift_intime = '07:00:00';
							//$shift_outtime = '15:00:00';
							//$act_in_punch_date = $svalue['date'];
							//$act_out_punch_date = $svalue['date'];

							if ($skey == 0) {
								if ($counter_shift_change == 1) {
									$date_starts = date("Y-m", strtotime($filter_date_start)).'-01';
									$date_start_minus_1 = date("Y-m-d", strtotime("-1 day", strtotime($date_starts)));
								} else {
									if (date('N', strtotime($filter_date_start)) == 6) {
										//$date_starts = date("Y-m", strtotime($filter_date_start)).'-16';
										$date_start_minus_1 = date("Y-m-d", strtotime("-1 day", strtotime($filter_date_start)));
									} 
								}
								$previous_shift = $this->db->query(" SELECT shift_id_manipulated FROM oc_transaction WHERE `date` = '".$date_start_minus_1."' AND emp_id = '".$svalue['emp_id']."' ");
								if ($previous_shift->num_rows > 0 && $previous_shift->row['shift_id_manipulated'] == 1) {
									$shift_id = 3;
								} elseif ($previous_shift->num_rows > 0 && $previous_shift->row['shift_id_manipulated'] == 2) {
									$shift_id = 1;
								} elseif ($previous_shift->num_rows > 0 && $previous_shift->row['shift_id_manipulated'] == 3) {
									$shift_id = 2;
								} else {
									$shift_id = 1;
								}
							}

							$shift_code = $all_shift[$shift_id]['shift_code'];
							$shift_intime = $all_shift[$shift_id]['shift_intime'];
							$shift_outtime = $all_shift[$shift_id]['shift_outtime'];
							$act_in_punch_date = $svalue['date'];
							$act_out_punch_date = $svalue['date'];

							//if($svalue['act_intime'] != "00:00:00"  || (date('N', strtotime($svalue['date'])) == 5)){	
							if($svalue['act_intime'] != "00:00:00" ){		
								$in_datetime = $this->get_all_times($shift_intime);
								$in_random = array_rand($in_datetime);
								$act_intime = $in_datetime[$in_random];

								if($svalue['halfday_status'] == '1'){
									$datatime = $this->create_time_range('04:31', '05:59', '1 mins');
								} else {
									$datatime = $this->get_all_timess("08:00:00");
								}
								$random = array_rand($datatime);
								$addtime = $datatime[$random];
							
								$secs = strtotime($act_intime)-strtotime("00:00:00");
								$act_outtime = date("H:i:s",strtotime($addtime)+$secs);
							
								$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
								$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
								$late_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
								if($since_start->invert == 1){
									$late_time = '00:00:00';
								} else {
									$late_time = '00:00:00';
								}

								$start_date = new DateTime($act_out_punch_date.' '.$shift_outtime);
								$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
								$early_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;					
								$over_time = '00:00:00';//$since_start->h.':'.$since_start->i.':'.$since_start->s;					
								if($since_start->invert == 0){
									$early_time = '00:00:00';
								} else {
									$early_time = '00:00:00';
									$over_time = '00:00:00';
								}

								$early_time = '00:00:00';
								$late_time = '00:00:00';
								$over_time = '00:00:00';

								$first_half = 1;
								$second_half = 1;
								$present_status = 1;
								$absent_status = 0;

								$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
								$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
								$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
							} else {
								$act_intime = '00:00:00';	
								$act_outtime = '00:00:00';	
								$late_time = '00:00:00';	
								$early_time = '00:00:00';	
								$over_time = '00:00:00';	
								$working_time = '00:00:00';	
								$first_half = 0;
								$second_half = 0;
								$present_status = 0;
								$absent_status = 1;
							}
							if($act_in_punch_date == $act_out_punch_date){
								$act_out_punch_date =  date('Y-m-d', strtotime($act_in_punch_date . ' +1 day'))	;
							    $start_date = new DateTime($act_in_punch_date.' '.$act_intime);
								$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
								$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
							}
							$update_sql = 	"UPDATE `oc_transaction` SET 
											`shift_id_manipulated` = '".$shift_id."',
											`shift_code_manipulated` = '".$shift_code."',
											`shift_intime_manipulated` = '".$shift_intime."',
											`shift_outtime_manipulated` = '".$shift_outtime."',
											`manual_status` = '1',
											`date_out_manipulated` = '".$act_out_punch_date."',
											`act_intime_manipulated` = '".$act_intime."',
											`act_outtime_manipulated` = '".$act_outtime."',
											`late_time_manipulated` = '".$late_time."',
											`early_time_manipulated` = '".$early_time."',
											`over_time_manipulated` = '".$over_time."',
											`working_time_manipulated` = '".$working_time."',
											`firsthalf_status_manipulated` = '".$first_half."',
											`secondhalf_status_manipulated` = '".$second_half."',
											`present_status_manipulated` = '".$present_status."',
											`absent_status_manipulated` = '".$absent_status."' 
											WHERE `transaction_id` = '".$svalue['transaction_id']."' ";
							//$sql_multiple_query = $sql_multiple_query.$update_sql;
							$this->db->query($update_sql);
							//echo $update_sql;
						}

						
						$delete_re = "TRUNCATE `oc_shift_change`";
						$this->db->query($delete_re);
						$update_re = "INSERT INTO `oc_shift_change` SET `date` = '".$filter_date_start."' ";
						$this->db->query($update_re);
						$counter_shift_change ++;
					} else {
						foreach ($tran_datas as $skey => $svalue) {
							//if($svalue['emp_id'] == '3'){
							if($svalue['act_intime'] != "00:00:00" ){
								$prev_filter_date_start = date('Y-m-d', strtotime($svalue['date'] . "-1 day"));
								$prev_shift_sql = "SELECT `emp_id`,`shift_id_manipulated`,`shift_intime_manipulated`, `shift_outtime_manipulated`,`shift_code_manipulated`,`shift_intime` FROM `oc_transaction` WHERE `date` = '".$prev_filter_date_start."' AND `emp_id` = '".$svalue['emp_id']."' ";
								$prev_shift_datas = $this->db->query($prev_shift_sql);
								
								if($prev_shift_datas->num_rows > 0){
									$shift_id = $prev_shift_datas->row['shift_id_manipulated'];
									if($shift_id == 1 || $shift_id == 2 || $shift_id == 3){
										$shift_intime = $all_shift[$shift_id]['shift_intime'];
										$shift_outtime = $all_shift[$shift_id]['shift_outtime'];
										$shift_code = $all_shift[$shift_id]['shift_code'];
									} else {
										$shift_id = 1;
										$shift_code = 'S1';
										$shift_intime = '07:00:00';
										$shift_outtime = '15:00:00';	
									}
								} else {
									$shift_id = 1;
									$shift_code = 'S1';
									$shift_intime = '07:00:00';
									$shift_outtime = '15:00:00';	
								}
								
								if($shift_id == 3){
									$act_in_punch_date = $svalue['date'];
									$act_out_punch_date = date('Y-m-d', strtotime($act_in_punch_date . "+1 day"));
								} else {
									$act_in_punch_date = $svalue['date'];
									$act_out_punch_date = $svalue['date'];
								}
								

								$in_datetime = $this->get_all_times($shift_intime);
								$in_random = array_rand($in_datetime);
								$act_intime = $in_datetime[$in_random];
								
								if($svalue['halfday_status'] == '1'){
									$datatime = $this->create_time_range('04:31', '05:59', '1 mins');
								} else {
									$datatime = $this->get_all_timess("08:00:00");
								}
								$random = array_rand($datatime);
								$addtime = $datatime[$random];
							
								$secs = strtotime($act_intime)-strtotime("00:00:00");
								$act_outtime = date("H:i:s",strtotime($addtime)+$secs);
								
								$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
								$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
								$late_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
								if($since_start->invert == 1){
									$late_time = '00:00:00';
								}

								$start_date = new DateTime($act_out_punch_date.' '.$shift_outtime);
								$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
								$early_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;					
								$over_time = '00:00:00';//$since_start->h.':'.$since_start->i.':'.$since_start->s;					
								if($since_start->invert == 0){
									$early_time = '00:00:00';
								} else {
									$over_time = '00:00:00';
								}	

								$late_time = '00:00:00';	
								$early_time = '00:00:00';	
								$over_time = '00:00:00';
								$first_half = 1;
								$second_half = 1;
								$present_status = 1;
								$absent_status = 0;

								$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
								$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
								$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
							} else {
								$prev_filter_date_start = date('Y-m-d', strtotime($svalue['date'] . "-1 day"));
								$prev_shift_sql = "SELECT `shift_id_manipulated` FROM `oc_transaction` WHERE `date` = '".$prev_filter_date_start."' AND `emp_id` = '".$svalue['emp_id']."' ";
								$prev_shift_datas = $this->db->query($prev_shift_sql);
								if($prev_shift_datas->num_rows > 0){
									$shift_id = $prev_shift_datas->row['shift_id_manipulated'];
									if($shift_id == 1 || $shift_id == 2 || $shift_id == 3){
										$shift_intime = $all_shift[$shift_id]['shift_intime'];
										$shift_outtime = $all_shift[$shift_id]['shift_outtime'];
										$shift_code = $all_shift[$shift_id]['shift_code'];
									} else {
										$shift_id = 1;
										$shift_code = 'S1';
										$shift_intime = '07:00:00';
										$shift_outtime = '15:30:00';	
									}
								} else {
									$shift_id = 1;
									$shift_code = 'S1';
									$shift_intime = '07:00:00';
									$shift_outtime = '15:30:00';	
								}
								if($shift_id == 3){
									$act_in_punch_date = $svalue['date'];
									$act_out_punch_date = date('Y-m-d', strtotime($act_in_punch_date . "+1 day"));
								} else {
									$act_in_punch_date = $svalue['date'];
									$act_out_punch_date = $svalue['date'];
								}
								$act_intime = '00:00:00';	
								$act_outtime = '00:00:00';	
								$late_time = '00:00:00';	
								$early_time = '00:00:00';	
								$over_time = '00:00:00';
								$working_time = '00:00:00';	
								$first_half = 0;
								$second_half = 0;
								$present_status = 0;
								$absent_status = 1;
							}
							if($act_in_punch_date == $act_out_punch_date){
								$act_out_punch_date =  date('Y-m-d', strtotime($act_in_punch_date . ' +1 day'))	;
							    $start_date = new DateTime($act_in_punch_date.' '.$act_intime);
								$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
								$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
							}
							$update_sql = 	"UPDATE `oc_transaction` SET 
												`shift_id_manipulated` = '".$shift_id."',
												`shift_code_manipulated` = '".$shift_code."',
												`shift_intime_manipulated` = '".$shift_intime."',
												`shift_outtime_manipulated` = '".$shift_outtime."',
												`manual_status` = '1',
												`date_out_manipulated` = '".$act_out_punch_date."',
												`act_intime_manipulated` = '".$act_intime."',
												`act_outtime_manipulated` = '".$act_outtime."',
												`late_time_manipulated` = '".$late_time."',
												`early_time_manipulated` = '".$early_time."',
												`over_time_manipulated` = '".$over_time."',
												`working_time_manipulated` = '".$working_time."',
												`firsthalf_status_manipulated` = '".$first_half."',
												`secondhalf_status_manipulated` = '".$second_half."',
												`present_status_manipulated` = '".$present_status."',
												`absent_status_manipulated` = '".$absent_status."' 
											WHERE `transaction_id` = '".$svalue['transaction_id']."' ";
							//$sql_multiple_query = $sql_multiple_query.$update_sql;
							$this->db->query($update_sql);
						}
						$delete_re = "TRUNCATE `oc_shift_change`";
						$this->db->query($delete_re);
						$update_re = "INSERT INTO `oc_shift_change` SET `date` = '".$filter_date_start."' ";
						$this->db->query($update_re);
					}

					$tran_datas_sql = "SELECT * FROM `oc_transaction` t LEFT JOIN `oc_employee` e ON (e.`employee_id` = t.`emp_id`) WHERE t.`date` = '".$filter_date_start."' AND t.`emp_type` = '1' AND t.`act_intime` <> '00:00:00' AND e.`status` = '1' ORDER BY t.`emp_id` ASC";
					$tran_datass = $this->db->query($tran_datas_sql);
					$tran_datas_num_rows = $tran_datass->num_rows;				
					$tran_datas = $tran_datass->rows;
					foreach ($tran_datas as $skey => $svalue) {
						$working_time = $svalue['working_time'];
						if(strtotime($working_time) > strtotime('09:15:00') || ($svalue['halfday_status'] == '1' && $svalue['act_intime'] != '00:00:00' && $svalue['act_outtime'] != '00:00:00') || ($svalue['department_id'] == '12' && strtotime($working_time) > strtotime('08:15:00') &&  $svalue['act_intime'] != '00:00:00' && $svalue['act_outtime'] != '00:00:00' )){
							$shift_intime = $svalue['shift_intime'];
							$shift_outtime = $svalue['shift_outtime'];
							
							$act_intime = $svalue['act_intime'];
							$act_in_punch_date = $svalue['date'];
							$act_out_punch_date = $svalue['date_out'];
							if($svalue['halfday_status'] == '1'){
								$datatime = $this->create_time_range('04:31', '05:59', '1 mins');
							} else {
								if($svalue['department_id'] == '12'){
									$datatime = $this->get_all_timess("08:00:00");
								} else {
									$datatime = $this->get_all_timess("09:00:00");
								}
							}

							//$datatime = $this->get_all_timess("09:00:00");
							$random = array_rand($datatime);
							$addtime = $datatime[$random];
						
							$secs = strtotime($act_intime)-strtotime("00:00:00");
							$act_outtime = date("H:i:s",strtotime($addtime)+$secs);

							if($act_outtime >= '00:00:00' && $act_outtime <= '03:00:00'){
								$act_out_punch_date = date('Y-m-d', strtotime($act_out_punch_date . "+1 day"));							
							}

							$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
							$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
							$late_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
							if($since_start->invert == 1){
								$late_time = '00:00:00';
							}
							$late_time = '00:00:00';

							$start_date = new DateTime($act_out_punch_date.' '.$shift_outtime);
							$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
							$early_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;					
							$over_time = '00:00:00';//$since_start->h.':'.$since_start->i.':'.$since_start->s;					
							if($since_start->invert == 0){
								$early_time = '00:00:00';
							} else {
								$over_time = '00:00:00';
							}
							$early_time = '00:00:00';
							$over_time = '00:00:00';

							$first_half = 1;
							$second_half = 1;
							$present_status = 1;
							$absent_status = 0;

							$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
							$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
							$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;

							if($act_in_punch_date == $act_out_punch_date){
								$act_out_punch_date =  date('Y-m-d', strtotime($act_in_punch_date . ' +1 day'))	;
							    $start_date = new DateTime($act_in_punch_date.' '.$act_intime);
								$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
								$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
							}
							
							$update_sql = 	"UPDATE `oc_transaction` SET 
												`manual_status` = '1',
												`act_intime_manipulated` = '".$act_intime."',
												`act_outtime_manipulated` = '".$act_outtime."',
												`late_time_manipulated` = '".$late_time."',
												`early_time_manipulated` = '".$early_time."',
												`over_time_manipulated` = '".$over_time."',
												`working_time_manipulated` = '".$working_time."',
												`firsthalf_status_manipulated` = '".$first_half."',
												`secondhalf_status_manipulated` = '".$second_half."',
												`present_status_manipulated` = '".$present_status."',
												`absent_status_manipulated` = '".$absent_status."',
												`date_out_manipulated` = '".$act_out_punch_date."' 
											WHERE `transaction_id` = '".$svalue['transaction_id']."'; ";
							$sql_multiple_query = $sql_multiple_query.$update_sql;
							//$this->db->query($update_sql);
											//echo $update_sql;
						} else {
							$shift_intime = $svalue['shift_intime'];
							$shift_outtime = $svalue['shift_outtime'];
							
							$act_intime = $svalue['act_intime'];
							$act_in_punch_date = $svalue['date'];
							$act_outtime = $svalue['act_outtime'];
							$act_out_punch_date = $svalue['date_out'];
							$working_time = $svalue['working_time'];
							$late_time = $svalue['late_time'];
							$early_time = $svalue['early_time'];
							$over_time = $svalue['over_time'];
							$first_half = $svalue['firsthalf_status'];
							$second_half = $svalue['secondhalf_status'];
							$present_status = $svalue['present_status'];
							$absent_status = $svalue['absent_status'];

							$late_time = '00:00:00';
							$early_time = '00:00:00';
							$over_time = '00:00:00';

							$update_sql = 	"UPDATE `oc_transaction` SET 
												`manual_status` = '1',
												`act_intime_manipulated` = '".$act_intime."',
												`act_outtime_manipulated` = '".$act_outtime."',
												`late_time_manipulated` = '".$late_time."',
												`early_time_manipulated` = '".$early_time."',
												`over_time_manipulated` = '".$over_time."',
												`working_time_manipulated` = '".$working_time."',
												`firsthalf_status_manipulated` = '".$first_half."',
												`secondhalf_status_manipulated` = '".$second_half."',
												`present_status_manipulated` = '".$present_status."',
												`absent_status_manipulated` = '".$absent_status."',
												`date_out_manipulated` = '".$act_out_punch_date."' 
											WHERE `transaction_id` = '".$svalue['transaction_id']."'; ";
							$sql_multiple_query = $sql_multiple_query.$update_sql;
							//$this->db->query($update_sql);
											//echo $update_sql;
						}
					}
					$filter_unit = '';
					//$this->model_transaction_dayprocess->update_close_day($filter_date_start, $filter_unit);
				}
				
				$sql = "UPDATE `oc_status` SET `process_status` = '0' ";
				$sql_multiple_query = $sql_multiple_query.$sql;
				
				$con=mysqli_connect("localhost","root","","db_attendance_mustang");
				mysqli_multi_query($con,$sql_multiple_query);
				mysqli_close($con);
				//exit;
				//$this->db->query($sql);
				$this->session->data['success'] = 'Employee Attendance Process Updating please wait 8 min';
				$this->redirect($this->url->link('snippets/dataprocess_daily2', 'token=' . $this->session->data['token'], 'SSL'));
			} else {
				$this->session->data['warning'] = 'Attendance Process Already Running';
				$this->redirect($this->url->link('snippets/dataprocess_daily2', 'token=' . $this->session->data['token'], 'SSL'));
			}
			
		}

		if(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->data['token'] = $this->session->data['token'];

		$this->data['filter_month'] = $filter_month;
		$this->data['filter_year'] = $filter_year;
		$this->data['filter_location'] = $filter_location;
		
		$this->template = 'catalog/dataprocess_daily2.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	public function get_all_times($time){
		date_default_timezone_set("Asia/Kolkata");
		$minus_start_time = date('H:i:s', strtotime($time . "-15 minutes"));
		$time_array = array();
		for($i=0; $i<15; $i++){
			$time_array[] = date('H:i:s', strtotime($minus_start_time . "+".$i." minutes"));
		}
		for($i=0; $i<=15; $i++){
			$time_array[] = date('H:i:s', strtotime($time . "+".$i." minutes"));
		}
		return $time_array;		
	}

	public function get_all_timess($time){
		for($i=0; $i<=15; $i++){
			$time_array[] = date('H:i:s', strtotime($time . "+".$i." minutes"));
		}
		return $time_array;
	}

	public function generate_random($length, $max_value){
		$randarray = array(); 
		for($i = 1; $i <= $length;) { 
		    unset($rand); 
		    $rand = rand(1, $max_value); 
		    $rand = $rand - 1;
		    if(!in_array($rand, $randarray)) { 
		        $randarray[] = $rand; 
		        $i++; 
		    } 
		}
		return $randarray;
	}

	public function convertToHoursMins($time, $format = '%02d:%02d') {
	    if ($time < 1) {
	        return;
	    }
	    $hours = floor($time / 60);
	    $minutes = ($time % 60);
	    return sprintf($format, $hours, $minutes);
	}

	public function sortByOrder($a, $b) {
		$v1 = strtotime($a['fdate']);
		$v2 = strtotime($b['fdate']);
		return $v1 - $v2; // $v2 - $v1 to reverse direction
		// if ($a['punch_date'] == $b['punch_date']) {
			//return ($a['in_time'] > $b['in_time']) ? -1 : 1;;
		//}
		//return $a['punch_date'] - $b['punch_date'];
	}

	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	public function array_sort($array, $on, $order=SORT_ASC){

		$new_array = array();
		$sortable_array = array();

		if (count($array) > 0) {
			foreach ($array as $k => $v) {
				if (is_array($v)) {
					foreach ($v as $k2 => $v2) {
						if ($k2 == $on) {
							$sortable_array[$k] = $v2;
						}
					}
				} else {
					$sortable_array[$k] = $v;
				}
			}

			switch ($order) {
				case SORT_ASC:
					asort($sortable_array);
					break;
				case SORT_DESC:
					arsort($sortable_array);
					break;
			}

			foreach ($sortable_array as $k => $v) {
				$new_array[$k] = $array[$k];
			}
		}

		return $new_array;
	}

	public function resetprocess(){
		$sql = "UPDATE `oc_status` SET `process_status` = '0' ";
		$this->db->query($sql);

		$this->session->data['success'] = 'Process Reset Sucessfully';
		$this->redirect($this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'));	
	}
	function create_time_range($start, $end, $interval = '30 mins', $format = '12') {
	    $startTime = strtotime($start); 
	    $endTime   = strtotime($end);
	    $returnTimeFormat = ($format == '12')?'g:i:s':'G:i:s';

	    $current   = time(); 
	    $addTime   = strtotime('+'.$interval, $current); 
	    $diff      = $addTime - $current;

	    $times = array(); 
	    while ($startTime < $endTime) { 
	        $times[] = date($returnTimeFormat, $startTime); 
	        $startTime += $diff; 
	    } 
	    $times[] = date($returnTimeFormat, $startTime); 
	    return $times; 
	}
}
?>