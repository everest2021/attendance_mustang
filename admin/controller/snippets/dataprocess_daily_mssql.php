<?php    
class ControllerSnippetsDataprocessdaily extends Controller { 
	private $error = array();

	public function index() {
		$this->load->model('transaction/transaction');
		$this->load->model('report/common_report');
		$this->load->model('catalog/employee');		
		
		if(isset($this->request->get['filter_month'])){
			$filter_month = $this->request->get['filter_month'];
		} else {
			$filter_month = date('n');
		}

		if(isset($this->request->get['filter_year'])){
			$filter_year = $this->request->get['filter_year'];
		} else {
			$filter_year = date('Y');
		}
		if(isset($this->request->get['filter_location'])){
			$filter_location = $this->request->get['filter_location'];
		} else {
			$filter_location = '';
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Daily Employee Update Attendance'),
			'href'      => $this->url->link('snippets/dataprocess_daily', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$months = array(
			'01' => 'January',
			'02' => 'Feburary',
			'03' => 'March',
			'04' => 'April',
			'05' => 'May',
			'06' => 'June',
			'07' => 'July',
			'08' => 'August',
			'09' => 'September',
			'10' => 'October',
			'11' => 'November',
			'12' => 'December'
		);
		$this->data['months'] = $months;
		
		$years = array(
			'2017' => '2017',
			'2018' => '2018',
			'2019' => '2019',
		);
		$this->data['years'] = $years;
		// echo'<pre>';
		// print_r($this->data);
		// exit;

		$this->load->model('catalog/location');
		$data = array();
		$unit_id = 0;
		$in = 0;
		if($this->user->getUserGroupId() != 1 && $this->user->getUnitId() != '0'){
			$in = 1;
			$unit_id = $this->user->getUnitId();
		}
		$results = $this->model_catalog_location->getLocations($data);
		$unit_datas = array();
		if($in == 0){
			$unit_datas['0'] = 'All';
		}
		foreach($results as $rkey => $rvalue){
			if($in == 1){
				if($unit_id == $rvalue['unit_id']){
					$unit_datas[$rvalue['unit']] = $rvalue['unit'];
				}
			} else {
				$unit_datas[$rvalue['unit']] = $rvalue['unit'];
			}
		}
		$this->data['unit_datas'] = $unit_datas;
		// echo'<pre>';
		// 		   print_r($unit_datas);
		// 		   exit;

		if(isset($this->request->get['once']) && $this->request->get['once'] == '1'){
			$batch_ids = $this->db->query("SELECT `batch_id` FROM `oc_transaction` ORDER BY `batch_id` DESC LIMIT 1");
			if($batch_ids->num_rows > 0){
				$batch_id = $batch_ids->row['batch_id'] + 1;
			} else {
				$batch_id = 1;
			}
				
			$start_date = $filter_year.'-'.$filter_month.'-01';
			$end_date = date("Y-m-t", strtotime($start_date));
			// echo $start_date;
			// echo '<br />';
			// echo $end_date;
			// exit;
			$day = array();
			$days = $this->GetDays($start_date, $end_date);
			foreach ($days as $dkey => $dvalue) {
				if(strtotime($dvalue) <= strtotime(date('Y-m-d'))){
					$dates = explode('-', $dvalue);
					$day[$dkey]['day'] = $dates[2];
					$day[$dkey]['date'] = $dvalue;
				}
			}
			// echo '<pre>';
			// print_r($day);
			// exit;
			$update_empty = "UPDATE `oc_transaction` SET `act_intime` = '00:00:00', `act_outtime` = '00:00:00', `late_time` = '00:00:00', `early_time` = '00:00:00', `working_time` = '00:00:00', `over_time` = '00:00:00', `leave_status` = '0', `absent_status` = '1', `present_status` = '0', `abnormal_status` = '0', `batch_id` = '0', `weekly_off` = '0', `holiday_id` = '0', `halfday_status` = '0', `day_close_status` = '0', `manual_status` = '0', `firsthalf_status` = '0', `secondhalf_status` = '0' WHERE `date` >= '".$start_date."' AND `date` <= '".$end_date."' AND `manual_status` = '0' ";
			//$update_empty = "UPDATE `oc_transaction` SET `act_intime` = '00:00:00', `act_outtime` = '00:00:00', `late_time` = '00:00:00', `early_time` = '00:00:00', `working_time` = '00:00:00', `over_time` = '00:00:00', `leave_status` = '0', `absent_status` = '1', `present_status` = '0', `abnormal_status` = '0', `batch_id` = '0', `weekly_off` = '0', `holiday_id` = '0', `halfday_status` = '0', `day_close_status` = '0', `manual_status` = '0', `firsthalf_status` = '0', `secondhalf_status` = '0' WHERE `date` >= '".$start_date."' AND `date` <= '".$end_date."' AND `manual_status` = '0' AND `emp_id` = '83' ";
			if($filter_location){
				$update_empty .= " AND `unit` = '".$filter_location."' ";
			}
			$this->db->query($update_empty);
			$this->log->write($update_empty);

			$serverName = "HPPLMUMSPINE";
			//$connectionInfo = array("Database"=>"epushserver", "UID"=>"sa", "PWD"=>"essl@123");
			$connectionInfo = array("Database"=>"SmartOffice");
			$conn1 = sqlsrv_connect($serverName, $connectionInfo);
			if($conn1) {
				//echo "Connection established.<br />";exit;
			} else {
				//echo "Connection could not be established.<br />";
				//echo '<pre>';
				//print_r(sqlsrv_errors());
				//exit;
			}
			
			$shift_datas_sql = "SELECT * FROM `oc_shift`";
			$shift_datass = $this->db->query($shift_datas_sql);
			$shift_count = $shift_datass->num_rows;
			$shift_datas = $shift_datass->rows;
			$shift_data = array();
			foreach ($shift_datas as $skey => $svalue) {
				$shift_data[$svalue['shift_id']] = array(
					'shift_intime' => $svalue['in_time'],
					'shift_outtime' => $svalue['out_time'],
				);
			}

			foreach($day as $dkeys => $dvalues){
				$filter_date_start = $dvalues['date'];
				$sql = "SELECT * FROM Employees WHERE 1=1  ";
				if(!empty($filter_location)){
					$sql .= "AND Location = '".$filter_location."' ";
				}
				// 
				$params = array();
				$options =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
				$stmt = sqlsrv_query($conn1, $sql, $params, $options);
				if( $stmt === false) {
				   //echo'<pre>';
				   //print_r(sqlsrv_errors());
				   //exit;
				}
				while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)){
					$this->log->write('---Employee Start EmployeeCode : '.$row['EmployeeCode'].'----'.' And Date : ' . $filter_date_start.'---');
					// echo '<pre>';
					// print_r($row);
					// exit;
					$emp_id = $row['EmployeeId'];
					$emp_codes = $row["EmployeeCode"];
					$emp_code = $emp_codes;
					$sql1 = "SELECT * FROM AttendanceLogs WHERE EmployeeId = ".$emp_id." AND AttendanceDate = '".$filter_date_start."' ";
					//echo $sql1;exit;
					$params1 = array();
					$options1 =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
					$stmt1 = sqlsrv_query($conn1, $sql1, $params1, $options1);
					if( $stmt1 === false) {
					   //echo'<pre>';
					   //print_r(sqlsrv_errors());
					   //exit;
					}
					
					//$results = odbc_fetch_array($rs);
					$rev_exp_datas = array();
					$found_emp_code = array();
					while($row1 = sqlsrv_fetch_array($stmt1, SQLSRV_FETCH_ASSOC)) {
						// echo '<pre>';
						// print_r($row1);
						// exit;
						$atten_log_data = $row1['AttendanceDate'];
						$attend_date = $atten_log_data->format("Y-m-d");
						//echo $attend_date;exit;
						if($attend_date != '1900-01-01' && $attend_date != '1970-01-01' && $attend_date != '0000-00-00'){
							// echo '<pre>';
							// print_r($row);
							// exit;
							//echo 'aaaa';exit;
							$in_log_data = $row1['InTimeDateTime'];
							$in_date = $in_log_data->format("Y-m-d");
							$in_time = $in_log_data->format("H:i:s");
							if($in_date != '1900-01-01' && $in_date != '1970-01-01' && $in_date != '0000-00-00'){
							} else {
								$in_date = '0000-00-00';
								$in_time = '00:00:00';
							}

							$out_log_data = $row1['OutTimeDateTime'];
							$out_date = $out_log_data->format("Y-m-d");
							$out_time = $out_log_data->format("H:i:s");
							if($out_date != '1900-01-01' && $out_date != '1970-01-01' && $out_date != '0000-00-00'){
							} else {
								$out_date = '0000-00-00';
								$out_time = '00:00:00';
							}

							//echo $in_date.' '.$in_time;
							//echo '<br />';
							//echo $out_date.' '.$out_time;
							//echo '<br />';
							//exit;

							
							$emp_datas = $this->model_transaction_transaction->getempid_by_empcode($emp_code);
							if(isset($emp_datas['employee_id'])){
								$employee_id = $emp_datas['employee_id'];
							} else {
								$employee_id = 0;
							}
							//echo $employee_id;exit;
							$result = $this->model_transaction_transaction->getEmployees_dat($employee_id);
							//echo '<pre>';
							//print_r($result);
							//exit;
							if(isset($result['employee_id'])){
								$sql2 = "SELECT * FROM Shifts WHERE ShiftId = ".$row1['ShiftId']." ";
								$params2 = array();
								$options2 =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
								$stmt2 = sqlsrv_query($conn1, $sql2, $params2, $options2);
								if($stmt2 === false) {
								   // echo'<pre>';
								   // print_r(sqlsrv_errors());
								   // exit;
								}
								$shift_intime = '09:30:00';
								$shift_outtime = '18:00:00';
								while($row2 = sqlsrv_fetch_array($stmt2, SQLSRV_FETCH_ASSOC)) {
									// echo '11111';
									// echo '<br />';
									// echo '<pre>';
									// print_r($row2);
									// echo '<br />';
									if($row2["BeginTime"] != '00:00'){
										$shift_intime = $row2["BeginTime"];
									}
									if($row2["BeginTime"] != '00:00'){
										$shift_outtime = $row2["EndTime"];
									}

									$is_exist = $this->db->query("SELECT * FROM `oc_shift` WHERE `in_time` = '".$shift_intime."' AND `out_time` = '".$shift_outtime."' ");
									if($is_exist->num_rows == 0){
										$shift_name = $shift_intime.' : '.$shift_outtime;
										$shift_insert = "INSERT INTO `oc_shift` SET `name` = '".$shift_name."', `in_time` = '".$shift_intime."', `out_time` = '".$shift_outtime."' ";
										$this->db->query($shift_insert);
									}
								}
								// echo $shift_intime;
								// echo '<br />';
								// echo $shift_outtime;
								// echo '<br />';
								// exit;

								$weekly_off = 0;
								$holiday_id = 0;
								$leave_status = 0;
								$present_status = 0;
								$absent_status = 0;
								$first_half = '';
								$second_half = '';

								$found_emp_code[$emp_code] = $emp_code;
								$emp_name = $result['name'];
								$employee_id = $result['employee_id'];
								$department = $result['department'];
								$department_id = $result['department_id'];
								$unit = $result['unit'];
								$unit_id = $result['unit_id'];
								$designation = $result['designation'];
								$designation_id = $result['designation_id'];
								$act_intime = $in_time;
								$act_outtime = $out_time;
								$date_out = $out_date;
								$date = $in_date;
								$day = date('j', strtotime($attend_date));
								$month = date('n', strtotime($attend_date));
								$year = date('Y', strtotime($attend_date));
								$working_time = '00:00:00';
								if($act_outtime != '00:00:00'){//for getting working time
									$start_date = new DateTime($date.' '.$act_intime);
									$since_start = $start_date->diff(new DateTime($date_out.' '.$act_outtime));
									$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
								} else {
									$working_time = $this->convertToHoursMins($row1['Duration']);
								}
								$late_time = $this->convertToHoursMins($row1['LateBy']);
								$early_time = $this->convertToHoursMins($row1['EarlyBy']);
								$over_time = $this->convertToHoursMins($row1['OverTime']);
								$present_status = $row1['Present'];
								$absent_status = $row1['Absent'];
								if($present_status == '1'){
									$first_half = '1';
									$second_half = '1';
								}
								if($absent_status == '1'){
									$first_half = '0';
									$second_half = '0';
								}
								if($row1['LeaveType'] != ''){
									$leave_status = 1;
									$first_half = 'PL';							
									$second_half = 'PL';							
								}
								if($row1['WeeklyOff'] != '0'){
									$weekly_off = 1;
									$first_half = 'WO';							
									$second_half = 'WO';							
								}
								if($row1['Holiday'] != '0'){
									$holiday_id = 1;
									$first_half = 'HLD';							
									$second_half = 'HLD';							
								}

								$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$employee_id."' AND `date` = '".$attend_date."' ";
								//echo $trans_exist_sql;exit;
								$trans_exist = $this->db->query($trans_exist_sql);
								// echo '<pre>';
								// print_r($trans_exist);
								// exit;
								$this->log->write('Manual Status : ' . $trans_exist->row['manual_status']);
								if($trans_exist->num_rows == 0){
									$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$employee_id."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', over_time = '".$over_time."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$attend_date."', `date_out` = '".$date_out."', `department` = '".$department."', `department_id` = '".$department_id."', `unit` = '".$unit."', `unit_id` = '".$unit_id."', `designation` = '".$designation."', `designation_id` = '".$designation_id."', `group` = '', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `weekly_off` = '".$weekly_off."', `holiday_id` = '".$holiday_id."', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '0'  ";
									$this->db->query($sql);
									$this->log->write($sql);
								} else {
									if($trans_exist->row['manual_status'] == '0'){
										$sql = "UPDATE `oc_transaction` SET `emp_id` = '".$employee_id."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', over_time = '".$over_time."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$attend_date."', `date_out` = '".$date_out."', `department` = '".$department."', `department_id` = '".$department_id."', `unit` = '".$unit."', `unit_id` = '".$unit_id."', `designation` = '".$designation."', `designation_id` = '".$designation_id."', `group` = '', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `weekly_off` = '".$weekly_off."', `holiday_id` = '".$holiday_id."', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '0' WHERE `emp_id` = '".$employee_id."' AND `date` = '".$attend_date."' ";			
										//echo $sql;exit;
										$this->db->query($sql);
										$this->log->write($sql);
									}
								}
							}
						}
					}
					$this->log->write('---Employee End EmployeeCode : '.$row['EmployeeCode'].'----'.' And Date : ' . $filter_date_start.'---');
				}
				//echo 'out';exit;
				$this->load->model('transaction/dayprocess');
				$tran_datas_sql = "SELECT * FROM `oc_transaction` WHERE `date` = '".$filter_date_start."' AND `act_intime` <> '00:00:00' ORDER BY `act_intime` ";
				$tran_datass = $this->db->query($tran_datas_sql);
				$tran_datas_num_rows = $tran_datass->num_rows;				
				$tran_datas = $tran_datass->rows;
				echo '<pre>';
				print_r($tran_datas);
				exit;
				$shift_re_sql = "SELECT * FROM `oc_shift_change`";
				$shift_re = $this->db->query($shift_re_sql)->row;
				$first_in = 0;
				if(isset($shift_re['date'])){
					$shift_change_date = $shift_re['date'];
				} else {
					$first_in = 1;
					$shift_change_date = $filter_date_start;
				}
				$times = '07:00:00';
				$start_date = new DateTime($filter_date_start.' '.$times);
				$since_start = $start_date->diff(new DateTime($filter_date_start.' '.$times));
				$difference_days = $since_start->d;
				if($first_in == 1 || $difference_days > 7){
					$tran_datas_chunk = array_chunk($tran_datas, $shift_count);
					$max_size = count($tran_datas_chunk);
					$random_array = $this->generate_random($max_size, $shift_count);
					$shift_1_data = $tran_datas_chunk[$random_array[0]];	
					$shift_2_data = $tran_datas_chunk[$random_array[1]];	
					$shift_3_data = $tran_datas_chunk[$random_array[2]];	
					$shift_4_data = $tran_datas_chunk[$random_array[3]];	
					$shift_5_data = $tran_datas_chunk[$random_array[4]];	
					foreach ($shift_1_data as $skey => $svalue) {
						$shift_id = '1';
						$shift_intime = '07:00:00';
						$shift_outtime = '15:30:00';
						$act_in_punch_date = $svalue['date'];
						$act_out_punch_date = $svalue['date'];

						$in_datetime = $this->get_all_times($shift_intime);
						$in_random = array_rand($in_datetime);
						$act_intime = $in_datetime[$in_random];

						$out_datetime = $this->get_all_times($shift_outtime);
						$out_random = array_rand($out_datetime);
						$act_outtime = $out_datetime[$out_random];

						$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
						$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
						$late_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
						if($since_start->invert == 1){
							$late_time = '00:00:00';
						}

						$start_date = new DateTime($act_out_punch_date.' '.$shift_outtime);
						$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
						$early_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;					
						$over_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;					
						if($since_start->invert == 0){
							$early_time = '00:00:00';
						} else {
							$over_time = '00:00:00';
						}

						$first_half = '1';
						$second_half = '1';
						$present_status = '1';
						$absent_status = '0';

						$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
						$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
						$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
					
						$update_sql = 	"UPDATE `oc_transaction` SET 
										`shift_id` = '".$shift_id."',
										`shift_intime` = '".$shift_intime."',
										`shift_outtime` = '".$shift_outtime."',
										`manual_status` = '1',
										`act_intime_manipulated` = '".$act_intime."',
										`act_outtime_manipulated` = '".$act_outtime."',
										`late_time_manipulated` = '".$late_time."',
										`early_time_manipulated` = '".$early_time."',
										`over_time_manipulated` = '".$over_time."',
										`firsthalf_status_manipulated	` = '".$first_half."',
										`secondhalf_status_manipulated` = '".$secondhalf_status_manipulated."',
										`present_status_manipulated` = '".$present_status_manipulated."',
										`absent_status_manipulated` = '".$absent_status_manipulated."' ";
						$this->db->query($update_sql);
					}

					foreach ($shift_2_data as $skey => $svalue) {
						$shift_id = '2';
						$shift_intime = '08:30:00';
						$shift_outtime = '17:30:00';
						$act_in_punch_date = $svalue['date'];
						$act_out_punch_date = $svalue['date'];

						$in_datetime = $this->get_all_times($shift_intime);
						$in_random = array_rand($in_datetime);
						$act_intime = $in_datetime[$in_random];

						$out_datetime = $this->get_all_times($shift_outtime);
						$out_random = array_rand($out_datetime);
						$act_outtime = $out_datetime[$out_random];

						$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
						$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
						$late_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
						if($since_start->invert == 1){
							$late_time = '00:00:00';
						}

						$start_date = new DateTime($act_out_punch_date.' '.$shift_outtime);
						$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
						$early_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;					
						$over_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;					
						if($since_start->invert == 0){
							$early_time = '00:00:00';
						} else {
							$over_time = '00:00:00';
						}

						$first_half = '1';
						$second_half = '1';
						$present_status = '1';
						$absent_status = '0';

						$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
						$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
						$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
					
						$update_sql = 	"UPDATE `oc_transaction` SET
										`shift_id` = '".$shift_id."', 
										`shift_intime` = '".$shift_intime."',
										`shift_outtime` = '".$shift_outtime."',
										`manual_status` = '1',
										`act_intime_manipulated` = '".$act_intime."',
										`act_outtime_manipulated` = '".$act_outtime."',
										`late_time_manipulated` = '".$late_time."',
										`early_time_manipulated` = '".$early_time."',
										`over_time_manipulated` = '".$over_time."',
										`firsthalf_status_manipulated	` = '".$first_half."',
										`secondhalf_status_manipulated` = '".$secondhalf_status_manipulated."',
										`present_status_manipulated` = '".$present_status_manipulated."',
										`absent_status_manipulated` = '".$absent_status_manipulated."' ";
						$this->db->query($update_sql);
					}

					foreach ($shift_3_data as $skey => $svalue) {
						$shift_id = '3';
						$shift_intime = '09:30:00';
						$shift_outtime = '18:30:00';
						$act_in_punch_date = $svalue['date'];
						$act_out_punch_date = $svalue['date'];

						$in_datetime = $this->get_all_times($shift_intime);
						$in_random = array_rand($in_datetime);
						$act_intime = $in_datetime[$in_random];

						$out_datetime = $this->get_all_times($shift_outtime);
						$out_random = array_rand($out_datetime);
						$act_outtime = $out_datetime[$out_random];

						$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
						$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
						$late_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
						if($since_start->invert == 1){
							$late_time = '00:00:00';
						}

						$start_date = new DateTime($act_out_punch_date.' '.$shift_outtime);
						$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
						$early_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;					
						$over_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;					
						if($since_start->invert == 0){
							$early_time = '00:00:00';
						} else {
							$over_time = '00:00:00';
						}

						$first_half = '1';
						$second_half = '1';
						$present_status = '1';
						$absent_status = '0';

						$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
						$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
						$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
					
						$update_sql = 	"UPDATE `oc_transaction` SET 
										`shift_id` = '".$shift_id."',
										`shift_intime` = '".$shift_intime."',
										`shift_outtime` = '".$shift_outtime."',
										`manual_status` = '1',
										`act_intime_manipulated` = '".$act_intime."',
										`act_outtime_manipulated` = '".$act_outtime."',
										`late_time_manipulated` = '".$late_time."',
										`early_time_manipulated` = '".$early_time."',
										`over_time_manipulated` = '".$over_time."',
										`firsthalf_status_manipulated	` = '".$first_half."',
										`secondhalf_status_manipulated` = '".$secondhalf_status_manipulated."',
										`present_status_manipulated` = '".$present_status_manipulated."',
										`absent_status_manipulated` = '".$absent_status_manipulated."' ";
						$this->db->query($update_sql);
					}

					foreach ($shift_4_data as $skey => $svalue) {
						$shift_id = '4';
						$shift_intime = '15:00:00';
						$shift_outtime = '23:30:00';
						$act_in_punch_date = $svalue['date'];
						$act_out_punch_date = $svalue['date'];

						$in_datetime = $this->get_all_times($shift_intime);
						$in_random = array_rand($in_datetime);
						$act_intime = $in_datetime[$in_random];

						$out_datetime = $this->get_all_times($shift_outtime);
						$out_random = array_rand($out_datetime);
						$act_outtime = $out_datetime[$out_random];

						$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
						$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
						$late_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
						if($since_start->invert == 1){
							$late_time = '00:00:00';
						}

						$start_date = new DateTime($act_out_punch_date.' '.$shift_outtime);
						$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
						$early_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;					
						$over_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;					
						if($since_start->invert == 0){
							$early_time = '00:00:00';
						} else {
							$over_time = '00:00:00';
						}

						$first_half = '1';
						$second_half = '1';
						$present_status = '1';
						$absent_status = '0';

						$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
						$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
						$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
					
						$update_sql = 	"UPDATE `oc_transaction` SET
										`shift_id` = '".$shift_id."', 
										`shift_intime` = '".$shift_intime."',
										`shift_outtime` = '".$shift_outtime."',
										`manual_status` = '1',
										`act_intime_manipulated` = '".$act_intime."',
										`act_outtime_manipulated` = '".$act_outtime."',
										`late_time_manipulated` = '".$late_time."',
										`early_time_manipulated` = '".$early_time."',
										`over_time_manipulated` = '".$over_time."',
										`firsthalf_status_manipulated	` = '".$first_half."',
										`secondhalf_status_manipulated` = '".$secondhalf_status_manipulated."',
										`present_status_manipulated` = '".$present_status_manipulated."',
										`absent_status_manipulated` = '".$absent_status_manipulated."' ";
						$this->db->query($update_sql);
					}

					foreach ($shift_5_data as $skey => $svalue) {
						$shift_id = '5';
						$shift_intime = '23:00:00';
						$shift_outtime = '07:30:00';
						$act_in_punch_date = $svalue['date'];
						$act_out_punch_date = date('Y-m-d', strtotime($act_in_punch_date . "+1 day"));

						$in_datetime = $this->get_all_times($shift_intime);
						$in_random = array_rand($in_datetime);
						$act_intime = $in_datetime[$in_random];

						$out_datetime = $this->get_all_times($shift_outtime);
						$out_random = array_rand($out_datetime);
						$act_outtime = $out_datetime[$out_random];

						$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
						$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
						$late_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
						if($since_start->invert == 1){
							$late_time = '00:00:00';
						}

						$start_date = new DateTime($act_out_punch_date.' '.$shift_outtime);
						$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
						$early_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;					
						$over_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;					
						if($since_start->invert == 0){
							$early_time = '00:00:00';
						} else {
							$over_time = '00:00:00';
						}

						$first_half = '1';
						$second_half = '1';
						$present_status = '1';
						$absent_status = '0';

						$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
						$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
						$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
					
						$update_sql = 	"UPDATE `oc_transaction` SET 
										`shift_id` = '".$shift_id."',
										`shift_intime` = '".$shift_intime."',
										`shift_outtime` = '".$shift_outtime."',
										`manual_status` = '1',
										`act_intime_manipulated` = '".$act_intime."',
										`act_outtime_manipulated` = '".$act_outtime."',
										`late_time_manipulated` = '".$late_time."',
										`early_time_manipulated` = '".$early_time."',
										`over_time_manipulated` = '".$over_time."',
										`firsthalf_status_manipulated	` = '".$first_half."',
										`secondhalf_status_manipulated` = '".$secondhalf_status_manipulated."',
										`present_status_manipulated` = '".$present_status_manipulated."',
										`absent_status_manipulated` = '".$absent_status_manipulated."' ";
						$this->db->query($update_sql);
					}
				} else {
					foreach ($tran_datas as $skey => $svalue) {
						$prev_filter_date_start = date('Y-m-d', strtotime($svalue['date'] . "-1 day"));
						$prev_shift_sql = "SELECT `shift_id` FROM `oc_transaction` WHERE `date` = '".$prev_filter_date_start."' ";
						$prev_shift_datas = $this->db->query($prev_shift_sql);
						if($prev_shift_datas->num_rows > 0){
							$shift_id = $prev_shift_datas->row['shift_id'];
							$shift_intime = $shift_data[$shift_id]['shift_intime'];
							$shift_outtime = $shift_data[$shift_id]['shift_outtime'];
						} else {
							$shift_id = 1;
							$shift_intime = '07:00:00';
							$shift_outtime = '15:30:00';	
						}
						if($shift_id == 5){
							$act_in_punch_date = $svalue['date'];
							$act_out_punch_date = date('Y-m-d', strtotime($act_in_punch_date . "+1 day"));
						} else {
							$act_in_punch_date = $svalue['date'];
							$act_out_punch_date = $svalue['date'];
						}
						$in_datetime = $this->get_all_times($shift_intime);
						$in_random = array_rand($in_datetime);
						$act_intime = $in_datetime[$in_random];

						$out_datetime = $this->get_all_times($shift_outtime);
						$out_random = array_rand($out_datetime);
						$act_outtime = $out_datetime[$out_random];

						$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
						$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
						$late_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
						if($since_start->invert == 1){
							$late_time = '00:00:00';
						}

						$start_date = new DateTime($act_out_punch_date.' '.$shift_outtime);
						$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
						$early_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;					
						$over_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;					
						if($since_start->invert == 0){
							$early_time = '00:00:00';
						} else {
							$over_time = '00:00:00';
						}

						$first_half = '1';
						$second_half = '1';
						$present_status = '1';
						$absent_status = '0';

						$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
						$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
						$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
					
						$update_sql = 	"UPDATE `oc_transaction` SET 
										`shift_id` = '".$shift_id."',
										`shift_intime` = '".$shift_intime."',
										`shift_outtime` = '".$shift_outtime."',
										`manual_status` = '1',
										`act_intime_manipulated` = '".$act_intime."',
										`act_outtime_manipulated` = '".$act_outtime."',
										`late_time_manipulated` = '".$late_time."',
										`early_time_manipulated` = '".$early_time."',
										`over_time_manipulated` = '".$over_time."',
										`firsthalf_status_manipulated	` = '".$first_half."',
										`secondhalf_status_manipulated` = '".$secondhalf_status_manipulated."',
										`present_status_manipulated` = '".$present_status_manipulated."',
										`absent_status_manipulated` = '".$absent_status_manipulated."' ";
						$this->db->query($update_sql);
					}
					$update_re = "UPDATE `oc_shift_change` SET `date` = '".$filter_date_start."' ";
					$this->db->query($update_re);
				}
				$filter_unit = '';
				$this->model_transaction_dayprocess->update_close_day($filter_date_start, $filter_unit);
			}
			//echo 'out1';exit;
			// $this->load->model('transaction/leaveprocess');
			// $this->load->model('transaction/dayprocess');
			// if (isset($this->request->get['unit'])) {
			// 	$unit = $this->request->get['unit'];
			// } else {
			// 	$unit = '';
			// }
			// $unprocessed = $this->model_transaction_leaveprocess->getUnprocessedLeaveTillDate_2($unit);
			// foreach($unprocessed as $data) {
			// 	$is_closed = $this->model_transaction_dayprocess->is_closed_stat($data['date'], $data['emp_id']); 
			// 	if($is_closed == 1){
			// 		$this->model_transaction_dayprocess->checkLeave($data['date'], $data['emp_id']);
			// 	}
			// }
			$this->session->data['success'] = 'Employee Attendance Updated Sucessfully';
			$this->redirect($this->url->link('snippets/dataprocess_daily', 'token=' . $this->session->data['token'], 'SSL'));
			//echo 'Done';exit;
		}

		if(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->data['token'] = $this->session->data['token'];

		$this->data['filter_month'] = $filter_month;
		$this->data['filter_year'] = $filter_year;
		$this->data['filter_location'] = $filter_location;
		
		$this->template = 'catalog/dataprocess_daily.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	public function get_all_times($time){
		date_default_timezone_set("Asia/Kolkata");
		$minus_start_time = date('H:i:s', strtotime($time . "-15 minutes"));
		$time_array = array();
		for($i=0; $i<15; $i++){
			$time_array[] = date('H:i:s', strtotime($minus_start_time . "+".$i." minutes"));
		}
		for($i=0; $i<=30; $i++){
			$time_array[] = date('H:i:s', strtotime($time . "+".$i." minutes"));
		}
		return $time_array;		
	}

	public function generate_random($length, $max_value){
		$randarray = array(); 
		for($i = 1; $i <= $length;) { 
		    unset($rand); 
		    $rand = rand(1, $max_value); 
		    if(!in_array($rand, $randarray)) { 
		        $randarray[] = $rand; 
		        $i++; 
		    } 
		}
		return $randarray;
	}

	public function convertToHoursMins($time, $format = '%02d:%02d') {
	    if ($time < 1) {
	        return;
	    }
	    $hours = floor($time / 60);
	    $minutes = ($time % 60);
	    return sprintf($format, $hours, $minutes);
	}

	public function sortByOrder($a, $b) {
		$v1 = strtotime($a['fdate']);
		$v2 = strtotime($b['fdate']);
		return $v1 - $v2; // $v2 - $v1 to reverse direction
		// if ($a['punch_date'] == $b['punch_date']) {
			//return ($a['in_time'] > $b['in_time']) ? -1 : 1;;
		//}
		//return $a['punch_date'] - $b['punch_date'];
	}

	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	public function array_sort($array, $on, $order=SORT_ASC){

		$new_array = array();
		$sortable_array = array();

		if (count($array) > 0) {
			foreach ($array as $k => $v) {
				if (is_array($v)) {
					foreach ($v as $k2 => $v2) {
						if ($k2 == $on) {
							$sortable_array[$k] = $v2;
						}
					}
				} else {
					$sortable_array[$k] = $v;
				}
			}

			switch ($order) {
				case SORT_ASC:
					asort($sortable_array);
					break;
				case SORT_DESC:
					arsort($sortable_array);
					break;
			}

			foreach ($sortable_array as $k => $v) {
				$new_array[$k] = $array[$k];
			}
		}

		return $new_array;
	}
}
?>