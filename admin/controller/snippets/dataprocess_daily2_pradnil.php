<?php    
class ControllerSnippetsDataprocessdaily2 extends Controller { 
	private $error = array();

	public function index() {

		//error_reporting(E_ALL);
		//ini_set("display_errors", 1);
		//echo date("h:i:s A");
		/*
		$shift_id = '2';
		$shift_code = 'S2';
		$shift_intime = '16:00:00';
		$shift_outtime = '00:00:00';
		$act_in_punch_date = '2019-01-01';
		$act_out_punch_date = '2019-01-01';
		$svalue['act_intime'] = '08:15:00';
		if($svalue['act_intime'] != "00:00:00"){		
			$in_datetime = $this->get_all_times($shift_intime);
			$in_random = array_rand($in_datetime);
			$act_intime = $in_datetime[$in_random];
			
			$datatime = $this->get_all_timess("08:00:00");
			$random = array_rand($datatime);
			$addtime = $datatime[$random];
		
			$secs = strtotime($act_intime)-strtotime("00:00:00");
			$act_outtime = date("H:i:s",strtotime($addtime)+$secs);
			$act_outtime_hours = date("H",strtotime($addtime)+$secs);
			echo $act_outtime;
			echo '<br />';
			if($act_outtime >= '00' && $act_outtime <= '01'){
				$act_out_punch_date = date('Y-m-d', strtotime($act_out_punch_date . "+1 day"));		
			}
		}
		echo $act_in_punch_date;
		echo '<br />';
		echo $act_out_punch_date;
		echo '<br />';
		exit;

		$shift_id = '3';
		$shift_code = 'S3';
		$shift_intime = '00:00:00';
		$shift_outtime = '08:00:00';
		$act_in_punch_date = '2019-01-01';
		$act_out_punch_date = '2019-01-01';
		$svalue['act_intime'] = '08:15:00';
		if($svalue['act_intime'] != "00:00:00"){		
			$in_datetime = $this->get_all_times($shift_intime);
			$in_random = array_rand($in_datetime);
			$act_intime = $in_datetime[$in_random];
			echo $act_intime;
			echo '<br />';
			$act_intime_hour = date('H', strtotime($act_intime));
			if($act_intime_hour >= '23'){
				$act_in_punch_date = date('Y-m-d', strtotime($act_in_punch_date . "-1 day"));
			}
		}
		echo $act_in_punch_date;
		echo '<br />';
		echo $act_out_punch_date;
		echo '<br />';
		echo 'out';exit;
		*/

		$this->load->model('transaction/transaction');
		$this->load->model('report/common_report');
		$this->load->model('catalog/employee');		
		
		if(isset($this->request->get['filter_month'])){
			$filter_month = $this->request->get['filter_month'];
		} else {
			$filter_month = date('n');
		}

		if(isset($this->request->get['filter_year'])){
			$filter_year = $this->request->get['filter_year'];
		} else {
			$filter_year = date('Y');
		}
		if(isset($this->request->get['filter_location'])){
			$filter_location = $this->request->get['filter_location'];
		} else {
			$filter_location = '';
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Daily Employee Update Attendance'),
			'href'      => $this->url->link('snippets/dataprocess_daily', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$months = array(
			'01' => 'January',
			'02' => 'Feburary',
			'03' => 'March',
			'04' => 'April',
			'05' => 'May',
			'06' => 'June',
			'07' => 'July',
			'08' => 'August',
			'09' => 'September',
			'10' => 'October',
			'11' => 'November',
			'12' => 'December'
		);
		$this->data['months'] = $months;
		
		$years = array(
			'2018' => '2018',
			'2019' => '2019',
			'2020' => '2020',
			'2021' => '2021',
		);
		$this->data['years'] = $years;
		/*echo'<pre>';
		print_r($this->data);
		exit;*/

		$this->load->model('catalog/location');
		$data = array();
		$unit_id = 0;
		$in = 0;
		if($this->user->getUserGroupId() != 1 && $this->user->getUnitId() != '0'){
			$in = 1;
			$unit_id = $this->user->getUnitId();
		}
		$results = $this->model_catalog_location->getLocations($data);
		$unit_datas = array();
		if($in == 0){
			$unit_datas['0'] = 'All';
		}
		foreach($results as $rkey => $rvalue){
			if($in == 1){
				if($unit_id == $rvalue['unit_id']){
					$unit_datas[$rvalue['unit']] = $rvalue['unit'];
				}
			} else {
				$unit_datas[$rvalue['unit']] = $rvalue['unit'];
			}
		}
		$this->data['unit_datas'] = $unit_datas;
		if(isset($this->request->get['once']) && $this->request->get['once'] == '1'){
			//echo 'aaaa';exit;
			
			$stats_sql = "SELECT * FROM `oc_status` WHERE `process_status` = '1' ";
			$stats_data = $this->db->query($stats_sql);
			// echo '<pre>';
			// print_r($stats_data);
			// exit;
			if($stats_data->num_rows == 0){
				$sql = "UPDATE `oc_status` SET `process_status` = '1' ";
				$this->db->query($sql);
				
				$start_date = $filter_year.'-'.$filter_month.'-01';
				$end_date = date("Y-m-t", strtotime($start_date));

				//$start_date = '2019-10-01';
				//$end_date = '2019-10-10';
				// echo $start_date;
				// echo '<br />';
				// echo $end_date;
				
				$day = array();
				$days = $this->GetDays($start_date, $end_date);
				foreach ($days as $dkey => $dvalue) {
					if(strtotime($dvalue) <= strtotime(date('Y-m-d'))){
						$dates = explode('-', $dvalue);
						$day[$dkey]['day'] = $dates[2];
						$day[$dkey]['date'] = $dvalue;
					}
				}
				
				$shift_datas_sql = "SELECT * FROM `oc_shift` WHERE `status` = '1' AND `shift_change` = '1' ";
				$shift_datass = $this->db->query($shift_datas_sql);
				$shift_count = $shift_datass->num_rows;
				
											
				$sql_multiple_query = '';
				$itr = 1;
				$counter_shift_change = 1;
				foreach($day as $dkeys => $dvalues){
					$filter_date_start = $dvalues['date'];
					/*echo "<br />";
					echo'<pre>';
					print_r($filter_date_start);
					echo "<br />";exit;*/
					
					$this->load->model('transaction/dayprocess');
					$tran_datas_sql = "SELECT * FROM `oc_transaction` t LEFT JOIN `oc_employee` e ON (e.`employee_id` = t.`emp_id`) WHERE t.`date` = '".$filter_date_start."' AND t.`emp_type` = '2' AND e.`status` = '1' ORDER BY t.`emp_id` ASC";
					// echo'<pre>';
					// print_r("SELECT * FROM `oc_transaction` t LEFT JOIN `oc_employee` e ON (e.`employee_id` = t.`emp_id`) WHERE t.`date` = '".$filter_date_start."' AND t.`emp_type` = '2' AND e.`status` = '1' ORDER BY t.`emp_id` ASC");
					// exit;
					$tran_datass = $this->db->query($tran_datas_sql);

					$tran_datas_num_rows = $tran_datass->num_rows;				
					$tran_datas = $tran_datass->rows;

					if($tran_datas_num_rows > 0){
						foreach ($tran_datas as $skey => $svalue) {
							$shift_id = $svalue['shift_id'];
							$shift_code = $svalue['shift_code'];
							$shift_intime = $svalue['shift_intime'];
							$shift_outtime = $svalue['shift_outtime'];
							$act_in_punch_date = $svalue['date'];
							$act_out_punch_date = $svalue['date'];
							if($svalue['act_intime'] != "00:00:00"){		
								$in_datetime = $this->get_all_times($shift_intime);
								$in_random = array_rand($in_datetime);
								$act_intime = $in_datetime[$in_random];

								$datatime = $this->get_all_timess("08:00:00");
								$random = array_rand($datatime);
								$addtime = $datatime[$random];
							
								$secs = strtotime($act_intime)-strtotime("00:00:00");
								$act_outtime = date("H:i:s",strtotime($addtime)+$secs);
							
								$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
								$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
								$late_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
								if($since_start->invert == 1){
									$late_time = '00:00:00';
								} else {
									$late_time = '00:00:00';
								}

								$start_date = new DateTime($act_out_punch_date.' '.$shift_outtime);
								$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
								$early_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;					
								$over_time = '00:00:00';//$since_start->h.':'.$since_start->i.':'.$since_start->s;					
								if($since_start->invert == 0){
									$early_time = '00:00:00';
								} else {
									$early_time = '00:00:00';
									$over_time = '00:00:00';
								}

								$early_time = '00:00:00';
								$late_time = '00:00:00';
								$over_time = '00:00:00';

								$first_half = 1;
								$second_half = 1;
								$present_status = 1;
								$absent_status = 0;

								$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
								$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
								$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
							} else {
								$act_intime = '00:00:00';	
								$act_outtime = '00:00:00';	
								$late_time = '00:00:00';	
								$early_time = '00:00:00';	
								$over_time = '00:00:00';	
								$working_time = '00:00:00';	
								$first_half = 0;
								$second_half = 0;
								$present_status = 0;
								$absent_status = 1;
							}
							if($act_in_punch_date == $act_out_punch_date){
								$act_out_punch_date =  date('Y-m-d', strtotime($act_in_punch_date . ' +1 day'))	;
							    $start_date = new DateTime($act_in_punch_date.' '.$act_intime);
								$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
								$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
							}
							$update_sql = 	"UPDATE `oc_transaction` SET 
											`shift_id_manipulated` = '".$shift_id."',
											`shift_code_manipulated` = '".$shift_code."',
											`shift_intime_manipulated` = '".$shift_intime."',
											`shift_outtime_manipulated` = '".$shift_outtime."',
											`manual_status` = '1',
											`date_out_manipulated` = '".$act_out_punch_date."',
											`act_intime_manipulated` = '".$act_intime."',
											`act_outtime_manipulated` = '".$act_outtime."',
											`late_time_manipulated` = '".$late_time."',
											`early_time_manipulated` = '".$early_time."',
											`over_time_manipulated` = '".$over_time."',
											`working_time_manipulated` = '".$working_time."',
											`firsthalf_status_manipulated` = '".$first_half."',
											`secondhalf_status_manipulated` = '".$second_half."',
											`present_status_manipulated` = '".$present_status."',
											`absent_status_manipulated` = '".$absent_status."' 
											WHERE `transaction_id` = '".$svalue['transaction_id']."'; ";
							$sql_multiple_query = $sql_multiple_query.$update_sql;
							//$this->db->query($update_sql);
							//echo $sql_multiple_query;echo "<br>";echo "<br>";
							
						}
					}
					
					$tran_datas_sql = "SELECT * FROM `oc_transaction` t LEFT JOIN `oc_employee` e ON (e.`employee_id` = t.`emp_id`) WHERE t.`date` = '".$filter_date_start."' AND t.`emp_type` = '1' AND t.`act_intime` <> '00:00:00' AND e.`status` = '1' ORDER BY t.`emp_id` ASC";
					$tran_datass = $this->db->query($tran_datas_sql);
					$tran_datas_num_rows = $tran_datass->num_rows;				
					$tran_datas = $tran_datass->rows;

					foreach ($tran_datas as $skey => $svalue) {
						$working_time = $svalue['working_time'];
						if(strtotime($working_time) > strtotime('09:15:00')){
							$shift_intime = $svalue['shift_intime'];
							$shift_outtime = $svalue['shift_outtime'];
							
							$act_intime = $svalue['act_intime'];
							$act_in_punch_date = $svalue['date'];
							$act_out_punch_date = $svalue['date_out'];

							$datatime = $this->get_all_timess("09:00:00");
							$random = array_rand($datatime);
							$addtime = $datatime[$random];
						
							$secs = strtotime($act_intime)-strtotime("00:00:00");
							$act_outtime = date("H:i:s",strtotime($addtime)+$secs);

							if($act_outtime >= '00:00:00' && $act_outtime <= '03:00:00'){
								$act_out_punch_date = date('Y-m-d', strtotime($act_out_punch_date . "+1 day"));							
							}

							$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
							$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
							$late_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
							if($since_start->invert == 1){
								$late_time = '00:00:00';
							}
							$late_time = '00:00:00';

							$start_date = new DateTime($act_out_punch_date.' '.$shift_outtime);
							$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
							$early_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;					
							$over_time = '00:00:00';//$since_start->h.':'.$since_start->i.':'.$since_start->s;					
							if($since_start->invert == 0){
								$early_time = '00:00:00';
							} else {
								$over_time = '00:00:00';
							}
							$early_time = '00:00:00';
							$over_time = '00:00:00';

							$first_half = 1;
							$second_half = 1;
							$present_status = 1;
							$absent_status = 0;

							$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
							$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
							$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
							
							$update_sql = 	"UPDATE `oc_transaction` SET 
												`manual_status` = '1',
												`act_intime_manipulated` = '".$act_intime."',
												`act_outtime_manipulated` = '".$act_outtime."',
												`late_time_manipulated` = '".$late_time."',
												`early_time_manipulated` = '".$early_time."',
												`over_time_manipulated` = '".$over_time."',
												`working_time_manipulated` = '".$working_time."',
												`firsthalf_status_manipulated` = '".$first_half."',
												`secondhalf_status_manipulated` = '".$second_half."',
												`present_status_manipulated` = '".$present_status."',
												`absent_status_manipulated` = '".$absent_status."',
												`date_out_manipulated` = '".$act_out_punch_date."' 
											WHERE `transaction_id` = '".$svalue['transaction_id']."'; ";
							$sql_multiple_query = $sql_multiple_query.$update_sql;
							//$this->db->query($update_sql);
											//echo $update_sql;
						} else {
							$shift_intime = $svalue['shift_intime'];
							$shift_outtime = $svalue['shift_outtime'];
							
							$act_intime = $svalue['act_intime'];
							$act_in_punch_date = $svalue['date'];
							$act_outtime = $svalue['act_outtime'];
							$act_out_punch_date = $svalue['date_out'];
							$working_time = $svalue['working_time'];
							$late_time = $svalue['late_time'];
							$early_time = $svalue['early_time'];
							$over_time = $svalue['over_time'];
							$first_half = $svalue['firsthalf_status'];
							$second_half = $svalue['secondhalf_status'];
							$present_status = $svalue['present_status'];
							$absent_status = $svalue['absent_status'];

							$late_time = '00:00:00';
							$early_time = '00:00:00';
							$over_time = '00:00:00';

							$update_sql = 	"UPDATE `oc_transaction` SET 
												`manual_status` = '1',
												`act_intime_manipulated` = '".$act_intime."',
												`act_outtime_manipulated` = '".$act_outtime."',
												`late_time_manipulated` = '".$late_time."',
												`early_time_manipulated` = '".$early_time."',
												`over_time_manipulated` = '".$over_time."',
												`working_time_manipulated` = '".$working_time."',
												`firsthalf_status_manipulated` = '".$first_half."',
												`secondhalf_status_manipulated` = '".$second_half."',
												`present_status_manipulated` = '".$present_status."',
												`absent_status_manipulated` = '".$absent_status."',
												`date_out_manipulated` = '".$act_out_punch_date."' 
											WHERE `transaction_id` = '".$svalue['transaction_id']."'; ";
							$sql_multiple_query = $sql_multiple_query.$update_sql;
							//$this->db->query($update_sql);
											//echo $update_sql;
						}
					}
					$filter_unit = '';
					//$this->model_transaction_dayprocess->update_close_day($filter_date_start, $filter_unit);
				}
				 //echo $sql_multiple_query; 
				$sql = "UPDATE `oc_status` SET `process_status` = '0' ";
				$sql_multiple_query = $sql_multiple_query.$sql;
				
				$con=mysqli_connect("localhost","root","","db_attendance_mustang");
				$done = mysqli_multi_query($con,$sql_multiple_query);


				mysqli_close($con);

				if($done){
					$this->session->data['success'] = 'Employee Attendance Updated Sucessfully';
				}
				
				//$this->db->query($sql);
				
				$this->redirect($this->url->link('snippets/dataprocess_daily2', 'token=' . $this->session->data['token'], 'SSL'));
			} else {
				$this->session->data['warning'] = 'Attendance Process Already Running';
				$this->redirect($this->url->link('snippets/dataprocess_daily2', 'token=' . $this->session->data['token'], 'SSL'));
			}
			
		}

		if(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->data['token'] = $this->session->data['token'];

		$this->data['filter_month'] = $filter_month;
		$this->data['filter_year'] = $filter_year;
		$this->data['filter_location'] = $filter_location;
		
		$this->template = 'catalog/dataprocess_daily2.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	public function get_all_times($time){
		date_default_timezone_set("Asia/Kolkata");
		$minus_start_time = date('H:i:s', strtotime($time . "-15 minutes"));
		$time_array = array();
		for($i=0; $i<15; $i++){
			$time_array[] = date('H:i:s', strtotime($minus_start_time . "+".$i." minutes"));
		}
		for($i=0; $i<=30; $i++){
			$time_array[] = date('H:i:s', strtotime($time . "+".$i." minutes"));
		}
		return $time_array;		
	}

	public function get_all_timess($time){
		for($i=0; $i<=15; $i++){
			$time_array[] = date('H:i:s', strtotime($time . "+".$i." minutes"));
		}
		return $time_array;
	}

	public function generate_random($length, $max_value){
		$randarray = array(); 
		for($i = 1; $i <= $length;) { 
		    unset($rand); 
		    $rand = rand(1, $max_value); 
		    $rand = $rand - 1;
		    if(!in_array($rand, $randarray)) { 
		        $randarray[] = $rand; 
		        $i++; 
		    } 
		}
		return $randarray;
	}

	public function convertToHoursMins($time, $format = '%02d:%02d') {
	    if ($time < 1) {
	        return;
	    }
	    $hours = floor($time / 60);
	    $minutes = ($time % 60);
	    return sprintf($format, $hours, $minutes);
	}

	public function sortByOrder($a, $b) {
		$v1 = strtotime($a['fdate']);
		$v2 = strtotime($b['fdate']);
		return $v1 - $v2; // $v2 - $v1 to reverse direction
		// if ($a['punch_date'] == $b['punch_date']) {
			//return ($a['in_time'] > $b['in_time']) ? -1 : 1;;
		//}
		//return $a['punch_date'] - $b['punch_date'];
	}

	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	public function array_sort($array, $on, $order=SORT_ASC){

		$new_array = array();
		$sortable_array = array();

		if (count($array) > 0) {
			foreach ($array as $k => $v) {
				if (is_array($v)) {
					foreach ($v as $k2 => $v2) {
						if ($k2 == $on) {
							$sortable_array[$k] = $v2;
						}
					}
				} else {
					$sortable_array[$k] = $v;
				}
			}

			switch ($order) {
				case SORT_ASC:
					asort($sortable_array);
					break;
				case SORT_DESC:
					arsort($sortable_array);
					break;
			}

			foreach ($sortable_array as $k => $v) {
				$new_array[$k] = $array[$k];
			}
		}

		return $new_array;
	}

	public function resetprocess(){
		$sql = "UPDATE `oc_status` SET `process_status` = '0' ";
		$this->db->query($sql);

		$this->session->data['success'] = 'Process Reset Sucessfully';
		$this->redirect($this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'));	
	}
}
?>