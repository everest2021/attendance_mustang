<?php    
class ControllerSnippetsDataprocessempty extends Controller { 
	private $error = array();

	public function index() {
		$this->load->model('transaction/transaction');
		$this->load->model('report/common_report');
		$this->load->model('catalog/employee');		
		
		$batch_id = '';
		$current_months = date('n');
		if($current_months == 1){
			$prev_month = 1;
		} else {
			$prev_month = $current_months;	
		}
		if($current_months == 12){
			$current_month = $current_months;
		} else {
			$current_month = $current_months;	
		}
		if($current_months == 1){
			$prev_year = date('Y');
			$current_year = date('Y');
		} else {
			$prev_year = date('Y');
			$current_year = date('Y');
		}
		$prev_day = 1;
		$prev_month = sprintf('%02d', $prev_month);
		$prev_day = sprintf('%02d', $prev_day);
		$next_date = $current_year.'-'.$current_month.'-01';
		$start_date = $prev_year.'-'.$prev_month.'-'.$prev_day;
		$end_date = date("Y-m-t", strtotime($next_date));
		//$start_date = '2020-08-01';//date('Y-m-d');
		//$end_date = '2020-08-31';//date('Y-m-d', strtotime($start_date . ' +30 day'));
		
	/*	echo $start_date;
		echo '<br />';
		echo $end_date;
		echo '<br />';
		exit;*/
		
		$day = array();
		$days = $this->GetDays($start_date, $end_date);
		foreach ($days as $dkey => $dvalue) {
			$dates = explode('-', $dvalue);
			$day[$dkey]['day'] = $dates[2];
			$day[$dkey]['date'] = $dvalue;
		}
		// echo '<pre>';
		// print_r($day);
		// exit;
		$filter_unit = '';
		$insert_query = '';
		foreach($day as $dkeys => $dvalues){
			$filter_date_start = $dvalues['date'];
			$data = array();
			$data['unit'] = $filter_unit;
			$results = $this->model_report_common_report->getemployees($data);
			foreach ($results as $rkey => $rvalue) {
				$emp_name = $rvalue['name'];
				$department = $rvalue['department'];
				$department_id = $rvalue['department_id'];
				$unit = $rvalue['unit'];
				$unit_id = $rvalue['unit_id'];
				$designation = $rvalue['designation'];
				$designation_id = $rvalue['designation_id'];
				$group = $rvalue['group'];

				if($rvalue['shift_type'] == 'F'){
					$emp_type = 1;
				} else {
					$emp_type = 2;
				}

				$day_date = date('j', strtotime($filter_date_start));
				$month = date('n', strtotime($filter_date_start));
				$year = date('Y', strtotime($filter_date_start));

				$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$rvalue['employee_id']."' AND `month` = '".$month."' AND `year` = '".$year."' ";
				//$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `month` ='".$month."' AND `year` = '".$year."' AND `emp_code` = '".$rvalue['employee_id']."' ";
				$shift_schedule = $this->db->query($update3)->row;
				$schedule_raw = explode('_', $shift_schedule[$day_date]);
				if(!isset($schedule_raw[2])){
					$schedule_raw[2]= 1;
				}
				if($schedule_raw[0] == 'S'){
					$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[1]);
					if(isset($shift_data['shift_id'])){
						$shift_id = $shift_data['shift_id'];
						$shift_code = $shift_data['shift_code'];
						$shift_intime = $shift_data['in_time'];
						$shift_outtime = $shift_data['out_time'];
						
						$day = date('j', strtotime($filter_date_start));
						$month = date('n', strtotime($filter_date_start));
						$year = date('Y', strtotime($filter_date_start));

						$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['employee_id']."' AND `date` = '".$filter_date_start."' ";
						$trans_exist = $this->db->query($trans_exist_sql);
						if($trans_exist->num_rows == 0){
							$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvalue['employee_id']."', `emp_name` = '".$emp_name."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `department_id` = '".$department_id."', `unit` = '".$unit."', `unit_id` = '".$unit_id."', `designation` = '".$designation."', `designation_id` = '".$designation_id."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', emp_type = '".$emp_type."'; ";
							//echo $sql.';';
							//echo '<br />';
							//$this->db->query($sql);
							$insert_query = $insert_query.$sql;
						} else {
							$sql = "UPDATE `oc_transaction` SET `emp_id` = '".$rvalue['employee_id']."', `emp_name` = '".$emp_name."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `department_id` = '".$department_id."', `unit` = '".$unit."', `unit_id` = '".$unit_id."', `designation` = '".$designation."', `designation_id` = '".$designation_id."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', emp_type = '".$emp_type."' WHERE `transaction_id` = '".$trans_exist->row['transaction_id']."';";
							//echo $sql.';';
							//echo '<br />';
							//$this->db->query($sql);
						}
					} else {
						$shift_data = $this->model_transaction_transaction->getshiftdata('1');
						$shift_id = $shift_data['shift_id'];
						$shift_code = $shift_data['shift_code'];
						$shift_intime = $shift_data['in_time'];
						$shift_outtime = $shift_data['out_time'];
						
						$day = date('j', strtotime($filter_date_start));
						$month = date('n', strtotime($filter_date_start));
						$year = date('Y', strtotime($filter_date_start));

						$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['employee_id']."' AND `date` = '".$filter_date_start."' ";
						$trans_exist = $this->db->query($trans_exist_sql);
						if($trans_exist->num_rows == 0){
							$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvalue['employee_id']."', `emp_name` = '".$emp_name."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `department_id` = '".$department_id."', `unit` = '".$unit."', `unit_id` = '".$unit_id."', `designation` = '".$designation."', `designation_id` = '".$designation_id."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', emp_type = '".$emp_type."'; ";
							//echo $sql.';';
							//echo '<br />';
							//$this->db->query($sql);
							$insert_query = $insert_query.$sql;
						} else {
							$sql = "UPDATE `oc_transaction` SET `emp_id` = '".$rvalue['employee_id']."', `emp_name` = '".$emp_name."', `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `department_id` = '".$department_id."', `unit` = '".$unit."', `unit_id` = '".$unit_id."', `designation` = '".$designation."', `designation_id` = '".$designation_id."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', emp_type = '".$emp_type."' WHERE `transaction_id` = '".$trans_exist->row['transaction_id']."'; ";
							//echo $sql.';';
							//echo '<br />';
							//$this->db->query($sql);
						}
					}
				} elseif ($schedule_raw[0] == 'W') {
					$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[2]);
					if(!isset($shift_data['shift_id'])){
						$shift_data = $this->model_transaction_transaction->getshiftdata('1');
					}
					$shift_id = $shift_data['shift_id'];
					$shift_code = $shift_data['shift_code'];
					$shift_intime = $shift_data['in_time'];
					$shift_outtime = $shift_data['out_time'];

					$act_intime = '00:00:00';
					$act_outtime = '00:00:00';
					$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['employee_id']."' AND `date` = '".$filter_date_start."' ";
					$trans_exist = $this->db->query($trans_exist_sql);
					
					if($trans_exist->num_rows == 0){
						$sql = "INSERT INTO `oc_transaction` SET `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['employee_id']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `department_id` = '".$department_id."', `unit` = '".$unit."', `unit_id` = '".$unit_id."', `designation` = '".$designation."', `designation_id` = '".$designation_id."', `group` = '".$group."', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', emp_type = '".$emp_type."'; ";
						//echo $sql.';';
						//echo '<br />';
						//$this->db->query($sql);
						$insert_query = $insert_query.$sql;
					} else {
						$sql = "UPDATE `oc_transaction` SET `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['employee_id']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `department_id` = '".$department_id."', `unit` = '".$unit."', `unit_id` = '".$unit_id."', `designation` = '".$designation."', `designation_id` = '".$designation_id."', `group` = '".$group."', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', emp_type = '".$emp_type."' WHERE `emp_id` = '".$rvalue['employee_id']."' AND `date` = '".$filter_date_start."' ";
						//echo $sql.';';
						//echo '<br />';
						//$this->db->query($sql);
					}
				} elseif ($schedule_raw[0] == 'H') {
					$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[2]);
					if(!isset($shift_data['shift_id'])){
						$shift_data = $this->model_transaction_transaction->getshiftdata('1');
					}
					$shift_id = $shift_data['shift_id'];
					$shift_code = $shift_data['shift_code'];
					$shift_intime = $shift_data['in_time'];
					$shift_outtime = $shift_data['out_time'];

					$act_intime = '00:00:00';
					$act_outtime = '00:00:00';
					
					$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['employee_id']."' AND `date` = '".$filter_date_start."' ";
					$trans_exist = $this->db->query($trans_exist_sql);
					if($trans_exist->num_rows == 0){
						$sql = "INSERT INTO `oc_transaction` SET `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['employee_id']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `department_id` = '".$department_id."', `unit` = '".$unit."', `unit_id` = '".$unit_id."', `designation` = '".$designation."', `designation_id` = '".$designation_id."', `group` = '".$group."', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `weekly_off` = '0', `holiday_id` = '".$schedule_raw[1]."', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', emp_type = '".$emp_type."';  ";
						//echo $sql.';';
						//echo '<br />';
						//$this->db->query($sql);
						$insert_query = $insert_query.$sql;
					} else {
						$sql = "UPDATE `oc_transaction` SET `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['employee_id']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `department_id` = '".$department_id."', `unit` = '".$unit."', `unit_id` = '".$unit_id."', `designation` = '".$designation."', `designation_id` = '".$designation_id."', `group` = '".$group."', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `weekly_off` = '0', `holiday_id` = '".$schedule_raw[1]."', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', emp_type = '".$emp_type."' WHERE `emp_id` = '".$rvalue['employee_id']."' AND `date` = '".$filter_date_start."' ";
						//echo $sql.';';
						//echo '<br />';
						//$this->db->query($sql);
					}
				} elseif ($schedule_raw[0] == 'HD') {
					$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[2]);
					if(!isset($shift_data['shift_id'])){
						$shift_data = $this->model_transaction_transaction->getshiftdata('1');
					}
					$shift_id = $shift_data['shift_id'];
					$shift_code = $shift_data['shift_code'];
					$shift_intime = $shift_data['in_time'];
					$shift_outtime = Date('H:i:s', strtotime($shift_intime .' +4 hours'));
					//$shift_outtime = $shift_data['out_time'];

					$act_intime = '00:00:00';
					$act_outtime = '00:00:00';
					
					$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['employee_id']."' AND `date` = '".$filter_date_start."' ";
					$trans_exist = $this->db->query($trans_exist_sql);
					if($trans_exist->num_rows == 0){
						$sql = "INSERT INTO `oc_transaction` SET `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['employee_id']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `department_id` = '".$department_id."', `unit` = '".$unit."', `unit_id` = '".$unit_id."', `designation` = '".$designation."', `designation_id` = '".$designation_id."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '".$schedule_raw[1]."', batch_id = '".$batch_id."', emp_type = '".$emp_type."'; ";
						//echo $sql.';';
						//echo '<br />';
						//$this->db->query($sql);
						$insert_query = $insert_query.$sql;
					} else {
						$sql = "UPDATE `oc_transaction` SET `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['employee_id']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `department_id` = '".$department_id."', `unit` = '".$unit."', `unit_id` = '".$unit_id."', `designation` = '".$designation."', `designation_id` = '".$designation_id."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '".$schedule_raw[1]."', batch_id = '".$batch_id."', emp_type = '".$emp_type."' WHERE `emp_id` = '".$rvalue['employee_id']."' AND `date` = '".$filter_date_start."' ";
						//echo $sql.';';
						//echo '<br />';
						//$this->db->query($sql);
					}
				}  elseif ($schedule_raw[0] == 'C') {
					$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[2]);
					if(!isset($shift_data['shift_id'])){
						$shift_data = $this->model_transaction_transaction->getshiftdata('1');
					}
					$shift_id = $shift_data['shift_id'];
					$shift_code = $shift_data['shift_code'];
					$shift_intime = $shift_data['in_time'];
					$shift_outtime = $shift_data['out_time'];

					$act_intime = '00:00:00';
					$act_outtime = '00:00:00';
					
					$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['employee_id']."' AND `date` = '".$filter_date_start."' ";
					$trans_exist = $this->db->query($trans_exist_sql);
					if($trans_exist->num_rows == 0){
						$sql = "INSERT INTO `oc_transaction` SET `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['employee_id']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `department_id` = '".$department_id."', `unit` = '".$unit."', `unit_id` = '".$unit_id."', `designation` = '".$designation."', `designation_id` = '".$designation_id."', `group` = '".$group."', `firsthalf_status` = 'COF', `secondhalf_status` = 'COF', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', compli_status = '".$schedule_raw[1]."', batch_id = '".$batch_id."', emp_type = '".$emp_type."'; ";
						//echo $sql.';';
						//echo '<br />';
						//$this->db->query($sql);
						$insert_query = $insert_query.$sql;
					} else {
						$sql = "UPDATE `oc_transaction` SET `shift_id` = '".$shift_id."', `shift_code` = '".$shift_code."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['employee_id']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `department_id` = '".$department_id."', `unit` = '".$unit."', `unit_id` = '".$unit_id."', `designation` = '".$designation."', `designation_id` = '".$designation_id."', `group` = '".$group."', `firsthalf_status` = 'COF', `secondhalf_status` = 'COF', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', compli_status = '".$schedule_raw[1]."', batch_id = '".$batch_id."', emp_type = '".$emp_type."' WHERE `emp_id` = '".$rvalue['employee_id']."' AND `date` = '".$filter_date_start."' ";
						//echo $sql.';';
						//echo '<br />';
						//$this->db->query($sql);
					}
				}
			}
		}

		$sql = "UPDATE `oc_status` SET `process_status` = '0' ";
		$insert_query = $insert_query.$sql;
		//echo $insert_query;
		$con=mysqli_connect("localhost","root","","db_attendance_mustang");
		mysqli_multi_query($con,$insert_query);
		mysqli_close($con);

		$this->session->data['success'] = 'Blank Data Insertion process takes 5 min, So pls  start the next process after that';
		$this->redirect($this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'));
		//echo 'Done';exit;
		$this->template = 'catalog/dataprocess.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	public function convertToHoursMins($time, $format = '%02d:%02d') {
	    if ($time < 1) {
	        return;
	    }
	    $hours = floor($time / 60);
	    $minutes = ($time % 60);
	    return sprintf($format, $hours, $minutes);
	}

	public function sortByOrder($a, $b) {
		$v1 = strtotime($a['fdate']);
		$v2 = strtotime($b['fdate']);
		return $v1 - $v2; // $v2 - $v1 to reverse direction
		// if ($a['punch_date'] == $b['punch_date']) {
			//return ($a['in_time'] > $b['in_time']) ? -1 : 1;;
		//}
		//return $a['punch_date'] - $b['punch_date'];
	}

	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	public function array_sort($array, $on, $order=SORT_ASC){

		$new_array = array();
		$sortable_array = array();

		if (count($array) > 0) {
			foreach ($array as $k => $v) {
				if (is_array($v)) {
					foreach ($v as $k2 => $v2) {
						if ($k2 == $on) {
							$sortable_array[$k] = $v2;
						}
					}
				} else {
					$sortable_array[$k] = $v;
				}
			}

			switch ($order) {
				case SORT_ASC:
					asort($sortable_array);
					break;
				case SORT_DESC:
					arsort($sortable_array);
					break;
			}

			foreach ($sortable_array as $k => $v) {
				$new_array[$k] = $array[$k];
			}
		}

		return $new_array;
	}
}
?>