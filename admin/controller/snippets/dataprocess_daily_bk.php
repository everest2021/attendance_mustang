<?php    
class ControllerSnippetsDataprocessdaily extends Controller { 
	private $error = array();

	public function index() {
		$this->load->model('transaction/transaction');
		$this->load->model('report/common_report');
		$this->load->model('catalog/employee');		
		
		$batch_ids = $this->db->query("SELECT `batch_id` FROM `oc_transaction` ORDER BY `batch_id` DESC LIMIT 1");
		if($batch_ids->num_rows > 0){
			$batch_id = $batch_ids->row['batch_id'] + 1;
		} else {
			$batch_id = 1;
		}
			
		$device_last_dates = $this->db->query("SELECT `date` FROM `oc_transaction` WHERE `emp_type` = '2' AND `day_close_status` = '1' ORDER BY `date` DESC LIMIT 1");
		if($device_last_dates->num_rows > 0){
			$device_last_date = $device_last_dates->row['date'];
		} else {
			$device_last_date = '2017-03-31';
		}
		$current_date = date('Y-m-d');
		$next_date = date('Y-m-d', strtotime($device_last_date . ' +1 day'));
		//$next_date = '2017-05-02';
		//$today_date = date('Y-m-d', strtotime($current_date . ' -1 day'));
		$today_date = '2017-05-31';
		// echo $next_date;
		// echo '<br />';
		// echo $today_date;
		$day = array();
		if(strtotime($today_date) > strtotime($next_date)){
			$days = $this->GetDays($next_date, $today_date);
			foreach ($days as $dkey => $dvalue) {
				$dates = explode('-', $dvalue);
				$day[$dkey]['day'] = $dates[2];
				$day[$dkey]['date'] = $dvalue;
			}
		} else {
			if(strtotime($next_date) == strtotime($today_date)){
				$dates = explode('-', $today_date);
				$day[0]['day'] = $dates[2];
				$day[0]['date'] = $today_date;
			}
		}
		//$time = $this->convertToHoursMins('360');
		// echo '<pre>';
		// print_r($day);
		// exit;
		$mdbFilename = 'Z:\Avi infotech Backup\eTimeTrackLite1.mdb';
		//$mdbFilename = DIR_DOWNLOAD.'eTimeTrackLite1.mdb';
		$user = '';
		$password = '';
		$connection = odbc_connect("Driver={Microsoft Access Driver (*.mdb)};Dbq=$mdbFilename", $user, $password);
		foreach($day as $dkeys => $dvalues){
			$filter_date_start = $dvalues['date'];
			
			$sql = "SELECT * FROM AttendanceLogs WHERE Format(AttendanceDate, 'yyyy-mm-dd') = '".$filter_date_start."' ";
			//echo $sql;exit;
			$rs = odbc_exec($connection, $sql);
			//$results = odbc_fetch_array($rs);
			$rev_exp_datas = array();
			$found_emp_code = array();
			while($row = (odbc_fetch_array($rs))){
				$atten_log_data = $row['AttendanceDate'];
				$attend_exp_date = explode(' ', $atten_log_data);
				$attend_date = $attend_exp_date[0];
				if($attend_date != '1900-01-01' && $attend_date != '1970-01-01' && $attend_date != '0000-00-00'){
					$emp_id = $row['EmployeeId'];
					$sql1 = "SELECT * FROM Employees WHERE EmployeeId = ".$emp_id." ";
					$rs1 = odbc_exec($connection, $sql1);
					$emp_code = 0;
					while ($row1 = odbc_fetch_row($rs1)) {
						$emp_codes = odbc_result($rs1, "EmployeeCode");
						$emp_code_start = substr($emp_codes, 0, 1);
						if(strtolower($emp_code_start) == '2'){
							//$emp_code = substr($emp_codes, 1);
							//$emp_code = sprintf('%03d', $emp_code);
							$emp_code = $emp_codes;
						}
					}
					if($emp_code != 0){
						$out_log_data = $row['OutTime'];
						$out_exp_date = explode(' ', $out_log_data);
						$out_time = $out_exp_date[1];
						$out_date = $out_exp_date[0];
						
						$in_log_data = $row['InTime'];
						$in_exp_date = explode(' ', $in_log_data);
						$in_time = $in_exp_date[1];
						$in_date = $attend_date;
						
						$result = $this->model_transaction_transaction->getEmployees_dat($emp_code);
						if(isset($result['emp_code'])){
							$shift_intime = '09:00:00';
							$shift_outtime = '18:00:00';
							$weekly_off = 0;
							$holiday_id = 0;
							$leave_status = 0;
							$present_status = 0;
							$absent_status = 0;
							$first_half = '';
							$second_half = '';

							$found_emp_code[$emp_code] = $emp_code;
							$emp_name = $result['name'];
							$department = $result['department'];
							$unit = $result['unit'];
							$act_intime = $in_time;
							$act_outtime = $out_time;
							$date_out = $out_date;
							$date = $in_date;
							$day = date('j', strtotime($in_date));
							$month = date('n', strtotime($in_date));
							$year = date('Y', strtotime($in_date));
							$working_time = '00:00:00';
							if($act_outtime != '00:00:00'){//for getting working time
								$start_date = new DateTime($date.' '.$act_intime);
								$since_start = $start_date->diff(new DateTime($date_out.' '.$act_outtime));
								$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
							} else {
								$working_time = $this->convertToHoursMins($row['Duration']);
							}
							$late_time = $this->convertToHoursMins($row['LateBy']);
							$early_time = $this->convertToHoursMins($row['EarlyBy']);
							$overtime = $this->convertToHoursMins($row['OverTime']);
							$present_status = $row['Present'];
							$absent_status = $row['Absent'];
							if($present_status == '1'){
								$first_half = '1';
								$second_half = '1';
							}
							if($absent_status == '1'){
								$first_half = '0';
								$second_half = '0';
							}
							if($row['LeaveType'] != ''){
								$leave_status = 1;
								$first_half = 'PL';							
								$second_half = 'PL';							
							}
							if($row['WeeklyOff'] != '0'){
								$weekly_off = 1;
								$first_half = 'WO';							
								$second_half = 'WO';							
							}
							if($row['Holiday'] != '0'){
								$holiday_id = 1;
								$first_half = 'HLD';							
								$second_half = 'HLD';							
							}

							$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$emp_code."' AND `date` = '".$date."' ";
							$trans_exist = $this->db->query($trans_exist_sql);
							if($trans_exist->num_rows == 0){
								$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$emp_code."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$date."', `date_out` = '".$date_out."', `department` = '".$department."', `unit` = '".$unit."', `group` = '', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `weekly_off` = '".$weekly_off."', `holiday_id` = '".$holiday_id."', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '0', overtime = '".$overtime."', emp_type = '2'  ";
								$this->db->query($sql);
							} else {
								if($trans_exist->row['manual_status'] == '0'){
									$sql = "UPDATE `oc_transaction` SET `emp_id` = '".$emp_code."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$date."', `date_out` = '".$date_out."', `department` = '".$department."', `unit` = '".$unit."', `group` = '', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `weekly_off` = '".$weekly_off."', `holiday_id` = '".$holiday_id."', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '0', overtime = '".$overtime."', emp_type = '2' WHERE `emp_id` = '".$emp_code."' AND `date` = '".$date."' ";			
									$this->db->query($sql);
								}
							}
							//$this->log->write($sql);

							// $employees_proc[] = array(
							// 	'employee_id' => $emp_code,//$result['emp_code'],
							// 	'card_id' => $emp_code,//$result['card_number'],
							// 	'shift_intime' => $shift_intime,
							// 	'shift_outtime' => $shift_outtime,
							// 	'act_intime' => $in_time,
							// 	'act_outtime' => $out_time,
							// 	'weekly_off' => $weekly_off,
							// 	'holiday_id' => $holiday_id,
							// 	'late_time' => $late_time,
							// 	'early_time' => $early_time,
							// 	'working_time' => $working_time,
							// 	'day' => $day,
							// 	'month' => $month,
							// 	'year' => $year,
							// 	'date' => $in_date,
							// 	'department' => $department,
							// 	'unit' => $unit,
							// 	'first_half' => $first_half,
							// 	'second_half' => $second_half,
							// 	'leave_status' => $leave_status,
							// 	'present_status' => $present_status,
							// 	'absent_status' => $absent_status,
							// 	'date_out' => $out_date,
							// 	'overtime' => $overtime,
							// 	'out_time' => $out_time,
							// 	'out_time' => $out_time,
							// 	'out_time' => $out_time,
							// 	'out_time' => $out_time,
							// );
						}
					}
				}
			}
			//echo 'out';exit;
			$this->load->model('transaction/dayprocess');
			$filter_unit = '';
			$absent = $this->model_transaction_dayprocess->getAbsent($filter_date_start,$filter_unit);
			foreach($absent as $data) {
				$check_leave = $this->model_transaction_dayprocess->checkLeave($data['date'],$data['emp_id']);
				//$this->model_transaction_dayprocess->checkLeave_1($data['date'],$data['emp_id']);
			}
			$this->model_transaction_dayprocess->update_close_day($filter_date_start, $filter_unit, 2);
		}

		/*
		foreach($day as $dkeys => $dvalues){
			$filter_date_start = $dvalues['date'];
			//echo $filter_date_start;exit;
			$exp_datas = array();
			$exp_datas_2 = array();
			$employees = array();
			$employees_proc = array();
			$employees_final = array();
			$mdbFilename = 'Z:\Avi infotech Backup\eTimeTrackLite1.mdb';
			//$mdbFilename = DIR_DOWNLOAD.'eTimeTrackLite1.mdb';
			$user = '';
			$password = '';
			$connection = odbc_connect("Driver={Microsoft Access Driver (*.mdb)};Dbq=$mdbFilename", $user, $password);
			$sql = "SELECT * FROM DeviceLogs WHERE Format(LogDate, 'yyyy-mm-dd') = '".$filter_date_start."' ";
			//echo $sql;exit;
			$rs = odbc_exec($connection, $sql);
			$results = odbc_fetch_array($rs);
			$rev_exp_datas = array();
			while($row = (odbc_fetch_array($rs))){
				$log_date = $row['LogDate'];
				$exp_date = explode(' ', $log_date);
				$in_time = $exp_date[1];
				$in_date = $exp_date[0];
				$emp_id = $row['UserId'];
				$emp_id_sub = substr($emp_id, 0, 1);
				if($emp_id_sub == '2'){
					$result = $this->model_transaction_transaction->getEmployees_dat($emp_id);
					//if(!isset($found_emp_code[$emp_id])){
						if(isset($result['emp_code'])){
							$employees[] = array(
								'employee_id' => $emp_id,//$result['emp_code'],
								'card_id' => $emp_id,//$result['card_number'],
								'in_time' => $in_time,
								'punch_date' => $in_date,
								'fdate' => date('Y-m-d H:i:s', strtotime($in_date.' '.$in_time))
							);
						}
					//}
				}
			}
			
			usort($employees, array($this, "sortByOrder"));
			$o_emp = array();
			foreach ($employees as $ekey => $evalue) {
				$employees_final[] = $evalue;
			}
			if (isset($this->request->get['unit'])) {
				$filter_unit = $this->request->get['unit'];
			} else {
				$filter_unit = 0;
			}
			if(isset($employees_final) && count($employees_final) > 0){
				foreach ($employees_final as $fkey => $employee) {
					$exist = $this->model_transaction_transaction->getorderhistory($employee);
					if($exist == 0){
						$mystring = $employee['in_time'];
						$findme   = ':';
						$pos = strpos($mystring, $findme);
						if($pos !== false){
							$in_times = substr($employee['in_time'], 0, 2). ":" .substr($employee['in_time'], 3, 2);
						} else {
							$in_times = substr($employee['in_time'], 0, 2). ":" .substr($employee['in_time'], 2, 2);
						}
						if($in_times != ':'){
							$in_time = date("h:i", strtotime($in_times));
						} else {
							$in_time = '';
						}
						$employee['in_time'] = $in_times;
						$this->model_transaction_transaction->insert_attendance_data($employee);
					}
				}
			}
			$response = array();
			
			$data = array();
			$data['unit'] = $filter_unit;
			$results = $this->model_report_common_report->getemployees($data);
			foreach ($results as $rkey => $rvalue) {
				$rvaluess = $this->model_transaction_transaction->getrawattendance_group_date_custom($rvalue['emp_code'], $filter_date_start);
				if($rvaluess) {
					$emp_data = $this->model_transaction_transaction->getempdata($rvaluess['emp_id']);
					$device_id = $rvaluess['device_id'];
					if(isset($emp_data['name']) && $emp_data['name'] != ''){
						
						$emp_name = $emp_data['name'];
						$department = $emp_data['department'];
						$unit = $emp_data['unit'];
						$group = $emp_data['group'];

						$emp_code_start = substr($rvalue['emp_code'], 0, 1);
						if($emp_code_start == '1'){
							$emp_type = 1;
						} else {
							$emp_type = 2;
						}

						$day_date = date('j', strtotime($filter_date_start));
						$day = date('j', strtotime($filter_date_start));
						$month = date('n', strtotime($filter_date_start));
						$year = date('Y', strtotime($filter_date_start));

						$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$rvaluess['emp_id']."' AND `month` = '".$month."' AND `year` = '".$year."' ";
						//$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `month` ='".$month."' AND `year` = '".$year."' AND `emp_code` = '".$rvaluess['emp_id']."' ";
						$shift_schedule = $this->db->query($update3)->row;
						$schedule_raw = explode('_', $shift_schedule[$day_date]);
						if(!isset($schedule_raw[2])){
							$schedule_raw[2] = 1;
						}
						if($schedule_raw[0] == 'S'){
							$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[1]);
							if(!isset($shift_data['shift_id'])){
								$shift_data = $this->model_transaction_transaction->getshiftdata('1');
							}
							if(isset($shift_data['shift_id'])){
								$abnormal_status = 0;
								$shift_intime = $shift_data['in_time'];
								$shift_outtime = $shift_data['out_time'];

								$act_intime = '00:00:00';
								$act_outtime = '00:00:00';
								$act_in_punch_date = $filter_date_start;
								$act_out_punch_date = $filter_date_start;
								
								$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$rvaluess['punch_date']."' ";
								$trans_exist = $this->db->query($trans_exist_sql);
								if($trans_exist->num_rows == 0){ 
									$act_intimes = $this->model_transaction_transaction->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
									if(isset($act_intimes['punch_time'])) {
										$act_intime = $act_intimes['punch_time'];
										$act_in_punch_date = $act_intimes['punch_date'];
									}
									$act_outtimes = $this->model_transaction_transaction->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
									if(isset($act_outtimes['punch_time'])) {
										$act_outtime = $act_outtimes['punch_time'];
										$act_out_punch_date = $act_outtimes['punch_date'];
									}
								} else {
									if($trans_exist->row['act_intime'] == '00:00:00'){
										$new_data_sql = "SELECT * FROM `oc_attendance` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `punch_date` = '".$rvaluess['punch_date']."' AND `status` = '0' ";
										$new_data_exist = $this->db->query($new_data_sql);
										if($new_data_exist->num_rows > 0){	
											$act_intimes = $this->model_transaction_transaction->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
											if(isset($act_intimes['punch_time'])) {
												$act_intime = $act_intimes['punch_time'];
												$act_in_punch_date = $act_intimes['punch_date'];
											}
										}	
									} else {
										$act_intime = $trans_exist->row['act_intime'];
										$act_in_punch_date = $trans_exist->row['date'];	
									}
								
									$new_data_sql = "SELECT * FROM `oc_attendance` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `punch_date` = '".$rvaluess['punch_date']."' AND `status` = '0' ";
									$new_data_exist = $this->db->query($new_data_sql);
									if($new_data_exist->num_rows > 0){
										$act_outtimes = $this->model_transaction_transaction->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
										if(isset($act_outtimes['punch_time'])) {
											$act_outtime = $act_outtimes['punch_time'];
											$act_out_punch_date = $act_outtimes['punch_date'];
										}
									} else {
										$act_outtime = $trans_exist->row['act_outtime'];
										$act_out_punch_date = $trans_exist->row['date_out'];
									}
								}

								if($act_intime == $act_outtime){
									$act_outtime = '00:00:00';
								}
								
								$first_half = 0;
								$second_half = 0;
								$late_time = '00:00:00';
								
								if($act_intime != '00:00:00'){
									$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
									$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
									if($since_start->h > 12){
										$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
										$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
									}
									$late_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
									if($since_start->invert == 1){
										$late_time = '00:00:00';
									}
									if($act_outtime != '00:00:00'){
										$first_half = 1;
										$second_half = 1;
										
									} else {
										$first_half = 0;
										$second_half = 0;
									}
								} else {
									$first_half = 0;
									$second_half = 0;
								}

								$working_time = '00:00:00';
								$early_time = '00:00:00';
								if($abnormal_status == 0){ //if abnormal status is zero calculate further 
									$early_time = '00:00:00';
									if($act_outtime != '00:00:00'){ //for getting early time i.e difference between shiftouttime and sctouttime 
										$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime);
										$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
										if($since_start->h > 12){
											$start_date = new DateTime($act_out_punch_date.' '.$shift_outtime);
											$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_outtime));
										}
										$early_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;					
										if($since_start->invert == 0){
											$early_time = '00:00:00';
										}
									}					
									
									$working_time = '00:00:00';
									if($act_outtime != '00:00:00'){//for getting working time
										$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
										$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
										$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
									}
								} else {
									$first_half = 0;
									$second_half = 0;
								}

								$overtime = '00:00:00';
								if($abnormal_status == 0){ //if abnormal status is zero calculate further 
									if($act_outtime != '00:00:00'){ //for getting early time i.e difference between shiftouttime and sctouttime 
										$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime);//echo $start_date;exit;
										$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
										if($since_start->h > 12){
											$start_date = new DateTime($act_out_punch_date.' '.$shift_outtime);
											$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_outtime));
										}
										$overtime = $since_start->h.':'.$since_start->i.':'.$since_start->s;					
										if($since_start->invert == 1){
											$overtime = '00:00:00';
										}
									}				
								}
								
								$abs_stat = 0;
								if($first_half == 1 && $second_half == 1){
									$present_status = 1;
									$absent_status = 0;
								} elseif($first_half == 1 && $second_half == 0){
									$present_status = 0.5;
									$absent_status = 0.5;
								} elseif($first_half == 0 && $second_half == 1){
									$present_status = 0.5;
									$absent_status = 0.5;
								} else {
									$present_status = 0;
									$absent_status = 1;
								}

								$day = date('j', strtotime($rvaluess['punch_date']));
								$month = date('n', strtotime($rvaluess['punch_date']));
								$year = date('Y', strtotime($rvaluess['punch_date']));
								//echo 'out';exit;
								$early_time = '00:00:00';
								$late_time = '00:00:00';
								if($trans_exist->num_rows == 0){
									if($abs_stat == 1){
										$act_intime = '00:00:00';
										$act_outtime = '00:00:00';
										$abnormal_status = 0;
										$overtime = '00:00:00';
										$act_out_punch_date = $filter_date_start;
										$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', halfday_status = '0', `compli_status` = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."', overtime = '".$overtime."', emp_type = '".$emp_type."'  ";
									} else {
										$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', halfday_status = '0', `compli_status` = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."' , overtime = '".$overtime."', emp_type = '".$emp_type."' ";
									}
									$this->db->query($sql);
									$transaction_id = $this->db->getLastId();
								} else {
									if($abs_stat == 1){
										$act_intime = '00:00:00';
										$act_outtime = '00:00:00';
										$abnormal_status = 0;
										$act_out_punch_date = $filter_date_start;
										$sql = "UPDATE `oc_transaction` SET `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', `weekly_off` = '0', `holiday_id` = '0', halfday_status = '0', `compli_status` = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."' , overtime = '".$overtime."', emp_type = '".$emp_type."' WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ";
									} else {
										$sql = "UPDATE `oc_transaction` SET `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', `weekly_off` = '0', `holiday_id` = '0', halfday_status = '0', `compli_status` = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."', overtime = '".$overtime."', emp_type = '".$emp_type."' WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ";
									}
									$this->db->query($sql);
									$transaction_id = $this->db->query("SELECT `transaction_id` FROM `oc_transaction` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ")->row['transaction_id'];
								}
								 //echo $sql.';';
								 //echo '<br />';
								 //exit;
								
							}
							//echo 'out1';exit;
						} elseif ($schedule_raw[0] == 'W') {
							$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[2]);
							if(!isset($shift_data['shift_id'])){
								$shift_data = $this->model_transaction_transaction->getshiftdata('1');
							}
							$shift_intime = $shift_data['in_time'];
							$shift_outtime = $shift_data['out_time'];

							$act_intime = '00:00:00';
							$act_outtime = '00:00:00';
							$act_in_punch_date = $filter_date_start;
							$act_out_punch_date = $filter_date_start;

							$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$rvaluess['punch_date']."' ";
							$trans_exist = $this->db->query($trans_exist_sql);
							if($trans_exist->num_rows == 0){ 
								$act_intimes = $this->model_transaction_transaction->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
								if(isset($act_intimes['punch_time'])) {
									$act_intime = $act_intimes['punch_time'];
									$act_in_punch_date = $act_intimes['punch_date'];
								}
								$act_outtimes = $this->model_transaction_transaction->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
								if(isset($act_outtimes['punch_time'])) {
									$act_outtime = $act_outtimes['punch_time'];
									$act_out_punch_date = $act_outtimes['punch_date'];
								}
							} else {
								if($trans_exist->row['act_intime'] == '00:00:00'){
									$new_data_sql = "SELECT * FROM `oc_attendance` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `punch_date` = '".$rvaluess['punch_date']."' AND `status` = '0' ";
									$new_data_exist = $this->db->query($new_data_sql);
									if($new_data_exist->num_rows > 0){	
										$act_intimes = $this->model_transaction_transaction->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
										if(isset($act_intimes['punch_time'])) {
											$act_intime = $act_intimes['punch_time'];
											$act_in_punch_date = $act_intimes['punch_date'];
										}
									}	
								} else {
									$act_intime = $trans_exist->row['act_intime'];
									$act_in_punch_date = $trans_exist->row['date'];	
								}
								
								$new_data_sql = "SELECT * FROM `oc_attendance` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `punch_date` = '".$rvaluess['punch_date']."' AND `status` = '0' ";
								$new_data_exist = $this->db->query($new_data_sql);
								if($new_data_exist->num_rows > 0){
									$act_outtimes = $this->model_transaction_transaction->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
									if(isset($act_outtimes['punch_time'])) {
										$act_outtime = $act_outtimes['punch_time'];
										$act_out_punch_date = $act_outtimes['punch_date'];
									}
								} else {
									$act_outtime = $trans_exist->row['act_outtime'];
									$act_out_punch_date = $trans_exist->row['date_out'];
								}
							}
							$abnormal_status = 0;
							if($act_intime == $act_outtime){
								$act_outtime = '00:00:00';
							}
							$first_half = 0;
							$second_half = 0;
							$late_time = '00:00:00';
							if($act_intime != '00:00:00'){
								$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
								$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
								if($since_start->h > 12){
									$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
									$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
								}
								$late_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
								if($since_start->invert == 1){
									$late_time = '00:00:00';
								}
								if($act_outtime != '00:00:00'){
									$first_half = 1;
									$second_half = 1;
									
								} else {
									$first_half = 0;
									$second_half = 0;
								}
							} else {
								$first_half = 0;
								$second_half = 0;
							}

							$working_time = '00:00:00';
							$early_time = '00:00:00';
							if($abnormal_status == 0){ //if abnormal status is zero calculate further 
								$early_time = '00:00:00';
								if($act_outtime != '00:00:00'){ //for getting early time i.e difference between shiftouttime and sctouttime 
									$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime);
									$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
									if($since_start->h > 12){
										$start_date = new DateTime($act_out_punch_date.' '.$shift_outtime);
										$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_outtime));
									}
									$early_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;					
									if($since_start->invert == 0){
										$early_time = '00:00:00';
									}
								}					
								
								$working_time = '00:00:00';
								if($act_outtime != '00:00:00'){//for getting working time
									$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
									$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
									$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
								}
							} else {
								$first_half = 0;
								$second_half = 0;
							}

							$overtime = '00:00:00';
							if($abnormal_status == 0){ //if abnormal status is zero calculate further 
								if($act_outtime != '00:00:00'){ //for getting early time i.e difference between shiftouttime and sctouttime 
									$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime);//echo $start_date;exit;
									$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
									if($since_start->h > 12){
										$start_date = new DateTime($act_out_punch_date.' '.$shift_outtime);
										$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_outtime));
									}
									$overtime = $since_start->h.':'.$since_start->i.':'.$since_start->s;					
									if($since_start->invert == 1){
										$overtime = '00:00:00';
									}
								}				
							}
							
							// $abs_stat = 0;
							// if($first_half == 1 && $second_half == 1){
							// 	$present_status = 1;
							// 	$absent_status = 0;
							// } elseif($first_half == 1 && $second_half == 0){
							// 	$present_status = 0.5;
							// 	$absent_status = 0.5;
							// } elseif($first_half == 0 && $second_half == 1){
							// 	$present_status = 0.5;
							// 	$absent_status = 0.5;
							// } else {
							// 	$present_status = 0;
							// 	$absent_status = 1;
							// }
							$abs_stat = 0;
							if($filter_date_start != $act_in_punch_date){
								$abs_stat = 1;
								$act_in_punch_date = $filter_date_start;
							}
							// if($act_intime == '00:00:00' || $act_outtime == '00:00:00'){
							// 	$abnormal_status = 1;
							// }
							$early_time = '00:00:00';
							$late_time = '00:00:00';
							if($trans_exist->num_rows == 0){
								if($abs_stat == 1){
									$act_intime = '00:00:00';
									$act_outtime = '00:00:00';
									$abnormal_status = 0;
									$act_out_punch_date = $filter_date_start;
									$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `working_time` = '".$working_time."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '".$abnormal_status."', halfday_status = '0', compli_status = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."', emp_type = '".$emp_type."' ";
								} else {
									$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `working_time` = '".$working_time."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '".$abnormal_status."', halfday_status = '0', compli_status = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."', emp_type = '".$emp_type."' ";
								}
								$this->db->query($sql);
								$transaction_id = $this->db->getLastId();
							} else {
								if($abs_stat == 1){
									$act_intime = '00:00:00';
									$act_outtime = '00:00:00';
									$abnormal_status = 0;
									$act_out_punch_date = $filter_date_start;
									$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `working_time` = '".$working_time."', abnormal_status = '".$abnormal_status."', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', compli_status = '0', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', halfday_status = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."', emp_type = '".$emp_type."' WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ";
								} else {
									$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `working_time` = '".$working_time."', abnormal_status = '".$abnormal_status."', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', compli_status = '0', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', halfday_status = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."', emp_type = '".$emp_type."' WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ";
								}
								$this->db->query($sql);
								$transaction_id = $this->db->query("SELECT `transaction_id` FROM `oc_transaction` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ")->row['transaction_id'];
							}
							//echo $sql.';';
							//echo '<br />';
							
						} elseif ($schedule_raw[0] == 'H') {
							$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[2]);
							if(!isset($shift_data['shift_id'])){
								$shift_data = $this->model_transaction_transaction->getshiftdata('1');
							}
							$shift_intime = $shift_data['in_time'];
							$shift_outtime = $shift_data['out_time'];

							$act_intime = '00:00:00';
							$act_outtime = '00:00:00';
							$act_in_punch_date = $filter_date_start;
							$act_out_punch_date = $filter_date_start;
							
							$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$rvaluess['punch_date']."' ";
							$trans_exist = $this->db->query($trans_exist_sql);
							if($trans_exist->num_rows == 0){ 
								$act_intimes = $this->model_transaction_transaction->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
								if(isset($act_intimes['punch_time'])) {
									$act_intime = $act_intimes['punch_time'];
									$act_in_punch_date = $act_intimes['punch_date'];
								}
								$act_outtimes = $this->model_transaction_transaction->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
								if(isset($act_outtimes['punch_time'])) {
									$act_outtime = $act_outtimes['punch_time'];
									$act_out_punch_date = $act_outtimes['punch_date'];
								}
							} else {
								if($trans_exist->row['act_intime'] == '00:00:00'){
									$new_data_sql = "SELECT * FROM `oc_attendance` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `punch_date` = '".$rvaluess['punch_date']."' AND `status` = '0' ";
									$new_data_exist = $this->db->query($new_data_sql);
									if($new_data_exist->num_rows > 0){	
										$act_intimes = $this->model_transaction_transaction->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
										if(isset($act_intimes['punch_time'])) {
											$act_intime = $act_intimes['punch_time'];
											$act_in_punch_date = $act_intimes['punch_date'];
										}
									}	
								} else {
									$act_intime = $trans_exist->row['act_intime'];
									$act_in_punch_date = $trans_exist->row['date'];	
								}


								$new_data_sql = "SELECT * FROM `oc_attendance` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `punch_date` = '".$rvaluess['punch_date']."' AND `status` = '0' ";
								$new_data_exist = $this->db->query($new_data_sql);
								if($new_data_exist->num_rows > 0){
									$act_outtimes = $this->model_transaction_transaction->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
									if(isset($act_outtimes['punch_time'])) {
										$act_outtime = $act_outtimes['punch_time'];
										$act_out_punch_date = $act_outtimes['punch_date'];
									}
								} else {
									$act_outtime = $trans_exist->row['act_outtime'];
									$act_out_punch_date = $trans_exist->row['date_out'];
								}
							}

							$abnormal_status = 0;
							if($act_intime == $act_outtime){
								$act_outtime = '00:00:00';
							}
							$first_half = 0;
							$second_half = 0;
							$late_time = '00:00:00';
							if($act_intime != '00:00:00'){
								$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
								$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
								if($since_start->h > 12){
									$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
									$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
								}
								$late_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
								if($since_start->invert == 1){
									$late_time = '00:00:00';
								}
								if($act_outtime != '00:00:00'){
									$first_half = 1;
									$second_half = 1;
									
								} else {
									$first_half = 0;
									$second_half = 0;
								}
							} else {
								$first_half = 0;
								$second_half = 0;
							}

							$working_time = '00:00:00';
							$early_time = '00:00:00';
							if($abnormal_status == 0){ //if abnormal status is zero calculate further 
								$early_time = '00:00:00';
								if($act_outtime != '00:00:00'){ //for getting early time i.e difference between shiftouttime and sctouttime 
									$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime);
									$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
									if($since_start->h > 12){
										$start_date = new DateTime($act_out_punch_date.' '.$shift_outtime);
										$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_outtime));
									}
									$early_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;					
									if($since_start->invert == 0){
										$early_time = '00:00:00';
									}
								}					
								
								$working_time = '00:00:00';
								if($act_outtime != '00:00:00'){//for getting working time
									$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
									$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
									$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
								}
							} else {
								$first_half = 0;
								$second_half = 0;
							}

							$overtime = '00:00:00';
							if($abnormal_status == 0){ //if abnormal status is zero calculate further 
								if($act_outtime != '00:00:00'){ //for getting early time i.e difference between shiftouttime and sctouttime 
									$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime);//echo $start_date;exit;
									$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
									if($since_start->h > 12){
										$start_date = new DateTime($act_out_punch_date.' '.$shift_outtime);
										$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_outtime));
									}
									$overtime = $since_start->h.':'.$since_start->i.':'.$since_start->s;					
									if($since_start->invert == 1){
										$overtime = '00:00:00';
									}
								}				
							}
							
							// $abs_stat = 0;
							// if($first_half == 1 && $second_half == 1){
							// 	$present_status = 1;
							// 	$absent_status = 0;
							// } elseif($first_half == 1 && $second_half == 0){
							// 	$present_status = 0.5;
							// 	$absent_status = 0.5;
							// } elseif($first_half == 0 && $second_half == 1){
							// 	$present_status = 0.5;
							// 	$absent_status = 0.5;
							// } else {
							// 	$present_status = 0;
							// 	$absent_status = 1;
							// }
							$abs_stat = 0;
							if($filter_date_start != $act_in_punch_date){
								$abs_stat = 1;
								$act_in_punch_date = $filter_date_start;
							}
							$early_time = '00:00:00';
							$late_time = '00:00:00';
							// if($act_intime == '00:00:00' || $act_outtime == '00:00:00'){
							// 	$abnormal_status = 1;
							// }
							if($trans_exist->num_rows == 0){
								if($abs_stat == 1){
									$act_intime = '00:00:00';
									$act_outtime = '00:00:00';
									$abnormal_status = 0;
									$act_out_punch_date = $filter_date_start;
									$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `working_time` = '".$working_time."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `weekly_off` = '0', `holiday_id` = '".$schedule_raw[1]."', `present_status` = '0', `absent_status` = '0', `abnormal_status` = '".$abnormal_status."', `halfday_status` = '0', `compli_status` = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."', emp_type = '".$emp_type."' ";
								} else {
									$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `working_time` = '".$working_time."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `weekly_off` = '0', `holiday_id` = '".$schedule_raw[1]."', `present_status` = '0', `absent_status` = '0', `abnormal_status` = '".$abnormal_status."', `halfday_status` = '0', `compli_status` = '0', `device_id` = '".$device_id."', batch_id = '".$batch_id."', emp_type = '".$emp_type."' ";
								}
								$this->db->query($sql);
								$transaction_id = $this->db->getLastId();
							} else {
								if($abs_stat == 1){
									$act_intime = '00:00:00';
									$act_outtime = '00:00:00';
									$abnormal_status = 0;
									$act_out_punch_date = $filter_date_start;
									$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `working_time` = '".$working_time."', abnormal_status = '".$abnormal_status."', `weekly_off` = '0', `present_status` = '0', `absent_status` = '0', `halfday_status` = '0', `compli_status` = '0', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `holiday_id` = '".$schedule_raw[1]."', `device_id` = '".$device_id."', batch_id = '".$batch_id."', emp_type = '".$emp_type."' WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ";
								} else {
									$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `working_time` = '".$working_time."', abnormal_status = '".$abnormal_status."', `weekly_off` = '0', `present_status` = '0', `absent_status` = '0', `halfday_status` = '0', `compli_status` = '0', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `holiday_id` = '".$schedule_raw[1]."', `device_id` = '".$device_id."', batch_id = '".$batch_id."', emp_type = '".$emp_type."' WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ";
								}
								$this->db->query($sql);
								$transaction_id = $this->db->query("SELECT `transaction_id` FROM `oc_transaction` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ")->row['transaction_id'];
							}
							//echo $sql.';';
							//echo '<br />';
						}
						if($act_in_punch_date == $act_out_punch_date) {
							if($act_intime != '00:00:00' && $act_outtime != '00:00:00'){
								$update = "UPDATE `oc_attendance` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_in_punch_date."' AND `punch_time` >= '".$act_intime."' AND `punch_time` <= '".$act_outtime."' AND `emp_id` = '".$rvaluess['emp_id']."' ";
								//echo $update;exit;
								$this->db->query($update);
								//$this->log->write($update);
							} elseif($act_intime != '00:00:00' && $act_outtime == '00:00:00') {
								$update = "UPDATE `oc_attendance` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_in_punch_date."' AND `punch_time` = '".$act_intime."' AND `emp_id` = '".$rvaluess['emp_id']."' ";
								//echo $update;exit;
								$this->db->query($update);
								//$this->log->write($update);
							} elseif($act_intime == '00:00:00' && $act_outtime != '00:00:00') {
								$update = "UPDATE `oc_attendance` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_out_punch_date."' AND `punch_time` = '".$act_outtime."' AND `emp_id` = '".$rvaluess['emp_id']."' ";
								//echo $update;exit;
								$this->db->query($update);
								//$this->log->write($update);
							}
						} else {
							$update = "UPDATE `oc_attendance` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_in_punch_date."' AND `punch_time` >= '".$act_intime."' AND `emp_id` = '".$rvaluess['emp_id']."' ";
							$this->db->query($update);
							//$this->log->write($update);

							$update = "UPDATE `oc_attendance` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_out_punch_date."' AND `punch_time` <= '".$act_outtime."' AND `emp_id` = '".$rvaluess['emp_id']."' ";
							$this->db->query($update);
							//$this->log->write($update);
						}
					}
				} else {
					$emp_data = $this->model_transaction_transaction->getempdata($rvalue['emp_code']);
					if(isset($emp_data['name']) && $emp_data['name'] != ''){
						$emp_name = $emp_data['name'];
						$department = $emp_data['department'];
						$unit = $emp_data['unit'];
						$group = $emp_data['group'];

						$emp_code_start = substr($rvalue['emp_code'], 0, 1);
						if($emp_code_start == '1'){
							$emp_type = 1;
						} else {
							$emp_type = 2;
						}

						$day_date = date('j', strtotime($filter_date_start));
						$month = date('n', strtotime($filter_date_start));
						$year = date('Y', strtotime($filter_date_start));

						$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$rvalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ";
						//$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `month` ='".$month."' AND `year` = '".$year."' AND `emp_code` = '".$rvalue['emp_code']."' ";
						$shift_schedule = $this->db->query($update3)->row;
						$schedule_raw = explode('_', $shift_schedule[$day_date]);
						if(!isset($schedule_raw[2])){
							$schedule_raw[2]= 1;
						}
						if($schedule_raw[0] == 'S'){
							$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[1]);
							if(isset($shift_data['shift_id'])){
								$shift_intime = $shift_data['in_time'];
								$shift_outtime = $shift_data['out_time'];
								
								$day = date('j', strtotime($filter_date_start));
								$month = date('n', strtotime($filter_date_start));
								$year = date('Y', strtotime($filter_date_start));

								$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
								$trans_exist = $this->db->query($trans_exist_sql);
								if($trans_exist->num_rows == 0){
									$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', emp_type = '".$emp_type."' ";
									//echo $sql.';';
									//echo '<br />';
									$this->db->query($sql);
								} else {
									$sql = "UPDATE `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', emp_type = '".$emp_type."' WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."'";
									//echo $sql.';';
									//echo '<br />';
									//$this->db->query($sql);
								}
							} else {
								$shift_data = $this->model_transaction_transaction->getshiftdata('1');
								$shift_intime = $shift_data['in_time'];
								$shift_outtime = $shift_data['out_time'];
								
								$day = date('j', strtotime($filter_date_start));
								$month = date('n', strtotime($filter_date_start));
								$year = date('Y', strtotime($filter_date_start));

								$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
								$trans_exist = $this->db->query($trans_exist_sql);
								if($trans_exist->num_rows == 0){
									$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', emp_type = '".$emp_type."' ";
									//echo $sql.';';
									//echo '<br />';
									$this->db->query($sql);
								} else {
									$sql = "UPDATE `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', emp_type = '".$emp_type."' WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
									//echo $sql.';';
									//echo '<br />';
									//$this->db->query($sql);
								}
							}
						} elseif ($schedule_raw[0] == 'W') {
							$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[2]);
							if(!isset($shift_data['shift_id'])){
								$shift_data = $this->model_transaction_transaction->getshiftdata('1');
							}
							$shift_intime = $shift_data['in_time'];
							$shift_outtime = $shift_data['out_time'];

							$act_intime = '00:00:00';
							$act_outtime = '00:00:00';
							$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
							$trans_exist = $this->db->query($trans_exist_sql);
							
							if($trans_exist->num_rows == 0){
								$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', emp_type = '".$emp_type."' ";
								//echo $sql.';';
								//echo '<br />';
								$this->db->query($sql);
							} else {
								$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', emp_type = '".$emp_type."' WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
								//echo $sql.';';
								//echo '<br />';
								//$this->db->query($sql);
							}
						} elseif ($schedule_raw[0] == 'H') {
							$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[2]);
							if(!isset($shift_data['shift_id'])){
								$shift_data = $this->model_transaction_transaction->getshiftdata('1');
							}
							$shift_intime = $shift_data['in_time'];
							$shift_outtime = $shift_data['out_time'];

							$act_intime = '00:00:00';
							$act_outtime = '00:00:00';
							
							$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
							$trans_exist = $this->db->query($trans_exist_sql);
							if($trans_exist->num_rows == 0){
								$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `weekly_off` = '0', `holiday_id` = '".$schedule_raw[1]."', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', emp_type = '".$emp_type."'  ";
								//echo $sql.';';
								//echo '<br />';
								$this->db->query($sql);
							} else {
								$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `weekly_off` = '0', `holiday_id` = '".$schedule_raw[1]."', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', batch_id = '".$batch_id."', emp_type = '".$emp_type."' WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
								//echo $sql.';';
								//echo '<br />';
								//$this->db->query($sql);
							}
						}
						// $update = "UPDATE `oc_attendance` SET `status` = '1' WHERE `punch_date` = '".$filter_date_start."' AND `punch_time` >= '".$act_intime."' AND `punch_time` <= '".$act_outtime."' AND `emp_id` = '".$rvalue['emp_code']."' ";
						// $this->db->query($update);
						// $this->log->write($update);
					}	
				}
			}
			$this->load->model('transaction/dayprocess');
			$absent = $this->model_transaction_dayprocess->getAbsent($filter_date_start,$filter_unit);
			foreach($absent as $data) {
				$check_leave = $this->model_transaction_dayprocess->checkLeave($data['date'],$data['emp_id']);
				//$this->model_transaction_dayprocess->checkLeave_1($data['date'],$data['emp_id']);
			}
			$this->model_transaction_dayprocess->update_close_day($filter_date_start, $filter_unit, 2);
		}
		*/

		$this->load->model('transaction/leaveprocess');
		$this->load->model('transaction/dayprocess');

		if (isset($this->request->get['unit'])) {
			$unit = $this->request->get['unit'];
		} else {
			$unit = '';
		}

		$unprocessed = $this->model_transaction_leaveprocess->getUnprocessedLeaveTillDate_2($unit);
		
		foreach($unprocessed as $data) {
			$is_closed = $this->model_transaction_dayprocess->is_closed_stat($data['date'], $data['emp_id']); 
			if($is_closed == 1){
				$this->model_transaction_dayprocess->checkLeave($data['date'], $data['emp_id']);
			}
		}
		
		$this->session->data['success'] = 'Daily Employee Attendance Updated Sucessfully';
		$this->redirect($this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'));
		//echo 'Done';exit;

		// $transaction_datas = $this->db->query("SELECT * FROM `oc_transaction` WHERE `batch_id` = '".$batch_id."' ")->rows;
		// $custom_data = array();
		// foreach($transaction_datas as $tkeys => $tvalues){
		// 	$emp_datas = $this->db->query("SELECT `emp_code`, `name`, `department`, `unit`, `division`, `region`, `doj` FROM `oc_employee` WHERE `emp_code` = '".$tvalues['emp_id']."' ");
		// 	if($emp_datas->num_rows > 0){
		// 		$emp_data = $emp_datas->row;
		// 		$emp_code = $emp_data['emp_code'];
		// 		$emp_name = $emp_data['name'];
		// 		$department = $emp_data['department'];
		// 		$unit = $emp_data['unit'];
		// 		$division = $emp_data['division'];
		// 		$region = $emp_data['region'];
		// 		$doj = $emp_data['doj'];
		// 	}

		// 	$date_to_compare = $filter_date_start;
		// 	if(strtotime($doj) > strtotime($date_to_compare)){
		// 		$status = 'x';
		// 	} elseif($tvalues['present_status'] == '1'){
		// 		$status = 'p';
		// 	} elseif($tvalues['weekly_off'] != '0'){
		// 		$working_times = explode(':', $tvalues['working_time']);
		// 		$working_hours = $working_times[0];
		// 		if($working_hours >= '06'){
		// 			$status = 'PW';
		// 		} else {
		// 			$status = 'W';	
		// 		}
		// 	} elseif($tvalues['holiday_id'] != '0'){
		// 		$working_times = explode(':', $tvalues['working_time']);
		// 		$working_hours = $working_times[0];
		// 		if($working_hours >= '06'){
		// 			$status = 'PPH';
		// 		} else {
		// 			$status = 'PH';
		// 		}
		// 	} elseif($tvalues['act_intime'] != '00:00:00' && $tvalues['act_outtime'] == '00:00:00'){
		// 		$status = 'E';
		// 	} elseif($tvalues['leave_status'] != '0'){
		// 		if($tvalues['leave_status'] == '0.5'){
		// 			$status = 'PL';
		// 		} else {
		// 			$status = 'PL';
		// 		}
		// 	} elseif($tvalues['absent_status'] == '1'){
		// 		$status = 'A';
		// 	}

		// 	$custom_data[] = array(
		// 		'emp_id' => $emp_code,
		// 		'emp_name' => $emp_name,
		// 		'punch_id' => $emp_code,
		// 		'department' => $department,
		// 		'site' => $unit,
		// 		'region' => $region,
		// 		'division' => $division,
		// 		'attend_date' => $filter_date_start,
		// 		'status' => $status,
		// 		'in_time' => $tvalues['act_intime'],
		// 		'out_time' => $tvalues['act_outtime'],
		// 		'total_hour' => $tvalues['working_time'],
		// 	);
		// }
		// echo '<pre>';
		// print_r($custom_data);
		// exit;
		
		
		$this->template = 'catalog/dataprocess.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	public function convertToHoursMins($time, $format = '%02d:%02d') {
	    if ($time < 1) {
	        return;
	    }
	    $hours = floor($time / 60);
	    $minutes = ($time % 60);
	    return sprintf($format, $hours, $minutes);
	}

	public function sortByOrder($a, $b) {
		$v1 = strtotime($a['fdate']);
		$v2 = strtotime($b['fdate']);
		return $v1 - $v2; // $v2 - $v1 to reverse direction
		// if ($a['punch_date'] == $b['punch_date']) {
			//return ($a['in_time'] > $b['in_time']) ? -1 : 1;;
		//}
		//return $a['punch_date'] - $b['punch_date'];
	}

	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	public function array_sort($array, $on, $order=SORT_ASC){

		$new_array = array();
		$sortable_array = array();

		if (count($array) > 0) {
			foreach ($array as $k => $v) {
				if (is_array($v)) {
					foreach ($v as $k2 => $v2) {
						if ($k2 == $on) {
							$sortable_array[$k] = $v2;
						}
					}
				} else {
					$sortable_array[$k] = $v;
				}
			}

			switch ($order) {
				case SORT_ASC:
					asort($sortable_array);
					break;
				case SORT_DESC:
					arsort($sortable_array);
					break;
			}

			foreach ($sortable_array as $k => $v) {
				$new_array[$k] = $array[$k];
			}
		}

		return $new_array;
	}
}
?>
