<?php    
class ControllerSnippetsEmpimport extends Controller { 
	private $error = array();

	public function index() {

		/*	$emp_datas = array();
		$all_employees = $this->db->query(" SELECT employee_id FROM oc_employee WHERE `shift_type` = 'R' AND `status` = '1' ORDER BY employee_id ASC ")->rows;

		$shift_1 = array();
		$shift_2 = array();
		$shift_3 = array();
		$tran_count = count($all_employees);
		$divide_number = ceil($tran_count / 3);
		$tran_datas_chunk = array_chunk($all_employees, $divide_number);

		if(isset($tran_datas_chunk[0])){
			$shift_1 = $tran_datas_chunk[0];
		}

		if(isset($tran_datas_chunk[1])){
			$shift_2 = $tran_datas_chunk[1];
		}

		if(isset($tran_datas_chunk[2])){
			$shift_3 = $tran_datas_chunk[2];
		}

		foreach ($shift_1 as $ekey => $evalue) {
			$this->db->query("UPDATE oc_employee SET chunk_id = 1 WHERE employee_id = '".$evalue['employee_id']."' ");
		}

		foreach ($shift_2 as $ekey => $evalue) {
			$this->db->query("UPDATE oc_employee SET chunk_id = 2 WHERE employee_id = '".$evalue['employee_id']."' ");
		}

		foreach ($shift_3 as $ekey => $evalue) {
			$this->db->query("UPDATE oc_employee SET chunk_id = 3 WHERE employee_id = '".$evalue['employee_id']."' ");
		}

		echo "chunk_added";
		$this->db->query("UPDATE `oc_employee` SET  shift_type = 'F',chunk_id = '0'  WHERE department ='SECURITY'");
		exit;*/

		$t = DIR_DOWNLOAD."mustang_original_csv_import.csv";
		//echo $t;exit;
		$file=fopen($t,"r");
		/*echo '<pre>';
		print_r($file);
		exit;*/
		$i=1;
		$site_data = array();
		$department_data = array();
		$designtion_data = array();
		$location_data = array();
		$this->db->query("TRUNCATE oc_employee");
		$this->db->query("TRUNCATE oc_shift_schedule");
		$this->db->query("TRUNCATE oc_leave");
		$this->db->query("TRUNCATE oc_unit");
		$this->db->query("TRUNCATE oc_designation");
		$this->db->query("TRUNCATE oc_department");
		$this->db->query("TRUNCATE oc_locations");
		//$this->db->query("TRUNCATE oc_transaction");
		$site_data['PALGHAR'] = 'palghar';
		while(($var=fgetcsv($file,100000000000000,","))!==FALSE){
			if($i != 1) {
				// echo '<pre>';
				// print_r($var);
				//exit;
				if($var[13] != ''){
					if(!isset($site_data[$var[13]])){
						$location_data[$var[13]] = html_entity_decode(strtolower(trim($var[13])));
					}
				}
				if($var[11] != ''){
					if(!isset($site_data[$var[11]])){
						$site_data[$var[11]] = html_entity_decode(strtolower(trim($var[11])));
					}
				}
				if($var[5] != ''){
					if(!isset($department_data[$var[5]])){
						$department_data[$var[5]] = html_entity_decode(strtolower(trim($var[5])));
					}
				}
				if($var[6] != ''){
					if(!isset($designtion_data[$var[6]])){
						$designtion_data[$var[6]] = html_entity_decode(strtolower(trim($var[6])));
					}
				}
			}
			$i ++;
		}//exit;
		fclose($file);
		
		$site_data_linked = array();
		foreach($site_data as $dkey => $dvalue){
			$dkey = trim(preg_replace('/\s+/',' ',$dkey));
			$is_exist = $this->db->query("SELECT `unit_id` FROM `oc_unit` WHERE `unit` = '".$this->db->escape($dkey)."' ");
			if($is_exist->num_rows == 0){
				$sql = "INSERT INTO `oc_unit` SET `unit` = '".$this->db->escape($dkey)."' ";
				$this->db->query($sql);
				$site_id = $this->db->getLastId();
				//$site_id = 0;
			} else {
				$site_id = $is_exist->row['unit_id'];
			}
			$site_data_linked[$dvalue] = $site_id;
		}

		$location_data_linked = array();
		foreach($location_data as $dkey => $dvalue){
			$dkey = trim(preg_replace('/\s+/',' ',$dkey));
			$is_exist = $this->db->query("SELECT `id` FROM `oc_locations` WHERE `name` = '".$this->db->escape($dkey)."' ");
			if($is_exist->num_rows == 0){
				$sql = "INSERT INTO `oc_locations` SET `name` = '".$this->db->escape($dkey)."', `status` = '1' ";
				$this->db->query($sql);
				$location_id = $this->db->getLastId();
				//$location_id = 0;
			} else {
				$location_id = $is_exist->row['id'];
			}
			$location_data_linked[$dvalue] = $location_id;
		}

		$department_data_linked = array();
		foreach($department_data as $dkey => $dvalue){
			$dkey = trim(preg_replace('/\s+/',' ',$dkey));
			$is_exist = $this->db->query("SELECT `department_id` FROM `oc_department` WHERE `d_name` = '".$this->db->escape($dkey)."' ");
			if($is_exist->num_rows == 0){
				$sql = "INSERT INTO `oc_department` SET `d_name` = '".$this->db->escape($dkey)."', `status` = '1' ";
				$this->db->query($sql);
				$department_id = $this->db->getLastId();
				//$department_id = 0;
			} else {
				$department_id = $is_exist->row['department_id'];
			}
			$department_data_linked[$dvalue] = $department_id;
		}

		$designation_data_linked = array();
		foreach($designtion_data as $dkey => $dvalue){
			$dkey = trim(preg_replace('/\s+/',' ',$dkey));
			$is_exist = $this->db->query("SELECT `designation_id` FROM `oc_designation` WHERE `d_name` = '".$this->db->escape($dkey)."' ");
			if($is_exist->num_rows == 0){
				$sql = "INSERT INTO `oc_designation` SET `d_name` = '".$this->db->escape($dkey)."', `status` = '1' ";
				$this->db->query($sql);
				$designation_id = $this->db->getLastId();
				//$designation_id = 0;
			} else {
				$designation_id = $is_exist->row['designation_id'];
			}
			$designation_data_linked[$dvalue] = $designation_id;
		}
		//echo 'out';exit;

		$file=fopen($t,"r");
		$i=1;
		$months_array = array(
			'1' => '1',
			'2' => '2',
			'3' => '3',
			'4' => '4',
			'5' => '5',
			'6' => '6',
			'7' => '7',
			'8' => '8',
			'9' => '9',
			'10' => '10',
			'11' => '11',
			'12' => '12',
		);
		$shortmonthsarray = array(
			'Jan' => '1',
			'Feb' => '2',
			'Mar' => '3',
			'Apr' => '4',
			'May' => '5',
			'Jun' => '6',
			'Jul' => '7',
			'Aug' => '8',
			'Sep' => '9',
			'Oct' => '10',
			'Nov' => '11',
			'Dec' => '12',	
		);
		$final_employee_error_data = array();
		$employee_error_data = array();
		while(($var=fgetcsv($file,100000000000000,","))!==FALSE){
			if($i != 1) {
				$employee_error_data = array();
				$var0=addslashes(trim($var[0]));//emp_code
				if($var0 != '' && !isset($emp_code_exist[$var0])){
					$var1 = html_entity_decode(trim($var[1]));//name
					if($var1 == ''){
						$employee_error_data[] = array(
							'line_number' => $i,
							'reason' => 'Employee Name Blank',
						);
					}
					$var22=addslashes($var[4]);//gender
					if($var22 == 'M' || $var22 == 'Male'){
						$var22 = 'M';
					} elseif($var22 == 'F' || $var22 == 'Female'){
						$var22 = 'F';
					} else {
						$var22 = 'M';
					}
					if($var22 == ''){
						$employee_error_data[] = array(
							'line_number' => $i,
							'reason' => 'Gender Value is Blank / Invalid',
						);
					}

					$var2 = '0000-00-00';
					/*
					$var33=addslashes($var[7777]);//dob
					if($var33 != ''){
						$var33_exp = explode('-', $var33); // yyyy/mm/dd
						if(isset($var33_exp[2])){
							$year_string = strlen($var33_exp[0]);
							$month_string = strlen(sprintf('%02d', $var33_exp[1]));
							$day_string = strlen(sprintf('%02d', $var33_exp[2]));
							if($year_string == '4' && $month_string == '2' && $day_string == '2'){
								$var3 = Date('Y-m-d', strtotime($var33));//dob
							} else {
								$var33_exp = explode('-', $var33);// dd/mm/yyyy
								if(isset($var33_exp[2])){
									$year_string = strlen($var33_exp[2]);
									$month_string = strlen(sprintf('%02d', $var33_exp[1]));
									$day_string = strlen(sprintf('%02d', $var33_exp[0]));
									if($year_string == '4' && $month_string == '2' && $day_string == '2'){
										$var3 = Date('Y-m-d', strtotime($var33));//dob
									} else {
										$var3 = '0000-00-00';
									}
								} else {
									$var3 = '0000-00-00';		
								}
							}
						} else {
							$var33_exp = explode('/', $var33); // yyyy/mm/dd
							if(isset($var33_exp[2])){
								$year_string = strlen($var33_exp[0]);
								$month_string = strlen(sprintf('%02d', $var33_exp[1]));
								$day_string = strlen(sprintf('%02d', $var33_exp[2]));
								if($year_string == '4' && $month_string == '2' && $day_string == '2'){
									$var3 = Date('Y-m-d', strtotime($var33));//dob
								} else {
									$var33_exp = explode('/', $var33);// dd/mm/yyyy
									if(isset($var33_exp[2])){
										$year_string = strlen($var33_exp[2]);
										$month_string = strlen(sprintf('%02d', $var33_exp[1]));
										$day_string = strlen(sprintf('%02d', $var33_exp[0]));
										if($year_string == '4' && $month_string == '2' && $day_string == '2'){
											$var3 = Date('Y-m-d', strtotime($var33));//dob
										} else {
											$var3 = '0000-00-00';
										}
									} else {
										$var3 = '0000-00-00';		
									}
								}
							} else {
								$var3 = '0000-00-00';	
							}	
						}
					} else {
						$var3 = '0000-00-00';
					}
					*/

					$var3 = '0000-00-00';
					/*
					$var44=addslashes($var[12]);//doj
					if($var44 != '' && $var44 != '0000-00-00' && $var44 != '00-00-0000'){
						$var44_exp = explode('-', $var44);// yyyy/mm/dd
						if(isset($var44_exp[2])){
							$year_string = strlen($var44_exp[0]);
							$month_string = strlen(sprintf('%02d', $var44_exp[1]));
							$day_string = strlen(sprintf('%02d', $var44_exp[2]));
							if($year_string == '4' && $month_string == '2' && $day_string == '2'){
								$var4 = Date('Y-m-d', strtotime($var44));//doj
							} else {
								$var44_exp = explode('-', $var44);// dd/mm/yyyy
								if(isset($var44_exp[2])){
									$year_string = strlen($var44_exp[2]);
									$month_string = strlen(sprintf('%02d', $var44_exp[1]));
									$day_string = strlen(sprintf('%02d', $var44_exp[0]));
									if($year_string == '4' && $month_string == '2' && $day_string == '2'){
										$var4 = Date('Y-m-d', strtotime($var44));//doj
									} else {
										$var4 = '0000-00-00';		
									}
								} else {
									$var4 = '0000-00-00';
								}
							}
						} else {
							$var44_exp = explode('/', $var44);// yyyy/mm/dd
							if(isset($var44_exp[2])){
								$year_string = strlen($var44_exp[0]);
								$month_string = strlen(sprintf('%02d', $var44_exp[1]));
								$day_string = strlen(sprintf('%02d', $var44_exp[2]));
								if($year_string == '4' && $month_string == '2' && $day_string == '2'){
									$var4 = Date('Y-m-d', strtotime($var44));//doj
								} else {
									$var44_exp = explode('/', $var44);// dd/mm/yyyy
									if(isset($var44_exp[2])){
										$year_string = strlen($var44_exp[2]);
										$month_string = strlen(sprintf('%02d', $var44_exp[1]));
										$day_string = strlen(sprintf('%02d', $var44_exp[0]));
										if($year_string == '4' && $month_string == '2' && $day_string == '2'){
											$var4 = Date('Y-m-d', strtotime($var44));//doj
										} else {
											$var4 = '0000-00-00';		
										}
									} else {
										$var4 = '0000-00-00';
									}
								}
							} else {
								$var4 = '0000-00-00';	
							}
						}
					} else {
						$var4 = '0000-00-00';
					}
					*/
					//echo $var4;exit;

					$var8 = '0000-00-00';
					/*
					$var55=addslashes($var[13]);//doc
					if($var55 != ''){
						$var5 = Date('Y-m-d', strtotime($var55));//doc
					} else {
						$var5 = '0000-00-00';
					}

					$var66=addslashes($var[17]);//dol
					if($var66 != ''){
						$var6 = Date('Y-m-d', strtotime($var66));//dol
					} else {
						$var6 = '0000-00-00';
					}
					
					if($var3 == '0000-00-00'){
						$employee_error_data[] = array(
							'line_number' => $i,
							'reason' => 'Date Of Birth Blank / Invalid'
						);
					}
					if($var4 == '0000-00-00'){
						$employee_error_data[] = array(
							'line_number' => $i,
							'reason' => 'Date Of Joining Blank / Invalid'
						);
					}
					*/

					$var66=html_entity_decode(addslashes($var[11]));//site
					$var6=html_entity_decode(strtolower(trim($var[11])));//site
					if($var6 != ''){
						$var6=$site_data_linked[$var6];
					}
					if($var6 == '0'){
						$employee_error_data[] = array(
							'line_number' => $i,
							'reason' => 'Site '.$var66.' Does Not Exist',
						);
					}
					
					$var77=html_entity_decode(addslashes($var[5]));//department
					$var7=html_entity_decode(strtolower(trim($var[5])));//department
					if($var7 != ''){
						$var7=$department_data_linked[$var7];
					}
					if($var7 == '0'){
						$employee_error_data[] = array(
							'line_number' => $i,
							'reason' => 'Department '.$var77.' Does Not Exist',
						);
					}
					
					$var88=html_entity_decode(addslashes($var[6]));//designation
					$var8=html_entity_decode(strtolower(trim($var[6])));//designation
					if($var88 != ''){
						if($var8 != ''){
							$var8=$designation_data_linked[$var8];
						}
						if($var8 == '0'){
							$employee_error_data[] = array(
								'line_number' => $i,
								'reason' => 'Designation '.$var8.' Does Not Exist',
							);
						}
					}

					$var133 =html_entity_decode(addslashes($var[13]));//designation
					$var13 =  html_entity_decode(strtolower(trim($var[13])));//designation
					if($var133 != ''){
						if($var13 != ''){
							$var13=$location_data_linked[$var13];
						}
						if($var13 == '0'){
							$employee_error_data[] = array(
								'line_number' => $i,
								'reason' => 'location '.$var13.' Does Not Exist',
							);
						}
					}
					
					$emp_code = $var0;
					
					if($var['12'] == 'R'){
						$shift_type = 'R';
						$shift_id = 1;
					} else {
						$shift_type = 'F';
						$shift_id = 6;
					}

					$var10=addslashes($var[10]);//status
					/*if($var10 == 'Working'){
						$var10 = '1';
					} else {
						$var10 = '0';
					}*/
					
					$user_name = $emp_code;
					$salt = substr(md5(uniqid(rand(), true)), 0, 9);
					$password = sha1($salt . sha1($salt . sha1($emp_code)));
					
					// echo '<pre>';
					// print_r($employee_error_data);
					// exit;
					//$var12=addslashes($var[12]);//shift type // rotational or fix
					if(empty($employee_error_data)){
						$is_exist = $this->db->query("SELECT `emp_code`, `shift`, `unit_id`, `doj`, `employee_id` FROM `oc_employee` WHERE `emp_code` = '".$var0."' ");
						if($is_exist->num_rows == 0){
							$db_shift_id = 0;
							$db_unit_id = 0;
							$db_employee_id = 0;
							$db_doj = '0000-00-00';
							$insert = "INSERT INTO `oc_employee` SET 
										`emp_code` = '".$var0."', 
										`name` = '".$this->db->escape($var1)."', 
										`gender` = '".$var22."', 
										`dob` = '".$var3."', 
										`doj` = '".$var2."', 
										`doc` = '".$var8."', 
										`department` = '".$var77."',
										`department_id` = '".$var7."',
										`designation` = '".$var88."', 
										`designation_id` = '".$var8."',
										`location_id` = '".$var13."',
										`division` ='".$var7."',
										`unit` = 'Palghar',
										`unit_id` = '1',
										`shift` = '".$shift_id."', 
										`status` = '".$var10."', 
										`category` = '".$var[7]."',
										`shift_type` = '".$shift_type."',
										`user_group_id` = '11',
										`username` = '".$user_name."', 
										`password` = '".$password."',
										`salt` = '".$salt."' "; 
							$this->db->query($insert);
							$employee_id = $this->db->getLastId();
							// echo $insert;
							// echo '<br />';
							// exit;
						} else {
							$db_emp_data = $is_exist->row;
							$db_shift_id = $db_emp_data['shift'];
							$db_unit_id = $db_emp_data['unit_id'];
							$db_doj = $db_emp_data['doj'];
							$db_employee_id = $db_emp_data['employee_id'];
							$update = "UPDATE `oc_employee` SET 
										`emp_code` = '".$var0."', 
										`name` = '".$this->db->escape($var1)."', 
										`gender` = '".$var22."', 
										`dob` = '".$var3."', 
										`doj` = '".$var2."', 
										`doc` = '".$var8."', 
										`department` = '".$var77."',
										`department_id` = '".$var7."',
										`designation` = '".$var88."', 
										`designation_id` = '".$var8."',
										`location_id` = '".$var13."',
										`division` ='".$var7."',
										`unit` = '".$var66."',
										`unit_id` = '".$var6."',
										`shift` = '".$shift_id."', 
										`status` = '".$var10."', 
										`category` = '".$var[7]."',
										`shift_type` = '".$shift_type."',
										`user_group_id` = '11',
										`username` = '".$user_name."', 
										`password` = '".$password."', 
										`salt` = '".$salt."'
										 WHERE `emp_code` = '".$var0."' "; 
							$this->db->query($update);
							$employee_id = $db_employee_id;
							// echo $insert;
							// echo '<br />';
							// exit;
						}
						$shift_id_data = 'S_'.$shift_id;
						foreach ($months_array as $key => $value) {
							$is_exist = $this->db->query("SELECT * FROM `oc_shift_schedule` WHERE `emp_code` = '".$employee_id."' AND `month` = '".$key."' AND `year` = '".date('Y')."' ");
							if($is_exist->num_rows == 0){
								$insert1 = "INSERT INTO `oc_shift_schedule` SET 
									`emp_code` = '".$employee_id."',
									`1` = '".$shift_id_data."',
									`2` = '".$shift_id_data."',
									`3` = '".$shift_id_data."',
									`4` = '".$shift_id_data."',
									`5` = '".$shift_id_data."',
									`6` = '".$shift_id_data."',
									`7` = '".$shift_id_data."',
									`8` = '".$shift_id_data."',
									`9` = '".$shift_id_data."',
									`10` = '".$shift_id_data."',
									`11` = '".$shift_id_data."',
									`12` = '".$shift_id_data."', 
									`13` = '".$shift_id_data."', 
									`14` = '".$shift_id_data."', 
									`15` = '".$shift_id_data."', 
									`16` = '".$shift_id_data."', 
									`17` = '".$shift_id_data."', 
									`18` = '".$shift_id_data."', 
									`19` = '".$shift_id_data."', 
									`20` = '".$shift_id_data."', 
									`21` = '".$shift_id_data."', 
									`22` = '".$shift_id_data."', 
									`23` = '".$shift_id_data."', 
									`24` = '".$shift_id_data."', 
									`25` = '".$shift_id_data."', 
									`26` = '".$shift_id_data."', 
									`27` = '".$shift_id_data."', 
									`28` = '".$shift_id_data."', 
									`29` = '".$shift_id_data."', 
									`30` = '".$shift_id_data."', 
									`31` = '".$shift_id_data."', 
									`month` = '".$key."',
									`year` = '".date('Y')."',
									`status` = '1' ,
									`unit` = '".$var66."',
									`unit_id` = '".$var6."' "; 
									// echo $insert1;
									// echo '<br />';
								$this->db->query($insert1);
							} else {
								$shift_schedule_data = $is_exist->row;
								$shift = $shift_id_data;
								
								$insert1 = "UPDATE `oc_shift_schedule` SET 
										`emp_code` = '".$employee_id."',
										`1` = '".$shift_id_data."',
										`2` = '".$shift_id_data."',
										`3` = '".$shift_id_data."',
										`4` = '".$shift_id_data."',
										`5` = '".$shift_id_data."',
										`6` = '".$shift_id_data."',
										`7` = '".$shift_id_data."',
										`8` = '".$shift_id_data."',
										`9` = '".$shift_id_data."',
										`10` = '".$shift_id_data."',
										`11` = '".$shift_id_data."',
										`12` = '".$shift_id_data."', 
										`13` = '".$shift_id_data."', 
										`14` = '".$shift_id_data."', 
										`15` = '".$shift_id_data."', 
										`16` = '".$shift_id_data."', 
										`17` = '".$shift_id_data."', 
										`18` = '".$shift_id_data."', 
										`19` = '".$shift_id_data."', 
										`20` = '".$shift_id_data."', 
										`21` = '".$shift_id_data."', 
										`22` = '".$shift_id_data."', 
										`23` = '".$shift_id_data."', 
										`24` = '".$shift_id_data."', 
										`25` = '".$shift_id_data."', 
										`26` = '".$shift_id_data."', 
										`27` = '".$shift_id_data."', 
										`28` = '".$shift_id_data."', 
										`29` = '".$shift_id_data."', 
										`30` = '".$shift_id_data."', 
										`31` = '".$shift_id_data."',
										`month` = '".$key."',
										`year` = '".date('Y')."',
										`status` = '1' ,
										`unit` = '".$var66."',
										`unit_id` = '".$var6."' 
										WHERE id = '".$shift_schedule_data['id']."' ";
								$this->db->query($insert1);
							} 
						}

						$open_years = $this->db->query("SELECT `year` FROM `oc_leave` WHERE `close_status` = '0' GROUP BY `year` ORDER BY `year` DESC LIMIT 1");
						if($open_years->num_rows > 0){
							$open_year = $open_years->row['year'];
						} else {
							$open_year = date('Y');
						}
						
						$is_exist = $this->db->query("SELECT `emp_id`, `leave_id` FROM `oc_leave` WHERE `emp_id` = '".$employee_id."' AND `year` = '".$open_year."' ");
						if($is_exist->num_rows == 0){
							$insert2 = "INSERT INTO `oc_leave` SET 
										`emp_id` = '".$employee_id."', 
										`emp_name` = '".$this->db->escape($var1)."', 
										`emp_doj` = '".$var2."', 
										`year` = '".$open_year."',
										`close_status` = '0' "; 
							$this->db->query($insert2);
							// echo $insert1;
							// echo '<br />';
							// exit;
						} else {
							$leave_data = $is_exist->row;
							$insert2 = "UPDATE `oc_leave` SET 
										`emp_id` = '".$employee_id."', 
										`emp_name` = '".$this->db->escape($var1)."', 
										`emp_doj` = '".$var2."',
										WHERE `leave_id` = '".$leave_data['leave_id']."' "; 
							$this->db->query($insert2);
						}
					} else {
						$final_employee_error_data[] = $employee_error_data;	
					}
					$emp_code_exist[$var0] = $var0;
					//echo 'out';exit;
				} else {
					if(isset($emp_code_exist[$var0])){
						$employee_error_data[] = array(
							'line_number' => $i,
							'reason' => 'Employee Code Duplicate'
						);
					}
					if($var0 == '' || $var0 == '0'){
						$employee_error_data[] = array(
							'line_number' => $i,
							'reason' => 'Employee Code Empty'
						);
					}
					$final_employee_error_data[] = $employee_error_data;
				}
			}
			$i ++;	
		}
		echo 'out';
		exit;
		fclose($file);
		echo '<pre>';
		print_r($final_employee_error_data);
		exit;
		echo 'out1111';exit;

		$t = DIR_DOWNLOAD."PERDAYDATA_CSV.csv";
		//echo $t;exit;
		$file=fopen($t,"r");
		$i=1;
		while(($var=fgetcsv($file,1000,","))!==FALSE){
			if($i != 1) {
				// echo '<pre>';
				// print_r($var);
				// exit;
				$var0=addslashes($var[0]);//emp_code
				if($var0 != ''){
					$var00 = sprintf('%03d', $var0);
					$var0 = '2'.$var00;
					//echo $var1;exit;
					$var11 = addslashes($var[1]);//gender
					if($var11 == 'M'){
						$var1 = 'Male';
					} elseif($var11 == 'F'){
						$var1 = 'Female';
					}
					$var2 = addslashes($var[2]);//basic no
					if($var2 != 'LEFT'){
						$var3 = addslashes($var[3]);//perday actual
						$var4 = addslashes($var[4]);//perday shown
						$emp_code = $var0;
						$update = "UPDATE `oc_employee` SET `gender` = '".$var1."', `basic` = '".$var2."', `perdaysalary` = '".$var3."', `perdaysalary2` = '".$var4."' WHERE `emp_code` = '".$emp_code."' "; 
						echo $update; 
						echo '<br />';
						//exit;
						$this->db->query($update);
					}
				}
			}
			$i ++;	
		}
		fclose($file);
		echo 'out';exit;
		
		/*
		$t = DIR_DOWNLOAD."MUSTERDATA_CSV.csv";
		//echo $t;exit;
		$file=fopen($t,"r");
		$i=1;
		while(($var=fgetcsv($file,1000,","))!==FALSE){
			if($i != 1) {
				// echo '<pre>';
				// print_r($var);
				// exit;
				$var0=addslashes($var[0]);//emp_code
				if($var0 != ''){
					$var000 = substr($var0, 1);
					$var00 = sprintf('%03d', $var000);
					$var0 = '1'.$var00;
					//echo $var1;exit;
					$var11 = addslashes($var[1]);//gender
					if($var11 == 'M'){
						$var1 = 'Male';
					} elseif($var11 == 'F'){
						$var1 = 'Female';
					}
					$var2 = addslashes($var[2]);//basic no
					$var3 = addslashes($var[3]);//incentive
					$emp_code = $var0;
					$update = "UPDATE `oc_employee` SET `gender` = '".$var1."', `basic` = '".$var2."', `incentive` = '".$var3."' WHERE `emp_code` = '".$emp_code."' "; 
					echo $update; 
					echo '<br />';
					//exit;
					$this->db->query($update);
				}
			}
			$i ++;	
		}
		fclose($file);
		echo 'out';exit;
		*/

		/*
		$t = DIR_DOWNLOAD."emp_list1.csv";
		//echo $t;exit;
		$file=fopen($t,"r");
		$i=1;
		while(($var=fgetcsv($file,1000,","))!==FALSE){
			if($i != 1) {
				// echo '<pre>';
				// print_r($var);
				// exit;
				$var1=addslashes($var[1]);//emp_code
				if($var1 != ''){
					$var11 = sprintf('%03d', $var1);
					$var1 = '2'.$var11;
					//echo $var1;exit;
					$var5 = addslashes($var[5]);//esic no
					$var666 = addslashes($var[6]);//uan no
					$var66 = explode('.', $var666);
					$var6 = $var66[0];
					$emp_code = $var1;
					$update = "UPDATE `oc_employee` SET 
								`esic_no` = '".$var5."',
								`pfuan_no` = '".$var6."'
								WHERE `emp_code` = '".$emp_code."' "; 
					echo $update; 
					echo '<br />';
					//exit;
					$this->db->query($update);
				}
			}
			$i ++;	
		}
		fclose($file);
		echo 'out';exit;
		*/

		/*
		$t = DIR_DOWNLOAD."emp_list2.csv";
		//echo $t;exit;
		$file=fopen($t,"r");
		$i=1;
		while(($var=fgetcsv($file,1000,","))!==FALSE){
			if($i != 1) {
				// echo '<pre>';
				// print_r($var);
				// exit;
				$var1=addslashes($var[1]);//emp_code
				if($var1 != ''){
					$var11 = sprintf('%03d', $var1);
					$var1 = '2'.$var11;
					//echo $var1;exit;
					$var4 = addslashes($var[4]);//esic no
					$var555 = addslashes($var[5]);//uan no
					$var55 = explode('.', $var555);
					$var5 = $var55[0];
					$emp_code = $var1;
					$update = "UPDATE `oc_employee` SET 
								`esic_no` = '".$var4."',
								`pfuan_no` = '".$var5."'
								WHERE `emp_code` = '".$emp_code."' "; 
					echo $update; 
					echo '<br />';
					//exit;
					$this->db->query($update);
				}
			}
			$i ++;	
		}
		fclose($file);
		echo 'out';exit;
		*/

		/*
		$t = DIR_DOWNLOAD."emp_list3.csv";
		//echo $t;exit;
		$file=fopen($t,"r");
		$i=1;
		while(($var=fgetcsv($file,1000,","))!==FALSE){
			if($i != 1) {
				// echo '<pre>';
				// print_r($var);
				// exit;
				$var1=addslashes($var[1]);//emp_code
				if($var1 != ''){
					$var111 = substr($var1, 1);
					$var11 = sprintf('%03d', $var111);
					$var1 = '1'.$var11;
					//echo $var1;exit;
					$var3 = addslashes($var[3]);//esic no
					$var444 = addslashes($var[4]);//uan no
					$var44 = explode('.', $var444);
					$var4 = $var44[0];
					$emp_code = $var1;
					$update = "UPDATE `oc_employee` SET 
								`esic_no` = '".$var3."',
								`pfuan_no` = '".$var4."'
								WHERE `emp_code` = '".$emp_code."' "; 
					echo $update; 
					echo '<br />';
					//exit;
					$this->db->query($update);
				}
			}
			$i ++;	
		}
		fclose($file);
		echo 'out';exit;
		*/

		/*
		
		

		$t = DIR_DOWNLOAD."emp_list2.csv";
		//echo $t;exit;
		$file=fopen($t,"r");
		$i=1;
		$months_array = array(
			'1' => '1',
			'2' => '2',
			'3' => '3',
			'4' => '4',
			'5' => '5',
			'6' => '6',
			'7' => '7',
			'8' => '8',
			'9' => '9',
			'10' => '10',
			'11' => '11',
			'12' => '12',
		);
		while(($var=fgetcsv($file,1000,","))!==FALSE){
			if($i != 1) {
				// echo '<pre>';
				// print_r($var);
				// exit;
				$var1=addslashes($var[1]);//emp_code
				if($var1 != ''){
					$var11 = sprintf('%03d', $var1);
					$var1 = '2'.$var11;
					//echo $var1;exit;
					$var2=addslashes($var[2]);//designation
					$var3=addslashes($var[3]);//doj
					if($var3 != ''){
						$var3 = Date('Y-m-d', strtotime($var3));
					} else {
						$var3 = '0000-00-00';
					}
					$var4 = addslashes($var[4]);//esic no
					$var5 = addslashes($var[5]);//uan no
					$var6 = addslashes($var[6]);//department
					if($var6 == 'FAB'){
						$department_id = 1;
						$department = 'FAB';
					} elseif($var6 == 'EXTRUSION'){
						$department_id = 2;
						$department = 'EXTRUSION';
					} elseif($var6 == 'SCREEN'){
						$department_id = 3;
						$department = 'SCREEN';
					} else {
						$department_id = 4;
						$department = 'OTHER';
					}
					$var7 = addslashes(trim($var[7]));//name
					//$var8 = addslashes($var[8]);//basic
					$var8 = addslashes($var[8]);//perdaysalary
					
					$emp_code = $var1;
					$user_name = $emp_code;
					$salt = substr(md5(uniqid(rand(), true)), 0, 9);
					$password = sha1($salt . sha1($salt . sha1($emp_code)));
					$insert = "INSERT INTO `oc_employee` SET 
								`emp_code` = '".$var1."', 
								`title` = '', 
								`name` = '".$this->db->escape($var7)."', 
								`department` = '".$department."',
								`department_id` = '".$department_id."',
								`group` = '', 
								`category` = '', 
								`unit` = 'Mumbai', 
								`designation` = 'Worker', 
								`designation_id` = '1', 
								`doj` = '".$var3."', 
								`doc` = '', 
								`email` = '', 
								`shift_type` = 'F',
								`user_group_id` = '11',
								`status` = '1',
								`esic_no` = '".$var4."',
								`pfuan_no` = '".$var5."',
								`perdaysalary` = '".$var8."',
								`username` = '".$user_name."', 
								`password` = '".$password."', 
								`salt` = '".$salt."' "; 
					//	echo $insert; exit;
					$this->db->query($insert);
					
					foreach ($months_array as $key => $value) {
						$insert1 = "INSERT INTO `oc_shift_schedule` SET 
								`emp_code` = '".$var1."',
								`1` = 'S_1',
								`2` = 'S_1',
								`3` = 'S_1',
								`4` = 'S_1',
								`5` = 'S_1',
								`6` = 'S_1',
								`7` = 'S_1',
								`8` = 'S_1',
								`9` = 'S_1',
								`10` = 'S_1',
								`11` = 'S_1',
								`12` = 'S_1', 
								`13` = 'S_1', 
								`14` = 'S_1', 
								`15` = 'S_1', 
								`16` = 'S_1', 
								`17` = 'S_1', 
								`18` = 'S_1', 
								`19` = 'S_1', 
								`20` = 'S_1', 
								`21` = 'S_1', 
								`22` = 'S_1', 
								`23` = 'S_1', 
								`24` = 'S_1', 
								`25` = 'S_1', 
								`26` = 'S_1', 
								`27` = 'S_1', 
								`28` = 'S_1', 
								`29` = 'S_1', 
								`30` = 'S_1', 
								`31` = 'S_1', 
								`month` = '".$key."',
								`year` = '2017',
								`status` = '1' ,
								`unit` = 'Mumbai' "; 
								//echo "<pre>";
								//echo $insert1;
						$this->db->query($insert1);
					}
					// exit;

					$insert2 = "INSERT INTO `oc_leave` SET 
								`emp_id` = '".$var1."', 
								`emp_name` = '".$this->db->escape($var7)."', 
								`emp_doj` = '".$var3."', 
								`year` = '2017',
								`close_status` = '0' "; 
					$this->db->query($insert2);
					// echo $insert2;
					//echo '<br />';
					 //exit;
				}
			}
			$i ++;	
		}
		fclose($file);
		//echo 'out';exit;
		

		$t = DIR_DOWNLOAD."emp_list3.csv";
		//echo $t;exit;
		$file=fopen($t,"r");
		$i=1;
		$months_array = array(
			'1' => '1',
			'2' => '2',
			'3' => '3',
			'4' => '4',
			'5' => '5',
			'6' => '6',
			'7' => '7',
			'8' => '8',
			'9' => '9',
			'10' => '10',
			'11' => '11',
			'12' => '12',
		);
		while(($var=fgetcsv($file,1000,","))!==FALSE){
			if($i != 1) {
				// echo '<pre>';
				// print_r($var);
				// exit;
				$var1=addslashes($var[1]);//emp_code
				if($var1 != ''){
					$var111 = substr($var1, 1);
					$var11 = sprintf('%03d', $var111);
					$var1 = '1'.$var11;
					$var2=addslashes($var[2]);//doj
					if($var2 != ''){
						$var2 = Date('Y-m-d', strtotime($var2));
					} else {
						$var2 = '0000-00-00';
					}
					$var3 = addslashes($var[3]);//esic no
					$var4 = addslashes($var[4]);//uan no
					$department = 'OTHER';
					$department_id = '2';
					// $var6 = addslashes($var[6]);//department
					// if($var6 == 'FAB'){
					// 	$department_id = 1;
					// 	$department = 'FAB';
					// } elseif($var6 == 'EXTRUSION'){
					// 	$department_id = 2;
					// 	$department = 'EXTRUSION';
					// } elseif($var6 == 'SCREEN'){
					// 	$department_id = 3;
					// 	$department = 'SCREEN';
					// } else {
					// 	$department_id = 4;
					// 	$department = 'OTHER';
					// }
					$var5 = addslashes(trim($var[5]));//name
					$var6 = addslashes($var[6]);//basic
					$var7 = addslashes($var[7]);//incentive
					
					$emp_code = $var1;
					$user_name = $emp_code;
					$salt = substr(md5(uniqid(rand(), true)), 0, 9);
					$password = sha1($salt . sha1($salt . sha1($emp_code)));
					$insert = "INSERT INTO `oc_employee` SET 
								`emp_code` = '".$var1."', 
								`title` = '', 
								`name` = '".$this->db->escape($var5)."', 
								`department` = '".$department."',
								`department_id` = '".$department_id."',
								`group` = '', 
								`category` = '', 
								`unit` = 'Mumbai', 
								`designation` = 'Worker', 
								`designation_id` = '1', 
								`doj` = '".$var2."', 
								`doc` = '', 
								`email` = '', 
								`shift_type` = 'F',
								`user_group_id` = '11',
								`status` = '1',
								`esic_no` = '".$var3."',
								`pfuan_no` = '".$var4."',
								`basic` = '".$var6."',
								`incentive` = '".$var7."',
								`username` = '".$user_name."', 
								`password` = '".$password."', 
								`salt` = '".$salt."' "; 
					//	echo $insert; exit;
					$this->db->query($insert);
					
					foreach ($months_array as $key => $value) {
						$insert1 = "INSERT INTO `oc_shift_schedule` SET 
								`emp_code` = '".$var1."',
								`1` = 'S_1',
								`2` = 'S_1',
								`3` = 'S_1',
								`4` = 'S_1',
								`5` = 'S_1',
								`6` = 'S_1',
								`7` = 'S_1',
								`8` = 'S_1',
								`9` = 'S_1',
								`10` = 'S_1',
								`11` = 'S_1',
								`12` = 'S_1', 
								`13` = 'S_1', 
								`14` = 'S_1', 
								`15` = 'S_1', 
								`16` = 'S_1', 
								`17` = 'S_1', 
								`18` = 'S_1', 
								`19` = 'S_1', 
								`20` = 'S_1', 
								`21` = 'S_1', 
								`22` = 'S_1', 
								`23` = 'S_1', 
								`24` = 'S_1', 
								`25` = 'S_1', 
								`26` = 'S_1', 
								`27` = 'S_1', 
								`28` = 'S_1', 
								`29` = 'S_1', 
								`30` = 'S_1', 
								`31` = 'S_1', 
								`month` = '".$key."',
								`year` = '2017',
								`status` = '1' ,
								`unit` = 'Mumbai' "; 
								//echo "<pre>";
								//echo $insert1;
						$this->db->query($insert1);
					}
					// exit;

					$insert2 = "INSERT INTO `oc_leave` SET 
								`emp_id` = '".$var1."', 
								`emp_name` = '".$this->db->escape($var5)."', 
								`emp_doj` = '".$var2."', 
								`year` = '2017',
								`close_status` = '0' "; 
					$this->db->query($insert2);
					// echo $insert2;
					//echo '<br />';
					 //exit;
				}
			}
			$i ++;	
		}
		fclose($file);
		echo 'out';exit;
		*/
	}

	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	public function array_sort($array, $on, $order=SORT_ASC){

	    $new_array = array();
	    $sortable_array = array();

	    if (count($array) > 0) {
	        foreach ($array as $k => $v) {
	            if (is_array($v)) {
	                foreach ($v as $k2 => $v2) {
	                    if ($k2 == $on) {
	                        $sortable_array[$k] = $v2;
	                    }
	                }
	            } else {
	                $sortable_array[$k] = $v;
	            }
	        }

	        switch ($order) {
	            case SORT_ASC:
	                asort($sortable_array);
	                break;
	            case SORT_DESC:
	                arsort($sortable_array);
	                break;
	        }

	        foreach ($sortable_array as $k => $v) {
	            $new_array[$k] = $array[$k];
	        }
	    }

	    return $new_array;
	}
}
?>