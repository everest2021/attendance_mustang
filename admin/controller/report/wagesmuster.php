<?php
class ControllerReportWagesMuster extends Controller { 
	public function index() {  
		$this->language->load('report/wagesmuster');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			//$filter_date_start = date('Y-m-d');
			$from = date('Y-m-d');
			$filter_date_start = date('Y-m-d', strtotime($from . "-1 day"));
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			//$filter_date_start = date('Y-m-d');
			$from = date('Y-m-d');
			$filter_date_end = date('Y-m-d', strtotime($from . "+29 day"));
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = html_entity_decode($this->request->get['filter_name']);
		} else {
			$filter_name = "";
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = html_entity_decode($this->request->get['filter_name_id']);
		} else {
			$filter_name_id = "";
		}

		if (isset($this->request->get['filter_gender'])) {
			$filter_gender = html_entity_decode($this->request->get['filter_gender']);
		} else {
			$filter_gender = "";
		}

		if (isset($this->request->get['filter_unit'])) {
			$filter_unit = html_entity_decode($this->request->get['filter_unit']);
		} else {
			$filter_unit = "";
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_gender'])) {
			$url .= '&filter_gender=' . $this->request->get['filter_gender'];
		}

		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('report/wagesmuster', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->load->model('report/wagesmuster');
		$this->load->model('catalog/shift');

		$data['attendace'] = array();

		$data = array(
			'filter_date_start'	     => $filter_date_start,
			'filter_date_end'	     => $filter_date_end,
			'filter_name'	    	 => $filter_name,
			'filter_name_id'	     => $filter_name_id,
			'filter_gender'          => $filter_gender,
			'filter_unit'            => $filter_unit,
			'start'                  => ($page - 1) * 7000,
			'limit'                  => 7000
		);
		
		$this->data['export'] = $this->url->link('report/wagesmuster/export', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['dailyreports'] = array();
		
		$day = array();
        $days = $this->GetDays($filter_date_start, $filter_date_end);
        foreach ($days as $dkey => $dvalue) {
        	$dates = explode('-', $dvalue);
        	$day[$dvalue]['day'] = $dates[2];
        	$day[$dvalue]['date'] = $dvalue;
        }

  //       echo '<pre>';
		// print_r($day);
		// exit;

		if(isset($this->request->get['once']) && $this->request->get['once'] == '1'){
			$emp_datas = $this->model_report_wagesmuster->getEmployees($data);
			foreach($emp_datas as $ekey => $evalue){
				$emp_code_start = substr($evalue['emp_code'], 0, 1);
				if($emp_code_start == '1'){
					$time_arr = array();
					$data['filter_name_id'] = $evalue['emp_code'];
					$resultss = $this->model_report_wagesmuster->getAttendance($data);
					$days_present = 0;
					$days_absent = 0;
					$days_leave = 0;
					$actual_overtime = 0;
					$actual_overtime_array= array();
					$calculated_overtime = 0;
					$calculated_overtime_array = array();
					$earned_basic = 0;
					$hra = 0;
					$ot_amount = 0;
					$difference_ot_hours = 0;
					$difference_ot_value = 0;
					$other_allowance = 0;
					$pa = 0;
					$tea_snacks = 0;
					$gross = 0;
					$pf = 0;
					$esic = 0;
					$pt = 0;
					$net_paid = 0;
					$total_working_hours = '00:00:00';
					// echo '<pre>';
					// print_r($results);
					// exit;
					$cnt = 0;
					// echo '<pre>';
					// print_r($results);
					// exit;

					$results = array();
					foreach($resultss as $rkey => $rvalue){
						$results['action'][$rvalue['date']] = $rvalue;
					}
					//$results['action'] = array();
					foreach ($day as $dkey => $dvalue) {
						foreach ($results as $pkey => $pvalue) {
							////$ids = array_search('2017-06-26', array_column($pvalue, 'date'));
							//$ids = array_column($results, 'date');
							// echo '<pre>';
							// print_r($pvalue);
							// echo '<pre>';
							// print_r($pvalue);
							// exit;
							//if(in_array($dvalue['date'], $ids)){
							if(isset($pvalue[$dvalue['date']]['date'])){
							} else {
								$results['action'][$dvalue['date']] = array(
									'transaction_id' => '0',
									'present_status' => '0',
									'absent_status' => '1',
									'holiday_id' => '0',
									'weekly_off' => '0',
									'leave_status' => '0',
									'working_time' => '00:00:00',
									'overtime' => '00:00:00',
									'over_time' => '00:00:00',
									'over_time' => '00:00:00',
									'date' => $dvalue['date'],
								); 
							}
						}
					}
					

					// echo '<pre>';
					// print_r($results);
					// exit;
					if(isset($results['action'])) {
						foreach ($results['action'] as $result) {
							if($result['present_status'] == '1' || $result['holiday_id'] != '0' || $result['weekly_off'] != '0'){
								$days_present ++;
							} elseif($result['present_status'] == '0.5'){
								$days_present = $days_present + 0.5;
							}
							if($result['absent_status'] == '1'){
								$days_absent ++;
							} elseif($result['absent_status'] == '0.5'){
								$days_absent = $days_absent + 0.5;
							}
							if($result['leave_status'] == '1'){
								$days_leave ++;
							} elseif($result['leave_status'] == '0.5') {
								$days_leave = $days_leave + 1;
							}
							if($result['working_time'] != '00:00:00'){
								$working_time = $result['working_time'];
								$cnt ++;
								$working_time = date('H:i:s', strtotime($working_time . "-30 minutes"));
								$time_arr[] = $working_time;
							}
							if($result['overtime'] != '00:00:00'){
								$actualovertime = $result['overtime'];
								$actualovertime = date('H:i:s', strtotime($actualovertime));
								$actual_overtime_array[] = $actualovertime;
							}
							if($result['over_time'] != '00:00:00' && $result['overtime'] != '00:00:00'){
								$calculatedovertime = $result['over_time'];
								$calculatedovertime = date('H:i:s', strtotime($calculatedovertime));
								$calculated_overtime_array[] = $calculatedovertime;
							}

						}
					}

					// echo '<pre>';
					// print_r($actual_overtime_array);
					// echo '<pre>';
					// print_r($calculated_overtime_array);
					// exit;
					
					$calculated_overtime = '0';
					$actual_overtime = '0';
					if($evalue['ot_calculate'] == '1'){
						$time = 0;
						$hours = 0;
						$minutes = 0;
						foreach ($actual_overtime_array as $time_val) {
							$times = explode(':', $time_val);
							$hours += $times[0];
							$minutes += $times[1];
						}
						$min_min = 0;
						$min_hours = 0;
						if($minutes > 0){
							$min_hours = floor($minutes / 60);
			    			$min_min = ($minutes % 60);
			    			$min_min = sprintf('%02d', $min_min);
			    		}
			    		$actual_overtime = ($hours + $min_hours).'.'.$min_min;

			    		$time = 0;
						$hours = 0;
						$minutes = 0;
						foreach ($calculated_overtime_array as $time_val) {
							$times = explode(':', $time_val);
							$hours += $times[0];
							$minutes += $times[1];
						}
						$min_min = 0;
						$min_hours = 0;
						if($minutes > 0){
							$min_hours = floor($minutes / 60);
			    			$min_min = ($minutes % 60);
			    			$min_min = sprintf('%02d', $min_min);
			    		}
			    		$calculated_overtime = ($hours + $min_hours).'.'.$min_min;
					}

					$basic = $evalue['basic'];
					$incentive = $evalue['incentive'];
					$total = $basic + $incentive;
					//$total_days_present = $days_present;//26 - $days_absent;
					$sal_month = date('n', strtotime($filter_date_start));
					$sal_year = date('Y', strtotime($filter_date_start));
					$total_days_month = cal_days_in_month(CAL_GREGORIAN, $sal_month, $sal_year);
					//echo $days_present;exit;
					$total_days_present = $total_days_month - $days_absent;

					$earned_basic = round(($evalue['basic'] / $total_days_month) * ($days_present + $days_leave));
					$hra = round(($earned_basic * 5) / 100);
					$other_allowance = round((($incentive / $total_days_month) * ($days_present + $days_leave)) - $hra);
					
					//$gross_earned = $earned_basic + $hra + $other_allowance + $pa + $tea_snacks;
					$gross_earned = $earned_basic + $hra + $other_allowance;
					if($evalue['ot_days'] != '' && $evalue['ot_days'] != '0'){
						$calculated_overtime = $evalue['ot_days'];
						$ot_amount = round(($gross_earned / $total_days_month) * $evalue['ot_days']);
						$tea_snacks = 0;
						$pa = 0;
						$difference_ot_hours = 0;
					} elseif($evalue['ot_hours'] != '' && $evalue['ot_hours'] != '0'){
						$calculated_overtime = $evalue['ot_hours'];
						$ot_amount = round($calculated_overtime * 50);
						$tea_snacks = 0;
						$pa = 0;
						$difference_ot_hours = 0;
					} else {
						$ot_amount = round((((($basic / 26) / 8) * $calculated_overtime) * 1.5));
									
						$difference_ot_hours = $actual_overtime - $calculated_overtime;
						if($difference_ot_hours > 0){
							$difference_ot_value = round((((($basic / 26) / 8) * $difference_ot_hours) * 1.5));
							$tea_snacks = $difference_ot_value % 500;
							$pa = $difference_ot_value - $tea_snacks;
						} else {
							$difference_ot_hours = 0;
						}
					}
					$gross = $gross_earned + $ot_amount + $pa + $tea_snacks;

					$esic = 0;
					if($total <= 21000){
						$esic = round(($gross_earned * 1.75) / 100);
					}
					$pf = round(($earned_basic * 12) / 100);
					if($evalue['gender'] == 'Female'){
						if($gross > '10000'){
							if(date('n', strtotime($filter_date_end)) == '2'){
								$pt = '300';
							} else {
								$pt = '200';
							}
						} else {
							$pt = '0';
						}
					} else {
						if($gross > '7500' && $gross <= '10000'){
							$pt = '175';
						} elseif($gross > '10000'){
							if(date('n', strtotime($filter_date_end)) == '2'){
								$pt = '300';
							} else {
								$pt = '200';
							}
						} else {
							$pt = '0';
						}
					}
					$net_paid = $gross - $pf - $esic - $pt;
					//$gross = round(($evalue['perdaysalary2'] / 8) * $total_working_hours);
					// if($other_allowance < '0'){
					// 	$other_allowance = '0';
					// }
					// echo 'Present : ' . $days_present;
					// echo '<br />';
					// echo 'Absent : ' . $days_absent;
					// echo '<br />';
					// echo 'Leave : ' . $days_leave;
					// echo '<br />';
					// echo 'Basic : ' . $basic;
					// echo '<br />';
					// echo 'Incentive : ' . $incentive;
					// echo '<br />';
					// echo 'Basic_Incentive : ' . $total;
					// echo '<br />';
					// echo 'Days Present : ' . $total_days_present;
					// echo '<br />';
					// echo 'Earned Basic : ' . $earned_basic;
					// echo '<br />';
					// echo 'HRA : ' . $hra;
					// echo '<br />';
					// echo 'Other Allowance : ' . $other_allowance;
					// echo '<br />';
					// echo 'Actual Overtime : ' . $actual_overtime;
					// echo '<br />';
					// echo 'Calculated Overtime : ' . $calculated_overtime;
					// echo '<br />';
					// echo 'OT Amount : ' . $ot_amount;
					// echo '<br />';
					// echo 'Difference OT Hours : ' . $difference_ot_hours;
					// echo '<br />';
					// echo 'Difference OT Value : ' . $difference_ot_value;
					// echo '<br />';
					// echo 'PA : ' . $pa;
					// echo '<br />';
					// echo 'Tea Snacks : ' . $tea_snacks;
					// echo '<br />';
					// echo 'Gross Earned : ' . $gross_earned;
					// echo '<br />';
					// echo 'Gross : ' . $gross;
					// echo '<br />';
					// echo 'ESIC : ' . $esic;
					// echo '<br />';
					// echo 'PT : ' . $pt;
					// echo '<br />';
					// echo 'PF : ' . $pf;
					// echo '<br />';
					// echo 'Net : ' . $net_paid;
					// echo '<br />';
					// echo '<br />';
					// exit;
					if($evalue['doj'] != '0000-00-00'){
						$doj = date('d-M-Y', strtotime($evalue['doj']));
					} else {
						$doj = '';
					}
					$this->data['dailyreports'][] = array(
						'emp_name'    => $evalue['name'],
						'emp_code'   => substr($evalue['emp_code'], 1),
						'department'   => $evalue['department'],
						'designation'   => $evalue['designation'],
						'doj'   => $doj,
						'esic_no'		 => $evalue['esic_no'],
						'pf_no' => $evalue['pfuan_no'],
						'fixed_basic' => $evalue['basic'],
						'incentive' => $evalue['incentive'],
						'total' => $total,
						'days_present' => $days_present,
						'days_absent' => $days_absent,
						'days_leave' => $days_leave,
						'total_days_present' => $total_days_present,
						'earned_basic'	=> $earned_basic,
						'hra'	=> $hra,
						'other_allowance'	=> $other_allowance,
						'pa'	=> $pa,
						'tea_snacks'	=> $tea_snacks,
						'gross_earned'	=> $gross_earned,
						'calculated_overtime'	=> $calculated_overtime,
						'ot_amount'	=> $ot_amount,
						'gross'	=> $gross,
						'esic'	=> $esic,
						'pt'	=> $pt,
						'pf'	=> $pf,
						'net_paid'	=> $net_paid
					);
				}
			}
		}

		$this->data['genders'] = array(
			'' => 'All',
			'1' => 'Male',
			'2' => 'Female',
		);

		$this->data['unit_data'] = array(
			'' => 'All',
			'Unit 31' => 'Unit 31',
			'Unit 64' => 'Unit 64',
		);

		// echo '<pre>';
		// print_r($this->data['dailyreports']);
		// exit;
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_all_status'] = $this->language->get('text_all_status');

		
		$this->data['entry_date_start'] = $this->language->get('entry_date_start');
		$this->data['entry_date_end'] = $this->language->get('entry_date_end');
		
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['button_export'] = $this->language->get('button_export');

		$this->data['token'] = $this->session->data['token'];

		if(isset($this->data['warning'])){
			$this->data['error_warning'] = $this->data['warning'];
		} elseif(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_gender'])) {
			$url .= '&filter_gender=' . $this->request->get['filter_gender'];
		}

		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}
		
		$this->data['token'] = $this->session->data['token'];

		$this->data['filter_date_start'] = $filter_date_start;
		$this->data['filter_date_end'] = $filter_date_end;
		$this->data['filter_name'] = $filter_name;
		$this->data['filter_name_id'] = $filter_name_id;
		$this->data['filter_gender'] = $filter_gender;
		$this->data['filter_unit'] = $filter_unit;
		
		$this->template = 'report/wagesmuster.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	public function explode_time($time) { //explode time and convert into seconds
        $time = explode(':', $time);
        $time = $time[0] * 3600 + $time[1] * 60;
        return $time;
	}

	public function second_to_hhmm($time) { //convert seconds to hh:mm
        $hour = floor($time / 3600);
        $minute = strval(floor(($time % 3600) / 60));
        if ($minute == 0) {
            $minute = "00";
        } else {
            $minute = $minute;
        }
        $time = $hour . "." . $minute;
        return $time;
	}

	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	public function export(){
		$this->language->load('report/wagesmuster');
		$this->load->model('report/common_report');
		$this->load->model('report/wagesmuster');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			$from = date('Y-m-d');
			$filter_date_start = date('Y-m-d', strtotime($from . "-1 day"));
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$from = date('Y-m-d');
			$filter_date_end = date('Y-m-d', strtotime($from . "+29 day"));
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = html_entity_decode($this->request->get['filter_name']);
		} else {
			$filter_name = "";
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = html_entity_decode($this->request->get['filter_name_id']);
		} else {
			$filter_name_id = "";
		}

		if (isset($this->request->get['filter_gender'])) {
			$filter_gender = html_entity_decode($this->request->get['filter_gender']);
		} else {
			$filter_gender = "";
		}

		if (isset($this->request->get['filter_unit'])) {
			$filter_unit = html_entity_decode($this->request->get['filter_unit']);
		} else {
			$filter_unit = "";
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_gender'])) {
			$url .= '&filter_gender=' . $this->request->get['filter_gender'];
		}

		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('report/dailyattendance', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->load->model('report/dailyattendance');

		$data['attendace'] = array();

		$data = array(
			'filter_date_start'	     => $filter_date_start,
			'filter_date_end'	     => $filter_date_end,
			'filter_name'	    	 => $filter_name,
			'filter_name_id'	     => $filter_name_id,
			'filter_gender'	     	 => $filter_gender,
			'filter_unit'	     	 => $filter_unit,
		);

		$day = array();
        $days = $this->GetDays($filter_date_start, $filter_date_end);
        foreach ($days as $dkey => $dvalue) {
        	$dates = explode('-', $dvalue);
        	$day[$dvalue]['day'] = $dates[2];
        	$day[$dvalue]['date'] = $dvalue;
        }

		$emp_datas = $this->model_report_wagesmuster->getEmployees($data);
		foreach($emp_datas as $ekey => $evalue){
			$emp_code_start = substr($evalue['emp_code'], 0, 1);
			if($emp_code_start == '1'){
				$time_arr = array();
				$data['filter_name_id'] = $evalue['emp_code'];
				$resultss = $this->model_report_wagesmuster->getAttendance($data);
				$days_present = 0;
				$days_absent = 0;
				$days_leave = 0;
				$actual_overtime = 0;
				$actual_overtime_array= array();
				$calculated_overtime = 0;
				$calculated_overtime_array = array();
				$earned_basic = 0;
				$hra = 0;
				$ot_amount = 0;
				$difference_ot_hours = 0;
				$difference_ot_value = 0;
				$other_allowance = 0;
				$pa = 0;
				$tea_snacks = 0;
				$gross = 0;
				$pf = 0;
				$esic = 0;
				$pt = 0;
				$net_paid = 0;
				$total_working_hours = '00:00:00';
				// echo '<pre>';
				// print_r($results);
				// exit;
				$cnt = 0;
				// echo '<pre>';
				// print_r($results);
				// exit;

				$results = array();
				foreach($resultss as $rkey => $rvalue){
					$results['action'][$rvalue['date']] = $rvalue;
				}

				foreach ($day as $dkey => $dvalue) {
					foreach ($results as $pkey => $pvalue) {
						////$ids = array_search('2017-06-26', array_column($pvalue, 'date'));
						//$ids = array_column($results, 'date');
						// echo '<pre>';
						// print_r($pvalue);
						// echo '<pre>';
						// print_r($pvalue);
						// exit;
						//if(in_array($dvalue['date'], $ids)){
						if(isset($pvalue[$dvalue['date']]['date'])){
						} else {
							$results['action'][$dvalue['date']] = array(
								'transaction_id' => '0',
								'present_status' => '0',
								'absent_status' => '1',
								'holiday_id' => '0',
								'weekly_off' => '0',
								'leave_status' => '0',
								'working_time' => '00:00:00',
								'overtime' => '00:00:00',
								'over_time' => '00:00:00',
								'over_time' => '00:00:00',
								'date' => $dvalue['date'],
							); 
						}
					}
				}
				

				// echo '<pre>';
				// print_r($results);
				// exit;
				if(isset($results['action'])) {
					foreach ($results['action'] as $result) {
						if($result['present_status'] == '1' || $result['holiday_id'] != '0' || $result['weekly_off'] != '0'){
							$days_present ++;
						} elseif($result['present_status'] == '0.5'){
							$days_present = $days_present + 0.5;
						}
						if($result['absent_status'] == '1'){
							$days_absent ++;
						} elseif($result['absent_status'] == '0.5'){
							$days_absent = $days_absent + 0.5;
						}
						if($result['leave_status'] == '1'){
							$days_leave ++;
						} elseif($result['leave_status'] == '0.5') {
							$days_leave = $days_leave + 1;
						}
						if($result['working_time'] != '00:00:00'){
							$working_time = $result['working_time'];
							$cnt ++;
							$working_time = date('H:i:s', strtotime($working_time . "-30 minutes"));
							$time_arr[] = $working_time;
						}
						if($result['overtime'] != '00:00:00'){
							$actualovertime = $result['overtime'];
							$actualovertime = date('H:i:s', strtotime($actualovertime));
							$actual_overtime_array[] = $actualovertime;
						}
						if($result['over_time'] != '00:00:00' && $result['overtime'] != '00:00:00'){
							$calculatedovertime = $result['over_time'];
							$calculatedovertime = date('H:i:s', strtotime($calculatedovertime));
							$calculated_overtime_array[] = $calculatedovertime;
						}

					}
				}
				
				$calculated_overtime = '0';
				$actual_overtime = '0';
				if($evalue['ot_calculate'] == '1'){
					$time = 0;
					$hours = 0;
					$minutes = 0;
					foreach ($actual_overtime_array as $time_val) {
						$times = explode(':', $time_val);
						$hours += $times[0];
						$minutes += $times[1];
					}
					$min_min = 0;
					$min_hours = 0;
					if($minutes > 0){
						$min_hours = floor($minutes / 60);
		    			$min_min = ($minutes % 60);
		    			$min_min = sprintf('%02d', $min_min);
		    		}
		    		$actual_overtime = ($hours + $min_hours).'.'.$min_min;

		    		$time = 0;
					$hours = 0;
					$minutes = 0;
					foreach ($calculated_overtime_array as $time_val) {
						$times = explode(':', $time_val);
						$hours += $times[0];
						$minutes += $times[1];
					}
					$min_min = 0;
					$min_hours = 0;
					if($minutes > 0){
						$min_hours = floor($minutes / 60);
		    			$min_min = ($minutes % 60);
		    			$min_min = sprintf('%02d', $min_min);
		    		}
		    		$calculated_overtime = ($hours + $min_hours).'.'.$min_min;
				}

				$basic = $evalue['basic'];
				$incentive = $evalue['incentive'];
				$total = $basic + $incentive;
				//$total_days_present = $days_present;//26 - $days_absent;
				$sal_month = date('n', strtotime($filter_date_start));
				$sal_year = date('Y', strtotime($filter_date_start));
				$total_days_month = cal_days_in_month(CAL_GREGORIAN, $sal_month, $sal_year);
				//echo $days_present;exit;
				$total_days_present = $total_days_month - $days_absent;

				$earned_basic = round(($evalue['basic'] / $total_days_month) * ($days_present + $days_leave));
				$hra = round(($earned_basic * 5) / 100);
				$other_allowance = round((($incentive / $total_days_month) * ($days_present + $days_leave)) - $hra);
				
				//$gross_earned = $earned_basic + $hra + $other_allowance + $pa + $tea_snacks;
				$gross_earned = $earned_basic + $hra + $other_allowance;
				if($evalue['ot_days'] != '' && $evalue['ot_days'] != '0'){
					$calculated_overtime = $evalue['ot_days'];
					$ot_amount = round(($gross_earned / $total_days_month) * $evalue['ot_days']);
					$tea_snacks = 0;
					$pa = 0;
					$difference_ot_hours = 0;
				} elseif($evalue['ot_hours'] != '' && $evalue['ot_hours'] != '0'){
					$calculated_overtime = $evalue['ot_hours'];
					$ot_amount = round($calculated_overtime * 50);
					$tea_snacks = 0;
					$pa = 0;
					$difference_ot_hours = 0;
				} else {
					$ot_amount = round((((($basic / 26) / 8) * $calculated_overtime) * 1.5));
								
					$difference_ot_hours = $actual_overtime - $calculated_overtime;
					if($difference_ot_hours > 0){
						$difference_ot_value = round((((($basic / 26) / 8) * $difference_ot_hours) * 1.5));
						$tea_snacks = $difference_ot_value % 500;
						$pa = $difference_ot_value - $tea_snacks;
					} else {
						$difference_ot_hours = 0;
					}
				}
				$gross = $gross_earned + $ot_amount + $pa + $tea_snacks;
				$esic = 0;
				if($total <= 21000){
					$esic = round(($gross_earned * 1.75) / 100);
				}
				$pf = round(($earned_basic * 12) / 100);
				if($evalue['gender'] == 'Female'){
					if($gross > '10000'){
						if(date('n', strtotime($filter_date_end)) == '2'){
							$pt = '300';
						} else {
							$pt = '200';
						}
					} else {
						$pt = '0';
					}
				} else {
					if($gross > '7500' && $gross <= '10000'){
						$pt = '175';
					} elseif($gross > '10000'){
						if(date('n', strtotime($filter_date_end)) == '2'){
							$pt = '300';
						} else {
							$pt = '200';
						}
					} else {
						$pt = '0';
					}
				}
				$net_paid = $gross - $pf - $esic - $pt;
				//$gross = round(($evalue['perdaysalary2'] / 8) * $total_working_hours);
				// if($other_allowance < '0'){
				// 	$other_allowance = '0';
				// }
				// echo 'Present : ' . $days_present;
				// echo '<br />';
				// echo 'Absent : ' . $days_absent;
				// echo '<br />';
				// echo 'Leave : ' . $days_leave;
				// echo '<br />';
				// echo 'Basic : ' . $basic;
				// echo '<br />';
				// echo 'Incentive : ' . $incentive;
				// echo '<br />';
				// echo 'Basic_Incentive : ' . $total;
				// echo '<br />';
				// echo 'Days Present : ' . $total_days_present;
				// echo '<br />';
				// echo 'Earned Basic : ' . $earned_basic;
				// echo '<br />';
				// echo 'HRA : ' . $hra;
				// echo '<br />';
				// echo 'Other Allowance : ' . $other_allowance;
				// echo '<br />';
				// echo 'Actual Overtime : ' . $actual_overtime;
				// echo '<br />';
				// echo 'Calculated Overtime : ' . $calculated_overtime;
				// echo '<br />';
				// echo 'OT Amount : ' . $ot_amount;
				// echo '<br />';
				// echo 'Difference OT Hours : ' . $difference_ot_hours;
				// echo '<br />';
				// echo 'Difference OT Value : ' . $difference_ot_value;
				// echo '<br />';
				// echo 'PA : ' . $pa;
				// echo '<br />';
				// echo 'Tea Snacks : ' . $tea_snacks;
				// echo '<br />';
				// echo 'Gross Earned : ' . $gross_earned;
				// echo '<br />';
				// echo 'Gross : ' . $gross;
				// echo '<br />';
				// echo 'ESIC : ' . $esic;
				// echo '<br />';
				// echo 'PT : ' . $pt;
				// echo '<br />';
				// echo 'PF : ' . $pf;
				// echo '<br />';
				// echo 'Net : ' . $net_paid;
				// echo '<br />';
				// echo '<br />';
				// exit;
				if($evalue['doj'] != '0000-00-00'){
					$doj = date('d-M-Y', strtotime($evalue['doj']));
				} else {
					$doj = '';
				}
				$dailyreports[] = array(
					'emp_name'    => $evalue['name'],
					'emp_code'   => substr($evalue['emp_code'], 1),
					'department'   => $evalue['department'],
					'designation'   => $evalue['designation'],
					'doj'   => $doj,
					'esic_no'		 => $evalue['esic_no'],
					'pf_no' => $evalue['pfuan_no'],
					'fixed_basic' => $evalue['basic'],
					'incentive' => $evalue['incentive'],
					'total' => $total,
					'days_present' => $days_present,
					'days_absent' => $days_absent,
					'days_leave' => $days_leave,
					'total_days_present' => $total_days_present,
					'earned_basic'	=> $earned_basic,
					'hra'	=> $hra,
					'other_allowance'	=> $other_allowance,
					'pa'	=> $pa,
					'tea_snacks'	=> $tea_snacks,
					'gross_earned'	=> $gross_earned,
					'calculated_overtime'	=> $calculated_overtime,
					'ot_amount'	=> $ot_amount,
					'gross'	=> $gross,
					'esic'	=> $esic,
					'pt'	=> $pt,
					'pf'	=> $pf,
					'net_paid'	=> $net_paid
				);
			}
		}
		
		if($dailyreports){
			$filter_month = date('n', strtotime($filter_date_start)); 
			$month = date("F", mktime(0, 0, 0, $filter_month, 10));

			$statuss = 'Wages Muster Report';
			$statusss = 'Wages Muster_Report';
			$template = new Template();		
			$template->data['dailyreports'] = $dailyreports;
			$template->data['month'] = $month;
			$template->data['title'] = $statuss;
			if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
				$template->data['base'] = HTTPS_SERVER;
			} else {
				$template->data['base'] = HTTP_SERVER;
			}
			$html = $template->fetch('report/wagesmuster_html.tpl');
			//echo $html;exit;
			// header('Content-type: text/html');
			// header('Content-Disposition: attachment; filename='.$filename);
			//echo $html;exit;
			$filename = "WagesMuster_".$filter_date_start;
			//echo $filename;exit();
			header("Content-Type: application/vnd.ms-excel; charset=utf-8");
			header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false);
			echo $html;
			exit;		
		} else {
			$this->session->data['warning'] = 'No Data Found';
			$this->redirect($this->url->link('report/wagesmuster', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/employee');

			if(isset($this->session->data['dept_names'])){
				$filter_departments = html_entity_decode($this->session->data['dept_names']);
				$filter_departments = "'" . str_replace(",", "','", html_entity_decode($filter_departments)) . "'";
			} else {
				$filter_departments = '';
			}

			if($filter_departments == ''){
				if(isset($this->session->data['dept_name'])){
					$filter_department = $this->session->data['dept_name'];
				} else {
					$filter_department = '';
				}
			} else {
				$filter_department = '';
			}

			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				'filter_department' => $filter_department,
				'filter_departments' => $filter_departments,
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_employee->getemployees($data);

			foreach ($results as $result) {
				$json[] = array(
					'employee_id' => $result['employee_id'],
					'emp_code' => $result['emp_code'], 
					'name'            => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}
}
?>