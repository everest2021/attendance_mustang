<?php
class ControllerReportToday extends Controller { 
	public function index() {  
		$this->language->load('report/attendance');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_name'])) {
			$filter_name = ($this->request->get['filter_name']);
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = ($this->request->get['filter_name_id']);
		} else {
			$filter_name_id = '';
		}
		
		if (isset($this->request->get['designation'])) {
			$designation = ($this->request->get['designation']);
		} else {
			$designation = '';
		}

		if (isset($this->request->get['unit'])) {
			$unit = $this->request->get['unit'];
		} else {
			$unit = '';	
		}

		if (isset($this->request->get['department'])) {
			$department = ($this->request->get['department']);
		} else {
			$department = '';
		}

		if (isset($this->request->get['group'])) {
			$group = $this->request->get['group'];
		} else {
			$group = 0;
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['designation'])) {
			$url .= '&designation=' . $this->request->get['designation'];
		}

		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}

		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('report/today', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);
		$this->data['generate_today'] = $this->url->link('transaction/transaction/generate_today', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['export'] = $this->url->link('report/today/export', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$this->load->model('report/attendance');

		$this->data['attendace'] = array();

		$data = array(
			'filter_name_id' 		=> $filter_name_id,
			'filter_name'			=> $filter_name,
			'designation'	=> $designation,
			'date'					=> date('Y-m-d'),
			'unit'			=> $unit,
			'department'		=> $department,
			'group'					=> $group, 
			'start'                 => ($page - 1) * 7000,
			'limit'                 => 7000
		);

		$final_datas = array();
		$results = $this->model_report_attendance->getTodaysAttendance($data);
		foreach ($results as $rkey => $rvalue) {
			$emp_data =$this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE `employee_id` ='".$rvalue['emp_id']."' ")->row;
			$final_datas[] = array(
				'emp_code' => $emp_data['emp_code'],
				'emp_name'	=> $rvalue['emp_name'],
				'shift_intime' => $rvalue['shift_intime'],
				'act_intime' => $rvalue['act_intime'],
				'late_time'=> $rvalue['late_time'],
			);		
		}
		
		$this->data['results'] = $final_datas;
		// echo'<pre>';
		// print_r($final_datas);
		// exit;
		
		$unit_datas = $this->model_report_attendance->getlocation_list();
		$unit_data = array();
		$unit_data['0'] = 'All';
		foreach ($unit_datas as $dkey => $dvalue) {
			$unit_data[$dvalue['unit_id']] = $dvalue['unit'];
		}
		$this->data['unit_data'] = $unit_data;
		
		$department_datas = $this->model_report_attendance->getdepartment_list();
		$department_data = array();
		$department_data['0'] = 'All';
		foreach ($department_datas as $dkey => $dvalue) {
			$department_data[$dvalue['department_id']] = $dvalue['department'];
		}
		$this->data['department_data'] = $department_data;


		$designation_datas = $this->model_report_attendance->getdesignation_list();
		$designation_data = array();
		$designation_data['0'] = 'All';
		foreach ($designation_datas as $dkey => $dvalue) {
			$designation_data[$dvalue['designation_id']] = $dvalue['designation'];
		}
		
		$this->data['designation_data'] = $designation_data;

		
		$group_datas = $this->model_report_attendance->getgroup_list();
		$group_data = array();
		$group_data['0'] = 'All';
		foreach ($group_datas as $gkey => $gvalue) {
			$group_data[$gvalue['group']] = $gvalue['group'];
		}
		
		$this->data['group_data'] = $group_data;
		
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_all_status'] = $this->language->get('text_all_status');

		
		$this->data['entry_date_start'] = $this->language->get('entry_date_start');
		$this->data['entry_date_end'] = $this->language->get('entry_date_end');
		
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['button_export'] = $this->language->get('button_export');

		$this->data['token'] = $this->session->data['token'];

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}

		if (isset($this->request->get['designation'])) {
			$url .= '&designation=' . $this->request->get['designation'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}

		$this->data['unit'] = $unit;
		$this->data['filter_name'] = $filter_name;
		$this->data['filter_name_id'] = $filter_name_id;
		$this->data['designation'] = $designation;
		$this->data['department'] = $department;
		$this->data['group'] = $group;

		if(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->template = 'report/today.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	public function export(){
		$this->language->load('report/attendance');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_name'])) {
			$filter_name = ($this->request->get['filter_name']);
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = ($this->request->get['filter_name_id']);
		} else {
			$filter_name_id = '';
		}
		
		if (isset($this->request->get['designation'])) {
			$designation = ($this->request->get['designation']);
		} else {
			$designation = '';
		}

		if (isset($this->request->get['unit'])) {
			$unit = $this->request->get['unit'];
		} else {
			$unit = '';	
		}

		if (isset($this->request->get['department'])) {
			$department = ($this->request->get['department']);
		} else {
			$department = '';
		}

		if (isset($this->request->get['group'])) {
			$group = $this->request->get['group'];
		} else {
			$group = 0;
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['designation'])) {
			$url .= '&designation=' . $this->request->get['designation'];
		}

		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}

		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('report/today', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);
		$this->data['generate_today'] = $this->url->link('transaction/transaction/generate_today', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['export'] = $this->url->link('report/today/export', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$this->load->model('report/attendance');

		$this->data['attendace'] = array();

		$data = array(
			'filter_name_id' 		=> $filter_name_id,
			'filter_name'			=> $filter_name,
			'designation'	=> $designation,
			'date'					=> date('Y-m-d'),
			'unit'			=> $unit,
			'department'		=> $department,
			'group'					=> $group, 
			'start'                 => ($page - 1) * 7000,
			'limit'                 => 7000
		);

		$final_datas = array();
		$results = $this->model_report_attendance->getTodaysAttendance($data);
			foreach ($results as $rkey => $rvalue) {
			$emp_data =$this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE `employee_id` ='".$rvalue['emp_id']."' ")->row;
			$final_datas[] = array(
				'emp_code' => $emp_data['emp_code'],
				'emp_name'	=> $rvalue['emp_name'],
				'shift_intime' => $rvalue['shift_intime'],
				'act_intime' => $rvalue['act_intime'],
				'late_time'=> $rvalue['late_time'],
				);		
			}
		
		$this->data['results'] = $final_datas;
		// echo'<pre>';
		// print_r($final_datas);
		// exit;
		
		$unit_datas = $this->model_report_attendance->getlocation_list();
		$unit_data = array();
		$unit_data['0'] = 'All';
		foreach ($unit_datas as $dkey => $dvalue) {
			$unit_data[$dvalue['unit_id']] = $dvalue['unit'];
		}
		$this->data['unit_data'] = $unit_data;
		
		$department_datas = $this->model_report_attendance->getdepartment_list();
		$department_data = array();
		$department_data['0'] = 'All';
		foreach ($department_datas as $dkey => $dvalue) {
			$department_data[$dvalue['department_id']] = $dvalue['department'];
		}
		$this->data['department_data'] = $department_data;


		$designation_datas = $this->model_report_attendance->getdesignation_list();
		$designation_data = array();
		$designation_data['0'] = 'All';
		foreach ($designation_datas as $dkey => $dvalue) {
			$designation_data[$dvalue['designation_id']] = $dvalue['designation'];
		}
		
		$this->data['designation_data'] = $designation_data;

		
		$group_datas = $this->model_report_attendance->getgroup_list();
		$group_data = array();
		$group_data['0'] = 'All';
		foreach ($group_datas as $gkey => $gvalue) {
			$group_data[$gvalue['group']] = $gvalue['group'];
		}
		
		$this->data['group_data'] = $group_data;
		
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_all_status'] = $this->language->get('text_all_status');

		
		$this->data['entry_date_start'] = $this->language->get('entry_date_start');
		$this->data['entry_date_end'] = $this->language->get('entry_date_end');
		
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['button_export'] = $this->language->get('button_export');

		$this->data['token'] = $this->session->data['token'];

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}

		if (isset($this->request->get['designation'])) {
			$url .= '&designation=' . $this->request->get['designation'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}

		$this->data['unit'] = $unit;
		$this->data['filter_name'] = $filter_name;
		$this->data['filter_name_id'] = $filter_name_id;
		$this->data['designation'] = $designation;
		$this->data['department'] = $department;
		$this->data['group'] = $group;

		if($final_datas){
			//$month = date("F", mktime(0, 0, 0, $filter_month, 10));
			//$tdays = cal_days_in_month(CAL_GREGORIAN, $filter_month, $filter_year);
			$template = new Template();		
			$template->data['final_datass'] = $final_datas;
			$template->data['date_start'] = $filter_date_start;
			$template->data['department'] = $department;
			$template->data['title'] = 'Todays Attendance Report';
			if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
				$template->data['base'] = HTTPS_SERVER;
			} else {
				$template->data['base'] = HTTP_SERVER;
			}
			$html = $template->fetch('report/today_html.tpl');
			//echo $html;exit;
			//$filename = "Todays_Attendance_".$department.".html";
			// header('Content-type: text/html');
			// header('Content-Disposition: attachment; filename='.$filename);
			// echo $html;
			$filename = "Todays_Attendance_".$department;
			header("Content-Type: application/vnd.ms-excel; charset=utf-8");
			header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false);
			echo $html;
			exit;		
		} else {
			$this->session->data['warning'] = 'No Data Found';
			$this->redirect($this->url->link('report/today', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}
	}
}
?>
