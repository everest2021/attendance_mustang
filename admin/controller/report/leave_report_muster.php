<?php
class ControllerReportLeavereportmuster extends Controller { 
	public function index() {  
		$this->language->load('report/leave_report_muster');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			//$filter_date_start = date('Y-m-d');
			//$from = date('Y-m-d');
			$filter_date_start = '';//date('Y-m-d', strtotime($from . "-1 day"));
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			//$filter_date_start = date('Y-m-d');
			//$from = date('Y-m-d');
			$filter_date_end = '';//date('Y-m-d', strtotime($from . "+29 day"));
		}

		if (isset($this->request->get['filter_year'])) {
			$filter_year = $this->request->get['filter_year'];
		} else {
			$filter_year = date('Y');
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = html_entity_decode($this->request->get['filter_name']);
		} else {
			$filter_name = "";
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = html_entity_decode($this->request->get['filter_name_id']);
		} else {
			$filter_name_id = "";
		}

		if (isset($this->request->get['filter_gender'])) {
			$filter_gender = html_entity_decode($this->request->get['filter_gender']);
		} else {
			$filter_gender = "";
		}

		if (isset($this->request->get['filter_unit'])) {
			$filter_unit = html_entity_decode($this->request->get['filter_unit']);
		} else {
			$filter_unit = "";
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_year'])) {
			$url .= '&filter_year=' . $this->request->get['filter_year'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_gender'])) {
			$url .= '&filter_gender=' . $this->request->get['filter_gender'];
		}

		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('report/leave_report_muster', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->load->model('report/leave_report_muster');
		$this->load->model('catalog/shift');

		$data['attendace'] = array();

		$data = array(
			//'filter_date_start'	     => $filter_date_start,
			//'filter_date_end'	     => $filter_date_end,
			'filter_name'	    	 => $filter_name,
			'filter_name_id'	     => $filter_name_id,
			'filter_gender'	     	 => $filter_gender,
			'filter_unit'	     	 => $filter_unit,
			'filter_year'	     	 => $filter_year,
			'start'                  => ($page - 1) * 7000,
			'limit'                  => 7000
		);
		
		$this->data['export'] = $this->url->link('report/leave_report_muster/export', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['dailyreports'] = array();
		
		if($filter_date_start != '' && $filter_date_end != ''){
			$day = array();
	        $days = $this->GetDays($filter_date_start, $filter_date_end);
	        foreach ($days as $dkey => $dvalue) {
	        	$dates = explode('-', $dvalue);
	        	$day[$dvalue]['day'] = $dates[2];
	        	$day[$dvalue]['date'] = $dvalue;
	        }	
		} else {
			$filter_date_start_mod = $filter_year.'-01-01';
			$filter_date_end_mod = $filter_year.'-12-31';
			$day = array();
	        $days = $this->GetDays($filter_date_start_mod, $filter_date_end_mod);
	        foreach ($days as $dkey => $dvalue) {
	        	$dates = explode('-', $dvalue);
	        	$day[$dvalue]['day'] = $dates[2];
	        	$day[$dvalue]['date'] = $dvalue;
	        }
		}

		$months = array(
        	'01' => '01',
        	'02' => '02',
        	'03' => '03',
        	'04' => '04',
        	'05' => '05',
        	'06' => '06',
        	'07' => '07',
        	'08' => '08',
        	'09' => '09',
        	'10' => '10',
        	'11' => '11',
        	'12' => '12',
        );

		// echo '<pre>';
		// print_r($months);
		// exit;	

		//if(isset($this->request->get['once']) && $this->request->get['once'] == '1'){
			$emp_datas = $this->model_report_leave_report_muster->getEmployees($data);
			foreach($emp_datas as $ekey => $evalue){
				$emp_code_start = substr($evalue['emp_code'], 0, 1);
				if($emp_code_start == '1'){
					$time_arr = array();
					$data['filter_name_id'] = $evalue['emp_code'];
					
					$total_basic_earned = 0;
					$total_days_present = 0;
					foreach($months as $mkey => $mvalue){
						$earned_basic = 0;
						$data['filter_month'] = $mvalue;
						$resultss = $this->model_report_leave_report_muster->getAttendance($data);

						$results = array();
						foreach($resultss as $rkey => $rvalue){
							$results['action'][$rvalue['date']] = $rvalue;
						}
						$filter_date_start_mod = $filter_year.'-'.$mvalue.'-01';

						$sal_month = date('n', strtotime($filter_date_start_mod));
						$sal_year = date('Y', strtotime($filter_date_start_mod));
						$total_days_month = cal_days_in_month(CAL_GREGORIAN, $sal_month, $sal_year);

						$filter_date_end_mod = $filter_year.'-'.$mvalue.'-'.$total_days_month;
						$day = array();
				        $days = $this->GetDays($filter_date_start_mod, $filter_date_end_mod);
				        foreach ($days as $dkey => $dvalue) {
				        	$dates = explode('-', $dvalue);
				        	$day[$dvalue]['day'] = $dates[2];
				        	$day[$dvalue]['date'] = $dvalue;
				        }

				        // echo '<pre>';
				        // print_r($day);
				        // exit;

						foreach ($day as $dkey => $dvalue) {
							foreach ($results as $pkey => $pvalue) {
								if(isset($pvalue[$dvalue['date']]['date'])){
								} else {
									$results['action'][$dvalue['date']] = array(
										'present_status' => '0',
										'absent_status' => '1',
										'holiday_id' => '0',
										'weekly_off' => '0',
										'leave_status' => '0',
										'date' => $dvalue['date'],
									); 
								}
							}
						}
						$days_present = 0;
						$days_absent = 0;
						$days_leave = 0;
						$cnt = 0;
						if(isset($results['action'])){
							foreach ($results['action'] as $result) {
								if($result['present_status'] == '1' || $result['holiday_id'] != '0' || $result['weekly_off'] != '0'){
									$days_present ++;
								} elseif($result['present_status'] == '0.5'){
									$days_present = $days_present + 0.5;
								}
								if($result['absent_status'] == '1'){
									$days_absent ++;
								} elseif($result['absent_status'] == '0.5'){
									$days_absent = $days_absent + 0.5;
								}
								if($result['leave_status'] == '1'){
									$days_leave ++;
								} elseif($result['leave_status'] == '0.5') {
									$days_leave = $days_leave + 1;
								}
							}
						}
						//$earned_basic = round(($evalue['basic'] / $total_days_month) * ($days_present + $days_leave));
						
						// echo $mvalue;
						// echo '<br />';
						// echo $days_present;
						// echo '<br />';
						// echo '<br />';
						
						//$total_basic_earned = $total_basic_earned + $earned_basic;
						$total_days_present = $total_days_present + $days_present;
					}
					
					// echo $total_days_present;
					// echo '<br />';
					// echo $total_basic_earned;
					// echo '<br />';
					//exit;

					$leave_amount = '0';//round(($total_basic_earned * 16.66) / 100);
					if($evalue['doj'] != '0000-00-00'){
						$doj = date('d-M-Y', strtotime($evalue['doj']));
					} else {
						$doj = '';
					}
					if($leave_amount > 0){
						$this->data['dailyreports'][] = array(
							'emp_name'    => $evalue['name'],
							'emp_code'   => substr($evalue['emp_code'], 1),
							'department'   => $evalue['department'],
							'designation'   => $evalue['designation'],
							'doj'   => $doj,
							'leave_amount'	=> $leave_amount
						);
					}
				}
			}
		//}

		$genders = array(
			'' => 'All',
			'1' => 'Male',
			'2' => 'Female',
		);
		$this->data['genders'] = $genders;

		$unit_data = array(
			'' => 'All',
			'Unit 31' => 'Unit 31',
			'Unit 64' => 'Unit 64',
		);
		$this->data['unit_data'] = $unit_data;

		$this->data['years'] = array();
		for($i = 2017; $i <= 2020; $i++){
			$this->data['years'][$i] = $i;
		}

		// echo '<pre>';
		// print_r($this->data['dailyreports']);
		// exit;
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_all_status'] = $this->language->get('text_all_status');

		
		$this->data['entry_date_start'] = $this->language->get('entry_date_start');
		$this->data['entry_date_end'] = $this->language->get('entry_date_end');
		
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['button_export'] = $this->language->get('button_export');

		$this->data['token'] = $this->session->data['token'];

		if(isset($this->data['warning'])){
			$this->data['error_warning'] = $this->data['warning'];
		} elseif(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_gender'])) {
			$url .= '&filter_gender=' . $this->request->get['filter_gender'];
		}

		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}

		if (isset($this->request->get['filter_year'])) {
			$url .= '&filter_year=' . $this->request->get['filter_year'];
		}		
		
		$this->data['token'] = $this->session->data['token'];

		$this->data['filter_date_start'] = $filter_date_start;
		$this->data['filter_date_end'] = $filter_date_end;
		$this->data['filter_name'] = $filter_name;
		$this->data['filter_name_id'] = $filter_name_id;
		$this->data['filter_gender'] = $filter_gender;
		$this->data['filter_unit'] = $filter_unit;
		$this->data['filter_year'] = $filter_year;
		
		$this->template = 'report/leave_report_muster.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	public function explode_time($time) { //explode time and convert into seconds
        $time = explode(':', $time);
        $time = $time[0] * 3600 + $time[1] * 60;
        return $time;
	}

	public function second_to_hhmm($time) { //convert seconds to hh:mm
        $hour = floor($time / 3600);
        $minute = strval(floor(($time % 3600) / 60));
        if ($minute == 0) {
            $minute = "00";
        } else {
            $minute = $minute;
        }
        $time = $hour . "." . $minute;
        return $time;
	}

	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	public function export(){
		$this->language->load('report/leave_report_muster');
		$this->load->model('report/common_report');
		$this->load->model('report/leave_report_muster');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			//$from = date('Y-m-d');
			$filter_date_start = '';//date('Y-m-d', strtotime($from . "-1 day"));
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			//$from = date('Y-m-d');
			$filter_date_end = '';//date('Y-m-d', strtotime($from . "+29 day"));
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = html_entity_decode($this->request->get['filter_name']);
		} else {
			$filter_name = "";
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = html_entity_decode($this->request->get['filter_name_id']);
		} else {
			$filter_name_id = "";
		}

		if (isset($this->request->get['filter_gender'])) {
			$filter_gender = html_entity_decode($this->request->get['filter_gender']);
		} else {
			$filter_gender = "";
		}

		if (isset($this->request->get['filter_unit'])) {
			$filter_unit = html_entity_decode($this->request->get['filter_unit']);
		} else {
			$filter_unit = "";
		}

		if (isset($this->request->get['filter_year'])) {
			$filter_year = html_entity_decode($this->request->get['filter_year']);
		} else {
			$filter_year = date('Y');
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_gender'])) {
			$url .= '&filter_gender=' . $this->request->get['filter_gender'];
		}

		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}

		if (isset($this->request->get['filter_year'])) {
			$url .= '&filter_year=' . $this->request->get['filter_year'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('report/dailyattendance', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->load->model('report/dailyattendance');

		$data['attendace'] = array();

		$data = array(
			//'filter_date_start'	     => $filter_date_start,
			//'filter_date_end'	     => $filter_date_end,
			'filter_name'	    	 => $filter_name,
			'filter_name_id'	     => $filter_name_id,
			'filter_gender'	     	 => $filter_gender,
			'filter_unit'	     	 => $filter_unit,
			'filter_year'	     	 => $filter_year,
		);

		if($filter_date_start != '' && $filter_date_end != ''){
			$day = array();
	        $days = $this->GetDays($filter_date_start, $filter_date_end);
	        foreach ($days as $dkey => $dvalue) {
	        	$dates = explode('-', $dvalue);
	        	$day[$dvalue]['day'] = $dates[2];
	        	$day[$dvalue]['date'] = $dvalue;
	        }	
		} else {
			$filter_date_start_mod = $filter_year.'-01-01';
			$filter_date_end_mod = $filter_year.'-12-31';
			$day = array();
	        $days = $this->GetDays($filter_date_start_mod, $filter_date_end_mod);
	        foreach ($days as $dkey => $dvalue) {
	        	$dates = explode('-', $dvalue);
	        	$day[$dvalue]['day'] = $dates[2];
	        	$day[$dvalue]['date'] = $dvalue;
	        }
		}

		$months = array(
        	'01' => '01',
        	'02' => '02',
        	'03' => '03',
        	'04' => '04',
        	'05' => '05',
        	'06' => '06',
        	'07' => '07',
        	'08' => '08',
        	'09' => '09',
        	'10' => '10',
        	'11' => '11',
        	'12' => '12',
        );

		$emp_datas = $this->model_report_leave_report_muster->getEmployees($data);
		$dailyreports = array();
		foreach($emp_datas as $ekey => $evalue){
			$emp_code_start = substr($evalue['emp_code'], 0, 1);
			if($emp_code_start == '1'){
				$time_arr = array();
				$data['filter_name_id'] = $evalue['emp_code'];
				$total_basic_earned = 0;
				$total_days_present = 0;
				foreach($months as $mkey => $mvalue){
					$earned_basic = 0;
					$data['filter_month'] = $mvalue;
					$resultss = $this->model_report_leave_report_muster->getAttendance($data);

					$results = array();
					foreach($resultss as $rkey => $rvalue){
						$results['action'][$rvalue['date']] = $rvalue;
					}
					$filter_date_start_mod = $filter_year.'-'.$mvalue.'-01';

					$sal_month = date('n', strtotime($filter_date_start_mod));
					$sal_year = date('Y', strtotime($filter_date_start_mod));
					$total_days_month = cal_days_in_month(CAL_GREGORIAN, $sal_month, $sal_year);

					$filter_date_end_mod = $filter_year.'-'.$mvalue.'-'.$total_days_month;
					$day = array();
			        $days = $this->GetDays($filter_date_start_mod, $filter_date_end_mod);
			        foreach ($days as $dkey => $dvalue) {
			        	$dates = explode('-', $dvalue);
			        	$day[$dvalue]['day'] = $dates[2];
			        	$day[$dvalue]['date'] = $dvalue;
			        }

			        // echo '<pre>';
			        // print_r($day);
			        // exit;

					foreach ($day as $dkey => $dvalue) {
						foreach ($results as $pkey => $pvalue) {
							if(isset($pvalue[$dvalue['date']]['date'])){
							} else {
								$results['action'][$dvalue['date']] = array(
									'present_status' => '0',
									'absent_status' => '1',
									'holiday_id' => '0',
									'weekly_off' => '0',
									'leave_status' => '0',
									'date' => $dvalue['date'],
								); 
							}
						}
					}
					$days_present = 0;
					$days_absent = 0;
					$days_leave = 0;
					$cnt = 0;
					if(isset($results['action'])){
						foreach ($results['action'] as $result) {
							if($result['present_status'] == '1' || $result['holiday_id'] != '0' || $result['weekly_off'] != '0'){
								$days_present ++;
							} elseif($result['present_status'] == '0.5'){
								$days_present = $days_present + 0.5;
							}
							if($result['absent_status'] == '1'){
								$days_absent ++;
							} elseif($result['absent_status'] == '0.5'){
								$days_absent = $days_absent + 0.5;
							}
							if($result['leave_status'] == '1'){
								$days_leave ++;
							} elseif($result['leave_status'] == '0.5') {
								$days_leave = $days_leave + 1;
							}
						}
					}
					//$earned_basic = round(($evalue['basic'] / $total_days_month) * ($days_present + $days_leave));
					
					// echo $mvalue;
					// echo '<br />';
					// echo $days_present;
					// echo '<br />';
					// echo '<br />';
					
					//$total_basic_earned = $total_basic_earned + $earned_basic;
					$total_days_present = $total_days_present + $days_present;
				}
				
				// echo $total_basic_earned;
				// echo '<br />';
				// exit;

				$leave_amount = '0';//round(($total_basic_earned * 16.66) / 100);
				if($evalue['doj'] != '0000-00-00'){
					$doj = date('d-M-Y', strtotime($evalue['doj']));
				} else {
					$doj = '';
				}
				if($leave_amount > 0){
					$dailyreports[] = array(
						'emp_name'    => $evalue['name'],
						'emp_code'   => substr($evalue['emp_code'], 1),
						'department'   => $evalue['department'],
						'designation'   => $evalue['designation'],
						'doj'   => $doj,
						'leave_amount'	=> $leave_amount
					);
				}
			}
		}
		
		if($dailyreports){
			//$month = date("F", mktime(0, 0, 0, $filter_month, 10));
			$statuss = 'Leave Report Muster';
			$statusss = 'Leave Report Muster';
			$template = new Template();		
			$template->data['dailyreports'] = $dailyreports;
			$template->data['title'] = $statuss;
			if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
				$template->data['base'] = HTTPS_SERVER;
			} else {
				$template->data['base'] = HTTP_SERVER;
			}
			$html = $template->fetch('report/leave_report_muster_html.tpl');
			//echo $html;exit;
			// header('Content-type: text/html');
			// header('Content-Disposition: attachment; filename='.$filename);
			//echo $html;exit;
			$filename = "Leave Report Muster".$filter_year;
			//echo $filename;exit();
			header("Content-Type: application/vnd.ms-excel; charset=utf-8");
			header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false);
			echo $html;
			exit;		
		} else {
			$this->session->data['warning'] = 'No Data Found';
			$this->redirect($this->url->link('report/leave_report_muster', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/employee');

			if(isset($this->session->data['dept_names'])){
				$filter_departments = html_entity_decode($this->session->data['dept_names']);
				$filter_departments = "'" . str_replace(",", "','", html_entity_decode($filter_departments)) . "'";
			} else {
				$filter_departments = '';
			}

			if($filter_departments == ''){
				if(isset($this->session->data['dept_name'])){
					$filter_department = $this->session->data['dept_name'];
				} else {
					$filter_department = '';
				}
			} else {
				$filter_department = '';
			}

			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				'filter_department' => $filter_department,
				'filter_departments' => $filter_departments,
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_employee->getemployees($data);

			foreach ($results as $result) {
				$json[] = array(
					'employee_id' => $result['employee_id'],
					'emp_code' => $result['emp_code'], 
					'name'            => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}
}
?>
