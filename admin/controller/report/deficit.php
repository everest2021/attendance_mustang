<?php
class ControllerReportDeficit extends Controller { 
	public function index() {  
		$this->language->load('report/deficit');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			//$filter_date_start = date('Y-m-d');
			//$filter_date_start = date('Y-m-d', strtotime(date('Y') . '-' . date('m') . '-01'));
			//$filter_date_start = '';
			$from = date('Y-m-d');
			$filter_date_start = date('Y-m-d', strtotime($from . "-30 day"));
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = date('Y-m-d');
		}

		if (isset($this->request->get['filter_month'])) {
			$filter_month = $this->request->get['filter_month'];
		} else {
			$filter_month = date('n');
		}

		if (isset($this->request->get['filter_year'])) {
			$filter_year = $this->request->get['filter_year'];
		} else {
			$filter_year = date('Y');
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['filter_type'])) {
			$filter_type = $this->request->get['filter_type'];
		} else {
			$filter_type = 1;
		}

		if (isset($this->request->get['unit'])) {
			$unit = $this->request->get['unit'];
		} else {
			$unit = 0;
		}

		if (isset($this->request->get['department'])) {
			$department = html_entity_decode($this->request->get['department']);
		} else {
			$department = 0;
		}

		if (isset($this->request->get['group'])) {
			$group = $this->request->get['group'];
		} else {
			$group = 0;
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['filter_month'])) {
			$url .= '&filter_month=' . $this->request->get['filter_month'];
		}

		if (isset($this->request->get['filter_year'])) {
			$url .= '&filter_year=' . $this->request->get['filter_year'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_type'])) {
			$url .= '&filter_type=' . $this->request->get['filter_type'];
		}

		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}
		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('report/deficit', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->load->model('report/common_report');
		$this->load->model('report/attendance');
		$this->load->model('catalog/employee');
		$this->load->model('transaction/transaction');

		$this->data['attendace'] = array();

		$data = array(
			'filter_date_start'	     => $filter_date_start,
			'filter_date_end'	     => $filter_date_end,
			'filter_month'	     	 => $filter_month,
			'filter_year'	     	 => $filter_year,
			'filter_name'	     	 => $filter_name,
			'filter_name_id'	     => $filter_name_id,
			'unit'					 => $unit,
			'department'			 => $department,
			'group'					 => $group, 
			'start'                  => ($page - 1) * 7000,
			'limit'                  => 7000
		);

		//$start_time = strtotime($filter_date_start);
		//$compare_time = strtotime(date('Y-m-d'));
		//$day = array();
  		//$days = $this->GetDays($filter_date_start, $filter_date_end);
  		//foreach ($days as $dkey => $dvalue) {
  			//$dates = explode('-', $dvalue);
  			//$day[$dkey]['day'] = $dates[2];
  			//$day[$dkey]['date'] = $dvalue;
  		//}
		//$this->data['days'] = $day;
		//foreach ($day as $dkey => $dvalue) {
        	//$results = $this->model_report_common_report->getAttendance($data);
        //}

        $final_datas = array();
        if(isset($this->request->get['once']) && $this->request->get['once'] == 1){
	        $results = $this->model_report_common_report->getemployees($data);
	        $data['filter_date_start'] = '';
	        $data['filter_date_end'] = '';
	        foreach ($results as $rkey => $rvalue) {
				$performance_data = array();
				$transaction_datas = $this->model_report_common_report->gettransaction_data($rvalue['emp_code'], $data);
				$final_datas[$rvalue['emp_code']]['name'] = $rvalue['name'];
				$final_datas[$rvalue['emp_code']]['department'] = $rvalue['department'];
				$final_datas[$rvalue['emp_code']]['emp_code'] = $rvalue['emp_code'];
				$final_datas[$rvalue['emp_code']]['designation'] = $rvalue['designation'];
				$final_datas[$rvalue['emp_code']]['department'] = $rvalue['department'];
				$final_datas[$rvalue['emp_code']]['company'] = 'RWITC';
				$transaction_inn = '0';
				$onehourcnt = 0;
				$twohourcnt = 0;
				$threehourcnt = 0;
				$fourhourcnt = 0;
				$fivehourcnt = 0;
				$greatfivehourcnt = 0;
				$totalcnt = 0;
				
				foreach($transaction_datas as $tkey => $tvalue){
					$shift_inn = '';
					$transaction_inn = '1';
					if($tvalue['shift_intime'] != '00:00:00' && $tvalue['shift_outtime'] != '00:00:00'){
						$shift_intime = $tvalue['shift_intime'];
						$shift_outtime = $tvalue['shift_outtime'];
						$shift_inn = '1';
					} 
					$late_time_stat = '0';
					

					if($shift_inn == '1' && $shift_intime != '00:00:00' && $tvalue['act_intime'] != '00:00:00'){
						$start_date2 = date('Y-m-d h:i:s a', strtotime($tvalue['date'].' '.$tvalue['act_intime']) );
						$start_date22 = date('Y-m-d h:i:s a', strtotime($tvalue['date'].' '.$shift_intime) );
						$start_date = new DateTime($start_date2);
						$since_start = $start_date->diff(new DateTime($start_date22));
						
						$leave_data_sql = "SELECT `leave_type`, `type` FROM `oc_leave_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$tvalue['date']."' AND `p_status` = '1' AND `a_status` = '1' ";
						$leave_data_res = $this->db->query($leave_data_sql);
						
						//created the "$new_val" as $since_start->h was giving problem in reassigning.

						$new_val = $since_start->h;
						$new_invert = $since_start->invert;
						if($leave_data_res->num_rows > 0){
							if($leave_data_res->row['type'] == '1'){
								$new_val = $new_val - 4;
							} elseif($leave_data_res->row['type'] == '2'){
								$new_val = $new_val - 4;
							} elseif($leave_data_res->row['type'] == 'F'){
								$new_val = $new_val - 8;
								$new_invert = 0;
							} 
						}

						//echo '<pre>';
						//print_r($tvalue);
						//echo '<pre>';
						//print_r($since_start);
						
						if($new_invert == 1){
							if(($since_start->i > 0 || $since_start->s > 0) && $new_val == 0){
								$onehourcnt ++;
							} elseif($new_val == 1){
								$twohourcnt ++;
							} elseif($new_val == 2){
								$threehourcnt ++;
							} elseif($new_val == 3){
								$fourhourcnt ++;
							} elseif($new_val == 4){
								$fivehourcnt ++;
							} elseif($new_val > 5){
								$greatfivehourcnt ++;
							}
							$late_time_stat = 1;
						}
					} elseif($tvalue['act_intime'] != '00:00:00' && $tvalue['secondhalf_status'] == 'HD'){
						
						$update3 = "SELECT  * FROM `oc_shift_schedule` WHERE `emp_code` = '".$rvalue['emp_code']."' ";
						$shift_schedules = $this->db->query($update3)->row;
						$shift_sel = 'S_1';
						foreach($shift_schedules as $sckey => $scvalue){
							$exp = explode('_', $scvalue);
							if($exp[0] == 'S'){
								$shift_sel = $scvalue;
							}
						}
						$schedule_raw = explode('_', $shift_sel);
						$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[1]);
						if(!isset($shift_data['shift_id'])){
							$shift_data = $this->model_transaction_transaction->getshiftdata('1');
						}
						$shift_intime = $shift_data['in_time'];

						$start_date2 = date('Y-m-d h:i:s a', strtotime($tvalue['date'].' '.$tvalue['act_intime']) );
						$start_date22 = date('Y-m-d h:i:s a', strtotime($tvalue['date'].' '.$shift_intime) );
						$start_date = new DateTime($start_date2);
						$since_start = $start_date->diff(new DateTime($start_date22));
						
						$leave_data_sql = "SELECT `leave_type`, `type` FROM `oc_leave_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$tvalue['date']."' AND `p_status` = '1' AND `a_status` = '1' ";
						$leave_data_res = $this->db->query($leave_data_sql);
						
						//created the "$new_val" as $since_start->h was giving problem in reassigning.
						$new_val = $since_start->h;
						$new_invert = $since_start->invert;
						if($leave_data_res->num_rows > 0){
							if($leave_data_res->row['type'] == '1'){
								$new_val = $new_val - 4;
							} elseif($leave_data_res->row['type'] == '2'){
								$new_val = $new_val - 4;
							} elseif($leave_data_res->row['type'] == 'F'){
								$new_val = $new_val - 8;
								$new_invert = 0;
							}
						}
						//echo '<pre>';
						//print_r($tvalue);
						//echo '<pre>';
						//print_r($since_start);
						
						
						if($new_invert == 1){
							if(($since_start->i > 0 || $since_start->s > 0) && $new_val == 0){
								$onehourcnt ++;
							} elseif($new_val == 1){
								$twohourcnt ++;
							} elseif($new_val == 2){
								$threehourcnt ++;
							} elseif($new_val == 3){
								$fourhourcnt ++;
							} elseif($new_val == 4){
								$fivehourcnt ++;
							} elseif($new_val > 5){
								$greatfivehourcnt ++;
							}
							$late_time_stat = 1;
						}
					}
				} 
				//exit;
				$totalcnt = $onehourcnt + $twohourcnt + $threehourcnt + $fourhourcnt + $fivehourcnt + $greatfivehourcnt;
				//$totalcnt = 6;
				$leave = 0;
				if($totalcnt >= 6 && $totalcnt <= 8){
					$leave = 0.5;
				} elseif($totalcnt >= 9 && $totalcnt <= 11){
					$leave = 1;
				} elseif($totalcnt >= 12 && $totalcnt <= 14){
					$leave = 1.5;
				} elseif($totalcnt >= 15 && $totalcnt <= 17){
					$leave = 2;
				} elseif($totalcnt >= 18 && $totalcnt <= 20){
					$leave = 2.5;
				} elseif ($totalcnt >= 21 && $totalcnt <= 23) {
					$leave = 3;
				} elseif($totalcnt >= 24 && $totalcnt <= 26){
					$leave = 3.5;
				} elseif($totalcnt >= 27 && $totalcnt <= 29){
					$leave = 4;
				} elseif($totalcnt >= 30){
					$leave = 4.5;
				}
				// echo $onehourcnt;
				// echo '<br />';
				// echo $twohourcnt;
				// echo '<br />';
				// echo $threehourcnt;
				// echo '<br />';
				// echo $fourhourcnt;
				// echo '<br />';
				// echo $fivehourcnt;
				// echo '<br />';
				// echo $greatfivehourcnt;
				// echo '<br />';
				// echo $totalcnt;
				// echo '<br />';
				// echo $leave;
				// echo '<br />';
				$final_datas[$rvalue['emp_code']]['onehourcnt'] = $onehourcnt;
				$final_datas[$rvalue['emp_code']]['twohourcnt'] = $twohourcnt;
				$final_datas[$rvalue['emp_code']]['threehourcnt'] = $threehourcnt;
				$final_datas[$rvalue['emp_code']]['fourhourcnt'] = $fourhourcnt;
				$final_datas[$rvalue['emp_code']]['fivehourcnt'] = $fivehourcnt;
				$final_datas[$rvalue['emp_code']]['greatfivehourcnt'] = $greatfivehourcnt;
				$final_datas[$rvalue['emp_code']]['totalcnt'] = $totalcnt;
				$final_datas[$rvalue['emp_code']]['leave'] = $leave;
				$final_datas[$rvalue['emp_code']]['status'] = 'Active';

				if($totalcnt == 0){
					unset($final_datas[$rvalue['emp_code']]);
				}
				// echo 'out';
				// echo '<br />';
				// exit;
			}
		}
		$this->data['final_datas'] = $final_datas;
		// echo '<pre>';
  		//print_r($this->data['final_datas']);
  		//exit;

		//$results = $this->model_report_common_report->getAttendance($data);
		
		$unit_data = array(
			'0' => 'All',
			'Mumbai' => 'Mumbai',
			'Pune' => 'Pune',
			'Delhi' => 'Delhi',
			'Chennai' => 'Chennai',
			'Bangalore' => 'Bangalore',
			'Ahmedabad' => 'Ahmedabad', 
		);

		$this->data['unit_data'] = $unit_data;
		
		$department_datas = $this->model_report_attendance->getdepartment_list();
		$department_data = array();
		$department_data['0'] = 'All';
		foreach ($department_datas as $dkey => $dvalue) {
			$department_data[$dvalue['department']] = $dvalue['department'];
		}
		$this->data['department_data'] = $department_data;
		
		$group_datas = $this->model_report_attendance->getgroup_list();
		$group_data = array();
		$group_data['0'] = 'All';
		$group_data['1'] = 'All WITHOUT OFFICIALS';
		foreach ($group_datas as $gkey => $gvalue) {
			$group_data[$gvalue['group']] = $gvalue['group'];
		}
		$this->data['group_data'] = $group_data;
		
		if(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_all_status'] = $this->language->get('text_all_status');

		
		$this->data['entry_date_start'] = $this->language->get('entry_date_start');
		$this->data['entry_date_end'] = $this->language->get('entry_date_end');
		
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['button_export'] = $this->language->get('button_export');

		$this->data['token'] = $this->session->data['token'];

		if(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		if (isset($this->request->get['filter_month'])) {
			$url .= '&filter_month=' . $this->request->get['filter_month'];
		}
		if (isset($this->request->get['filter_year'])) {
			$url .= '&filter_year=' . $this->request->get['filter_year'];
		}
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}
		if (isset($this->request->get['filter_type'])) {
			$url .= '&filter_type=' . $this->request->get['filter_type'];
		}
		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}
		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}

		$months = array(
			'1' => 'January',
			'2' => 'Feburary',
			'3' => 'March',
			'4' => 'April',
			'5' => 'May',
			'6' => 'June',
			'7' => 'July',
			'8' => 'August',
			'9' => 'September',
			'10' => 'October',
			'11' => 'November',
			'12' => 'December'
		);

		$this->data['months'] = $months;

		$this->data['filter_date_start'] = $filter_date_start;
		$this->data['filter_date_end'] = $filter_date_end;
		$this->data['filter_month'] = $filter_month;
		$this->data['filter_year'] = $filter_year;
		$this->data['filter_name'] = $filter_name;
		$this->data['filter_name_id'] = $filter_name_id;
		$this->data['filter_type'] = $filter_type;
		$this->data['unit'] = $unit;
		$this->data['department'] = $department;
		$this->data['group'] = $group;

		$this->template = 'report/deficit.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	function array_sort($array, $on, $order=SORT_ASC){

	    $new_array = array();
	    $sortable_array = array();

	    if (count($array) > 0) {
	        foreach ($array as $k => $v) {
	            if (is_array($v)) {
	                foreach ($v as $k2 => $v2) {
	                    if ($k2 == $on) {
	                        $sortable_array[$k] = $v2;
	                    }
	                }
	            } else {
	                $sortable_array[$k] = $v;
	            }
	        }

	        switch ($order) {
	            case SORT_ASC:
	                asort($sortable_array);
	                break;
	            case SORT_DESC:
	                arsort($sortable_array);
	                break;
	        }

	        foreach ($sortable_array as $k => $v) {
	            $new_array[$k] = $array[$k];
	        }
	    }

	    return $new_array;
	}

	public function export(){
		$this->language->load('report/deficit');
		$this->load->model('transaction/transaction');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			$filter_date_start = '';
			//$filter_date_start = date('Y-m-d', strtotime(date('Y') . '-' . date('m') . '-01'));
			//$from = date('Y-m-d');
			//$filter_date_start = date('Y-m-d', strtotime($from . "-1 day"));
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = '';
		}

		if (isset($this->request->get['filter_month'])) {
			$filter_month = $this->request->get['filter_month'];
		} else {
			$filter_month = date('n');
		}

		if (isset($this->request->get['filter_year'])) {
			$filter_year = $this->request->get['filter_year'];
		} else {
			$filter_year = date('Y');
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['filter_type'])) {
			$filter_type = $this->request->get['filter_type'];
		} else {
			$filter_type = 1;
		}

		if (isset($this->request->get['unit'])) {
			$unit = $this->request->get['unit'];
		} else {
			$unit = 0;
		}

		if (isset($this->request->get['department'])) {
			$department = html_entity_decode($this->request->get['department']);
		} else {
			$department = 0;
		}

		if (isset($this->request->get['group'])) {
			$group = $this->request->get['group'];
		} else {
			$group = 0;
		}

		$this->load->model('report/common_report');
		$this->load->model('report/attendance');
		$this->load->model('catalog/employee');

		$this->data['attendace'] = array();

		$data = array(
			'filter_date_start'	     => $filter_date_start,
			'filter_date_end'	     => $filter_date_end,
			'filter_month'	     	 => $filter_month,
			'filter_year'	     	 => $filter_year,
			'filter_name'	     	 => $filter_name,
			'filter_name_id'	     => $filter_name_id,
			'unit'					 => $unit,
			'department'			 => $department,
			'group'					 => $group 
		);

		//$start_time = strtotime($filter_date_start);
		//$compare_time = strtotime(date('Y-m-d'));

		// $day = array();
  //       $days = $this->GetDays($filter_date_start, $filter_date_end);
  //       foreach ($days as $dkey => $dvalue) {
  //       	$dates = explode('-', $dvalue);
  //       	$day[$dkey]['day'] = $dates[2];
  //       	$day[$dkey]['date'] = $dvalue;
  //       }

  //       $this->data['days'] = $day;

        //foreach ($day as $dkey => $dvalue) {
        	//$results = $this->model_report_common_report->getAttendance($data);
        //}
        $final_datas = array();
		$results = $this->model_report_common_report->getemployees($data);
		$data['filter_date_start'] = '';
        $data['filter_date_end'] = '';
        foreach ($results as $rkey => $rvalue) {
        	
        	// echo '<pre>';
        	// print_r($rvalue);
        	

			$performance_data = array();
			$transaction_datas = $this->model_report_common_report->gettransaction_data($rvalue['emp_code'], $data);
			$final_datas[$rvalue['emp_code']]['name'] = $rvalue['name'];
			$final_datas[$rvalue['emp_code']]['department'] = $rvalue['department'];
			$final_datas[$rvalue['emp_code']]['emp_code'] = $rvalue['emp_code'];
			$final_datas[$rvalue['emp_code']]['designation'] = $rvalue['designation'];
			$final_datas[$rvalue['emp_code']]['department'] = $rvalue['department'];
			$final_datas[$rvalue['emp_code']]['company'] = 'RWITC';
			$transaction_inn = '0';
			$onehourcnt = 0;
			$twohourcnt = 0;
			$threehourcnt = 0;
			$fourhourcnt = 0;
			$fivehourcnt = 0;
			$greatfivehourcnt = 0;
			$totalcnt = 0;
			
			// echo '<pre>';
			// print_r($transaction_datas);
			// exit;
			
			foreach($transaction_datas as $tkey => $tvalue){
				$shift_inn = '';
				$transaction_inn = '1';
				if($tvalue['shift_intime'] != '00:00:00' && $tvalue['shift_outtime'] != '00:00:00'){
					$shift_intime = $tvalue['shift_intime'];
					$shift_outtime = $tvalue['shift_outtime'];
					$shift_inn = '1';
				} 
				$late_time_stat = '0';
				if($shift_inn == '1' && $shift_intime != '00:00:00' && $tvalue['act_intime'] != '00:00:00'){
					$start_date2 = date('Y-m-d h:i:s a', strtotime($tvalue['date'].' '.$tvalue['act_intime']) );
					$start_date22 = date('Y-m-d h:i:s a', strtotime($tvalue['date'].' '.$shift_intime) );
					$start_date = new DateTime($start_date2);
					$since_start = $start_date->diff(new DateTime($start_date22));
					
					// echo '<pre>';
					// print_r($since_start);

					$leave_data_sql = "SELECT `leave_type`, `type` FROM `oc_leave_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$tvalue['date']."' AND `p_status` = '1' AND `a_status` = '1' ";
					$leave_data_res = $this->db->query($leave_data_sql);
					//created the "$new_val" as $since_start->h was giving problem in reassigning.

					$new_val = $since_start->h;
					$new_invert = $since_start->invert;
					if($leave_data_res->num_rows > 0){
						if($leave_data_res->row['type'] == '1'){
							$new_val = $new_val - 4;
						} elseif($leave_data_res->row['type'] == '2'){
							$new_val = $new_val - 4;
						} elseif($leave_data_res->row['type'] == 'F'){
							$new_val = $new_val - 8;
							$new_invert = 0;
						}
					}
					
					if($new_invert == 1){
						if(($since_start->i > 0 || $since_start->s > 0) && $new_val == 0){
							$onehourcnt ++;
						} elseif($new_val == 1){
							$twohourcnt ++;
						} elseif($new_val == 2){
							$threehourcnt ++;
						} elseif($new_val == 3){
							$fourhourcnt ++;
						} elseif($new_val == 4){
							$fivehourcnt ++;
						} elseif($new_val > 5){
							$greatfivehourcnt ++;
						}
						$late_time_stat = 1;
					}
				}  elseif($tvalue['act_intime'] != '00:00:00' && $tvalue['secondhalf_status'] == 'HD'){
					$update3 = "SELECT  * FROM `oc_shift_schedule` WHERE `emp_code` = '".$rvalue['emp_code']."' ";
					$shift_schedules = $this->db->query($update3)->row;
					$shift_sel = 'S_1';
					foreach($shift_schedules as $sckey => $scvalue){
						$exp = explode('_', $scvalue);
						if($exp[0] == 'S'){
							$shift_sel = $scvalue;
						}
					}
					$schedule_raw = explode('_', $shift_sel);
					$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[1]);
					if(!isset($shift_data['shift_id'])){
						$shift_data = $this->model_transaction_transaction->getshiftdata('1');
					}
					$shift_intime = $shift_data['in_time'];

					$start_date2 = date('Y-m-d h:i:s a', strtotime($tvalue['date'].' '.$tvalue['act_intime']) );
					$start_date22 = date('Y-m-d h:i:s a', strtotime($tvalue['date'].' '.$shift_intime) );
					$start_date = new DateTime($start_date2);
					$since_start = $start_date->diff(new DateTime($start_date22));
					
					$leave_data_sql = "SELECT `leave_type`, `type` FROM `oc_leave_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$tvalue['date']."' AND `p_status` = '1' AND `a_status` = '1' ";
					$leave_data_res = $this->db->query($leave_data_sql);
					//created the "$new_val" as $since_start->h was giving problem in reassigning.

					$new_val = $since_start->h;
					$new_invert = $since_start->invert;
					if($leave_data_res->num_rows > 0){
						if($leave_data_res->row['type'] == '1'){
							$new_val = $new_val - 4;
						} elseif($leave_data_res->row['type'] == '2'){
							$new_val = $new_val - 4;
						} elseif($leave_data_res->row['type'] == 'F'){
							$new_val = $new_val - 8;
							$new_invert = 0;
						}
					}
					
					if($new_invert == 1){
						if(($since_start->i > 0 || $since_start->s > 0) && $new_val == 0){
							$onehourcnt ++;
						} elseif($new_val == 1){
							$twohourcnt ++;
						} elseif($new_val == 2){
							$threehourcnt ++;
						} elseif($new_val == 3){
							$fourhourcnt ++;
						} elseif($new_val == 4){
							$fivehourcnt ++;
						} elseif($new_val > 5){
							$greatfivehourcnt ++;
						}
						$late_time_stat = 1;
					}
				}
			}
			$totalcnt = $onehourcnt + $twohourcnt + $threehourcnt + $fourhourcnt + $fivehourcnt + $greatfivehourcnt;
			//$totalcnt = 6;
			$leave = 0;
			if($totalcnt >= 6 && $totalcnt <= 8){
				$leave = 0.5;
			} elseif($totalcnt >= 9 && $totalcnt <= 11){
				$leave = 1;
			} elseif($totalcnt >= 12 && $totalcnt <= 14){
				$leave = 1.5;
			} elseif($totalcnt >= 15 && $totalcnt <= 17){
				$leave = 2;
			} elseif($totalcnt >= 18 && $totalcnt <= 20){
				$leave = 2.5;
			} elseif ($totalcnt >= 21 && $totalcnt <= 23) {
				$leave = 3;
			} elseif($totalcnt >= 24 && $totalcnt <= 26){
				$leave = 3.5;
			} elseif($totalcnt >= 27 && $totalcnt <= 29){
				$leave = 4;
			} elseif($totalcnt >= 30){
				$leave = 4.5;
			}
			// echo $onehourcnt;
			// echo '<br />';
			// echo $twohourcnt;
			// echo '<br />';
			// echo $threehourcnt;
			// echo '<br />';
			// echo $fourhourcnt;
			// echo '<br />';
			// echo $fivehourcnt;
			// echo '<br />';
			// echo $greatfivehourcnt;
			// echo '<br />';
			// echo $totalcnt;
			// echo '<br />';
			// echo $leave;
			// echo '<br />';
			$final_datas[$rvalue['emp_code']]['onehourcnt'] = $onehourcnt;
			$final_datas[$rvalue['emp_code']]['twohourcnt'] = $twohourcnt;
			$final_datas[$rvalue['emp_code']]['threehourcnt'] = $threehourcnt;
			$final_datas[$rvalue['emp_code']]['fourhourcnt'] = $fourhourcnt;
			$final_datas[$rvalue['emp_code']]['fivehourcnt'] = $fivehourcnt;
			$final_datas[$rvalue['emp_code']]['greatfivehourcnt'] = $greatfivehourcnt;
			$final_datas[$rvalue['emp_code']]['totalcnt'] = $totalcnt;
			$final_datas[$rvalue['emp_code']]['leave'] = $leave;
			$final_datas[$rvalue['emp_code']]['status'] = 'Active';

			if($totalcnt == 0){
				unset($final_datas[$rvalue['emp_code']]);
			}
			// echo 'out';
			// echo '<br />';
			// exit;
		}
		

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		if (isset($this->request->get['filter_month'])) {
			$url .= '&filter_month=' . $this->request->get['filter_month'];
		}
		if (isset($this->request->get['filter_year'])) {
			$url .= '&filter_year=' . $this->request->get['filter_year'];
		}
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}
		if (isset($this->request->get['filter_type'])) {
			$url .= '&filter_type=' . $this->request->get['filter_type'];
		}
		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}
		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}
		$url .= '&once=1';

		$final_datas = array_chunk($final_datas, 15);

		// echo '<pre>';
		// print_r($final_datas);
		// exit;
		
		if($final_datas){
			//$month = date("F", mktime(0, 0, 0, $filter_month, 10));
			$tdays = cal_days_in_month(CAL_GREGORIAN, $filter_month, $filter_year);
			$template = new Template();		
			$template->data['final_datass'] = $final_datas;
			$template->data['date_start'] = '01-'.$filter_month.'-'.$filter_year;
			$template->data['date_end'] = $tdays.'-'.$filter_month.'-'.$filter_year;
			$template->data['title'] = 'Deficit Report';
			if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
				$template->data['base'] = HTTPS_SERVER;
			} else {
				$template->data['base'] = HTTP_SERVER;
			}
			$html = $template->fetch('report/deficit_html.tpl');
			//echo $html;exit;
			$filename = "Deficit_Report.html";
			header('Content-type: text/html');
			header('Content-Disposition: attachment; filename='.$filename);
			echo $html;
			// header("Content-Type: application/vnd.ms-excel; charset=utf-8");
			// header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
			// header("Expires: 0");
			// header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			// header("Cache-Control: private",false);
			// echo $html;
			exit;		
		} else {
			$this->session->data['warning'] = 'No Data Found';
			$this->redirect($this->url->link('report/deficit', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}
	}
}
?>