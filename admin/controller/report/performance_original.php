<?php
class ControllerReportPerformance extends Controller { 
	public function index() {  

		if(isset($this->session->data['employee_id'])){
			$emp_code = $this->session->data['employee'];
			$is_set = $this->db->query("SELECT `is_set` FROM `oc_employee` WHERE `employee_id` = '".$emp_code."' ")->row['is_set'];	
			if($is_set == '1'){
				//$this->redirect($this->url->link('transaction/leave_ess', 'token=' . $this->session->data['token'], 'SSL'));
			} else {
				$this->redirect($this->url->link('user/password_change', 'token=' . $this->session->data['token'].'&employee_id='.$emp_code, 'SSL'));
			}
		} elseif(isset($this->session->data['is_dept'])){
			$emp_code = $this->session->data['d_emp_id'];
			$is_set = $this->db->query("SELECT `is_set` FROM `oc_employee` WHERE `employee_id` = '".$emp_code."' ")->row['is_set'];	
			if($is_set == '1'){
				//$this->redirect($this->url->link('transaction/leave_ess', 'token=' . $this->session->data['token'], 'SSL'));
			} else {
				$this->redirect($this->url->link('user/password_change', 'token=' . $this->session->data['token'].'&employee_id='.$emp_code, 'SSL'));
			}
		} elseif(isset($this->session->data['is_super'])){
			$emp_code = $this->session->data['d_emp_id'];
			$is_set = $this->db->query("SELECT `is_set` FROM `oc_employee` WHERE `employee_id` = '".$emp_code."' ")->row['is_set'];	
			if($is_set == '1'){
				//$this->redirect($this->url->link('transaction/leave_ess', 'token=' . $this->session->data['token'], 'SSL'));
			} else {
				$this->redirect($this->url->link('user/password_change', 'token=' . $this->session->data['token'].'&employee_id='.$emp_code, 'SSL'));
			}
		}

		$this->language->load('report/performance');
		$this->load->model('report/common_report');
		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			$from = date('Y-m-d');
			$filter_date_start = date('Y-m-d', strtotime($from . "-30 day"));
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} elseif(isset($this->session->data['employee_id'])){
			$emp_name = $this->db->query("SELECT `name` FROM `oc_employee` WHERE `employee_id` = '".$this->session->data['employee_id']."' ")->row['name'];
			$filter_name = $emp_name;
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} elseif(isset($this->session->data['employee_id'])){
			$filter_name_id = $this->session->data['employee_id'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['filter_unit'])) {
			$filter_unit = $this->request->get['filter_unit'];
		} else {
			$filter_unit = '';
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
			//$filter_date_end1 = $this->model_report_common_report->getfilter_date_end($filter_date_start, $filter_name_id, $filter_unit);
			//if(strtotime($filter_date_end) > strtotime($filter_date_end1) ){
				//$filter_date_end = $filter_date_end1;
			//}
		} else {
			$filter_date_end = date('Y-m-d');
		}
		
		if (isset($this->request->get['filter_type'])) {
			$filter_type = $this->request->get['filter_type'];
		} else {
			$filter_type = 1;
		}

		if (isset($this->request->get['filter_designation'])) {
			$filter_designation = $this->request->get['filter_designation'];
		} else {
			$filter_designation = '';
		}

		if (isset($this->request->get['filter_department'])) {
			$filter_department = $this->request->get['filter_department'];
		} else {
			$filter_department = '';
		}

		//echo "<pre>";print_r($filter_department);exit;
		if(isset($this->session->data['dept_names'])){
			$filter_departments = html_entity_decode($this->session->data['dept_names']);
			$filter_departments = "'" . str_replace(",", "','", html_entity_decode($filter_departments)) . "'";
		} else {
			$filter_departments = '';
		}

		if (isset($this->request->get['group'])) {
			$group = $this->request->get['group'];
		} else {
			$group = '0';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_type'])) {
			$url .= '&filter_type=' . $this->request->get['filter_type'];
		}

		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}
		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}
		
		if (isset($this->request->get['filter_designation'])) {
			$url .= '&filter_designation=' . $this->request->get['filter_designation'];
		}
		
		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('report/performance', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);
	$this->data['export'] = $this->url->link('report/performance/export', 'token=' . $this->session->data['token'] . $url, 'SSL');


		$this->load->model('report/attendance');
		$this->load->model('transaction/transaction');
		$this->load->model('catalog/employee');

		$this->data['attendace'] = array();

		$data = array(
			'filter_date_start'	     => $filter_date_start,
			'filter_date_end'	     => $filter_date_end,
			'filter_name'	     	 => $filter_name,
			'filter_name_id'	     => $filter_name_id,
			'filter_unit'			=> $filter_unit,
			'filter_designation'	=>$filter_designation,
			'filter_department'		=> $filter_department,
			'filter_departments'	 => $filter_departments,
			'group'					 => $group,
			'day_close'              => 1,
			'start'                  => ($page - 1) * 7000,
			'limit'                  => 7000
		);

		$day = array();
		$days = $this->GetDays($filter_date_start, $filter_date_end);
        foreach ($days as $dkey => $dvalue) {
        	$dates = explode('-', $dvalue);
        	$day[$dkey]['day'] = $dates[2];
        	$day[$dkey]['date'] = $dvalue;
        }
        $this->data['days'] = $day;

        $final_datas = array();
        if(isset($this->request->get['once']) && $this->request->get['once'] == 1){
	        $results1 = $this->model_report_common_report->getemployees($data);
	        $results2 = array();//$this->model_report_common_report->getemployees_reporting($data);
	        $results = array_merge($results1, $results2);
	        foreach ($results as $rkey => $rvalue) {
				$performance_data = array();
				$transaction_datas = $this->model_report_common_report->gettransaction_data($rvalue['employee_id'], $data);
				//echo "<pre>";print_r($transaction_datas);exit;
				$final_datas[$rvalue['employee_id']]['basic_data']['name'] = $rvalue['name'];
				$final_datas[$rvalue['employee_id']]['basic_data']['department'] = $rvalue['department'];
				$final_datas[$rvalue['employee_id']]['basic_data']['emp_code'] = $rvalue['emp_code'];

				$transaction_inn = '0';
				
				if($transaction_datas){
					foreach($transaction_datas as $tkey => $tvalue){
						$shift_inn = '';
						$leave_inn = '';
						$transaction_inn = '1';
						
						if($tvalue['firsthalf_status_manipulated'] == '1'){
							$firsthalf_status = 'P';
						} elseif($tvalue['firsthalf_status_manipulated'] == '0'){
							if($tvalue['firsthalf_status'] == 'PL'){
								$firsthalf_status = 'PL';
							} else {
								$firsthalf_status = 'A';

							}
						}
						// elseif ($tvalue['firsthalf_status_manipulated'] == '') {
						// 	$firsthalf_status = 'A';
						// }
						else {
							$firsthalf_status = $tvalue['firsthalf_status_manipulated'];
						}
						if($tvalue['secondhalf_status_manipulated'] == '1'){
							$secondhalf_status = 'P';
						} elseif($tvalue['secondhalf_status_manipulated'] == '0'){
							if($tvalue['secondhalf_status'] == 'PL'){
								$secondhalf_status = 'PL';
							} else {
								$secondhalf_status = 'A';
							}
						} else {
							$secondhalf_status = $tvalue['secondhalf_status_manipulated'];
						}
						if($tvalue['shift_intime_manipulated'] != '00:00:00' && $tvalue['shift_outtime_manipulated'] != '00:00:00'){
							$shift_intime = $tvalue['shift_intime_manipulated'];
							$shift_outtime = $tvalue['shift_outtime_manipulated'];
							$shift_inn = '1';
						} else {
							$shift_intime = $tvalue['shift_intime'];
							$shift_outtime = $tvalue['shift_outtime'];
							$shift_inn = '1';
						}
						
						if(ctype_alpha($shift_intime) === false){
							$shift_intime = date('H:i', strtotime($shift_intime));
						}

						if(ctype_alpha($shift_outtime) === false){
							$shift_outtime = date('H:i', strtotime($shift_outtime));
						}

						if(ctype_alpha($tvalue['act_intime_manipulated']) === false){
							$tvalue['act_intime_manipulated'] = date('H:i', strtotime($tvalue['act_intime_manipulated']));
						}

						if(ctype_alpha($tvalue['act_outtime_manipulated']) === false){
							$tvalue['act_outtime_manipulated'] = date('H:i', strtotime($tvalue['act_outtime_manipulated']));
						}

						if(ctype_alpha($tvalue['late_time_manipulated']) === false){
							$tvalue['late_time_manipulated'] = date('H:i', strtotime($tvalue['late_time_manipulated']));
						}

						if(ctype_alpha($tvalue['early_time_manipulated']) === false){
							$tvalue['early_time_manipulated'] = date('H:i', strtotime($tvalue['early_time_manipulated']));
						}

						if(ctype_alpha($tvalue['working_time_manipulated']) === false){
							$tvalue['working_time_manipulated'] = date('H:i', strtotime($tvalue['working_time_manipulated']));
						}

						if($tvalue['absent_status_manipulated'] == '1'){
							$leave_href = $this->url->link('transaction/leave/insert', 'token=' . $this->session->data['token'].'&filter_name_id='.$tvalue['emp_id'].'&from='.$tvalue['date'], 'SSL');
						} else {
							$leave_href = '';
						}

						if($firsthalf_status == 'WO'){
							$shift_intime = 'WO';
							$shift_outtime = 'WO';
						} elseif($firsthalf_status == 'HLD'){
							$shift_intime = 'HLD';
							$shift_outtime = 'HLD';
						} 




						// if($tvalue['firsthalf_status'] == 'PL'){
						// // 	echo'<pre>';
						// // print_r($tvalue['firsthalf_status']);
						// // exit;
						// 	$tvalue['act_intime_manipulated'] = 'PL';
						// 	$tvalue['act_outtime_manipulated'] = 'PL';
						// 	$tvalue['working_time_manipulated']  = 'PL';
						// }

						$is_week_off = 0;
						if($tvalue['department_id'] != '15'){
							if(date('N', strtotime($tvalue['date'])) == 5){
								$is_week_off = 1;
							}
						}

						$is_week_off_1 = 0;
						if($tvalue['department_id'] == '15'){
							if(date('N', strtotime($tvalue['date'])) == 5){
								$is_week_off_1 = 1;
							}
						}

						if($is_week_off == 1){
							$tvalue['act_intime'] = '00:00';
							$tvalue['act_intime_manipulated'] = '00:00';
							$tvalue['act_outtime']    = '00:00';
							$tvalue['act_outtime_manipulated']    = '00:00';
							$tvalue['working_time']  = '00:00';
							$tvalue['working_time_manipulated']  = '00:00';
							$tvalue['late_time'] =  '00:00';
							$tvalue['late_time_manipulated'] =  '00:00';
							$tvalue['early_time']	=  '00:00';
							$tvalue['early_time_manipulated']	=  '00:00';
							$tvalue['over_time']	= '00:00';
							$tvalue['over_time_manipulated']	= '00:00';
							$shift_intime = 'WO';
							$shift_outtime = 'WO';
							$firsthalf_status = 'WO';
							$secondhalf_status = 'WO';
						} else if($is_week_off_1 == 1 && $tvalue['working_time_manipulated'] != '00:00'){
							$shift_intime = 'WOP';
							$shift_outtime = 'WOP';
							$firsthalf_status = 'WOP';
							$secondhalf_status = 'WOP';
						}

						if($firsthalf_status == 'A' && $secondhalf_status =='A' ){
							$tvalue['act_intime_manipulated'] ='00.00';
							$tvalue['act_outtime_manipulated']='00.00';
						}

						/*if($tvalue['present_status'] == '0.5'){
							$firsthalf_status = 'P1/2';
						}*/

						if(!isset($firsthalf_status)){
							if($tvalue['working_time_manipulated'] >= '04:00' && $tvalue['working_time_manipulated'] <= '07:50'  ){
							$firsthalf_status = 'P1/2';
							}
							if($tvalue['working_time_manipulated'] >= '07:50' ){
								$firsthalf_status = 'P';
							} else if($tvalue['working_time_manipulated'] < '04:00' ){
								$firsthalf_status = 'A';
							}
						}


						// if($tvalue['absent_status'] == '1'){
						// 	$firsthalf_status = 'A';
						// }

						// if($tvalue['absent_status_manipulated'] == 1){
						// 	$firsthalf_status = 'A';							
						// }
						$performance_data[$rvalue['employee_id']]['action'][$tvalue['date']] = array(
							'name' => $tvalue['emp_name'],
							'emp_code' => $tvalue['emp_id'],
							'shift_intime' => $shift_intime,
							'shift_outtime' => $shift_outtime,
							'act_intime' => $tvalue['act_intime_manipulated'],
							'act_outtime' => $tvalue['act_outtime_manipulated'],
							'late_time' => $tvalue['late_time_manipulated'],
							'early_time' => $tvalue['early_time_manipulated'],
							'working_time' => $tvalue['working_time_manipulated'],
							'date' => $tvalue['date'],
							'weekly_off' => $tvalue['weekly_off'],
							'holiday_id' => $tvalue['holiday_id'],
							'firsthalf_status' => $firsthalf_status,
							'secondhalf_status' => $secondhalf_status,
							'absent' => $tvalue['absent_status_manipulated'],
							'present' => $tvalue['present_status'],
						);
					}
				} else {
					unset($final_datas[$rvalue['employee_id']]);
				}
				if($performance_data){
					foreach ($day as $dkey => $dvalue) {
						foreach ($performance_data as $pkey => $pvalue) {
							if(isset($pvalue['action'][$dvalue['date']]['date'])){
							} else {
								$performance_data[$rvalue['employee_id']]['action'][$dvalue['date']] = array(
									'name' => $rvalue['name'],
									'emp_code' => $rvalue['emp_code'],
									'shift_intime' => '',
									'shift_outtime' => '',
									'act_intime' => '',
									'act_outtime' => '',
									'late_time' => '',
									'early_time' => '',
									'working_time' => '',
									'weekly_off' => 0,
									'holiday_id' => 0,
									'firsthalf_status' => '',
									'secondhalf_status' => '',
									'absent' => 1,
									'date' => $dvalue['date'],
									'present' => 0,
								);			
							}
						}
					}
				}
				foreach ($performance_data as $pkey => $pvalue) {
					//echo"<pre>";print_r($pvalue);exit;
					$per_sort_data = $this->array_sort($pvalue['action'], 'date', SORT_ASC);
					$final_datas[$rvalue['employee_id']]['tran_data'] = $per_sort_data;
				}
			}
		}

		/*echo'<pre>';
		print_r($final_datas);
		exit;*/

		$this->data['final_datas'] = $final_datas;
		$type_data = array(
			'1' => 'Default',
			//'2' => 'Late',
			'3' => 'Loss',
			'4' => 'Absent',
			'5' => 'Excess',
			//'6' => 'Manual'
		);

		$this->data['type_data'] = $type_data;

		$unit_datas = $this->model_report_attendance->getlocation_list();
		$unit_data = array();
		$unit_data['0'] = 'All';
		foreach ($unit_datas as $dkey => $dvalue) {
			$unit_data[$dvalue['unit_id']] = $dvalue['unit'];
		}
		// echo '<pre>';
		// print_r($unit_datas);
		// exit;
		$this->data['unit_data'] = $unit_data;

		$designation_datas = $this->model_report_attendance->getdesignation_list();
		$designation_data = array();
		$designation_data['0'] = 'All';
		foreach ($designation_datas as $dkey => $dvalue) {
			$designation_data[$dvalue['designation_id']] = $dvalue['designation'];
		}
		
		$this->data['designation_data'] = $designation_data;

		
		$department_datas = $this->model_report_attendance->getdepartment_list();
		$department_data = array();
		$department_data['0'] = 'All';
		foreach ($department_datas as $dkey => $dvalue) {
			$department_data[$dvalue['department_id']] = $dvalue['department'];
		}
		$this->data['department_data'] = $department_data;
		//echo "<pre>";print_r($department_data);exit;
		$group_datas = $this->model_report_attendance->getgroup_list();
		$group_data = array();
		$group_data['0'] = 'All';
		$group_data['1'] = 'All WITHOUT OFFICIALS';
		foreach ($group_datas as $gkey => $gvalue) {
			$group_data[$gvalue['group']] = $gvalue['group'];
		}
		$this->data['group_data'] = $group_data;
		
		if(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_all_status'] = $this->language->get('text_all_status');

		
		$this->data['entry_date_start'] = $this->language->get('entry_date_start');
		$this->data['entry_date_end'] = $this->language->get('entry_date_end');
		
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['button_export'] = $this->language->get('button_export');

		$this->data['token'] = $this->session->data['token'];

		if(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}
		if (isset($this->request->get['filter_type'])) {
			$url .= '&filter_type=' . $this->request->get['filter_type'];
		}
		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}
		if (isset($this->request->get['filter_designation'])) {
			$url .= '&filter_designation=' . $this->request->get['filter_designation'];
		}
		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}
		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}

		if(isset($this->session->data['is_dept']) && $this->session->data['is_dept'] == '1'){
			$user_dept = 1;
			$is_dept = 1;
		} elseif(isset($this->session->data['is_user'])){
			$user_dept = 1;
			$is_dept = 0;
		} else {
			$user_dept = 0;
			$is_dept = 0;
		}
		$this->data['user_dept'] = $user_dept;
		$this->data['is_dept'] = $is_dept;

		$this->data['filter_date_start'] = $filter_date_start;
		$this->data['filter_date_end'] = $filter_date_end;
		$this->data['filter_name'] = $filter_name;
		$this->data['filter_name_id'] = $filter_name_id;
		$this->data['filter_type'] = $filter_type;
		$this->data['filter_unit'] = $filter_unit;
		$this->data['filter_designation'] = $filter_designation;
		$this->data['filter_department'] = $filter_department;
		$this->data['group'] = $group;
		//echo 'i m in';exit;
		$this->template = 'report/performance.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	function array_sort($array, $on, $order=SORT_ASC){

	    $new_array = array();
	    $sortable_array = array();

	    if (count($array) > 0) {
	        foreach ($array as $k => $v) {
	            if (is_array($v)) {
	                foreach ($v as $k2 => $v2) {
	                    if ($k2 == $on) {
	                        $sortable_array[$k] = $v2;
	                    }
	                }
	            } else {
	                $sortable_array[$k] = $v;
	            }
	        }

	        switch ($order) {
	            case SORT_ASC:
	                asort($sortable_array);
	                break;
	            case SORT_DESC:
	                arsort($sortable_array);
	                break;
	        }

	        foreach ($sortable_array as $k => $v) {
	            $new_array[$k] = $array[$k];
	        }
	    }

	    return $new_array;
	}

	public function export(){
		$this->language->load('report/performance');
		$this->load->model('report/common_report');
		$this->load->model('transaction/transaction');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			//$filter_date_start = date('Y-m-d');
			$filter_date_start = date('Y-m-d', strtotime(date('Y') . '-' . date('m') . '-01'));
			//$from = date('Y-m-d');
			//$filter_date_start = date('Y-m-d', strtotime($from . "-1 day"));
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
			// $filter_date_end1 = $this->model_report_common_report->getfilter_date_end($filter_date_start);
			// if(strtotime($filter_date_end) > strtotime($filter_date_end1) ){
			// 	$filter_date_end = $filter_date_end1;
			// }
		} else {
			$filter_date_end = date('Y-m-d');
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} elseif(isset($this->session->data['employee_id'])){
			$emp_name = $this->db->query("SELECT `name` FROM `oc_employee` WHERE `employee_id` = '".$this->session->data['employee_id']."' ")->row['name'];
			$filter_name = $emp_name;
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} elseif(isset($this->session->data['employee_id'])){
			$filter_name_id = $this->session->data['employee_id'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['filter_unit'])) {
			$filter_unit = $this->request->get['filter_unit'];
		} else {
			$filter_unit = '';
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
			//$filter_date_end1 = $this->model_report_common_report->getfilter_date_end($filter_date_start, $filter_name_id, $filter_unit);
			//if(strtotime($filter_date_end) > strtotime($filter_date_end1) ){
				//$filter_date_end = $filter_date_end1;
			//}
		} else {
			$filter_date_end = date('Y-m-d');
		}
		
		if (isset($this->request->get['filter_type'])) {
			$filter_type = $this->request->get['filter_type'];
		} else {
			$filter_type = 1;
		}

		if (isset($this->request->get['filter_designation'])) {
			$filter_designation = $this->request->get['filter_designation'];
		} else {
			$filter_designation = '';
		}

		if (isset($this->request->get['filter_department'])) {
			$filter_department = $this->request->get['filter_department'];
		} else {
			$filter_department = '';
		}

		if(isset($this->session->data['dept_names'])){
			$filter_departments = html_entity_decode($this->session->data['dept_names']);
			$filter_departments = "'" . str_replace(",", "','", html_entity_decode($filter_departments)) . "'";
		} else {
			$filter_departments = '';
		}

		if (isset($this->request->get['group'])) {
			$group = $this->request->get['group'];
		} else {
			$group = '0';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$this->load->model('report/attendance');
		$this->load->model('transaction/transaction');
		$this->load->model('catalog/employee');

		$this->data['attendace'] = array();

		$data = array(
			'filter_date_start'	     => $filter_date_start,
			'filter_date_end'	     => $filter_date_end,
			'filter_name'	     	 => $filter_name,
			'filter_name_id'	     => $filter_name_id,
			'filter_unit'			=> $filter_unit,
			'filter_designation'	=>$filter_designation,
			'filter_department'		=> $filter_department,
			'filter_departments'	 => $filter_departments,
			'group'					 => $group,
			'day_close'              => 1,
			'start'                  => ($page - 1) * 7000,
			'limit'                  => 7000
		);

		//$start_time = strtotime($filter_date_start);
		//$compare_time = strtotime(date('Y-m-d'));

		$day = array();
		$days = $this->GetDays($filter_date_start, $filter_date_end);
        foreach ($days as $dkey => $dvalue) {
        	$dates = explode('-', $dvalue);
        	$day[$dkey]['day'] = $dates[2];
        	$day[$dkey]['date'] = $dvalue;
        }
        $this->data['days'] = $day;

        $final_datas = array();
       	$results1 = $this->model_report_common_report->getemployees($data);
        $results2 = array();//$this->model_report_common_report->getemployees_reporting($data);
        $results = array_merge($results1, $results2);
        foreach ($results as $rkey => $rvalue) {
			$performance_data = array();
			$transaction_datas = $this->model_report_common_report->gettransaction_data($rvalue['employee_id'], $data);
			$final_datas[$rvalue['employee_id']]['basic_data']['name'] = $rvalue['name'];
			$final_datas[$rvalue['employee_id']]['basic_data']['department'] = $rvalue['department'];
			$final_datas[$rvalue['employee_id']]['basic_data']['emp_code'] = $rvalue['emp_code'];

			$transaction_inn = '0';
			
			if($transaction_datas){
				foreach($transaction_datas as $tkey => $tvalue){
					$shift_inn = '';
					$leave_inn = '';
					$transaction_inn = '1';

					// echo'<pre>';
					// print_r($tvalue['leave_status'] );
					// exit;
					
					if($tvalue['firsthalf_status_manipulated'] == '1'){
						$firsthalf_status = 'P';
					} elseif($tvalue['firsthalf_status_manipulated'] == '0'){
						if($tvalue['firsthalf_status'] == 'PL'){
							$firsthalf_status = 'PL';
						} else {
							$firsthalf_status = 'A';

						}
					} else {
						$firsthalf_status = $tvalue['firsthalf_status_manipulated'];
					}
					if($tvalue['secondhalf_status_manipulated'] == '1'){
						$secondhalf_status = 'P';
					} elseif($tvalue['secondhalf_status_manipulated'] == '0'){
						if($tvalue['secondhalf_status'] == 'PL'){
							$secondhalf_status = 'PL';
						} else {
							$secondhalf_status = 'A';

						}
						$secondhalf_status = 'A';
					} else {
						$secondhalf_status = $tvalue['secondhalf_status_manipulated'];
					}
					if($tvalue['shift_intime_manipulated'] != '00:00:00' && $tvalue['shift_outtime_manipulated'] != '00:00:00'){
						$shift_intime = $tvalue['shift_intime_manipulated'];
						$shift_outtime = $tvalue['shift_outtime_manipulated'];
						$shift_inn = '1';
					} else {

						if($tvalue['leave_status'] == 0){
							$shift_intime = $tvalue['shift_intime'];
							$shift_outtime = $tvalue['shift_outtime'];
							$shift_inn = '1';
						}else {
							$shift_intime = $tvalue['firsthalf_status'];
							$shift_outtime = $tvalue['secondhalf_status'];
						}
					}


					
					if(ctype_alpha($shift_intime) === false){
						$shift_intime = date('H:i', strtotime($shift_intime));
					}

					if(ctype_alpha($shift_outtime) === false){
						$shift_outtime = date('H:i', strtotime($shift_outtime));
					}

					if(ctype_alpha($tvalue['act_intime_manipulated']) === false){
						$tvalue['act_intime_manipulated'] = date('H:i', strtotime($tvalue['act_intime_manipulated']));
					}

					if(ctype_alpha($tvalue['act_outtime_manipulated']) === false){
						$tvalue['act_outtime_manipulated'] = date('H:i', strtotime($tvalue['act_outtime_manipulated']));
					}

					if(ctype_alpha($tvalue['late_time_manipulated']) === false){
						$tvalue['late_time_manipulated'] = date('H:i', strtotime($tvalue['late_time_manipulated']));
					}

					if(ctype_alpha($tvalue['early_time_manipulated']) === false){
						$tvalue['early_time_manipulated'] = date('H:i', strtotime($tvalue['early_time_manipulated']));
					}

					if(ctype_alpha($tvalue['working_time_manipulated']) === false){
						$tvalue['working_time_manipulated'] = date('H:i', strtotime($tvalue['working_time_manipulated']));
					}

					if($tvalue['absent_status_manipulated'] == '1'){
						$leave_href = $this->url->link('transaction/leave/insert', 'token=' . $this->session->data['token'].'&filter_name_id='.$tvalue['emp_id'].'&from='.$tvalue['date'], 'SSL');
					} else {
						$leave_href = '';
					}

					if($firsthalf_status == 'WO'){
						$shift_intime = 'WO';
						$shift_outtime = 'WO';
					} elseif($firsthalf_status == 'HLD'){
						$shift_intime = 'HLD';
						$shift_outtime = 'HLD';
					}

					$is_week_off = 0;
					if($tvalue['department_id'] != '15'){
						if(date('N', strtotime($tvalue['date'])) == 5){
							$is_week_off = 1;
						}
					}

					$is_week_off_1 = 0;
					if($tvalue['department_id'] == '15'){
						if(date('N', strtotime($tvalue['date'])) == 5){
							$is_week_off_1 = 1;
						}
					}

					if($is_week_off == 1){
						$tvalue['act_intime'] = '00:00';
						$tvalue['act_intime_manipulated'] = '00:00';
						$tvalue['act_outtime']    = '00:00';
						$tvalue['act_outtime_manipulated']    = '00:00';
						$tvalue['working_time']  = '00:00';
						$tvalue['working_time_manipulated']  = '00:00';
						$tvalue['late_time'] =  '00:00';
						$tvalue['late_time_manipulated'] =  '00:00';
						$tvalue['early_time']	=  '00:00';
						$tvalue['early_time_manipulated']	=  '00:00';
						$tvalue['over_time']	= '00:00';
						$tvalue['over_time_manipulated']	= '00:00';
						$shift_intime = 'WO';
						$shift_outtime = 'WO';
						$firsthalf_status = 'WO';
						$secondhalf_status = 'WO';
					} else if($is_week_off_1 == 1 && $tvalue['working_time_manipulated'] != '00:00'){
						$shift_intime = 'WOP';
						$shift_outtime = 'WOP';
						$firsthalf_status = 'WOP';
						$secondhalf_status = 'WOP';
					}

					if($firsthalf_status == 'A' && $secondhalf_status =='A' ){
						$tvalue['act_intime_manipulated'] ='00:00';
						$tvalue['act_outtime_manipulated']='00:00';
					}

				/*	if($tvalue['present_status'] == '0.5'){
						$firsthalf_status = 'P1/2';
					}*/

					if(!isset($firsthalf_status)){
						if($tvalue['working_time_manipulated'] >= '04:00' && $tvalue['working_time_manipulated'] <= '07:50'  ){
						$firsthalf_status = 'P1/2';
						}
						if($tvalue['working_time_manipulated'] >= '07:50' ){
							$firsthalf_status = 'P';
						} else if($tvalue['working_time_manipulated'] < '04:00' ){
							$firsthalf_status = 'A';
						}
					}

					$performance_data[$rvalue['employee_id']]['action'][$tvalue['date']] = array(
						'name' => $tvalue['emp_name'],
						'emp_code' => $tvalue['emp_id'],
						'shift_intime' => $shift_intime,
						'shift_outtime' => $shift_outtime,
						'act_intime' => $tvalue['act_intime_manipulated'],
						'act_outtime' => $tvalue['act_outtime_manipulated'],
						'late_time' => $tvalue['late_time_manipulated'],
						'early_time' => $tvalue['early_time_manipulated'],
						'working_time' => $tvalue['working_time_manipulated'],
						'date' => $tvalue['date'],
						'weekly_off' => $tvalue['weekly_off'],
						'holiday_id' => $tvalue['holiday_id'],
						'firsthalf_status' => $firsthalf_status,
						'secondhalf_status' => $secondhalf_status,
						'absent' => $tvalue['absent_status_manipulated'],
						'present' => $tvalue['present_status'],
					);
				}
			} else {
				unset($final_datas[$rvalue['employee_id']]);
			}
			if($performance_data){
				foreach ($day as $dkey => $dvalue) {
					foreach ($performance_data as $pkey => $pvalue) {
						if(isset($pvalue['action'][$dvalue['date']]['date'])){
						} else {
							$performance_data[$rvalue['employee_id']]['action'][$dvalue['date']] = array(
								'name' => $rvalue['name'],
								'emp_code' => $rvalue['emp_code'],
								'shift_intime' => '',
								'shift_outtime' => '',
								'act_intime' => '',
								'act_outtime' => '',
								'late_time' => '',
								'early_time' => '',
								'working_time' => '',
								'weekly_off' => 0,
								'holiday_id' => 0,
								'firsthalf_status' => '',
								'secondhalf_status' => '',
								'absent' => 1,
								'date' => $dvalue['date'],
							);			
						}
					}
				}
			}
			foreach ($performance_data as $pkey => $pvalue) {
				$per_sort_data = $this->array_sort($pvalue['action'], 'date', SORT_ASC);
				$final_datas[$rvalue['employee_id']]['tran_data'] = $per_sort_data;
			}
		}
		$this->data['final_datas'] = $final_datas;
		
		//$results = $this->model_report_common_report->getAttendance($data);
		
		$type_data = array(
			'1' => 'Default',
			//'2' => 'Late',
			'3' => 'Loss',
			'4' => 'Absent',
			'5' => 'Excess',
			//'6' => 'Manual'
		);

		$this->data['type_data'] = $type_data;

		$unit_datas = $this->model_report_attendance->getlocation_list();
		$unit_data = array();
		$unit_data['0'] = 'All';
		foreach ($unit_datas as $dkey => $dvalue) {
			$unit_data[$dvalue['unit_id']] = $dvalue['unit'];
		}
		// echo '<pre>';
		// print_r($unit_datas);
		// exit;
		$this->data['unit_data'] = $unit_data;

		$designation_datas = $this->model_report_attendance->getdesignation_list();
		$designation_data = array();
		$designation_data['0'] = 'All';
		foreach ($designation_datas as $dkey => $dvalue) {
			$designation_data[$dvalue['designation_id']] = $dvalue['designation'];
		}
		
		$this->data['designation_data'] = $designation_data;

		
		$department_datas = $this->model_report_attendance->getdepartment_list();
		$department_data = array();
		$department_data['0'] = 'All';
		foreach ($department_datas as $dkey => $dvalue) {
			$department_data[$dvalue['department_id']] = $dvalue['department'];
		}
		$this->data['department_data'] = $department_data;
		
		$group_datas = $this->model_report_attendance->getgroup_list();
		$group_data = array();
		$group_data['0'] = 'All';
		$group_data['1'] = 'All WITHOUT OFFICIALS';
		foreach ($group_datas as $gkey => $gvalue) {
			$group_data[$gvalue['group']] = $gvalue['group'];
		}
		$this->data['group_data'] = $group_data;
		
		if(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_all_status'] = $this->language->get('text_all_status');

		
		$this->data['entry_date_start'] = $this->language->get('entry_date_start');
		$this->data['entry_date_end'] = $this->language->get('entry_date_end');
		
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['button_export'] = $this->language->get('button_export');

		$this->data['token'] = $this->session->data['token'];

		if(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}


		$url = '';

	if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}
		if (isset($this->request->get['filter_type'])) {
			$url .= '&filter_type=' . $this->request->get['filter_type'];
		}
		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}
		if (isset($this->request->get['filter_designation'])) {
			$url .= '&filter_designation=' . $this->request->get['filter_designation'];
		}
		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}
		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}
		$url .= '&once=1';

		//$final_datas = array_chunk($final_datas, 3);

		/*echo '<pre>';
		print_r($final_datas);
		exit;*/
		
		if($final_datas){
			if($filter_type == 1){
				$report_type = 'Defult';
			} elseif($filter_type == 2){
				$report_type = 'Late';
			} elseif ($filter_type == 3) {
				$report_type = 'Less Time';
			} elseif($filter_type == 4){
				$report_type = 'Absent';
			} elseif($filter_type == 5){
				$report_type = 'Excess';
			} elseif($filter_type == 6){
				$report_type = 'Manual';
			}
			//$month = date("F", mktime(0, 0, 0, $filter_month, 10));
			$template = new Template();		
			$template->data['final_datass'] = $final_datas;
			$template->data['date_start'] = $filter_date_start;
			$template->data['filter_type'] = $filter_type;
			$template->data['date_end'] = $filter_date_end;
			$template->data['days'] = $day;
			$template->data['report_type'] = $report_type;
			$template->data['title'] = 'Periodic Report';
			if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
				$template->data['base'] = HTTPS_SERVER;
			} else {
				$template->data['base'] = HTTP_SERVER;
			}
			$html = $template->fetch('report/performance_html.tpl');
			//echo $html;exit;
			$filename = "Periodic_Report_".$report_type;
			
			// if($filter_type == 6){
			// 	header('Content-type: text/html');
			// 	header('Content-Disposition: attachment; filename='.$filename.".html");
			// 	echo $html;
			// } else {
				header("Content-Type: application/vnd.ms-excel; charset=utf-8");
				header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				header("Cache-Control: private",false);
				echo $html;
				exit;
			//}		
		} else {
			$this->session->data['warning'] = 'No Data Found';
			$this->redirect($this->url->link('report/performance', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/employee');

			if(isset($this->session->data['dept_names'])){
				$filter_departments = html_entity_decode($this->session->data['dept_names']);
				$filter_departments = "'" . str_replace(",", "','", html_entity_decode($filter_departments)) . "'";
			} else {
				$filter_departments = '';
			}

			if($filter_departments == ''){
				if(isset($this->session->data['dept_name'])){
					$filter_department = $this->session->data['dept_name'];
				} else {
					$filter_department = '';
				}
			} else {
				$filter_department = '';
			}

			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				'filter_department' => $filter_department,
				'filter_departments' => $filter_departments,
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_employee->getemployees($data);

			foreach ($results as $result) {
				$json[] = array(
					'employee_id' => $result['employee_id'],
					'emp_code' => $result['emp_code'], 
					'name'            => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}
}
?>