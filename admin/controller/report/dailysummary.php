<?php
class ControllerReportDailySummary extends Controller { 
	public function index() {  
		$this->language->load('report/dailysummary');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			//$filter_date_start = date('Y-m-d');
			$from = date('Y-m-d');
			$filter_date_start = date('Y-m-01');
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			//$filter_date_start = date('Y-m-d');
			$from = date('Y-m-d');
			$filter_date_end = date('Y-m-d', strtotime($from));
		}

		if (isset($this->request->get['unit'])) {
			$unit = $this->request->get['unit'];
			} else {
			$unit = '';
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name =	($this->request->get['filter_name']);
		} else {
			$filter_name = "";
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = ($this->request->get['filter_name_id']);
		} else {
			$filter_name_id = "";
		}

		if (isset($this->request->get['department'])) {
			$department = ($this->request->get['department']);
		} else {
			$department ="";
		}
		if (isset($this->request->get['designation'])) {
			$designation = $this->request->get['designation'];
		} else {
			$designation = '';
		}


		if (isset($this->request->get['group'])) {
			$group = $this->request->get['group'];
		} else {
			$group = "";
		}

		if (isset($this->request->get['status'])) {
			$status = $this->request->get['status'];
		} else {
			$status = "";
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		if (isset($this->request->get['designation'])) {
			$url .= '&designation=' . $this->request->get['designation'];
		}

		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}
		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}
		if (isset($this->request->get['status'])) {
			$url .= '&status=' . $this->request->get['status'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('report/dailysummary', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->load->model('report/dailysummary');
		$this->load->model('report/attendance');

		$data['attendace'] = array();

		$data = array(
			'filter_date_start'	     => $filter_date_start,
			'filter_date_end'	     => $filter_date_end,
			'filter_name'	    	 => $filter_name,
			'filter_name_id'	     => $filter_name_id,
			'designation'			 => $designation,
			'unit'					 => $unit,
			'department'			 => $department,
			'group'					 => $group,
			'status'			     => $status, 
			'start'                  => ($page - 1) * 7000,
			'limit'                  => 7000
		);
		//echo "<pre>"; print_r($data);

		if($url == ''){
			$this->data['generate'] = $this->url->link('transaction/transaction/generate_filter_date', 'token=' . $this->session->data['token'] . '&filter_date_start=' . $filter_date_start, 'SSL');
		} else {
			$this->data['generate'] = $this->url->link('transaction/transaction/generate_filter_date', 'token=' . $this->session->data['token'] . $url, 'SSL');
		}

		$this->data['export'] = $this->url->link('report/dailysummary/export', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$this->data['dailyreports'] = array();
		if(isset($this->request->get['once'])){
			$results = $this->model_report_attendance->gettransaction_data($data);
			$cnt = 0;
			$current_dept_id = '';
			$current_emp_id = '';
			$rgvalues = array();
			foreach ($results as $rkey => $result) {
				if($current_emp_id != $result['emp_id']){
					$current_emp_id = $result['emp_id'];
					$time_arr = array();
					$time_arr_1 = array();
					$overtime_arr = array();
					$countholiday = 0;
					$countoff = 0;
					$countleave = 0;
					$countabsent = 0;
					$countpresent = 0;
					$countoff_present = 0;
				}

				$is_sunday = 0;
				if($result['department_id'] != '15'){
					if(date('N', strtotime($result['date'])) == 5){
					//if(date('N', strtotime($result['date'])) > 6){
						$is_sunday = 1;
					}
				}

				$is_sunday_1 = 0;
				if($result['department_id'] == '15'){
					if(date('N', strtotime($result['date'])) == 5){
						$is_sunday_1 = 1;
					}
				}
				//echo $is_sunday_1;exit;
				// echo '<pre>';
				// print_r($result);
				// exit;

				/*
				if($result['leave_status'] == '1' || $result['leave_status'] == '0.5') { 
					$Shift_type='NS';
				} elseif($result['weekly_off'] != '0') { 
					$Shift_type='NS';
				} elseif($result['holiday_id'] != '0') {
					$Shift_type='NS';
				} elseif($result['present_status'] != '0' || $result['present_status_manipulated'] != '0') { 
					$Shift_type='GS';
				} elseif($result['absent_status'] != '0' || $result['absent_status_manipulated'] != '0') {
					$Shift_type='NS';
				}
				*/
				if($result['shift_code_manipulated'] != ''){
					$Shift_type=$result['shift_code_manipulated'];
				} else {
					$Shift_type=$result['shift_code'];
				}

				if ($is_sunday == '1') {
					if($result['department_id'] == '12' && $result['present_status_manipulated'] == 1){
						$countoff_present = $countoff_present + 1;
					} else {
						$countoff = $countoff + 1;
					}
				} elseif ($result['holiday_id'] == '1') {
					$countholiday = $countholiday + 1;
				} elseif ($result['leave_status'] == '1') {
					$countleave = $countleave + 1;
				} elseif ($result['leave_status'] == '0.5') {
					$countleave = $countleave + 0.5;
					if ($result['absent_status'] == '0.5') {
						$countabsent = $countabsent + 0.5;
					} else {
						$countpresent = $countpresent + 0.5;
					}
				} elseif ($result['present_status'] == '1' || $result['present_status_manipulated'] == '1') {
					$countpresent = $countpresent + 1 ;
				} elseif ($result['present_status'] == '0.5' || $result['present_status_manipulated'] == '0.5') {
					$countpresent = $countpresent + 0.5;
				} elseif ($result['absent_status'] == '1' || $result['absent_status_manipulated'] == '1') {
					$countabsent = $countabsent + 1;
				} elseif ($result['absent_status'] == '0.5' || $result['absent_status_manipulated'] == '0.5') {
					$countabsent = $countabsent + 0.5;
				}
				$status = '';
				if($result['leave_status'] == 1) { 
					$status = 'leave';
				} elseif($is_sunday == '1') { 
					if($result['department_id'] == '12' && $result['present_status_manipulated'] == 1){
						$status = 'Weekly Off Present';
					} else {
						$status = 'Weekly Off';
					}
				} elseif($is_sunday_1 == '1' && $result['working_time_manipulated'] != '00:00:00') { 
					$status = 'Weekly Off Present';
				} elseif($result['holiday_id'] != '0') {
					$status = 'Holiday';
				} elseif($result['halfday_status'] != '0') {
					$status = 'Half day';
				} elseif($result['present_status'] == '1' || $result['present_status_manipulated'] == '1') {
					$status = 'Present';
				} elseif($result['absent_status'] == '1' || $result['absent_status_manipulated'] == '1') {
					$status = '<b>Absent</b>';
				}

				if($result['manual_status'] == 1){
					$act_intime = $result['act_intime_manipulated'];
					$act_outtime = $result['act_outtime_manipulated'];
					$working_time = $result['working_time_manipulated'];
					$late_time = $result['late_time_manipulated'];
					$early_time = $result['early_time_manipulated'];
					$over_time = $result['over_time_manipulated'];
				} else {
					if($result['act_intime_manipulated'] != '00:00:00'){
						$act_intime = $result['act_intime_manipulated'];
					} else {
						$act_intime = $result['act_intime_manipulated'];
					}
					if($result['act_outtime_manipulated'] != '00:00:00'){
						$act_outtime = $result['act_outtime_manipulated'];
					} else {
						$act_outtime = $result['act_outtime_manipulated'];
					}
					if($result['working_time_manipulated'] != '00:00:00'){
						$working_time = $result['working_time_manipulated'];
					} else {
						$working_time = $result['working_time_manipulated'];
					}
					if($result['late_time_manipulated'] != '00:00:00'){
						$late_time = $result['late_time_manipulated'];
					} else {
						$late_time = $result['late_time_manipulated'];
					}
					if($result['early_time_manipulated'] != '00:00:00'){
						$early_time = $result['early_time_manipulated'];
					} else {
						$early_time = $result['early_time_manipulated'];
					}
					if($result['over_time_manipulated'] != '00:00:00'){
						$over_time = $result['over_time_manipulated'];
					} else {
						$over_time = $result['over_time_manipulated'];
					}
				}

				if ($result['holiday_id'] == '0' && $is_sunday == '0') {
					if($working_time != '00:00:00'){
					   $time_arr[] = $working_time;
					}
					if($over_time != '00:00:00'){
					   $time_arr_1[] = $over_time;
					}
				}
				
				if ($result['holiday_id'] != '0' || $is_sunday == '1') {
					if($countoff_present == 0){
						$act_intime = '00:00:00';
						$act_outtime    = '00:00:00';
						$working_time  = '00:00:00';
						$late_time =  '00:00:00';
						$early_time	=  '00:00:00';
						$over_time	= '00:00:00';
					}
				}

				$this->data['dailyreports'][$result['emp_id']]['trans_data'][] = array(
					'emp_name'    => $result['emp_name'],
					'emp_id'   => $result['emp_id'],
					'department'   => $result['department'],
					'date'		 => $result['date'],
					//'shift_name'	=> $Shift_type,
					'shift_name'	=>$result['shift_intime'].' - '.$result['shift_outtime'],
					'act_intime' => $act_intime,
					'outtime'	=> $act_outtime,
					'duration' => $working_time,
					'early'			=>$early_time,
					'late'			=>$late_time,
					'over_time'		=> $over_time,
					'status_name'	=> $status,
				);

				$next_key = $rkey + 1;
				if( (isset($results[$next_key]['emp_id']) && $result['emp_id'] != $results[$next_key]['emp_id']) || !isset($results[$next_key]['emp_id'])){
					$ltime = 0;
					$lhours = 0;
					$lminutes = 0;
					$lmin_hours = 0;
					$lmin_min = 0;
					foreach ($time_arr as $ltime_val) {
						$ltimes = explode(':', $ltime_val);
						$lhours += $ltimes[0];
						$lminutes += $ltimes[1];
					}
					if($lminutes > 0){
						$lmin_hours = floor($lminutes / 60);
						$lmin_min = ($lminutes % 60);
						$lmin_min = sprintf('%02d', $lmin_min);
					}
					
					$total_working_hours = ($lhours + $lmin_hours);
					$total_working_minutes = $lmin_min;

					// echo '<pre>';
					// print_r($time_arr_1);
					// exit;

					$ltime = 0;
					$lhours = 0;
					$lminutes = 0;
					$lmin_hours = 0;
					$lmin_min = 0;
					foreach ($time_arr_1 as $ltime_val) {
						$ltimes = explode(':', $ltime_val);
						$lhours += $ltimes[0];
						$lminutes += $ltimes[1];
					}
					if($lminutes > 0){
						$lmin_hours = floor($lminutes / 60);
						$lmin_min = ($lminutes % 60);
						$lmin_min = sprintf('%02d', $lmin_min);
					}
					
					$total_overtime_hours = ($lhours + $lmin_hours);
					$total_overtime_minutes = $lmin_min;

					$emp_data = $this->db->query("SELECT `is_lunch`, `emp_code` FROM `oc_employee` WHERE `employee_id` = '".$result['emp_id']."' ")->row;

					$this->data['dailyreports'][$result['emp_id']]['summary_data']['total_working_hours'] = $total_working_hours;
					$this->data['dailyreports'][$result['emp_id']]['summary_data']['total_working_minutes'] = $total_working_minutes;
					$this->data['dailyreports'][$result['emp_id']]['summary_data']['total_overtime_hours'] = $total_overtime_hours;
					$this->data['dailyreports'][$result['emp_id']]['summary_data']['total_overtime_minutes'] = $total_overtime_minutes;
					$this->data['dailyreports'][$result['emp_id']]['summary_data']['countpresent'] = $countpresent;
					$this->data['dailyreports'][$result['emp_id']]['summary_data']['countleave'] = $countleave;
					$this->data['dailyreports'][$result['emp_id']]['summary_data']['countholiday'] = $countholiday;
					$this->data['dailyreports'][$result['emp_id']]['summary_data']['countabsent'] = $countabsent;
					$this->data['dailyreports'][$result['emp_id']]['summary_data']['countoff'] = $countoff;
					$this->data['dailyreports'][$result['emp_id']]['summary_data']['countoff_present'] = $countoff_present;
					
					$this->data['dailyreports'][$result['emp_id']]['summary_data']['emp_name'] = $result['emp_name'];
					$this->data['dailyreports'][$result['emp_id']]['summary_data']['emp_id'] = $result['emp_id'];
					$this->data['dailyreports'][$result['emp_id']]['summary_data']['emp_code'] = $emp_data['emp_code'];
					$this->data['dailyreports'][$result['emp_id']]['summary_data']['department'] = $result['department'];
					$this->data['dailyreports'][$result['emp_id']]['summary_data']['unit'] = $result['unit'];
					$this->data['dailyreports'][$result['emp_id']]['summary_data']['designation'] = $result['designation'];
				}        
			}
		}

		$department_datas = $this->model_report_dailysummary->getdepartment_list();
		$department_data = array();
		$department_data['0'] = 'All';
		foreach ($department_datas as $dkey => $dvalue) {
			$department_data[$dvalue['department_id']] = $dvalue['department'];
		}
		$this->data['department_data'] = $department_data;
		$group_datas = $this->model_report_dailysummary->getgroup_list();
		$group_data = array();
		$group_data['0'] = 'All';
		foreach ($group_datas as $gkey => $gvalue) {
			$group_data[$gvalue['group']] = $gvalue['group'];
		}
		$this->data['group_data'] = $group_data;

		$designation_datas = $this->model_report_attendance->getdesignation_list();
		$designation_data = array();
		$designation_data['0'] = 'All';
		foreach ($designation_datas as $dkey => $dvalue) {
			$designation_data[$dvalue['designation_id']] = $dvalue['designation'];
		}
		// echo '<pre>';
		// print_r($designation_data);
		// exit;
		// $designation_data = array(
		// 	'0' => 'All',
		// 	'DISPENSARY' => 'DISPENSARY',
		// 	'A.D.M.' => 'A.D.M.',
		// 	'ELECT.& MECH.' => 'ELECT.& MECH.'
		// );
		$this->data['designation_data'] = $designation_data;

		$unit_id = 0;
		$in = 0;
		if($this->user->getUserGroupId() != 1 && $this->user->getUnitId() != '0'){
			$in = 1;
			$unit_id = $this->user->getUnitId();
		}
		$unit_datas = $this->model_report_attendance->getlocation_list();
		$unit_data = array();
		if($in == 0){
			$unit_data['0'] = 'All';
		}
		foreach ($unit_datas as $dkey => $dvalue) {
			if($in == 1){
				if($unit_id == $dvalue['unit_id']){
					$unit_data[$dvalue['unit_id']] = $dvalue['unit'];	
				}
			} else {
				$unit_data[$dvalue['unit_id']] = $dvalue['unit'];
			}
		}
		// echo '<pre>';
		// print_r($unit_data);
		// exit;
		$this->data['unit_data'] = $unit_data;
		
	
		$statuses = array(
			'0' => 'All',
			'1' => 'Present',
			'2' => 'Absent'
		);
		$this->data['statuses'] = $statuses;
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_all_status'] = $this->language->get('text_all_status');

		
		$this->data['entry_date_start'] = $this->language->get('entry_date_start');
		$this->data['entry_date_end'] = $this->language->get('entry_date_end');
		
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['button_export'] = $this->language->get('button_export');

		$this->data['token'] = $this->session->data['token'];

		if(isset($this->data['warning'])){
			$this->data['error_warning'] = $this->data['warning'];
		} elseif(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['designation'])) {
			$url .= '&designation=' . $this->request->get['designation'];
		}

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}
		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}
		if (isset($this->request->get['status'])) {
			$url .= '&status=' . $this->request->get['status'];
		}

		$this->data['filter_date_start'] = $filter_date_start;
		$this->data['filter_date_end'] = $filter_date_end;
		$this->data['filter_name'] = $filter_name;
		$this->data['filter_name_id'] = $filter_name_id;
		$this->data['unit'] = $unit;
		$this->data['designation'] = $designation;
		$this->data['department'] = $department;
		$this->data['group'] = $group;
		$this->data['status'] = $status;

		$this->template = 'report/dailysummary.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	public function export(){
		$this->language->load('report/dailysummary');
		$this->load->model('report/common_report');
		$this->load->model('report/dailysummary');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			//$filter_date_start = date('Y-m-d');
			$from = date('Y-m-d');
			$filter_date_start = date('Y-m-d', strtotime($from . "-1 day"));
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			//$filter_date_start = date('Y-m-d');
			$from = date('Y-m-d');
			$filter_date_end = date('Y-m-d', strtotime($from . "+29 day"));
		}

		if (isset($this->request->get['unit'])) {
			$unit = $this->request->get['unit'];
			} else {
			if($this->user->getId() == 1) {
				$unit = '';
			} else if($this->user->getId() == 7) {
				$unit = 'Moving';
			} else if($this->user->getId() == 3) {
				$unit = 'Mumbai';
			} else if($this->user->getId() == 4) {
				$unit = 'Pune';
			} else {
				$unit = 'Mumbai';
			}
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = html_entity_decode($this->request->get['filter_name']);
		} else {
			$filter_name = "";
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = html_entity_decode($this->request->get['filter_name_id']);
		} else {
			$filter_name_id = "";
		}

		if (isset($this->request->get['department'])) {
			$department = html_entity_decode($this->request->get['department']);
		} else {
			$department ="";
		}

		if (isset($this->request->get['group'])) {
			$group = $this->request->get['group'];
		} else {
			$group = "";
		}

		if (isset($this->request->get['status'])) {
			$status = $this->request->get['status'];
		} else {
			$status = "";
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}
		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}
		if (isset($this->request->get['status'])) {
			$url .= '&status=' . $this->request->get['status'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('report/dailysummary', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->load->model('report/dailysummary');
		$this->load->model('report/attendance');

		$data['attendace'] = array();

		$data = array(
			'filter_date_start'	     => $filter_date_start,
			'filter_date_end'	     => $filter_date_end,
			'filter_name'	    	 => $filter_name,
			'filter_name_id'	     => $filter_name_id,
			'unit'					 => $unit,
			'department'			 => $department,
			'group'					 => $group,
			'status'			     => $status, 
			'start'                  => ($page - 1) * 7000,
			'limit'                  => 7000
		);
		//echo "<pre>"; print_r($data);
		$this->data['dailyreports'] = array();
		$results = $this->model_report_attendance->gettransaction_data($data);
		$cnt = 0;
		$current_dept_id = '';
		$current_emp_id = '';
		$rgvalues = array();
		foreach ($results as $rkey => $result) {
			if($current_emp_id != $result['emp_id']){
				$current_emp_id = $result['emp_id'];
				$time_arr = array();
				$time_arr_1 = array();
				$overtime_arr = array();
				$countholiday = 0;
				$countoff = 0;
				$countleave = 0;
				$countabsent = 0;
				$countpresent = 0;
				$countoff_present = 0;
			}

			$is_sunday = 0;
			if($result['department_id'] != '15'){
				if(date('N', strtotime($result['date'])) == 5){
				//if(date('N', strtotime($result['date'])) > 6){
					$is_sunday = 1;
				}
			}

			$is_sunday_1 = 0;
			if($result['department_id'] == '15'){
				if(date('N', strtotime($result['date'])) == 5){
					$is_sunday_1 = 1;
				}
			}
			//echo $is_sunday_1;exit;
			// echo '<pre>';
			// print_r($result);
			// exit;

			/*
			if($result['leave_status'] == '1' || $result['leave_status'] == '0.5') { 
				$Shift_type='NS';
			} elseif($result['weekly_off'] != '0') { 
				$Shift_type='NS';
			} elseif($result['holiday_id'] != '0') {
				$Shift_type='NS';
			} elseif($result['present_status'] != '0' || $result['present_status_manipulated'] != '0') { 
				$Shift_type='GS';
			} elseif($result['absent_status'] != '0' || $result['absent_status_manipulated'] != '0') {
				$Shift_type='NS';
			}
			*/
			if($result['shift_code_manipulated'] != ''){
				$Shift_type=$result['shift_code_manipulated'];
			} else {
				$Shift_type=$result['shift_code'];
			}

			if ($is_sunday == '1') {
				if($result['department_id'] == '12' && $result['present_status_manipulated'] == 1){
					$countoff_present = $countoff_present + 1;
				} else {
					$countoff = $countoff + 1;
				}
			} elseif ($result['holiday_id'] == '1') {
				$countholiday = $countholiday + 1;
			} elseif ($result['leave_status'] == '1') {
				$countleave = $countleave + 1;
			} elseif ($result['leave_status'] == '0.5') {
				$countleave = $countleave + 0.5;
				if ($result['absent_status'] == '0.5') {
					$countabsent = $countabsent + 0.5;
				} else {
					$countpresent = $countpresent + 0.5;
				}
			} elseif ($result['present_status'] == '1' || $result['present_status_manipulated'] == '1') {
				$countpresent = $countpresent + 1 ;
			} elseif ($result['present_status'] == '0.5' || $result['present_status_manipulated'] == '0.5') {
				$countpresent = $countpresent + 0.5;
			} elseif ($result['absent_status'] == '1' || $result['absent_status_manipulated'] == '1') {
				$countabsent = $countabsent + 1;
			} elseif ($result['absent_status'] == '0.5' || $result['absent_status_manipulated'] == '0.5') {
				$countabsent = $countabsent + 0.5;
			}

			$status = '';
			if($result['leave_status'] == 1) { 
				$status = 'leave';
			} elseif($is_sunday == '1') { 
				if($result['department_id'] == '12' && $result['present_status_manipulated'] == 1){
					$status = 'Weekly Off Present';
				} else {
					$status = 'Weekly Off';
				}
			} elseif($is_sunday_1 == '1' && $result['working_time_manipulated'] != '00:00:00') { 
				$status = 'Weekly Off Present';
			} elseif($result['holiday_id'] != '0') {
				$status = 'Holiday';
			} elseif($result['halfday_status'] != '0') {
				$status = 'Half day';
			} elseif($result['present_status'] == '1' || $result['present_status_manipulated'] == '1') {
				$status = 'Present';
			} elseif($result['absent_status'] == '1' || $result['absent_status_manipulated'] == '1') {
				$status = '<b>Absent</b>';
			}

			if($result['manual_status'] == 1){
				$act_intime = $result['act_intime_manipulated'];
				$act_outtime = $result['act_outtime_manipulated'];
				$working_time = $result['working_time_manipulated'];
				$late_time = $result['late_time_manipulated'];
				$early_time = $result['early_time_manipulated'];
				$over_time = $result['over_time_manipulated'];
			} else {
				if($result['act_intime_manipulated'] != '00:00:00'){
					$act_intime = $result['act_intime_manipulated'];
				} else {
					$act_intime = $result['act_intime_manipulated'];
				}
				if($result['act_outtime_manipulated'] != '00:00:00'){
					$act_outtime = $result['act_outtime_manipulated'];
				} else {
					$act_outtime = $result['act_outtime_manipulated'];
				}
				if($result['working_time_manipulated'] != '00:00:00'){
					$working_time = $result['working_time_manipulated'];
				} else {
					$working_time = $result['working_time_manipulated'];
				}
				if($result['late_time_manipulated'] != '00:00:00'){
					$late_time = $result['late_time_manipulated'];
				} else {
					$late_time = $result['late_time_manipulated'];
				}
				if($result['early_time_manipulated'] != '00:00:00'){
					$early_time = $result['early_time_manipulated'];
				} else {
					$early_time = $result['early_time_manipulated'];
				}
				if($result['over_time_manipulated'] != '00:00:00'){
					$over_time = $result['over_time_manipulated'];
				} else {
					$over_time = $result['over_time_manipulated'];
				}
			}

			if ($result['holiday_id'] == '0' && $is_sunday == '0') {
				if($working_time != '00:00:00'){
				   $time_arr[] = $working_time;
				}
				if($over_time != '00:00:00'){
				   $time_arr_1[] = $over_time;
				}
			}
			
			if ($result['holiday_id'] != '0' || $is_sunday == '1') {
				if($countoff_present == 0){
					$act_intime = '00:00:00';
					$act_outtime    = '00:00:00';
					$working_time  = '00:00:00';
					$late_time =  '00:00:00';
					$early_time	=  '00:00:00';
					$over_time	= '00:00:00';
				}
			}

			$this->data['dailyreports'][$result['emp_id']]['trans_data'][] = array(
				'emp_name'    => $result['emp_name'],
				'emp_id'   => $result['emp_id'],
				'department'   => $result['department'],
				'date'		 => $result['date'],
				//'shift_name'	=> $Shift_type,
				'shift_name'	=>$result['shift_intime'].' - '.$result['shift_outtime'],
				'act_intime' => $act_intime,
				'outtime'	=> $act_outtime,
				'duration' => $working_time,
				'early'			=>$early_time,
				'late'			=>$late_time,
				'over_time'		=> $over_time,
				'status_name'	=> $status,
			);

			$next_key = $rkey + 1;
			if( (isset($results[$next_key]['emp_id']) && $result['emp_id'] != $results[$next_key]['emp_id']) || !isset($results[$next_key]['emp_id'])){
				$ltime = 0;
				$lhours = 0;
				$lminutes = 0;
				$lmin_hours = 0;
				$lmin_min = 0;
				foreach ($time_arr as $ltime_val) {
					$ltimes = explode(':', $ltime_val);
					$lhours += $ltimes[0];
					$lminutes += $ltimes[1];
				}
				if($lminutes > 0){
					$lmin_hours = floor($lminutes / 60);
					$lmin_min = ($lminutes % 60);
					$lmin_min = sprintf('%02d', $lmin_min);
				}
				
				$total_working_hours = ($lhours + $lmin_hours);
				$total_working_minutes = $lmin_min;

				// echo '<pre>';
				// print_r($time_arr_1);
				// exit;

				$ltime = 0;
				$lhours = 0;
				$lminutes = 0;
				$lmin_hours = 0;
				$lmin_min = 0;
				foreach ($time_arr_1 as $ltime_val) {
					$ltimes = explode(':', $ltime_val);
					$lhours += $ltimes[0];
					$lminutes += $ltimes[1];
				}
				if($lminutes > 0){
					$lmin_hours = floor($lminutes / 60);
					$lmin_min = ($lminutes % 60);
					$lmin_min = sprintf('%02d', $lmin_min);
				}
				
				$total_overtime_hours = ($lhours + $lmin_hours);
				$total_overtime_minutes = $lmin_min;

				$emp_data = $this->db->query("SELECT `is_lunch`, `emp_code` FROM `oc_employee` WHERE `employee_id` = '".$result['emp_id']."' ")->row;

				$this->data['dailyreports'][$result['emp_id']]['summary_data']['total_working_hours'] = $total_working_hours;
				$this->data['dailyreports'][$result['emp_id']]['summary_data']['total_working_minutes'] = $total_working_minutes;
				$this->data['dailyreports'][$result['emp_id']]['summary_data']['total_overtime_hours'] = $total_overtime_hours;
				$this->data['dailyreports'][$result['emp_id']]['summary_data']['total_overtime_minutes'] = $total_overtime_minutes;
				$this->data['dailyreports'][$result['emp_id']]['summary_data']['countpresent'] = $countpresent;
				$this->data['dailyreports'][$result['emp_id']]['summary_data']['countleave'] = $countleave;
				$this->data['dailyreports'][$result['emp_id']]['summary_data']['countholiday'] = $countholiday;
				$this->data['dailyreports'][$result['emp_id']]['summary_data']['countabsent'] = $countabsent;
				$this->data['dailyreports'][$result['emp_id']]['summary_data']['countoff'] = $countoff;
				$this->data['dailyreports'][$result['emp_id']]['summary_data']['countoff_present'] = $countoff_present;
				
				$this->data['dailyreports'][$result['emp_id']]['summary_data']['emp_name'] = $result['emp_name'];
				$this->data['dailyreports'][$result['emp_id']]['summary_data']['emp_id'] = $result['emp_id'];
				$this->data['dailyreports'][$result['emp_id']]['summary_data']['emp_code'] = $emp_data['emp_code'];
				$this->data['dailyreports'][$result['emp_id']]['summary_data']['department'] = $result['department'];
				$this->data['dailyreports'][$result['emp_id']]['summary_data']['unit'] = $result['unit'];
				$this->data['dailyreports'][$result['emp_id']]['summary_data']['designation'] = $result['designation'];
			}        
		}
		
		if($results){
			//$month = date("F", mktime(0, 0, 0, $filter_month, 10));
			$statuss = 'Daily Summary Report';
			$statusss = 'Daily_Summary_Report';
			$template = new Template();		
			//$template->data['dailyreports'] = $dailyreports;
			$template->data['dailyreports'] = $this->data['dailyreports'];
			$template->data['filter_date_start'] = $filter_date_start;
			$template->data['filter_date_end'] = $filter_date_end;
			$template->data['title'] = $statuss;
			if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
				$template->data['base'] = HTTPS_SERVER;
			} else {
				$template->data['base'] = HTTP_SERVER;
			}
			$html = $template->fetch('report/dailysummary_html.tpl');
			//echo $html;exit;
			 //header('Content-type: text/html');
			 //header('Content-Disposition: attachment; filename='.$filename);
			//echo $html;exit;
			if($filter_name != ''){
				$filename = $filter_name."_".$filter_date_start;
			} else {
				$filename = "Daily_Summary_Report_".$filter_date_start;
			}
			//echo $filename;exit();
			header("Content-Type: application/vnd.ms-excel; charset=utf-8");
			header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false);
			echo $html;
			exit;		
		} else {
			$this->session->data['warning'] = 'No Data Found';
			$this->redirect($this->url->link('report/dailysummary', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/employee');

			if(isset($this->session->data['dept_names'])){
				$filter_departments = html_entity_decode($this->session->data['dept_names']);
				$filter_departments = "'" . str_replace(",", "','", html_entity_decode($filter_departments)) . "'";
			} else {
				$filter_departments = '';
			}

			if($filter_departments == ''){
				if(isset($this->session->data['dept_name'])){
					$filter_department = $this->session->data['dept_name'];
				} else {
					$filter_department = '';
				}
			} else {
				$filter_department = '';
			}

			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				'filter_department' => $filter_department,
				'filter_departments' => $filter_departments,
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_employee->getemployees($data);

			foreach ($results as $result) {
				$json[] = array(
					'employee_id' => $result['employee_id'],
					'emp_code' => $result['emp_code'], 
					'name'            => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}
}
?>