<?php
class ControllerReportAttendance extends Controller { 
	public function index() {  
		$this->language->load('report/attendance');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			//$filter_date_start = date('Y-m-d');
			$from = date('Y-m-d');
			$filter_date_start = date('Y-m-d', strtotime($from . "-1 day"));
		}

		if (isset($this->request->get['filter_unit'])) {
			$filter_unit = $this->request->get['filter_unit'];
		} else {
			$filter_unit ='';
		}

		if (isset($this->request->get['filter_department'])) {
			$filter_department = $this->request->get['filter_department'];
		} else {
			$filter_department = '';
		}

		if (isset($this->request->get['filter_designation'])) {
			$filter_designation = $this->request->get['filter_designation'];
		} else {
			$filter_designation = '';
		}


		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}


		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['group'])) {
			$group = $this->request->get['group'];
		} else {
			$group = 0;
		}

		if (isset($this->request->get['status'])) {
			$status = $this->request->get['status'];
		} else {
			$status = 1;
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_designation'])) {
			$url .= '&filter_designation=' . $this->request->get['filter_designation'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}
		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}
		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}
		if (isset($this->request->get['status'])) {
			$url .= '&status=' . $this->request->get['status'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('report/attendance', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->load->model('report/attendance');

		$this->data['attendace'] = array();

		$data = array(
			'filter_date_start'	    	 => $filter_date_start,
			'filter_name'	     		=> $filter_name,
			'filter_name_id'	     	=> $filter_name_id,
			'filter_designation'		 => $filter_designation,
			'filter_unit'				 => $filter_unit,
			'filter_department'			 => $filter_department,
			'group'					 	=> $group,
			'status'			     	=> $status, 
			'start'                  	=> ($page - 1) * 7000,
			'limit'                  	=> 7000
		);

		if($url == ''){
			$this->data['generate'] = $this->url->link('transaction/transaction/generate_filter_date', 'token=' . $this->session->data['token'] . '&filter_date_start=' . $filter_date_start, 'SSL');
		} else {
			$this->data['generate'] = $this->url->link('transaction/transaction/generate_filter_date', 'token=' . $this->session->data['token'] . $url, 'SSL');
		}

		$this->data['export'] = $this->url->link('report/attendance/export', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$start_time = strtotime($filter_date_start);
		$compare_time = strtotime(date('Y-m-d'));

		$results = array();
		if($start_time <= $compare_time) {
			if($data['status'] == 2){
				$tran_data = $this->model_report_attendance->getclose_status($data);
				if(isset($tran_data[0]['transaction_id'])){
					$results = $this->model_report_attendance->getAttendance($data);
				} else {
					$this->data['warning'] = 'Please Process the Day before getting absent report';
				}
			} else {
				$results = $this->model_report_attendance->getAttendance($data);
			}
		} else {
			
			$results = array();	
		}

		foreach($results as $rkey => $rvalue){
		//	echo'<pre>';print_r($rvalue);exit;

			$query = $this->db->query("SELECT * FROM ".DB_PREFIX."leave_transaction WHERE `emp_id` = '".$rvalue['emp_id']."' AND `date` = '".$rvalue['date']."' AND (`p_status` = '0' OR `p_status` = '1') AND `a_status` = '1' ");
			if($query->num_rows > 0){
				$leave_data = $query->row;
				if($leave_data['type'] != ''){
					$results[$rkey]['leave_status'] = 1;
					if($leave_data['type'] == 'F'){
						$results[$rkey]['firsthalf_status'] = $leave_data['leave_type'];
						$results[$rkey]['secondhalf_status'] = $leave_data['leave_type'];
					} elseif($leave_data['type'] == '1'){
						$results[$rkey]['firsthalf_status'] = $leave_data['leave_type'];
						if($rvalue['halfday_status'] != 0){
							$results[$rkey]['secondhalf_status'] = 'Half Day';
						} elseif($results[$rkey]['secondhalf_status'] == 1){
							$results[$rkey]['secondhalf_status'] = 'Present';
						} elseif($results[$rkey]['secondhalf_status'] == 0){
							$results[$rkey]['secondhalf_status'] = 'Absent';
						}  
					}  elseif($leave_data['type'] == '2'){
						$results[$rkey]['secondhalf_status'] = $leave_data['leave_type'];
						if($rvalue['halfday_status'] != 0){
							$results[$rkey]['firsthalf_status'] = 'Half Day';
						} elseif($results[$rkey]['firsthalf_status'] == 1){
							$results[$rkey]['firsthalf_status'] = 'Present';
						} elseif($results[$rkey]['firsthalf_status'] == 0){
							$results[$rkey]['firsthalf_status'] = 'Absent';
						}
					}
				} else {
					$results[$rkey]['leave_status'] = 1;
					$results[$rkey]['firsthalf_status'] = $leave_data['leave_type'];
					$results[$rkey]['secondhalf_status'] = $leave_data['leave_type'];
				}
			} else {
				$results[$rkey]['leave_status'] = 0;
			}
			$results[$rkey]['remove_href'] = $this->url->link('report/attendance/remove_transaction', 'token=' . $this->session->data['token'] . '&transaction_id=' . $rvalue['transaction_id'].$url, 'SSL');
		}

		$this->data['results'] = $results;
		// echo'<pre>';
		// 	print_r($results);
		// 	exit;

	
		$unit_datas = $this->model_report_attendance->getlocation_list();
		$unit_data = array();
		$unit_data['0'] = 'All';
		foreach ($unit_datas as $dkey => $dvalue) {
			$unit_data[$dvalue['unit_id']] = $dvalue['unit'];
		}
		// echo '<pre>';
		// print_r($unit_data);
		// exit;
		$this->data['unit_data'] = $unit_data;
		
		
		$department_datas = $this->model_report_attendance->getdepartment_list();
		$department_data = array();
		$department_data['0'] = 'All';
		foreach ($department_datas as $dkey => $dvalue) {

			$department_data[$dvalue['department_id']] = $dvalue['department'];
		}
		// echo '<pre>';
		// print_r($department_data);
		// exit;
		$this->data['department_data'] = $department_data;

		
		$designation_datas = $this->model_report_attendance->getdesignation_list();
		$designation_data = array();
		$designation_data['0'] = 'All';
		foreach ($designation_datas as $dkey => $dvalue) {
			$designation_data[$dvalue['designation_id']] = $dvalue['designation'];
		}
		
		$this->data['designation_data'] = $designation_data;

		$group_datas = $this->model_report_attendance->getgroup_list();
		$group_data = array();
		$group_data['0'] = 'All';
		foreach ($group_datas as $gkey => $gvalue) {
			$group_data[$gvalue['group']] = $gvalue['group'];
		}
		
		$this->data['group_data'] = $group_data;


		$statuses = array(
			'0' => 'All',
			'1' => 'Present',
			'2' => 'Absent'
		);
		$this->data['statuses'] = $statuses;
		
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_all_status'] = $this->language->get('text_all_status');

		
		$this->data['entry_date_start'] = $this->language->get('entry_date_start');
		$this->data['entry_date_end'] = $this->language->get('entry_date_end');
		
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['button_export'] = $this->language->get('button_export');

		$this->data['token'] = $this->session->data['token'];

		if(isset($this->data['warning'])){
			$this->data['error_warning'] = $this->data['warning'];
		} elseif(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_designation'])) {
			$url .= '&filter_designation=' . $this->request->get['filter_designation'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}


		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}
		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}
		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}
		if (isset($this->request->get['status'])) {
			$url .= '&status=' . $this->request->get['status'];
		}

		$this->data['filter_date_start'] = $filter_date_start;
		$this->data['filter_name'] = $filter_name;
		$this->data['filter_name_id'] = $filter_name_id;
		$this->data['filter_designation'] = $filter_designation;
		$this->data['filter_unit'] = $filter_unit;
		$this->data['filter_department'] = $filter_department;
		$this->data['group'] = $group;
		$this->data['status'] = $status;

		$this->template = 'report/attendance.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	public function export(){
		$this->language->load('report/attendance');
		$this->load->model('report/common_report');
		$this->load->model('report/attendance');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			//$filter_date_start = date('Y-m-d');
			$from = date('Y-m-d');
			$filter_date_start = date('Y-m-d', strtotime($from . "-1 day"));
		}

		if (isset($this->request->get['filter_unit'])) {
			$filter_unit = $this->request->get['filter_unit'];
		} else {
			$filter_unit ='';
		}

		if (isset($this->request->get['filter_department'])) {
			$filter_department = $this->request->get['filter_department'];
		} else {
			$filter_department = '';
		}

		if (isset($this->request->get['filter_designation'])) {
			$filter_designation = $this->request->get['filter_designation'];
		} else {
			$filter_designation = '';
		}


		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}


		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} else {
			$filter_name_id = '';
		}

		


		if (isset($this->request->get['group'])) {
			$group = $this->request->get['group'];
		} else {
			$group = 0;
		}

		if (isset($this->request->get['status'])) {
			$status = $this->request->get['status'];
		} else {
			$status = 1;
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_designation'])) {
			$url .= '&filter_designation=' . $this->request->get['filter_designation'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}
		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}
		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}
		if (isset($this->request->get['status'])) {
			$url .= '&status=' . $this->request->get['status'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('report/attendance', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->load->model('report/attendance');

		$this->data['attendace'] = array();

		$data = array(
			'filter_date_start'	     => $filter_date_start,
			'filter_name'	     => $filter_name,
			'filter_name_id'	     => $filter_name_id,
			'filter_designation'			 => $filter_designation,
			'filter_unit'					 => $filter_unit,
			'filter_department'			 => $filter_department,
			'group'					 => $group,
			'status'			     => $status, 
			'start'                  => ($page - 1) * 7000,
			'limit'                  => 7000
		);

		if($url == ''){
			$this->data['generate'] = $this->url->link('transaction/transaction/generate_filter_date', 'token=' . $this->session->data['token'] . '&filter_date_start=' . $filter_date_start, 'SSL');
		} else {
			$this->data['generate'] = $this->url->link('transaction/transaction/generate_filter_date', 'token=' . $this->session->data['token'] . $url, 'SSL');
		}

		$this->data['export'] = $this->url->link('report/attendance/export', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$start_time = strtotime($filter_date_start);
		$compare_time = strtotime(date('Y-m-d'));

		$results = array();
		if($start_time <= $compare_time) {
			if($data['status'] == 2){
				$tran_data = $this->model_report_attendance->getclose_status($data);
				if(isset($tran_data[0]['transaction_id'])){
					$results = $this->model_report_attendance->getAttendance($data);
				} else {
					$this->data['warning'] = 'Please Process the Day before getting absent report';
				}
			} else {
				$results = $this->model_report_attendance->getAttendance($data);
			}
		} else {
			
			$results = array();	
		}

		foreach($results as $rkey => $rvalue){

			$query = $this->db->query("SELECT * FROM ".DB_PREFIX."leave_transaction WHERE `emp_id` = '".$rvalue['emp_id']."' AND `date` = '".$rvalue['date']."' AND (`p_status` = '0' OR `p_status` = '1') AND `a_status` = '1' ");
			if($query->num_rows > 0){
				$leave_data = $query->row;
				if($leave_data['type'] != ''){
					$results[$rkey]['leave_status'] = 1;
					if($leave_data['type'] == 'F'){
						$results[$rkey]['firsthalf_status'] = $leave_data['leave_type'];
						$results[$rkey]['secondhalf_status'] = $leave_data['leave_type'];
					} elseif($leave_data['type'] == '1'){
						$results[$rkey]['firsthalf_status'] = $leave_data['leave_type'];
						if($rvalue['halfday_status'] != 0){
							$results[$rkey]['secondhalf_status'] = 'Half Day';
						} elseif($results[$rkey]['secondhalf_status'] == 1){
							$results[$rkey]['secondhalf_status'] = 'Present';
						} elseif($results[$rkey]['secondhalf_status'] == 0){
							$results[$rkey]['secondhalf_status'] = 'Absent';
						}  
					}  elseif($leave_data['type'] == '2'){
						$results[$rkey]['secondhalf_status'] = $leave_data['leave_type'];
						if($rvalue['halfday_status'] != 0){
							$results[$rkey]['firsthalf_status'] = 'Half Day';
						} elseif($results[$rkey]['firsthalf_status'] == 1){
							$results[$rkey]['firsthalf_status'] = 'Present';
						} elseif($results[$rkey]['firsthalf_status'] == 0){
							$results[$rkey]['firsthalf_status'] = 'Absent';
						}
					}
				} else {
					$results[$rkey]['leave_status'] = 1;
					$results[$rkey]['firsthalf_status'] = $leave_data['leave_type'];
					$results[$rkey]['secondhalf_status'] = $leave_data['leave_type'];
				}
			} else {
				$results[$rkey]['leave_status'] = 0;
			}
			$results[$rkey]['remove_href'] = $this->url->link('report/attendance/remove_transaction', 'token=' . $this->session->data['token'] . '&transaction_id=' . $rvalue['transaction_id'].$url, 'SSL');
		}

		$this->data['results'] = $results;
		// echo'<pre>';
		// 	print_r($results);
		// 	exit;

		
		// if($this->user->getId() == 1) {
		// 	$unit_data = array(
		// 		'' => 'All',
		// 		'Unit 31' => 'Unit 31',
		// 		'Unit 64' => 'Unit 64',
		// 		//'Mumbai' => 'Vasai',
		// 		// 'Pune' => 'Pune',
		// 		// 'Delhi' => 'Delhi',
		// 		// 'Chennai' => 'Chennai',
		// 		// 'Bangalore' => 'Bangalore',
		// 		// 'Ahmedabad' => 'Ahmedabad', 
		// 	);
		// } else if($this->user->getId() == 7) {
		// 	$unit_data = array(
		// 		'Moving' => 'Moving' 
		// 	);
		// } else if($this->user->getId() == 3) {
		// 	$unit_data = array(
		// 		'Mumbai' => 'Mumbai' 
		// 	);
		// } else if($this->user->getId() == 4) {
		// 	$unit_data = array(
		// 		'Pune' => 'Pune' 
		// 	);
		// } else {
		// 	$unit_data = array(
		// 		'' => 'All',
		// 		'Mumbai' => 'Mumbai',
		// 		'Pune' => 'Pune',
		// 		'Delhi' => 'Delhi',
		// 		'Chennai' => 'Chennai',
		// 		'Bangalore' => 'Bangalore',
		// 		'Ahmedabad' => 'Ahmedabad', 
		// 	);
		// }
		// /*		
		// $unit_data = array(
		// 	'0' => 'All',
		// 	'Mumbai' => 'Mumbai',
		// 	'Pune' => 'Pune',
		// 	'Moving' => 'Moving' 
		// );
		// */

		//$this->data['unit_data'] = $unit_data;

		$unit_datas = $this->model_report_attendance->getlocation_list();
		$unit_data = array();
		$unit_data['0'] = 'All';
		foreach ($unit_datas as $dkey => $dvalue) {
			$unit_data[$dvalue['unit_id']] = $dvalue['unit'];
		}
		// echo '<pre>';
		// print_r($unit_data);
		// exit;
		$this->data['unit_data'] = $unit_data;
		
		
		$department_datas = $this->model_report_attendance->getdepartment_list();
		$department_data = array();
		$department_data['0'] = 'All';
		foreach ($department_datas as $dkey => $dvalue) {

			$department_data[$dvalue['department_id']] = $dvalue['department'];
		}
		// echo '<pre>';
		// print_r($department_data);
		// exit;
		$this->data['department_data'] = $department_data;

		
		$designation_datas = $this->model_report_attendance->getdesignation_list();
		$designation_data = array();
		$designation_data['0'] = 'All';
		foreach ($designation_datas as $dkey => $dvalue) {
			$designation_data[$dvalue['designation_id']] = $dvalue['designation'];
		}
		
		$this->data['designation_data'] = $designation_data;

		$group_datas = $this->model_report_attendance->getgroup_list();
		$group_data = array();
		$group_data['0'] = 'All';
		foreach ($group_datas as $gkey => $gvalue) {
			$group_data[$gvalue['group']] = $gvalue['group'];
		}
		
		$this->data['group_data'] = $group_data;


		$statuses = array(
			'0' => 'All',
			'1' => 'Present',
			'2' => 'Absent'
		);
		$this->data['statuses'] = $statuses;
		
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_all_status'] = $this->language->get('text_all_status');

		
		$this->data['entry_date_start'] = $this->language->get('entry_date_start');
		$this->data['entry_date_end'] = $this->language->get('entry_date_end');
		
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['button_export'] = $this->language->get('button_export');

		$this->data['token'] = $this->session->data['token'];

		if(isset($this->data['warning'])){
			$this->data['error_warning'] = $this->data['warning'];
		} elseif(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_designation'])) {
			$url .= '&filter_designation=' . $this->request->get['filter_designation'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}


		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}
		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}
		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}
		if (isset($this->request->get['status'])) {
			$url .= '&status=' . $this->request->get['status'];
		}

		$this->data['filter_date_start'] = $filter_date_start;
		$this->data['filter_name'] = $filter_name;
		$this->data['filter_name_id'] = $filter_name_id;
		$this->data['filter_designation'] = $filter_designation;
		$this->data['filter_unit'] = $filter_unit;
		$this->data['filter_department'] = $filter_department;
		$this->data['group'] = $group;
		$this->data['status'] = $status;

		
		if($results){
			//$month = date("F", mktime(0, 0, 0, $filter_month, 10));
			if($status == 1 || $status == 0){
				$statuss = 'Present Report';
				$statusss = 'Present_Report';
			} else {
				$statuss = 'Absent Report';
				$statusss = 'Absent_Report';
			}
			$template = new Template();		
			$template->data['results'] = $results;
			$template->data['filter_date_start'] = $filter_date_start;
			$template->data['filter_department'] = $department;
			$template->data['status'] = $status;
			$template->data['title'] = $statuss;
			if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
				$template->data['base'] = HTTPS_SERVER;
			} else {
				$template->data['base'] = HTTP_SERVER;
			}
			$html = $template->fetch('report/attendance_html.tpl');
			//echo $html;exit;
			// header('Content-type: text/html');
			// header('Content-Disposition: attachment; filename='.$filename);
			//echo $html;exit;
			$filename = $statusss."_".$filter_date_start;
			header("Content-Type: application/vnd.ms-excel; charset=utf-8");
			header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false);
			echo $html;
			exit;		
		} else {
			$this->session->data['warning'] = 'No Data Found';
			$this->redirect($this->url->link('report/attendance', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}
	}

	public function remove_transaction(){
		$this->language->load('report/attendance');
		$this->load->model('report/common_report');
		$this->load->model('report/attendance');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			//$filter_date_start = date('Y-m-d');
			$from = date('Y-m-d');
			$filter_date_start = date('Y-m-d', strtotime($from . "-1 day"));
		}

		if (isset($this->request->get['unit'])) {
			$unit = $this->request->get['unit'];
		} else {
			$unit = 0;
		}

		if (isset($this->request->get['department'])) {
			$department = html_entity_decode($this->request->get['department']);
		} else {
			$department = 0;
		}

		if (isset($this->request->get['group'])) {
			$group = $this->request->get['group'];
		} else {
			$group = 0;
		}

		if (isset($this->request->get['status'])) {
			$status = $this->request->get['status'];
		} else {
			$status = 1;
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		if (isset($this->request->get['transaction_id'])) {
			$transaction_id = $this->request->get['transaction_id'];
		} else {
			$transaction_id = 0;
		}

		$sql = "DELETE FROM `oc_transaction` WHERE `transaction_id` = '".$transaction_id."'";
		//echo $sql;exit;
		$this->db->query($sql);

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}
		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}
		if (isset($this->request->get['status'])) {
			$url .= '&status=' . $this->request->get['status'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->session->data['success'] = 'Transaction Removed Succesfully';
		$this->redirect($this->url->link('report/attendance', 'token=' . $this->session->data['token'].$url, 'SSL'));
	}
}
?>
