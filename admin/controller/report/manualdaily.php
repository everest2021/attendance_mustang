<?php
class ControllerReportManualdaily extends Controller { 
	public function index() {  
		$this->language->load('report/manualdaily');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			//$filter_date_start = date('Y-m-d');
			$from = date('Y-m-d');
			$filter_date_start = date('Y-m-01');
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			//$filter_date_start = date('Y-m-d');
			$from = date('Y-m-d');
			$filter_date_end = date('Y-m-d', strtotime($from));
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = html_entity_decode($this->request->get['filter_name']);
		} else {
			$filter_name = "";
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = html_entity_decode($this->request->get['filter_name_id']);
		} else {
			$filter_name_id = "";
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('report/manualdaily', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->load->model('report/manualdaily');
		$this->load->model('catalog/shift');

		$data['attendace'] = array();

		$data = array(
			'filter_date_start'	     => $filter_date_start,
			'filter_date_end'	     => $filter_date_end,
			'filter_name'	    	 => $filter_name,
			'filter_name_id'	     => $filter_name_id,
			'start'                  => ($page - 1) * 7000,
			'limit'                  => 7000
		);
		$this->data['action'] = $this->url->link('report/manualdaily/update_transaction', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$this->data['dailyreports'] = array();
		if(isset($this->request->get['once'])){
			$results = $this->model_report_manualdaily->getAttendance($data);
			$counttime = '0'; $countoff = '0'; $countabsent = '0';$countpresent = '0'; $countleave = '0';
			$countholiday = '0';$total_time='0';$new_time ='0';
			foreach ($results as $result) {
				if($result['leave_status'] == 1) { 
					$status = 'leave';
				} elseif($result['weekly_off'] != '0') { 
					$status = 'Weekly Off';
					$shift_id = 'WO';
				} elseif($result['holiday_id'] != '0') {
					$status = 'Holiday';
					$shift_id = 'HLD';
				} elseif($result['halfday_status'] != '0') {
					$status = 'Half day';
					$shift_id = 'HD';
				} elseif($result['present_status'] !='0') {
					$shift_datas_sql = "SELECT * FROM `oc_shift` WHERE `in_time` = '".$result['shift_intime']."' AND `out_time` = '".$result['shift_outtime']."' ";
					$shift_data_datas = $this->db->query($shift_datas_sql);
					$shift_id = 0;
					if($shift_data_datas->num_rows > 0){
						$shift_id = $shift_data_datas->row['shift_id'];
					}
					$status = 'Present';
				} elseif($result['absent_status'] !='0') {
					$shift_datas_sql = "SELECT * FROM `oc_shift` WHERE `in_time` = '".$result['shift_intime']."' AND `out_time` = '".$result['shift_outtime']."' ";
					$shift_data_datas = $this->db->query($shift_datas_sql);
					$shift_id = 0;
					if($shift_data_datas->num_rows > 0){
						$shift_id = $shift_data_datas->row['shift_id'];
					}
					$status = '<b>Absent</b>';
				}
				if($result['act_intime_manipulated'] != '00:00:00'){
					$act_intime = $result['act_intime_manipulated'];
				} else {
					$act_intime = $result['act_intime'];
				}
				if($result['act_outtime_manipulated'] != '00:00:00'){
					$act_outtime = $result['act_outtime_manipulated'];
				} else {
					$act_outtime = $result['act_outtime'];
				}
				$emp_data = $this->db->query("SELECT `emp_code` FROM `oc_employee` WHERE `employee_id` = '".$result['emp_id']."' ")->row;
				$this->data['dailyreports'][] = array(
					'transaction_id'    => $result['transaction_id'],
					'emp_name'    => $result['emp_name'],
					'emp_id'   => $emp_data['emp_code'],
					'department'   => $result['department'],
					'date'		 => $result['date'],
					'shift_intime' => $result['shift_intime'],
					'shift_outtime' => $result['shift_outtime'],
					'act_intime' => $act_intime,
					'act_outtime' => $act_outtime,
					'status_name'	=> $status,
					'shift_id'	=> $shift_id,
				);
			}
		}

		// echo '<pre>';
		// print_r($this->data['dailyreports']);
		// exit;

		$shift_data = array();
		$shift_datas = $this->model_catalog_shift->getshifts($shift_data);
		
		$shift_data[1]['shift_id'] = 'WO';
		$shift_data[1]['shift_name'] = 'Weekly Off';

		$shift_data[2]['shift_id'] = 'HLD';
		$shift_data[2]['shift_name'] = 'Holiday';

		// $shift_data[3]['shift_id'] = 'HD';
		// $shift_data[3]['shift_name'] = 'Half Day';

		// $shift_data[4]['shift_id'] = 'COF';
		// $shift_data[4]['shift_name'] = 'Compensatory Off';

		$ckey = 3;
		foreach ($shift_datas as $skey => $svalue) {
			$shift_data[$ckey]['shift_id'] = $svalue['shift_id'];
			$shift_data[$ckey]['shift_name'] = $svalue['name'];
			$ckey ++;
		}
		$this->data['shift_data'] = $shift_data;
		
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_all_status'] = $this->language->get('text_all_status');

		
		$this->data['entry_date_start'] = $this->language->get('entry_date_start');
		$this->data['entry_date_end'] = $this->language->get('entry_date_end');
		
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['button_export'] = $this->language->get('button_export');

		$this->data['token'] = $this->session->data['token'];

		if(isset($this->data['warning'])){
			$this->data['error_warning'] = $this->data['warning'];
		} elseif(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}
		
		$this->data['token'] = $this->session->data['token'];

		$this->data['filter_date_start'] = $filter_date_start;
		$this->data['filter_date_end'] = $filter_date_end;
		$this->data['filter_name'] = $filter_name;
		$this->data['filter_name_id'] = $filter_name_id;
		
		$this->template = 'report/manualdaily.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	public function getshiftdata() {
		$jsondata = array();
		$jsondata['status'] = 0;
		//$this->log->write(print_r($this->request->get,true));
		$shift_data = array();
		if(isset($this->request->get['shift_id'])){
			$shfit_id = $this->request->get['shift_id'];
	        $this->load->model('transaction/transaction');
			$shift_data = $this->model_transaction_transaction->getshiftdata($this->request->get['shift_id']);
			if(isset($shift_data['shift_id'])){
				$shift_data['in_time'] = $shift_data['in_time'];
				$shift_data['out_time'] = $shift_data['out_time'];
				$jsondata['shift_data'] = $shift_data;
				$jsondata['status'] = 1;
			} elseif(isset($this->request->get['transaction_id'])){
				$trans_data = $this->db->query("SELECT * FROM `oc_transaction` WHERE `transaction_id` = '".$this->request->get['transaction_id']."' ")->row;
				$shift_data['in_time'] = $trans_data['shift_intime'];
				$shift_data['out_time'] = $trans_data['shift_outtime'];
				$jsondata['shift_data'] = $shift_data;
				$jsondata['status'] = 1;
			} else{
				$filter_date_start = $this->request->get['dot'];
				$emp_id = $this->request->get['emp_id'];
				$day_date = date('j', strtotime($filter_date_start));
				$day = date('j', strtotime($filter_date_start));
				$month = date('n', strtotime($filter_date_start));
				$year = date('Y', strtotime($filter_date_start));

				$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$emp_id."' AND `month` = '".$month."' AND `year` = '".$year."' ";
				$shift_schedule = $this->db->query($update3)->row;
				$schedule_raw = explode('_', $shift_schedule[$day_date]);
				if(!isset($schedule_raw[2])){
					$schedule_raw[2] = 1;
				}
				$shift_id = $schedule_raw[2];
				$shift_data = $this->model_transaction_transaction->getshiftdata($shift_id);
				if(isset($shift_data['shift_id'])){
					$shift_data['in_time'] = $shift_data['in_time'];
					$shift_data['out_time'] = $shift_data['out_time'];
					$jsondata['shift_data'] = $shift_data;
					$jsondata['status'] = 1;
				}
			}
		}
    	$this->response->setOutput(Json_encode($jsondata));
	}

	public function update_transaction(){
		$this->language->load('report/attendance');
		$this->load->model('report/common_report');
		$this->load->model('report/attendance');

		$this->document->setTitle($this->language->get('heading_title'));

		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
			// echo '<pre>';
			// print_r($this->request->post);
			// exit;
			foreach($this->request->post['attendance_data'] as $dkey => $data){
				$change_status = 0;
				if($data['actual_intime'] != $data['hidden_actual_intime']){
					$change_status = 1;
				} elseif($data['actual_outtime'] != $data['hidden_actual_outtime']){
					$change_status = 1;
				} elseif($data['shift_intime'] != $data['hidden_shift_intime']){
					$change_status = 1;
				} elseif($data['shift_outtime'] != $data['hidden_shift_outtime']){
					$change_status = 1;
				} elseif($data['shift_id'] != $data['hidden_shift_id']){
					$change_status = 1;
				}

				if($change_status == 1){

					if($data['actual_intime'] == '' || $data['actual_intime'] == '0') {
						$data['actual_intime'] = '00:00:00';
					}
					if($data['actual_outtime'] == '' || $data['actual_outtime'] == '0') {
						$data['actual_outtime'] = '00:00:00';
					}
					if($data['dot'] != ''){
						$data['dot'] = date('Y-m-d', strtotime($data['dot']));
						$data['day'] = date('j', strtotime($data['dot']));
						$data['month'] = date('n', strtotime($data['dot']));
						$data['year'] = date('Y', strtotime($data['dot']));
					} else {
						$data['dot'] = date('Y-m-d');
						$data['day'] = date('j');
						$data['month'] = date('n');
						$data['year'] = date('Y');
					}
					$day = $data['day'];
					$month = $data['month'];
					$year = $data['year'];

					$act_intime = $data['actual_intime'];
					$act_outtime = $data['actual_outtime'];
					$shift_intime = $data['shift_intime'];
					$shift_outtime = $data['shift_outtime'];
					$punch_date = $data['dot'];
					$punch_in_date = date('Y-m-d', strtotime($data['dot']));
					$punch_out_date = date('Y-m-d', strtotime($data['dot']));
					
					$first_half = 0;
					$second_half = 0;
					$abnormal_status = 0;
					$late_time = '00:00:00';
					$working_time = '00:00:00';
					$early_time = '00:00:00';
					$overtime = '00:00:00';
					if($act_intime != '00:00:00'){
						$start_date = new DateTime($punch_in_date.' '.$shift_intime);
						$since_start = $start_date->diff(new DateTime($punch_in_date.' '.$act_intime));
						if($since_start->h > 12){
							$start_date = new DateTime($punch_in_date.' '.$shift_intime);
							$since_start = $start_date->diff(new DateTime($punch_out_date.' '.$act_intime));
						}
						$late_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
						if($since_start->invert == 1){
							$late_time = '00:00:00';
						}
						if($act_outtime != '00:00:00'){
							$first_half = 1;
							$second_half = 1;
						} else {
							$first_half = 0;
							$second_half = 0;
						}
						//echo 'out';exit;
						$working_time = '00:00:00';
						$early_time = '00:00:00';
						$overtime = '00:00:00';
						if($abnormal_status == 0){ //if abnormal status is zero calculate further 
							$early_time = '00:00:00';
							if($act_outtime != '00:00:00'){ //for getting early time i.e difference between shiftouttime and sctouttime 
								$start_date = new DateTime($punch_in_date.' '.$shift_outtime);
								$since_start = $start_date->diff(new DateTime($punch_out_date.' '.$act_outtime));
								if($since_start->h > 12){
									$start_date = new DateTime($punch_out_date.' '.$shift_outtime);
									$since_start = $start_date->diff(new DateTime($punch_in_date.' '.$act_outtime));
								}
								$early_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;					
								if($since_start->invert == 0){
									$early_time = '00:00:00';
								}

								$overtime = $since_start->h.':'.$since_start->i.':'.$since_start->s;					
								if($since_start->invert == 1){
									$overtime = '00:00:00';
								}
							}					
							$working_time = '00:00:00';
							if($act_outtime != '00:00:00'){//for getting working time
								$start_date = new DateTime($punch_in_date.' '.$act_intime);
								$since_start = $start_date->diff(new DateTime($punch_out_date.' '.$act_outtime));
								$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
							}
						} else {
							$first_half = 0;
							$second_half = 0;
						}
					} else {
						$first_half = 0;
						$second_half = 0;
					}
					
					$trans_data_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$data['e_name_id']."' AND `date` = '".$punch_in_date."' ";
					$trans_data_res = $this->db->query($trans_data_sql)->row;

					$leave_data_sql = "SELECT `leave_type`, `type` FROM `oc_leave_transaction` WHERE `emp_id` = '".$data['e_name_id']."' AND `date` = '".$punch_in_date."' AND `p_status` = '1' AND `a_status` = '1' ";
					$leave_data_res = $this->db->query($leave_data_sql);
					$leave_in = 0;
					if($leave_data_res->num_rows > 0){
						if($leave_data_res->row['type'] == ''){
							$leave_in = 1;
							$first_half = $leave_data_res->row['leave_type'];
							$second_half = $leave_data_res->row['leave_type'];
						} else {
							if($leave_data_res->row['type'] == 'F'){
								$leave_in = 1;
								$first_half = $leave_data_res->row['leave_type'];
								$second_half = $leave_data_res->row['leave_type'];
							} elseif($leave_data_res->row['type'] == '1'){
								$leave_in = 2;
								$first_half = $leave_data_res->row['leave_type'];
							} elseif($leave_data_res->row['type'] == '2'){
								$leave_in = 3;
								$second_half = $leave_data_res->row['leave_type'];
							}
						}
					}

					if($first_half == '1' && $second_half == '1'){
						$present_status = 1;
						$absent_status = 0;
					} elseif($first_half == '1' && $second_half == '0'){
						$present_status = 0.5;
						$absent_status = 0.5;
					} elseif($first_half == '0' && $second_half == '1'){
						$present_status = 0.5;
						$absent_status = 0.5;
					} elseif($leave_in == '1'){
						$present_status = 0;
						$absent_status = 0;
					} elseif($leave_in == '2'){
						if($second_half == '0'){
							$present_status = 0;
							$absent_status = 0.5;
						} else {
							$present_status = 0.5;
							$absent_status = 0;
						}
					} elseif($leave_in == '3'){
						if($first_half == '0'){
							$present_status = 0;
							$absent_status = 0.5;
						} else {
							$present_status = 0.5;
							$absent_status = 0;
						}
					} else {
						$present_status = 0;
						$absent_status = 1;
					}

					if($data['shift_id'] == 'WO'){
						$late_time = '00:00:00';
						$early_time = '00:00:00';
						$present_status = 0;
						$absent_status = 0;
						$weekly_off = 1;
						$compli = 0;
						$holiday = 0;
						$halfday = 0;
						$first_half = 'WO';
						$second_half = 'WO';
						$day_info_id = 'W_1';
					} elseif($data['shift_id'] == 'HLD'){
						$late_time = '00:00:00';
						$early_time = '00:00:00';
						$present_status = 0;
						$absent_status = 0;
						$weekly_off = 0;
						$holiday = 1;
						$compli = 0;
						$halfday = 0;
						$first_half = 'HLD';
						$second_half = 'HLD';
						$day_info_id = 'H_1';
					} else {
						$weekly_off = 0;
						$holiday = 0;
						$halfday = 0;
						$compli = 0;
						$day_info_id = 'S_'.$data['shift_id'];
					}
					$data['insert'] = '0';
					if($data['insert'] == '0'){
						$sql = "UPDATE `oc_transaction` SET `date` = '".$punch_in_date."', `date_out` = '".$punch_out_date."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime_manipulated` = '".$act_intime."', `act_outtime_manipulated` = '".$act_outtime."', `late_time_manipulated` = '".$late_time."', `early_time_manipulated` = '".$early_time."', `over_time_manipulated` = '".$overtime."', `working_time_manipulated` = '".$working_time."', `weekly_off` = '".$weekly_off."', `holiday_id` = '".$holiday."', `halfday_status` = '".$halfday."', `firsthalf_status_manipulated` = '".$first_half."', `secondhalf_status_manipulated` = '".$second_half."', `abnormal_status` = '".$abnormal_status."', `present_status_manipulated` = '".$present_status."', `absent_status_manipulated` = '".$absent_status."', `compli_status` = '".$compli."', `manual_status` = '1' WHERE `transaction_id` = '".$data['transaction_id']."' ";
						$this->db->query($sql);	
						//$this->log->write($sql);	
					} else {
						$this->log->write('in else');
						$emp_data = $this->model_transaction_transaction->getEmployees_dat($data['e_name_id']);
						if(isset($emp_data['emp_code'])){
							$department = $emp_data['department'];
							$unit = $emp_data['unit'];
							$group = $emp_data['group'];
						} else {
							$department = '';
							$unit = '';
							$group = '';
						}
						if($leave_in == 1 || $leave_in == 2 || $leave_in == 3){
							$leave_status = 1;
						} else {
							$leave_status = 0;
						}
						$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$data['e_name_id']."', `emp_name` = '".$data['e_name']."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime_manipulated` = '".$act_intime."', `act_outtime_manipulated` = '".$act_outtime."', `late_time_manipulated` = '".$late_time."', `early_time_manipulated` = '".$early_time."', `over_time_manipulated` = '".$overtime."', `working_time_manipulated` = '".$working_time."', `day` = '".$data['day']."', `month` = '".$data['month']."', `year` = '".$data['year']."', `date` = '".$punch_in_date."', `date_out` = '".$punch_out_date."', `department` = '".$this->db->escape($department)."', `unit` = '".$this->db->escape($unit)."', `group` = '".$this->db->escape($group)."', `weekly_off` = '".$weekly_off."', `holiday_id` = '".$holiday."', `halfday_status` = '".$halfday."', `firsthalf_status_manipulated` = '".$first_half."', `secondhalf_status_manipulated` = '".$second_half."', `abnormal_status` = '".$abnormal_status."', `present_status_manipulated` = '".$present_status."', `absent_status_manipulated` = '".$absent_status."', `compli_status` = '".$compli."', `leave_status` = '".$leave_status."', `manual_status` = '1' ";
						$this->db->query($sql);
					}
				}
			}
			
			$url = '';

			if (isset($this->request->get['filter_date_start'])) {
				$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
			}

			if (isset($this->request->get['filter_date_end'])) {
				$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
			}

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			$url .= '&once=1';
			$this->session->data['success'] = 'Transaction Updated Succesfully';
			$this->redirect($this->url->link('report/manualdaily', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/employee');

			if(isset($this->session->data['dept_names'])){
				$filter_departments = html_entity_decode($this->session->data['dept_names']);
				$filter_departments = "'" . str_replace(",", "','", html_entity_decode($filter_departments)) . "'";
			} else {
				$filter_departments = '';
			}

			if($filter_departments == ''){
				if(isset($this->session->data['dept_name'])){
					$filter_department = $this->session->data['dept_name'];
				} else {
					$filter_department = '';
				}
			} else {
				$filter_department = '';
			}

			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				'filter_department' => $filter_department,
				'filter_departments' => $filter_departments,
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_employee->getemployees($data);

			foreach ($results as $result) {
				$json[] = array(
					'employee_id' => $result['employee_id'],
					'emp_code' => $result['emp_code'], 
					'name'            => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}
}
?>