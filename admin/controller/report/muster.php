<?php
class ControllerReportMuster extends Controller { 
	public function index() {  
		$this->language->load('report/muster');
		$this->load->model('report/common_report');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			//$filter_date_start = date('Y-m-d');
			//$filter_date_start = date('Y-m-d', strtotime(date('Y') . '-' . date('m') . '-01'));
			//$filter_date_start = '';
			$from = date('Y-m-d');
			$filter_date_start = date('Y-m-d', strtotime($from . "-30 day"));
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['filter_type'])) {
			$filter_type = $this->request->get['filter_type'];
		} else {
			$filter_type = 0;
		}

		if (isset($this->request->get['unit'])) {
			$unit = $this->request->get['unit'];
		} else {
			$unit = 0;
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
			$filter_date_end1 = $this->model_report_common_report->getfilter_date_end($filter_date_start, $filter_name_id, $unit);
			if(strtotime($filter_date_end) > strtotime($filter_date_end1) ){
				$filter_date_end = $filter_date_end1;
			}
		} else {
			$filter_date_end = date('Y-m-d');
		}
		
		// if (isset($this->request->get['filter_month'])) {
		// 	$filter_month = $this->request->get['filter_month'];
		// } else {
		// 	$filter_month = date('n');
		// }

		// if (isset($this->request->get['filter_year'])) {
		// 	$filter_year = $this->request->get['filter_year'];
		// } else {
		// 	$filter_year = date('Y');
		// }

		if (isset($this->request->get['department'])) {
			$department = html_entity_decode($this->request->get['department']);
		} else {
			$department = 0;
		}

		if (isset($this->request->get['group'])) {
			$group = $this->request->get['group'];
		} else {
			$group = 0;
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		// if (isset($this->request->get['filter_month'])) {
		// 	$url .= '&filter_month=' . $this->request->get['filter_month'];
		// }

		// if (isset($this->request->get['filter_year'])) {
		// 	$url .= '&filter_year=' . $this->request->get['filter_year'];
		// }

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_type'])) {
			$url .= '&filter_type=' . $this->request->get['filter_type'];
		}

		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}
		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('report/muster', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->load->model('report/common_report');
		$this->load->model('report/attendance');
		$this->load->model('catalog/employee');

		$this->data['attendace'] = array();

		$data = array(
			'filter_date_start'	     => $filter_date_start,
			'filter_date_end'	     => $filter_date_end,
			// 'filter_month'	     	 => $filter_month,
			// 'filter_year'	     => $filter_year,
			'filter_name'	     	 => $filter_name,
			'filter_name_id'	     => $filter_name_id,
			'unit'					 => $unit,
			'department'			 => $department,
			'group'					 => $group, 
			'start'                  => ($page - 1) * 7000,
			'limit'                  => 7000
		);

		// $filter_date_start = $filter_year.'-'.$filter_month.'-'.'01';
		// $max_days = cal_days_in_month(CAL_GREGORIAN, $filter_month, $filter_year);
		// $filter_date_end = $this->model_report_common_report->getNextDate();
		// $filter_date_end = $filter_year.'-'.$filter_month.'-'.$max_days;

		$day = array();
        $days = $this->GetDays($filter_date_start, $filter_date_end);
        foreach ($days as $dkey => $dvalue) {
        	$dates = explode('-', $dvalue);
        	$day[$dvalue]['day'] = $dates[2];
        	$day[$dvalue]['date'] = $dvalue;
        }

        $this->data['days'] = $day;
        $final_datas = array();
		// echo '<pre>';
		// print_r($data);
		// exit;
		$act_total_count = 0;	
		$total_cnt = 0;	
		$cnt_array = array();
		$act_cnt_array = array();
		if(isset($this->request->get['once']) && $this->request->get['once'] == 1){
			$results = $this->model_report_common_report->getemployees_muster($data);
			foreach ($results as $rkey => $rvalue) {
				$performance_data = array();
				$transaction_datas = $this->model_report_common_report->gettransaction_data($rvalue['emp_code'], $data);
				
				$final_datas[$rvalue['emp_code']]['basic_data']['name'] = $rvalue['name'];
				$final_datas[$rvalue['emp_code']]['basic_data']['department'] = $rvalue['department'];
				$final_datas[$rvalue['emp_code']]['basic_data']['emp_code'] = $rvalue['emp_code'];

				$week_count = 0;
				$holiday_count = 0;
				$halfday_count = 0;
				$leave_count = 0;
				$present_count = 0;
				$absent_count = 0;
				$compli_count = 0;
				$total_cnt = 0;
				// echo '<pre>';
				// print_r($transaction_datas);
				// exit;
				if($transaction_datas){
					// echo '<pre>';
					// print_r($transaction_datas);
					// exit;
					$count_t = count($transaction_datas) - 1;
					foreach($transaction_datas as $tkey => $tvalue){
						
						// echo '<pre>';
						// print_r($tvalue);

						if($tvalue['firsthalf_status'] == '1'){
							if($tvalue['halfday_status'] != '0'){
								$firsthalf_status = 'HD';	
							} else{
								$firsthalf_status = 'P';
							}
						} elseif($tvalue['firsthalf_status'] == '0'){
							$firsthalf_status = 'A';
						} else {
							$firsthalf_status = $tvalue['firsthalf_status'];
						}

						if($tvalue['secondhalf_status'] == '1'){
							$secondhalf_status = 'P';
						} elseif($tvalue['secondhalf_status'] == '0'){
							$secondhalf_status = 'A';
						} else {
							$secondhalf_status = $tvalue['secondhalf_status'];
						}

						// echo '<pre>';
						// print_r($tvalue);
						if($tvalue['leave_status'] == 1){
							// echo 'in leave count 1';
							// echo '<br />';
							$total_cnt = $total_cnt + 1;
							if($secondhalf_status == 'COF' || $secondhalf_status == 'OD'){
								$present_count ++;
							} elseif($secondhalf_status == 'ESIC'){
								$absent_count ++;
							} else { 
								$leave_count ++;
							}
						} elseif($tvalue['leave_status'] == 0.5){
							// echo 'in leave count 0.5';
							// echo '<br />';
							if($firsthalf_status == 'COF' || $secondhalf_status == 'COF'){
								$present_count = $present_count + 0.5;
							} elseif($firsthalf_status == 'OD' || $secondhalf_status == 'OD'){
								$present_count = $present_count + 0.5;
							} elseif($firsthalf_status == 'ESIC' || $secondhalf_status == 'ESIC') {
								$absent_count = $absent_count + 0.5;	
							} else {
								$leave_count = $leave_count + 0.5;	
							}
							$total_cnt = $total_cnt + 0.5;
							if($tvalue['present_status'] == 0.5 && $tvalue['halfday_status'] == 0){
								$total_cnt = $total_cnt + 0.5;
								$present_count = $present_count + 0.5;	
							}
							if($tvalue['absent_status'] == 0.5){
								$total_cnt = $total_cnt + 0.5;
								$absent_count = $absent_count + 0.5;	
							}
							
							if($tvalue['halfday_status'] != 0 && ($firsthalf_status != 'OD' && $firsthalf_status != 'COF' && $firsthalf_status != 'PL' && $firsthalf_status != 'CL' && $firsthalf_status != 'SL')){
								//$halfday_count = $halfday_count + 0.5;
								$total_cnt = $total_cnt + 0.5;
							} elseif($tvalue['halfday_status'] != 0 && ($firsthalf_status == 'OD' || $firsthalf_status == 'COF' || $firsthalf_status == 'PL' || $firsthalf_status == 'CL' || $firsthalf_status == 'SL') ) {
								//$halfday_count = $halfday_count + 0.5;
								$total_cnt = $total_cnt + 0.5;
								$present_count = $present_count + 0.5;
							} elseif($tvalue['halfday_status'] != 0 && ($secondhalf_status != 'OD' && $secondhalf_status != 'COF' && $secondhalf_status != 'PL' && $secondhalf_status != 'CL' && $secondhalf_status != 'SL') ){
								//$halfday_count = $halfday_count + 0.5;
								$total_cnt = $total_cnt + 0.5;
							} elseif($tvalue['halfday_status'] != 0 && ($secondhalf_status == 'OD' || $secondhalf_status == 'COF' || $secondhalf_status == 'PL' || $secondhalf_status == 'CL' || $secondhalf_status == 'SL') ) {
								//$halfday_count = $halfday_count + 0.5;
								$total_cnt = $total_cnt + 0.5;
								$present_count = $present_count + 0.5;
							}
						} elseif($tvalue['on_duty'] == 1){
							// echo 'in present count 1';
							// echo '<br />';
							$total_cnt = $total_cnt + 1;
							$present_count ++;
						} elseif($tvalue['present_status'] == 1){
							// echo 'in present count 1';
							// echo '<br />';
							$total_cnt = $total_cnt + 1;
							$present_count ++;
						} elseif($tvalue['present_status'] == 0.5){
							// echo 'in present count 0.5';
							// echo '<br />';
							$total_cnt = $total_cnt + 0.5;
							$present_count = $present_count + 0.5;
							if($tvalue['absent_status'] == 0.5){
								$total_cnt = $total_cnt + 0.5;
								$absent_count = $absent_count + 0.5;
							}
						} elseif($tvalue['absent_status'] == 1){
							// echo 'in absent count 1';
							// echo '<br />';
							$total_cnt = $total_cnt + 1;
							$absent_count ++;
						} elseif($tvalue['absent_status'] == 0.5){
							// echo 'in absent count 0.5';
							// echo '<br />';
							$total_cnt = $total_cnt + 0.5;
							$absent_count = $absent_count + 0.5;
							if($tvalue['present_status'] == 0.5){
								$total_cnt = $total_cnt + 0.5;
								$present_count = $present_count + 0.5;
							}
						} elseif($tvalue['weekly_off'] != 0){
							// echo 'in weekly_off count';
							// echo '<br />';
							if($tkey == 0){
								$last_key = $tkey;
							} else {
								$last_key = $tkey - 1;
							}
							if($count_t == $tkey){
								$next_key = $tkey;
							} else {
								$next_key = $tkey + 1;	
							}
							if($transaction_datas[$last_key]['absent_status'] == '1' && $transaction_datas[$next_key]['absent_status'] == '1'){
								$absent_count ++;
								$firsthalf_status = 'A';
								$secondhalf_status = 'A';
							} else {
								//echo 'aaaaa';
								//echo '<br />';
								$week_count ++;	
							}
							$total_cnt = $total_cnt + 1;
						} elseif($tvalue['holiday_id'] != 0){
							// echo 'in holiday count';
							// echo '<br />';
							if($tkey == 0){
								$last_key = $tkey;
							} else {
								$last_key = $tkey - 1;
							}
							if($count_t == $tkey){
								$next_key = $tkey;
							} else {
								$next_key = $tkey + 1;	
							}
							if($transaction_datas[$last_key]['absent_status'] == '1' && $transaction_datas[$next_key]['absent_status'] == '1'){
								$absent_count ++;
								$firsthalf_status = 'A';
								$secondhalf_status = 'A';
							} else {
								$holiday_count ++;	
							}
							$total_cnt = $total_cnt + 1;
							//$holiday_count ++;
						} elseif($tvalue['halfday_status'] != 0){
							// echo 'in halffday count';
							// echo '<br />';
							if($tkey == 0){
								$last_key = $tkey;
							} else {
								$last_key = $tkey - 1;
							}
							if($count_t == $tkey){
								$next_key = $tkey;
							} else {
								$next_key = $tkey + 1;	
							}
							if($transaction_datas[$last_key]['absent_status'] == '1' && $transaction_datas[$next_key]['absent_status'] == '1'){
								$absent_count ++;
								$firsthalf_status = 'A';
								$secondhalf_status = 'A';
							} else {
								$present_count ++;	
							}
							$total_cnt = $total_cnt + 1;
							//$present_count ++;
							//$halfday_count ++;
						} elseif($tvalue['compli_status'] != 0){
							// echo 'in halffday count';
							// echo '<br />';
							if($tkey == 0){
								$last_key = $tkey;
							} else {
								$last_key = $tkey - 1;
							}
							if($count_t == $tkey){
								$next_key = $tkey;
							} else {
								$next_key = $tkey + 1;	
							}
							if($transaction_datas[$last_key]['absent_status'] == '1' && $transaction_datas[$next_key]['absent_status'] == '1'){
								$absent_count ++;
								$firsthalf_status = 'A';
								$secondhalf_status = 'A';
							} else {
								$present_count ++;	
							}
							$total_cnt = $total_cnt + 1;
							//$present_count ++;
							//$compli_count ++;
						}
						
						$performance_data['action'][$tvalue['date']] = array(
							'firsthalf_status' => $firsthalf_status,
							'secondhalf_status' => $secondhalf_status,
							'date' => $tvalue['date'],
						);
					}
					//exit;
					//$total_cnt = count($performance_data['action']);
					foreach ($day as $dkey => $dvalue) {
						foreach ($performance_data as $pkey => $pvalue) {
							// echo '<pre>';
							// print_r($pvalue[$dvalue['date']]['date']);
							// exit;
							if(isset($pvalue[$dvalue['date']]['date'])){
							} else {
								$performance_data['action'][$dvalue['date']] = array(
									'firsthalf_status' => '',
									'secondhalf_status' => '',
									'date' => $dvalue['date']
								);		
							}
						}
					}
					$in = 0;
					if($filter_type == 1){
						if($absent_count > 0){
							$in = 1;
						}
					} elseif($filter_type == 0){
						$in = 1;
					}
					$performance_data = $this->array_sort($performance_data['action'], 'date', SORT_ASC);
					// echo '<pre>';
					// print_r($performance_data);
					// exit;
					$act_total_count = $present_count + $absent_count + $week_count + $holiday_count + $leave_count;
					//echo $present_count;exit;
					if($in == 1){
						$cnt_array[$rvalue['status']] = $total_cnt;
						$act_cnt_array[$rvalue['status']] = $act_total_count; 
						$final_datas[$rvalue['emp_code']]['basic_data']['week_count'] = $week_count;
						$final_datas[$rvalue['emp_code']]['basic_data']['holiday_count'] = $holiday_count;
						$final_datas[$rvalue['emp_code']]['basic_data']['halfday_count'] = $halfday_count;
						$final_datas[$rvalue['emp_code']]['basic_data']['compli_count'] = $compli_count;
						$final_datas[$rvalue['emp_code']]['basic_data']['leave_count'] = $leave_count;
						$final_datas[$rvalue['emp_code']]['basic_data']['present_count'] = $present_count;
						$final_datas[$rvalue['emp_code']]['basic_data']['absent_count'] = $absent_count;
						$final_datas[$rvalue['emp_code']]['basic_data']['total_cnt'] = $total_cnt;
						$final_datas[$rvalue['emp_code']]['tran_data']['action'] = $performance_data;
					} else {
						unset($final_datas[$rvalue['emp_code']]);
					}
				} else {
					unset($final_datas[$rvalue['emp_code']]);
				}
			}
			//exit;
		}
		
		//$final_datas = array();
		$this->data['final_datas'] = $final_datas;
		
		// echo '<pre>';
		// print_r($this->data['final_datas']);
		// exit;

		$type_data = array(
			'0' => 'All',
			'1' => 'Absent',
		);

		$this->data['type_data'] = $type_data;

		$unit_data = array(
			'0' => 'All',
			'Mumbai' => 'Mumbai',
			'Pune' => 'Pune',
			'Delhi' => 'Delhi',
			'Chennai' => 'Chennai',
			'Bangalore' => 'Bangalore',
			'Ahmedabad' => 'Ahmedabad', 
		);

		$this->data['unit_data'] = $unit_data;
		
		$department_datas = $this->model_report_attendance->getdepartment_list();
		$department_data = array();
		$department_data['0'] = 'All';
		foreach ($department_datas as $dkey => $dvalue) {
			$department_data[$dvalue['department']] = $dvalue['department'];
		}
		$this->data['department_data'] = $department_data;
		
		$group_datas = $this->model_report_attendance->getgroup_list();
		$group_data = array();
		$group_data['0'] = 'All';
		$group_data['1'] = 'All WITHOUT OFFICIALS';
		foreach ($group_datas as $gkey => $gvalue) {
			$group_data[$gvalue['group']] = $gvalue['group'];
		}
		$this->data['group_data'] = $group_data;
		
		if(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}
		// echo '<pre>';
		// print_r($cnt_array);
		// exit;
		// echo count($day);
		// echo '<br />';
		// echo $total_cnt;
		// exit;
		
		$this->data['count_mismatch'] = '0';
		// if(isset($cnt_array[1]) && $cnt_array[1] > '0' && count($day) != $cnt_array[1]){
		// 	$this->data['count_mismatch'] = '1';
		// 	$this->data['error_warning'] = 'Days Does not Match';			
		// } elseif(isset($act_cnt_array[1]) && $act_cnt_array[1] > '0' && $act_cnt_array[1] != count($day)){
		// 	$this->data['count_mismatch'] = '1';
		// 	$this->data['error_warning'] = 'Total of Present + Absent + Weekly off + Holiday + Leave + Absent and Days Does not Match';
		// }

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_all_status'] = $this->language->get('text_all_status');

		
		$this->data['entry_date_start'] = $this->language->get('entry_date_start');
		$this->data['entry_date_end'] = $this->language->get('entry_date_end');
		
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['button_export'] = $this->language->get('button_export');

		$this->data['token'] = $this->session->data['token'];

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		// if (isset($this->request->get['filter_month'])) {
		// 	$url .= '&filter_month=' . $this->request->get['filter_month'];
		// }

		// if (isset($this->request->get['filter_year'])) {
		// 	$url .= '&filter_year=' . $this->request->get['filter_year'];
		// }

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_type'])) {
			$url .= '&filter_type=' . $this->request->get['filter_type'];
		}

		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}
		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}

		$months = array(
			'01' => 'January',
			'02' => 'Feburary',
			'03' => 'March',
			'04' => 'April',
			'05' => 'May',
			'06' => 'June',
			'07' => 'July',
			'08' => 'August',
			'09' => 'September',
			'10' => 'October',
			'11' => 'November',
			'12' => 'December'
		);

		$this->data['months'] = $months;

		$this->data['filter_date_start'] = $filter_date_start;
		$this->data['filter_date_end'] = $filter_date_end;
		// $this->data['filter_month'] = $filter_month;
		// $this->data['filter_year'] = $filter_year;
		$this->data['filter_name'] = $filter_name;
		$this->data['filter_name_id'] = $filter_name_id;
		$this->data['filter_type'] = $filter_type;
		$this->data['unit'] = $unit;
		$this->data['department'] = $department;
		$this->data['group'] = $group;

		$this->template = 'report/muster.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	function array_sort($array, $on, $order=SORT_ASC){

		$new_array = array();
		$sortable_array = array();

		if (count($array) > 0) {
			foreach ($array as $k => $v) {
				if (is_array($v)) {
					foreach ($v as $k2 => $v2) {
						if ($k2 == $on) {
							$sortable_array[$k] = $v2;
						}
					}
				} else {
					$sortable_array[$k] = $v;
				}
			}

			switch ($order) {
				case SORT_ASC:
					asort($sortable_array);
					break;
				case SORT_DESC:
					arsort($sortable_array);
					break;
			}

			foreach ($sortable_array as $k => $v) {
				$new_array[$k] = $array[$k];
			}
		}

		return $new_array;
	}

	public function export(){
		$this->language->load('report/muster');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			//$filter_date_start = date('Y-m-d');
			//$filter_date_start = date('Y-m-d', strtotime(date('Y') . '-' . date('m') . '-01'));
			//$filter_date_start = '';
			$from = date('Y-m-d');
			$filter_date_start = date('Y-m-d', strtotime($from . "-30 day"));
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = date('Y-m-d');
		}
		
		// if (isset($this->request->get['filter_month'])) {
		// 	$filter_month = $this->request->get['filter_month'];
		// } else {
		// 	$filter_month = date('n');
		// }

		// if (isset($this->request->get['filter_year'])) {
		// 	$filter_year = $this->request->get['filter_year'];
		// } else {
		// 	$filter_year = date('Y');
		// }

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['unit'])) {
			$unit = $this->request->get['unit'];
		} else {
			$unit = 0;
		}

		if (isset($this->request->get['department'])) {
			$department = html_entity_decode($this->request->get['department']);
		} else {
			$department = 0;
		}

		if (isset($this->request->get['group'])) {
			$group = $this->request->get['group'];
		} else {
			$group = 0;
		}

		if (isset($this->request->get['filter_type'])) {
			$filter_type = $this->request->get['filter_type'];
		} else {
			$filter_type = 0;
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		// if (isset($this->request->get['filter_month'])) {
		// 	$url .= '&filter_month=' . $this->request->get['filter_month'];
		// }

		// if (isset($this->request->get['filter_year'])) {
		// 	$url .= '&filter_year=' . $this->request->get['filter_year'];
		// }

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_type'])) {
			$url .= '&filter_type=' . $this->request->get['filter_type'];
		}

		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}
		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$url .= '&once=1';

		$this->load->model('report/common_report');
		$this->load->model('report/attendance');
		$this->load->model('catalog/employee');

		$this->data['attendace'] = array();

		$data = array(
			'filter_date_start'	     => $filter_date_start,
			'filter_date_end'	     => $filter_date_end,
			// 'filter_month'	     	 => $filter_month,
			// 'filter_year'	     => $filter_year,
			'filter_name'	     	 => $filter_name,
			'filter_name_id'	     => $filter_name_id,
			'unit'					 => $unit,
			'department'			 => $department,
			'group'					 => $group, 
			'start'                  => ($page - 1) * 7000,
			'limit'                  => 7000
		);

		// $filter_date_start = $filter_year.'-'.$filter_month.'-'.'01';
		// $max_days = cal_days_in_month(CAL_GREGORIAN, $filter_month, $filter_year);
		// //$filter_date_end = $this->model_report_common_report->getNextDate();
		// $filter_date_end = $filter_year.'-'.$filter_month.'-'.$max_days;

		$day = array();
        $days = $this->GetDays($filter_date_start, $filter_date_end);
        foreach ($days as $dkey => $dvalue) {
        	$dates = explode('-', $dvalue);
        	$day[$dvalue]['day'] = $dates[2];
        	$day[$dvalue]['date'] = $dvalue;
        }

        $days = $day;
        $final_datas = array();
		// echo '<pre>';
		// print_r($data);
		// exit;
		//if(isset($this->request->get['once']) && $this->request->get['once'] == 1){
			$results = $this->model_report_common_report->getemployees_muster($data);
			foreach ($results as $rkey => $rvalue) {
				$performance_data = array();
				$transaction_datas = $this->model_report_common_report->gettransaction_data($rvalue['emp_code'], $data);
				
				$final_datas[$rvalue['emp_code']]['basic_data']['name'] = $rvalue['name'];
				$final_datas[$rvalue['emp_code']]['basic_data']['department'] = $rvalue['department'];
				$final_datas[$rvalue['emp_code']]['basic_data']['emp_code'] = $rvalue['emp_code'];

				$week_count = 0;
				$holiday_count = 0;
				$halfday_count = 0;
				$leave_count = 0;
				$present_count = 0;
				$absent_count = 0;
				$compli_count = 0;
				$total_cnt = 0;
				// echo '<pre>';
				// print_r($transaction_datas);
				// exit;
				if($transaction_datas){
					$count_t = count($transaction_datas) - 1;
					foreach($transaction_datas as $tkey => $tvalue){
						if($tvalue['firsthalf_status'] == '1'){
							if($tvalue['halfday_status'] != '0'){
								$firsthalf_status = 'HD';	
							} else{
								$firsthalf_status = 'P';
							}
						} elseif($tvalue['firsthalf_status'] == '0'){
							$firsthalf_status = 'A';
						} else {
							$firsthalf_status = $tvalue['firsthalf_status'];
						}

						if($tvalue['secondhalf_status'] == '1'){
							$secondhalf_status = 'P';
						} elseif($tvalue['secondhalf_status'] == '0'){
							$secondhalf_status = 'A';
						} else {
							$secondhalf_status = $tvalue['secondhalf_status'];
						}

						// echo '<pre>';
						// print_r($tvalue);
						if($tvalue['leave_status'] == 1){
							// echo 'in leave count 1';
							// echo '<br />';
							$total_cnt = $total_cnt + 1;
							if($secondhalf_status == 'COF' || $secondhalf_status == 'OD'){
								$present_count ++;
							} elseif($secondhalf_status == 'ESIC'){
								$absent_count ++;
							} else { 
								$leave_count ++;
							}
						} elseif($tvalue['leave_status'] == 0.5){
							// echo 'in leave count 0.5';
							// echo '<br />';
							if($firsthalf_status == 'COF' || $secondhalf_status == 'COF'){
								$present_count = $present_count + 0.5;
							} elseif($firsthalf_status == 'OD' || $secondhalf_status == 'OD'){
								$present_count = $present_count + 0.5;
							} elseif($firsthalf_status == 'ESIC' || $secondhalf_status == 'ESIC') {
								$absent_count = $absent_count + 0.5;	
							} else {
								$leave_count = $leave_count + 0.5;
							}
							$total_cnt = $total_cnt + 0.5;
							if($tvalue['present_status'] == 0.5 && $tvalue['halfday_status'] == 0){
								$total_cnt = $total_cnt + 0.5;
								$present_count = $present_count + 0.5;	
							}
							if($tvalue['absent_status'] == 0.5){
								$total_cnt = $total_cnt + 0.5;
								$absent_count = $absent_count + 0.5;	
							}
							
							if($tvalue['halfday_status'] != 0 && ($firsthalf_status != 'OD' && $firsthalf_status != 'COF' && $firsthalf_status != 'PL' && $firsthalf_status != 'CL' && $firsthalf_status != 'SL')){
								//$halfday_count = $halfday_count + 0.5;
								$total_cnt = $total_cnt + 0.5;
							} elseif($tvalue['halfday_status'] != 0 && ($firsthalf_status == 'OD' || $firsthalf_status == 'COF' || $firsthalf_status == 'PL' || $firsthalf_status == 'CL' || $firsthalf_status == 'SL') ) {
								//$halfday_count = $halfday_count + 0.5;
								$total_cnt = $total_cnt + 0.5;
								$present_count = $present_count + 0.5;
							} elseif($tvalue['halfday_status'] != 0 && ($secondhalf_status != 'OD' && $secondhalf_status != 'COF' && $secondhalf_status != 'PL' && $secondhalf_status != 'CL' && $secondhalf_status != 'SL') ){
								//$halfday_count = $halfday_count + 0.5;
								$total_cnt = $total_cnt + 0.5;
							} elseif($tvalue['halfday_status'] != 0 && ($secondhalf_status == 'OD' || $secondhalf_status == 'COF' || $secondhalf_status == 'PL' || $secondhalf_status == 'CL' || $secondhalf_status == 'SL') ) {
								//$halfday_count = $halfday_count + 0.5;
								$total_cnt = $total_cnt + 0.5;
								$present_count = $present_count + 0.5;
							}
						} elseif($tvalue['on_duty'] == 1){
							// echo 'in present count 1';
							// echo '<br />';
							$total_cnt = $total_cnt + 1;
							$present_count ++;
						} elseif($tvalue['present_status'] == 1){
							// echo 'in present count 1';
							// echo '<br />';
							$total_cnt = $total_cnt + 1;
							$present_count ++;
						} elseif($tvalue['present_status'] == 0.5){
							// echo 'in present count 0.5';
							// echo '<br />';
							$total_cnt = $total_cnt + 0.5;
							$present_count = $present_count + 0.5;
							if($tvalue['absent_status'] == 0.5){
								$total_cnt = $total_cnt + 0.5;
								$absent_count = $absent_count + 0.5;
							}
						} elseif($tvalue['absent_status'] == 1){
							// echo 'in absent count 1';
							// echo '<br />';
							$total_cnt = $total_cnt + 1;
							$absent_count ++;
						} elseif($tvalue['absent_status'] == 0.5){
							// echo 'in absent count 0.5';
							// echo '<br />';
							$total_cnt = $total_cnt + 0.5;
							$absent_count = $absent_count + 0.5;
							if($tvalue['present_status'] == 0.5){
								$total_cnt = $total_cnt + 0.5;
								$present_count = $present_count + 0.5;
							}
						} elseif($tvalue['weekly_off'] != 0){
							// echo 'in weekly_off count';
							// echo '<br />';
							$total_cnt = $total_cnt + 1;
							if($tkey == 0){
								$last_key = $tkey;
							} else {
								$last_key = $tkey - 1;
							}
							if($count_t == $tkey){
								$next_key = $tkey;
							} else {
								$next_key = $tkey + 1;	
							}
							if($transaction_datas[$last_key]['absent_status'] == '1' && $transaction_datas[$next_key]['absent_status'] == '1'){
								$absent_count ++;
								$firsthalf_status = 'A';
								$secondhalf_status = 'A';
							} else {
								$week_count ++;
							}
						} elseif($tvalue['holiday_id'] != 0){
							// echo 'in holiday count';
							// echo '<br />';
							if($tkey == 0){
								$last_key = $tkey;
							} else {
								$last_key = $tkey - 1;
							}
							if($count_t == $tkey){
								$next_key = $tkey;
							} else {
								$next_key = $tkey + 1;	
							}
							if($transaction_datas[$last_key]['absent_status'] == '1' && $transaction_datas[$next_key]['absent_status'] == '1'){
								$absent_count ++;
								$firsthalf_status = 'A';
								$secondhalf_status = 'A';
							} else {
								$holiday_count ++;	
							}
							$total_cnt = $total_cnt + 1;
							//$holiday_count ++;
						} elseif($tvalue['halfday_status'] != 0){
							// echo 'in halffday count';
							// echo '<br />';
							if($tkey == 0){
								$last_key = $tkey;
							} else {
								$last_key = $tkey - 1;
							}
							if($count_t == $tkey){
								$next_key = $tkey;
							} else {
								$next_key = $tkey + 1;	
							}
							if($transaction_datas[$last_key]['absent_status'] == '1' && $transaction_datas[$next_key]['absent_status'] == '1'){
								$absent_count ++;
								$firsthalf_status = 'A';
								$secondhalf_status = 'A';
							} else {
								$present_count ++;	
							}
							$total_cnt = $total_cnt + 1;
							//$present_count ++;
							//$halfday_count ++;
						} elseif($tvalue['compli_status'] != 0){
							// echo 'in halffday count';
							// echo '<br />';
							if($tkey == 0){
								$last_key = $tkey;
							} else {
								$last_key = $tkey - 1;
							}
							if($count_t == $tkey){
								$next_key = $tkey;
							} else {
								$next_key = $tkey + 1;	
							}
							if($transaction_datas[$last_key]['absent_status'] == '1' && $transaction_datas[$next_key]['absent_status'] == '1'){
								$absent_count ++;
								$firsthalf_status = 'A';
								$secondhalf_status = 'A';
							} else {
								$present_count ++;	
							}
							$total_cnt = $total_cnt + 1;
							//$compli_count ++;
						}
						
						$performance_data['action'][$tvalue['date']] = array(
							'firsthalf_status' => $firsthalf_status,
							'secondhalf_status' => $secondhalf_status,
							'date' => $tvalue['date'],
						);
					}
					//$total_cnt = count($performance_data['action']);
					foreach ($day as $dkey => $dvalue) {
						foreach ($performance_data as $pkey => $pvalue) {
							// echo '<pre>';
							// print_r($pvalue[$dvalue['date']]['date']);
							// exit;
							if(isset($pvalue[$dvalue['date']]['date'])){
							} else {
								$performance_data['action'][$dvalue['date']] = array(
									'firsthalf_status' => '',
									'secondhalf_status' => '',
									'date' => $dvalue['date']
								);		
							}
						}
					}
					$in = 0;
					if($filter_type == 1){
						if($absent_count > 0){
							$in = 1;
						}
					} elseif($filter_type == 0){
						$in = 1;
					}
					$performance_data = $this->array_sort($performance_data['action'], 'date', SORT_ASC);
					// echo '<pre>';
					// print_r($performance_data);
					// exit;
					//echo $present_count;exit;
					if($in == 1){
						$final_datas[$rvalue['emp_code']]['basic_data']['week_count'] = $week_count;
						$final_datas[$rvalue['emp_code']]['basic_data']['holiday_count'] = $holiday_count;
						$final_datas[$rvalue['emp_code']]['basic_data']['halfday_count'] = $halfday_count;
						$final_datas[$rvalue['emp_code']]['basic_data']['compli_count'] = $compli_count;
						$final_datas[$rvalue['emp_code']]['basic_data']['leave_count'] = $leave_count;
						$final_datas[$rvalue['emp_code']]['basic_data']['present_count'] = $present_count;
						$final_datas[$rvalue['emp_code']]['basic_data']['absent_count'] = $absent_count;
						$final_datas[$rvalue['emp_code']]['basic_data']['total_cnt'] = $total_cnt;
						$final_datas[$rvalue['emp_code']]['tran_data']['action'] = $performance_data;
					} else {
						unset($final_datas[$rvalue['emp_code']]);
					}
				} else {
					unset($final_datas[$rvalue['emp_code']]);
				}
			}
			//exit;
		//}

		//$final_datas = array();
		//$this->data['final_datas'] = $final_datas;
		
		//$final_datass = array_chunk($final_datas, 9);

		// echo '<pre>';
		// print_r($final_datass);
		// exit;
		
		if($final_datas){
			// if($filter_type == 1){
			// 	$report_type = 'Defult';
			// } elseif($filter_type == 2){
			// 	$report_type = 'Late';
			// } elseif ($filter_type == 3) {
			// 	$report_type = 'Less Time';
			// }
			//$month = date("F", mktime(0, 0, 0, $filter_month, 10));
			$template = new Template();		
			$template->data['final_datas'] = $final_datas;
			$template->data['filter_date_start'] = $filter_date_start;
			$template->data['filter_date_end'] = $filter_date_end;
			$template->data['days'] = $days;
			//$template->data['report_type'] = $report_type;
			$template->data['title'] = 'Muster Report';
			if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
				$template->data['base'] = HTTPS_SERVER;
			} else {
				$template->data['base'] = HTTP_SERVER;
			}
			$html = $template->fetch('report/muster_html.tpl');
			//echo $html;exit;
			/*
			$filename = "Muster_Report.xls";
			$dpath = DIR_DOWNLOAD.$filename;

			@unlink($dpath);
			$handle = fopen($dpath, "w+");
			fwrite($handle, $html);
			fclose($handle);
			//echo 'sudo chmod -R 400 '.$dpath;exit;
			exec('sudo chmod -R 400 '.$dpath);
			
			$file = $dpath;
			$mask = '';

			if (!headers_sent()) {
				if (file_exists($file)) {
					header('Content-Type: application/octet-stream');
					header('Content-Description: File Transfer');
					header('Content-Disposition: attachment; filename="' . ($mask ? $mask : basename($file)) . '"');
					header('Content-Transfer-Encoding: binary');
					header('Expires: 0');
					header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
					header('Pragma: public');
					header('Content-Length: ' . filesize($file));

					readfile($file, 'rb');
					exit;
				} else {
					exit('Error: Could not find file ' . $file . '!');
				}
			} else {
				exit('Error: Headers already sent out!');
			}
			*/
			$filename = "Muster_Report";			
			header("Content-Type: application/vnd.ms-excel; charset=utf-8");
			header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false);
			echo $html;
			exit;		
		} else {
			$this->session->data['warning'] = 'No Data Found';
			$this->redirect($this->url->link('report/muster', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}
	}

	public function export_summary(){
		$this->language->load('report/muster');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			//$filter_date_start = date('Y-m-d');
			//$filter_date_start = date('Y-m-d', strtotime(date('Y') . '-' . date('m') . '-01'));
			//$filter_date_start = '';
			$from = date('Y-m-d');
			$filter_date_start = date('Y-m-d', strtotime($from . "-30 day"));
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = date('Y-m-d');
		}
		
		// if (isset($this->request->get['filter_month'])) {
		// 	$filter_month = $this->request->get['filter_month'];
		// } else {
		// 	$filter_month = date('n');
		// }

		// if (isset($this->request->get['filter_year'])) {
		// 	$filter_year = $this->request->get['filter_year'];
		// } else {
		// 	$filter_year = date('Y');
		// }

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['unit'])) {
			$unit = $this->request->get['unit'];
		} else {
			$unit = 0;
		}

		if (isset($this->request->get['department'])) {
			$department = html_entity_decode($this->request->get['department']);
		} else {
			$department = 0;
		}

		if (isset($this->request->get['group'])) {
			$group = $this->request->get['group'];
		} else {
			$group = 0;
		}

		if (isset($this->request->get['filter_type'])) {
			$filter_type = $this->request->get['filter_type'];
		} else {
			$filter_type = 0;
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		
		// if (isset($this->request->get['filter_month'])) {
		// 	$url .= '&filter_month=' . $this->request->get['filter_month'];
		// }

		// if (isset($this->request->get['filter_year'])) {
		// 	$url .= '&filter_year=' . $this->request->get['filter_year'];
		// }

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_type'])) {
			$url .= '&filter_type=' . $this->request->get['filter_type'];
		}

		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}
		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$url .= '&once=1';

		$this->load->model('report/common_report');
		$this->load->model('report/attendance');
		$this->load->model('catalog/employee');

		$this->data['attendace'] = array();

		$data = array(
			'filter_date_start'	     => $filter_date_start,
			'filter_date_end'	     => $filter_date_end,
			// 'filter_month'	     	 => $filter_month,
			// 'filter_year'	     => $filter_year,
			'filter_name'	     	 => $filter_name,
			'filter_name_id'	     => $filter_name_id,
			'unit'					 => $unit,
			'department'			 => $department,
			'group'					 => $group, 
			'start'                  => ($page - 1) * 7000,
			'limit'                  => 7000
		);

		// $filter_date_start = $filter_year.'-'.$filter_month.'-'.'01';
		// $max_days = cal_days_in_month(CAL_GREGORIAN, $filter_month, $filter_year);
		// //$filter_date_end = $this->model_report_common_report->getNextDate();
		// $filter_date_end = $filter_year.'-'.$filter_month.'-'.$max_days;

		$day = array();
        $days = $this->GetDays($filter_date_start, $filter_date_end);
        foreach ($days as $dkey => $dvalue) {
        	$dates = explode('-', $dvalue);
        	$day[$dvalue]['day'] = $dates[2];
        	$day[$dvalue]['date'] = $dvalue;
        }

        $days = $day;
        $final_datas = array();
		// echo '<pre>';
		// print_r($data);
		// exit;
		//if(isset($this->request->get['once']) && $this->request->get['once'] == 1){
			$results = $this->model_report_common_report->getemployees_muster($data);
			foreach ($results as $rkey => $rvalue) {
				$performance_data = array();
				$encash_days = $this->model_report_common_report->getencash_data($rvalue['emp_code'], $data);
				$un_encash_days = $this->model_report_common_report->getencash_data_un($rvalue['emp_code'], $data);
				
				$transaction_datas = $this->model_report_common_report->gettransaction_data($rvalue['emp_code'], $data);
				
				$final_datas[$rvalue['emp_code']]['basic_data']['name'] = $rvalue['name'];
				$final_datas[$rvalue['emp_code']]['basic_data']['department'] = $rvalue['department'];
				$final_datas[$rvalue['emp_code']]['basic_data']['emp_code'] = $rvalue['emp_code'];

				$week_count = 0;
				$holiday_count = 0;
				$halfday_count = 0;
				$leave_count = 0;
				$present_count = 0;
				$absent_count = 0;
				$compli_count = 0;
				$pl_cnt = 0;
				$sl_cnt = 0;
				$cl_cnt = 0;
				$total_cnt = 0;
				// echo '<pre>';
				// print_r($transaction_datas);
				// exit;
				if($transaction_datas){
					$count_t = count($transaction_datas) - 1;
					foreach($transaction_datas as $tkey => $tvalue){
						if($tvalue['firsthalf_status'] == '1'){
							if($tvalue['halfday_status'] != '0'){
								$firsthalf_status = 'HD';
							} else {
								$firsthalf_status = 'P';
							}
						} elseif($tvalue['firsthalf_status'] == '0'){
							$firsthalf_status = 'A';
						} else {
							$firsthalf_status = $tvalue['firsthalf_status'];
						}

						if($tvalue['secondhalf_status'] == '1'){
							if($tvalue['halfday_status'] != '0'){
								$secondhalf_status = 'HD';
							} else {
								$secondhalf_status = 'P';
							}
						} elseif($tvalue['secondhalf_status'] == '0'){
							$secondhalf_status = 'A';
						} else {
							$secondhalf_status = $tvalue['secondhalf_status'];
						}

						// echo '<pre>';
						// print_r($tvalue);

						if($tvalue['leave_status'] == 1){
							// echo 'in leave count 1';
							// echo '<br />';
							$total_cnt = $total_cnt + 1;
							if($tvalue['firsthalf_status'] == 'PL'){
								$pl_cnt ++;
							} elseif($tvalue['firsthalf_status'] == 'CL'){
								$cl_cnt ++;
							} elseif($tvalue['firsthalf_status'] == 'SL'){
								$sl_cnt ++;
							}
							if($firsthalf_status == 'COF' || $firsthalf_status == 'OD'){
								$present_count = $present_count + 1;
							} elseif($firsthalf_status == 'ESIC'){
								$absent_count = $absent_count + 1;
							} else {
								$leave_count ++;;	
							}
						} elseif($tvalue['leave_status'] == 0.5){
							// echo 'in leave count 0.5';
							// echo '<br />';
							if($tvalue['firsthalf_status'] != '1' && $tvalue['firsthalf_status'] != '0'){
								if($tvalue['firsthalf_status'] == 'PL'){
									$pl_cnt = $pl_cnt + 0.5;
								} elseif($tvalue['firsthalf_status'] == 'CL'){
									$cl_cnt = $cl_cnt + 0.5;
								} elseif($tvalue['firsthalf_status'] == 'SL'){
									$sl_cnt = $sl_cnt + 0.5;
								}
							} elseif($tvalue['secondhalf_status'] != '1' && $tvalue['secondhalf_status'] != '0') {
								if($tvalue['secondhalf_status'] == 'PL'){
									$pl_cnt = $pl_cnt + 0.5;
								} elseif($tvalue['secondhalf_status'] == 'CL'){
									$cl_cnt = $cl_cnt + 0.5;
								} elseif($tvalue['secondhalf_status'] == 'SL'){
									$sl_cnt = $sl_cnt + 0.5;
								}
							}
							$total_cnt = $total_cnt + 0.5;
							if($firsthalf_status == 'COF' || $secondhalf_status == 'COF'){
								$present_count = $present_count + 0.5;
							} elseif($firsthalf_status == 'OD' || $secondhalf_status == 'OD'){
								$present_count = $present_count + 0.5;
							} elseif($firsthalf_status == 'ESIC' || $secondhalf_status == 'ESIC') {
								$absent_count = $absent_count + 0.5;	
							} else {
								$leave_count = $leave_count + 0.5;
							}
							if($tvalue['present_status'] == 0.5 && $tvalue['halfday_status'] == 0){
								$total_cnt = $total_cnt + 0.5;
								$present_count = $present_count + 0.5;	
							}
							if($tvalue['absent_status'] == 0.5){
								$total_cnt = $total_cnt + 0.5;
								$absent_count = $absent_count + 0.5;	
							}

							if($tvalue['halfday_status'] != 0 && ($firsthalf_status != 'OD' && $firsthalf_status != 'COF' && $firsthalf_status != 'PL' && $firsthalf_status != 'CL' && $firsthalf_status != 'SL')){
								//$halfday_count = $halfday_count + 0.5;
								$total_cnt = $total_cnt + 0.5;
							} elseif($tvalue['halfday_status'] != 0 && ($firsthalf_status == 'OD' || $firsthalf_status == 'COF' || $firsthalf_status == 'PL' || $firsthalf_status == 'CL' || $firsthalf_status == 'SL') ) {
								$halfday_count = $halfday_count + 0.5;
								$total_cnt = $total_cnt + 0.5;
								$present_count = $present_count + 0.5;
							} elseif($tvalue['halfday_status'] != 0 && ($secondhalf_status != 'OD' && $secondhalf_status != 'COF' && $secondhalf_status != 'PL' && $secondhalf_status != 'CL' && $secondhalf_status != 'SL') ){
								//$halfday_count = $halfday_count + 0.5;
								$total_cnt = $total_cnt + 0.5;
							} elseif($tvalue['halfday_status'] != 0 && ($secondhalf_status == 'OD' || $secondhalf_status == 'COF' || $secondhalf_status == 'PL' || $secondhalf_status == 'CL' || $secondhalf_status == 'SL') ) {
								$halfday_count = $halfday_count + 0.5;
								$total_cnt = $total_cnt + 0.5;
								$present_count = $present_count + 0.5;
							}
							// if($tvalue['halfday_status'] != 0){
							// 	$halfday_count = $halfday_count + 0.5;
							// 	$total_cnt = $total_cnt + 0.5;
							// 	$present_count = $present_count + 0.5;
							// }
						} elseif($tvalue['on_duty'] == 1){
							// echo 'in present count 1';
							// echo '<br />';
							$total_cnt = $total_cnt + 1;
							$present_count ++;
						} elseif($tvalue['present_status'] == 1){
							// echo 'in present count 1';
							// echo '<br />';
							$present_count ++;
							$total_cnt = $total_cnt + 1;
						} elseif($tvalue['present_status'] == 0.5){
							// echo 'in present count 0.5';
							// echo '<br />';
							$present_count = $present_count + 0.5;
							$total_cnt = $total_cnt + 0.5;
							if($tvalue['absent_status'] == 0.5){
								$total_cnt = $total_cnt + 0.5;
								$absent_count = $absent_count + 0.5;
							}
							if($tvalue['leave_status'] == 0.5){
								if($tvalue['firsthalf_status'] != '1' && $tvalue['firsthalf_status'] != '0'){
									if($tvalue['firsthalf_status'] == 'PL'){
										$pl_cnt = $pl_cnt + 0.5;
									} elseif($tvalue['firsthalf_status'] == 'CL'){
										$cl_cnt = $cl_cnt + 0.5;
									} elseif($tvalue['firsthalf_status'] == 'SL'){
										$sl_cnt = $sl_cnt + 0.5;
									}
								} elseif($tvalue['secondhalf_status'] != '1' && $tvalue['secondhalf_status'] != '0') {
									if($tvalue['secondhalf_status'] == 'PL'){
										$pl_cnt = $pl_cnt + 0.5;
									} elseif($tvalue['secondhalf_status'] == 'CL'){
										$cl_cnt = $cl_cnt + 0.5;
									} elseif($tvalue['secondhalf_status'] == 'SL'){
										$sl_cnt = $sl_cnt + 0.5;
									}
								}
								$total_cnt = $total_cnt + 0.5;
								$leave_count = $leave_count + 0.5;	
							}
						} elseif($tvalue['absent_status'] == 1){
							// echo 'in absent count 1';
							// echo '<br />';
							$absent_count ++;
							$total_cnt = $total_cnt + 1;
						} elseif($tvalue['absent_status'] == 0.5){
							// echo 'in absent count 0.5';
							// echo '<br />';
							$absent_count = $absent_count + 0.5;
							$total_cnt = $total_cnt + 0.5;
							if($tvalue['present_status'] == 0.5){
								$total_cnt = $total_cnt + 0.5;
								$present_count = $present_count + 0.5;
							}
							if($tvalue['leave_status'] == 0.5){
								if($tvalue['firsthalf_status'] != '1' && $tvalue['firsthalf_status'] != '0'){
									if($tvalue['firsthalf_status'] == 'PL'){
										$pl_cnt = $pl_cnt + 0.5;
									} elseif($tvalue['firsthalf_status'] == 'CL'){
										$cl_cnt = $cl_cnt + 0.5;
									} elseif($tvalue['firsthalf_status'] == 'SL'){
										$sl_cnt = $sl_cnt + 0.5;
									}
								} elseif($tvalue['secondhalf_status'] != '1' && $tvalue['secondhalf_status'] != '0') {
									if($tvalue['secondhalf_status'] == 'PL'){
										$pl_cnt = $pl_cnt + 0.5;
									} elseif($tvalue['secondhalf_status'] == 'CL'){
										$cl_cnt = $cl_cnt + 0.5;
									} elseif($tvalue['secondhalf_status'] == 'SL'){
										$sl_cnt = $sl_cnt + 0.5;
									}
								}
								$total_cnt = $total_cnt + 0.5;
								$leave_count = $leave_count + 0.5;	
							}
						} elseif($tvalue['weekly_off'] != 0){
							// echo 'in weekly_off count';
							// echo '<br />';
							if($tkey == 0){
								$last_key = $tkey;
							} else {
								$last_key = $tkey - 1;
							}
							if($count_t == $tkey){
								$next_key = $tkey;
							} else {
								$next_key = $tkey + 1;	
							}
							if($transaction_datas[$last_key]['absent_status'] == '1' && $transaction_datas[$next_key]['absent_status'] == '1'){
								$absent_count ++;
								$firsthalf_status = 'A';
								$secondhalf_status = 'A';
							} else {
								$week_count ++;
							}
							$total_cnt = $total_cnt + 1;
						} elseif($tvalue['holiday_id'] != 0){
							// echo 'in holiday count';
							// echo '<br />';
							if($tkey == 0){
								$last_key = $tkey;
							} else {
								$last_key = $tkey - 1;
							}
							if($count_t == $tkey){
								$next_key = $tkey;
							} else {
								$next_key = $tkey + 1;	
							}
							if($transaction_datas[$last_key]['absent_status'] == '1' && $transaction_datas[$next_key]['absent_status'] == '1'){
								$absent_count ++;
								$firsthalf_status = 'A';
								$secondhalf_status = 'A';
							} else {
								$holiday_count ++;	
							}
							//$holiday_count ++;
							$total_cnt = $total_cnt + 1;
						} elseif($tvalue['halfday_status'] != 0){
							// echo 'in halffday count';
							// echo '<br />';
							if($tkey == 0){
								$last_key = $tkey;
							} else {
								$last_key = $tkey - 1;
							}
							if($count_t == $tkey){
								$next_key = $tkey;
							} else {
								$next_key = $tkey + 1;	
							}
							if($transaction_datas[$last_key]['absent_status'] == '1' && $transaction_datas[$next_key]['absent_status'] == '1'){
								$absent_count ++;
								$firsthalf_status = 'A';
								$secondhalf_status = 'A';
							} else {
								$halfday_count ++;	
								$present_count ++;
							}
							//$halfday_count ++;
							//$present_count ++;
							$total_cnt = $total_cnt + 1;
						} elseif($tvalue['compli_status'] != 0){
							// echo 'in halffday count';
							// echo '<br />';
							if($tkey == 0){
								$last_key = $tkey;
							} else {
								$last_key = $tkey - 1;
							}
							if($count_t == $tkey){
								$next_key = $tkey;
							} else {
								$next_key = $tkey + 1;	
							}
							if($transaction_datas[$last_key]['absent_status'] == '1' && $transaction_datas[$next_key]['absent_status'] == '1'){
								$absent_count ++;
								$firsthalf_status = 'A';
								$secondhalf_status = 'A';
							} else {
								$compli_count ++;	
								$present_count ++;
							}
							//$present_count ++;
							//$compli_count ++;
							$total_cnt = $total_cnt + 1;
						}

						$performance_data['action'][$tvalue['date']] = array(
							'firsthalf_status' => $firsthalf_status,
							'secondhalf_status' => $secondhalf_status,
							'date' => $tvalue['date'],
						);
					}
					//$total_cnt = count($performance_data['action']);
					foreach ($day as $dkey => $dvalue) {
						foreach ($performance_data as $pkey => $pvalue) {
							// echo '<pre>';
							// print_r($pvalue[$dvalue['date']]['date']);
							// exit;
							if(isset($pvalue[$dvalue['date']]['date'])){
							} else {
								$performance_data['action'][$dvalue['date']] = array(
									'firsthalf_status' => '',
									'secondhalf_status' => '',
									'date' => $dvalue['date']
								);		
							}
						}
					}
					$in = 0;
					if($filter_type == 1){
						if($absent_count > 0){
							$in = 1;
						}
					} elseif($filter_type == 0){
						$in = 1;
					}
					$performance_data = $this->array_sort($performance_data['action'], 'date', SORT_ASC);
					// echo '<pre>';
					// print_r($performance_data);
					// exit;
					//echo $present_count;exit;
					$total_en = $encash_days + $un_encash_days;
					if($in == 1 && $rvalue['grade'] != 'CONSULTANT' && $rvalue['emp_code'] < 50000){
						$filter_month = date('n', strtotime($filter_date_end));
						$filter_year = date('Y', strtotime($filter_date_end));
						$number = cal_days_in_month(CAL_GREGORIAN, $filter_month, $filter_year);
						//echo $present_count;exit;
						if($total_cnt < $number){
							if($absent_count == $total_cnt){
								$total_cnt = $total_cnt + 1;
								$absent_count = $absent_count + 1;
							} elseif($present_count > 0){
								$total_cnt = $total_cnt + 1;
								$present_count = $present_count + 1;
							} else {
								$total_cnt = $total_cnt + 1;
								$absent_count = $absent_count + 1;
							}
						}

						if($total_cnt > $number){
							if($absent_count == $total_cnt){
								$total_cnt = $total_cnt - 1;
								$absent_count = $absent_count - 1;
							} elseif($present_count > 0){
								$total_cnt = $total_cnt - 1;
								$present_count = $present_count - 1;
							} else {
								$total_cnt = $total_cnt - 1;
								$absent_count = $absent_count - 1;
							}
						}

						if($absent_count > $number){
							$absent_count = $number;			
						}

						if($pl_cnt > $number){
							$pl_cnt = $number;			
						}

						if($cl_cnt > $number){
							$cl_cnt = $number;			
						}

						if($sl_cnt > $number){
							$sl_cnt = $number;			
						}						
	
						$final_datas[$rvalue['emp_code']]['basic_data']['week_count'] = $week_count;
						$final_datas[$rvalue['emp_code']]['basic_data']['holiday_count'] = $holiday_count;
						$final_datas[$rvalue['emp_code']]['basic_data']['halfday_count'] = $halfday_count;
						$final_datas[$rvalue['emp_code']]['basic_data']['leave_count'] = $leave_count;
						$final_datas[$rvalue['emp_code']]['basic_data']['present_count'] = $present_count;
						$final_datas[$rvalue['emp_code']]['basic_data']['absent_count'] = $absent_count;
						$final_datas[$rvalue['emp_code']]['basic_data']['total_cnt'] = $total_cnt;
						$final_datas[$rvalue['emp_code']]['basic_data']['pl_cnt'] = $pl_cnt;
						$final_datas[$rvalue['emp_code']]['basic_data']['sl_cnt'] = $sl_cnt;
						$final_datas[$rvalue['emp_code']]['basic_data']['cl_cnt'] = $cl_cnt;
						$final_datas[$rvalue['emp_code']]['basic_data']['encash'] = $encash_days + $un_encash_days;
						$final_datas[$rvalue['emp_code']]['tran_data']['action'] = $performance_data;
					} else {
						unset($final_datas[$rvalue['emp_code']]);
					}
				} else {
					unset($final_datas[$rvalue['emp_code']]);
				}
			}
			//exit;
		//}

		//$final_datas = array();
		//$this->data['final_datas'] = $final_datas;
		
		//$final_datass = array_chunk($final_datas, 43);

		// echo '<pre>';
		// print_r($final_datas);
		// exit;
		
		if($final_datas){
			// if($filter_type == 1){
			// 	$report_type = 'Defult';
			// } elseif($filter_type == 2){
			// 	$report_type = 'Late';
			// } elseif ($filter_type == 3) {
			// 	$report_type = 'Less Time';
			// }
			//$month = date("F", mktime(0, 0, 0, $filter_month, 10));
			$template = new Template();		
			$template->data['final_datas'] = $final_datas;
			$template->data['filter_date_start'] = $filter_date_start;
			$template->data['filter_date_end'] = $filter_date_end;
			$template->data['days'] = $days;
			//$template->data['report_type'] = $report_type;
			$template->data['title'] = 'Muster Summary Report';
			if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
				$template->data['base'] = HTTPS_SERVER;
			} else {
				$template->data['base'] = HTTP_SERVER;
			}
			$html = $template->fetch('report/muster_sum_html.tpl');
			//echo $html;exit;
			$filename = "Muster_Summary_Report";
			
			header("Content-Type: application/vnd.ms-excel; charset=utf-8");
			header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false);
			echo $html;
			exit;		
		} else {
			$this->session->data['warning'] = 'No Data Found';
			$this->redirect($this->url->link('report/muster', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}
	}

	public function export_insert(){
		$this->language->load('report/muster');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			//$filter_date_start = date('Y-m-d');
			//$filter_date_start = date('Y-m-d', strtotime(date('Y') . '-' . date('m') . '-01'));
			//$filter_date_start = '';
			$from = date('Y-m-d');
			$filter_date_start = date('Y-m-d', strtotime($from . "-30 day"));
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = date('Y-m-d');
		}
		
		// if (isset($this->request->get['filter_month'])) {
		// 	$filter_month = $this->request->get['filter_month'];
		// } else {
		// 	$filter_month = date('n');
		// }

		// if (isset($this->request->get['filter_year'])) {
		// 	$filter_year = $this->request->get['filter_year'];
		// } else {
		// 	$filter_year = date('Y');
		// }

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['unit'])) {
			$unit = $this->request->get['unit'];
		} else {
			$unit = 0;
		}

		if (isset($this->request->get['department'])) {
			$department = html_entity_decode($this->request->get['department']);
		} else {
			$department = 0;
		}

		if (isset($this->request->get['group'])) {
			$group = $this->request->get['group'];
		} else {
			$group = 0;
		}

		if (isset($this->request->get['filter_type'])) {
			$filter_type = $this->request->get['filter_type'];
		} else {
			$filter_type = 0;
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		
		// if (isset($this->request->get['filter_month'])) {
		// 	$url .= '&filter_month=' . $this->request->get['filter_month'];
		// }

		// if (isset($this->request->get['filter_year'])) {
		// 	$url .= '&filter_year=' . $this->request->get['filter_year'];
		// }

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_type'])) {
			$url .= '&filter_type=' . $this->request->get['filter_type'];
		}

		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}
		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$url .= '&once=1';

		$this->load->model('report/common_report');
		$this->load->model('report/attendance');
		$this->load->model('catalog/employee');

		$this->data['attendace'] = array();

		$data = array(
			'filter_date_start'	     => $filter_date_start,
			'filter_date_end'	     => $filter_date_end,
			// 'filter_month'	     	 => $filter_month,
			// 'filter_year'	     => $filter_year,
			'filter_name'	     	 => $filter_name,
			'filter_name_id'	     => $filter_name_id,
			'unit'					 => $unit,
			'department'			 => $department,
			'group'					 => $group, 
			'start'                  => ($page - 1) * 7000,
			'limit'                  => 7000
		);

		// $filter_date_start = $filter_year.'-'.$filter_month.'-'.'01';
		// $max_days = cal_days_in_month(CAL_GREGORIAN, $filter_month, $filter_year);
		// //$filter_date_end = $this->model_report_common_report->getNextDate();
		// $filter_date_end = $filter_year.'-'.$filter_month.'-'.$max_days;

		$day = array();
        $days = $this->GetDays($filter_date_start, $filter_date_end);
        foreach ($days as $dkey => $dvalue) {
        	$dates = explode('-', $dvalue);
        	$day[$dvalue]['day'] = $dates[2];
        	$day[$dvalue]['date'] = $dvalue;
        }

        $days = $day;
        $final_datas = array();
		// echo '<pre>';
		// print_r($data);
		// exit;
		$year = date('Y', strtotime($filter_date_end));
		$month = date('m', strtotime($filter_date_end));
		$month_year = $year.$month;
		//if(isset($this->request->get['once']) && $this->request->get['once'] == 1){
			$results = $this->model_report_common_report->getemployees_muster($data);
			foreach ($results as $rkey => $rvalue) {
				$performance_data = array();
				$encash_days = $this->model_report_common_report->getencash_data($rvalue['emp_code'], $data);
				$un_encash_days = $this->model_report_common_report->getencash_data_un($rvalue['emp_code'], $data);
				
				$transaction_datas = $this->model_report_common_report->gettransaction_data($rvalue['emp_code'], $data);
				
				$final_datas[$rvalue['emp_code']]['name'] = $rvalue['name'];
				$final_datas[$rvalue['emp_code']]['department'] = $rvalue['department'];
				$final_datas[$rvalue['emp_code']]['emp_code'] = $rvalue['emp_code'];

				$week_count = 0;
				$holiday_count = 0;
				$halfday_count = 0;
				$leave_count = 0;
				$present_count = 0;
				$absent_count = 0;
				$compli_count = 0;
				$pl_cnt = 0;
				$sl_cnt = 0;
				$cl_cnt = 0;
				$total_cnt = 0;
				// echo '<pre>';
				// print_r($transaction_datas);
				// exit;
				if($transaction_datas){
					$count_t = count($transaction_datas) - 1;
					foreach($transaction_datas as $tkey => $tvalue){
						if($tvalue['firsthalf_status'] == '1'){
							if($tvalue['halfday_status'] != '0'){
								$firsthalf_status = 'HD';
							} else {
								$firsthalf_status = 'P';
							}
						} elseif($tvalue['firsthalf_status'] == '0'){
							$firsthalf_status = 'A';
						} else {
							$firsthalf_status = $tvalue['firsthalf_status'];
						}

						if($tvalue['secondhalf_status'] == '1'){
							if($tvalue['halfday_status'] != '0'){
								$secondhalf_status = 'HD';
							} else {
								$secondhalf_status = 'P';
							}
						} elseif($tvalue['secondhalf_status'] == '0'){
							$secondhalf_status = 'A';
						} else {
							$secondhalf_status = $tvalue['secondhalf_status'];
						}

						// echo '<pre>';
						// print_r($tvalue);

						if($tvalue['leave_status'] == 1){
							// echo 'in leave count 1';
							// echo '<br />';
							$total_cnt = $total_cnt + 1;
							if($tvalue['firsthalf_status'] == 'PL'){
								$pl_cnt ++;
							} elseif($tvalue['firsthalf_status'] == 'CL'){
								$cl_cnt ++;
							} elseif($tvalue['firsthalf_status'] == 'SL'){
								$sl_cnt ++;
							}
							if($firsthalf_status == 'COF' || $firsthalf_status == 'OD'){
								$present_count = $present_count + 1;
							} elseif($firsthalf_status == 'ESIC'){
								$absent_count = $absent_count + 1;
							} else {
								$leave_count ++;;	
							}
						} elseif($tvalue['leave_status'] == 0.5){
							// echo 'in leave count 0.5';
							// echo '<br />';
							if($tvalue['firsthalf_status'] != '1' && $tvalue['firsthalf_status'] != '0'){
								if($tvalue['firsthalf_status'] == 'PL'){
									$pl_cnt = $pl_cnt + 0.5;
								} elseif($tvalue['firsthalf_status'] == 'CL'){
									$cl_cnt = $cl_cnt + 0.5;
								} elseif($tvalue['firsthalf_status'] == 'SL'){
									$sl_cnt = $sl_cnt + 0.5;
								}
							} elseif($tvalue['secondhalf_status'] != '1' && $tvalue['secondhalf_status'] != '0') {
								if($tvalue['secondhalf_status'] == 'PL'){
									$pl_cnt = $pl_cnt + 0.5;
								} elseif($tvalue['secondhalf_status'] == 'CL'){
									$cl_cnt = $cl_cnt + 0.5;
								} elseif($tvalue['secondhalf_status'] == 'SL'){
									$sl_cnt = $sl_cnt + 0.5;
								}
							}
							$total_cnt = $total_cnt + 0.5;
							if($firsthalf_status == 'COF' || $secondhalf_status == 'COF'){
								$present_count = $present_count + 0.5;
							} elseif($firsthalf_status == 'OD' || $secondhalf_status == 'OD'){
								$present_count = $present_count + 0.5;
							} elseif($firsthalf_status == 'ESIC' || $secondhalf_status == 'ESIC') {
								$absent_count = $absent_count + 0.5;	
							} else {
								$leave_count = $leave_count + 0.5;
							}
							if($tvalue['present_status'] == 0.5 && $tvalue['halfday_status'] == 0){
								$total_cnt = $total_cnt + 0.5;
								$present_count = $present_count + 0.5;	
							}
							if($tvalue['absent_status'] == 0.5){
								$total_cnt = $total_cnt + 0.5;
								$absent_count = $absent_count + 0.5;	
							}

							if($tvalue['halfday_status'] != 0 && ($firsthalf_status != 'OD' && $firsthalf_status != 'COF' && $firsthalf_status != 'PL' && $firsthalf_status != 'CL' && $firsthalf_status != 'SL')){
								//$halfday_count = $halfday_count + 0.5;
								$total_cnt = $total_cnt + 0.5;
							} elseif($tvalue['halfday_status'] != 0 && ($firsthalf_status == 'OD' || $firsthalf_status == 'COF' || $firsthalf_status == 'PL' || $firsthalf_status == 'CL' || $firsthalf_status == 'SL') ) {
								$halfday_count = $halfday_count + 0.5;
								$total_cnt = $total_cnt + 0.5;
								$present_count = $present_count + 0.5;
							} elseif($tvalue['halfday_status'] != 0 && ($secondhalf_status != 'OD' && $secondhalf_status != 'COF' && $secondhalf_status != 'PL' && $secondhalf_status != 'CL' && $secondhalf_status != 'SL') ){
								//$halfday_count = $halfday_count + 0.5;
								$total_cnt = $total_cnt + 0.5;
							} elseif($tvalue['halfday_status'] != 0 && ($secondhalf_status == 'OD' || $secondhalf_status == 'COF' || $secondhalf_status == 'PL' || $secondhalf_status == 'CL' || $secondhalf_status == 'SL') ) {
								$halfday_count = $halfday_count + 0.5;
								$total_cnt = $total_cnt + 0.5;
								$present_count = $present_count + 0.5;
							}
							// if($tvalue['halfday_status'] != 0){
							// 	$halfday_count = $halfday_count + 0.5;
							// 	$total_cnt = $total_cnt + 0.5;
							// 	$present_count = $present_count + 0.5;
							// }
						} elseif($tvalue['on_duty'] == 1){
							// echo 'in present count 1';
							// echo '<br />';
							$total_cnt = $total_cnt + 1;
							$present_count ++;
						} elseif($tvalue['present_status'] == 1){
							// echo 'in present count 1';
							// echo '<br />';
							$present_count ++;
							$total_cnt = $total_cnt + 1;
						} elseif($tvalue['present_status'] == 0.5){
							// echo 'in present count 0.5';
							// echo '<br />';
							$present_count = $present_count + 0.5;
							$total_cnt = $total_cnt + 0.5;
							if($tvalue['absent_status'] == 0.5){
								$total_cnt = $total_cnt + 0.5;
								$absent_count = $absent_count + 0.5;
							}
							if($tvalue['leave_status'] == 0.5){
								if($tvalue['firsthalf_status'] != '1' && $tvalue['firsthalf_status'] != '0'){
									if($tvalue['firsthalf_status'] == 'PL'){
										$pl_cnt = $pl_cnt + 0.5;
									} elseif($tvalue['firsthalf_status'] == 'CL'){
										$cl_cnt = $cl_cnt + 0.5;
									} elseif($tvalue['firsthalf_status'] == 'SL'){
										$sl_cnt = $sl_cnt + 0.5;
									}
								} elseif($tvalue['secondhalf_status'] != '1' && $tvalue['secondhalf_status'] != '0') {
									if($tvalue['secondhalf_status'] == 'PL'){
										$pl_cnt = $pl_cnt + 0.5;
									} elseif($tvalue['secondhalf_status'] == 'CL'){
										$cl_cnt = $cl_cnt + 0.5;
									} elseif($tvalue['secondhalf_status'] == 'SL'){
										$sl_cnt = $sl_cnt + 0.5;
									}
								}
								$total_cnt = $total_cnt + 0.5;
								$leave_count = $leave_count + 0.5;	
							}
						} elseif($tvalue['absent_status'] == 1){
							// echo 'in absent count 1';
							// echo '<br />';
							$absent_count ++;
							$total_cnt = $total_cnt + 1;
						} elseif($tvalue['absent_status'] == 0.5){
							// echo 'in absent count 0.5';
							// echo '<br />';
							$absent_count = $absent_count + 0.5;
							$total_cnt = $total_cnt + 0.5;
							if($tvalue['present_status'] == 0.5){
								$total_cnt = $total_cnt + 0.5;
								$present_count = $present_count + 0.5;
							}
							if($tvalue['leave_status'] == 0.5){
								if($tvalue['firsthalf_status'] != '1' && $tvalue['firsthalf_status'] != '0'){
									if($tvalue['firsthalf_status'] == 'PL'){
										$pl_cnt = $pl_cnt + 0.5;
									} elseif($tvalue['firsthalf_status'] == 'CL'){
										$cl_cnt = $cl_cnt + 0.5;
									} elseif($tvalue['firsthalf_status'] == 'SL'){
										$sl_cnt = $sl_cnt + 0.5;
									}
								} elseif($tvalue['secondhalf_status'] != '1' && $tvalue['secondhalf_status'] != '0') {
									if($tvalue['secondhalf_status'] == 'PL'){
										$pl_cnt = $pl_cnt + 0.5;
									} elseif($tvalue['secondhalf_status'] == 'CL'){
										$cl_cnt = $cl_cnt + 0.5;
									} elseif($tvalue['secondhalf_status'] == 'SL'){
										$sl_cnt = $sl_cnt + 0.5;
									}
								}
								$total_cnt = $total_cnt + 0.5;
								$leave_count = $leave_count + 0.5;	
							}
						} elseif($tvalue['weekly_off'] != 0){
							// echo 'in weekly_off count';
							// echo '<br />';
							if($tkey == 0){
								$last_key = $tkey;
							} else {
								$last_key = $tkey - 1;
							}
							if($count_t == $tkey){
								$next_key = $tkey;
							} else {
								$next_key = $tkey + 1;	
							}
							if($transaction_datas[$last_key]['absent_status'] == '1' && $transaction_datas[$next_key]['absent_status'] == '1'){
								$absent_count ++;
								$firsthalf_status = 'A';
								$secondhalf_status = 'A';
							} else {
								$week_count ++;
							}
							$total_cnt = $total_cnt + 1;
						} elseif($tvalue['holiday_id'] != 0){
							// echo 'in holiday count';
							// echo '<br />';
							if($tkey == 0){
								$last_key = $tkey;
							} else {
								$last_key = $tkey - 1;
							}
							if($count_t == $tkey){
								$next_key = $tkey;
							} else {
								$next_key = $tkey + 1;	
							}
							if($transaction_datas[$last_key]['absent_status'] == '1' && $transaction_datas[$next_key]['absent_status'] == '1'){
								$absent_count ++;
								$firsthalf_status = 'A';
								$secondhalf_status = 'A';
							} else {
								$holiday_count ++;	
							}
							//$holiday_count ++;
							$total_cnt = $total_cnt + 1;
						} elseif($tvalue['halfday_status'] != 0){
							// echo 'in halffday count';
							// echo '<br />';
							if($tkey == 0){
								$last_key = $tkey;
							} else {
								$last_key = $tkey - 1;
							}
							if($count_t == $tkey){
								$next_key = $tkey;
							} else {
								$next_key = $tkey + 1;	
							}
							if($transaction_datas[$last_key]['absent_status'] == '1' && $transaction_datas[$next_key]['absent_status'] == '1'){
								$absent_count ++;
								$firsthalf_status = 'A';
								$secondhalf_status = 'A';
							} else {
								$halfday_count ++;	
								$present_count ++;
							}
							//$halfday_count ++;
							//$present_count ++;
							$total_cnt = $total_cnt + 1;
						} elseif($tvalue['compli_status'] != 0){
							// echo 'in halffday count';
							// echo '<br />';
							if($tkey == 0){
								$last_key = $tkey;
							} else {
								$last_key = $tkey - 1;
							}
							if($count_t == $tkey){
								$next_key = $tkey;
							} else {
								$next_key = $tkey + 1;	
							}
							if($transaction_datas[$last_key]['absent_status'] == '1' && $transaction_datas[$next_key]['absent_status'] == '1'){
								$absent_count ++;
								$firsthalf_status = 'A';
								$secondhalf_status = 'A';
							} else {
								$compli_count ++;	
								$present_count ++;
							}
							//$present_count ++;
							//$compli_count ++;
							$total_cnt = $total_cnt + 1;
						}

						$performance_data['action'][$tvalue['date']] = array(
							'firsthalf_status' => $firsthalf_status,
							'secondhalf_status' => $secondhalf_status,
							'date' => $tvalue['date'],
						);
					}
					//$total_cnt = count($performance_data['action']);
					foreach ($day as $dkey => $dvalue) {
						foreach ($performance_data as $pkey => $pvalue) {
							// echo '<pre>';
							// print_r($pvalue[$dvalue['date']]['date']);
							// exit;
							if(isset($pvalue[$dvalue['date']]['date'])){
							} else {
								$performance_data['action'][$dvalue['date']] = array(
									'firsthalf_status' => '',
									'secondhalf_status' => '',
									'date' => $dvalue['date']
								);		
							}
						}
					}
					$in = 0;
					if($filter_type == 1){
						if($absent_count > 0){
							$in = 1;
						}
					} elseif($filter_type == 0){
						$in = 1;
					}
					$performance_data = $this->array_sort($performance_data['action'], 'date', SORT_ASC);
					// echo '<pre>';
					// print_r($performance_data);
					// exit;
					//echo $present_count;exit;
					$total_en = $encash_days + $un_encash_days;
					if($in == 1 && $rvalue['grade'] != 'CONSULTANT' && $rvalue['emp_code'] < 50000){
						
						$filter_month = date('n', strtotime($filter_date_end));
						$filter_year = date('Y', strtotime($filter_date_end));
						$number = cal_days_in_month(CAL_GREGORIAN, $filter_month, $filter_year);
						
						// echo $number;
						// echo '<br />';
						// echo '<br />';

						// echo $total_cnt;
						// echo '<br />';
						// echo $present_count;
						// echo '<br />';

						if($total_cnt < $number){
							if($absent_count == $total_cnt){
								$total_cnt = $total_cnt + 1;
								$absent_count = $absent_count + 1;
							} elseif($present_count > 0){
								$total_cnt = $total_cnt + 1;
								$present_count = $present_count + 1;
							} else {
								$total_cnt = $total_cnt + 1;
								$absent_count = $absent_count + 1;
							}
						}

						if($total_cnt > $number){
							if($absent_count == $total_cnt){
								$total_cnt = $total_cnt - 1;
								$absent_count = $absent_count - 1;
							} elseif($present_count > 0){
								$total_cnt = $total_cnt - 1;
								$present_count = $present_count - 1;
							} else {
								$total_cnt = $total_cnt - 1;
								$absent_count = $absent_count - 1;
							}
						}
						
						// echo $total_cnt;
						// echo '<br />';
						// echo $present_count;
						// echo '<br />';
						// exit;

						if($absent_count > $number){
							$absent_count = $number;			
						}

						if($pl_cnt > $number){
							$pl_cnt = $number;			
						}

						if($cl_cnt > $number){
							$cl_cnt = $number;			
						}

						if($sl_cnt > $number){
							$sl_cnt = $number;			
						}

						$final_datas[$rvalue['emp_code']]['week_count'] = $week_count;
						$final_datas[$rvalue['emp_code']]['holiday_count'] = $holiday_count;
						//$final_datas[$rvalue['emp_code']]['basic_data']['halfday_count'] = $halfday_count;
						//$final_datas[$rvalue['emp_code']]['basic_data']['leave_count'] = $leave_count;
						$final_datas[$rvalue['emp_code']]['present_count'] = $present_count;
						$final_datas[$rvalue['emp_code']]['absent_count'] = $absent_count;
						$final_datas[$rvalue['emp_code']]['total_cnt'] = $total_cnt;
						$final_datas[$rvalue['emp_code']]['pl_cnt'] = $pl_cnt;
						$final_datas[$rvalue['emp_code']]['sl_cnt'] = $sl_cnt;
						$final_datas[$rvalue['emp_code']]['cl_cnt'] = $cl_cnt;
						$final_datas[$rvalue['emp_code']]['encash'] = $encash_days + $un_encash_days;
						$final_datas[$rvalue['emp_code']]['month_year'] = $month_year;//$encash_days + $un_encash_days;
						//$final_datas[$rvalue['emp_code']]['tran_data']['action'] = $performance_data;
					} else {
						unset($final_datas[$rvalue['emp_code']]);
					}
				} else {
					unset($final_datas[$rvalue['emp_code']]);
				}
			}
			//exit;
		//}

		//$final_datas = array();
		//$this->data['final_datas'] = $final_datas;
		
		//$final_datass = array_chunk($final_datas, 43);

		//echo '<pre>';
		//print_r($final_datas);
		//exit;
		
		if($final_datas){
			// if($filter_type == 1){
			// 	$report_type = 'Defult';
			// } elseif($filter_type == 2){
			// 	$report_type = 'Late';
			// } elseif ($filter_type == 3) {
			// 	$report_type = 'Less Time';
			// }
			//$month = date("F", mktime(0, 0, 0, $filter_month, 10));
			//echo 'In Progress';exit;
			$url_curl = "http://192.168.0.150:8014/attendance_api/accept_data.php";
			//$fields_string = '';
			//url-ify the data for the POST
			// foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
			// $fields_string = rtrim($fields_string, '&');
			$data = json_encode($final_datas);
			//open connection
			$ch = curl_init($url_curl);
			//set the url, number of POST vars, POST data
			//curl_setopt($ch,CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			    'Content-Type: application/json',                                                                                
			    'Content-Length: ' . strlen($data))                                                                       
			); 
			curl_setopt($ch,CURLOPT_POSTFIELDS, $data);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			//execute post
			$result = curl_exec($ch);
			//close connection
			curl_close($ch);
			//decode the result
			$dat = json_decode($result, true);
			//echo 'aaaaaaaaaaa';
			//echo '<br />';
			//echo '<pre>';
			//print_r($result);
			//echo '<pre>';
			//print_r($dat);
			//exit;
			if(!isset($dat['connection']) && !isset($dat['query_error'])){
				$this->session->data['success'] = 'Data inserted sucessfully, Records Updated : ' . $dat['rows_affected'];
			} else {
				$this->session->data['warning'] = 'Please try again';
			}
			$this->redirect($this->url->link('report/muster', 'token=' . $this->session->data['token'].$url, 'SSL'));
		} else {
			$this->session->data['warning'] = 'No Data Found';
			$this->redirect($this->url->link('report/muster', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}
	}
}
?>