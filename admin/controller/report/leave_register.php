<?php
class ControllerReportLeaveRegister extends Controller { 
	public function index() {  

		if(isset($this->session->data['emp_code'])){
			$emp_code = $this->session->data['emp_code'];
			$is_set = $this->db->query("SELECT `is_set` FROM `oc_employee` WHERE `emp_code` = '".$emp_code."' ")->row['is_set'];	
			if($is_set == '1'){
				//$this->redirect($this->url->link('transaction/leave_ess', 'token=' . $this->session->data['token'], 'SSL'));
			} else {
				$this->redirect($this->url->link('user/password_change', 'token=' . $this->session->data['token'].'&emp_code='.$emp_code, 'SSL'));
			}
		} elseif(isset($this->session->data['is_dept'])){
			$emp_code = $this->session->data['d_emp_id'];
			$is_set = $this->db->query("SELECT `is_set` FROM `oc_employee` WHERE `emp_code` = '".$emp_code."' ")->row['is_set'];	
			if($is_set == '1'){
				//$this->redirect($this->url->link('transaction/leave_ess', 'token=' . $this->session->data['token'], 'SSL'));
			} else {
				$this->redirect($this->url->link('user/password_change', 'token=' . $this->session->data['token'].'&emp_code='.$emp_code, 'SSL'));
			}
		} elseif(isset($this->session->data['is_super'])){
			$emp_code = $this->session->data['d_emp_id'];
			$is_set = $this->db->query("SELECT `is_set` FROM `oc_employee` WHERE `emp_code` = '".$emp_code."' ")->row['is_set'];	
			if($is_set == '1'){
				//$this->redirect($this->url->link('transaction/leave_ess', 'token=' . $this->session->data['token'], 'SSL'));
			} else {
				$this->redirect($this->url->link('user/password_change', 'token=' . $this->session->data['token'].'&emp_code='.$emp_code, 'SSL'));
			}
		}

		$this->language->load('report/leave_register');
		$this->load->model('report/common_report');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			$filter_date_start = date('Y').'-01-01';//date('Y-m-d');
			//$filter_date_start = date('Y-m-d', strtotime($from . "-30 day"));
		}

		if (isset($this->request->get['filter_year'])) {
			$filter_year = $this->request->get['filter_year'];
		} else {
			$filter_year = '2017';
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} elseif(isset($this->session->data['emp_code'])){
			$emp_name = $this->db->query("SELECT `name` FROM `oc_employee` WHERE `emp_code` = '".$this->session->data['emp_code']."' ")->row['name'];
			$filter_name = $emp_name;
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} elseif(isset($this->session->data['emp_code'])){
			$filter_name_id = $this->session->data['emp_code'];
		}  else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['unit'])) {
			$unit = $this->request->get['unit'];
		} else {
			$unit = 0;
		}

		if (isset($this->request->get['filter_leave'])) {
			$filter_leave = $this->request->get['filter_leave'];
		} else {
			$filter_leave = '0';
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
			$filter_date_end1 = $this->model_report_common_report->getfilter_date_end($filter_date_start, $filter_name_id, $unit);
			if(strtotime($filter_date_end) > strtotime($filter_date_end1) ){
				$filter_date_end = $filter_date_end1;
			}
		} else {
			$filter_date_end = date('Y-m-d');
		}

		if(isset($this->session->data['dept_names'])){
			$filter_departments = html_entity_decode($this->session->data['dept_names']);
			$filter_departments = "'" . str_replace(",", "','", $filter_departments) . "'";
		} else {
			$filter_departments = '';
		}

		if (isset($this->request->get['department'])) {
			$department = html_entity_decode($this->request->get['department']);
		} elseif(isset($this->session->data['dept_name'])){
			$department = $this->session->data['dept_name'];
		} else {
			$department = 0;
		}

		if (isset($this->request->get['group'])) {
			$group = $this->request->get['group'];
		} else {
			$group = 0;
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_year'])) {
			$url .= '&filter_year=' . $this->request->get['filter_year'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_type'])) {
			$url .= '&filter_type=' . $this->request->get['filter_type'];
		}

		if (isset($this->request->get['filter_leave'])) {
			$url .= '&filter_leave=' . $this->request->get['filter_leave'];
		}

		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}

		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}

		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('report/leave_register', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->load->model('report/common_report');
		$this->load->model('report/attendance');
		$this->load->model('catalog/employee');

		$this->data['attendace'] = array();

		$data = array(
			'filter_date_start'	     => $filter_year.'-01-01',
			'filter_year'	     	 => $filter_year,
			'filter_date_end'	     => $filter_date_end,
			'filter_name'	     	 => $filter_name,
			'filter_name_id'	     => $filter_name_id,
			'unit'					 => $unit,
			'filter_departments'	 => $filter_departments,
			'department'			 => $department,
			'group'					 => $group, 
			'start'                  => ($page - 1) * 7000,
			'limit'                  => 7000
		);

		$final_datas = array();
		// echo '<pre>';
		// print_r($data);
		// exit;
		//$from_year = date('Y', strtotime($filter_date_start));
		$from_year = $filter_year;

		if(isset($this->request->get['once']) && $this->request->get['once'] == 1){
			$results = $this->model_report_common_report->getemployees($data);
			foreach ($results as $rkey => $rvalue) {
				$performance_data = array();
				$transaction_datas = $this->model_report_common_report->getleave_transaction_data_group_ess($rvalue['emp_code'], $data);
				
				$leave_datas = $this->model_report_common_report->getleave_data($rvalue['emp_code'], $from_year);
				if(isset($leave_datas['pl_acc'])){
					$emp_code = $rvalue['emp_code'];
					$final_datas[$rvalue['emp_code']]['basic_data']['name'] = $rvalue['name'];
					$final_datas[$rvalue['emp_code']]['basic_data']['department'] = $rvalue['department'];
					$final_datas[$rvalue['emp_code']]['basic_data']['emp_code'] = $rvalue['emp_code'];
					$final_datas[$rvalue['emp_code']]['basic_data']['pl_open'] = $leave_datas['pl_acc'];
					$final_datas[$rvalue['emp_code']]['basic_data']['sl_open'] = $leave_datas['sl_acc'];
					$final_datas[$rvalue['emp_code']]['basic_data']['bl_open'] = $leave_datas['bl_acc'];
					$final_datas[$rvalue['emp_code']]['basic_data']['ml_open'] = $leave_datas['ml_acc'];
					$final_datas[$rvalue['emp_code']]['basic_data']['mal_open'] = $leave_datas['mal_acc'];
					$final_datas[$rvalue['emp_code']]['basic_data']['pal_open'] = $leave_datas['pal_acc'];
					$final_datas[$rvalue['emp_code']]['basic_data']['lwp_open'] = $leave_datas['lwp_acc'];
					//$a = explode('-', $filter_date_start);
					$a = $filter_year;
					if($a == '2015'){
						$final_datas[$rvalue['emp_code']]['basic_data']['open_date'] = '01/11/2015';
					} else {
						$final_datas[$rvalue['emp_code']]['basic_data']['open_date'] = '01/01/'.date('Y');
					}
					$total_days_pl = 0;
					$total_days_bl = 0;
					$total_days_sl = 0;
					$total_days_ml = 0;
					$total_days_mal = 0;
					$total_days_pal = 0;
					$total_days_lwp = 0;

					$un_total_days_pl = 0;
					$un_total_days_bl = 0;
					$un_total_days_sl = 0;
					$un_total_days_ml = 0;
					$un_total_days_mal = 0;
					$un_total_days_pal = 0;
					$un_total_days_lwp = 0;
					
					if($transaction_datas){
						foreach($transaction_datas as $tkey => $tvalue){
							$days_pl = 0;
							$days_bl = 0;
							$days_sl = 0;
							$days_ml = 0;
							$days_mal = 0;
							$days_pal = 0;
							$days_lwp = 0;
							$t_datas = $this->model_report_common_report->getleave_transaction_data_ess($tvalue['batch_id']);	
							usort($t_datas, array($this, "date_compare"));
							end($t_datas);
							$e_key = key($t_datas);
							$start_date = $t_datas[0]['date'];
							$end_date = $t_datas[$e_key]['date'];
							// echo '<pre>';
							// print_r($t_datas);
							// exit;
							$days = 0;
							foreach ($t_datas as $ttkey => $ttvalue) {
								if($ttvalue['p_status'] == 1){
									$days ++;
								}
							}

							if($t_datas[0]['type'] != ''){
								$leave_types = $t_datas[0]['type'];
							} else {
								$leave_types = 'F';
							}				
							$en_days_pl = 0;
							if($t_datas[0]['leave_type'] == 'PL'){
								if($t_datas[0]['type'] != ''){
									if($t_datas[0]['type'] == 'F'){
										$days_pl = 1;
									} else {
										$days_pl = 0.5;
									}
								} else {
									$days_pl = $days;
									//if($tkey == 0){
										if($t_datas[0]['encash'] != ''){
											$en_days_pl = $t_datas[0]['encash'];
										}
									//}
								}
							} elseif($t_datas[0]['leave_type'] == 'BL'){
								if($t_datas[0]['type'] != ''){
									if($t_datas[0]['type'] == 'F'){
										$days_bl = 1;
									} else {
										$days_bl = 0.5;
									}
								} else {
									$days_bl = $days;
								}
							} elseif($t_datas[0]['leave_type'] == 'SL'){
								if($t_datas[0]['type'] != ''){
									if($t_datas[0]['type'] == 'F'){
										$days_sl = 1;
									} else {
										$days_sl = 0.5;
									}
								} else {
									$days_sl = $days;
								}
							} elseif($t_datas[0]['leave_type'] == 'ML'){
								if($t_datas[0]['type'] != ''){
									if($t_datas[0]['type'] == 'F'){
										$days_ml = 1;
									} else {
										$days_ml = 0.5;
									}
								} else {
									$days_ml = $days;
								}
							} elseif($t_datas[0]['leave_type'] == 'MAL'){
								if($t_datas[0]['type'] != ''){
									if($t_datas[0]['type'] == 'F'){
										$days_mal = 1;
									} else {
										$days_mal = 0.5;
									}
								} else {
									$days_mal = $days;
								}
							} elseif($t_datas[0]['leave_type'] == 'PAL'){
								if($t_datas[0]['type'] != ''){
									if($t_datas[0]['type'] == 'F'){
										$days_pal = 1;
									} else {
										$days_pal = 0.5;
									}
								} else {
									$days_pal = $days;
								}
							} elseif($t_datas[0]['leave_type'] == 'LWP'){
								if($t_datas[0]['type'] != ''){
									if($t_datas[0]['type'] == 'F'){
										$days_lwp = 1;
									} else {
										$days_lwp = 0.5;
									}
								} else {
									$days_lwp = $days;
								}
							}
							$encash_status = 0;
							$encash = 0;
							if($t_datas[0]['encash'] != ''){
								$encash_status = 1;
								$encash = $t_datas[0]['encash'];
							}
							// $encash_days_sql = "SELECT * FROM `oc_leave_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `p_status` = '0' AND `leave_type` = 'PL' ";
							// $encash_datas = $this->db->query($encash_days_sql);
							// if($encash_datas->num_rows > 0){
							// 	$encash_data = $encash_datas->row;
							// 	if($encash_data['encash'] != ''){
							// 		$encash_status = 1;
							// 		$en_days_pl = $en_days_pl + $encash_data['encash'];
							// 		$encash = $encash + $encash_data['encash'];
							// 	}
							// }
							$total_days_pl = $total_days_pl + $days_pl + $en_days_pl;
							$total_days_bl = $total_days_bl + $days_bl;
							$total_days_sl = $total_days_sl + $days_sl;
							$total_days_ml = $total_days_ml + $days_ml;
							$total_days_mal = $total_days_mal + $days_mal;
							$total_days_pal = $total_days_pal + $days_pal;
							$total_days_lwp = $total_days_lwp + $days_lwp;
							$performance_data['action'][$tvalue['date']] = array(
								'start_date' => date('d/m/Y', strtotime($start_date)),
								'end_date' => date('d/m/Y', strtotime($end_date)),
								'days_pl' => $days_pl,
								'days_bl' => $days_bl,
								'days_sl' => $days_sl,
								'days_ml' => $days_ml,
								'days_mal' => $days_mal,
								'days_pal' => $days_pal,
								'days_lwp' => $days_lwp,
								'leave_types' => $leave_types,
								'encash_status' => $encash_status,
								'encash' => $encash,
								'performance_stat' => '1'
							);
						}
						$in = 0;
						$sql = "SELECT * FROM `oc_leave_transaction_temp` WHERE 1=1";
						$sql .= " AND emp_id = '".$emp_code."' AND `p_status` = '0' AND `a_status` = '1' AND `encash` <> '' ";
						$sql .= " AND `dot` >= '".$data['filter_date_start']."' AND `dot` <= '".$data['filter_date_end']."' GROUP BY `batch_id` ";
						$query = $this->db->query($sql);
						$encash = 0;
						foreach ($query->rows as $key => $value) {
							$sql1 = "SELECT * FROM `oc_leave_transaction_temp` WHERE 1=1";
							$sql1 .= " AND emp_id = '".$emp_code."' AND `batch_id` = '".$value['batch_id']."' ";	
							$query1 = $this->db->query($sql1);
							$p_status = 0;
							foreach($query1->rows as $qkey => $qvalue){
								if($qvalue['p_status'] == 1){
									$p_status = 1;
									break;
								}
							}
							if($p_status == 0){
								$in = 1;
								//$en_days_pl = $en_days_pl + $value['encash'];
								$encash = $encash + $value['encash'];
							}
						}

						if($in == 1){
							$encash_data = $query->row;
							$encash_status = 1;
							$total_days_pl = $total_days_pl + $encash_data['encash'];
							$start_date = $encash_data['date'];
							$end_date = $encash_data['date'];
							$performance_data['action'][$encash_data['date']] = array(
								'start_date' => date('d/m/Y', strtotime($start_date)),
								'end_date' => date('d/m/Y', strtotime($end_date)),
								'days_pl' => 0,
								'days_bl' => 0,
								'days_sl' => 0,
								'days_ml' => 0,
								'days_mal' => 0,
								'days_pal' => 0,
								'days_lwp' => 0,
								'leave_types' => 'F',
								'encash_status' => $encash_status,
								'encash' => $encash_data['encash'],
								'performance_stat' => '0'
							);
						}
					} else {
						$total_days_pl = 0;
						$total_days_bl = 0;
						$total_days_sl = 0;
						$total_days_ml = 0;
						$total_days_mal = 0;
						$total_days_pal = 0;
						$total_days_lwp = 0;
						$in = 0;
						$sql = "SELECT * FROM `oc_leave_transaction_temp` WHERE 1=1";
						$sql .= " AND emp_id = '".$emp_code."' AND `p_status` = '0' AND `a_status` = '1' AND `encash` <> '' ";
						$sql .= " AND `dot` >= '".$data['filter_date_start']."' AND `dot` <= '".$data['filter_date_end']."' GROUP BY `batch_id` ";
						$query = $this->db->query($sql);
						$encash = 0;
						foreach ($query->rows as $key => $value) {
							$sql1 = "SELECT * FROM `oc_leave_transaction_temp` WHERE 1=1";
							$sql1 .= " AND emp_id = '".$emp_code."' AND `batch_id` = '".$value['batch_id']."' ";	
							$query1 = $this->db->query($sql1);
							$p_status = 0;
							foreach($query1->rows as $qkey => $qvalue){
								if($qvalue['p_status'] == 1){
									$p_status = 1;
									break;
								}
							}
							if($p_status == 0){
								$in = 1;
								//$en_days_pl = $en_days_pl + $value['encash'];
								$encash = $encash + $value['encash'];
							}
						}

						if($in == 1){
							$encash_data = $query->row;
							$encash_status = 1;
							$total_days_pl = $encash_data['encash'];
							$start_date = $encash_data['date'];
							$end_date = $encash_data['date'];
							$performance_data['action'][$encash_data['date']] = array(
								'start_date' => date('d/m/Y', strtotime($start_date)),
								'end_date' => date('d/m/Y', strtotime($end_date)),
								'days_pl' => 0,
								'days_bl' => 0,
								'days_sl' => 0,
								'days_ml' => 0,
								'days_mal' => 0,
								'days_pal' => 0,
								'days_lwp' => 0,
								'leave_types' => 'F',
								'encash_status' => $encash_status,
								'encash' => $encash_data['encash'],
								'performance_stat' => '0'
							);
						}
						// $encash_days_sql = "SELECT * FROM `oc_leave_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `p_status` = '0' AND `leave_type` = 'PL' ";
						// $encash_datas = $this->db->query($encash_days_sql);
						// if($encash_datas->num_rows > 0){
						// 	$encash_data = $encash_datas->row;
						// 	if($encash_data['encash'] != ''){
						// 		$encash_status = 1;
						// 		$total_days_pl = $encash_data['encash'];
						// 		$start_date = $encash_data['date'];
						// 		$end_date = $encash_data['date'];
						// 		$performance_data['action'][$encash_data['date']] = array(
						// 			'start_date' => date('d/m/Y', strtotime($start_date)),
						// 			'end_date' => date('d/m/Y', strtotime($end_date)),
						// 			'days_pl' => 0,
						// 			'days_cl' => 0,
						// 			'days_sl' => 0,
						// 			'leave_types' => 'F',
						// 			'encash_status' => $encash_status,
						// 			'encash' => $encash_data['encash'],
						// 			'performance_stat' => '0'
						// 		);		
						// 	}
						// }						
					}

					$sql = "SELECT * FROM `oc_leave_transaction_temp` WHERE 1=1";
					$a = date('Y', strtotime($data['filter_date_start']));					
					$filter_end = $a.'-12-31';
					if (!empty($data['filter_date_start'])) {
						$sql .= " AND DATE(`date`) >= '" . $this->db->escape($data['filter_date_start']) . "'";
					}
					if (!empty($data['filter_date_end'])) {
						$sql .= " AND DATE(`date`) <= '" . $this->db->escape($filter_end) . "'";
					}
					if (!empty($data['unit'])) {
						$sql .= " AND LOWER(`unit`) = '" . $this->db->escape(strtolower($data['unit'])) . "'";
					}
					if (isset($data['filter_leave']) && !empty($data['filter_leave'])) {
						$sql .= " AND LOWER(`leave_type`) = '" . $this->db->escape(strtolower($data['filter_leave'])) . "'";
					}
					//$sql .= " AND emp_id = '".$emp_code."' AND `p_status` = '0' AND `a_status` = '1' AND (`leave_type` = 'PL' OR `leave_type` = 'CL' OR `leave_type` = 'SL') GROUP BY `batch_id` ";
					$sql .= " AND emp_id = '".$emp_code."' AND `p_status` = '0' AND `a_status` = '1' GROUP BY `batch_id` ";
					//echo $sql;exit;
					$un_transaction_datas = $this->db->query($sql)->rows;
					
					foreach($un_transaction_datas as $tkey => $tvalue){
						$un_days_pl = 0;
						$un_days_bl = 0;
						$un_days_sl = 0;
						$un_days_ml = 0;
						$un_days_mal = 0;
						$un_days_pal = 0;
						$un_days_lwp = 0;
						$un_t_datas = $this->model_report_common_report->getleave_transaction_data_ess($tvalue['batch_id']);	
						usort($un_t_datas, array($this, "date_compare"));
						end($un_t_datas);
						$un_e_key = key($un_t_datas);
						$un_start_date = $un_t_datas[0]['date'];
						$un_end_date = $un_t_datas[$un_e_key]['date'];
						// echo '<pre>';
						// print_r($t_datas);
						// exit;
						$un_days = 0;
						foreach ($un_t_datas as $ttkey => $ttvalue) {
							if($ttvalue['p_status'] == 0){
								$un_days ++;
							}
						}

						if($un_t_datas[0]['type'] != ''){
							$un_leave_types = $un_t_datas[0]['type'];
						} else {
							$un_leave_types = 'F';
						}				
						$un_en_days_pl = 0;
						if($un_t_datas[0]['leave_type'] == 'PL'){
							if($un_t_datas[0]['type'] != ''){
								if($un_t_datas[0]['type'] == 'F'){
									$un_days_pl = 1;
								} else {
									$un_days_pl = 0.5;
								}
							} else {
								$un_days_pl = $un_days;
							}
						} elseif($un_t_datas[0]['leave_type'] == 'BL'){
							if($un_t_datas[0]['type'] != ''){
								if($un_t_datas[0]['type'] == 'F'){
									$un_days_bl = 1;
								} else {
									$un_days_bl = 0.5;
								}
							} else {
								$un_days_bl = $un_days;
							}
						} elseif($un_t_datas[0]['leave_type'] == 'SL'){
							if($un_t_datas[0]['type'] != ''){
								if($un_t_datas[0]['type'] == 'F'){
									$un_days_sl = 1;
								} else {
									$un_days_sl = 0.5;
								}
							} else {
								$un_days_sl = $un_days;
							}
						} elseif($un_t_datas[0]['leave_type'] == 'ML'){
							if($un_t_datas[0]['type'] != ''){
								if($un_t_datas[0]['type'] == 'F'){
									$un_days_ml = 1;
								} else {
									$un_days_ml = 0.5;
								}
							} else {
								$un_days_ml = $un_days;
							}
						} elseif($un_t_datas[0]['leave_type'] == 'MAL'){
							if($un_t_datas[0]['type'] != ''){
								if($un_t_datas[0]['type'] == 'F'){
									$un_days_mal = 1;
								} else {
									$un_days_mal = 0.5;
								}
							} else {
								$un_days_mal = $un_days;
							}
						} elseif($un_t_datas[0]['leave_type'] == 'PAL'){
							if($un_t_datas[0]['type'] != ''){
								if($un_t_datas[0]['type'] == 'F'){
									$un_days_pal = 1;
								} else {
									$un_days_pal = 0.5;
								}
							} else {
								$un_days_pal = $un_days;
							}
						} elseif($un_t_datas[0]['leave_type'] == 'LWP'){
							if($un_t_datas[0]['type'] != ''){
								if($un_t_datas[0]['type'] == 'F'){
									$un_days_lwp = 1;
								} else {
									$un_days_lwp = 0.5;
								}
							} else {
								$un_days_lwp = $un_days;
							}
						}
						
						
						$un_total_days_pl = $un_total_days_pl + $un_days_pl + $un_en_days_pl;
						$un_total_days_bl = $un_total_days_bl + $un_days_bl;
						$un_total_days_sl = $un_total_days_sl + $un_days_sl;
						$un_total_days_ml = $un_total_days_ml + $un_days_ml;
						$un_total_days_mal = $un_total_days_mal + $un_days_mal;
						$un_total_days_pal = $un_total_days_pal + $un_days_pal;
						$un_total_days_lwp = $un_total_days_lwp + $un_days_lwp;
					}
					
					$final_datas[$rvalue['emp_code']]['basic_data']['total_days_pl'] = $total_days_pl;
					$final_datas[$rvalue['emp_code']]['basic_data']['total_days_bl'] = $total_days_bl;
					$final_datas[$rvalue['emp_code']]['basic_data']['total_days_sl'] = $total_days_sl;
					$final_datas[$rvalue['emp_code']]['basic_data']['total_days_ml'] = $total_days_ml;
					$final_datas[$rvalue['emp_code']]['basic_data']['total_days_mal'] = $total_days_mal;
					$final_datas[$rvalue['emp_code']]['basic_data']['total_days_pal'] = $total_days_pal;
					$final_datas[$rvalue['emp_code']]['basic_data']['total_days_lwp'] = $total_days_lwp;

					$final_datas[$rvalue['emp_code']]['basic_data']['un_total_days_pl'] = $un_total_days_pl;
					$final_datas[$rvalue['emp_code']]['basic_data']['un_total_days_bl'] = $un_total_days_bl;
					$final_datas[$rvalue['emp_code']]['basic_data']['un_total_days_sl'] = $un_total_days_sl;
					$final_datas[$rvalue['emp_code']]['basic_data']['un_total_days_ml'] = $un_total_days_ml;
					$final_datas[$rvalue['emp_code']]['basic_data']['un_total_days_mal'] = $un_total_days_mal;
					$final_datas[$rvalue['emp_code']]['basic_data']['un_total_days_pal'] = $un_total_days_pal;
					$final_datas[$rvalue['emp_code']]['basic_data']['un_total_days_lwp'] = $un_total_days_lwp;
					
					$closing_pl = $leave_datas['pl_acc'] - $total_days_pl;
					$closing_bl = $leave_datas['bl_acc'] - $total_days_bl;
					$closing_sl = $leave_datas['sl_acc'] - $total_days_sl;
					$closing_ml = $leave_datas['ml_acc'] - $total_days_ml;
					$closing_mal = $leave_datas['mal_acc'] - $total_days_mal;
					$closing_pal = $leave_datas['pal_acc'] - $total_days_pal;
					$closing_lwp = $leave_datas['lwp_acc'] - $total_days_lwp;
					
					$final_datas[$rvalue['emp_code']]['basic_data']['un_total_bal_pl'] = $closing_pl - $un_total_days_pl;
					$final_datas[$rvalue['emp_code']]['basic_data']['un_total_bal_bl'] = $closing_bl - $un_total_days_bl;
					$final_datas[$rvalue['emp_code']]['basic_data']['un_total_bal_sl'] = $closing_sl - $un_total_days_sl;
					$final_datas[$rvalue['emp_code']]['basic_data']['un_total_bal_ml'] = $closing_ml - $un_total_days_ml;
					$final_datas[$rvalue['emp_code']]['basic_data']['un_total_bal_mal'] = $closing_mal - $un_total_days_mal;
					$final_datas[$rvalue['emp_code']]['basic_data']['un_total_bal_pal'] = $closing_pal - $un_total_days_pal;
					$final_datas[$rvalue['emp_code']]['basic_data']['un_total_bal_lwp'] = $closing_lwp - $un_total_days_lwp;
	
					$final_datas[$rvalue['emp_code']]['basic_data']['closing_pl'] = $closing_pl;
					$final_datas[$rvalue['emp_code']]['basic_data']['closing_bl'] = $closing_bl;
					$final_datas[$rvalue['emp_code']]['basic_data']['closing_sl'] = $closing_sl;
					$final_datas[$rvalue['emp_code']]['basic_data']['closing_ml'] = $closing_ml;
					$final_datas[$rvalue['emp_code']]['basic_data']['closing_mal'] = $closing_mal;
					$final_datas[$rvalue['emp_code']]['basic_data']['closing_pal'] = $closing_pal;
					$final_datas[$rvalue['emp_code']]['basic_data']['closing_lwp'] = $closing_lwp;
					
					$final_datas[$rvalue['emp_code']]['tran_data'] = $performance_data;
				}
			}
			//exit;
		}
		//$final_datas = array();
		$this->data['final_datas'] = $final_datas;
		
		// echo '<pre>';
		// print_r($this->data['final_datas']);
		// exit;

		$leaves = array(
			'0'  => 'All',
			'PL' => 'PL',
			'BL' => 'BL',
			'SL' => 'SL',
			'ML' => 'ML',
			'MAL' => 'MAL',
			'PAL' => 'PAL',
			'LWP' => 'LWP', 
		);

		$this->data['leaves'] = $leaves;

		$unit_data = array(
			'0' => 'All',
			'Mumbai' => 'Mumbai',
			'Pune' => 'Pune',
			'Delhi' => 'Delhi',
			'Chennai' => 'Chennai',
			'Bangalore' => 'Bangalore',
			'Ahmedabad' => 'Ahmedabad',
		);

		$this->data['unit_data'] = $unit_data;
		
		$department_datas = $this->model_report_attendance->getdepartment_list();
		$department_data = array();
		$department_data['0'] = 'All';
		foreach ($department_datas as $dkey => $dvalue) {
			$department_data[$dvalue['department']] = $dvalue['department'];
		}
		$this->data['department_data'] = $department_data;
		
		$group_datas = $this->model_report_attendance->getgroup_list();
		$group_data = array();
		$group_data['0'] = 'All';
		foreach ($group_datas as $gkey => $gvalue) {
			$group_data[$gvalue['group']] = $gvalue['group'];
		}
		$this->data['group_data'] = $group_data;
		
		if(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_all_status'] = $this->language->get('text_all_status');

		
		$this->data['entry_date_start'] = $this->language->get('entry_date_start');
		$this->data['entry_date_end'] = $this->language->get('entry_date_end');
		
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['button_export'] = $this->language->get('button_export');

		$this->data['token'] = $this->session->data['token'];

		if(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		// if (isset($this->request->get['filter_month'])) {
		// 	$url .= '&filter_month=' . $this->request->get['filter_month'];
		// }

		if (isset($this->request->get['filter_year'])) {
			$url .= '&filter_year=' . $this->request->get['filter_year'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_type'])) {
			$url .= '&filter_type=' . $this->request->get['filter_type'];
		}

		if (isset($this->request->get['filter_leave'])) {
			$url .= '&filter_leave=' . $this->request->get['filter_leave'];
		}

		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}

		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}

		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}

		$months = array(
			'01' => 'January',
			'02' => 'Feburary',
			'03' => 'March',
			'04' => 'April',
			'05' => 'May',
			'06' => 'June',
			'07' => 'July',
			'08' => 'August',
			'09' => 'September',
			'10' => 'October',
			'11' => 'November',
			'12' => 'December'
		);

		$this->data['months'] = $months;

		if(isset($this->session->data['is_dept']) && $this->session->data['is_dept'] == '1'){
			$user_dept = 1;
			$is_dept = 1;
		} elseif(isset($this->session->data['is_user'])){
			$user_dept = 1;
			$is_dept = 0;
		} else {
			$user_dept = 0;
			$is_dept = 0;
		}
		$this->data['user_dept'] = $user_dept;
		$this->data['is_dept'] = $is_dept;

		$this->data['filter_date_start'] = $filter_date_start;
		$this->data['filter_date_end'] = $filter_date_end;
		// $this->data['filter_month'] = $filter_month;
		$this->data['filter_year'] = $filter_year;
		$this->data['filter_name'] = $filter_name;
		$this->data['filter_name_id'] = $filter_name_id;
		$this->data['filter_leave'] = $filter_leave;
		$this->data['unit'] = $unit;
		$this->data['department'] = $department;
		$this->data['group'] = $group;

		$this->template = 'report/leave_register.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	public function date_compare($a, $b){
	    $t1 = strtotime($a['date']);
	    $t2 = strtotime($b['date']);
	    return $t1 - $t2;
	}

	function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	}

	function array_sort($array, $on, $order=SORT_ASC){

		$new_array = array();
		$sortable_array = array();

		if (count($array) > 0) {
			foreach ($array as $k => $v) {
				if (is_array($v)) {
					foreach ($v as $k2 => $v2) {
						if ($k2 == $on) {
							$sortable_array[$k] = $v2;
						}
					}
				} else {
					$sortable_array[$k] = $v;
				}
			}

			switch ($order) {
				case SORT_ASC:
					asort($sortable_array);
					break;
				case SORT_DESC:
					arsort($sortable_array);
					break;
			}

			foreach ($sortable_array as $k => $v) {
				$new_array[$k] = $array[$k];
			}
		}

		return $new_array;
	}

	public function export(){
		$this->language->load('report/leave_register');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			$from = date('Y-m-d');
			$filter_date_start = date('Y-m-d', strtotime($from . "-30 day"));
		}

		if (isset($this->request->get['filter_year'])) {
			$filter_year = $this->request->get['filter_year'];
		} else {
			$filter_year = '2017';
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = date('Y-m-d');
		}
		
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['unit'])) {
			$unit = $this->request->get['unit'];
		} else {
			$unit = 0;
		}

		if (isset($this->request->get['department'])) {
			$department = html_entity_decode($this->request->get['department']);
		} else {
			$department = 0;
		}

		if(isset($this->session->data['dept_names'])){
			$filter_departments = html_entity_decode($this->session->data['dept_names']);
			$filter_departments = "'" . str_replace(",", "','", html_entity_decode($filter_departments)) . "'";
		} else {
			$filter_departments = '';
		}

		if (isset($this->request->get['filter_leave'])) {
			$filter_leave = $this->request->get['filter_leave'];
			// if($filter_leave == '0'){
			// 	$filter_leave = 'PL';
			// }
		} else {
			$filter_leave = '0';
		}

		if (isset($this->request->get['group'])) {
			$group = $this->request->get['group'];
		} else {
			$group = 0;
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_year'])) {
			$url .= '&filter_year=' . $this->request->get['filter_year'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_type'])) {
			$url .= '&filter_type=' . $this->request->get['filter_type'];
		}

		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}

		if (isset($this->request->get['filter_leave'])) {
			$url .= '&filter_leave=' . $this->request->get['filter_leave'];
		}

		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}

		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$url .= '&once=1';

		$this->load->model('report/common_report');
		$this->load->model('report/attendance');
		$this->load->model('catalog/employee');

		$this->data['attendace'] = array();

		$data = array(
			'filter_date_start'	     => $filter_year.'-01-01',
			'filter_year'	     	 => $filter_year,
			'filter_date_end'	     => $filter_date_end,
			'filter_name'	     	 => $filter_name,
			'filter_name_id'	     => $filter_name_id,
			'filter_leave'	     	 => $filter_leave,
			'unit'					 => $unit,
			'department'			 => $department,
			'filter_departments'     => $filter_departments,
			'group'					 => $group, 
			'start'                  => ($page - 1) * 7000,
			'limit'                  => 7000
		);

		
        $final_datas = array();
		// echo '<pre>';
		// print_r($data);
		// exit;
		//$from_year = date('Y', strtotime($filter_date_start));
		$from_year = $filter_year;
		
		$results = $this->model_report_common_report->getemployees($data);
		foreach ($results as $rkey => $rvalue) {
			$performance_data = array();
			$transaction_datas = $this->model_report_common_report->getleave_transaction_data_group_ess($rvalue['emp_code'], $data);
			
			$leave_datas = $this->model_report_common_report->getleave_data($rvalue['emp_code'], $from_year);
			if(isset($leave_datas['pl_acc'])){
				$emp_code = $rvalue['emp_code'];
				$final_datas[$rvalue['emp_code']]['basic_data']['name'] = $rvalue['name'];
				$final_datas[$rvalue['emp_code']]['basic_data']['department'] = $rvalue['department'];
				$final_datas[$rvalue['emp_code']]['basic_data']['emp_code'] = $rvalue['emp_code'];
				$final_datas[$rvalue['emp_code']]['basic_data']['pl_open'] = $leave_datas['pl_acc'];
				$final_datas[$rvalue['emp_code']]['basic_data']['sl_open'] = $leave_datas['sl_acc'];
				$final_datas[$rvalue['emp_code']]['basic_data']['bl_open'] = $leave_datas['bl_acc'];
				$final_datas[$rvalue['emp_code']]['basic_data']['ml_open'] = $leave_datas['ml_acc'];
				$final_datas[$rvalue['emp_code']]['basic_data']['mal_open'] = $leave_datas['mal_acc'];
				$final_datas[$rvalue['emp_code']]['basic_data']['pal_open'] = $leave_datas['pal_acc'];
				$final_datas[$rvalue['emp_code']]['basic_data']['lwp_open'] = $leave_datas['lwp_acc'];
				//$a = explode('-', $filter_date_start);
				$a = $filter_year;
				if($a == '2015'){
					$final_datas[$rvalue['emp_code']]['basic_data']['open_date'] = '01/11/2015';
				} else {
					$final_datas[$rvalue['emp_code']]['basic_data']['open_date'] = '01/01/'.date('Y');
				}
				$total_days_pl = 0;
				$total_days_bl = 0;
				$total_days_sl = 0;
				$total_days_ml = 0;
				$total_days_mal = 0;
				$total_days_pal = 0;
				$total_days_lwp = 0;

				$un_total_days_pl = 0;
				$un_total_days_bl = 0;
				$un_total_days_sl = 0;
				$un_total_days_ml = 0;
				$un_total_days_mal = 0;
				$un_total_days_pal = 0;
				$un_total_days_lwp = 0;
				
				if($transaction_datas){
					foreach($transaction_datas as $tkey => $tvalue){
						$days_pl = 0;
						$days_bl = 0;
						$days_sl = 0;
						$days_ml = 0;
						$days_mal = 0;
						$days_pal = 0;
						$days_lwp = 0;
						$t_datas = $this->model_report_common_report->getleave_transaction_data_ess($tvalue['batch_id']);	
						usort($t_datas, array($this, "date_compare"));
						end($t_datas);
						$e_key = key($t_datas);
						$start_date = $t_datas[0]['date'];
						$end_date = $t_datas[$e_key]['date'];
						// echo '<pre>';
						// print_r($t_datas);
						// exit;
						$days = 0;
						foreach ($t_datas as $ttkey => $ttvalue) {
							if($ttvalue['p_status'] == 1){
								$days ++;
							}
						}

						if($t_datas[0]['type'] != ''){
							$leave_types = $t_datas[0]['type'];
						} else {
							$leave_types = 'F';
						}				
						$en_days_pl = 0;
						if($t_datas[0]['leave_type'] == 'PL'){
							if($t_datas[0]['type'] != ''){
								if($t_datas[0]['type'] == 'F'){
									$days_pl = 1;
								} else {
									$days_pl = 0.5;
								}
							} else {
								$days_pl = $days;
								//if($tkey == 0){
									if($t_datas[0]['encash'] != ''){
										$en_days_pl = $t_datas[0]['encash'];
									}
								//}
							}
						} elseif($t_datas[0]['leave_type'] == 'BL'){
							if($t_datas[0]['type'] != ''){
								if($t_datas[0]['type'] == 'F'){
									$days_bl = 1;
								} else {
									$days_bl = 0.5;
								}
							} else {
								$days_bl = $days;
							}
						} elseif($t_datas[0]['leave_type'] == 'SL'){
							if($t_datas[0]['type'] != ''){
								if($t_datas[0]['type'] == 'F'){
									$days_sl = 1;
								} else {
									$days_sl = 0.5;
								}
							} else {
								$days_sl = $days;
							}
						} elseif($t_datas[0]['leave_type'] == 'ML'){
							if($t_datas[0]['type'] != ''){
								if($t_datas[0]['type'] == 'F'){
									$days_ml = 1;
								} else {
									$days_ml = 0.5;
								}
							} else {
								$days_ml = $days;
							}
						} elseif($t_datas[0]['leave_type'] == 'MAL'){
							if($t_datas[0]['type'] != ''){
								if($t_datas[0]['type'] == 'F'){
									$days_mal = 1;
								} else {
									$days_mal = 0.5;
								}
							} else {
								$days_mal = $days;
							}
						} elseif($t_datas[0]['leave_type'] == 'PAL'){
							if($t_datas[0]['type'] != ''){
								if($t_datas[0]['type'] == 'F'){
									$days_pal = 1;
								} else {
									$days_pal = 0.5;
								}
							} else {
								$days_pal = $days;
							}
						} elseif($t_datas[0]['leave_type'] == 'LWP'){
							if($t_datas[0]['type'] != ''){
								if($t_datas[0]['type'] == 'F'){
									$days_lwp = 1;
								} else {
									$days_lwp = 0.5;
								}
							} else {
								$days_lwp = $days;
							}
						}
						$encash_status = 0;
						$encash = 0;
						if($t_datas[0]['encash'] != ''){
							$encash_status = 1;
							$encash = $t_datas[0]['encash'];
						}
						// $encash_days_sql = "SELECT * FROM `oc_leave_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `p_status` = '0' AND `leave_type` = 'PL' ";
						// $encash_datas = $this->db->query($encash_days_sql);
						// if($encash_datas->num_rows > 0){
						// 	$encash_data = $encash_datas->row;
						// 	if($encash_data['encash'] != ''){
						// 		$encash_status = 1;
						// 		$en_days_pl = $en_days_pl + $encash_data['encash'];
						// 		$encash = $encash + $encash_data['encash'];
						// 	}
						// }
						$total_days_pl = $total_days_pl + $days_pl + $en_days_pl;
						$total_days_bl = $total_days_bl + $days_bl;
						$total_days_sl = $total_days_sl + $days_sl;
						$total_days_ml = $total_days_ml + $days_ml;
						$total_days_mal = $total_days_mal + $days_mal;
						$total_days_pal = $total_days_pal + $days_pal;
						$total_days_lwp = $total_days_lwp + $days_lwp;
						$performance_data['action'][$tvalue['date']] = array(
							'start_date' => date('d/m/Y', strtotime($start_date)),
							'end_date' => date('d/m/Y', strtotime($end_date)),
							'days_pl' => $days_pl,
							'days_bl' => $days_bl,
							'days_sl' => $days_sl,
							'days_ml' => $days_ml,
							'days_mal' => $days_mal,
							'days_pal' => $days_pal,
							'days_lwp' => $days_lwp,
							'leave_types' => $leave_types,
							'encash_status' => $encash_status,
							'encash' => $encash,
							'performance_stat' => '1'
						);
					}
					$in = 0;
					$sql = "SELECT * FROM `oc_leave_transaction_temp` WHERE 1=1";
					$sql .= " AND emp_id = '".$emp_code."' AND `p_status` = '0' AND `a_status` = '1' AND `encash` <> '' ";
					$sql .= " AND `dot` >= '".$data['filter_date_start']."' AND `dot` <= '".$data['filter_date_end']."' GROUP BY `batch_id` ";
					$query = $this->db->query($sql);
					$encash = 0;
					foreach ($query->rows as $key => $value) {
						$sql1 = "SELECT * FROM `oc_leave_transaction_temp` WHERE 1=1";
						$sql1 .= " AND emp_id = '".$emp_code."' AND `batch_id` = '".$value['batch_id']."' ";	
						$query1 = $this->db->query($sql1);
						$p_status = 0;
						foreach($query1->rows as $qkey => $qvalue){
							if($qvalue['p_status'] == 1){
								$p_status = 1;
								break;
							}
						}
						if($p_status == 0){
							$in = 1;
							//$en_days_pl = $en_days_pl + $value['encash'];
							$encash = $encash + $value['encash'];
						}
					}

					if($in == 1){
						$encash_data = $query->row;
						$encash_status = 1;
						$total_days_pl = $total_days_pl + $encash_data['encash'];
						$start_date = $encash_data['date'];
						$end_date = $encash_data['date'];
						$performance_data['action'][$encash_data['date']] = array(
							'start_date' => date('d/m/Y', strtotime($start_date)),
							'end_date' => date('d/m/Y', strtotime($end_date)),
							'days_pl' => 0,
							'days_bl' => 0,
							'days_sl' => 0,
							'days_ml' => 0,
							'days_mal' => 0,
							'days_pal' => 0,
							'days_lwp' => 0,
							'leave_types' => 'F',
							'encash_status' => $encash_status,
							'encash' => $encash_data['encash'],
							'performance_stat' => '0'
						);
					}
				} else {
					$total_days_pl = 0;
					$total_days_bl = 0;
					$total_days_sl = 0;
					$total_days_ml = 0;
					$total_days_mal = 0;
					$total_days_pal = 0;
					$total_days_lwp = 0;
					$in = 0;
					$sql = "SELECT * FROM `oc_leave_transaction_temp` WHERE 1=1";
					$sql .= " AND emp_id = '".$emp_code."' AND `p_status` = '0' AND `a_status` = '1' AND `encash` <> '' ";
					$sql .= " AND `dot` >= '".$data['filter_date_start']."' AND `dot` <= '".$data['filter_date_end']."' GROUP BY `batch_id` ";
					$query = $this->db->query($sql);
					$encash = 0;
					foreach ($query->rows as $key => $value) {
						$sql1 = "SELECT * FROM `oc_leave_transaction_temp` WHERE 1=1";
						$sql1 .= " AND emp_id = '".$emp_code."' AND `batch_id` = '".$value['batch_id']."' ";	
						$query1 = $this->db->query($sql1);
						$p_status = 0;
						foreach($query1->rows as $qkey => $qvalue){
							if($qvalue['p_status'] == 1){
								$p_status = 1;
								break;
							}
						}
						if($p_status == 0){
							$in = 1;
							//$en_days_pl = $en_days_pl + $value['encash'];
							$encash = $encash + $value['encash'];
						}
					}

					if($in == 1){
						$encash_data = $query->row;
						$encash_status = 1;
						$total_days_pl = $encash_data['encash'];
						$start_date = $encash_data['date'];
						$end_date = $encash_data['date'];
						$performance_data['action'][$encash_data['date']] = array(
							'start_date' => date('d/m/Y', strtotime($start_date)),
							'end_date' => date('d/m/Y', strtotime($end_date)),
							'days_pl' => 0,
							'days_bl' => 0,
							'days_sl' => 0,
							'days_ml' => 0,
							'days_mal' => 0,
							'days_pal' => 0,
							'days_lwp' => 0,
							'leave_types' => 'F',
							'encash_status' => $encash_status,
							'encash' => $encash_data['encash'],
							'performance_stat' => '0'
						);
					}
					// $encash_days_sql = "SELECT * FROM `oc_leave_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `p_status` = '0' AND `leave_type` = 'PL' ";
					// $encash_datas = $this->db->query($encash_days_sql);
					// if($encash_datas->num_rows > 0){
					// 	$encash_data = $encash_datas->row;
					// 	if($encash_data['encash'] != ''){
					// 		$encash_status = 1;
					// 		$total_days_pl = $encash_data['encash'];
					// 		$start_date = $encash_data['date'];
					// 		$end_date = $encash_data['date'];
					// 		$performance_data['action'][$encash_data['date']] = array(
					// 			'start_date' => date('d/m/Y', strtotime($start_date)),
					// 			'end_date' => date('d/m/Y', strtotime($end_date)),
					// 			'days_pl' => 0,
					// 			'days_cl' => 0,
					// 			'days_sl' => 0,
					// 			'leave_types' => 'F',
					// 			'encash_status' => $encash_status,
					// 			'encash' => $encash_data['encash'],
					// 			'performance_stat' => '0'
					// 		);		
					// 	}
					// }						
				}

				$sql = "SELECT * FROM `oc_leave_transaction_temp` WHERE 1=1";
				$a = date('Y', strtotime($data['filter_date_start']));					
				$filter_end = $a.'-12-31';
				if (!empty($data['filter_date_start'])) {
					$sql .= " AND DATE(`date`) >= '" . $this->db->escape($data['filter_date_start']) . "'";
				}
				if (!empty($data['filter_date_end'])) {
					$sql .= " AND DATE(`date`) <= '" . $this->db->escape($filter_end) . "'";
				}
				if (!empty($data['unit'])) {
					$sql .= " AND LOWER(`unit`) = '" . $this->db->escape(strtolower($data['unit'])) . "'";
				}
				if (isset($data['filter_leave']) && !empty($data['filter_leave'])) {
					$sql .= " AND LOWER(`leave_type`) = '" . $this->db->escape(strtolower($data['filter_leave'])) . "'";
				}
				//$sql .= " AND emp_id = '".$emp_code."' AND `p_status` = '0' AND `a_status` = '1' AND (`leave_type` = 'PL' OR `leave_type` = 'CL' OR `leave_type` = 'SL') GROUP BY `batch_id` ";
				$sql .= " AND emp_id = '".$emp_code."' AND `p_status` = '0' AND `a_status` = '1' GROUP BY `batch_id` ";
				//echo $sql;exit;
				$un_transaction_datas = $this->db->query($sql)->rows;
				
				foreach($un_transaction_datas as $tkey => $tvalue){
					$un_days_pl = 0;
					$un_days_bl = 0;
					$un_days_sl = 0;
					$un_days_ml = 0;
					$un_days_mal = 0;
					$un_days_pal = 0;
					$un_days_lwp = 0;
					$un_t_datas = $this->model_report_common_report->getleave_transaction_data_ess($tvalue['batch_id']);	
					usort($un_t_datas, array($this, "date_compare"));
					end($un_t_datas);
					$un_e_key = key($un_t_datas);
					$un_start_date = $un_t_datas[0]['date'];
					$un_end_date = $un_t_datas[$un_e_key]['date'];
					// echo '<pre>';
					// print_r($t_datas);
					// exit;
					$un_days = 0;
					foreach ($un_t_datas as $ttkey => $ttvalue) {
						if($ttvalue['p_status'] == 0){
							$un_days ++;
						}
					}

					if($un_t_datas[0]['type'] != ''){
						$un_leave_types = $un_t_datas[0]['type'];
					} else {
						$un_leave_types = 'F';
					}				
					$un_en_days_pl = 0;
					if($un_t_datas[0]['leave_type'] == 'PL'){
						if($un_t_datas[0]['type'] != ''){
							if($un_t_datas[0]['type'] == 'F'){
								$un_days_pl = 1;
							} else {
								$un_days_pl = 0.5;
							}
						} else {
							$un_days_pl = $un_days;
						}
					} elseif($un_t_datas[0]['leave_type'] == 'BL'){
						if($un_t_datas[0]['type'] != ''){
							if($un_t_datas[0]['type'] == 'F'){
								$un_days_bl = 1;
							} else {
								$un_days_bl = 0.5;
							}
						} else {
							$un_days_bl = $un_days;
						}
					} elseif($un_t_datas[0]['leave_type'] == 'SL'){
						if($un_t_datas[0]['type'] != ''){
							if($un_t_datas[0]['type'] == 'F'){
								$un_days_sl = 1;
							} else {
								$un_days_sl = 0.5;
							}
						} else {
							$un_days_sl = $un_days;
						}
					} elseif($un_t_datas[0]['leave_type'] == 'ML'){
						if($un_t_datas[0]['type'] != ''){
							if($un_t_datas[0]['type'] == 'F'){
								$un_days_ml = 1;
							} else {
								$un_days_ml = 0.5;
							}
						} else {
							$un_days_ml = $un_days;
						}
					} elseif($un_t_datas[0]['leave_type'] == 'MAL'){
						if($un_t_datas[0]['type'] != ''){
							if($un_t_datas[0]['type'] == 'F'){
								$un_days_mal = 1;
							} else {
								$un_days_mal = 0.5;
							}
						} else {
							$un_days_mal = $un_days;
						}
					} elseif($un_t_datas[0]['leave_type'] == 'PAL'){
						if($un_t_datas[0]['type'] != ''){
							if($un_t_datas[0]['type'] == 'F'){
								$un_days_pal = 1;
							} else {
								$un_days_pal = 0.5;
							}
						} else {
							$un_days_pal = $un_days;
						}
					} elseif($un_t_datas[0]['leave_type'] == 'LWP'){
						if($un_t_datas[0]['type'] != ''){
							if($un_t_datas[0]['type'] == 'F'){
								$un_days_lwp = 1;
							} else {
								$un_days_lwp = 0.5;
							}
						} else {
							$un_days_lwp = $un_days;
						}
					}
					
					
					$un_total_days_pl = $un_total_days_pl + $un_days_pl + $un_en_days_pl;
					$un_total_days_bl = $un_total_days_bl + $un_days_bl;
					$un_total_days_sl = $un_total_days_sl + $un_days_sl;
					$un_total_days_ml = $un_total_days_ml + $un_days_ml;
					$un_total_days_mal = $un_total_days_mal + $un_days_mal;
					$un_total_days_pal = $un_total_days_pal + $un_days_pal;
					$un_total_days_lwp = $un_total_days_lwp + $un_days_lwp;
				}
				
				$final_datas[$rvalue['emp_code']]['basic_data']['total_days_pl'] = $total_days_pl;
				$final_datas[$rvalue['emp_code']]['basic_data']['total_days_bl'] = $total_days_bl;
				$final_datas[$rvalue['emp_code']]['basic_data']['total_days_sl'] = $total_days_sl;
				$final_datas[$rvalue['emp_code']]['basic_data']['total_days_ml'] = $total_days_ml;
				$final_datas[$rvalue['emp_code']]['basic_data']['total_days_mal'] = $total_days_mal;
				$final_datas[$rvalue['emp_code']]['basic_data']['total_days_pal'] = $total_days_pal;
				$final_datas[$rvalue['emp_code']]['basic_data']['total_days_lwp'] = $total_days_lwp;

				$final_datas[$rvalue['emp_code']]['basic_data']['un_total_days_pl'] = $un_total_days_pl;
				$final_datas[$rvalue['emp_code']]['basic_data']['un_total_days_bl'] = $un_total_days_bl;
				$final_datas[$rvalue['emp_code']]['basic_data']['un_total_days_sl'] = $un_total_days_sl;
				$final_datas[$rvalue['emp_code']]['basic_data']['un_total_days_ml'] = $un_total_days_ml;
				$final_datas[$rvalue['emp_code']]['basic_data']['un_total_days_mal'] = $un_total_days_mal;
				$final_datas[$rvalue['emp_code']]['basic_data']['un_total_days_pal'] = $un_total_days_pal;
				$final_datas[$rvalue['emp_code']]['basic_data']['un_total_days_lwp'] = $un_total_days_lwp;
				
				$closing_pl = $leave_datas['pl_acc'] - $total_days_pl;
				$closing_bl = $leave_datas['bl_acc'] - $total_days_bl;
				$closing_sl = $leave_datas['sl_acc'] - $total_days_sl;
				$closing_ml = $leave_datas['ml_acc'] - $total_days_ml;
				$closing_mal = $leave_datas['mal_acc'] - $total_days_mal;
				$closing_pal = $leave_datas['pal_acc'] - $total_days_pal;
				$closing_lwp = $leave_datas['lwp_acc'] - $total_days_lwp;
				
				$final_datas[$rvalue['emp_code']]['basic_data']['un_total_bal_pl'] = $closing_pl - $un_total_days_pl;
				$final_datas[$rvalue['emp_code']]['basic_data']['un_total_bal_bl'] = $closing_bl - $un_total_days_bl;
				$final_datas[$rvalue['emp_code']]['basic_data']['un_total_bal_sl'] = $closing_sl - $un_total_days_sl;
				$final_datas[$rvalue['emp_code']]['basic_data']['un_total_bal_ml'] = $closing_ml - $un_total_days_ml;
				$final_datas[$rvalue['emp_code']]['basic_data']['un_total_bal_mal'] = $closing_mal - $un_total_days_mal;
				$final_datas[$rvalue['emp_code']]['basic_data']['un_total_bal_pal'] = $closing_pal - $un_total_days_pal;
				$final_datas[$rvalue['emp_code']]['basic_data']['un_total_bal_lwp'] = $closing_lwp - $un_total_days_lwp;

				$final_datas[$rvalue['emp_code']]['basic_data']['closing_pl'] = $closing_pl;
				$final_datas[$rvalue['emp_code']]['basic_data']['closing_bl'] = $closing_bl;
				$final_datas[$rvalue['emp_code']]['basic_data']['closing_sl'] = $closing_sl;
				$final_datas[$rvalue['emp_code']]['basic_data']['closing_ml'] = $closing_ml;
				$final_datas[$rvalue['emp_code']]['basic_data']['closing_mal'] = $closing_mal;
				$final_datas[$rvalue['emp_code']]['basic_data']['closing_pal'] = $closing_pal;
				$final_datas[$rvalue['emp_code']]['basic_data']['closing_lwp'] = $closing_lwp;
				
				$final_datas[$rvalue['emp_code']]['tran_data'] = $performance_data;
			}
			//exit;
		}

		//$final_datas = array();
		//$final_datass = array_chunk($final_datas, 3);

		// echo '<pre>';
		// print_r($final_datass);
		// exit;
		
		if($final_datas){
			//$month = date("F", mktime(0, 0, 0, $filter_month, 10));
			$filter_year1 = $filter_year.'-01-01';
			$template = new Template();		
			$template->data['final_datas'] = $final_datas;
			$template->data['filter_date_start'] = $filter_year1;
			$template->data['filter_date_end'] = $filter_date_end;
			$template->data['filter_year'] = $filter_year;
			$template->data['filter_leave'] = $filter_leave;
			//$template->data['report_type'] = $report_type;
			$template->data['title'] = 'Leave Register';
			if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
				$template->data['base'] = HTTPS_SERVER;
			} else {
				$template->data['base'] = HTTP_SERVER;
			}
			$html = $template->fetch('report/leave_register_html.tpl');
			//echo $html;exit;
			$filename = "Leave_Register";
			
			header("Content-Type: application/vnd.ms-excel; charset=utf-8");
			header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false);
			echo $html;
			exit;		
		} else {
			$this->session->data['warning'] = 'No Data Found';
			$this->redirect($this->url->link('report/leave_register', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}
	}

	public function export_summary(){
		$this->language->load('report/leave_register');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			$from = date('Y-m-d');
			$filter_date_start = date('Y-m-d', strtotime($from . "-30 day"));
		}

		if (isset($this->request->get['filter_year'])) {
			$filter_year = $this->request->get['filter_year'];
		} else {
			$filter_year = 2017;
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = date('Y-m-d');
		}
		
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['unit'])) {
			$unit = $this->request->get['unit'];
		} else {
			$unit = 0;
		}

		if (isset($this->request->get['department'])) {
			$department = html_entity_decode($this->request->get['department']);
		} else {
			$department = 0;
		}

		if(isset($this->session->data['dept_names'])){
			$filter_departments = html_entity_decode($this->session->data['dept_names']);
			$filter_departments = "'" . str_replace(",", "','", html_entity_decode($filter_departments)) . "'";
		} else {
			$filter_departments = '';
		}

		if (isset($this->request->get['group'])) {
			$group = $this->request->get['group'];
		} else {
			$group = 0;
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_year'])) {
			$url .= '&filter_year=' . $this->request->get['filter_year'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_type'])) {
			$url .= '&filter_type=' . $this->request->get['filter_type'];
		}

		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}
		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$url .= '&once=1';

		$this->load->model('report/common_report');
		$this->load->model('report/attendance');
		$this->load->model('catalog/employee');

		$this->data['attendace'] = array();

		$data = array(
			'filter_date_start'	     => $filter_year.'-01-01',
			'filter_year'	         => $filter_year,
			'filter_date_end'	     => $filter_date_end,
			'filter_name'	     	 => $filter_name,
			'filter_name_id'	     => $filter_name_id,
			'unit'					 => $unit,
			'department'			 => $department,
			'filter_departments'     => $filter_departments,
			'group'					 => $group, 
			'start'                  => ($page - 1) * 7000,
			'limit'                  => 7000
		);

		
        $final_datas = array();
		// echo '<pre>';
		// print_r($data);
		// exit;
		//$from_year = date('Y', strtotime($filter_date_start));
		$from_year = $filter_year;
		$results = $this->model_report_common_report->getemployees($data);
		foreach ($results as $rkey => $rvalue) {
			$performance_data = array();
			$transaction_datas = $this->model_report_common_report->getleave_transaction_data_group_ess($rvalue['emp_code'], $data);
			
			$leave_datas = $this->model_report_common_report->getleave_data($rvalue['emp_code'], $from_year);
			if(isset($leave_datas['pl_acc'])){
				$emp_code = $rvalue['emp_code'];
				$final_datas[$rvalue['emp_code']]['basic_data']['name'] = $rvalue['name'];
				$final_datas[$rvalue['emp_code']]['basic_data']['department'] = $rvalue['department'];
				$final_datas[$rvalue['emp_code']]['basic_data']['emp_code'] = $rvalue['emp_code'];
				$final_datas[$rvalue['emp_code']]['basic_data']['pl_open'] = $leave_datas['pl_acc'];
				$final_datas[$rvalue['emp_code']]['basic_data']['sl_open'] = $leave_datas['sl_acc'];
				$final_datas[$rvalue['emp_code']]['basic_data']['bl_open'] = $leave_datas['bl_acc'];
				$final_datas[$rvalue['emp_code']]['basic_data']['ml_open'] = $leave_datas['ml_acc'];
				$final_datas[$rvalue['emp_code']]['basic_data']['mal_open'] = $leave_datas['mal_acc'];
				$final_datas[$rvalue['emp_code']]['basic_data']['pal_open'] = $leave_datas['pal_acc'];
				$final_datas[$rvalue['emp_code']]['basic_data']['lwp_open'] = $leave_datas['lwp_acc'];
				//$a = explode('-', $filter_date_start);
				$a = $filter_year;
				if($a == '2015'){
					$final_datas[$rvalue['emp_code']]['basic_data']['open_date'] = '01/11/2015';
				} else {
					$final_datas[$rvalue['emp_code']]['basic_data']['open_date'] = '01/01/'.date('Y');
				}
				$total_days_pl = 0;
				$total_days_bl = 0;
				$total_days_sl = 0;
				$total_days_ml = 0;
				$total_days_mal = 0;
				$total_days_pal = 0;
				$total_days_lwp = 0;

				$un_total_days_pl = 0;
				$un_total_days_bl = 0;
				$un_total_days_sl = 0;
				$un_total_days_ml = 0;
				$un_total_days_mal = 0;
				$un_total_days_pal = 0;
				$un_total_days_lwp = 0;
				
				if($transaction_datas){
					foreach($transaction_datas as $tkey => $tvalue){
						$days_pl = 0;
						$days_bl = 0;
						$days_sl = 0;
						$days_ml = 0;
						$days_mal = 0;
						$days_pal = 0;
						$days_lwp = 0;
						$t_datas = $this->model_report_common_report->getleave_transaction_data_ess($tvalue['batch_id']);	
						usort($t_datas, array($this, "date_compare"));
						end($t_datas);
						$e_key = key($t_datas);
						$start_date = $t_datas[0]['date'];
						$end_date = $t_datas[$e_key]['date'];
						// echo '<pre>';
						// print_r($t_datas);
						// exit;
						$days = 0;
						foreach ($t_datas as $ttkey => $ttvalue) {
							if($ttvalue['p_status'] == 1){
								$days ++;
							}
						}

						if($t_datas[0]['type'] != ''){
							$leave_types = $t_datas[0]['type'];
						} else {
							$leave_types = 'F';
						}				
						$en_days_pl = 0;
						if($t_datas[0]['leave_type'] == 'PL'){
							if($t_datas[0]['type'] != ''){
								if($t_datas[0]['type'] == 'F'){
									$days_pl = 1;
								} else {
									$days_pl = 0.5;
								}
							} else {
								$days_pl = $days;
								//if($tkey == 0){
									if($t_datas[0]['encash'] != ''){
										$en_days_pl = $t_datas[0]['encash'];
									}
								//}
							}
						} elseif($t_datas[0]['leave_type'] == 'BL'){
							if($t_datas[0]['type'] != ''){
								if($t_datas[0]['type'] == 'F'){
									$days_bl = 1;
								} else {
									$days_bl = 0.5;
								}
							} else {
								$days_bl = $days;
							}
						} elseif($t_datas[0]['leave_type'] == 'SL'){
							if($t_datas[0]['type'] != ''){
								if($t_datas[0]['type'] == 'F'){
									$days_sl = 1;
								} else {
									$days_sl = 0.5;
								}
							} else {
								$days_sl = $days;
							}
						} elseif($t_datas[0]['leave_type'] == 'ML'){
							if($t_datas[0]['type'] != ''){
								if($t_datas[0]['type'] == 'F'){
									$days_ml = 1;
								} else {
									$days_ml = 0.5;
								}
							} else {
								$days_ml = $days;
							}
						} elseif($t_datas[0]['leave_type'] == 'MAL'){
							if($t_datas[0]['type'] != ''){
								if($t_datas[0]['type'] == 'F'){
									$days_mal = 1;
								} else {
									$days_mal = 0.5;
								}
							} else {
								$days_mal = $days;
							}
						} elseif($t_datas[0]['leave_type'] == 'PAL'){
							if($t_datas[0]['type'] != ''){
								if($t_datas[0]['type'] == 'F'){
									$days_pal = 1;
								} else {
									$days_pal = 0.5;
								}
							} else {
								$days_pal = $days;
							}
						} elseif($t_datas[0]['leave_type'] == 'LWP'){
							if($t_datas[0]['type'] != ''){
								if($t_datas[0]['type'] == 'F'){
									$days_lwp = 1;
								} else {
									$days_lwp = 0.5;
								}
							} else {
								$days_lwp = $days;
							}
						}
						$encash_status = 0;
						$encash = 0;
						if($t_datas[0]['encash'] != ''){
							$encash_status = 1;
							$encash = $t_datas[0]['encash'];
						}
						// $encash_days_sql = "SELECT * FROM `oc_leave_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `p_status` = '0' AND `leave_type` = 'PL' ";
						// $encash_datas = $this->db->query($encash_days_sql);
						// if($encash_datas->num_rows > 0){
						// 	$encash_data = $encash_datas->row;
						// 	if($encash_data['encash'] != ''){
						// 		$encash_status = 1;
						// 		$en_days_pl = $en_days_pl + $encash_data['encash'];
						// 		$encash = $encash + $encash_data['encash'];
						// 	}
						// }
						$total_days_pl = $total_days_pl + $days_pl + $en_days_pl;
						$total_days_bl = $total_days_bl + $days_bl;
						$total_days_sl = $total_days_sl + $days_sl;
						$total_days_ml = $total_days_ml + $days_ml;
						$total_days_mal = $total_days_mal + $days_mal;
						$total_days_pal = $total_days_pal + $days_pal;
						$total_days_lwp = $total_days_lwp + $days_lwp;
						$performance_data['action'][$tvalue['date']] = array(
							'start_date' => date('d/m/Y', strtotime($start_date)),
							'end_date' => date('d/m/Y', strtotime($end_date)),
							'days_pl' => $days_pl,
							'days_bl' => $days_bl,
							'days_sl' => $days_sl,
							'days_ml' => $days_ml,
							'days_mal' => $days_mal,
							'days_pal' => $days_pal,
							'days_lwp' => $days_lwp,
							'leave_types' => $leave_types,
							'encash_status' => $encash_status,
							'encash' => $encash,
							'performance_stat' => '1'
						);
					}
					$in = 0;
					$sql = "SELECT * FROM `oc_leave_transaction_temp` WHERE 1=1";
					$sql .= " AND emp_id = '".$emp_code."' AND `p_status` = '0' AND `a_status` = '1' AND `encash` <> '' ";
					$sql .= " AND `dot` >= '".$data['filter_date_start']."' AND `dot` <= '".$data['filter_date_end']."' GROUP BY `batch_id` ";
					$query = $this->db->query($sql);
					$encash = 0;
					foreach ($query->rows as $key => $value) {
						$sql1 = "SELECT * FROM `oc_leave_transaction_temp` WHERE 1=1";
						$sql1 .= " AND emp_id = '".$emp_code."' AND `batch_id` = '".$value['batch_id']."' ";	
						$query1 = $this->db->query($sql1);
						$p_status = 0;
						foreach($query1->rows as $qkey => $qvalue){
							if($qvalue['p_status'] == 1){
								$p_status = 1;
								break;
							}
						}
						if($p_status == 0){
							$in = 1;
							//$en_days_pl = $en_days_pl + $value['encash'];
							$encash = $encash + $value['encash'];
						}
					}

					if($in == 1){
						$encash_data = $query->row;
						$encash_status = 1;
						$total_days_pl = $total_days_pl + $encash_data['encash'];
						$start_date = $encash_data['date'];
						$end_date = $encash_data['date'];
						$performance_data['action'][$encash_data['date']] = array(
							'start_date' => date('d/m/Y', strtotime($start_date)),
							'end_date' => date('d/m/Y', strtotime($end_date)),
							'days_pl' => 0,
							'days_bl' => 0,
							'days_sl' => 0,
							'days_ml' => 0,
							'days_mal' => 0,
							'days_pal' => 0,
							'days_lwp' => 0,
							'leave_types' => 'F',
							'encash_status' => $encash_status,
							'encash' => $encash_data['encash'],
							'performance_stat' => '0'
						);
					}
				} else {
					$total_days_pl = 0;
					$total_days_bl = 0;
					$total_days_sl = 0;
					$total_days_ml = 0;
					$total_days_mal = 0;
					$total_days_pal = 0;
					$total_days_lwp = 0;
					$in = 0;
					$sql = "SELECT * FROM `oc_leave_transaction_temp` WHERE 1=1";
					$sql .= " AND emp_id = '".$emp_code."' AND `p_status` = '0' AND `a_status` = '1' AND `encash` <> '' ";
					$sql .= " AND `dot` >= '".$data['filter_date_start']."' AND `dot` <= '".$data['filter_date_end']."' GROUP BY `batch_id` ";
					$query = $this->db->query($sql);
					$encash = 0;
					foreach ($query->rows as $key => $value) {
						$sql1 = "SELECT * FROM `oc_leave_transaction_temp` WHERE 1=1";
						$sql1 .= " AND emp_id = '".$emp_code."' AND `batch_id` = '".$value['batch_id']."' ";	
						$query1 = $this->db->query($sql1);
						$p_status = 0;
						foreach($query1->rows as $qkey => $qvalue){
							if($qvalue['p_status'] == 1){
								$p_status = 1;
								break;
							}
						}
						if($p_status == 0){
							$in = 1;
							//$en_days_pl = $en_days_pl + $value['encash'];
							$encash = $encash + $value['encash'];
						}
					}

					if($in == 1){
						$encash_data = $query->row;
						$encash_status = 1;
						$total_days_pl = $encash_data['encash'];
						$start_date = $encash_data['date'];
						$end_date = $encash_data['date'];
						$performance_data['action'][$encash_data['date']] = array(
							'start_date' => date('d/m/Y', strtotime($start_date)),
							'end_date' => date('d/m/Y', strtotime($end_date)),
							'days_pl' => 0,
							'days_bl' => 0,
							'days_sl' => 0,
							'days_ml' => 0,
							'days_mal' => 0,
							'days_pal' => 0,
							'days_lwp' => 0,
							'leave_types' => 'F',
							'encash_status' => $encash_status,
							'encash' => $encash_data['encash'],
							'performance_stat' => '0'
						);
					}
					// $encash_days_sql = "SELECT * FROM `oc_leave_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `p_status` = '0' AND `leave_type` = 'PL' ";
					// $encash_datas = $this->db->query($encash_days_sql);
					// if($encash_datas->num_rows > 0){
					// 	$encash_data = $encash_datas->row;
					// 	if($encash_data['encash'] != ''){
					// 		$encash_status = 1;
					// 		$total_days_pl = $encash_data['encash'];
					// 		$start_date = $encash_data['date'];
					// 		$end_date = $encash_data['date'];
					// 		$performance_data['action'][$encash_data['date']] = array(
					// 			'start_date' => date('d/m/Y', strtotime($start_date)),
					// 			'end_date' => date('d/m/Y', strtotime($end_date)),
					// 			'days_pl' => 0,
					// 			'days_cl' => 0,
					// 			'days_sl' => 0,
					// 			'leave_types' => 'F',
					// 			'encash_status' => $encash_status,
					// 			'encash' => $encash_data['encash'],
					// 			'performance_stat' => '0'
					// 		);		
					// 	}
					// }						
				}

				$sql = "SELECT * FROM `oc_leave_transaction_temp` WHERE 1=1";
				$a = date('Y', strtotime($data['filter_date_start']));					
				$filter_end = $a.'-12-31';
				if (!empty($data['filter_date_start'])) {
					$sql .= " AND DATE(`date`) >= '" . $this->db->escape($data['filter_date_start']) . "'";
				}
				if (!empty($data['filter_date_end'])) {
					$sql .= " AND DATE(`date`) <= '" . $this->db->escape($filter_end) . "'";
				}
				if (!empty($data['unit'])) {
					$sql .= " AND LOWER(`unit`) = '" . $this->db->escape(strtolower($data['unit'])) . "'";
				}
				if (isset($data['filter_leave']) && !empty($data['filter_leave'])) {
					$sql .= " AND LOWER(`leave_type`) = '" . $this->db->escape(strtolower($data['filter_leave'])) . "'";
				}
				//$sql .= " AND emp_id = '".$emp_code."' AND `p_status` = '0' AND `a_status` = '1' AND (`leave_type` = 'PL' OR `leave_type` = 'CL' OR `leave_type` = 'SL') GROUP BY `batch_id` ";
				$sql .= " AND emp_id = '".$emp_code."' AND `p_status` = '0' AND `a_status` = '1' GROUP BY `batch_id` ";
				//echo $sql;exit;
				$un_transaction_datas = $this->db->query($sql)->rows;
				
				foreach($un_transaction_datas as $tkey => $tvalue){
					$un_days_pl = 0;
					$un_days_bl = 0;
					$un_days_sl = 0;
					$un_days_ml = 0;
					$un_days_mal = 0;
					$un_days_pal = 0;
					$un_days_lwp = 0;
					$un_t_datas = $this->model_report_common_report->getleave_transaction_data_ess($tvalue['batch_id']);	
					usort($un_t_datas, array($this, "date_compare"));
					end($un_t_datas);
					$un_e_key = key($un_t_datas);
					$un_start_date = $un_t_datas[0]['date'];
					$un_end_date = $un_t_datas[$un_e_key]['date'];
					// echo '<pre>';
					// print_r($t_datas);
					// exit;
					$un_days = 0;
					foreach ($un_t_datas as $ttkey => $ttvalue) {
						if($ttvalue['p_status'] == 0){
							$un_days ++;
						}
					}

					if($un_t_datas[0]['type'] != ''){
						$un_leave_types = $un_t_datas[0]['type'];
					} else {
						$un_leave_types = 'F';
					}				
					$un_en_days_pl = 0;
					if($un_t_datas[0]['leave_type'] == 'PL'){
						if($un_t_datas[0]['type'] != ''){
							if($un_t_datas[0]['type'] == 'F'){
								$un_days_pl = 1;
							} else {
								$un_days_pl = 0.5;
							}
						} else {
							$un_days_pl = $un_days;
						}
					} elseif($un_t_datas[0]['leave_type'] == 'BL'){
						if($un_t_datas[0]['type'] != ''){
							if($un_t_datas[0]['type'] == 'F'){
								$un_days_bl = 1;
							} else {
								$un_days_bl = 0.5;
							}
						} else {
							$un_days_bl = $un_days;
						}
					} elseif($un_t_datas[0]['leave_type'] == 'SL'){
						if($un_t_datas[0]['type'] != ''){
							if($un_t_datas[0]['type'] == 'F'){
								$un_days_sl = 1;
							} else {
								$un_days_sl = 0.5;
							}
						} else {
							$un_days_sl = $un_days;
						}
					} elseif($un_t_datas[0]['leave_type'] == 'ML'){
						if($un_t_datas[0]['type'] != ''){
							if($un_t_datas[0]['type'] == 'F'){
								$un_days_ml = 1;
							} else {
								$un_days_ml = 0.5;
							}
						} else {
							$un_days_ml = $un_days;
						}
					} elseif($un_t_datas[0]['leave_type'] == 'MAL'){
						if($un_t_datas[0]['type'] != ''){
							if($un_t_datas[0]['type'] == 'F'){
								$un_days_mal = 1;
							} else {
								$un_days_mal = 0.5;
							}
						} else {
							$un_days_mal = $un_days;
						}
					} elseif($un_t_datas[0]['leave_type'] == 'PAL'){
						if($un_t_datas[0]['type'] != ''){
							if($un_t_datas[0]['type'] == 'F'){
								$un_days_pal = 1;
							} else {
								$un_days_pal = 0.5;
							}
						} else {
							$un_days_pal = $un_days;
						}
					} elseif($un_t_datas[0]['leave_type'] == 'LWP'){
						if($un_t_datas[0]['type'] != ''){
							if($un_t_datas[0]['type'] == 'F'){
								$un_days_lwp = 1;
							} else {
								$un_days_lwp = 0.5;
							}
						} else {
							$un_days_lwp = $un_days;
						}
					}
					
					
					$un_total_days_pl = $un_total_days_pl + $un_days_pl + $un_en_days_pl;
					$un_total_days_bl = $un_total_days_bl + $un_days_bl;
					$un_total_days_sl = $un_total_days_sl + $un_days_sl;
					$un_total_days_ml = $un_total_days_ml + $un_days_ml;
					$un_total_days_mal = $un_total_days_mal + $un_days_mal;
					$un_total_days_pal = $un_total_days_pal + $un_days_pal;
					$un_total_days_lwp = $un_total_days_lwp + $un_days_lwp;
				}
				
				$final_datas[$rvalue['emp_code']]['basic_data']['total_days_pl'] = $total_days_pl;
				$final_datas[$rvalue['emp_code']]['basic_data']['total_days_bl'] = $total_days_bl;
				$final_datas[$rvalue['emp_code']]['basic_data']['total_days_sl'] = $total_days_sl;
				$final_datas[$rvalue['emp_code']]['basic_data']['total_days_ml'] = $total_days_ml;
				$final_datas[$rvalue['emp_code']]['basic_data']['total_days_mal'] = $total_days_mal;
				$final_datas[$rvalue['emp_code']]['basic_data']['total_days_pal'] = $total_days_pal;
				$final_datas[$rvalue['emp_code']]['basic_data']['total_days_lwp'] = $total_days_lwp;

				$final_datas[$rvalue['emp_code']]['basic_data']['un_total_days_pl'] = $un_total_days_pl;
				$final_datas[$rvalue['emp_code']]['basic_data']['un_total_days_bl'] = $un_total_days_bl;
				$final_datas[$rvalue['emp_code']]['basic_data']['un_total_days_sl'] = $un_total_days_sl;
				$final_datas[$rvalue['emp_code']]['basic_data']['un_total_days_ml'] = $un_total_days_ml;
				$final_datas[$rvalue['emp_code']]['basic_data']['un_total_days_mal'] = $un_total_days_mal;
				$final_datas[$rvalue['emp_code']]['basic_data']['un_total_days_pal'] = $un_total_days_pal;
				$final_datas[$rvalue['emp_code']]['basic_data']['un_total_days_lwp'] = $un_total_days_lwp;
				
				$closing_pl = $leave_datas['pl_acc'] - $total_days_pl;
				$closing_bl = $leave_datas['bl_acc'] - $total_days_bl;
				$closing_sl = $leave_datas['sl_acc'] - $total_days_sl;
				$closing_ml = $leave_datas['ml_acc'] - $total_days_ml;
				$closing_mal = $leave_datas['mal_acc'] - $total_days_mal;
				$closing_pal = $leave_datas['pal_acc'] - $total_days_pal;
				$closing_lwp = $leave_datas['lwp_acc'] - $total_days_lwp;
				
				$final_datas[$rvalue['emp_code']]['basic_data']['un_total_bal_pl'] = $closing_pl - $un_total_days_pl;
				$final_datas[$rvalue['emp_code']]['basic_data']['un_total_bal_bl'] = $closing_bl - $un_total_days_bl;
				$final_datas[$rvalue['emp_code']]['basic_data']['un_total_bal_sl'] = $closing_sl - $un_total_days_sl;
				$final_datas[$rvalue['emp_code']]['basic_data']['un_total_bal_ml'] = $closing_ml - $un_total_days_ml;
				$final_datas[$rvalue['emp_code']]['basic_data']['un_total_bal_mal'] = $closing_mal - $un_total_days_mal;
				$final_datas[$rvalue['emp_code']]['basic_data']['un_total_bal_pal'] = $closing_pal - $un_total_days_pal;
				$final_datas[$rvalue['emp_code']]['basic_data']['un_total_bal_lwp'] = $closing_lwp - $un_total_days_lwp;

				$final_datas[$rvalue['emp_code']]['basic_data']['closing_pl'] = $closing_pl;
				$final_datas[$rvalue['emp_code']]['basic_data']['closing_bl'] = $closing_bl;
				$final_datas[$rvalue['emp_code']]['basic_data']['closing_sl'] = $closing_sl;
				$final_datas[$rvalue['emp_code']]['basic_data']['closing_ml'] = $closing_ml;
				$final_datas[$rvalue['emp_code']]['basic_data']['closing_mal'] = $closing_mal;
				$final_datas[$rvalue['emp_code']]['basic_data']['closing_pal'] = $closing_pal;
				$final_datas[$rvalue['emp_code']]['basic_data']['closing_lwp'] = $closing_lwp;
				
				$final_datas[$rvalue['emp_code']]['tran_data'] = $performance_data;
			}
			//exit;
		}

		//$final_datas = array();
		//$final_datass = array_chunk($final_datas, 5);

		// echo '<pre>';
		// print_r($final_datass);
		// exit;
		
		if($final_datas){
			$filter_year1 = $filter_year.'-01-01';
			//$month = date("F", mktime(0, 0, 0, $filter_month, 10));
			$template = new Template();		
			$template->data['final_datas'] = $final_datas;
			$template->data['filter_date_start'] = $filter_year1;
			$template->data['filter_date_end'] = $filter_date_end;
			$template->data['filter_year'] = $filter_year;
			//$template->data['report_type'] = $report_type;
			$template->data['title'] = 'Leave Register Summary';
			if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
				$template->data['base'] = HTTPS_SERVER;
			} else {
				$template->data['base'] = HTTP_SERVER;
			}
			$html = $template->fetch('report/leave_register_summ_html.tpl');
			//echo $html;exit;
			$filename = "Leave_Register_Summary";
			
			header("Content-Type: application/vnd.ms-excel; charset=utf-8");
			header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false);
			echo $html;
			exit;		
		} else {
			$this->session->data['warning'] = 'No Data Found';
			$this->redirect($this->url->link('report/leave_register', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}
	}

	public function export_custom(){
		$this->language->load('report/leave_register');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			$from = date('Y-m-d');
			$filter_date_start = date('Y-m-d', strtotime($from . "-30 day"));
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = date('Y-m-d');
		}

		if (isset($this->request->get['filter_year'])) {
			$filter_year = $this->request->get['filter_year'];
		} else {
			$filter_year = '';
		}
		
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['unit'])) {
			$unit = $this->request->get['unit'];
		} else {
			$unit = 0;
		}

		if (isset($this->request->get['department'])) {
			$department = html_entity_decode($this->request->get['department']);
		} else {
			$department = 0;
		}

		if(isset($this->session->data['dept_names'])){
			$filter_departments = html_entity_decode($this->session->data['dept_names']);
			$filter_departments = "'" . str_replace(",", "','", html_entity_decode($filter_departments)) . "'";
		} else {
			$filter_departments = '';
		}

		if (isset($this->request->get['group'])) {
			$group = $this->request->get['group'];
		} else {
			$group = 0;
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		if (isset($this->request->get['filter_leave'])) {
			$filter_leave = $this->request->get['filter_leave'];
			if($filter_leave == '0'){
				$filter_leave = 'PL';
			}
		} else {
			$filter_leave = 'PL';
		}		

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['filter_year'])) {
			$url .= '&filter_year=' . $this->request->get['filter_year'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_type'])) {
			$url .= '&filter_type=' . $this->request->get['filter_type'];
		}

		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		
		if (isset($this->request->get['department'])) {
			$url .= '&department=' . $this->request->get['department'];
		}
		
		if (isset($this->request->get['group'])) {
			$url .= '&group=' . $this->request->get['group'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['filter_leave'])) {
			$url .= '&filter_leave=' . $this->request->get['filter_leave'];
		}

		$url .= '&once=1';

		$this->load->model('report/common_report');
		$this->load->model('report/attendance');
		$this->load->model('catalog/employee');

		$this->data['attendace'] = array();

		$data = array(
			'filter_date_start'	     => $filter_year.'-01-01',
			'filter_date_end'	     => $filter_date_end,
			'filter_year'	     	 => $filter_year,
			'filter_name'	     	 => $filter_name,
			'filter_name_id'	     => $filter_name_id,
			'filter_leave'	     	 => $filter_leave,
			'unit'					 => $unit,
			'department'			 => $department,
			'filter_departments'     => $filter_departments,
			'group'					 => $group, 
			'start'                  => ($page - 1) * 7000,
			'limit'                  => 7000
		);

		
        $final_datas = array();
		// echo '<pre>';
		// print_r($data);
		// exit;
		//$from_year = date('Y', strtotime($filter_date_start));
		$from_year = $filter_year;
		$results = $this->model_report_common_report->getemployees($data);
		foreach ($results as $rkey => $rvalue) {
			$performance_data = array();
			$transaction_datas = $this->model_report_common_report->getleave_transaction_data_group_ess($rvalue['emp_code'], $data);
			// echo '<pre>';
			// print_r($transaction_datas);
			// exit;
			$leave_datas = $this->model_report_common_report->getleave_data($rvalue['emp_code'], $from_year);
			if(isset($leave_datas['pl_acc'])){
				$emp_code = $rvalue['emp_code'];
				$final_datas[$rvalue['emp_code']]['name'] = $rvalue['name'];
				$final_datas[$rvalue['emp_code']]['department'] = $rvalue['department'];
				$final_datas[$rvalue['emp_code']]['emp_code'] = $rvalue['emp_code'];
				if($filter_leave == 'PL'){
					$final_datas[$rvalue['emp_code']]['open'] = $leave_datas['pl_acc'];
				} elseif($filter_leave == 'SL'){
					$final_datas[$rvalue['emp_code']]['open'] = $leave_datas['sl_acc'];
				} elseif($filter_leave == 'BL'){
					$final_datas[$rvalue['emp_code']]['open'] = $leave_datas['bl_acc'];
				} elseif($filter_leave == 'ML'){
					$final_datas[$rvalue['emp_code']]['open'] = $leave_datas['ml_acc'];
				} elseif($filter_leave == 'MAL'){
					$final_datas[$rvalue['emp_code']]['open'] = $leave_datas['mal_acc'];
				} elseif($filter_leave == 'PAL'){
					$final_datas[$rvalue['emp_code']]['open'] = $leave_datas['pal_acc'];
				} elseif($filter_leave == 'LWP'){
					$final_datas[$rvalue['emp_code']]['open'] = $leave_datas['lwp_acc'];
				}
				$total_days_pl = 0;
				$total_days_bl = 0;
				$total_days_sl = 0;
				$total_days_ml = 0;
				$total_days_mal = 0;
				$total_days_pal = 0;
				$total_days_lwp = 0;
				if($transaction_datas){
					foreach($transaction_datas as $tkey => $tvalue){
						$days_pl = 0;
						$days_bl = 0;
						$days_sl = 0;
						$days_ml = 0;
						$days_mal = 0;
						$days_pal = 0;
						$days_lwp = 0;
						$t_datas = $this->model_report_common_report->getleave_transaction_data_ess($tvalue['batch_id']);	
						usort($t_datas, array($this, "date_compare"));
						end($t_datas);
						$e_key = key($t_datas);
						$start_date = $t_datas[0]['date'];
						$end_date = $t_datas[$e_key]['date'];
						// echo '<pre>';
						// print_r($t_datas);
						// exit;
						$days = 0;
						foreach ($t_datas as $ttkey => $ttvalue) {
							if($ttvalue['p_status'] == 1){
								$days ++;
							}
						}

						if($t_datas[0]['type'] != ''){
							$leave_types = $t_datas[0]['type'];
						} else {
							$leave_types = 'F';
						}				
						$en_days_pl = 0;
						if($t_datas[0]['leave_type'] == 'PL' && $filter_leave == 'PL'){
							if($t_datas[0]['type'] != ''){
								if($t_datas[0]['type'] == 'F'){
									$days_pl = 1;
								} else {
									$days_pl = 0.5;
								}
							} else {
								$days_pl = $days;
								if($t_datas[0]['encash'] != ''){
									$en_days_pl = $t_datas[0]['encash'];
								}
							}
						} elseif($t_datas[0]['leave_type'] == 'BL' && $filter_leave == 'BL'){
							if($t_datas[0]['type'] != ''){
								if($t_datas[0]['type'] == 'F'){
									$days_bl = 1;
								} else {
									$days_bl = 0.5;
								}
							} else {
								$days_bl = $days;
							}
						} elseif($t_datas[0]['leave_type'] == 'SL' && $filter_leave == 'SL'){
							if($t_datas[0]['type'] != ''){
								if($t_datas[0]['type'] == 'F'){
									$days_sl = 1;
								} else {
									$days_sl = 0.5;
								}
							} else {
								$days_sl = $days;
							}
						} elseif($t_datas[0]['leave_type'] == 'ML' && $filter_leave == 'ML'){
							if($t_datas[0]['type'] != ''){
								if($t_datas[0]['type'] == 'F'){
									$days_ml = 1;
								} else {
									$days_ml = 0.5;
								}
							} else {
								$days_ml = $days;
							}
						} elseif($t_datas[0]['leave_type'] == 'MAL' && $filter_leave == 'MAL'){
							if($t_datas[0]['type'] != ''){
								if($t_datas[0]['type'] == 'F'){
									$days_mal = 1;
								} else {
									$days_mal = 0.5;
								}
							} else {
								$days_mal = $days;
							}
						} elseif($t_datas[0]['leave_type'] == 'PAL' && $filter_leave == 'PAL'){
							if($t_datas[0]['type'] != ''){
								if($t_datas[0]['type'] == 'F'){
									$days_pal = 1;
								} else {
									$days_pal = 0.5;
								}
							} else {
								$days_pal = $days;
							}
						} elseif($t_datas[0]['leave_type'] == 'LWP' && $filter_leave == 'LWP'){
							if($t_datas[0]['type'] != ''){
								if($t_datas[0]['type'] == 'F'){
									$days_lwp = 1;
								} else {
									$days_lwp = 0.5;
								}
							} else {
								$days_lwp = $days;
							}
						}

						$encash_status = 0;
						$encash = 0;
						if($t_datas[0]['encash'] != ''){
							$encash_status = 1;
							$encash = $t_datas[0]['encash'];
						}
						
						$total_days_pl = $total_days_pl + $days_pl + $en_days_pl;
						$total_days_bl = $total_days_bl + $days_bl;
						$total_days_sl = $total_days_sl + $days_sl;
						$total_days_ml = $total_days_ml + $days_ml;
						$total_days_mal = $total_days_mal + $days_mal;
						$total_days_pal = $total_days_pal + $days_pal;
						$total_days_lwp = $total_days_lwp + $days_lwp;
						// $performance_data['action'][$tvalue['date']] = array(
						// 	'start_date' => date('d/m/Y', strtotime($start_date)),
						// 	'end_date' => date('d/m/Y', strtotime($end_date)),
						// 	'days_pl' => $days_pl,
						// 	'days_cl' => $days_cl,
						// 	'days_sl' => $days_sl,
						// 	'leave_types' => $leave_types,
						// 	'encash_status' => $encash_status,
						// 	'encash' => $encash,
						// 	'performance_stat' => '1'
						// );
					}
					$in = 0;
					$sql = "SELECT * FROM `oc_leave_transaction_temp` WHERE 1=1";
					$sql .= " AND emp_id = '".$emp_code."' AND `p_status` = '0' AND `a_status` = '1' AND `encash` <> '' ";
					$sql .= " AND `dot` >= '".$data['filter_date_start']."' AND `dot` <= '".$data['filter_date_end']."' GROUP BY `batch_id` ";
					$query = $this->db->query($sql);
					$encash = 0;
					foreach ($query->rows as $key => $value) {
						$sql1 = "SELECT * FROM `oc_leave_transaction_temp` WHERE 1=1";
						$sql1 .= " AND emp_id = '".$emp_code."' AND `batch_id` = '".$value['batch_id']."' ";	
						$query1 = $this->db->query($sql1);
						$p_status = 0;
						foreach($query1->rows as $qkey => $qvalue){
							if($qvalue['p_status'] == 1){
								$p_status = 1;
								break;
							}
						}
						if($p_status == 0){
							$in = 1;
							//$en_days_pl = $en_days_pl + $value['encash'];
							$encash = $encash + $value['encash'];
						}
					}

					if($in == 1){
						$encash_data = $query->row;
						$encash_status = 1;
						$total_days_pl = $total_days_pl + $encash_data['encash'];
						$start_date = $encash_data['date'];
						$end_date = $encash_data['date'];
						$performance_data['action'][$encash_data['date']] = array(
							'start_date' => date('d/m/Y', strtotime($start_date)),
							'end_date' => date('d/m/Y', strtotime($end_date)),
							'days_pl' => 0,
							'days_bl' => 0,
							'days_sl' => 0,
							'days_ml' => 0,
							'days_mal' => 0,
							'days_pal' => 0,
							'days_lwp' => 0,
							'leave_types' => 'F',
							'encash_status' => $encash_status,
							'encash' => $encash_data['encash'],
							'performance_stat' => '0'
						);
					}
				} else {
					$total_days_pl = 0;
					$total_days_bl = 0;
					$total_days_sl = 0;
					$total_days_ml = 0;
					$total_days_mal = 0;
					$total_days_pal = 0;
					$total_days_lwp = 0;
					$in = 0;
					$sql = "SELECT * FROM `oc_leave_transaction_temp` WHERE 1=1";
					$sql .= " AND emp_id = '".$emp_code."' AND `p_status` = '0' AND `a_status` = '1' AND `encash` <> '' ";
					$sql .= " AND `dot` >= '".$data['filter_date_start']."' AND `dot` <= '".$data['filter_date_end']."' GROUP BY `batch_id` ";
					$query = $this->db->query($sql);
					$encash = 0;
					foreach ($query->rows as $key => $value) {
						$sql1 = "SELECT * FROM `oc_leave_transaction_temp` WHERE 1=1";
						$sql1 .= " AND emp_id = '".$emp_code."' AND `batch_id` = '".$value['batch_id']."' ";	
						$query1 = $this->db->query($sql1);
						$p_status = 0;
						foreach($query1->rows as $qkey => $qvalue){
							if($qvalue['p_status'] == 1){
								$p_status = 1;
								break;
							}
						}
						if($p_status == 0){
							$in = 1;
							//$en_days_pl = $en_days_pl + $value['encash'];
							$encash = $encash + $value['encash'];
						}
					}

					if($in == 1){
						$encash_data = $query->row;
						$encash_status = 1;
						$total_days_pl = $encash_data['encash'];
						$start_date = $encash_data['date'];
						$end_date = $encash_data['date'];
						$performance_data['action'][$encash_data['date']] = array(
							'start_date' => date('d/m/Y', strtotime($start_date)),
							'end_date' => date('d/m/Y', strtotime($end_date)),
							'days_pl' => 0,
							'days_bl' => 0,
							'days_sl' => 0,
							'days_ml' => 0,
							'days_mal' => 0,
							'days_pal' => 0,
							'days_lwp' => 0,
							'leave_types' => 'F',
							'encash_status' => $encash_status,
							'encash' => $encash_data['encash'],
							'performance_stat' => '0'
						);
					}
				}

				if($filter_leave == 'PL'){
					$final_datas[$rvalue['emp_code']]['total_days_consumed'] = $total_days_pl;
				} elseif($filter_leave == 'BL'){
					$final_datas[$rvalue['emp_code']]['total_days_consumed'] = $total_days_bl;
				} elseif($filter_leave == 'SL'){
					$final_datas[$rvalue['emp_code']]['total_days_consumed'] = $total_days_sl;
				} elseif($filter_leave == 'ML'){
					$final_datas[$rvalue['emp_code']]['total_days_consumed'] = $total_days_ml;
				} elseif($filter_leave == 'MAL'){
					$final_datas[$rvalue['emp_code']]['total_days_consumed'] = $total_days_mal;
				} elseif($filter_leave == 'PAL'){
					$final_datas[$rvalue['emp_code']]['total_days_consumed'] = $total_days_pal;
				} elseif($filter_leave == 'LWP'){
					$final_datas[$rvalue['emp_code']]['total_days_consumed'] = $total_days_lwp;
				}
				$closing_pl = $leave_datas['pl_acc'] - $total_days_pl;
				$closing_bl = $leave_datas['bl_acc'] - $total_days_bl;
				$closing_sl = $leave_datas['sl_acc'] - $total_days_sl;
				$closing_ml = $leave_datas['ml_acc'] - $total_days_ml;
				$closing_mal = $leave_datas['mal_acc'] - $total_days_mal;
				$closing_pal = $leave_datas['pal_acc'] - $total_days_pal;
				$closing_lwp = $leave_datas['lwp_acc'] - $total_days_lwp;
				if($filter_leave == 'PL'){
					$final_datas[$rvalue['emp_code']]['closing_bal'] = $closing_pl;	
				} elseif($filter_leave == 'SL'){
					$final_datas[$rvalue['emp_code']]['closing_bal'] = $closing_sl;	
				} elseif($filter_leave == 'BL'){
					$final_datas[$rvalue['emp_code']]['closing_bal'] = $closing_bl;	
				} elseif($filter_leave == 'ML'){
					$final_datas[$rvalue['emp_code']]['closing_bal'] = $closing_ml;	
				} elseif($filter_leave == 'MAL'){
					$final_datas[$rvalue['emp_code']]['closing_bal'] = $closing_mal;	
				} elseif($filter_leave == 'PAL'){
					$final_datas[$rvalue['emp_code']]['closing_bal'] = $closing_pal;	
				} elseif($filter_leave == 'LWP'){
					$final_datas[$rvalue['emp_code']]['closing_bal'] = $closing_lwp;	
				}
				//$final_datas[$rvalue['emp_code']]['tran_data'] = $performance_data;
			}
			//exit;
		}

		//$final_datas = array();
		//$final_datass = array_chunk($final_datas, 5);

		// echo '<pre>';
		// print_r($final_datas);
		// exit;
		
		if($final_datas){
			//$month = date("F", mktime(0, 0, 0, $filter_month, 10));
			$filter_year1 = $filter_year.'-01-01';
			$template = new Template();		
			$template->data['final_datas'] = $final_datas;
			$template->data['filter_date_start'] = $filter_year1;
			$template->data['filter_date_end'] = $filter_date_end;
			$template->data['filter_leave'] = $filter_leave;
			$template->data['filter_year'] = $filter_year;
			$template->data['title'] = 'Leave Register';
			if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
				$template->data['base'] = HTTPS_SERVER;
			} else {
				$template->data['base'] = HTTP_SERVER;
			}
			$html = $template->fetch('report/leave_register_custom_html.tpl');
			//echo $html;exit;
			$filename = "Leave_Register_".$filter_leave;
			
			header("Content-Type: application/vnd.ms-excel; charset=utf-8");
			header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false);
			echo $html;
			exit;		
		} else {
			$this->session->data['warning'] = 'No Data Found';
			$this->redirect($this->url->link('report/leave_register', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/employee');

			if(isset($this->session->data['dept_names'])){
				$filter_departments = html_entity_decode($this->session->data['dept_names']);
				$filter_departments = "'" . str_replace(",", "','", html_entity_decode($filter_departments)) . "'";
			} else {
				$filter_departments = '';
			}

			if($filter_departments == ''){
				if(isset($this->session->data['dept_name'])){
					$filter_department = $this->session->data['dept_name'];
				} else {
					$filter_department = '';
				}
			} else {
				$filter_department = '';
			}

			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				'filter_departments' => $filter_departments,
				'filter_department' => $filter_department,
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_employee->getemployees($data);

			foreach ($results as $result) {
				$json[] = array(
					'employee_id' => $result['employee_id'],
					'emp_code' => $result['emp_code'], 
					'name'            => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}
}
?>