<?php
class ControllerTransactionDayprocess extends Controller { 
	public function index() {  
		$this->language->load('transaction/dayprocess');
		$this->load->model('transaction/dayprocess');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['unit'])) {
			$unit = $this->request->get['unit'];
		} else {
			if($this->user->getId() == 1) {
				$unit = '';
			} else if($this->user->getId() == 7) {
				$unit = 'Moving';
			} else if($this->user->getId() == 3) {
				$unit = 'Mumbai';
			} else if($this->user->getId() == 4) {
				$unit = 'Pune';
			} else {
				$unit = 'Mumbai';
			}
		}
		//echo $this->user->getId();
		//exit;
		$filter_date = $this->model_transaction_dayprocess->getNextDate($unit);
		$this->data['filter_date'] = $filter_date;

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';


		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('transaction/dayprocess', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->load->model('report/common_report');
		$this->load->model('report/attendance');
		$this->load->model('catalog/employee');
		

		$this->data['exceptional'] = array();

		$data = array(
			'filter_date'	     => $filter_date,
			'unit'				 => $unit,
			'start'              => ($page - 1) * 7000,
			'limit'              => 7000
		);

		if($this->user->getId() == 1) {
			$unit_data = array(
				'' => 'All',
				'Mumbai' => 'Mumbai',
				'Pune' => 'Pune',
				'Delhi' => 'Delhi',
				'Chennai' => 'Chennai',
				'Bangalore' => 'Bangalore',
				'Ahmedabad' => 'Ahmedabad', 
			);
		} else if($this->user->getId() == 7) {
			$unit_data = array(
				'Moving' => 'Moving' 
			);
		} else if($this->user->getId() == 3) {
			$unit_data = array(
				'Mumbai' => 'Mumbai' 
			);
		} else if($this->user->getId() == 4) {
			$unit_data = array(
				'Pune' => 'Pune' 
			);
		} else {
			$unit_data = array(
				'' => 'All',
				'Mumbai' => 'Mumbai',
				'Pune' => 'Pune',
				'Delhi' => 'Delhi',
				'Chennai' => 'Chennai',
				'Bangalore' => 'Bangalore',
				'Ahmedabad' => 'Ahmedabad', 
			);
		}

		$this->data['unit_data'] = $unit_data;
		$exceptional = array();
		$exceptional = $this->model_transaction_dayprocess->getData($filter_date, $unit);
		foreach ($exceptional as $ekey => $evalue) {
			$man_href = $this->url->link('transaction/manualpunch', 'token=' . $this->session->data['token'] . '&transaction_id=' . $evalue['transaction_id'] . '&unit=' . $unit, 'SSL');
			$exceptional[$ekey]['man_href'] = $man_href;
		}
		$this->data['exceptional'] = $exceptional;

		$exp_count_sql = "SELECT COUNT(*) as act_total FROM `oc_employee` WHERE `status` = '1' ";
		if($unit){
			$exp_count_sql .= " AND `unit` = '".$unit."' ";
		}
		$exp_count_datas = $this->db->query($exp_count_sql);
		$exp_count = '0';		
		if($exp_count_datas->num_rows > 0){
			$exp_count = $exp_count_datas->row['act_total'];		
		}

		$act_count_sql = "SELECT COUNT(*) as act_total FROM `oc_transaction` WHERE `date` = '".$filter_date."' ";
		if($unit){
			$act_count_sql .= " AND `unit` = '".$unit."' ";
		}
		$act_count_datas = $this->db->query($act_count_sql);
		$act_count = '0';		
		if($act_count_datas->num_rows > 0){
			$act_count = $act_count_datas->row['act_total'];		
		}
		
		if(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		if(isset($this->session->data['success'])){
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->data['count_mismatch'] = '0';
		if($act_count > '0' && $exp_count != $act_count){
			$this->data['count_mismatch'] = '1';
			$this->data['error_warning'] = 'The Records in Master and Transaction Does not Match';
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_all_status'] = $this->language->get('text_all_status');

		
		$this->data['entry_date'] = $this->language->get('entry_date');
		
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['button_export'] = $this->language->get('button_export');

		$this->data['token'] = $this->session->data['token'];

		$url = '';

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . $this->request->get['filter_date'];
		}
		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		
		$this->data['filter_date'] = $filter_date;
		$this->data['unit'] = $unit;
		
		$this->template = 'transaction/dayprocess.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	public function close_day() {
		$this->load->model('transaction/dayprocess');

		if (isset($this->request->get['unit'])) {
			$unit = $this->request->get['unit'];
		} else {
			$unit = '';
		}

		$filter_date = $this->model_transaction_dayprocess->getNextDate($unit);
		$this->data['filter_date'] = $filter_date;

		$absent = $this->model_transaction_dayprocess->getAbsent($filter_date,$unit);
		// echo '<pre>';
		// print_r($absent);
		// exit;
		foreach($absent as $data) {
			$check_leave = $this->model_transaction_dayprocess->checkLeave($data['date'],$data['emp_id']);
			$this->model_transaction_dayprocess->checkLeave_1($data['date'],$data['emp_id']);
		}
		
		$this->model_transaction_dayprocess->update_close_day($filter_date, $unit);
		
		$url = '';

		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}

		$this->session->data['success'] = 'Day Closed Sucessfully';
		
		$this->redirect($this->url->link('transaction/dayprocess', 'token=' . $this->session->data['token'].$url, 'SSL'));
	}	

	public function export(){

		$this->language->load('transaction/dayprocess');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('transaction/dayprocess');

		if (isset($this->request->get['unit'])) {
			$unit = $this->request->get['unit'];
		} else {
			$unit = 'Mumbai';
		}

		$filter_date = $this->model_transaction_dayprocess->getNextDate($unit);
		$this->data['filter_date'] = $filter_date;

		$this->load->model('report/common_report');
		$this->load->model('report/attendance');
		$this->load->model('catalog/employee');

		$this->data['exceptional'] = array();

		$exceptional = $this->model_transaction_dayprocess->getData($filter_date, $unit);
		$this->data['exceptional'] = $exceptional;
		
		if($exceptional){
			$template = new Template();		
			$template->data['exceptional'] = $exceptional;
			$template->data['filter_date'] = $filter_date;

			$template->data['title'] = 'Day Process';
			if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
				$template->data['base'] = HTTPS_SERVER;
			} else {
				$template->data['base'] = HTTP_SERVER;
			}
			$html = $template->fetch('transaction/dayprocess_html.tpl');
			//echo $html;exit;
			$filename = "day_processing";
			
			
			header("Content-Type: application/vnd.ms-excel; charset=utf-8");
			header("Content-Disposition: attachment; filename=".$filename.".xls");//File name extension was wrong
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false);
			echo $html;
			exit;		
		} else {
			$this->session->data['warning'] = 'No Data Found';
			$this->redirect($this->url->link('transaction/dayprocess', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}
	}
}
?>