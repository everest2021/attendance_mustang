<?php
/*
@author: Eric
This code is for leave processing.
First we select all the unprocessed leaves for a particular location and display on screen.
When the "Leave Process" button is clicked 

*/
class ControllerTransactionLeaveprocess extends Controller { 
	public function index() {  
		$this->language->load('transaction/leaveprocess');
		$this->load->model('transaction/leaveprocess');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['unit'])) {
			$unit = $this->request->get['unit'];
		} else {
			$unit = '';
		}

		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';


		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('transaction/leaveprocess', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->load->model('report/common_report');
		$this->load->model('report/attendance');
		$this->load->model('catalog/employee');
		

		$this->data['exceptional'] = array();

		$data = array(
			'unit'				 => $unit,
			'start'              => ($page - 1) * 7000,
			'limit'              => 7000
		);

		$unit_data = array(
			'' => 'All',
			'Mumbai' => 'Mumbai',
			'Pune' => 'Pune',
			'Delhi' => 'Delhi',
			'Chennai' => 'Chennai',
			'Bangalore' => 'Bangalore',
			'Ahmedabad' => 'Ahmedabad', 
		);

		$this->data['unit_data'] = $unit_data;
		
		$unprocessed = $this->model_transaction_leaveprocess->getUnprocessedLeaveTillDate($unit);
		foreach($unprocessed as $akey => $avalue){
			$action = array();
			if($avalue['a_status'] == '0'){
				// $action[] = array(
				// 	'text' => 'Activate',
				// 	'href' => $this->url->link('transaction/leave/active', 'token=' . $this->session->data['token'] . '&batch_id=' . $avalue['batch_id'] . '&filter_name_id=' . $avalue['emp_id'] . '&unit=' . $unit . '&leaveprocess=1', 'SSL')
				// );
				$action[] = array(
					'text' => 'In Active',
					'href' => ''
				);	
			} else {
				$action[] = array(
					'text' => 'Active',
					'href' => ''
				);
			}
			$action[] = array(
				'text' => 'View',
				'href' => $this->url->link('transaction/leave/getForm', 'token=' . $this->session->data['token'] . '&batch_id=' . $avalue['batch_id'] . '&filter_name_id=' . $avalue['emp_id'] . '&unit=' . $unit . '&leaveprocess=1', 'SSL')
			);
			// $action[] = array(
			// 	'text' => 'Reject',
			// 	'href' => $this->url->link('transaction/leave/reject', 'token=' . $this->session->data['token'] . '&batch_id=' . $avalue['batch_id'] . '&unit=' . $unit . '&leaveprocess=1', 'SSL')
			// );
			$unprocessed[$akey]['action'] = $action;
		}
		// echo "<pre>"; 
		// print_r($unprocessed);
		// exit;
		$this->data['unprocessed'] = $unprocessed;
		
		if(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_all_status'] = $this->language->get('text_all_status');

		
		$this->data['entry_date'] = $this->language->get('entry_date');
		
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['button_export'] = $this->language->get('button_export');

		$this->data['token'] = $this->session->data['token'];

		if(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		
		$this->data['unit'] = $unit;
		
		$this->template = 'transaction/leaveprocess.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	public function close_day() {
		
		$this->load->model('transaction/leaveprocess');
		$this->load->model('transaction/dayprocess');

		if (isset($this->request->get['unit'])) {
			$unit = $this->request->get['unit'];
		} else {
			$unit = '';
		}

		$unprocessed = $this->model_transaction_leaveprocess->getUnprocessedLeaveTillDate_2($unit);
		
		foreach($unprocessed as $data) {
			$is_closed = $this->model_transaction_dayprocess->is_closed_stat($data['date'], $data['emp_id']); 
			if($is_closed == 1){
				$this->model_transaction_dayprocess->checkLeave($data['date'], $data['emp_id']);
			}
		}

		$url = '';

		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		
		$this->redirect($this->url->link('transaction/leaveprocess', 'token=' . $this->session->data['token'].$url, 'SSL'));
	}

	public function close_day_1() {
		$this->load->model('transaction/leaveprocess');
		$this->load->model('transaction/dayprocess');

		$unprocessed = $this->model_transaction_leaveprocess->getUnprocessedLeaveTillDate_1();
		// echo '<pre>';
		// print_r($unprocessed);
		// exit;
		foreach($unprocessed as $data) {
			$is_closed = $this->model_transaction_dayprocess->is_closed_stat($data['date'], $data['emp_id']); 
			if($is_closed == 1){
				$this->model_transaction_dayprocess->checkLeave_1($data['date'], $data['emp_id']);
			}
		}
		echo 'out';exit;

		$url = '';

		if (isset($this->request->get['unit'])) {
			$url .= '&unit=' . $this->request->get['unit'];
		}
		
		$this->redirect($this->url->link('transaction/leaveprocess', 'token=' . $this->session->data['token'].$url, 'SSL'));
	}	

	
}
?>