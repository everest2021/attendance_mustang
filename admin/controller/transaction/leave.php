<?php    
class ControllerTransactionLeave extends Controller { 
	private $error = array();

	public function index() {
		$this->language->load('transaction/leave');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('transaction/transaction');
		$this->load->model('catalog/shift');
		$this->load->model('catalog/employee');

		$this->getList();
	}

	public function leave_script(){
		$sql = "SELECT * FROM `oc_leave_transaction` WHERE `dot` = '0000-00-00' ORDER BY `date` ASC ";
		$datas = $this->db->query($sql)->rows;
		// echo '<pre>';
		// print_r($datas);
		// exit;
		foreach ($datas as $key => $value) {
			$date_exp = explode('-', $value['date']);
			if($date_exp[1] == '12'){
				if($date_exp[2] >= '16'){
					$date_up = '2015-12-20';
					$update = "UPDATE `oc_leave_transaction` SET `dot` = '".$date_up."' WHERE `id` = '".$value['id']."' ";
					$this->db->query($update);
					echo $update;
					echo '<br/ >';
				}
			} elseif($date_exp[1] == '1' || $date_exp[1] == '2') {
				//if($date_exp[2] <= '15'){
					$date_up = '2016-01-07';
					$update = "UPDATE `oc_leave_transaction` SET `dot` = '".$date_up."' WHERE `id` = '".$value['id']."' ";
					$this->db->query($update);
					echo $update;
					echo '<br/ >';
				//}
			}
			//$date_p = date('Y-m-d', strtotime($value['date'] . "-2 day"));
		}
		echo 'Done';
		exit;
	}

	public function insert_1() {
		$this->language->load('transaction/leave');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('transaction/transaction');
		$this->load->model('catalog/shift');
		$this->load->model('catalog/employee');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateRejectForm()) {
			// echo '<pre>';
			// print_r($this->request->post);
			// exit;
			$data = $this->request->post;
			$batch_id = $data['batch_id'];
			
			$leav_data_sql = "SELECT * FROM `oc_leave_transaction` WHERE `batch_id` = '".$batch_id."' ";
			$leav_data = $this->db->query($leav_data_sql)->rows;

			$approve_sql = "UPDATE `oc_leave_transaction_temp` SET `reject_reason` = '".$this->db->escape($data['reject_reason'])."', `approval_date_1` = '0000-00-00', `approval_date_2` = '0000-00-00', `approved_1_by` = '0', `approval_1` = '0', `approval_2` = '0', `reject_date` = '".date('Y-m-d')."' WHERE `batch_id` = '".$leav_data[0]['linked_batch_id']."' ";
			$this->db->query($approve_sql);

			if(isset($leav_data[0]['encash']) && $leav_data[0]['encash'] != ''){
				$encah_leave = $leav_data[0]['encash'];
				$sql = "SELECT * FROM `oc_leave` WHERE `emp_id` = '".$leav_data[0]['emp_id']."' AND `close_status` = '0' ";
				$query1 = $this->db->query($sql);
				if($query1->num_rows > 0){
					if($leav_data[0]['leave_type'] == 'PL'){
						//$balance = $query1->row['pl_bal'];
						$total_bal = $this->model_transaction_transaction->gettotal_bal_ess($leav_data[0]['emp_id']);
						$total_bal_pro = $this->model_transaction_transaction->gettotal_bal_pro_ess($leav_data[0]['emp_id'], 'PL');
						$total_bal_pro1 = $this->model_transaction_transaction->gettotal_bal_pro1_ess($leav_data[0]['emp_id'], 'PL');
						$total_bal_pl = 0;
						if(isset($total_bal['pl_acc'])){
							if(isset($total_bal_pro['bal_p'])){
								$total_bal_pl = $total_bal['pl_acc'] - ($total_bal_pro['bal_p'] + $total_bal_pro1['leave_amount']);
							} else {
								$total_bal_pl = $total_bal['pl_acc'];
							}
						}
						$balance = $total_bal_pl + $leav_data[0]['encash'];
						$upd_bal_sql = "UPDATE `oc_leave` SET `pl_bal` = '".$balance."' WHERE `emp_id` = '".$query1->row['emp_id']."' AND `close_status` = '0' ";
						$this->db->query($upd_bal_sql);
						//$this->log->write($upd_bal_sql);
					} 
				}
			}
			$approve_sql = "DELETE FROM `oc_leave_transaction` WHERE `batch_id` = '".$batch_id."' ";
			$this->db->query($approve_sql);	
			$this->redirect($this->url->link('transaction/leaveprocess', 'token=' . $this->session->data['token'].'&unit='.$this->request->get['unit'], 'SSL'));
		}
		$this->getForm();
		
	}

	public function insert() {
		$this->language->load('transaction/leave');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('transaction/transaction');
		$this->load->model('catalog/shift');
		$this->load->model('catalog/employee');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			// echo '<pre>';
			// print_r($this->request->post);
			// exit;
			$data = $this->request->post;
			
			$emp_data = $this->model_transaction_transaction->getEmployees_dat($data['e_name_id']);
			if(isset($emp_data['emp_code'])){
				$department = $emp_data['department'];
				$unit = $emp_data['unit'];
				$group = $emp_data['group'];
			} else {
				$department = '';
				$unit = '';
				$group = '';
			}

			$days = $this->GetDays($data['from'], $data['to']);
			$day = array();
			foreach ($days as $dkey => $dvalue) {
	        	$dates = explode('-', $dvalue);
	        	$day[$dkey]['day'] = $dates[2];
	        	$day[$dkey]['date'] = $dvalue;
	        }

	        if($data['multi_day'] == 1){
	        	$data['leave_amount'] = '';
	        	$data['type'] = '';
	        } else {
	        	$data['days'] = '';
	        	$data['encash'] = '';
	        }

	        $batch_id_sql = "SELECT `batch_id` FROM `oc_leave_transaction` ORDER BY `batch_id` DESC LIMIT 1";
			$obatch_ids = $this->db->query($batch_id_sql);
			if($obatch_ids->num_rows == 0){
				$batch_id = 1;
			} else{
				$obatch_id = $obatch_ids->row['batch_id'];
				$batch_id = $obatch_id + 1;
			}

			$batch_id_sql1 = "SELECT `batch_id` FROM `oc_leave_transaction_temp` ORDER BY `batch_id` DESC LIMIT 1";
			$obatch_ids1 = $this->db->query($batch_id_sql1);
			if($obatch_ids1->num_rows == 0){
				$batch_id1 = 1;
			} else{
				$obatch_id1 = $obatch_ids1->row['batch_id'];
				$batch_id1 = $obatch_id1 + 1;
			}

			if($data['enable_encash'] == 0){
				$data['encash'] = '';
			}
			//echo $batch_id;exit;
			foreach($day as $dkey => $dvalue){
				$sql = "INSERT INTO `oc_leave_transaction_temp` SET 
					`emp_id` = '".$data['e_name_id']."', 
					`leave_type` = '".$data['leave_type']."',
					`date` = '".$dvalue['date']."',
					`date_cof` = '".$data['date_cof']."',
					`leave_amount` = '".$data['leave_amount']."',
					`type` = '".$data['type']."',
					`days` = '".$data['days']."',
					`encash` = '".$data['encash']."',
					`a_status` = '1',
					`approval_1` = '1',
					`approval_2` = '1',
					`unit` = '".$unit."',
					`group` = '".$group."',
					`leave_reason` = '".$data['leave_reason']."',
					`year` = '".date('Y', strtotime($dvalue['date']))."',
					`dot` = '".date('Y-m-d')."',
					`dept_name` = '".$department."',
					`batch_id` = '".$batch_id1."' ";
				//echo $sql;
				//echo '<br />';
				$this->db->query($sql);

				$sql = "INSERT INTO `oc_leave_transaction` SET 
					`emp_id` = '".$data['e_name_id']."', 
					`leave_type` = '".$data['leave_type']."',
					`date` = '".$dvalue['date']."',
					`date_cof` = '".$data['date_cof']."',
					`leave_amount` = '".$data['leave_amount']."',
					`type` = '".$data['type']."',
					`days` = '".$data['days']."',
					`encash` = '".$data['encash']."',
					`a_status` = '1',
					`unit` = '".$unit."',
					`group` = '".$group."',
					`leave_reason` = '".$data['leave_reason']."',
					`year` = '".date('Y', strtotime($dvalue['date']))."',
					`dot` = '".date('Y-m-d')."',
					`batch_id` = '".$batch_id."', 
					`linked_batch_id` = '".$batch_id1."' ";
				//echo $sql;
				//echo '<br />';
				$this->db->query($sql);
			}

			if($data['enable_encash'] == 1 && $data['encash'] > 0 && $data['leave_type'] = 'PL'){
				//$pl_bal_sql = "SELECT `pl_bal` FROM `oc_leave` WHERE emp_id = '".$data['e_name_id']."' ";
				//$pl_bal_res = $this->db->query($pl_bal_sql);

				$total_bal = $this->model_transaction_transaction->gettotal_bal($data['e_name_id']);
				$total_bal_pro = $this->model_transaction_transaction->gettotal_bal_pro($data['e_name_id'], 'PL');
				$total_bal_pro1 = $this->model_transaction_transaction->gettotal_bal_pro1($data['e_name_id'], 'PL');
				$total_bal_pl = 0;
				if(isset($total_bal['pl_acc'])){
					if(isset($total_bal_pro['bal_p'])){
						$total_bal_pl = $total_bal['pl_acc'] - ($total_bal_pro['bal_p'] + $total_bal_pro1['leave_amount']);
					} else {
						$total_bal_pl = $total_bal['pl_acc'];
					}
				}

				if($total_bal_pl > 0){
					$pl_balance = $total_bal_pl;
					$pl_balance = $pl_balance - $data['encash'];
					$upd_pl_bal_sql = "UPDATE `oc_leave` SET `pl_bal` = '".$pl_balance."' WHERE `emp_id` = '".$data['e_name_id']."' AND `close_status` = '0' ";
					//echo $upd_pl_bal_sql;
					//echo '<br />';
					$this->db->query($upd_pl_bal_sql);
				}
			}

			//exit;
			$this->session->data['success'] = 'You have Inserted the Leave Transaction';	
			$url = '';
			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['filter_name_id_1'])) {
				$url .= '&filter_name_id_1=' . $this->request->get['filter_name_id_1'];
			}

			$this->redirect($this->url->link('transaction/leave', 'token=' . $this->session->data['token'].$url.'&batch_id='.$batch_id, 'SSL'));
		}

		$this->getForm();
	}

	public function delete() {
		$this->language->load('transaction/leave');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('transaction/transaction');
		$this->load->model('catalog/shift');
		$this->load->model('catalog/employee');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $batch_id) {
				$leav_data_sql = "SELECT * FROM `oc_leave_transaction` WHERE `batch_id` = '".$batch_id."' ";
				$leav_data = $this->db->query($leav_data_sql)->row;
				if(isset($leav_data['encash']) && $leav_data['encash'] != ''){
					$encah_leave = $leav_data['encash'];
					$sql = "SELECT * FROM `oc_leave` WHERE `emp_id` = '".$leav_data['emp_id']."' AND `close_status` = '0' ";
					$query1 = $this->db->query($sql);
					if($query1->num_rows > 0){
						if($query->row['leave_type'] == 'PL'){
							//$balance = $query1->row['pl_bal'];
							$total_bal = $this->model_transaction_transaction->gettotal_bal($leav_data['emp_id']);
							$total_bal_pro = $this->model_transaction_transaction->gettotal_bal_pro($leav_data['emp_id'], 'PL');
							$total_bal_pro1 = $this->model_transaction_transaction->gettotal_bal_pro1($leav_data['emp_id'], 'PL');
							$total_bal_pl = 0;
							if(isset($total_bal['pl_acc'])){
								if(isset($total_bal_pro['bal_p'])){
									$total_bal_pl = $total_bal['pl_acc'] - ($total_bal_pro['bal_p'] + $total_bal_pro1['leave_amount']);
								} else {
									$total_bal_pl = $total_bal['pl_acc'];
								}
							}
							$balance = $total_bal_pl + $leav_data['encash'];
							$upd_bal_sql = "UPDATE `oc_leave` SET `pl_bal` = '".$balance."' WHERE `emp_id` = '".$query1->row['emp_id']."' AND `close_status` = '0' ";
							$this->db->query($upd_bal_sql);
							$this->log->write($upd_bal_sql);
						} 
					}
				}
				$can_leave_sql = "DELETE FROM `oc_leave_transaction` WHERE `batch_id` = '".$batch_id."' ";
				$this->db->query($can_leave_sql);

				$can_leave_sql = "DELETE FROM `oc_leave_transaction_temp` WHERE `batch_id` = '".$leav_data['linked_batch_id']."' ";
				$this->db->query($can_leave_sql);
			}

			$this->session->data['success'] = 'Leave Deleted Successfully';

			$url = '';
			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['filter_name_id_1'])) {
				$url .= '&filter_name_id_1=' . $this->request->get['filter_name_id_1'];
			}

			$this->redirect($this->url->link('transaction/leave', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		} elseif(isset($this->request->get['batch_id']) && $this->validateDelete()){
			$batch_id = $this->request->get['batch_id'];
			$leav_data_sql = "SELECT * FROM `oc_leave_transaction` WHERE `batch_id` = '".$batch_id."' ";
			$leav_data = $this->db->query($leav_data_sql)->row;
			if(isset($leav_data['encash']) && $leav_data['encash'] != ''){
				$encah_leave = $leav_data['encash'];
				$sql = "SELECT * FROM `oc_leave` WHERE `emp_id` = '".$leav_data['emp_id']."' AND `close_status` = '0' ";
				$query1 = $this->db->query($sql);
				if($query1->num_rows > 0){
					if($leav_data['leave_type'] == 'PL'){
						//$balance = $query1->row['pl_bal'];
						$total_bal = $this->model_transaction_transaction->gettotal_bal($leav_data['emp_id']);
						$total_bal_pro = $this->model_transaction_transaction->gettotal_bal_pro($leav_data['emp_id'], 'PL');
						$total_bal_pro1 = $this->model_transaction_transaction->gettotal_bal_pro1($leav_data['emp_id'], 'PL');
						$total_bal_pl = 0;
						if(isset($total_bal['pl_acc'])){
							if(isset($total_bal_pro['bal_p'])){
								$total_bal_pl = $total_bal['pl_acc'] - ($total_bal_pro['bal_p'] + $total_bal_pro1['leave_amount']);
							} else {
								$total_bal_pl = $total_bal['pl_acc'];
							}
						}
						$balance = $total_bal_pl + $leav_data['encash'];
						$upd_bal_sql = "UPDATE `oc_leave` SET `pl_bal` = '".$balance."' WHERE `emp_id` = '".$query1->row['emp_id']."' AND `close_status` = '0' ";
						$this->db->query($upd_bal_sql);
						$this->log->write($upd_bal_sql);
					} 
				}
			}
			$can_leave_sql = "DELETE FROM `oc_leave_transaction` WHERE `batch_id` = '".$batch_id."' ";
			$this->db->query($can_leave_sql);

			$can_leave_sql = "DELETE FROM `oc_leave_transaction_temp` WHERE `batch_id` = '".$leav_data['linked_batch_id']."' ";
			$this->db->query($can_leave_sql);
			
			$this->session->data['success'] = 'Leave Deleted Successfully';

			$url = '';
			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['filter_name_id_1'])) {
				$url .= '&filter_name_id_1=' . $this->request->get['filter_name_id_1'];
			}

			$this->redirect($this->url->link('transaction/leave', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	public function revert() {
		$this->language->load('transaction/leave');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('transaction/transaction');
		$this->load->model('catalog/shift');
		$this->load->model('catalog/employee');

		if(isset($this->request->get['batch_id']) && $this->validateDelete()){
			$batch_id = $this->request->get['batch_id'];
			$leav_data_sql = "SELECT * FROM `oc_leave_transaction` WHERE `batch_id` = '".$batch_id."' ";
			$leav_data = $this->db->query($leav_data_sql)->row;
			
			if(isset($leav_data['encash']) && $leav_data['encash'] != ''){
				//echo 'in encash';exit;
				$encah_leave = $leav_data['encash'];
				$sql = "SELECT * FROM `oc_leave` WHERE `emp_id` = '".$leav_data['emp_id']."' AND `close_status` = '0' ";
				$query1 = $this->db->query($sql);
				if($query1->num_rows > 0){
					if($leav_data['leave_type'] == 'PL'){
						//$balance = $query1->row['pl_bal'];
						$total_bal = $this->model_transaction_transaction->gettotal_bal($leav_data['emp_id']);
						$total_bal_pro = $this->model_transaction_transaction->gettotal_bal_pro($leav_data['emp_id'], 'PL');
						$total_bal_pro1 = $this->model_transaction_transaction->gettotal_bal_pro1($leav_data['emp_id'], 'PL');
						$total_bal_pl = 0;
						if(isset($total_bal['pl_acc'])){
							if(isset($total_bal_pro['bal_p'])){
								$total_bal_pl = $total_bal['pl_acc'] - ($total_bal_pro['bal_p'] + $total_bal_pro1['leave_amount']);
							} else {
								$total_bal_pl = $total_bal['pl_acc'];
							}
						}
						$balance = $total_bal_pl + $leav_data['encash'] + $leav_data['days'];
						$upd_bal_sql = "UPDATE `oc_leave` SET `pl_bal` = '".$balance."' WHERE `emp_id` = '".$query1->row['emp_id']."' AND `close_status` = '0' ";
						$this->db->query($upd_bal_sql);
						//$this->log->write($upd_bal_sql);
						//echo $upd_bal_sql;
						//echo '<br />';
					} 
				}
			} else {
				$sql = "SELECT * FROM `oc_leave` WHERE `emp_id` = '".$leav_data['emp_id']."' AND `close_status` = '0' ";
				$query1 = $this->db->query($sql);
				if($query1->num_rows > 0){
					if($leav_data['leave_type'] == 'PL'){
						//echo 'in pl';exit;
						//$balance = $query1->row['pl_bal'];
						$total_bal = $this->model_transaction_transaction->gettotal_bal($leav_data['emp_id']);
						$total_bal_pro = $this->model_transaction_transaction->gettotal_bal_pro($leav_data['emp_id'], 'PL');
						$total_bal_pro1 = $this->model_transaction_transaction->gettotal_bal_pro1($leav_data['emp_id'], 'PL');
						$total_bal_pl = 0;
						if(isset($total_bal['pl_acc'])){
							if(isset($total_bal_pro['bal_p'])){
								$total_bal_pl = $total_bal['pl_acc'] - ($total_bal_pro['bal_p'] + $total_bal_pro1['leave_amount']);
							} else {
								$total_bal_pl = $total_bal['pl_acc'];
							}
						}
						$balance = $total_bal_pl + $leav_data['encash'] + $leav_data['days'];
						$upd_bal_sql = "UPDATE `oc_leave` SET `pl_bal` = '".$balance."' WHERE `emp_id` = '".$query1->row['emp_id']."' AND `close_status` = '0' ";
						$this->db->query($upd_bal_sql);
						//$this->log->write($upd_bal_sql);
						//echo $upd_bal_sql;
						//echo '<br />';
					} elseif($leav_data['leave_type'] == 'CL'){
						//echo 'in cl';exit;
						//$balance = $query1->row['cl_bal'];
						$total_bal = $this->model_transaction_transaction->gettotal_bal($leav_data['emp_id']);
						$total_bal_pro = $this->model_transaction_transaction->gettotal_bal_pro($leav_data['emp_id'], 'CL');
						$total_bal_pro1 = $this->model_transaction_transaction->gettotal_bal_pro1($leav_data['emp_id'], 'CL');
						$total_bal_cl = 0;
						if(isset($total_bal['cl_acc'])){
							if(isset($total_bal_pro['bal_p'])){
								$total_bal_cl = $total_bal['cl_acc'] - ($total_bal_pro['bal_p'] + $total_bal_pro1['leave_amount']);
							} else {
								$total_bal_cl = $total_bal['cl_acc'];
							}
						}
						$balance = $total_bal_cl + $leav_data['days'];
						$upd_bal_sql = "UPDATE `oc_leave` SET `cl_bal` = '".$balance."' WHERE `emp_id` = '".$query1->row['emp_id']."' AND `close_status` = '0' ";
						$this->db->query($upd_bal_sql);
						//$this->log->write($upd_bal_sql);
						//echo $upd_bal_sql;
						//echo '<br />';
					} elseif($leav_data['leave_type'] == 'SL'){
						//echo 'in sl';exit;
						//$balance = $query1->row['sl_bal'];
						$total_bal = $this->model_transaction_transaction->gettotal_bal($leav_data['emp_id']);
						$total_bal_pro = $this->model_transaction_transaction->gettotal_bal_pro($leav_data['emp_id'], 'SL');
						$total_bal_pro1 = $this->model_transaction_transaction->gettotal_bal_pro1($leav_data['emp_id'], 'SL');
						$total_bal_sl = 0;
						if(isset($total_bal['sl_acc'])){
							if(isset($total_bal_pro['bal_p'])){
								$total_bal_sl = $total_bal['sl_acc'] - ($total_bal_pro['bal_p'] + $total_bal_pro1['leave_amount']);
							} else {
								$total_bal_sl = $total_bal['sl_acc'];
							}
						}
						$balance = $total_bal_sl + $leav_data['days'];
						$upd_bal_sql = "UPDATE `oc_leave` SET `sl_bal` = '".$balance."' WHERE `emp_id` = '".$query1->row['emp_id']."' AND `close_status` = '0' ";
						$this->db->query($upd_bal_sql);
						//$this->log->write($upd_bal_sql);
						//echo $upd_bal_sql;
						//echo '<br />';
					} elseif($leav_data['leave_type'] == 'COF'){
						//echo 'in cof';exit;
						//$balance = $query1->row['cof_bal'];
						$total_bal = $this->model_transaction_transaction->gettotal_bal($leav_data['emp_id']);
						$total_bal_pro = $this->model_transaction_transaction->gettotal_bal_pro($leav_data['emp_id'], 'COF');
						$total_bal_pro1 = $this->model_transaction_transaction->gettotal_bal_pro1($leav_data['emp_id'], 'COF');
						$total_bal_cof = 0;
						if(isset($total_bal['cof_acc'])){
							if(isset($total_bal_pro['bal_p'])){
								$total_bal_cof = $total_bal['cof_acc'] - ($total_bal_pro['bal_p'] + $total_bal_pro1['leave_amount']);
							} else {
								$total_bal_cof = $total_bal['cof_acc'];
							}
						}
						$balance = $total_bal_cof + $leav_data['days'];
						$upd_bal_sql = "UPDATE `oc_leave` SET `cof_bal` = '".$balance."' WHERE `emp_id` = '".$query1->row['emp_id']."' AND `close_status` = '0' ";
						$this->db->query($upd_bal_sql);
						//$this->log->write($upd_bal_sql);
						//echo $upd_bal_sql;
						//echo '<br />';
					}


				}
			}

			$can_leave_sql = "UPDATE `oc_leave_transaction` SET `a_status` = '0', `p_status` = '0' WHERE `batch_id` = '".$this->request->get['batch_id']."' ";
			// echo $can_leave_sql;
			// echo '<br />';
			$this->db->query($can_leave_sql);

			$can_leave_sql = "UPDATE `oc_leave_transaction_temp` SET `a_status` = '0', `p_status` = '0' WHERE `batch_id` = '".$leav_data['linked_batch_id']."' ";
			$this->db->query($can_leave_sql);

			$leav_data_sql = "SELECT * FROM `oc_leave_transaction` WHERE `batch_id` = '".$batch_id."' ";
			// echo $leav_data_sql;
			// echo '<br />';
			$leav_datas = $this->db->query($leav_data_sql)->rows;
			// echo '<pre>';
			// print_r($leav_datas);

			foreach($leav_datas as $lkey => $lvalue){
				$emp_id = $lvalue['emp_id'];
				$date = $lvalue['date'];
				$tran_data_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$lvalue['emp_id']."' AND `date` = '".$lvalue['date']."' ";
				$tran_datas = $this->db->query($tran_data_sql);				
				
				// echo '<pre>';
				// print_r($tran_datas);
			
				if($tran_datas->num_rows > 0){
					$tran_data = $tran_datas->row;
					if($tran_data['leave_status'] == '1'){
						if($tran_data['weekly_off'] != '0'){
							$firsthalf_status = 'WO';
							$secondhalf_status = 'WO';
							$absent_status = '0';
							$present_status = '0';
						} elseif($tran_data['holiday_id'] != '0'){
							$firsthalf_status = 'HLD';
							$secondhalf_status = 'HLD';
							$absent_status = '0';
							$present_status = '0';
						} elseif($tran_data['halfday_status'] != '0'){
							$firsthalf_status = '0';
							$secondhalf_status = '0';
							$absent_status = '1';
							$present_status = '0';
						} elseif($tran_data['compli_status'] != '0'){
							$firsthalf_status = 'COF';
							$secondhalf_status = 'COF';
							$absent_status = '0';
							$present_status = '0';
						} else {
							$firsthalf_status = '0';
							$secondhalf_status = '0';
							$absent_status = '1';
							$present_status = '0';
						}
					} elseif($tran_data['leave_status'] == '0.5'){
						if($lvalue['type'] == '1'){
							if($tran_data['secondhalf_status'] == 'HD'){
								$firsthalf_status = '0';
								$secondhalf_status = '0';
								$present_status = '0';
								$absent_status = '1';
							} else {
								if($tran_data['secondhalf_status'] == '0'){
									$firsthalf_status = '0';
									$secondhalf_status = '0';
									$present_status = '0';
									$absent_status = '1';
								} else {
									$firsthalf_status = '0';
									$secondhalf_status = $tran_data['secondhalf_status'];
									$present_status = '0.5';
									$absent_status = '0.5';
								}
							}
						} elseif($lvalue['type'] == '2'){
							if($tran_data['firsthalf_status'] == '0'){
								$firsthalf_status = '0';
								$secondhalf_status = '0';
								$present_status = '0';
								$absent_status = '1';
							} else {
								$firsthalf_status = $tran_data['firsthalf_status'];
								$secondhalf_status = '0';
								$present_status = '0.5';
								$absent_status = '0.5';
							}
						}
					} elseif($tran_data['weekly_off'] != '0'){
						$firsthalf_status = 'WO';
						$secondhalf_status = 'WO';
						$absent_status = '0';
						$present_status = '0';
					} elseif($tran_data['holiday_id'] != '0'){
						$firsthalf_status = 'HLD';
						$secondhalf_status = 'HLD';
						$absent_status = '0';
						$present_status = '0';
					} elseif($tran_data['halfday_status'] != '0'){
						$firsthalf_status = '1';
						$secondhalf_status = 'HD';
						$absent_status = '0';
						$present_status = '0';
					} elseif($tran_data['compli_status'] != '0'){
						$firsthalf_status = 'COF';
						$secondhalf_status = 'COF';
						$absent_status = '0';
						$present_status = '0';
					}
					$this->db->query("UPDATE " . DB_PREFIX . "transaction SET `firsthalf_status` = '".$firsthalf_status."', `secondhalf_status` = '".$secondhalf_status."', `leave_status` = '0', `absent_status` = '".$absent_status."', `present_status` = '".$present_status."' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'"); 
					$this->log->write("UPDATE " . DB_PREFIX . "transaction SET `firsthalf_status` = '".$firsthalf_status."', `secondhalf_status` = '".$secondhalf_status."', `leave_status` = '0', `absent_status` = '".$absent_status."', `present_status` = '".$present_status."' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'"); 
					//echo "UPDATE " . DB_PREFIX . "transaction SET `firsthalf_status` = '".$firsthalf_status."', `secondhalf_status` = '".$secondhalf_status."', `leave_status` = '0', `absent_status` = '".$absent_status."' WHERE `date` = '".$date."' AND `emp_id` = '".$emp_id."'";
					//echo '<br />';
					//exit;
				}
			}
			//echo 'out';
			//exit;

			$this->session->data['success'] = 'Leave Reverted Successfully';

			$url = '';
			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['filter_approval_1'])) {
				$url .= '&filter_approval_1=' . $this->request->get['filter_approval_1'];
			}

			if (isset($this->request->get['filter_name_id_1'])) {
				$url .= '&filter_name_id_1=' . $this->request->get['filter_name_id_1'];
			}

			if (isset($this->request->get['filter_batch'])) {
				$url .= '&filter_batch=' . $this->request->get['filter_batch'];
			}

			if (isset($this->request->get['requestform'])) {
				$url .= '&requestform=' . $this->request->get['requestform'];
			}

			if (isset($this->request->get['filter_dept'])) {
				$url .= '&filter_dept=' . $this->request->get['filter_dept'];
			}

			if (isset($this->request->get['filter_unit'])) {
				$url .= '&filter_unit=' . $this->request->get['filter_unit'];
			}


			$this->redirect($this->url->link('transaction/leave', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	public function active() {
		$this->language->load('transaction/leave');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('transaction/transaction');
		$this->load->model('catalog/shift');
		$this->load->model('catalog/employee');

		if(isset($this->request->get['batch_id']) && $this->validateDelete()){
			$batch_id = $this->request->get['batch_id'];
			$leav_data_sql = "SELECT * FROM `oc_leave_transaction` WHERE `batch_id` = '".$batch_id."' GROUP BY `batch_id` ";
			//echo $leav_data_sql;exit;
			$leav_data = $this->db->query($leav_data_sql)->row;
			if(isset($leav_data['encash']) && $leav_data['encash'] != ''){
				$encah_leave = $leav_data['encash'];
				$sql = "SELECT * FROM `oc_leave` WHERE `emp_id` = '".$emp_id."' AND `close_status` = '0' ";
				$query1 = $this->db->query($sql);
				if($query1->num_rows > 0){
					if($query->row['leave_type'] == 'PL'){
						$balance = $query1->row['pl_bal'];
						$total_bal = $this->model_transaction_transaction->gettotal_bal($leav_data['emp_id']);
						$total_bal_pro = $this->model_transaction_transaction->gettotal_bal_pro($leav_data['emp_id'], 'PL');
						$total_bal_pro1 = $this->model_transaction_transaction->gettotal_bal_pro1($leav_data['emp_id'], 'PL');
						$total_bal_pl = 0;
						if(isset($total_bal['pl_acc'])){
							if(isset($total_bal_pro['bal_p'])){
								$total_bal_pl = $total_bal['pl_acc'] - ($total_bal_pro['bal_p'] + $total_bal_pro1['leave_amount']);
							} else {
								$total_bal_pl = $total_bal['pl_acc'];
							}
						}
						$balance = $total_bal_pl - $leav_data['encash'];
						$upd_bal_sql = "UPDATE `oc_leave` SET `pl_bal` = '".$balance."' WHERE `emp_id` = '".$query1->row['emp_id']."' AND `close_status` = '0' ";
						$this->db->query($upd_bal_sql);
						$this->log->write($upd_bal_sql);
					} 
				}
			}
			$can_leave_sql = "UPDATE `oc_leave_transaction` SET `a_status` = '1' WHERE `batch_id` = '".$this->request->get['batch_id']."' ";
			$this->db->query($can_leave_sql);

			$can_leave_sql = "UPDATE `oc_leave_transaction_temp` SET `a_status` = '1' WHERE `batch_id` = '".$leav_data['linked_batch_id']."' ";
			$this->db->query($can_leave_sql);

			$this->session->data['success'] = 'Leave Activated Successfully';

			$url = '';
			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['filter_name_id_1'])) {
				$url .= '&filter_name_id_1=' . $this->request->get['filter_name_id_1'];
			}

			if (isset($this->request->get['filter_unit'])) {
				$url .= '&filter_unit=' . $this->request->get['filter_unit'];
			}

			if (isset($this->request->get['leaveprocess'])) {
				$this->redirect($this->url->link('transaction/leaveprocess', 'token=' . $this->session->data['token'] . $url, 'SSL'));
			} else {
				$this->redirect($this->url->link('transaction/leave', 'token=' . $this->session->data['token'] . $url, 'SSL'));
			}
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['filter_name_id_1'])) {
			$filter_name_id_1 = $this->request->get['filter_name_id_1'];
		} else {
			$filter_name_id_1 = '';
		}

		if (isset($this->request->get['filter_date'])) {
			if (!isset($this->request->get['requestform'])) {
				$filter_date = $this->request->get['filter_date'];
			} else {
				$filter_date = '';
			}
		} else {
			$filter_date = '';
		}

		if (isset($this->request->get['filter_unit'])) {
			$filter_unit = $this->request->get['filter_unit'];
		} else {
			$filter_unit = '';
		}

		if (isset($this->request->get['filter_batch'])) {
			$filter_batch = $this->request->get['filter_batch'];
		} else {
			$filter_batch = '';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_approval_1'])) {
			$url .= '&filter_approval_1=' . $this->request->get['filter_approval_1'];
		}

		if (isset($this->request->get['filter_name_id_1'])) {
			$url .= '&filter_name_id_1=' . $this->request->get['filter_name_id_1'];
		}

		if (isset($this->request->get['filter_date'])) {
			if (!isset($this->request->get['requestform'])) {
				$url .= '&filter_date=' . $this->request->get['filter_date'];
			}
		}

		if (isset($this->request->get['filter_dept'])) {
			$url .= '&filter_dept=' . $this->request->get['filter_dept'];
		}

		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}

		if (isset($this->request->get['requestform'])) {
			$url .= '&requestform=' . $this->request->get['requestform'];
		}

		if (isset($this->request->get['filter_batch'])) {
			$url .= '&filter_batch=' . $this->request->get['filter_batch'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('transaction/leave', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->data['insert'] = $this->url->link('transaction/leave/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('transaction/leave/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['export'] = $this->url->link('transaction/leave/export', 'token=' . $this->session->data['token'] . $url, 'SSL');
		

		if (isset($this->request->get['requestform'])) {
			$this->data['request_list'] = $this->url->link('transaction/requestformunit', 'token=' . $this->session->data['token'].$url, 'SSL');
		} else {
			$this->data['request_list'] = '';
		}


		$this->data['leaves'] = array();
		$employee_total = 0;

		$data = array(
			'filter_name' => $filter_name,
			'filter_name_id' => $filter_name_id,
			'filter_name_id_1' => $filter_name_id_1,
			'filter_date' => $filter_date,
			'filter_unit' => $filter_unit,
			'filter_batch' => $filter_batch,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);

		//if($data['filter_name_id'] != ''){
			$employee_total = $this->model_transaction_transaction->getTotalleaveetransaction($data);
			$results = $this->model_transaction_transaction->getleavetransaction($data);
			foreach ($results as $result) {
				$action = array();

				// $action[] = array(
				// 	'text' => $this->language->get('text_edit'),
				// 	'href' => $this->url->link('transaction/leave/update', 'token=' . $this->session->data['token'] . '&batch_id=' . $result['batch_id'] . $url, 'SSL')
				// );

				$leave_sql = "SELECT `id` FROM `oc_leave_transaction` WHERE `batch_id` = '".$result['batch_id']."' AND `p_status` = '1' ";
				$date_res = $this->db->query($leave_sql);
				if ($date_res->num_rows == 0) {
					$leave_sql = "SELECT `id` FROM `oc_leave_transaction` WHERE `batch_id` = '".$result['batch_id']."' AND `a_status` = '1' ";
					$date_res = $this->db->query($leave_sql);
					if($date_res->num_rows > 0){
						//$leave_sql_1 = "SELECT `id` FROM `oc_leave_transaction` WHERE `batch_id` = '".$result['batch_id']."' AND `p_status` = '1' ";
						//$date_res_1 = $this->db->query($leave_sql_1);
						//if($date_res_1->num_rows > 0){
							//$action[] = array(
								//'text' => 'Revert',
								//'href' => $this->url->link('transaction/leave/revert', 'token=' . $this->session->data['token'] . '&batch_id=' . $result['batch_id'] . $url, 'SSL')
							//);
						//} else {
							if($result['encash'] == ''){
								$action[] = array(
									'text' => 'Delete',
									'href' => $this->url->link('transaction/leave/delete', 'token=' . $this->session->data['token'] . '&batch_id=' . $result['batch_id'] . $url, 'SSL')
								);
							} elseif($this->user->getId() == 1){
								$action[] = array(
									'text' => 'Delete',
									'href' => $this->url->link('transaction/leave/delete', 'token=' . $this->session->data['token'] . '&batch_id=' . $result['batch_id'] . $url, 'SSL')
								);
							}
						//}
					} else {
						$action[] = array(
							'text' => 'Activate',
							'href' => $this->url->link('transaction/leave/active', 'token=' . $this->session->data['token'] . '&batch_id=' . $result['batch_id'] . $url, 'SSL')
						);
					}
				} else {
					$tran_data_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$result['emp_id']."' AND `date` = '".$result['date']."' AND `month_close_status` = '1' ";
					$tran_datas = $this->db->query($tran_data_sql);
					if ($tran_datas->num_rows == 0) {	
						if($result['encash'] == ''){
							$action[] = array(
								'text' => 'Revert',
								'href' => $this->url->link('transaction/leave/revert', 'token=' . $this->session->data['token'] . '&batch_id=' . $result['batch_id'] . $url, 'SSL')
							);
						} elseif($this->user->getId() == 1){
							$action[] = array(
								'text' => 'Revert',
								'href' => $this->url->link('transaction/leave/revert', 'token=' . $this->session->data['token'] . '&batch_id=' . $result['batch_id'] . $url, 'SSL')
							);						
						}
					} 
				}

				$total_leave_dayss = $this->model_transaction_transaction->gettotal_leave_days($result['batch_id']);
				if($total_leave_dayss['days'] == ''){
					$total_leave_days = $total_leave_dayss['leave_amount'];
				} else {
					$total_leave_days = $total_leave_dayss['days'];
				}
				$leave_from = $this->model_transaction_transaction->getleave_from($result['batch_id']);
				$leave_to = $this->model_transaction_transaction->getleave_to($result['batch_id']);

				$emp_data = $this->model_transaction_transaction->getempdata($result['emp_id']);

				if($result['a_status'] == 1){
					$status = 'Active';
				} else {
					$status = 'InActive';
				}

				if($result['p_status'] == 1){
					$p_status = 'Processed';
				} else {
					$p_status = 'UnProcessed';
				}

				if($result['leave_type'] == 'COF'){
					$leave_type = $result['leave_type'].' / '.$result['date_cof'];
				} else {
					$leave_type = $result['leave_type'];
				}

				$this->data['leaves'][] = array(
					'batch_id' => $result['batch_id'],
					'dot' => $result['dot'],
					'encash' => $result['encash'],
					'id' => $result['id'],
					'emp_id' => $result['emp_id'],
					'name' => $emp_data['name'],
					'unit' => $emp_data['unit'],
					'leave_type'        => $leave_type,
					'total_leave_days' 	      => $total_leave_days,
					'leave_from' => $leave_from,
					'leave_to' => $leave_to,
					'status' => $status,
					'p_status' => $p_status,
					'selected'        => isset($this->request->post['selected']) && in_array($result['id'], $this->request->post['selected']),
					'action'          => $action
				);
			}
		//}

		// echo '<pre>';
		// print_r($this->data['leaves']);
		// exit;

		$unit_data = array(
			'0' => 'All',
			'Mumbai' => 'Mumbai',
			'Pune' => 'Pune',
			'Delhi' => 'Delhi',
			'Chennai' => 'Chennai',
			'Bangalore' => 'Bangalore',
			'Ahmedabad' => 'Ahmedabad'
		);

		$this->data['unit_data'] = $unit_data;

		$this->data['token'] = $this->session->data['token'];	

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_delete'] = $this->language->get('text_delete');

		$this->data['button_insert'] = $this->language->get('button_insert');

		$this->data['button_filter'] = $this->language->get('button_filter');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		// if (isset($this->request->get['page'])) {
		// 	$url .= '&page=' . $this->request->get['page'];
		// }

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_name_id_1'])) {
			$url .= '&filter_name_id_1=' . $this->request->get['filter_name_id_1'];
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . $this->request->get['filter_date'];
		}

		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}

		if (isset($this->request->get['filter_batch'])) {
			$url .= '&filter_batch=' . $this->request->get['filter_batch'];
		}

		$pagination = new Pagination();
		$pagination->total = $employee_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('transaction/leave', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		
		$this->data['filter_name'] = $filter_name;
		$this->data['filter_name_id'] = $filter_name_id;
		$this->data['filter_name_id_1'] = $filter_name_id_1;
		$this->data['filter_date'] = $filter_date;
		$this->data['filter_unit'] = $filter_unit;
		$this->data['filter_batch'] = $filter_batch;
		
		$this->template = 'transaction/leave_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	public function getForm() {
		$this->language->load('transaction/leave');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('transaction/transaction');
		$this->load->model('catalog/shift');
		$this->load->model('catalog/employee');

		$this->data['heading_title'] = $this->language->get('heading_title');

		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		$this->data['text_no_results'] = $this->language->get('text_no_results');

		$this->data['tab_general'] = $this->language->get('tab_general');

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {	
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['encash'])) {
			$this->data['error_encash'] = $this->error['encash'];
		} else {
			$this->data['error_encash'] = '';
		}

		if (isset($this->error['days'])) {
			$this->data['error_days'] = $this->error['days'];
		} else {
			$this->data['error_days'] = '';
		}

		if (isset($this->error['leave_type'])) {
			$this->data['error_leave_type'] = $this->error['leave_type'];
		} else {
			$this->data['error_leave_type'] = '';
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('transaction/leave', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		
		$url = '';
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_name_id_1'])) {
			$url .= '&filter_name_id_1=' . $this->request->get['filter_name_id_1'];
		}

		if(isset($this->request->get['leaveprocess']) && $this->request->get['leaveprocess'] == '1'){
			$this->data['cancel'] = $this->url->link('transaction/leaveprocess', 'token=' . $this->session->data['token'].'&unit='.$this->request->get['unit'], 'SSL');
			$show = '0';
			$this->data['action'] = $this->url->link('transaction/leave/insert_1', 'token=' . $this->session->data['token'].'&unit='.$this->request->get['unit'].'&batch_id='.$this->request->get['batch_id'].'&leaveprocess=1', 'SSL');
			$this->data['action_1'] = $this->url->link('transaction/leave/active', 'token=' . $this->session->data['token'].'&unit='.$this->request->get['unit'].'&batch_id='.$this->request->get['batch_id'].'&leaveprocess=1', 'SSL');
		} elseif(isset($this->request->get['leaveprocess']) && $this->request->get['leaveprocess'] == '2'){
			$this->data['cancel'] = $this->url->link('transaction/leave_ess_dept', 'token=' . $this->session->data['token'], 'SSL');
			$show = '0';
			$this->data['action'] = $this->url->link('transaction/leave_ess_dept/cancel', 'token=' . $this->session->data['token'].'&batch_id='.$this->request->get['batch_id'].'&leaveprocess=2', 'SSL');
			$this->data['action_1'] = $this->url->link('transaction/leave_ess_dept/approve', 'token=' . $this->session->data['token'].'&batch_id='.$this->request->get['batch_id'].'&leaveprocess=2', 'SSL');
		} else {
			$this->data['cancel'] = $this->url->link('transaction/leave', 'token=' . $this->session->data['token'].$url, 'SSL');
			$show = '1';
			$this->data['action'] = $this->url->link('transaction/leave/insert', 'token=' . $this->session->data['token'].$url, 'SSL');
			$this->data['action_1'] = '';
		}

		if(isset($this->request->post['show'])){
			$this->data['show'] = $this->request->post['show'];
		} else {
			$this->data['show'] = $show;
		}
		

		
		$transaction_data = array();
		$emp_data = array();
		$this->data['edit_status'] = 0;
		if (isset($this->request->get['batch_id']) && isset($this->request->get['leaveprocess']) && $this->request->get['leaveprocess'] == '1' && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$this->data['edit_status'] = 1;
			$transaction_data = $this->model_transaction_transaction->getleave_transaction_data($this->request->get['batch_id']);
			$emp_data = $this->model_transaction_transaction->getEmployees_dat($this->request->get['filter_name_id']);
		} elseif (isset($this->request->get['batch_id']) && isset($this->request->get['leaveprocess']) && $this->request->get['leaveprocess'] == '2' && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$this->data['edit_status'] = 1;
			$transaction_data = $this->model_transaction_transaction->getleave_transaction_data_ess($this->request->get['batch_id']);
			$emp_data = $this->model_transaction_transaction->getEmployees_dat($this->request->get['filter_name_id']);
		} elseif(isset($this->request->get['filter_name_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')){
			$this->data['edit_status'] = 0;
			$emp_data = $this->model_transaction_transaction->getEmployees_dat($this->request->get['filter_name_id']);
		} elseif(isset($this->request->get['filter_name_id_1']) && ($this->request->server['REQUEST_METHOD'] != 'POST')){
			$this->data['edit_status'] = 0;
			$emp_data = $this->model_transaction_transaction->getEmployees_dat($this->request->get['filter_name_id_1']);
		}

		$this->data['token'] = $this->session->data['token'];

		$leaves = array(
			'0' => array(
				'leave_id' => 'PL',
				'leave_name' => 'PL'
			),
			'1' => array(
				'leave_id' => 'BL',
				'leave_name' => 'BL'
			),
			'2' => array(
				'leave_id' => 'SL',
				'leave_name' => 'SL'
			),
			'3' => array(
				'leave_id' => 'ML',
				'leave_name' => 'ML'
			),
			//'4' => array(
				//'leave_id' => 'PLE',
				//'leave_name' => 'PLE'
			//),
			'5' => array(
				'leave_id' => 'MAL',
				'leave_name' => 'MAL'
			),
			'6' => array(
				'leave_id' => 'PAL',
				'leave_name' => 'PAL'
			),
			'7' => array(
				'leave_id' => 'OD',
				'leave_name' => 'OD'
			),
			'8' => array(
				'leave_id' => 'LWP',
				'leave_name' => 'LWP'
			),
		);
		
		$this->data['leaves'] = $leaves;

		if (isset($this->request->post['insert'])) {
			$this->data['insert'] = $this->request->post['insert'];
		} elseif(isset($transaction_data[0]['transaction_id'])) {
			$this->data['insert'] = 0;
		} else {
			$this->data['insert'] = '';
		}

		if (isset($this->request->post['batch_id'])) {
			$this->data['batch_id'] = $this->request->post['batch_id'];
		} elseif(isset($transaction_data[0]['batch_id'])) {
			$this->data['batch_id'] = $transaction_data[0]['batch_id'];
		} else {	
			$this->data['batch_id'] = 0;
		}

		if (isset($this->request->post['leave_reason'])) {
			$this->data['leave_reason'] = $this->request->post['leave_reason'];
		} elseif (isset($transaction_data[0]['leave_reason'])) {
			$this->data['leave_reason'] = $transaction_data[0]['leave_reason'];
		} else {	
			$this->data['leave_reason'] = '';//date('d-m-Y');
		}

		if (isset($this->request->post['reject_reason'])) {
			$this->data['reject_reason'] = $this->request->post['reject_reason'];
		} else {	
			$this->data['reject_reason'] = '';//date('d-m-Y');
		}

		if (isset($this->request->post['leave_type'])) {
			$this->data['leave_type'] = $this->request->post['leave_type'];
		} elseif(isset($transaction_data[0]['leave_type'])) {
			$this->data['leave_type'] = $transaction_data[0]['leave_type'];
		} else {	
			$this->data['leave_type'] = 'PL';
		}

		if (isset($this->request->post['from'])) {
			$this->data['from'] = $this->request->post['from'];
		} elseif(isset($transaction_data[0]['date'])) {
			$this->data['from'] = $transaction_data[0]['date'];
		} elseif (isset($this->request->get['from'])) {
			$this->data['from'] = $this->request->get['from'];
		} else {	
			$this->data['from'] = date('Y-m-d');
		}

		$t_transaction_data = $transaction_data;
		end($t_transaction_data);
		$lkey = key($t_transaction_data);

		// echo '<pre>';
		// print_r($lkey);
		// echo '<pre>';
		// print_r($transaction_data);
		// exit;

		if (isset($this->request->post['to'])) {
			$this->data['to'] = $this->request->post['to'];
		} elseif(isset($transaction_data[$lkey]['date'])) {
			$this->data['to'] = $transaction_data[$lkey]['date'];
		} else {	
			$this->data['to'] = date('Y-m-d');
		}

		if (isset($this->request->post['leave_amount'])) {
			$this->data['leave_amount'] = $this->request->post['leave_amount'];
		} elseif (isset($transaction_data[0]['leave_amount'])) {
			$this->data['leave_amount'] = $transaction_data[0]['leave_amount'];
		} else {	
			$this->data['leave_amount'] = '';//date('d-m-Y');
		}

		if (isset($this->request->post['type'])) {
			$this->data['type'] = $this->request->post['type'];
		} elseif (isset($transaction_data[0]['type'])) {
			$this->data['type'] = $transaction_data[0]['type'];
		} else {	
			$this->data['type'] = 'F';//date('d-m-Y');
		}

		if (isset($this->request->post['days'])) {
			$this->data['days'] = $this->request->post['days'];
		} elseif (isset($transaction_data[0]['days'])) {
			$this->data['days'] = $transaction_data[0]['days'];
		} else {	
			$this->data['days'] = '';//date('d-m-Y');
		}

		if (isset($this->request->post['encash'])) {
			$this->data['encash'] = $this->request->post['encash'];
		} elseif (isset($transaction_data[0]['encash'])) {
			$this->data['encash'] = $transaction_data[0]['encash'];
		} else {	
			$this->data['encash'] = '';//date('d-m-Y');
		}

		if (isset($this->request->post['e_name'])) {
			$this->data['e_name'] = $this->request->post['e_name'];
			$this->data['e_name_id'] = $this->request->post['e_name_id'];
			$this->data['emp_code'] = $this->request->post['e_name_id'];
			$this->data['group'] = $this->request->post['group'];
		} 
		// elseif (isset($transaction_data[0]['emp_id'])) {
		// 	$this->data['e_name'] = $transaction_data[0]['emp_name'];
		// 	$this->data['e_name_id'] = $transaction_data[0]['emp_id'];
		// 	$this->data['emp_code'] = $transaction_data[0]['emp_id'];
		// } 
		elseif (isset($emp_data['emp_code'])) {
			$this->data['e_name'] = $emp_data['name'];
			$this->data['e_name_id'] = $emp_data['emp_code'];
			$this->data['emp_code'] = $emp_data['emp_code'];
			$this->data['group'] = $emp_data['group'];
		} else {	
			$this->data['e_name'] = '';
			$this->data['e_name_id'] = '';
			$this->data['emp_code'] = '';
			$this->data['group'] = '';
		}

		if (isset($this->request->post['a_status'])) {
			$this->data['a_status'] = $this->request->post['a_status'];
		} elseif (isset($transaction_data[0]['a_status'])) {
			$this->data['a_status'] = $transaction_data[0]['a_status'];
		} else {	
			$this->data['a_status'] = 1;//date('d-m-Y');
		}

		if (isset($this->request->post['date_cof'])) {
			$this->data['date_cof'] = $this->request->post['date_cof'];
		} elseif (isset($transaction_data[0]['a_status'])) {
			$this->data['date_cof'] = $transaction_data[0]['date_cof'];
		} else {	
			$this->data['date_cof'] = '';//date('d-m-Y');
		}

		if (isset($this->request->post['dot'])) {
			$this->data['dot'] = $this->request->post['dot'];
		} elseif (isset($transaction_data[0]['dot'])) {
			$this->data['dot'] = $transaction_data[0]['dot'];
		} else {	
			$this->data['dot'] = '';//date('d-m-Y');
		}

		if (isset($this->request->post['diffDays'])) {
			$this->data['diffDays'] = $this->request->post['diffDays'];
		} else {	
			$this->data['diffDays'] = 1;
		}

		if (isset($this->request->post['encash_done_year'])) {
			$this->data['encash_done_year'] = $this->request->post['encash_done_year'];
		} elseif(isset($emp_data['emp_code'])) {
			$emp_code = $emp_data['emp_code'];
			$is_exist = $this->model_transaction_transaction->getpltakenyear($emp_code);
			if(isset($is_exist['id'])){
				$this->data['encash_done_year'] = 1;
			} else {
				$this->data['encash_done_year'] = 0;
			}
		} else {	
			$this->data['encash_done_year'] = 0;
		}

		$statuses = array(
			'0' => 'InActive',
			'1' => 'Active'
		);
		$this->data['statuses'] = $statuses;

		$this->data['multi_day'] = 0;
		if(isset($this->request->post['multi_day'])){
			$this->data['multi_day'] = $this->request->post['multi_day'];
		} elseif(isset($this->request->get['leaveprocess']) && $this->request->get['leaveprocess'] == '1') {	
			$total_leave_dayss = $this->model_transaction_transaction->gettotal_leave_days($this->data['batch_id']);
			if(isset($total_leave_dayss['days'])){
				if($total_leave_dayss['days'] == ''){
					$total_leave_days = $total_leave_dayss['leave_amount'];
				} else {
					$total_leave_days = $total_leave_dayss['days'];
				}
			} else {
				$total_leave_days = 0;
			}
			if($total_leave_days > 1){
				$this->data['multi_day'] = 1;
			} else {
				$this->data['multi_day'] = 0;
			}
		} elseif(isset($this->request->get['leaveprocess']) && $this->request->get['leaveprocess'] == '2') {	
			$total_leave_dayss = $this->model_transaction_transaction->gettotal_leave_days_ess($this->data['batch_id']);
			if(isset($total_leave_dayss['days'])){
				if($total_leave_dayss['days'] == ''){
					$total_leave_days = $total_leave_dayss['leave_amount'];
				} else {
					$total_leave_days = $total_leave_dayss['days'];
				}
			} else {
				$total_leave_days = 0;
			}
			if($total_leave_days > 1){
				$this->data['multi_day'] = 1;
			} else {
				$this->data['multi_day'] = 0;
			}
		}

		if(isset($this->request->post['enable_encash'])){
			$this->data['enable_encash'] = $this->request->post['enable_encash'];
		} elseif (isset($transaction_data[0]['encash'])) {
			$encash = $transaction_data[0]['encash'];
			if($encash != '0' && $encash != ''){
				$this->data['enable_encash'] = 1;	
			} else {
				$this->data['enable_encash'] = 0;
			}
		} else { 
			$this->data['enable_encash'] = 0;
		}

		$this->data['total_bal_pl'] = 0;
		if($this->data['emp_code']){
			$total_bal = $this->model_transaction_transaction->gettotal_bal($this->data['emp_code']);
			
			$total_bal_pro = $this->model_transaction_transaction->gettotal_bal_pro($this->data['emp_code'], 'PL');
			$total_bal_pro1 = $this->model_transaction_transaction->gettotal_bal_pro1($this->data['emp_code'], 'PL');
			$this->data['total_bal_pl'] = 0;
			if(isset($total_bal['pl_acc'])){
				if(isset($total_bal_pro['bal_p'])){
					$this->data['total_bal_pl'] = $total_bal['pl_acc'] - ($total_bal_pro['bal_p'] + $total_bal_pro['encash'] + $total_bal_pro1['leave_amount']);
				} else {
					$this->data['total_bal_pl'] = $total_bal['pl_acc'];
				}
			}

			//$total_bal_cl = $this->model_transaction_transaction->gettotal_bal($this->data['emp_code']);
			$total_bal_pro = $this->model_transaction_transaction->gettotal_bal_pro($this->data['emp_code'], 'CL');
			$total_bal_pro1 = $this->model_transaction_transaction->gettotal_bal_pro1($this->data['emp_code'], 'CL');
			$this->data['total_bal_cl'] = 0;
			if(isset($total_bal['cl_acc'])){
				if(isset($total_bal_pro['bal_p'])){
					$this->data['total_bal_cl'] = $total_bal['cl_acc'] - ($total_bal_pro['bal_p'] + $total_bal_pro1['leave_amount']);
				} else {
					$this->data['total_bal_cl'] = $total_bal['cl_acc'];
				}
			}

			//$total_bal_sl = $this->model_transaction_transaction->gettotal_bal($this->data['emp_code']);
			$total_bal_pro = $this->model_transaction_transaction->gettotal_bal_pro($this->data['emp_code'], 'SL');
			$total_bal_pro1 = $this->model_transaction_transaction->gettotal_bal_pro1($this->data['emp_code'], 'SL');
			$this->data['total_bal_sl'] = 0;
			if(isset($total_bal['sl_acc'])){
				if(isset($total_bal_pro['bal_p'])){
					$this->data['total_bal_sl'] = $total_bal['sl_acc'] - ($total_bal_pro['bal_p'] + $total_bal_pro1['leave_amount']);
				} else {
					$this->data['total_bal_sl'] = $total_bal['sl_acc'];
				}
			}

			$total_bal_p = $this->model_transaction_transaction->gettotal_bal_p($this->data['emp_code'], 'PL');
			$total_bal_p1 = $this->model_transaction_transaction->gettotal_bal_p1($this->data['emp_code'], 'PL');
			$this->data['total_bal_pl_p'] = 0;
			if(isset($total_bal_p['bal_p'])){
				if(isset($total_bal['pl_acc'])){
					//$this->data['total_bal_pl_p'] = $total_bal['pl_acc'] - $total_bal_p['bal_p'];
					$this->data['total_bal_pl_p'] = $total_bal_p['bal_p'] + $total_bal_p['encash'] + $total_bal_p1['leave_amount'];
				}
			}

			$total_bal_p = $this->model_transaction_transaction->gettotal_bal_p($this->data['emp_code'], 'CL');
			$total_bal_p1 = $this->model_transaction_transaction->gettotal_bal_p1($this->data['emp_code'], 'CL');
			$this->data['total_bal_cl_p'] = 0;
			if(isset($total_bal_p['bal_p'])){
				if(isset($total_bal['cl_acc'])){
					//$this->data['total_bal_cl_p'] = $total_bal['cl_acc'] - $total_bal_p['bal_p'];
					$this->data['total_bal_cl_p'] = $total_bal_p['bal_p'] + $total_bal_p1['leave_amount'];
				}
			}

			$total_bal_p = $this->model_transaction_transaction->gettotal_bal_p($this->data['emp_code'], 'SL');
			$total_bal_p1 = $this->model_transaction_transaction->gettotal_bal_p1($this->data['emp_code'], 'SL');
			$this->data['total_bal_sl_p'] = 0;
			if(isset($total_bal_p['bal_p'])){
				if(isset($total_bal['sl_acc'])){
					//$this->data['total_bal_sl_p'] = $total_bal['sl_acc'] - $total_bal_p['bal_p'];
					$this->data['total_bal_sl_p'] = $total_bal_p['bal_p'] + $total_bal_p1['leave_amount'];
				}
			}
			// echo '<pre>';
			// print_r($this->data['total_bal_sl']);
			// echo '<pre>';
			// print_r($this->data['total_bal_sl_p']);
			// exit;
		}

		if (isset($this->request->post['approval_1'])) {
			$this->data['approval_1'] = $this->request->post['approval_1'];
		} elseif(isset($this->request->get['leaveprocess']) && $this->request->get['leaveprocess'] == '1') {	
			$leav_temp_data = $this->db->query("SELECT `approval_1`, `approved_1_by` FROM `oc_leave_transaction_temp` WHERE `batch_id` = '".$transaction_data[0]['linked_batch_id']."' ");
			if($leav_temp_data->row['approval_1'] == '1'){
				$approval_1_status = 'Approved';
				$emp_code = $leav_temp_data->row['approved_1_by'];
				if($emp_code != '0'){
					$emp_names = $this->db->query("SELECT `title`, `name` FROM `oc_employee` WHERE `emp_code` = '".$emp_code."'")->row;
					$emp_name = $emp_names['title'].' '.$emp_names['name'];
					$approval_1_status = $approval_1_status.' by ' . $emp_name;
				}
			} else {
				$approval_1_status = 'Pending';
			}
			$this->data['approval_1'] = $approval_1_status;
		} elseif(isset($this->request->get['leaveprocess']) && $this->request->get['leaveprocess'] == '2') {	
			$leav_temp_data = $this->db->query("SELECT `approval_1`, `approved_1_by` FROM `oc_leave_transaction_temp` WHERE `batch_id` = '".$transaction_data[0]['batch_id']."' ");
			if($leav_temp_data->row['approval_1'] == '1'){
				$approval_1_status = 'Approved';
				$emp_code = $leav_temp_data->row['approved_1_by'];
				if($emp_code != '0'){
					$emp_names = $this->db->query("SELECT `title`, `name` FROM `oc_employee` WHERE `emp_code` = '".$emp_code."'")->row;
					$emp_name = $emp_names['title'].' '.$emp_names['name'];
					$approval_1_status = $approval_1_status.' by ' . $emp_name;
				}
			} else {
				$approval_1_status = 'Pending';
			}
			$this->data['approval_1'] = $approval_1_status;
		} else {
			$this->data['approval_1'] = '';
		}

		if (isset($this->request->post['approval_2'])) {
			$this->data['approval_2'] = $this->request->post['approval_2'];
		} elseif(isset($this->request->get['leaveprocess']) && $this->request->get['leaveprocess'] == '1') {	
			$leav_temp_data = $this->db->query("SELECT `approval_2` FROM `oc_leave_transaction_temp` WHERE `batch_id` = '".$transaction_data[0]['linked_batch_id']."' ");
			if($leav_temp_data->row['approval_2'] == '1'){
				$approval_2_status = 'Approved';
			} else {
				$approval_2_status = 'Pending';
			}
			$this->data['approval_2'] = $approval_2_status;
		} elseif(isset($this->request->get['leaveprocess']) && $this->request->get['leaveprocess'] == '2') {	
			$leav_temp_data = $this->db->query("SELECT `approval_2` FROM `oc_leave_transaction_temp` WHERE `batch_id` = '".$transaction_data[0]['batch_id']."' ");
			if($leav_temp_data->row['approval_2'] == '1'){
				$approval_2_status = 'Approved';
			} else {
				$approval_2_status = 'Pending';
			}
			$this->data['approval_2'] = $approval_2_status;
		} else {
			$this->data['approval_2'] = '';
		}

		if (isset($this->request->post['reject_reason_1'])) {
			$this->data['reject_reason_1'] = $this->request->post['reject_reason_1'];
		} elseif (isset($transaction_data[0]['reject_reason_1'])) {
			$this->data['reject_reason_1'] = $transaction_data[0]['reject_reason_1'];
		} else {	
			$this->data['reject_reason_1'] = '';//date('d-m-Y');
		}

		if (isset($this->request->post['reject_reason_2'])) {
			$this->data['reject_reason_2'] = $this->request->post['reject_reason_2'];
		} elseif (isset($transaction_data[0]['reject_reason_2'])) {
			$this->data['reject_reason_2'] = $transaction_data[0]['reject_reason_2'];
		} else {	
			$this->data['reject_reason_2'] = '';//date('d-m-Y');
		}

		
		if($this->request->server['REQUEST_METHOD'] == 'POST') {
			$this->data['post_status'] = 1;
		} else {
			$this->data['post_status'] = 0;
		}

		$types = array(
			'F' => 'Full Day',
			'1' => 'First Half',
			'2' => 'Second Half'
		);
		$this->data['types'] = $types;
		
		// echo '<pre>';
		// print_r($this->data);
		// exit;

		$this->template = 'transaction/leave.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	} 

	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	} 

	protected function validateForm() {
		$this->load->model('transaction/transaction');

		// if (!$this->user->hasPermission('modify', 'transaction/horse_wise')) {
		// 	$this->error['warning'] = $this->language->get('error_permission');
		// }
		$from = $this->request->post['from'];
		$to = $this->request->post['to'];
		$close_sql = "SELECT * FROM `oc_transaction` WHERE `date` = '".$from."' ";
		$close_res = $this->db->query($close_sql);
		if($close_res->num_rows > 0){
			$close_sql = "SELECT * FROM `oc_transaction` WHERE `date` = '".$from."' AND `month_close_status` = '0' ";
			$close_res = $this->db->query($close_sql);
			if($close_res->num_rows == 0){
				$this->error['warning'] = 'Month Already Closed';
			}
		}
		//echo '<pre>';
		//print_r($this->error);
		//exit;
		//if($this->request->post['diffDays'] > 1){
			if($this->request->post['leave_type'] == 'PL'){
				//$pl_bal_sql = "SELECT `pl_bal` FROM `oc_leave` WHERE emp_id = '".$this->request->post['e_name_id']."' ";
				//$pl_bal_res = $this->db->query($pl_bal_sql);
				
				$total_bal = $this->model_transaction_transaction->gettotal_bal($this->request->post['e_name_id']);
				$total_bal_pro = $this->model_transaction_transaction->gettotal_bal_pro($this->request->post['e_name_id'], 'PL');
				$total_bal_pro1 = $this->model_transaction_transaction->gettotal_bal_pro1($this->request->post['e_name_id'], 'PL');
				$total_bal_pl = 0;
				if(isset($total_bal['pl_acc'])){
					if(isset($total_bal_pro['bal_p'])){
						$total_bal_pl = $total_bal['pl_acc'] - ($total_bal_pro['bal_p'] + $total_bal_pro['encash'] + $total_bal_pro1['leave_amount']);
					} else {
						$total_bal_pl = $total_bal['pl_acc'];
					}
				}
				

				$pl_bal_tran_sql = "SELECT count(*) as cnt FROM `oc_leave_transaction` WHERE emp_id = '".$this->request->post['e_name_id']."' AND `leave_type` = 'PL' AND `p_status` = '0' AND `a_status` = '1' ";
				$pl_bal_tran_res = $this->db->query($pl_bal_tran_sql);
				$pl_bal_f = $total_bal_pl - $pl_bal_tran_res->row['cnt'];
				if($total_bal_pl > 0){
					if($this->request->post['enable_encash'] == 1){ 
						if($pl_bal_f >= 30){
							$pl_balance = $pl_bal_f;
							if($this->request->post['leave_amount'] != '0'){
								$total_added = $this->request->post['leave_amount'];
							} else {
								$total_added = $this->request->post['days'] + $this->request->post['encash'];	
							}
							//$total_pl_added = $this->request->post['days'] + $this->request->post['encash'];
							if($total_added > $pl_balance){
								$this->error['days'] = 'Only ' . $pl_balance . ' days PL leave available For Employee';	
							} else {
								if($this->request->post['enable_encash'] == 1){
									$days = $this->request->post['days'];
									$act_encash = $this->request->post['encash'];
									$exp_encash = $days * 3.29;
									$exp_encash = round($exp_encash);
									if($days < 7 && $this->request->post['group'] != 'OFFICIALS'){
										//if($days < 7){
										//$this->error['encash'] = 'Minimum PL Leave Days Should be 7 days'; 
									} else {
										if($act_encash > $exp_encash){
											$this->error['encash'] = 'Encash Should be less than or equal to ' . $exp_encash . ' days'; 
										} elseif($act_encash < 15){
											$this->error['encash'] = 'Minimum Encash Should be 15 days'; 
										}
									}
								}
							}
						} else {
							$this->error['leave_type'] = 'PL Balance Leave For Employee Less than 30';	
						}
					} else {
						$pl_balance = $pl_bal_f;
						if($this->request->post['leave_amount'] != '0'){
							$total_added = $this->request->post['leave_amount'];
						} else {
							$total_added = $this->request->post['days'];	
						}
						if($total_added > $pl_balance){
							$this->error['days'] = 'Only ' . $pl_balance . ' days PL leave available For Employee';	
						} else {
							$days = $this->request->post['days'];
							if($days < 7 && $this->request->post['group'] != 'OFFICIALS'){
								//$this->error['days'] = 'Minimum PL Leave Days Should be 7 days'; 
							}
						}
					}
				} else {
					//if($this->request->post['enable_encash'] == 1){
						$this->error['leave_type'] = 'PL Balance Leave For Employee Does Not Exist';
					//}
				}
			} elseif($this->request->post['leave_type'] == 'CL'){
				//$bal_sql = "SELECT `cl_bal` FROM `oc_leave` WHERE emp_id = '".$this->request->post['e_name_id']."' ";
				//$bal_res = $this->db->query($bal_sql);

				$from = $this->request->post['from'];
				$to = $this->request->post['to'];

				$date1 = new DateTime($from);
				$date2 = new DateTime($to);

				$diff = $date2->diff($date1)->format("%a");
				$diff_days = $diff + 1;
				if($diff_days > 4){
					$this->error['leave_type'] = 'Max CL Leave Period is 4 days';
				}

				$total_bal = $this->model_transaction_transaction->gettotal_bal($this->request->post['e_name_id']);
				$total_bal_pro = $this->model_transaction_transaction->gettotal_bal_pro($this->request->post['e_name_id'], 'CL');
				$total_bal_pro1 = $this->model_transaction_transaction->gettotal_bal_pro1($this->request->post['e_name_id'], 'CL');
				$total_bal_cl = 0;
				if(isset($total_bal['cl_acc'])){
					if(isset($total_bal_pro['bal_p'])){
						$total_bal_cl = $total_bal['cl_acc'] - ($total_bal_pro['bal_p'] + $total_bal_pro1['leave_amount']);
					} else {
						$total_bal_cl = $total_bal['cl_acc'];
					}
				}

				$bal_tran_sql = "SELECT count(*) as cnt FROM `oc_leave_transaction` WHERE emp_id = '".$this->request->post['e_name_id']."' AND `leave_type` = '".$this->request->post['leave_type']."' AND `p_status` = '0' AND `a_status` = '1' ";
				$bal_tran_res = $this->db->query($bal_tran_sql);
				$bal_f = $total_bal_cl - $bal_tran_res->row['cnt'];
				
				$balance = $bal_f;
				if($this->request->post['leave_amount'] != '0'){
					$total_added = $this->request->post['leave_amount'];
				} else {
					$total_added = $this->request->post['days'];	
				}
				if($total_added > $balance){
					if($balance < 0){
						$balance = 0;
					}
					$this->error['days'] = 'Only ' . $balance . ' days '. $this->request->post['leave_type'] .' leave available For Employee';	
				} 
			} elseif($this->request->post['leave_type'] == 'SL'){
				//$bal_sql = "SELECT `sl_bal` FROM `oc_leave` WHERE emp_id = '".$this->request->post['e_name_id']."' ";
				//$bal_res = $this->db->query($bal_sql);

				$total_bal = $this->model_transaction_transaction->gettotal_bal($this->request->post['e_name_id']);
				$total_bal_pro = $this->model_transaction_transaction->gettotal_bal_pro($this->request->post['e_name_id'], 'SL');
				$total_bal_pro1 = $this->model_transaction_transaction->gettotal_bal_pro1($this->request->post['e_name_id'], 'SL');
				$total_bal_sl = 0;
				if(isset($total_bal['sl_acc'])){
					if(isset($total_bal_pro['bal_p'])){
						$total_bal_sl = $total_bal['sl_acc'] - ($total_bal_pro['bal_p'] + $total_bal_pro1['leave_amount']);
					} else {
						$total_bal_sl = $total_bal['sl_acc'];
					}
				}

				$bal_tran_sql = "SELECT count(*) as cnt FROM `oc_leave_transaction` WHERE emp_id = '".$this->request->post['e_name_id']."' AND `leave_type` = '".$this->request->post['leave_type']."' AND `p_status` = '0' AND `a_status` = '1' ";
				$bal_tran_res = $this->db->query($bal_tran_sql);
				$bal_f = $total_bal_sl - $bal_tran_res->row['cnt'];
				$balance = $bal_f;
				if($this->request->post['leave_amount'] != '0'){
					$total_added = $this->request->post['leave_amount'];
				} else {
					$total_added = $this->request->post['days'];	
				}
				if($total_added > $balance){
					if($balance < 0){
						$balance = 0;
					}
					$this->error['days'] = 'Only ' . $balance . ' days '. $this->request->post['leave_type'] .' leave available For Employee';	
				} 
			} 

			// elseif($this->request->post['leave_type'] == 'COF'){
			// 	//echo 'aaaa';
			// 	//$bal_sql = "SELECT `cof_bal` FROM `oc_leave` WHERE emp_id = '".$this->request->post['e_name_id']."' ";
			// 	//$bal_res = $this->db->query($bal_sql);

			// 	$total_bal = $this->model_transaction_transaction->gettotal_bal($this->request->post['e_name_id']);
			// 	$total_bal_pro = $this->model_transaction_transaction->gettotal_bal_pro($this->request->post['e_name_id'], 'COF');
			// 	$total_bal_cof = 0;
			// 	if(isset($total_bal['cof_acc'])){
			// 		if(isset($total_bal_pro['bal_p'])){
			// 			$total_bal_cof = $total_bal['cof_acc'] - $total_bal_pro['bal_p'];
			// 		} else {
			// 			$total_bal_cof = $total_bal['cof_acc'];
			// 		}
			// 	}
				
			// 	$bal_tran_sql = "SELECT count(*) as cnt FROM `oc_leave_transaction` WHERE emp_id = '".$this->request->post['e_name_id']."' AND `leave_type` = '".$this->request->post['leave_type']."' AND `p_status` = '0' AND `a_status` = '1' ";
			// 	$bal_tran_res = $this->db->query($bal_tran_sql);
			// 	$bal_f = $total_bal_cof - $bal_tran_res->row['cnt'];
				
			// 	$balance = $bal_f;
			// 	$total_added = $this->request->post['days'];
			// 	if($total_added > $balance){
			// 		if($balance < 0){
			// 			$balance = 0;
			// 		}
			// 		$this->error['days'] = 'Only ' . $balance . ' days '. $this->request->post['leave_type'] .' leave available For Employee';	
			// 	} 
			// }

			// echo '<pre>';
			// print_r($this->error);
			// exit;

			$from = $this->request->post['from'];
			$to = $this->request->post['to'];

			$date_sql = "SELECT `date`, `leave_amount`, `type` FROM `oc_leave_transaction` WHERE `date` BETWEEN '".$from."' AND '".$to."' AND `a_status` = '1' AND `emp_id`= '".$this->request->post['e_name_id']."' ";
			$date_res = $this->db->query($date_sql);
			if($date_res->num_rows > 0){
				if($date_res->row['type'] != ''){
					if($date_res->row['leave_amount'] == 0.5 && $this->request->post['leave_amount'] == 0.5){
						if($date_res->row['type'] == $this->request->post['type']){
							$this->error['warning'] = 'Leave For Dates Exist';
						}
					} else {
						$this->error['warning'] = 'Leave For Dates Exist';
					}
				} else {
					$this->error['warning'] = 'Leave For Dates Exist';	
				}
			}
		//}

		$day = array();
		$days = $this->GetDays($from, $to);
        foreach ($days as $dkey => $dvalue) {
        	$dates = explode('-', $dvalue);
        	$day[$dkey]['day'] = $dates[2];
        	$day[$dkey]['date'] = $dvalue;
        }

        foreach($day as $dkey => $dvalue){
        	$is_holidays = $this->db->query("SELECT `holiday_id` FROM `oc_holiday` WHERE `date` = '".$dvalue['date']."' ");
        	if($is_holidays->num_rows > 0){
        		if($is_holidays->num_rows > 0){
	        		$day_date = date('j', strtotime($dvalue['date']));
	        		$day_month = date('n', strtotime($dvalue['date']));
	        		$day_year = date('Y', strtotime($dvalue['date']));
	        		$sql = "SELECT `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$this->request->post['e_name_id']."' AND `month` = '".$day_month."' AND `year` = '".$day_year."' ";
	        		$shift_datas = $this->db->query($sql)->row[$day_date];
	        		$shift_explode = explode('_', $shift_datas);
	        		// echo '<pre>';
	        		// print_r($shift_explode);
	        		// exit;
	        		if($shift_explode[0] == 'H'){
		        		$this->error['warning'] = 'Holiday Exist in Selected Date Period';
		        		break;
	        		}
	        	}
        		//$this->error['warning'] = 'Holiday Exist in Selected Date Period';
        		//break;
        	}
        	if(date('N', strtotime($dvalue['date'])) >= 6){
        		$this->error['warning'] = 'Weekend Exist in Selected Date Period';
        		break;	
        	}
        }

        $from_date = $this->request->post['from'];
		$to_date = $this->request->post['to'];
		$from_year = date('Y', strtotime($from_date));
		$to_year = date('Y', strtotime($to_date));
		if($from_year != $to_year){
			$this->error['warning'] = 'From And To Year should be same';
		}
		
		// echo '<pre>';
		// print_r($this->error);
		// exit;

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	protected function validateRejectForm(){
		$data = $this->request->post;
		if ($data['reject_reason'] == '') {
			$this->error['warning'] = 'Please Enter a Reject Reason';
		}

		// echo '<pre>';
		// print_r($this->error);
		// exit;
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}		
	}

	protected function validateDelete(){
		// $this->load->model('transaction/transaction');
		// if(isset($this->request->post['selected'])){
		// 	foreach ($this->request->post['selected'] as $batch_id) {
		// 		$leave_sql = "SELECT `id` FROM `oc_leave_transaction` WHERE `batch_id` = '".$batch_id."' AND `p_status` = '1' ";
		// 		$date_res = $this->db->query($leave_sql);
		// 		if ($date_res->num_rows > 0) {
		// 			$this->error['warning'] = 'Leave With batch ' . $batch_id . ' already processed';
		// 		}
		// 	}	
		// } elseif(isset($this->request->get['batch_id'])){
		// 	$batch_id = $this->request->get['batch_id'];
		// 	$leave_sql = "SELECT `id` FROM `oc_leave_transaction` WHERE `batch_id` = '".$batch_id."' AND `p_status` = '1' ";
		// 	$date_res = $this->db->query($leave_sql);
		// 	if ($date_res->num_rows > 0) {
		// 		$this->error['warning'] = 'Leave With batch ' . $batch_id . ' already processed';
		// 	}
		// }

		if (!$this->error) {
			return true;
		} else {
			return false;
		}		
	}

	public function getshift_details() {
		$jsondata = array();
		$jsondata['status'] = 0;
		//$this->log->write(print_r($this->request->get,true));
		$shift_data = array();
		if(isset($this->request->get['emp_code'])){
			$jsondata['status'] = 1;
			$emp_code = $this->request->get['emp_code'];
			$from = $this->request->get['from'];
			$to = $this->request->get['to'];
			if(!isset($this->request->get['cof_stat'])){
				$from = date('Y-m-d', strtotime($from . ' -1 day'));
				$to = date('Y-m-d', strtotime($to . ' +1 day'));
			}
			$data = array(
				'filter_date_start' => $from,
				'filter_date_end' => $to
			);
	        $this->load->model('transaction/transaction');
			$trans_datas = $this->model_transaction_transaction->gettransaction_data_leave($emp_code, $data);
			$html = '';
			$html .= "<table border='1' style='margin:0px auto;'>";
				$html .= "<tr>";
					$tot = count($trans_datas) + 1;
					$html .= "<td style='padding:10px;text-align:center;font-weight:bold;' colspan = '".$tot."'>";
						$html .= "Attendance Status";
					$html .= "</td>";
				$html .= "</tr>";
				$html .= "<tr>";
					$html .= "<td style='padding:2px;'>";
						$html .= " ";
					$html .= "</td>";
					foreach($trans_datas as $tkey => $tvalue){
						$html .= "<td style='padding:2px;font-weight:bold;font-size:9px;'>";
							$html .= $tvalue['day'];
						$html .= "</td>";
					}
				$html .= "</tr>";
				$html .= "<tr>";
					$html .= "<td style='font-weight:bold;padding:2px;'>";
						$html .= "Act In Time";
					$html .= "</td>";
					foreach($trans_datas as $tkey => $tvalue){
						$html .= "<td style='padding:2px;font-size:9px;'>";
							if($tvalue['act_intime'] != '00:00:00' && $tvalue['act_intime'] != ''){
								$html .= $tvalue['act_intime'];
							} else {
								$html .= '00:00';
							}
						$html .= "</td>";
					}
				$html .= "</tr>";
				$html .= "<tr>";
					$html .= "<td style='font-weight:bold;padding:2px;'>";
						$html .= "Act Out Time";
					$html .= "</td>";
					foreach($trans_datas as $tkey => $tvalue){
						$html .= "<td style='padding:2px;font-size:9px;'>";
							if($tvalue['act_outtime'] != '00:00:00' && $tvalue['act_outtime'] != ''){
								$html .= $tvalue['act_outtime'];
							} else {
								$html .= '00:00';
							}
						$html .= "</td>";
					}
				$html .= "</tr>";
				$html .= "<tr>";
					$html .= "<td style='font-weight:bold;padding:2px;'>";
						$html .= "FH";
					$html .= "</td>";
					foreach($trans_datas as $tkey => $tvalue){
						$html .= "<td style='padding:2px;font-size:9px;'>";
							if($tvalue['firsthalf_status'] == '1'){
								$html .= 'P';	
							} elseif($tvalue['firsthalf_status'] == '0'){
								$html .= 'A';
							} else {
								$html .= $tvalue['firsthalf_status'];	
							}
						$html .= "</td>";
					}
				$html .= "</tr>";
				$html .= "<tr>";
					$html .= "<td style='font-weight:bold;padding:2px;'>";
						$html .= "SH";
					$html .= "</td>";
					foreach($trans_datas as $tkey => $tvalue){
						$html .= "<td style='padding:2px;font-size:9px;'>";
							if($tvalue['secondhalf_status'] == '1'){
								$html .= 'P';	
							} elseif($tvalue['secondhalf_status'] == '0'){
								$html .= 'A';
							} else {
								$html .= $tvalue['secondhalf_status'];	
							}
						$html .= "</td>";
					}
				$html .= "</tr>";
			$html .= "</table>";
			if(!isset($this->request->get['cof_stat'])){
				$close_sql = "SELECT * FROM `oc_transaction` WHERE `date` = '".$this->request->get['from']."' ";
				//$this->log->write(print_r($close_sql,true));				
				$close_res = $this->db->query($close_sql);
				//$this->log->write(print_r($close_res,true));
				if($close_res->num_rows > 0){
					$close_sql = "SELECT * FROM `oc_transaction` WHERE `date` = '".$this->request->get['from']."' AND `month_close_status` = '0' ";					
					//$this->log->write(print_r($close_sql,true));
					$close_res = $this->db->query($close_sql);
					//$this->log->write(print_r($close_res->num_rows,true));					
					if($close_res->num_rows == 0){
						$jsondata['status'] = 2;
						$jsondata['message'] = 'Month Already Closed';
					}
				}
			}
		}
		$jsondata['html'] = $html;
		//$this->log->write($html);
	    $this->response->setOutput(Json_encode($jsondata));
	}

	public function export(){
		$this->load->model('transaction/transaction');
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['filter_name_id_1'])) {
			$filter_name_id_1 = $this->request->get['filter_name_id_1'];
		} else {
			$filter_name_id_1 = '';
		}

		if (isset($this->request->get['filter_date'])) {
			$filter_date = $this->request->get['filter_date'];
		} else {
			$filter_date = '';
		}

		if (isset($this->request->get['filter_unit'])) {
			$filter_unit = $this->request->get['filter_unit'];
		} else {
			$filter_unit = '';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_name_id_1'])) {
			$url .= '&filter_name_id_1=' . $this->request->get['filter_name_id_1'];
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . $this->request->get['filter_date'];
		}

		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data = array(
			'filter_name' => $filter_name,
			'filter_name_id' => $filter_name_id,
			'filter_name_id_1' => $filter_name_id_1,
			'filter_date' => $filter_date,
			'filter_unit' => $filter_unit,
		);

		$results = $this->model_transaction_transaction->getleavetransaction($data);
		$leaves = array();
		foreach ($results as $result) {
			$action = array();
			$total_leave_dayss = $this->model_transaction_transaction->gettotal_leave_days($result['batch_id']);
			if($total_leave_dayss['days'] == ''){
				$total_leave_days = $total_leave_dayss['leave_amount'];
			} else {
				$total_leave_days = $total_leave_dayss['days'];
			}
			$leave_from = $this->model_transaction_transaction->getleave_from($result['batch_id']);
			$leave_to = $this->model_transaction_transaction->getleave_to($result['batch_id']);

			$emp_data = $this->model_transaction_transaction->getempdata($result['emp_id']);

			if($result['a_status'] == 1){
				$status = 'Active';
			} else {
				$status = 'InActive';
			}

			if($result['p_status'] == 1){
				$p_status = 'Processed';
			} else {
				$p_status = 'UNProcessed';
			}

			if($result['leave_type'] == 'COF'){
				$leave_type = $result['leave_type'].' / '.$result['date_cof'];
			} else {
				$leave_type = $result['leave_type'];
			}

			$leaves[] = array(
				'batch_id' => $result['batch_id'],
				'dot' => $result['dot'],
				'encash' => $result['encash'],
				'id' => $result['id'],
				'emp_id' => $result['emp_id'],
				'name' => $emp_data['name'],
				'unit' => $emp_data['unit'],
				'leave_type'        => $leave_type,
				'total_leave_days' 	      => $total_leave_days,
				'leave_from' => $leave_from,
				'leave_to' => $leave_to,
				'status' => $status,
				'p_status' => $p_status,
			);
		}


		if($leaves){
			//$month = date("F", mktime(0, 0, 0, $filter_month, 10));
			$template = new Template();		
			$template->data['final_datas'] = $leaves;
			$template->data['title'] = 'Leave List';
			if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
				$template->data['base'] = HTTPS_SERVER;
			} else {
				$template->data['base'] = HTTP_SERVER;
			}
			$html = $template->fetch('transaction/leave_html.tpl');
			//echo $html;exit;
			$filename = "Leave_List.html";
			header('Content-type: text/html');
			header('Content-Disposition: attachment; filename='.$filename);
			echo $html;
			exit;		
		} else {
			$this->session->data['warning'] = 'No Data Found';
			$this->redirect($this->url->link('transaction/leave', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}
	}
}
?>
