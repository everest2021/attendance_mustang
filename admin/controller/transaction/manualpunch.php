<?php    
class ControllerTransactionManualpunch extends Controller { 
	private $error = array();

	public function index() {
		$this->language->load('transaction/manualpunch');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('transaction/transaction');
		$this->load->model('catalog/shift');
		$this->load->model('catalog/employee');

		$this->getForm();
	}

	public function update() {
		$this->language->load('transaction/manualpunch');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('transaction/transaction');
		$this->load->model('catalog/shift');
		$this->load->model('catalog/employee');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			if($this->request->post['actual_intime'] == '' || $this->request->post['actual_intime'] == '0') {
				$this->request->post['actual_intime'] = '00:00:00';
			}
			if($this->request->post['actual_outtime'] == '' || $this->request->post['actual_outtime'] == '0') {
				$this->request->post['actual_outtime'] = '00:00:00';
			}
			if($this->request->post['dot'] != ''){
				$this->request->post['dot'] = date('Y-m-d', strtotime($this->request->post['dot']));
				$this->request->post['day'] = date('j', strtotime($this->request->post['dot']));
				$this->request->post['month'] = date('n', strtotime($this->request->post['dot']));
				$this->request->post['year'] = date('Y', strtotime($this->request->post['dot']));
			} else {
				$this->request->post['dot'] = date('Y-m-d');
				$this->request->post['day'] = date('j');
				$this->request->post['month'] = date('n');
				$this->request->post['year'] = date('Y');
			}
			$day = $this->request->post['day'];
			$month = $this->request->post['month'];
			$year = $this->request->post['year'];

			$act_intime = $this->request->post['actual_intime'];
			$act_outtime = $this->request->post['actual_outtime'];
			$shift_intime = $this->request->post['shift_intime'];
			$shift_outtime = $this->request->post['shift_outtime'];
			$punch_date = $this->request->post['dot'];
			$punch_in_date = date('Y-m-d', strtotime($this->request->post['date_from']));
			$punch_out_date = date('Y-m-d', strtotime($this->request->post['date_to']));
			
			$first_half = 0;
			$second_half = 0;
			$abnormal_status = 0;
			$late_time = '00:00:00';
			if($act_intime != '00:00:00'){
				$start_date = new DateTime($punch_in_date.' '.$shift_intime);
				$since_start = $start_date->diff(new DateTime($punch_in_date.' '.$act_intime));
				if($since_start->h > 12){
					$start_date = new DateTime($punch_in_date.' '.$shift_intime);
					$since_start = $start_date->diff(new DateTime($punch_out_date.' '.$act_intime));
				}
				$late_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
				if($since_start->invert == 1){
					$late_time = '00:00:00';
				}
				if($act_outtime != '00:00:00'){
					$first_half = 1;
					$second_half = 1;
				} else {
					$first_half = 0;
					$second_half = 0;
				}
				//echo 'out';exit;
				$working_time = '00:00:00';
				$early_time = '00:00:00';
				if($abnormal_status == 0){ //if abnormal status is zero calculate further 
					$early_time = '00:00:00';
					if($act_outtime != '00:00:00'){ //for getting early time i.e difference between shiftouttime and sctouttime 
						$start_date = new DateTime($punch_in_date.' '.$shift_outtime);
						$since_start = $start_date->diff(new DateTime($punch_out_date.' '.$act_outtime));
						if($since_start->h > 12){
							$start_date = new DateTime($punch_out_date.' '.$shift_outtime);
							$since_start = $start_date->diff(new DateTime($punch_in_date.' '.$act_outtime));
						}
						$early_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;					
						if($since_start->invert == 0){
							$early_time = '00:00:00';
						}
					}					
					$working_time = '00:00:00';
					if($act_outtime != '00:00:00'){//for getting working time
						$start_date = new DateTime($punch_in_date.' '.$act_intime);
						$since_start = $start_date->diff(new DateTime($punch_out_date.' '.$act_outtime));
						$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
					}
				} else {
					$first_half = 0;
					$second_half = 0;
				}
			} else {
				$first_half = 0;
				$second_half = 0;
			}
			
			$trans_data_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$this->request->post['e_name_id']."' AND `date` = '".$punch_in_date."' ";
			$trans_data_res = $this->db->query($trans_data_sql)->row;

			$leave_data_sql = "SELECT `leave_type`, `type` FROM `oc_leave_transaction` WHERE `emp_id` = '".$this->request->post['e_name_id']."' AND `date` = '".$punch_in_date."' AND `p_status` = '1' AND `a_status` = '1' ";
			$leave_data_res = $this->db->query($leave_data_sql);
			$leave_in = 0;
			if($leave_data_res->num_rows > 0){
				//$shift_inn = '1';
				if($leave_data_res->row['type'] == ''){
					$leave_in = 1;
					$first_half = $leave_data_res->row['leave_type'];
					$second_half = $leave_data_res->row['leave_type'];
				} else {
					if($leave_data_res->row['type'] == 'F'){
						$leave_in = 1;
						$first_half = $leave_data_res->row['leave_type'];
						$second_half = $leave_data_res->row['leave_type'];
					} elseif($leave_data_res->row['type'] == '1'){
						$leave_in = 2;
						$first_half = $leave_data_res->row['leave_type'];
						if($trans_data_res['halfday_status'] != 0){
							$second_half = 'HD';	
						}
					} elseif($leave_data_res->row['type'] == '2'){
						$leave_in = 3;
						if($trans_data_res['halfday_status'] != 0){
							$first_half = 'HD';	
						}
						$second_half = $leave_data_res->row['leave_type'];
					}
				}
			}
			$data = $this->request->post;

			// echo $first_half;
			// echo '<br/ >';
			// echo $second_half;
			// echo '<br/ >';
			// echo $leave_in;
			// echo '<br/ >';
			// exit;

			if($data['shift_id'] == 'HD'){
				if($first_half == '1' && $second_half == '1'){
					$present_status = 1;
					$absent_status = 0;
					$first_half = '1';
					$second_half = 'HD';
				} elseif($first_half == '1' && $second_half == '0'){
					$present_status = 1;
					$absent_status = 0;
					$first_half = '1';
					$second_half = 'HD';
				} elseif($first_half == '0' && $second_half == '1'){
					$present_status = 0;
					$absent_status = 1;
					$first_half = '0';
					$second_half = 'HD';
				} elseif($first_half == '0' && $second_half == '0'){
					$present_status = 0;
					$absent_status = 1;
					$first_half = '0';
					$second_half = 'HD';
				}
			} elseif($first_half == '1' && $second_half == '1'){
				$present_status = 1;
				$absent_status = 0;
			} elseif($first_half == '1' && $second_half == '0'){
				$present_status = 0.5;
				$absent_status = 0.5;
			} elseif($first_half == '0' && $second_half == '1'){
				$present_status = 0.5;
				$absent_status = 0.5;
			} elseif($leave_in == '1'){
				$present_status = 0;
				$absent_status = 0;
			} elseif($leave_in == '2'){
				if($second_half == '0'){
					$present_status = 0;
					$absent_status = 0.5;
				} else {
					$present_status = 0.5;
					$absent_status = 0;
				}
			} elseif($leave_in == '3'){
				if($first_half == '0'){
					$present_status = 0;
					$absent_status = 0.5;
				} else {
					$present_status = 0.5;
					$absent_status = 0;
				}
			} else {
				$present_status = 0;
				$absent_status = 1;
			}

			$halfday = 0;
			if($data['shift_id'] == 'WO'){
				$late_time = '00:00:00';
				$early_time = '00:00:00';
				$present_status = 0;
				$absent_status = 0;
				$weekly_off = 1;
				$compli = 0;
				$holiday = 0;
				$halfday = 0;
				$first_half = 'WO';
				$second_half = 'WO';
				$day_info_id = 'W_1';
			} elseif($data['shift_id'] == 'HLD'){
				$late_time = '00:00:00';
				$early_time = '00:00:00';
				$present_status = 0;
				$absent_status = 0;
				$weekly_off = 0;
				$holiday = 1;
				$compli = 0;
				$halfday = 0;
				$first_half = 'HLD';
				$second_half = 'HLD';
				$day_info_id = 'H_1';
			} elseif($data['shift_id'] == 'HD'){
				//$late_time = '00:00:00';
				//$early_time = '00:00:00';
				//$present_status = 0;
				//$absent_status = 0;
				$weekly_off = 0;
				$holiday = 0;
				$halfday = 1;
				$compli = 0;
				if($leave_in == 2 || $leave_in == 3){
				} else {
					if($first_half == 1){
						$second_half = 'HD';
					}
					//$first_half = '1';
					//$second_half = 'HD';
				}
				$day_info_id = 'HD_1';
			}  elseif($data['shift_id'] == 'COF'){
				$late_time = '00:00:00';
				$early_time = '00:00:00';
				$present_status = 0;
				$absent_status = 0;
				$weekly_off = 0;
				$holiday = 0;
				$halfday = 0;
				$compli = 1;
				$first_half = 'COF';
				$second_half = 'COF';
				$day_info_id = 'C_1';
			}else {
				$weekly_off = 0;
				$holiday = 0;
				$halfday = 0;
				$compli = 0;
				$day_info_id = 'S_'.$data['shift_id'];
			}

			$old_data_sql = "SELECT * FROM `oc_transaction` WHERE `date` = '".$data['dot']."' AND `emp_id` = '".$data['e_name_id']."' ";
			$old_datas = $this->db->query($old_data_sql);
			if($old_datas->num_rows > 0){
				$old_data = $old_datas->row;
				
				if($data['actual_outtime'] != $old_data['act_outtime']){
					$sql = "UPDATE `oc_attendance` SET `status` = '0' WHERE `emp_id` = '".$data['e_name_id']."' AND `punch_time` = '".$old_data['act_outtime']."' AND `punch_date` = '".$old_data['date_out']."' ";
					$this->db->query($sql);
					//echo $sql;exit;
				}

				// if($data['actual_intime'] != $old_data['act_intime']){
				// 	$sql = "UPDATE `oc_attendance` SET `status` = '0' WHERE `emp_id` = '".$data['e_name_id']."' AND `punch_time` = '".$old_data['act_intime']."' AND `punch_date` = '".$old_data['date']."' ";
				// 	$this->db->query($sql);
				// 	//echo $sql;exit;
				// }

				if($data['actual_intime'] == '00:00:00' && $data['actual_outtime'] == '00:00:00'){
					$sql = "UPDATE `oc_attendance` SET `status` = '0' WHERE `emp_id` = '".$data['e_name_id']."' AND `punch_date` > '".$old_data['date']."' ";
					$this->db->query($sql);
				}
				//echo 'out';exit;

				$o_shift_datas_sql = "SELECT * FROM `oc_shift` WHERE `in_time` = '".$old_data['shift_intime']."' AND `out_time` = '".$old_data['shift_outtime']."' ";
				$o_shift_data_datas = $this->db->query($o_shift_datas_sql);
				$o_shift_id = 0;
				if($o_shift_data_datas->num_rows > 0){
					$o_shift_id = $o_shift_data_datas->row['shift_id'];
				}

				$n_shift_datas_sql = "SELECT * FROM `oc_shift` WHERE `in_time` = '".$shift_intime."' AND `out_time` = '".$shift_outtime."' ";
				$n_shift_data_datas = $this->db->query($n_shift_datas_sql);
				$n_shift_id = 0;
				if($n_shift_data_datas->num_rows > 0){
					$n_shift_id = $n_shift_data_datas->row['shift_id'];
				}

				$this->Mlog->write('---Start---');
				
				$this->Mlog->write('User Id : ' . $this->user->getUserName());
				$this->Mlog->write('Emp ID : ' . $data['e_name_id']);
				$this->Mlog->write('Date : ' . $data['dot']);

				if($o_shift_id == '0' || $o_shift_id == 0){
					if($old_data['leave_status'] == '1' || $old_data['leave_status'] == '0.5'){
						$o_shift_id = 'LEAVE';
					} elseif($old_data['weekly_off'] != '0'){
						$o_shift_id = 'WO';
					} elseif($old_data['holiday_id'] != '0'){
						$o_shift_id = 'HLD';
					} elseif($old_data['halfday_status'] != '0'){
						$o_shift_id = 'HD';
					} elseif($old_data['compli_status'] != '0'){
						$o_shift_id = 'COMP';
					}
				}
				$this->Mlog->write('Old Shift : ' . $o_shift_id);
				$this->Mlog->write('New Shift : ' . $data['shift_id']);

				$this->Mlog->write('Old Shift In Time: ' . $old_data['shift_intime']);
				$this->Mlog->write('New Shift In TIme: ' . $shift_intime);

				$this->Mlog->write('Old Shift Out Time: ' . $old_data['shift_outtime']);
				$this->Mlog->write('New Shift Out Time: ' . $shift_outtime);

				$this->Mlog->write('Old Act In Date: ' . $old_data['date']);
				$this->Mlog->write('New Act In Date: ' . $punch_in_date);

				$this->Mlog->write('Old Act In Time: ' . $old_data['act_intime']);
				$this->Mlog->write('New Act In TIme: ' . $act_intime);

				$this->Mlog->write('Old Act Out Date: ' . $old_data['date_out']);
				$this->Mlog->write('New Act Out Date: ' . $punch_out_date);

				$this->Mlog->write('Old Act Out Time: ' . $old_data['act_outtime']);
				$this->Mlog->write('New Act Out TIme: ' . $act_outtime);
				
				$this->Mlog->write('---End---');
			}

			//echo $day_info_id;exit;

			//$this->db->query("UPDATE `" . DB_PREFIX . "shift_schedule` SET `".$day."` = '" . $this->db->escape($day_info_id) . "' WHERE `emp_code` = '" . (int)$data['e_name_id'] . "' ");	
			//$this->db->query("UPDATE `" . DB_PREFIX . "shift_schedule` SET `".$day."` = '" . $this->db->escape($day_info_id) . "' WHERE `emp_code` = '" . (int)$data['e_name_id'] . "' AND `month` = '".$month."' AND `year` = '".$year."' ");	
			//echo "UPDATE `" . DB_PREFIX . "shift_schedule` SET `".$day."` = '" . $this->db->escape($day_info_id) . "' WHERE `emp_code` = '" . (int)$data['e_name_id'] . "' AND `month` = '".$month."' AND `year` = '".$year."' ";
			//echo '<br />';
			if($data['insert'] == '0'){
				$sql = "UPDATE `oc_transaction` SET `date` = '".$punch_in_date."', `date_out` = '".$punch_out_date."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', `weekly_off` = '".$weekly_off."', `holiday_id` = '".$holiday."', `halfday_status` = '".$halfday."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `abnormal_status` = '".$abnormal_status."', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', `compli_status` = '".$compli."', `manual_status` = '1' WHERE `transaction_id` = '".$data['transaction_id']."' ";
				//echo $sql;
				//echo '<br />';
				//exit;
				$this->db->query($sql);	
				$this->session->data['success'] = 'You have updated the Transaction';
				$transaction_id = $data['transaction_id'];
			} else {
				$emp_data = $this->model_transaction_transaction->getEmployees_dat($data['e_name_id']);
				if(isset($emp_data['emp_code'])){
					$department = $emp_data['department'];
					$unit = $emp_data['unit'];
					$group = $emp_data['group'];
				} else {
					$department = '';
					$unit = '';
					$group = '';
				}
				if($leave_in == 1 || $leave_in == 2 || $leave_in == 3){
					$leave_status = 1;
				} else {
					$leave_status = 0;
				}
				$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$data['e_name_id']."', `emp_name` = '".$data['e_name']."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', `day` = '".$data['day']."', `month` = '".$data['month']."', `year` = '".$data['year']."', `date` = '".$punch_in_date."', `date_out` = '".$punch_out_date."', `department` = '".$this->db->escape($department)."', `unit` = '".$this->db->escape($unit)."', `group` = '".$this->db->escape($group)."', `weekly_off` = '".$weekly_off."', `holiday_id` = '".$holiday."', `halfday_status` = '".$halfday."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `abnormal_status` = '".$abnormal_status."', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', `compli_status` = '".$compli."', `leave_status` = '".$leave_status."', `manual_status` = '1'  ";
				//echo $sql;
				//echo '<br />';
				//exit;
				$this->db->query($sql);
				$transaction_id = $this->db->getLastId();
				$this->session->data['success'] = 'You have Inserted the Transaction';	
			}
			
			/*
			@author: Eric
			This part is for updating OD status.
			I have created on_duty field in transaction table
			commented as OD is updated as leave from leave input
			*/
			/*
			if(isset($this->request->post['od']) && $this->request->post['od'] == '1') {
				//this case is for person who is physicall not present for full day
				if($act_intime == '00:00:00' && $act_outtime == '00:00:00') {
					$sql = "UPDATE `oc_transaction` SET `on_duty` = '1', `present_status` = '1', `absent_status` = '0', `firsthalf_status` = 'OD', `secondhalf_status` = 'OD' WHERE `transaction_id` = '".$transaction_id."'";
					$this->db->query($sql);
				} else {
					if($first_half == 0) {
						$first_half = "OD";
					}
					if($second_half == 0) {
						$second_half = "OD";
					}
					$sql = "UPDATE `oc_transaction` SET `on_duty` = '1', `present_status` = '1', `absent_status` = '0', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."' WHERE `transaction_id` = '".$transaction_id."'";
					$this->db->query($sql);	
				}
			}
			*/

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['filter_approval_1'])) {
				$url .= '&filter_approval_1=' . $this->request->get['filter_approval_1'];
			}

			if (isset($this->request->get['filter_date'])) {
				$url .= '&filter_date=' . $this->request->get['filter_date'];
			}

			if (isset($this->request->get['filter_dept'])) {
				$url .= '&filter_dept=' . $this->request->get['filter_dept'];
			}

			if (isset($this->request->get['filter_unit'])) {
				$url .= '&filter_unit=' . $this->request->get['filter_unit'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['requestform'])) {
				$url .= '&requestform=' . $this->request->get['requestform'];
			} 

			if (isset($this->request->get['unit'])) {
				$funit = $this->request->get['unit'];
			} else {
				$funit = '';
			}

			//echo 'out';exit;
			if (isset($this->request->get['requestform'])) {
				$r_url = $this->url->link('transaction/manualpunch', 'token=' . $this->session->data['token'].'&transaction_id='.$transaction_id.$url, 'SSL');
				$this->redirect($r_url);
			} else {
				$r_url = $this->url->link('transaction/manualpunch', 'token=' . $this->session->data['token'].'&transaction_id='.$transaction_id, 'SSL');
				if($funit){
					$r_url .= "&unit=".$funit;
				}	
				$this->redirect($r_url);
			}
		}

		$this->getForm();
	}

	protected function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');

		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		$this->data['text_no_results'] = $this->language->get('text_no_results');

		$this->data['tab_general'] = $this->language->get('tab_general');

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {	
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['e_name'])) {
			$this->data['error_employee'] = $this->error['e_name'];
		} else {
			$this->data['error_employee'] = '';
		}

		if (isset($this->error['dot'])) {
			$this->data['error_dot'] = $this->error['dot'];
		} else {
			$this->data['error_dot'] = '';
		}

		if (isset($this->error['shift_id'])) {
			$this->data['error_shift_id'] = $this->error['shift_id'];
		} else {
			$this->data['error_shift_id'] = array();
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('transaction/manualpunch', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_approval_1'])) {
			$url .= '&filter_approval_1=' . $this->request->get['filter_approval_1'];
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . $this->request->get['filter_date'];
		}

		if (isset($this->request->get['filter_dept'])) {
			$url .= '&filter_dept=' . $this->request->get['filter_dept'];
		}

		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['requestform'])) {
			$url .= '&requestform=' . $this->request->get['requestform'];
		}

		if (isset($this->request->get['unit'])) {
			$unit = $this->request->get['unit'];
			$this->data['abnormal_list'] = $this->url->link('transaction/dayprocess', 'token=' . $this->session->data['token'].'&unit=' . $unit, 'SSL');
			$this->data['request_list'] = '';
			$this->data['action'] = $this->url->link('transaction/manualpunch/update', 'token=' . $this->session->data['token'].'&unit=' . $unit.$url, 'SSL');
			$this->data['cancel'] = $this->url->link('transaction/dayprocess', 'token=' . $this->session->data['token'].'&unit=' . $unit, 'SSL');
		} elseif (isset($this->request->get['requestform'])) {
			$unit = '';
			$this->data['request_list'] = $this->url->link('transaction/requestformunit', 'token=' . $this->session->data['token'].$url, 'SSL');
			$this->data['abnormal_list'] = '';
			$this->data['action'] = $this->url->link('transaction/manualpunch/update', 'token=' . $this->session->data['token'].$url, 'SSL');
			$this->data['cancel'] = $this->url->link('transaction/requestformunit', 'token=' . $this->session->data['token'].$url, 'SSL');
		} else {
			$this->data['abnormal_list'] = '';
			$this->data['request_list'] = '';
			$unit = '';
			$this->data['action'] = $this->url->link('transaction/manualpunch/update', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['cancel'] = $this->url->link('transaction/manualpunch', 'token=' . $this->session->data['token'], 'SSL');
		}

		$transaction_data = array();
		if (isset($this->request->get['transaction_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$transaction_data = $this->model_transaction_transaction->gettransaction_data($this->request->get['transaction_id']);
		}

		$this->data['token'] = $this->session->data['token'];

		// echo'<pre>';
		// print_r($this->request->get);
		// exit;

		$statuses[1]['stat_id'] = '0';
		$statuses[1]['stat_name'] = 'Absent';
		$statuses[2]['stat_id'] = '1';
		$statuses[2]['stat_name'] = 'Present';
		$statuses[3]['stat_id'] = 'WO';
		$statuses[3]['stat_name'] = 'Weekly Off';
		$statuses[4]['stat_id'] = 'HLD';
		$statuses[4]['stat_name'] = 'Holiday';
		$statuses[5]['stat_id'] = 'HD';
		$statuses[5]['stat_name'] = 'Half Day';
		$statuses[6]['stat_id'] = 'OD';
		$statuses[6]['stat_name'] = 'On Duty';
		$statuses[7]['stat_id'] = 'COF';
		$statuses[7]['stat_name'] = 'Compensatory Off';
		$statuses[8]['stat_id'] = 'PL';
		$statuses[8]['stat_name'] = 'PL';
		$statuses[9]['stat_id'] = 'CL';
		$statuses[9]['stat_name'] = 'CL';
		$statuses[10]['stat_id'] = 'SL';
		$statuses[10]['stat_name'] = 'SL';
		$statuses[11]['stat_id'] = 'CL';
		$statuses[11]['stat_name'] = 'CL';
		$statuses[12]['stat_id'] = 'MT';
		$statuses[12]['stat_name'] = 'MT';
		$statuses[13]['stat_id'] = 'ESIC';
		$statuses[13]['stat_name'] = 'ESIC';
		$statuses[14]['stat_id'] = 'SUS';
		$statuses[14]['stat_name'] = 'SUS';
		$statuses[15]['stat_id'] = 'SWP';
		$statuses[15]['stat_name'] = 'SWP';
		$statuses[16]['stat_id'] = 'SHL';
		$statuses[16]['stat_name'] = 'SHL';
		
		$this->data['statuses'] = $statuses;

		$datas['filter_name'] = '';

		$shift_data = array();
		$shift_datas = $this->model_catalog_shift->getshifts($datas);
		
		$shift_data[1]['shift_id'] = 'WO';
		$shift_data[1]['shift_name'] = 'Weekly Off';

		$shift_data[2]['shift_id'] = 'HLD';
		$shift_data[2]['shift_name'] = 'Holiday';

		$shift_data[3]['shift_id'] = 'HD';
		$shift_data[3]['shift_name'] = 'Half Day';

		$shift_data[4]['shift_id'] = 'COF';
		$shift_data[4]['shift_name'] = 'Compensatory Off';

		$ckey = 5;
		foreach ($shift_datas as $skey => $svalue) {
			$shift_data[$ckey]['shift_id'] = $svalue['shift_id'];
			$shift_data[$ckey]['shift_name'] = $svalue['name'];
			$ckey ++;
		}
		$this->data['shift_data'] = $shift_data;

		if (isset($this->request->post['insert'])) {
			$this->data['insert'] = $this->request->post['insert'];
		} elseif(isset($transaction_data[0]['transaction_id'])) {
			$this->data['insert'] = 0;
		} else {
			$this->data['insert'] = '';
		}

		if (isset($this->request->post['transaction_id'])) {
			$this->data['transaction_id'] = $this->request->post['transaction_id'];
		} elseif(isset($transaction_data[0]['transaction_id'])) {
			$this->data['transaction_id'] = $transaction_data[0]['transaction_id'];
		} else {	
			$this->data['transaction_id'] = 0;
		}

		if (isset($this->request->post['od'])) {
			$this->data['od'] = $this->request->post['od'];
		} elseif(isset($transaction_data[0]['on_duty'])) {
			$this->data['od'] = $transaction_data[0]['on_duty'];
		} else {	
			$this->data['od'] = 0;
		}

		if (isset($this->request->post['dot'])) {
			$this->data['dot'] = $this->request->post['dot'];
		} elseif (isset($transaction_data[0]['date'])) {
			$this->data['dot'] = date('Y-m-d', strtotime($transaction_data[0]['date']));
		} else {	
			$this->data['dot'] = date('Y-m-d');
		}
		$this->data['date_from'] = $this->data['dot'];

		if (isset($this->request->post['date_to'])) {
			$this->data['date_to'] = $this->request->post['date_to'];
		} elseif (isset($transaction_data[0]['date_out'])) {
			$this->data['date_to'] = date('Y-m-d', strtotime($transaction_data[0]['date_out']));
		} else {	
			$this->data['date_to'] = '0000-00-00';
		}

		if (isset($this->request->post['e_name'])) {
			$this->data['e_name'] = $this->request->post['e_name'];
			$this->data['e_name_id'] = $this->request->post['e_name_id'];
			$this->data['emp_code'] = $this->request->post['e_name_id'];
		} elseif (isset($transaction_data[0]['emp_name'])) {
			$this->data['e_name'] = $transaction_data[0]['emp_name'];
			$this->data['e_name_id'] = $transaction_data[0]['emp_id'];
			$this->data['emp_code'] = $transaction_data[0]['emp_id'];
		} else {	
			$this->data['e_name'] = '';
			$this->data['e_name_id'] = '';
			$this->data['emp_code'] = '';
		}

		if (isset($this->request->post['shift_id'])) {
			$this->data['shift_id'] = $this->request->post['shift_id'];
			$this->data['shift_intime'] = $this->request->post['shift_intime']; 
			$this->data['shift_outtime'] = $this->request->post['shift_outtime']; 
			$this->data['actual_intime'] = $this->request->post['actual_intime']; 
			$this->data['actual_outtime'] = $this->request->post['actual_outtime']; 
			$this->data['firsthalf_status'] = $this->request->post['firsthalf_status']; 
			$this->data['secondhalf_status'] = $this->request->post['secondhalf_status']; 
		} elseif (isset($transaction_data[0]['emp_name'])) {
			$today_date = date('j', strtotime($transaction_data[0]['date']));
			$month = date('n', strtotime($transaction_data[0]['date']));
			$year = date('Y', strtotime($transaction_data[0]['date']));
			$shift_data = $this->model_catalog_employee->getshift_id($transaction_data[0]['emp_id'], $today_date, $month, $year);
			$shift_ids = explode('_', $shift_data);
			$shift_intime = '';
			$shift_outtime = '';
			$act_intime = '';
			$act_outtime = '';
			$shift_id = 0;
			if($transaction_data[0]['weekly_off'] != '0'){
				$shift_id = 'WO';
				$shift_intime = $transaction_data[0]['shift_intime'];
				$shift_outtime = $transaction_data[0]['shift_outtime'];
				$act_intime = $transaction_data[0]['act_intime'];
				$act_outtime = $transaction_data[0]['act_outtime'];
			} elseif($transaction_data[0]['holiday_id'] != '0'){
				$shift_id = 'HLD';
				$shift_intime = $transaction_data[0]['shift_intime'];
				$shift_outtime = $transaction_data[0]['shift_outtime'];
				$act_intime = $transaction_data[0]['act_intime'];
				$act_outtime = $transaction_data[0]['act_outtime'];
			} elseif($transaction_data[0]['halfday_status'] != '0'){
				$shift_id = 'HD';
				$shift_intime = $transaction_data[0]['shift_intime'];
				$shift_outtime = $transaction_data[0]['shift_outtime'];
				$act_intime = $transaction_data[0]['act_intime'];
				$act_outtime = $transaction_data[0]['act_outtime'];
			} elseif($transaction_data[0]['compli_status'] != '0'){
				$shift_id = 'COF';
				$shift_intime = $transaction_data[0]['shift_intime'];
				$shift_outtime = $transaction_data[0]['shift_outtime'];
				$act_intime = $transaction_data[0]['act_intime'];
				$act_outtime = $transaction_data[0]['act_outtime'];
			} elseif($transaction_data[0]['on_duty'] != '0'){
				$shift_id = 'OD';
				$shift_intime = $transaction_data[0]['shift_intime'];
				$shift_outtime = $transaction_data[0]['shift_outtime'];
				$act_intime = $transaction_data[0]['act_intime'];
				$act_outtime = $transaction_data[0]['act_outtime'];
			} else {
				$shift_datas_sql = "SELECT * FROM `oc_shift` WHERE `in_time` = '".$transaction_data[0]['shift_intime']."' AND `out_time` = '".$transaction_data[0]['shift_outtime']."' ";
				$shift_data_datas = $this->db->query($shift_datas_sql);
				$shift_id = 0;
				if($shift_data_datas->num_rows > 0){
					$shift_id = $shift_data_datas->row['shift_id'];
				}
				$shift_intime = $transaction_data[0]['shift_intime'];
				$shift_outtime = $transaction_data[0]['shift_outtime'];
				$act_intime = $transaction_data[0]['act_intime'];
				$act_outtime = $transaction_data[0]['act_outtime'];
			}

			$this->data['shift_id'] = $shift_id;
			$this->data['shift_intime'] = $shift_intime;
			$this->data['shift_outtime'] = $shift_outtime;
			$this->data['actual_intime'] = $act_intime;
			$this->data['actual_outtime'] = $act_outtime;
			$this->data['firsthalf_status'] = $transaction_data[0]['firsthalf_status'];
			$this->data['secondhalf_status'] = $transaction_data[0]['secondhalf_status'];
			
			if($shift_id == 'WO'){
				$this->data['firsthalf_status'] = 'WO';
				$this->data['secondhalf_status'] = 'WO';
			} elseif($shift_id == 'HLD'){
				$this->data['firsthalf_status'] = 'HLD';
				$this->data['secondhalf_status'] = 'HLD';
			} elseif($shift_id == 'HD'){
				//$this->data['firsthalf_status'] = 'HD';
				//$this->data['secondhalf_status'] = 'HD';
			} elseif($shift_id == 'COF'){
				$this->data['firsthalf_status'] = 'COF';
				$this->data['secondhalf_status'] = 'COF';
			} elseif($shift_id == 'OD'){
				$this->data['firsthalf_status'] = 'OD';
				$this->data['secondhalf_status'] = 'OD';
			}

			$leave_data_sql = "SELECT `leave_type`, `type` FROM `oc_leave_transaction` WHERE `emp_id` = '".$this->data['e_name_id']."' AND `date` = '".$this->data['dot']."' AND `p_status` = '1' AND `a_status` = '1' ";
			$leave_data_res = $this->db->query($leave_data_sql);
			$leave_in = 0;
			if($leave_data_res->num_rows > 0){
				//$shift_inn = '1';
				if($leave_data_res->row['type'] == ''){
					$leave_in = 1;
					$first_half = $leave_data_res->row['leave_type'];
					$second_half = $leave_data_res->row['leave_type'];
				} else {
					if($leave_data_res->row['type'] == 'F'){
						$leave_in = 1;
						$this->data['firsthalf_status'] = $leave_data_res->row['leave_type'];
						$this->data['secondhalf_status'] = $leave_data_res->row['leave_type'];
					} elseif($leave_data_res->row['type'] == '1'){
						$leave_in = 2;
						$this->data['firsthalf_status'] = $leave_data_res->row['leave_type'];
						if($transaction_data[0]['halfday_status'] != 0){
							$this->data['secondhalf_status'] = 'HD';
						}
					} elseif($leave_data_res->row['type'] == '2'){
						$leave_in = 3;
						$this->data['secondhalf_status'] = $leave_data_res->row['leave_type'];
						if($transaction_data[0]['halfday_status'] != 0){
							$this->data['firsthalf_status'] = 'HD';
						}
					}
				}
			}

		} else {	
			$this->data['shift_id'] = 0;
			$this->data['shift_intime'] = '';
			$this->data['shift_outtime'] = '';
			$this->data['actual_intime'] = '';
			$this->data['actual_outtime'] = '';
			$this->data['firsthalf_status'] = '0';
			$this->data['secondhalf_status'] = '0';
		}



		
		$this->template = 'transaction/manualpunch.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}  

	protected function validateForm() {
		$this->load->model('transaction/transaction');

		if (!$this->user->hasPermission('modify', 'transaction/horse_wise')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if(strlen(utf8_decode(trim($this->request->post['e_name']))) < 1 || strlen(utf8_decode(trim($this->request->post['e_name']))) > 255){
			$this->error['e_name'] = 'Please Select Employee Name';
		} else {
			if($this->request->post['e_name_id'] == ''){
				$emp_id = $this->model_transaction_transaction->getempexist($this->request->post['e_name_id']);				
				if($emp_id == 0){
					$this->error['e_name'] = 'Employee Does Not Exist';
				}
			}
		}

		if(strlen(utf8_decode(trim($this->request->post['dot']))) < 1 || strlen(utf8_decode(trim($this->request->post['dot']))) > 255){
			$this->error['dot'] = $this->language->get('error_dot');
		}

		if($this->request->post['shift_id'] === 0){
			$this->error['shift_id'] = 'Please Select Value From Shift Data';
		}

		// echo '<pre>';
		// print_r($this->error);
		// exit;

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	public function autocomplete() {
		$json = array();

		if ((isset($this->request->get['filter_name']) || isset($this->request->get['filter_name_id']) ) && isset($this->request->get['filter_date'])) {
			$this->load->model('catalog/employee');
			$this->load->model('report/common_report');

			if(isset($this->request->get['filter_name'])){
				$filter_name = $this->request->get['filter_name'];
			} else {
				$filter_name = '';
			}

			if(isset($this->request->get['filter_name_id'])){
				$filter_name_id = $this->request->get['filter_name_id'];
			} else {
				$filter_name_id = '';
			}

			$data = array(
				'filter_name' => $filter_name,
				'filter_name_id' => $filter_name_id,
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_employee->getemployees($data);

			foreach ($results as $result) {
				$filter_date = $this->request->get['filter_date'];
				$datas = array(
					'filter_date' => date('Y-m-d', strtotime($this->request->get['filter_date'])),
					'filter_limit' => 1,
					//'month_close' => 1,
					'filter_date_start' => '',
					'filter_date_end' => '',
					'unit' => '',
					'department' => '',
					'group' => ''
				);
				$transaction_data = $this->model_report_common_report->gettransaction_data($result['employee_id'], $datas);
				$actual_intime = '00:00:00';
				$actual_outtime = '00:00:00';
				$shift_intime = '00:00:00';
				$shift_outtime = '00:00:00';
				$secondhalf_status = 0;
				$firsthalf_status = 0;
				$shift_id = 0;
				$transaction_id = 0;
				$date_from = '0000-00-00';
				$date_to = '0000-00-00';
				$month_close = 0;
				$shift_id = 0; 
				if(isset($transaction_data[0]['transaction_id'])){
					$actual_intime = $transaction_data[0]['act_intime'];
					$actual_outtime = $transaction_data[0]['act_outtime'];
					$shift_intime = $transaction_data[0]['shift_intime'];
					$shift_outtime = $transaction_data[0]['shift_outtime'];
					$firsthalf_status = $transaction_data[0]['firsthalf_status'];
					$secondhalf_status = $transaction_data[0]['secondhalf_status'];
					$transaction_id = $transaction_data[0]['transaction_id'];
					$date_from = $transaction_data[0]['date'];
					$date_to = $transaction_data[0]['date_out'];
					$month_close = $transaction_data[0]['month_close_status'];
					$insert_stat = 0;
					
					if($transaction_data[0]['weekly_off'] != '0'){
						$shift_id = 'WO';
					} elseif($transaction_data[0]['holiday_id'] != '0'){
						$shift_id = 'HLD';
					} elseif($transaction_data[0]['halfday_status'] != '0'){
						$shift_id = 'HD';
					} elseif($transaction_data[0]['compli_status'] != '0'){
						$shift_id = 'COF';
					} elseif($transaction_data[0]['on_duty'] != '0'){
						$shift_id = 'OD';
					} else {
						$shift_datas_sql = "SELECT * FROM `oc_shift` WHERE `in_time` = '".$transaction_data[0]['shift_intime']."' AND `out_time` = '".$transaction_data[0]['shift_outtime']."' ";
						$shift_data_datas = $this->db->query($shift_datas_sql);
						$shift_id = 0;
						if($shift_data_datas->num_rows > 0){
							$shift_id = $shift_data_datas->row['shift_id'];
						}
					}
				} else {
					$insert_stat = 1;
				}
				// $today_date = date('j', strtotime($filter_date));
				// $month = date('n', strtotime($filter_date));
				// $year = date('Y', strtotime($filter_date));
				// $shift_data = $this->model_catalog_employee->getshift_id($result['emp_code'], $today_date, $month, $year);
				// $shift_ids = explode('_', $shift_data);
				// if(isset($shift_ids[1])){
				// 	if($shift_ids[0] == 'S'){
				// 		$shift_id = $shift_ids['1'];
				// 	} elseif($shift_ids[0] == 'W') {
				// 		$shift_id = 'WO';
				// 	} elseif($shift_ids[0] == 'H'){
				// 		$shift_id = 'HLD';
				// 	} elseif($shift_ids[0] == 'HD'){
				// 		$shift_id = 'HD';
				// 	} elseif($shift_ids[0] == 'C'){
				// 		$shift_id = 'COF';
				// 	}
				// } else {
				// 	$shift_id = 0;
				// }
				if(isset($transaction_data[0]['on_duty'])){
					$on_duty = $transaction_data[0]['on_duty'];
				} else {
					$on_duty = 0;
				}
				$json[] = array(
					'emp_id' =>$result['employee_id'],
					'emp_code' => $result['emp_code'],
					'on_duty' => $on_duty, 
					'name'            => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
					'shift_id' => $shift_id,
					'actual_intime' => $actual_intime,
					'actual_outtime' => $actual_outtime,
					'shift_intime' => $shift_intime,
					'shift_outtime' => $shift_outtime,
					'firsthalf_status' => $firsthalf_status,
					'secondhalf_status' => $secondhalf_status,
					'insert_stat' => $insert_stat,
					'date_from' => $date_from,
					'date_to' => $date_to,
					'month_close' => $month_close,
					'transaction_id' => $transaction_id
				);
			}		
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		// echo '<pre>';
		// print_r($json);


		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}

	public function getshiftdata() {
		$jsondata = array();
		$jsondata['status'] = 0;
		//$this->log->write(print_r($this->request->get,true));
		$shift_data = array();
		if(isset($this->request->get['shift_id'])){
			$shfit_id = $this->request->get['shift_id'];
	        	$this->load->model('transaction/transaction');
			$shift_data = $this->model_transaction_transaction->getshiftdata($this->request->get['shift_id']);
			if(isset($shift_data['shift_id'])){
				$shift_data['in_time'] = $shift_data['in_time'];
				$shift_data['out_time'] = $shift_data['out_time'];
				$jsondata['shift_data'] = $shift_data;
				$jsondata['status'] = 1;
			} elseif(isset($this->request->get['transaction_id'])){
				$trans_data = $this->db->query("SELECT * FROM `oc_transaction` WHERE `transaction_id` = '".$this->request->get['transaction_id']."' ")->row;
				$shift_data['in_time'] = $trans_data['shift_intime'];
				$shift_data['out_time'] = $trans_data['shift_outtime'];
				$jsondata['shift_data'] = $shift_data;
				$jsondata['status'] = 1;
			}
		}
	    	$this->response->setOutput(Json_encode($jsondata));
	}
}
?>