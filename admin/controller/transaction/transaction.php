<?php
class ControllerTransactionTransaction extends Controller { 
	public function index() {  
		$this->language->load('transaction/transaction');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		
		$url = '';

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('transaction/transaction', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->load->model('report/common_report');

		if(isset($this->session->data['warning'])){
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['generate'] = $this->url->link('transaction/transaction/generate', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['token'] = $this->session->data['token'];

		$this->template = 'transaction/transaction.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	public function generate(){
		$this->language->load('transaction/transaction');
		$this->document->setTitle($this->language->get('heading_title'));			
		
		$this->load->model('transaction/transaction');

		$raw_attendance_datass = $this->model_transaction_transaction->getrawattendance_group();
		//echo '<pre>';
		//print_r($raw_attendance_datass);
		// exit;
		if($raw_attendance_datass) {
			foreach ($raw_attendance_datass as $rkeyss => $rvaluess) {
				$emp_data = $this->model_transaction_transaction->getempdata($rvaluess['emp_id']);
				if(isset($emp_data['name']) && $emp_data['name'] != ''){
					$emp_name = $emp_data['name'];
					$department = $emp_data['department'];
					$unit = $emp_data['unit'];
					$group = $emp_data['group'];

					$shift_data = $this->model_transaction_transaction->getshiftdata($emp_data['shift']);
					if(isset($shift_data['shift_id'])){
						$shift_intime = $shift_data['in_time'];
						$shift_outtime = $shift_data['out_time'];

						$act_intime = $this->model_transaction_transaction->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
						$act_outtime = $this->model_transaction_transaction->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date']);
						
						//$act_intime = "09:15:00";
						//$act_outtime = "17:20:00";


						$start_date = new DateTime($rvaluess['punch_date'].' '.$shift_intime);
						$since_start = $start_date->diff(new DateTime($rvaluess['punch_date'].' '.$act_intime));
						$late_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
						if($since_start->invert == 1){
							$late_time = '00:00:00';
						}

						$start_date1 = new DateTime($rvaluess['punch_date'].' '.$shift_outtime);
						$since_start1 = $start_date1->diff(new DateTime($rvaluess['punch_date'].' '.$act_outtime));
						$early_time = $since_start1->h.':'.$since_start1->i.':'.$since_start1->s;					
						if($since_start1->invert == 0){
							$early_time = '00:00:00';
						}					
						
						$start_date2 = new DateTime($rvaluess['punch_date'].' '.$act_intime);
						$since_start2 = $start_date2->diff(new DateTime($rvaluess['punch_date'].' '.$act_outtime));
						$working_time = $since_start2->h.':'.$since_start2->i.':'.$since_start2->s;


						$day = date('j', strtotime($rvaluess['punch_date']));
						$month = date('n', strtotime($rvaluess['punch_date']));
						$year = date('Y', strtotime($rvaluess['punch_date']));

						$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `weekly_off` = '".$emp_data['weekly_off']."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$rvaluess['punch_date']."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."' ";
						$this->db->query($sql);
						//echo $sql;
						//exit;
						// echo '<pre>';
						// print_r($act_intime);
						// echo '<pre>';
						// print_r($shift_intime);
						// echo '<pre>';
						// print_r($act_outtime);
						// echo '<pre>';
						// print_r($shift_outtime);
						// echo '<pre>';
						// print_r($late_time);
						// echo '<pre>';
						// print_r($early_time);
						// echo '<pre>';
						// print_r($working_time);
						// exit;
					}
				}
			}
			$update = "UPDATE `oc_attendance` SET `status` = '1' ";
			$this->db->query($update);
			//echo 'Done';exit;
			$this->session->data['success'] = 'Transaction Generated';
			$this->redirect($this->url->link('transaction/transaction', 'token=' . $this->session->data['token'], 'SSL'));
		} else {
			$this->session->data['warning'] = 'No Data available';
			$this->redirect($this->url->link('transaction/transaction', 'token=' . $this->session->data['token'], 'SSL'));
		}
	}

	public function generate_today(){
		if (isset($this->request->get['unit'])) {
			$filter_unit = $this->request->get['unit'];
		} else {
			$filter_unit = 0;
		}
		$this->db->query("start transaction;");
		$this->language->load('transaction/transaction');
		$this->document->setTitle($this->language->get('heading_title'));			
		$this->load->model('transaction/transaction');
		$this->load->model('report/common_report');
		$this->load->model('catalog/employee');		
		$exp_datas = array();
		$exp_datas_2 = array();
		$filter_date_start = date('Y-m-d');
		//$filter_date_start = '2017-04-01';
		$employees = array();
		$employees_final = array();
		$mdbFilename = 'Z:\Avi infotech Backup\eTimeTrackLite1.mdb';
		//$mdbFilename = DIR_DOWNLOAD.'eTimeTrackLite1.mdb';
		$user = '';
		$password = '';

		$connection = odbc_connect("Driver={Microsoft Access Driver (*.mdb)};Dbq=$mdbFilename", $user, $password);
		$sql = "SELECT * FROM DeviceLogs WHERE Format(LogDate, 'yyyy-mm-dd') = '".$filter_date_start."' ";
		//echo $sql;exit;
		$rs = odbc_exec($connection, $sql);
		$results = odbc_fetch_array($rs);
		$rev_exp_datas = array();
		while($row = (odbc_fetch_array($rs))){
			$log_date = $row['LogDate'];
			$exp_date = explode(' ', $log_date);
			$in_time = $exp_date[1];
			$in_date = $exp_date[0];
			$emp_id = $row['UserId'];
			$result = $this->model_transaction_transaction->getEmployees_dat($emp_id);
			if(isset($result['emp_code'])){
				$employees[] = array(
					'employee_id' => $emp_id,//$result['emp_code'],
					'card_id' => $emp_id,//$result['card_number'],
					'emp_name' => $result['name'],
					'department' => $result['department'],
					'unit' => $result['unit'],
					'group' => $result['group'],
					'in_time' => $in_time,
					'punch_date' => $in_date,
					'fdate' => date('Y-m-d H:i:s', strtotime($in_date.' '.$in_time))
				);
			}
		}
		$this->data['employees'] = array();
		usort($employees, array($this, "sortByOrder"));
		$o_emp = array();
		foreach ($employees as $ekey => $evalue) {
			if(!isset($o_emp[$evalue['employee_id']])){
				$o_emp[$evalue['employee_id']] = $evalue['employee_id'];
				$this->data['employees'][] = $evalue;
			} 
		}
		if(isset($this->data['employees']) && count($this->data['employees']) > 0){
			$this->model_transaction_transaction->deleteorderhistory_new($filter_date_start);
			foreach ($this->data['employees'] as $fkey => $employee) {
				$mystring = $employee['in_time'];
				$findme   = ':';
				$pos = strpos($mystring, $findme);
				if($pos !== false){
					$in_times = substr($employee['in_time'], 0, 2). ":" .substr($employee['in_time'], 3, 2);
				} else {
					$in_times = substr($employee['in_time'], 0, 3). ":" .substr($employee['in_time'], 3, 2);
				}
				$employee['in_time'] = $in_times;
				$day_date = date('j');
				$month = date('n');
				$year = date('Y');
				$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$employee['employee_id']."' AND `month` = '".$month."' AND `year` = '".$year."' ";
				//$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `month` ='".$month."' AND `year` = '".$year."' AND `emp_code` = '".$rvaluess['emp_id']."' ";
				$shift_schedule = $this->db->query($update3)->row;
				$schedule_raw = explode('_', $shift_schedule[$day_date]);
				if(!isset($schedule_raw[2])){
					$schedule_raw[2] = 1;
				}	
				$shift_intime = '00:00:00';
				if($schedule_raw[0] == 'S'){
					$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[1]);
					if(isset($shift_data['shift_id'])){
						$shift_intime = $shift_data['in_time'];
					}
				} elseif($schedule_raw[0] == 'W' || $schedule_raw[0] == 'H' || $schedule_raw[0] == 'HD' || $schedule_raw[0] == 'C'){
					$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[2]);
					if(isset($shift_data['shift_id'])){
						$shift_intime = $shift_data['in_time'];
					}
				}
				$start_date = new DateTime($filter_date_start.' '.$shift_intime);
				$since_start = $start_date->diff(new DateTime($filter_date_start.' '.$in_times));
				$late_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
				if($since_start->invert == 1){
					$late_time = '00:00:00';
				}
				$employee['shift_intime'] = $shift_intime;
				$employee['late_time'] = $late_time;
				// echo '<pre>';
				// print_r($employee);
				// exit;
				$this->model_transaction_transaction->insert_attendance_data_new($employee);
			}
		}
		$this->db->query("commit;");
		if(!isset($this->request->get['script'])){
			$this->session->data['success'] = 'Transaction Generated';
			$url = '&filter_date_start='.$filter_date_start;
			if($filter_unit){
				$url .= '&unit='.$filter_unit;
			}
			$this->redirect($this->url->link('report/today', 'token=' . $this->session->data['token'].$url, 'SSL'));
		} else {
			echo 'Done';exit;
		}
	}

	public function sortByOrder($a, $b) {
		$v1 = strtotime($a['fdate']);
	   	$v2 = strtotime($b['fdate']);
	   	return $v1 - $v2; // $v2 - $v1 to reverse direction
		// if ($a['punch_date'] == $b['punch_date']) {
	 		//return ($a['in_time'] > $b['in_time']) ? -1 : 1;;
	 	//}
	 	//return $a['punch_date'] - $b['punch_date'];
	}

	public function generate_filter_date(){
		// $referrerss = explode('route=', $this->request->server['HTTP_REFERER']);
		// $referrers = explode('&', $referrerss[1]);
		// $referrer = $referrers[0]; 
		// echo '<pre>';
		// print_r($referrer);
		// exit;

		// echo '<pre>';
		// print_r($this->request->get);
		// exit;

		if (isset($this->request->get['unit'])) {
			$filter_unit = $this->request->get['unit'];
		} else {
			$filter_unit = 0;
		}

		$this->db->query("start transaction;");

		$filter_date_start = '';
		if(isset($this->request->get['filter_date_start'])){
			$filter_date_start = $this->request->get['filter_date_start'];
		}

		$this->language->load('transaction/transaction');
		$this->document->setTitle($this->language->get('heading_title'));			
		
		$this->load->model('transaction/transaction');
		$this->load->model('report/common_report');

		//echo $filter_date_start;exit;

		if($filter_date_start != '2016-10-01'){
			$prev_day = date('Y-m-d', strtotime($filter_date_start .' -1 day'));
			$update2 = "SELECT * FROM `oc_transaction` WHERE `date` = '".$prev_day."' AND `day_close_status` = '1' ";
			$day_status = $this->db->query($update2);
			if($day_status->num_rows == 0){
				$this->db->query("commit;");
				$this->session->data['warning'] = 'Please Close Previous Day';
				if(isset($this->request->get['home'])){
					$this->redirect($this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'));
				} else {
					$this->redirect($this->url->link('report/attendance', 'token=' . $this->session->data['token'], 'SSL'));
				}
			}
		}


		$update2 = "SELECT * FROM `oc_transaction` WHERE `date` = '".$filter_date_start."' AND `day_close_status` = '1' ";
		if($filter_unit){
			$update2 .= " AND LOWER(`unit`) = '" . $this->db->escape(strtolower($filter_unit)) . "'";
		}
		$day_status = $this->db->query($update2);
		// echo '<pre>';
		// print_r($day_status);
		// exit;
		if($day_status->num_rows == 0){
			$exp_datas = array();
			$exp_datas_2 = array();
			$a = glob(DIR_DOWNLOAD."*.Txt");
			$b = glob(DIR_DOWNLOAD."*.txt");
			$c = glob(DIR_DOWNLOAD."*.TXT");
			$all_files = array_merge($a, $b, $c);
			$next_date = date('Y-m-d', strtotime($filter_date_start .' +1 day'));

			$in1 = 0;
			$in2 = 0;
			$in3 = 0;
			$in4 = 0;
			$in5 = 0;

			foreach($all_files as $file_name) {
				$exp_datas_temp = array();
				//$date_array = explode('-',$filter_date_start);
				$texp = explode('/', $file_name);
				//$date_array[0] = substr($date_array[0], 2);
				//echo $texp[count($texp)-1];exit;
				$comp_date = date('d-m-Y', strtotime($filter_date_start));
				$comp_text = $comp_date.'.txt';
				$dir_file_name = strtolower(end($texp));
				if($comp_text == $dir_file_name){
				//if($texp[count($texp)-1] == $date_array[2].$date_array[1].$date_array[0].'00.Dat' || $texp[count($texp)-1] == $date_array[2].$date_array[1].$date_array[0].'00.dat' || $texp[count($texp)-1] == $date_array[2].$date_array[1].$date_array[0].'00.DAT'){
					$in1 = 1;
					//echo 'in 00';
					//echo '<br />';
					$data = file_get_contents($file_name, FILE_USE_INCLUDE_PATH, null);
					$exp_datas_temp = explode(PHP_EOL, $data);
					$exp_datas = array_merge($exp_datas, $exp_datas_temp);
				}

				// if($texp[count($texp)-1] == $date_array[2].$date_array[1].$date_array[0].'01.Dat' || $texp[count($texp)-1] == $date_array[2].$date_array[1].$date_array[0].'01.dat' || $texp[count($texp)-1] == $date_array[2].$date_array[1].$date_array[0].'01.DAT'){
				// 	//echo 'in 00';
				// 	//echo '<br />';
				// 	$data = file_get_contents($file_name, FILE_USE_INCLUDE_PATH, null);
				// 	$exp_datas_temp = explode(' 000', $data);
				// 	$exp_datas = array_merge($exp_datas, $exp_datas_temp);
				// }

				// if($texp[count($texp)-1] == $date_array[2].$date_array[1].$date_array[0].'02.Dat' || $texp[count($texp)-1] == $date_array[2].$date_array[1].$date_array[0].'02.dat' || $texp[count($texp)-1] == $date_array[2].$date_array[1].$date_array[0].'02.DAT'){
				// 	//echo 'in 00';
				// 	//echo '<br />';
				// 	$data = file_get_contents($file_name, FILE_USE_INCLUDE_PATH, null);
				// 	$exp_datas_temp = explode(' 000', $data);
				// 	$exp_datas = array_merge($exp_datas, $exp_datas_temp);
				// }

				// if($texp[count($texp)-1] == $date_array[2].$date_array[1].$date_array[0].'03.Dat' || $texp[count($texp)-1] == $date_array[2].$date_array[1].$date_array[0].'03.dat' || $texp[count($texp)-1] == $date_array[2].$date_array[1].$date_array[0].'03.DAT'){
				// 	//echo 'in 00';
				// 	//echo '<br />';
				// 	$data = file_get_contents($file_name, FILE_USE_INCLUDE_PATH, null);
				// 	$exp_datas_temp = explode(' 000', $data);
				// 	$exp_datas = array_merge($exp_datas, $exp_datas_temp);
				// }

				// if($texp[count($texp)-1] == $date_array[2].$date_array[1].$date_array[0].'04.Dat' || $texp[count($texp)-1] == $date_array[2].$date_array[1].$date_array[0].'04.dat' || $texp[count($texp)-1] == $date_array[2].$date_array[1].$date_array[0].'04.DAT'){
				// 	//echo 'in 00';
				// 	//echo '<br />';
				// 	$data = file_get_contents($file_name, FILE_USE_INCLUDE_PATH, null);
				// 	$exp_datas_temp = explode(' 000', $data);
				// 	$exp_datas = array_merge($exp_datas, $exp_datas_temp);
				// }

				// if($texp[count($texp)-1] == $date_array[2].$date_array[1].$date_array[0].'11.Dat' || $texp[count($texp)-1] == $date_array[2].$date_array[1].$date_array[0].'11.dat' || $texp[count($texp)-1] == $date_array[2].$date_array[1].$date_array[0].'11.DAT'){
				// 	$in2 = 1;
				// 	//echo 'in 11';
				// 	//echo '<br />';
				// 	$data = file_get_contents($file_name, FILE_USE_INCLUDE_PATH, null);
				// 	$exp_datas_temp = explode(' 000', $data);
				// 	$exp_datas = array_merge($exp_datas, $exp_datas_temp);
				// }

				// if($texp[count($texp)-1] == $date_array[2].$date_array[1].$date_array[0].'12.Dat' || $texp[count($texp)-1] == $date_array[2].$date_array[1].$date_array[0].'12.dat' || $texp[count($texp)-1] == $date_array[2].$date_array[1].$date_array[0].'12.DAT'){
				// 	$in3 = 1;
				// 	//echo 'in 12';
				// 	//echo '<br />';
				// 	$data = file_get_contents($file_name, FILE_USE_INCLUDE_PATH, null);
				// 	$exp_datas_temp = explode(' 000', $data);
				// 	$exp_datas = array_merge($exp_datas, $exp_datas_temp);
				// }

				// if($texp[count($texp)-1] == $date_array[2].$date_array[1].$date_array[0].'13.Dat' || $texp[count($texp)-1] == $date_array[2].$date_array[1].$date_array[0].'13.dat' || $texp[count($texp)-1] == $date_array[2].$date_array[1].$date_array[0].'13.DAT'){
				// 	$in4 = 1;
				// 	//echo 'in 13';
				// 	//echo '<br />';
				// 	$data = file_get_contents($file_name, FILE_USE_INCLUDE_PATH, null);
				// 	$exp_datas_temp = explode(' 000', $data);
				// 	$exp_datas = array_merge($exp_datas, $exp_datas_temp);
				// }

				// if($texp[count($texp)-1] == $date_array[2].$date_array[1].$date_array[0].'14.Dat' || $texp[count($texp)-1] == $date_array[2].$date_array[1].$date_array[0].'14.dat' || $texp[count($texp)-1] == $date_array[2].$date_array[1].$date_array[0].'14.DAT'){
				// 	$in5 = 1;
				// 	//echo 'in 14';
				// 	//echo '<br />';
				// 	$data = file_get_contents($file_name, FILE_USE_INCLUDE_PATH, null);
				// 	$exp_datas_temp = explode(' 000', $data);
				// 	$exp_datas = array_merge($exp_datas, $exp_datas_temp);
				// }
			}

			// if($in1 == 1 && $in2 == 1 && $in3 == 1 && $in4 == 1 && $in5 == 1){
			// 	$this->db->query("commit;");
			// } else {
			// 	$this->db->query("commit;");
			// 	$this->session->data['warning'] = 'Please Upload All DAT files into Download Folder';
			// 	if(isset($this->request->get['home'])){
			// 		$this->redirect($this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'));
			// 	} else {
			// 		$this->redirect($this->url->link('report/attendance', 'token=' . $this->session->data['token'], 'SSL'));
			// 	}
			// }

			// echo '<pre>';
			// print_r($exp_datas);
			// exit;
			//exit;

			$this->data['employees'] = array();
			$employees = array();
			$this->load->model('catalog/employee');		
			if($exp_datas[0] == ''){
				unset($exp_datas[0]);
				$rev_exp_datas = $exp_datas;
			} else {
				$rev_exp_datas = $exp_datas;			
			}
			/* commented as array_revere used to revert the array values and then ids for punch time are reversed */
			//$rev_exp_datas = array_reverse($exp_datas);
			//echo '<pre>';
			//print_r($rev_exp_datas);
			//exit;
			foreach($rev_exp_datas as $data) {
				$tdata = trim($data);
				$emp_id  = substr($tdata, 0, 8);
				$in_date_raw = substr($tdata, 9, 11);
				$in_date = date('Y-m-d', strtotime($in_date_raw));
				$in_time_hour = substr($tdata, 21, 2);
				$in_time_min = substr($tdata, 24, 2);
				$in_time = $in_time_hour.':'.$in_time_min;

				if($emp_id != '') {
					$result = $this->model_transaction_transaction->getEmployees_dat($emp_id);
					if(isset($result['emp_code'])){
						$employees[] = array(
							'employee_id' => $result['emp_code'],
							'card_id' => $result['card_number'],
							'in_time' => $in_time,
							'punch_date' => $in_date,
							'fdate' => date('Y-m-d H:i:s', strtotime($in_date.' '.$in_time))
						);
					}
				}
			}

			// echo '<pre>';
			// print_r($employees);
			// exit;

			//usort($employees, array($this, "sortByOrder"));
			// $exp_datas = array();
			// $a = glob(DIR_DOWNLOAD."*.Txt");
			// $b = glob(DIR_DOWNLOAD."*.txt");
			// $c = glob(DIR_DOWNLOAD."*.TXT");
			// $all_files = array_merge($a, $b, $c);
			// $next_date = date('Y-m-d', strtotime($filter_date_start .' +1 day'));
			// foreach($all_files as $file_name) {
			// 	$exp_datas_temp = array();
			// 	//$date_array = explode('-',$filter_date_start);
			// 	$texp = explode('/', $file_name);
			// 	//$date_array[0] = substr($date_array[0], 2);
			// 	//echo $texp[count($texp)-1];exit;
			// 	$comp_date = date('d-m-Y', strtotime($next_date));
			// 	$comp_text = $comp_date.'.txt';
			// 	$dir_file_name = strtolower(end($texp));
			// 	if($comp_text == $dir_file_name){
			// 	//if($texp[count($texp)-1] == $date_array[2].$date_array[1].$date_array[0].'00.Dat' || $texp[count($texp)-1] == $date_array[2].$date_array[1].$date_array[0].'00.dat' || $texp[count($texp)-1] == $date_array[2].$date_array[1].$date_array[0].'00.DAT'){
			// 		$in1 = 1;
			// 		//echo 'in 00';
			// 		//echo '<br />';
			// 		$data = file_get_contents($file_name, FILE_USE_INCLUDE_PATH, null);
			// 		$exp_datas_temp = explode(PHP_EOL, $data);
			// 		$exp_datas = array_merge($exp_datas, $exp_datas_temp);
			// 	}

			// 	// if($texp[count($texp)-1] == $date_array1[2].$date_array1[1].$date_array1[0].'01.Dat' || $texp[count($texp)-1] == $date_array1[2].$date_array1[1].$date_array1[0].'01.dat' || $texp[count($texp)-1] == $date_array1[2].$date_array1[1].$date_array1[0].'01.DAT'){
			// 	// 	//echo 'in 00';
			// 	// 	//echo '<br />';
			// 	// 	$data = file_get_contents($file_name, FILE_USE_INCLUDE_PATH, null);
			// 	// 	$exp_datas_temp = explode(' 000', $data);
			// 	// 	$exp_datas = array_merge($exp_datas, $exp_datas_temp);
			// 	// }

			// 	// if($texp[count($texp)-1] == $date_array1[2].$date_array1[1].$date_array1[0].'02.Dat' || $texp[count($texp)-1] == $date_array1[2].$date_array1[1].$date_array1[0].'02.dat' || $texp[count($texp)-1] == $date_array1[2].$date_array1[1].$date_array1[0].'02.DAT'){
			// 	// 	//echo 'in 00';
			// 	// 	//echo '<br />';
			// 	// 	$data = file_get_contents($file_name, FILE_USE_INCLUDE_PATH, null);
			// 	// 	$exp_datas_temp = explode(' 000', $data);
			// 	// 	$exp_datas = array_merge($exp_datas, $exp_datas_temp);
			// 	// }

			// 	// if($texp[count($texp)-1] == $date_array1[2].$date_array1[1].$date_array1[0].'03.Dat' || $texp[count($texp)-1] == $date_array1[2].$date_array1[1].$date_array1[0].'03.dat' || $texp[count($texp)-1] == $date_array1[2].$date_array1[1].$date_array1[0].'03.DAT'){
			// 	// 	//echo 'in 00';
			// 	// 	//echo '<br />';
			// 	// 	$data = file_get_contents($file_name, FILE_USE_INCLUDE_PATH, null);
			// 	// 	$exp_datas_temp = explode(' 000', $data);
			// 	// 	$exp_datas = array_merge($exp_datas, $exp_datas_temp);
			// 	// }

			// 	// if($texp[count($texp)-1] == $date_array1[2].$date_array1[1].$date_array1[0].'04.Dat' || $texp[count($texp)-1] == $date_array1[2].$date_array1[1].$date_array1[0].'04.dat' || $texp[count($texp)-1] == $date_array1[2].$date_array1[1].$date_array1[0].'04.DAT'){
			// 	// 	//echo 'in 00';
			// 	// 	//echo '<br />';
			// 	// 	$data = file_get_contents($file_name, FILE_USE_INCLUDE_PATH, null);
			// 	// 	$exp_datas_temp = explode(' 000', $data);
			// 	// 	$exp_datas = array_merge($exp_datas, $exp_datas_temp);
			// 	// }

			// 	// if($texp[count($texp)-1] == $date_array1[2].$date_array1[1].$date_array1[0].'11.Dat' || $texp[count($texp)-1] == $date_array1[2].$date_array1[1].$date_array1[0].'11.dat' || $texp[count($texp)-1] == $date_array1[2].$date_array1[1].$date_array1[0].'11.DAT'){
			// 	// 	//echo 'in 11';
			// 	// 	//echo '<br />';
			// 	// 	$data = file_get_contents($file_name, FILE_USE_INCLUDE_PATH, null);
			// 	// 	$exp_datas_temp = explode(' 000', $data);
			// 	// 	$exp_datas = array_merge($exp_datas, $exp_datas_temp);
			// 	// }

			// 	// if($texp[count($texp)-1] == $date_array1[2].$date_array1[1].$date_array1[0].'12.Dat' || $texp[count($texp)-1] == $date_array1[2].$date_array1[1].$date_array1[0].'12.dat' || $texp[count($texp)-1] == $date_array1[2].$date_array1[1].$date_array1[0].'12.DAT'){
			// 	// 	//echo 'in 12';
			// 	// 	//echo '<br />';
			// 	// 	$data = file_get_contents($file_name, FILE_USE_INCLUDE_PATH, null);
			// 	// 	$exp_datas_temp = explode(' 000', $data);
			// 	// 	$exp_datas = array_merge($exp_datas, $exp_datas_temp);
			// 	// }

			// 	// if($texp[count($texp)-1] == $date_array1[2].$date_array1[1].$date_array1[0].'13.Dat' || $texp[count($texp)-1] == $date_array1[2].$date_array1[1].$date_array1[0].'13.dat' || $texp[count($texp)-1] == $date_array1[2].$date_array1[1].$date_array1[0].'13.DAT'){
			// 	// 	//echo 'in 13';
			// 	// 	//echo '<br />';
			// 	// 	$data = file_get_contents($file_name, FILE_USE_INCLUDE_PATH, null);
			// 	// 	$exp_datas_temp = explode(' 000', $data);
			// 	// 	$exp_datas = array_merge($exp_datas, $exp_datas_temp);
			// 	// }

			// 	// if($texp[count($texp)-1] == $date_array1[2].$date_array1[1].$date_array1[0].'14.Dat' || $texp[count($texp)-1] == $date_array1[2].$date_array1[1].$date_array1[0].'14.dat' || $texp[count($texp)-1] == $date_array1[2].$date_array1[1].$date_array1[0].'14.DAT'){
			// 	// 	//echo 'in 14';
			// 	// 	//echo '<br />';
			// 	// 	$data = file_get_contents($file_name, FILE_USE_INCLUDE_PATH, null);
			// 	// 	$exp_datas_temp = explode(' 000', $data);
			// 	// 	$exp_datas = array_merge($exp_datas, $exp_datas_temp);
			// 	// }
			// }
			// if(isset($exp_datas[0]) && $exp_datas[0] == ''){
			// 	unset($exp_datas[0]);
			// 	$rev_exp_datas = $exp_datas;
			// } else {
			// 	$rev_exp_datas = $exp_datas;			
			// }
			// /* commented as array_revere used to revert the array values and then ids for punch time are reversed */
			// //$rev_exp_datas = array_reverse($exp_datas);
			// foreach($rev_exp_datas as $data) {
			// 	$tdata = trim($data);
			// 	$emp_id  = substr($tdata, 0, 8);
			// 	$in_date_raw = substr($tdata, 9, 11);
			// 	$in_date = date('Y-m-d', strtotime($in_date_raw));
			// 	$in_time_hour = substr($tdata, 21, 2);
			// 	$in_time_min = substr($tdata, 24, 2);
			// 	$in_time = $in_time_hour.':'.$in_time_min;

			// 	if($emp_id != '') {
			// 		$result = $this->model_transaction_transaction->getEmployees_dat($emp_id);
			// 		if(isset($result['emp_code'])){
			// 			$employees[] = array(
			// 				'employee_id' => $result['emp_code'],
			// 				'card_id' => $result['card_number'],
			// 				'in_time' => $in_time,
			// 				'punch_date' => $in_date,
			// 				'fdate' => date('Y-m-d H:i:s', strtotime($in_date.' '.$in_time))
			// 			);
			// 		}
			// 	}
			// }
			
			usort($employees, array($this, "sortByOrder"));
			
			// echo '<pre>';
			// print_r($employees);
			// exit;

			foreach ($employees as $ekey => $evalue) {
				$this->data['employees'][] = $evalue;
			}

			// echo '<pre>';
			// print_r($this->data['employees']);
			// exit;
			
			if(isset($this->data['employees']) && count($this->data['employees']) > 0){
				//$this->model_transaction_transaction->deleteorderhistory($filter_date_start);
				//$this->model_transaction_transaction->deleteorderhistory($next_date);
				foreach ($this->data['employees'] as $fkey => $employee) {
					$exist = $this->model_transaction_transaction->getorderhistory($employee);
					if($exist == 0){
						$mystring = $employee['in_time'];
						$findme   = ':';
						$pos = strpos($mystring, $findme);
						if($pos !== false){
							$in_times = substr($employee['in_time'], 0, 2). ":" .substr($employee['in_time'], 3, 2);
						} else {
							$in_times = substr($employee['in_time'], 0, 2). ":" .substr($employee['in_time'], 2, 2);
						}
						if($in_times != ':'){
							$in_time = date("h:i", strtotime($in_times));
						} else {
							$in_time = '';
						}
						$employee['in_time'] = $in_times;
						// echo '<pre>';
						// print_r($employee);
						// exit;
						$this->model_transaction_transaction->insert_attendance_data($employee);
					}
				}
			}
			//echo 'Done';exit;
			
			// $update1 = "UPDATE `oc_attendance` SET `status` = '0' WHERE punch_date = '".$filter_date_start."'";
			// $this->db->query($update1);
			// $update1 = "UPDATE `oc_attendance` SET `status` = '0' WHERE punch_date = '".$next_date."'";
			// $this->db->query($update1);
			
			// $update2 = "DELETE FROM `oc_transaction` WHERE `date` = '".$filter_date_start."'";
			// if($filter_unit){
			// 	$update2 .= " AND LOWER(`unit`) = '" . $this->db->escape(strtolower($filter_unit)) . "'";
			// }
			// $this->db->query($update2);
			
			// $update2 = "DELETE FROM `oc_transaction` WHERE `date` = '".$next_date."'";
			// if($filter_unit){
			// 	$update2 .= " AND LOWER(`unit`) = '" . $this->db->escape(strtolower($filter_unit)) . "'";
			// }
			// $this->db->query($update2);
			
			$is_exist = $this->model_report_common_report->getattendance_exist($filter_date_start);
			//if($is_exist == 1){
				$data = array();
				$data['unit'] = $filter_unit;
				$results = $this->model_report_common_report->getemployees($data);
				foreach ($results as $rkey => $rvalue) {
					$rvaluess = $this->model_transaction_transaction->getrawattendance_group_date_custom($rvalue['emp_code'], $filter_date_start);
					// echo 'Emp Id : ' . $rvalue['emp_code'];
					// echo '<br />';

					//echo "<pre>"; print_r($rvaluess); exit;
					if($rvaluess) {
						//$raw_attendance_datass = $this->model_transaction_transaction->getrawattendance_group();
						//if($raw_attendance_datass) {
						//foreach ($raw_attendance_datass as $rkeyss => $rvaluess) {
						$emp_data = $this->model_transaction_transaction->getempdata($rvaluess['emp_id']);
						if(isset($emp_data['name']) && $emp_data['name'] != ''){
							
							$emp_name = $emp_data['name'];
							$department = $emp_data['department'];
							$unit = $emp_data['unit'];
							$group = $emp_data['group'];

							$day_date = date('j', strtotime($filter_date_start));
							$day = date('j', strtotime($filter_date_start));
							$month = date('n', strtotime($filter_date_start));
							$year = date('Y', strtotime($filter_date_start));

							$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$rvaluess['emp_id']."' AND `month` = '".$month."' AND `year` = '".$year."' ";
							//$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `month` ='".$month."' AND `year` = '".$year."' AND `emp_code` = '".$rvaluess['emp_id']."' ";
							$shift_schedule = $this->db->query($update3)->row;
							$schedule_raw = explode('_', $shift_schedule[$day_date]);
							if(!isset($schedule_raw[2])){
								$schedule_raw[2] = 1;
							}
							if($schedule_raw[0] == 'S'){
								$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[1]);
								if(!isset($shift_data['shift_id'])){
									$shift_data = $this->model_transaction_transaction->getshiftdata('1');
								}
								if(isset($shift_data['shift_id'])){
									$abnormal_status = 0;
									$shift_intime = $shift_data['in_time'];
									$shift_outtime = $shift_data['out_time'];

									$act_intime = '00:00:00';
									$act_outtime = '00:00:00';
									$act_in_punch_date = $filter_date_start;
									$act_out_punch_date = $filter_date_start;
									
									$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$rvaluess['punch_date']."' ";
									$trans_exist = $this->db->query($trans_exist_sql);
									if($trans_exist->num_rows == 0){ 
										$act_intimes = $this->model_transaction_transaction->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
										if(isset($act_intimes['punch_time'])) {
											$act_intime = $act_intimes['punch_time'];
											$act_in_punch_date = $act_intimes['punch_date'];
										}
										$act_outtimes = $this->model_transaction_transaction->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
										if(isset($act_outtimes['punch_time'])) {
											$act_outtime = $act_outtimes['punch_time'];
											$act_out_punch_date = $act_outtimes['punch_date'];
										}
									} else {
										if($trans_exist->row['act_intime'] == '00:00:00'){
											$new_data_sql = "SELECT * FROM `oc_attendance` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `punch_date` = '".$rvaluess['punch_date']."' AND `status` = '0' ";
											$new_data_exist = $this->db->query($new_data_sql);
											if($new_data_exist->num_rows > 0){	
												$act_intimes = $this->model_transaction_transaction->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
												if(isset($act_intimes['punch_time'])) {
													$act_intime = $act_intimes['punch_time'];
													$act_in_punch_date = $act_intimes['punch_date'];
												}
											}	
										} else {
											$act_intime = $trans_exist->row['act_intime'];
											$act_in_punch_date = $trans_exist->row['date'];	
										}
									
										$new_data_sql = "SELECT * FROM `oc_attendance` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `punch_date` = '".$rvaluess['punch_date']."' AND `status` = '0' ";
										$new_data_exist = $this->db->query($new_data_sql);
										if($new_data_exist->num_rows > 0){
											$act_outtimes = $this->model_transaction_transaction->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
											if(isset($act_outtimes['punch_time'])) {
												$act_outtime = $act_outtimes['punch_time'];
												$act_out_punch_date = $act_outtimes['punch_date'];
											}
										} else {
											$act_outtime = $trans_exist->row['act_outtime'];
											$act_out_punch_date = $trans_exist->row['date_out'];
										}
									}
									$abs_stat = 0;
									if($act_intime == $act_outtime){
										$act_outtime = '00:00:00';
									}
									$first_half = 0;
									$second_half = 0;
									$late_time = '00:00:00';
									if($act_intime != '00:00:00'){
										$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
										$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
										if($since_start->h > 12){
											$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
											$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
										}
										$late_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
										if($since_start->invert == 1){
											$late_time = '00:00:00';
										}
										//echo 'in else'; exit;
										if($act_outtime != '00:00:00'){
											$first_half = 1;
											$second_half = 1;
										} else {
											$first_half = 0;
											$second_half = 0;
										}
									} else {
										$first_half = 0;
										$second_half = 0;
									}

									$working_time = '00:00:00';
									$early_time = '00:00:00';
									if($abnormal_status == 0){ //if abnormal status is zero calculate further 
										$early_time = '00:00:00';
										if($act_outtime != '00:00:00'){ //for getting early time i.e difference between shiftouttime and sctouttime 
											$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime);
											$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
											if($since_start->h > 12){
												$start_date = new DateTime($act_out_punch_date.' '.$shift_outtime);
												$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_outtime));
											}
											$early_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;					
											if($since_start->invert == 0){
												$early_time = '00:00:00';
											}
										}					
										$working_time = '00:00:00';
										if($act_outtime != '00:00:00'){//for getting working time
											$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
											$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
											$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
										}
									} else {
										$first_half = 0;
										$second_half = 0;
									}
									if($first_half == 1 && $second_half == 1){
										$present_status = 1;
										$absent_status = 0;
									} elseif($first_half == 1 && $second_half == 0){
										$present_status = 0.5;
										$absent_status = 0.5;
									} elseif($first_half == 0 && $second_half == 1){
										$present_status = 0.5;
										$absent_status = 0.5;
									} else {
										$present_status = 0;
										$absent_status = 1;
									}

									$day = date('j', strtotime($rvaluess['punch_date']));
									$month = date('n', strtotime($rvaluess['punch_date']));
									$year = date('Y', strtotime($rvaluess['punch_date']));
									//echo 'out';exit;
									if($trans_exist->num_rows == 0){
										if($abs_stat == 1){
											$act_intime = '00:00:00';
											$act_outtime = '00:00:00';
											$abnormal_status = 0;
											$act_out_punch_date = $filter_date_start;
											$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', halfday_status = '0', `compli_status` = '0' ";
										} else {
											$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', halfday_status = '0', `compli_status` = '0' ";
										}
										$this->db->query($sql);
										$transaction_id = $this->db->getLastId();
									} else {
										if($abs_stat == 1){
											$act_intime = '00:00:00';
											$act_outtime = '00:00:00';
											$abnormal_status = 0;
											$act_out_punch_date = $filter_date_start;
											$sql = "UPDATE `oc_transaction` SET `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', `weekly_off` = '0', `holiday_id` = '0', halfday_status = '0', `compli_status` = '0' WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ";
										} else {
											$sql = "UPDATE `oc_transaction` SET `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', `weekly_off` = '0', `holiday_id` = '0', halfday_status = '0', `compli_status` = '0' WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ";
										}
										$this->db->query($sql);
										$transaction_id = $this->db->query("SELECT `transaction_id` FROM `oc_transaction` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ")->row['transaction_id'];
									}
									// echo $sql.';';
									// echo '<br />';
									// exit;
									
								}
								//echo 'out1';exit;
							} elseif ($schedule_raw[0] == 'W') {
								$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[2]);
								if(!isset($shift_data['shift_id'])){
									$shift_data = $this->model_transaction_transaction->getshiftdata('1');
								}
								$shift_intime = $shift_data['in_time'];
								$shift_outtime = $shift_data['out_time'];

								$act_intime = '00:00:00';
								$act_outtime = '00:00:00';
								$act_in_punch_date = $filter_date_start;
								$act_out_punch_date = $filter_date_start;

								$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$rvaluess['punch_date']."' ";
								$trans_exist = $this->db->query($trans_exist_sql);
								if($trans_exist->num_rows == 0){ 
									$act_intimes = $this->model_transaction_transaction->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
									if(isset($act_intimes['punch_time'])) {
										$act_intime = $act_intimes['punch_time'];
										$act_in_punch_date = $act_intimes['punch_date'];
									}
									$act_outtimes = $this->model_transaction_transaction->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
									if(isset($act_outtimes['punch_time'])) {
										$act_outtime = $act_outtimes['punch_time'];
										$act_out_punch_date = $act_outtimes['punch_date'];
									}
								} else {
									if($trans_exist->row['act_intime'] == '00:00:00'){
										$new_data_sql = "SELECT * FROM `oc_attendance` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `punch_date` = '".$rvaluess['punch_date']."' AND `status` = '0' ";
										$new_data_exist = $this->db->query($new_data_sql);
										if($new_data_exist->num_rows > 0){	
											$act_intimes = $this->model_transaction_transaction->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
											if(isset($act_intimes['punch_time'])) {
												$act_intime = $act_intimes['punch_time'];
												$act_in_punch_date = $act_intimes['punch_date'];
											}
										}	
									} else {
										$act_intime = $trans_exist->row['act_intime'];
										$act_in_punch_date = $trans_exist->row['date'];	
									}
									
									$new_data_sql = "SELECT * FROM `oc_attendance` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `punch_date` = '".$rvaluess['punch_date']."' AND `status` = '0' ";
									$new_data_exist = $this->db->query($new_data_sql);
									if($new_data_exist->num_rows > 0){
										$act_outtimes = $this->model_transaction_transaction->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
										if(isset($act_outtimes['punch_time'])) {
											$act_outtime = $act_outtimes['punch_time'];
											$act_out_punch_date = $act_outtimes['punch_date'];
										}
									} else {
										$act_outtime = $trans_exist->row['act_outtime'];
										$act_out_punch_date = $trans_exist->row['date_out'];
									}
								}
								$abnormal_status = 0;

								$abs_stat = 0;
								if($filter_date_start != $act_in_punch_date){
									$abs_stat = 1;
									$act_in_punch_date = $filter_date_start;
								}

								// if($act_intime == '00:00:00' || $act_outtime == '00:00:00'){
								// 	$abnormal_status = 1;
								// }
								
								if($act_intime == $act_outtime){
									$act_outtime = '00:00:00';
								}

								if($trans_exist->num_rows == 0){
									if($abs_stat == 1){
										$act_intime = '00:00:00';
										$act_outtime = '00:00:00';
										$abnormal_status = 0;
										$act_out_punch_date = $filter_date_start;
										$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '".$abnormal_status."', halfday_status = '0', compli_status = '0' ";
									} else {
										$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '".$abnormal_status."', halfday_status = '0', compli_status = '0' ";
									}
									$this->db->query($sql);
									$transaction_id = $this->db->getLastId();
								} else {
									if($abs_stat == 1){
										$act_intime = '00:00:00';
										$act_outtime = '00:00:00';
										$abnormal_status = 0;
										$act_out_punch_date = $filter_date_start;
										$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', abnormal_status = '".$abnormal_status."', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', compli_status = '0', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', halfday_status = '0' WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ";
									} else {
										$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', abnormal_status = '".$abnormal_status."', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', compli_status = '0', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', halfday_status = '0' WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ";
									}
									$this->db->query($sql);
									$transaction_id = $this->db->query("SELECT `transaction_id` FROM `oc_transaction` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ")->row['transaction_id'];
								}
								//echo $sql.';';
								//echo '<br />';
								
							} elseif ($schedule_raw[0] == 'H') {
								$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[2]);
								if(!isset($shift_data['shift_id'])){
									$shift_data = $this->model_transaction_transaction->getshiftdata('1');
								}
								$shift_intime = $shift_data['in_time'];
								$shift_outtime = $shift_data['out_time'];

								$act_intime = '00:00:00';
								$act_outtime = '00:00:00';
								$act_in_punch_date = $filter_date_start;
								$act_out_punch_date = $filter_date_start;
								
								$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$rvaluess['punch_date']."' ";
								$trans_exist = $this->db->query($trans_exist_sql);
								if($trans_exist->num_rows == 0){ 
									$act_intimes = $this->model_transaction_transaction->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
									if(isset($act_intimes['punch_time'])) {
										$act_intime = $act_intimes['punch_time'];
										$act_in_punch_date = $act_intimes['punch_date'];
									}
									$act_outtimes = $this->model_transaction_transaction->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
									if(isset($act_outtimes['punch_time'])) {
										$act_outtime = $act_outtimes['punch_time'];
										$act_out_punch_date = $act_outtimes['punch_date'];
									}
								} else {
									if($trans_exist->row['act_intime'] == '00:00:00'){
										$new_data_sql = "SELECT * FROM `oc_attendance` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `punch_date` = '".$rvaluess['punch_date']."' AND `status` = '0' ";
										$new_data_exist = $this->db->query($new_data_sql);
										if($new_data_exist->num_rows > 0){	
											$act_intimes = $this->model_transaction_transaction->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
											if(isset($act_intimes['punch_time'])) {
												$act_intime = $act_intimes['punch_time'];
												$act_in_punch_date = $act_intimes['punch_date'];
											}
										}	
									} else {
										$act_intime = $trans_exist->row['act_intime'];
										$act_in_punch_date = $trans_exist->row['date'];	
									}


									$new_data_sql = "SELECT * FROM `oc_attendance` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `punch_date` = '".$rvaluess['punch_date']."' AND `status` = '0' ";
									$new_data_exist = $this->db->query($new_data_sql);
									if($new_data_exist->num_rows > 0){
										$act_outtimes = $this->model_transaction_transaction->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
										if(isset($act_outtimes['punch_time'])) {
											$act_outtime = $act_outtimes['punch_time'];
											$act_out_punch_date = $act_outtimes['punch_date'];
										}
									} else {
										$act_outtime = $trans_exist->row['act_outtime'];
										$act_out_punch_date = $trans_exist->row['date_out'];
									}
								}

								$abnormal_status = 0;
								
								$abs_stat = 0;
								if($filter_date_start != $act_in_punch_date){
									$abs_stat = 1;
									$act_in_punch_date = $filter_date_start;
								}

								// if($act_intime == '00:00:00' || $act_outtime == '00:00:00'){
								// 	$abnormal_status = 1;
								// }
								
								if($act_intime == $act_outtime){
									$act_outtime = '00:00:00';
								}

								if($trans_exist->num_rows == 0){
									if($abs_stat == 1){
										$act_intime = '00:00:00';
										$act_outtime = '00:00:00';
										$abnormal_status = 0;
										$act_out_punch_date = $filter_date_start;
										$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `weekly_off` = '0', `holiday_id` = '".$schedule_raw[1]."', `present_status` = '0', `absent_status` = '0', `abnormal_status` = '".$abnormal_status."', `halfday_status` = '0', `compli_status` = '0' ";
									} else {
										$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `weekly_off` = '0', `holiday_id` = '".$schedule_raw[1]."', `present_status` = '0', `absent_status` = '0', `abnormal_status` = '".$abnormal_status."', `halfday_status` = '0', `compli_status` = '0' ";
									}
									$this->db->query($sql);
									$transaction_id = $this->db->getLastId();
								} else {
									if($abs_stat == 1){
										$act_intime = '00:00:00';
										$act_outtime = '00:00:00';
										$abnormal_status = 0;
										$act_out_punch_date = $filter_date_start;
										$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', abnormal_status = '".$abnormal_status."', `weekly_off` = '0', `present_status` = '0', `absent_status` = '0', `halfday_status` = '0', `compli_status` = '0', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `holiday_id` = '".$schedule_raw[1]."' WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ";
									} else {
										$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', abnormal_status = '".$abnormal_status."', `weekly_off` = '0', `present_status` = '0', `absent_status` = '0', `halfday_status` = '0', `compli_status` = '0', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `holiday_id` = '".$schedule_raw[1]."' WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ";
									}
									$this->db->query($sql);
									$transaction_id = $this->db->query("SELECT `transaction_id` FROM `oc_transaction` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ")->row['transaction_id'];
								}
								//echo $sql.';';
								//echo '<br />';
							} elseif ($schedule_raw[0] == 'HD') {
								$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[2]);
								if(!isset($shift_data['shift_id'])){
									$shift_data = $this->model_transaction_transaction->getshiftdata('1');
								}
								$shift_intime = $shift_data['in_time'];
								$shift_outtime = Date('H:i:s', strtotime($shift_intime .' +4 hours'));
								//$shift_outtime = $shift_data['out_time'];

								$act_intime = '00:00:00';
								$act_outtime = '00:00:00';
								$act_in_punch_date = $filter_date_start;
								$act_out_punch_date = $filter_date_start;
								
								$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$rvaluess['punch_date']."' ";
								$trans_exist = $this->db->query($trans_exist_sql);
								if($trans_exist->num_rows == 0){ 
									$act_intimes = $this->model_transaction_transaction->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
									if(isset($act_intimes['punch_time'])) {
										$act_intime = $act_intimes['punch_time'];
										$act_in_punch_date = $act_intimes['punch_date'];
									}
									$act_outtimes = $this->model_transaction_transaction->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
									if(isset($act_outtimes['punch_time'])) {
										$act_outtime = $act_outtimes['punch_time'];
										$act_out_punch_date = $act_outtimes['punch_date'];
									}
								} else {
									if($trans_exist->row['act_intime'] == '00:00:00'){
										$new_data_sql = "SELECT * FROM `oc_attendance` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `punch_date` = '".$rvaluess['punch_date']."' AND `status` = '0' ";
										$new_data_exist = $this->db->query($new_data_sql);
										if($new_data_exist->num_rows > 0){	
											$act_intimes = $this->model_transaction_transaction->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
											if(isset($act_intimes['punch_time'])) {
												$act_intime = $act_intimes['punch_time'];
												$act_in_punch_date = $act_intimes['punch_date'];
											}
										}	
									} else {
										$act_intime = $trans_exist->row['act_intime'];
										$act_in_punch_date = $trans_exist->row['date'];	
									}
									
									
									$new_data_sql = "SELECT * FROM `oc_attendance` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `punch_date` = '".$rvaluess['punch_date']."' AND `status` = '0' ";
									$new_data_exist = $this->db->query($new_data_sql);
									if($new_data_exist->num_rows > 0){
										$act_outtimes = $this->model_transaction_transaction->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
										if(isset($act_outtimes['punch_time'])) {
											$act_outtime = $act_outtimes['punch_time'];
											$act_out_punch_date = $act_outtimes['punch_date'];
										}
									} else {
										$act_outtime = $trans_exist->row['act_outtime'];
										$act_out_punch_date = $trans_exist->row['date_out'];
									}
								}

								$abnormal_status = 0;
								$abs_stat = 0;
								if($act_intime == $act_outtime){
									$act_outtime = '00:00:00';
								}
								$first_half = 0;
								$second_half = 0;
								$late_time = '00:00:00';
								
								if($act_intime != '00:00:00'){
									$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
									$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_intime));
									if($since_start->h > 12){
										$start_date = new DateTime($act_in_punch_date.' '.$shift_intime);
										$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_intime));
									}
									$late_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
									if($since_start->invert == 1){
										$late_time = '00:00:00';
									}
									if($act_outtime != '00:00:00'){
										$first_half = 1;
										$second_half = 'HD';
									} else {
										$first_half = 0;
										$second_half = 0;
									}
								} else {
									$first_half = 0;
									$second_half = 0;
								}

								$working_time = '00:00:00';
								$early_time = '00:00:00';
								if($abnormal_status == 0){ //if abnormal status is zero calculate further 
									$early_time = '00:00:00';
									if($act_outtime != '00:00:00'){ //for getting early time i.e difference between shiftouttime and sctouttime 
										$start_date = new DateTime($act_in_punch_date.' '.$shift_outtime);
										$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
										if($since_start->h > 12){
											$start_date = new DateTime($act_out_punch_date.' '.$shift_outtime);
											$since_start = $start_date->diff(new DateTime($act_in_punch_date.' '.$act_outtime));
										}
										$early_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;					
										if($since_start->invert == 0){
											$early_time = '00:00:00';
										}
									}					
									
									$working_time = '00:00:00';
									if($act_outtime != '00:00:00'){//for getting working time
										$start_date = new DateTime($act_in_punch_date.' '.$act_intime);
										$since_start = $start_date->diff(new DateTime($act_out_punch_date.' '.$act_outtime));
										$working_time = $since_start->h.':'.$since_start->i.':'.$since_start->s;
									}
								} else {
									$first_half = 0;
									$second_half = 0;
								}
								
								if($first_half == 1 && $second_half == 'HD'){
									$present_status = 1;
									$absent_status = 0;
								} else {
									$present_status = 0;
									$absent_status = 1;
								}

								if($trans_exist->num_rows == 0){
									if($abs_stat == 1){
										$act_intime = '00:00:00';
										$act_outtime = '00:00:00';
										$abnormal_status = 0;
										$act_out_punch_date = $filter_date_start;
										$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', `halfday_status` = '".$schedule_raw[1]."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."' ";
									} else {
										$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', `halfday_status` = '".$schedule_raw[1]."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."' ";
									}
									$this->db->query($sql);
									$transaction_id = $this->db->getLastId();
								} else {
									if($abs_stat == 1){
										$act_intime = '00:00:00';
										$act_outtime = '00:00:00';
										$abnormal_status = 0;
										$act_out_punch_date = $filter_date_start;
										$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', `halfday_status` = '".$schedule_raw[1]."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."' WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ";
									} else {
										$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `firsthalf_status` = '".$first_half."', `secondhalf_status` = '".$second_half."', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '".$present_status."', `absent_status` = '".$absent_status."', abnormal_status = '".$abnormal_status."', `halfday_status` = '".$schedule_raw[1]."', `late_time` = '".$late_time."', `early_time` = '".$early_time."', `working_time` = '".$working_time."' WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ";
									}
									$this->db->query($sql);
									$transaction_id = $this->db->query("SELECT `transaction_id` FROM `oc_transaction` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ")->row['transaction_id'];
								}
								//echo $sql.';';
								//echo '<br />';
							}  elseif ($schedule_raw[0] == 'C') {
								$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[2]);
								if(!isset($shift_data['shift_id'])){
									$shift_data = $this->model_transaction_transaction->getshiftdata('1');
								}
								$shift_intime = $shift_data['in_time'];
								$shift_outtime = $shift_data['out_time'];

								$act_intime = '00:00:00';
								$act_outtime = '00:00:00';
								$act_in_punch_date = $filter_date_start;
								$act_out_punch_date = $filter_date_start;
								
								$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$rvaluess['punch_date']."' ";
								$trans_exist = $this->db->query($trans_exist_sql);
								if($trans_exist->num_rows == 0){ 
									$act_intimes = $this->model_transaction_transaction->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
									if(isset($act_intimes['punch_time'])) {
										$act_intime = $act_intimes['punch_time'];
										$act_in_punch_date = $act_intimes['punch_date'];
									}
									$act_outtimes = $this->model_transaction_transaction->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
									if(isset($act_outtimes['punch_time'])) {
										$act_outtime = $act_outtimes['punch_time'];
										$act_out_punch_date = $act_outtimes['punch_date'];
									}
								} else {
									if($trans_exist->row['act_intime'] == '00:00:00'){
										$new_data_sql = "SELECT * FROM `oc_attendance` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `punch_date` = '".$rvaluess['punch_date']."' AND `status` = '0' ";
										$new_data_exist = $this->db->query($new_data_sql);
										if($new_data_exist->num_rows > 0){	
											$act_intimes = $this->model_transaction_transaction->getrawattendance_in_time($rvaluess['emp_id'], $rvaluess['punch_date']);
											if(isset($act_intimes['punch_time'])) {
												$act_intime = $act_intimes['punch_time'];
												$act_in_punch_date = $act_intimes['punch_date'];
											}
										}	
									} else {
										$act_intime = $trans_exist->row['act_intime'];
										$act_in_punch_date = $trans_exist->row['date'];	
									}
									
									$new_data_sql = "SELECT * FROM `oc_attendance` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `punch_date` = '".$rvaluess['punch_date']."' AND `status` = '0' ";
									$new_data_exist = $this->db->query($new_data_sql);
									if($new_data_exist->num_rows > 0){
										$act_outtimes = $this->model_transaction_transaction->getrawattendance_out_time($rvaluess['emp_id'], $rvaluess['punch_date'], $act_intime, $act_in_punch_date);
										if(isset($act_outtimes['punch_time'])) {
											$act_outtime = $act_outtimes['punch_time'];
											$act_out_punch_date = $act_outtimes['punch_date'];
										}
									} else {
										$act_outtime = $trans_exist->row['act_outtime'];
										$act_out_punch_date = $trans_exist->row['date_out'];
									}
								}

								$abnormal_status = 0;
								
								$abs_stat = 0;
								if($filter_date_start != $act_in_punch_date){
									$abs_stat = 1;
									$act_in_punch_date = $filter_date_start;
								}

								// if($act_intime == '00:00:00' || $act_outtime == '00:00:00'){
								// 	$abnormal_status = 1;
								// }
								
								if($act_intime == $act_outtime){
									$act_outtime = '00:00:00';
								}

								if($trans_exist->num_rows == 0){
									if($abs_stat == 1){
										$act_intime = '00:00:00';
										$act_outtime = '00:00:00';
										$abnormal_status = 0;
										$act_out_punch_date = $filter_date_start;
										$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'COF', `secondhalf_status` = 'COF', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '".$abnormal_status."', `halfday_status` = '0', `compli_status` = '".$schedule_raw[1]."' ";
									} else {
										$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvaluess['emp_id']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$act_in_punch_date."', `date_out` = '".$act_out_punch_date."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'COF', `secondhalf_status` = 'COF', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '".$abnormal_status."', `halfday_status` = '0', `compli_status` = '".$schedule_raw[1]."' ";
									}
									$this->db->query($sql);
									$transaction_id = $this->db->getLastId();
								} else {
									if($abs_stat == 1){
										$act_intime = '00:00:00';
										$act_outtime = '00:00:00';
										$abnormal_status = 0;
										$act_out_punch_date = $filter_date_start;
										$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `firsthalf_status` = 'COF', `secondhalf_status` = 'COF', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '".$abnormal_status."', `halfday_status` = '0', `compli_status` = '".$schedule_raw[1]."' WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ";
									} else {
										$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `act_intime` = '".$act_intime."', `date` = '".$act_in_punch_date."', `act_outtime` = '".$act_outtime."', `date_out` = '".$act_out_punch_date."', `firsthalf_status` = 'COF', `secondhalf_status` = 'COF', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '".$abnormal_status."', `halfday_status` = '0', `compli_status` = '".$schedule_raw[1]."' WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ";	
									}
									$this->db->query($sql);
									$transaction_id = $this->db->query("SELECT `transaction_id` FROM `oc_transaction` WHERE `emp_id` = '".$rvaluess['emp_id']."' AND `date` = '".$act_in_punch_date."' ")->row['transaction_id'];
								}
								//echo $sql.';';
								//echo '<br />';
							}
							if($act_in_punch_date == $act_out_punch_date) {
								if($act_intime != '00:00:00' && $act_outtime != '00:00:00'){
									$update = "UPDATE `oc_attendance` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_in_punch_date."' AND `punch_time` >= '".$act_intime."' AND `punch_time` <= '".$act_outtime."' AND `emp_id` = '".$rvaluess['emp_id']."' ";
									//echo $update;exit;
									$this->db->query($update);
									$this->log->write($update);
								} elseif($act_intime != '00:00:00' && $act_outtime == '00:00:00') {
									$update = "UPDATE `oc_attendance` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_in_punch_date."' AND `punch_time` = '".$act_intime."' AND `emp_id` = '".$rvaluess['emp_id']."' ";
									//echo $update;exit;
									$this->db->query($update);
									$this->log->write($update);
								} elseif($act_intime == '00:00:00' && $act_outtime != '00:00:00') {
									$update = "UPDATE `oc_attendance` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_out_punch_date."' AND `punch_time` = '".$act_outtime."' AND `emp_id` = '".$rvaluess['emp_id']."' ";
									//echo $update;exit;
									$this->db->query($update);
									$this->log->write($update);
								}
							} else {
								$update = "UPDATE `oc_attendance` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_in_punch_date."' AND `punch_time` >= '".$act_intime."' AND `emp_id` = '".$rvaluess['emp_id']."' ";
								$this->db->query($update);
								$this->log->write($update);

								$update = "UPDATE `oc_attendance` SET `status` = '1', `transaction_id` = '".$transaction_id."' WHERE `punch_date` = '".$act_out_punch_date."' AND `punch_time` <= '".$act_outtime."' AND `emp_id` = '".$rvaluess['emp_id']."' ";
								$this->db->query($update);
								$this->log->write($update);
							}
						}
					} else {
						$emp_data = $this->model_transaction_transaction->getempdata($rvalue['emp_code']);
						if(isset($emp_data['name']) && $emp_data['name'] != ''){
							$emp_name = $emp_data['name'];
							$department = $emp_data['department'];
							$unit = $emp_data['unit'];
							$group = $emp_data['group'];

							$day_date = date('j', strtotime($filter_date_start));
							$month = date('n', strtotime($filter_date_start));
							$year = date('Y', strtotime($filter_date_start));

							$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$rvalue['emp_code']."' AND `month` = '".$month."' AND `year` = '".$year."' ";
							//$update3 = "SELECT  `".$day_date."` FROM `oc_shift_schedule` WHERE `month` ='".$month."' AND `year` = '".$year."' AND `emp_code` = '".$rvalue['emp_code']."' ";
							$shift_schedule = $this->db->query($update3)->row;
							$schedule_raw = explode('_', $shift_schedule[$day_date]);
							if($schedule_raw[0] == 'S'){
								$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[1]);
								if(isset($shift_data['shift_id'])){
									$shift_intime = $shift_data['in_time'];
									$shift_outtime = $shift_data['out_time'];
									
									$day = date('j', strtotime($filter_date_start));
									$month = date('n', strtotime($filter_date_start));
									$year = date('Y', strtotime($filter_date_start));

									$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
									$trans_exist = $this->db->query($trans_exist_sql);
									if($trans_exist->num_rows == 0){
										$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0' ";
										//echo $sql.';';
										//echo '<br />';
										$this->db->query($sql);
									} else {
										$sql = "UPDATE `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0' WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."'";
										//echo $sql.';';
										//echo '<br />';
										$this->db->query($sql);
									}
								} else {
									$shift_data = $this->model_transaction_transaction->getshiftdata('1');
									$shift_intime = $shift_data['in_time'];
									$shift_outtime = $shift_data['out_time'];
									
									$day = date('j', strtotime($filter_date_start));
									$month = date('n', strtotime($filter_date_start));
									$year = date('Y', strtotime($filter_date_start));

									$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
									$trans_exist = $this->db->query($trans_exist_sql);
									if($trans_exist->num_rows == 0){
										$sql = "INSERT INTO `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0' ";
										//echo $sql.';';
										//echo '<br />';
										$this->db->query($sql);
									} else {
										$sql = "UPDATE `oc_transaction` SET `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `day` = '".$day."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '0' WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
										//echo $sql.';';
										//echo '<br />';
										$this->db->query($sql);
									}
								}
							} elseif ($schedule_raw[0] == 'W') {
								$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[2]);
								if(!isset($shift_data['shift_id'])){
									$shift_data = $this->model_transaction_transaction->getshiftdata('1');
								}
								$shift_intime = $shift_data['in_time'];
								$shift_outtime = $shift_data['out_time'];

								$act_intime = '00:00:00';
								$act_outtime = '00:00:00';
								$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
								$trans_exist = $this->db->query($trans_exist_sql);
								
								if($trans_exist->num_rows == 0){
									$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0' ";
									//echo $sql.';';
									//echo '<br />';
									$this->db->query($sql);
								} else {
									$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'WO', `secondhalf_status` = 'WO', `weekly_off` = '".$schedule_raw[1]."', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0' WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
									//echo $sql.';';
									//echo '<br />';
									$this->db->query($sql);
								}
							} elseif ($schedule_raw[0] == 'H') {
								$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[2]);
								if(!isset($shift_data['shift_id'])){
									$shift_data = $this->model_transaction_transaction->getshiftdata('1');
								}
								$shift_intime = $shift_data['in_time'];
								$shift_outtime = $shift_data['out_time'];

								$act_intime = '00:00:00';
								$act_outtime = '00:00:00';
								
								$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
								$trans_exist = $this->db->query($trans_exist_sql);
								if($trans_exist->num_rows == 0){
									$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `weekly_off` = '0', `holiday_id` = '".$schedule_raw[1]."', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0'  ";
									//echo $sql.';';
									//echo '<br />';
									$this->db->query($sql);
								} else {
									$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'HLD', `secondhalf_status` = 'HLD', `weekly_off` = '0', `holiday_id` = '".$schedule_raw[1]."', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0' WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
									//echo $sql.';';
									//echo '<br />';
									$this->db->query($sql);
								}
							} elseif ($schedule_raw[0] == 'HD') {
								$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[2]);
								if(!isset($shift_data['shift_id'])){
									$shift_data = $this->model_transaction_transaction->getshiftdata('1');
								}
								$shift_intime = $shift_data['in_time'];
								$shift_outtime = Date('H:i:s', strtotime($shift_intime .' +4 hours'));
								//$shift_outtime = $shift_data['out_time'];

								$act_intime = '00:00:00';
								$act_outtime = '00:00:00';
								
								$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
								$trans_exist = $this->db->query($trans_exist_sql);
								if($trans_exist->num_rows == 0){
									$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '".$schedule_raw[1]."' ";
									//echo $sql.';';
									//echo '<br />';
									$this->db->query($sql);
								} else {
									$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = '0', `secondhalf_status` = '0', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '1', abnormal_status = '0', halfday_status = '".$schedule_raw[1]."' WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
									//echo $sql.';';
									//echo '<br />';
									$this->db->query($sql);
								}
							}  elseif ($schedule_raw[0] == 'C') {
								$shift_data = $this->model_transaction_transaction->getshiftdata($schedule_raw[2]);
								if(!isset($shift_data['shift_id'])){
									$shift_data = $this->model_transaction_transaction->getshiftdata('1');
								}
								$shift_intime = $shift_data['in_time'];
								$shift_outtime = $shift_data['out_time'];

								$act_intime = '00:00:00';
								$act_outtime = '00:00:00';
								
								$trans_exist_sql = "SELECT * FROM `oc_transaction` WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
								$trans_exist = $this->db->query($trans_exist_sql);
								if($trans_exist->num_rows == 0){
									$sql = "INSERT INTO `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'COF', `secondhalf_status` = 'COF', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', compli_status = '".$schedule_raw[1]."' ";
									//echo $sql.';';
									//echo '<br />';
									$this->db->query($sql);
								} else {
									$sql = "UPDATE `oc_transaction` SET `shift_intime` = '".$shift_intime."', `shift_outtime` = '".$shift_outtime."', `emp_id` = '".$rvalue['emp_code']."', `emp_name` = '".$emp_name."', `act_intime` = '".$act_intime."', `act_outtime` = '".$act_outtime."', `day` = '".$day_date."', `month` = '".$month."', `year` = '".$year."', `date` = '".$filter_date_start."', `date_out` = '".$filter_date_start."', `department` = '".$department."', `unit` = '".$unit."', `group` = '".$group."', `firsthalf_status` = 'COF', `secondhalf_status` = 'COF', `weekly_off` = '0', `holiday_id` = '0', `present_status` = '0', `absent_status` = '0', abnormal_status = '0', halfday_status = '0', compli_status = '".$schedule_raw[1]."' WHERE `emp_id` = '".$rvalue['emp_code']."' AND `date` = '".$filter_date_start."' ";
									//echo $sql.';';
									//echo '<br />';
									$this->db->query($sql);
								}
							}
							// $update = "UPDATE `oc_attendance` SET `status` = '1' WHERE `punch_date` = '".$filter_date_start."' AND `punch_time` >= '".$act_intime."' AND `punch_time` <= '".$act_outtime."' AND `emp_id` = '".$rvalue['emp_code']."' ";
							// $this->db->query($update);
							// $this->log->write($update);
						}	
					}
				}
				
				// $update = "UPDATE `oc_attendance` SET `status` = '1' WHERE `punch_date` = '".$filter_date_start."'";
				// $this->db->query($update);
				// $update = "UPDATE `oc_attendance` SET `status` = '1' WHERE `punch_date` = '".$next_date."'";
				// $this->db->query($update);
				
				$this->db->query("commit;");

				//echo 'out';exit;
				//echo 'Done';exit;
				$this->session->data['success'] = 'Transaction Generated';
				if(isset($this->request->get['home'])){
					$this->redirect($this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'));
				} elseif(isset($this->request->get['transaction'])){
					$url = '&filter_date_start='.$filter_date_start;
					if($filter_unit){
						$url .= '&unit='.$filter_unit;
					}
					$this->redirect($this->url->link('transaction/attendance', 'token=' . $this->session->data['token'].$url, 'SSL'));
				} else {
					$url = '&filter_date_start='.$filter_date_start;
					if($filter_unit){
						$url .= '&unit='.$filter_unit;
					}
					$this->redirect($this->url->link('report/attendance', 'token=' . $this->session->data['token'].$url, 'SSL'));
				}
			// } else {

			// 	$this->db->query("commit;");

			// 	$this->session->data['warning'] = 'No Data available';
			// 	if(isset($this->request->get['home'])){
			// 		$this->redirect($this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'));
			// 	} elseif(isset($this->request->get['transaction'])){
			// 		$url = '&filter_date_start='.$filter_date_start;
			// 		if($filter_unit){
			// 			$url .= '&unit='.$filter_unit;
			// 		}
			// 		$this->redirect($this->url->link('report/attendance', 'token=' . $this->session->data['token'].$url, 'SSL'));
			// 	} else {
			// 		$url = '&filter_date_start='.$filter_date_start;
			// 		if($filter_unit){
			// 			$url .= '&unit='.$filter_unit;
			// 		}
			// 		$this->redirect($this->url->link('report/attendance', 'token=' . $this->session->data['token'].$url, 'SSL'));
			// 	}
			// 	//$this->redirect($this->url->link('report/today', 'token=' . $this->session->data['token'], 'SSL'));
			// }
		} else {
			$this->db->query("commit;");

			$this->session->data['warning'] = 'Day already Closed';
			if(isset($this->request->get['home'])){
				$this->redirect($this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'));
			}  elseif(isset($this->request->get['transaction'])){
				$url = '&filter_date_start='.$filter_date_start;
				if($filter_unit){
					$url .= '&unit='.$filter_unit;
				}
				$this->redirect($this->url->link('report/attendance', 'token=' . $this->session->data['token'].$url, 'SSL'));
			} else {
				$url = '&filter_date_start='.$filter_date_start;
				$this->redirect($this->url->link('report/attendance', 'token=' . $this->session->data['token'].$url, 'SSL'));
			}
		}
	}
}
?>