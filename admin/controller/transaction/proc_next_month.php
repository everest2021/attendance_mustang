<?php    
class ControllerTransactionProcNextMonth extends Controller { 
	private $error = array();

	public function index() {
		$this->language->load('transaction/proc_next_month');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('transaction/transaction');
		$this->load->model('report/attendance');

		$this->getForm();
	}

	protected function getForm() {
		$filter_date = $this->model_report_attendance->getNextDate_1();

		if (isset($this->request->get['filter_month'])) {
			$filter_month = date('n', strtotime($filter_date));
			//$filter_month = $this->request->get['filter_month'];
		} else {
			$filter_month = date('n', strtotime($filter_date));
		}

		if (isset($this->request->get['filter_year'])) {
			//$filter_year = $this->request->get['filter_year'];
			$filter_year = date('Y', strtotime($filter_date));
		} else {
			$filter_year = date('Y', strtotime($filter_date));
		}

	    $url = '';
	    
	    if (isset($this->request->get['filter_month'])) {
			$url .= '&filter_month=' . $this->request->get['filter_month'];
		}

		if (isset($this->request->get['filter_year'])) {
			$url .= '&filter_year=' . $this->request->get['filter_year'];
		}  
	    
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['tab_general'] = $this->language->get('tab_general');

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('transaction/proc_next_month', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$months = array(
			'1' => 'January',
			'2' => 'Feburary',
			'3' => 'March',
			'4' => 'April',
			'5' => 'May',
			'6' => 'June',
			'7' => 'July',
			'8' => 'August',
			'9' => 'September',
			'10' => 'October',
			'11' => 'November',
			'12' => 'December'
		);

		foreach ($months as $key => $value) {
			if($key > $filter_month){
				unset($months[$key]);
			}
		}

		$this->data['months'] = $months;

		
		if (isset($this->session->data['warning'])) {
			$this->data['error_warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		$this->data['token'] = $this->session->data['token'];

		$this->data['filter_month'] = $filter_month;
		$this->data['filter_year'] = $filter_year;
		
		
		$this->template = 'transaction/proc_next_month.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}  

	protected function validateForm() {
		$this->load->model('transaction/transaction');

		if (!$this->user->hasPermission('modify', 'transaction/proc_next_month')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	public function process(){
		$months = array(
			'1' => 'January',
			'2' => 'Feburary',
			'3' => 'March',
			'4' => 'April',
			'5' => 'May',
			'6' => 'June',
			'7' => 'July',
			'8' => 'August',
			'9' => 'September',
			'10' => 'October',
			'11' => 'November',
			'12' => 'December'
		);
		if(isset($this->request->get['filter_month']) && isset($this->request->get['filter_year'])){
			$this->load->model('transaction/transaction');
			$filter_months = $this->request->get['filter_month'];
			$filter_years = $this->request->get['filter_year'];
			
			$insert = "UPDATE `oc_transaction` SET `month_close_status` = '1' WHERE `month` = '".$this->request->get['filter_month']."' AND `year` = '".$this->request->get['filter_year']."' ";	
			$this->db->query($insert);
			
			//echo $insert;
			//exit;
			// if($filter_months == 1){
			// 	$filter_month = 12;
			// } else {
			// 	$filter_month = $filter_months - 1;
			// }
			// if($filter_months == 1){
			// 	$filter_year = $filter_years - 1;
			// } else {
			// 	$filter_year = $filter_years;
			// }
			//$is_exist = $this->model_transaction_transaction->getexistdata($this->request->get['filter_month'], $this->request->get['filter_year']);
			// echo '<pre>';
			// print_r($is_exist);
			// exit;
			//if(!$is_exist){
			// $results = $this->model_transaction_transaction->getprevmonthtransdata($filter_month, $filter_year);
			// foreach ($results as $rkey => $rvalue) {
			// 	$is_enabled = $this->model_transaction_transaction->getemployee_exist($rvalue['emp_code']);
			// 	if($is_enabled){
			// 		$shift_sel = 'S_1';
			// 		foreach($rvalue as $rkey => $rval){
			// 			$exp = explode('_', $rval);
			// 			if($exp[0] == 'S'){
			// 				$shift_sel = $rval;
			// 			}
			// 		}
			// 		$update = "UPDATE `oc_shift_schedule` SET `emp_code` = '".$rvalue['emp_code']."', `1` = '".$shift_sel."', `2` = '".$shift_sel."', `3` = '".$shift_sel."', `4` = '".$shift_sel."', `5` = '".$shift_sel."', `6` = '".$shift_sel."', `7` = '".$shift_sel."', `8` = '".$shift_sel."', `9` = '".$shift_sel."', `10` = '".$shift_sel."', `11` = '".$shift_sel."', `12` = '".$shift_sel."', `13` = '".$shift_sel."', `14` = '".$shift_sel."', `15` = '".$shift_sel."', `16` = '".$shift_sel."', `17` = '".$shift_sel."', `18` = '".$shift_sel."', `19` = '".$shift_sel."', `20` = '".$shift_sel."', `21` = '".$shift_sel."', `22` = '".$shift_sel."', `23` = '".$shift_sel."', `24` = '".$shift_sel."', `25` = '".$shift_sel."', `26` = '".$shift_sel."', `27` = '".$shift_sel."', `28` = '".$shift_sel."', `29` = '".$shift_sel."', `30` = '".$shift_sel."', `31` = '".$shift_sel."' WHERE `emp_code` = '".$rvalue['emp_code']."' ";	
			// 		$this->db->query($update);
			// 		// echo $update;
			// 		// echo '<br />';
			// 	}
			// }
			//exit;
			$this->session->data['success'] = 'You have Succesfully Closed '.$months[$filter_months].' month';
			$this->redirect($this->url->link('transaction/proc_next_month', 'token=' . $this->session->data['token'].'&filter_month='.$this->request->get['filter_month'].'&filter_year='.$this->request->get['filter_year'], 'SSL'));
			//} else {
				//$this->session->data['warning'] = 'Records For the Month And Year Already Exist';
				//$this->redirect($this->url->link('transaction/proc_next_month', 'token=' . $this->session->data['token'], 'SSL'));
			//}
		} else {
			$this->session->data['warning'] = 'No Filter Selected';
			$this->redirect($this->url->link('transaction/proc_next_month', 'token=' . $this->session->data['token'], 'SSL'));
		}
	}
}
?>
