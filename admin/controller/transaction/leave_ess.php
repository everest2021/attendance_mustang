<?php    
class ControllerTransactionLeaveEss extends Controller { 
	private $error = array();

	public function index() {

		if(isset($this->session->data['emp_code'])){
			$emp_code = $this->session->data['emp_code'];
			$is_set = $this->db->query("SELECT `is_set` FROM `oc_employee` WHERE `emp_code` = '".$emp_code."' ")->row['is_set'];	
			if($is_set == '1'){
				//$this->redirect($this->url->link('transaction/leave_ess', 'token=' . $this->session->data['token'], 'SSL'));
			} else {
				$this->redirect($this->url->link('user/password_change', 'token=' . $this->session->data['token'].'&emp_code='.$emp_code, 'SSL'));
			}
		} elseif(isset($this->session->data['is_dept'])){
			$emp_code = $this->session->data['d_emp_id'];
			$is_set = $this->db->query("SELECT `is_set` FROM `oc_employee` WHERE `emp_code` = '".$emp_code."' ")->row['is_set'];	
			if($is_set == '1'){
				//$this->redirect($this->url->link('transaction/leave_ess', 'token=' . $this->session->data['token'], 'SSL'));
			} else {
				$this->redirect($this->url->link('user/password_change', 'token=' . $this->session->data['token'].'&emp_code='.$emp_code, 'SSL'));
			}
		} elseif(isset($this->session->data['is_super'])){
			$emp_code = $this->session->data['d_emp_id'];
			$is_set = $this->db->query("SELECT `is_set` FROM `oc_employee` WHERE `emp_code` = '".$emp_code."' ")->row['is_set'];	
			if($is_set == '1'){
				//$this->redirect($this->url->link('transaction/leave_ess', 'token=' . $this->session->data['token'], 'SSL'));
			} else {
				$this->redirect($this->url->link('user/password_change', 'token=' . $this->session->data['token'].'&emp_code='.$emp_code, 'SSL'));
			}
		}

		$this->language->load('transaction/leave');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('transaction/transaction');
		$this->load->model('catalog/shift');
		$this->load->model('catalog/employee');

		$this->getList();
	}

	public function insert() {
		$this->language->load('transaction/leave');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('transaction/transaction');
		$this->load->model('catalog/shift');
		$this->load->model('catalog/employee');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			// echo '<pre>';
			// print_r($this->request->post);
			// exit;
			$data = $this->request->post;
			
			$emp_data = $this->model_transaction_transaction->getEmployees_dat($data['e_name_id']);
			if(isset($emp_data['emp_code'])){
				$department = $emp_data['department'];
				$unit = $emp_data['unit'];
				$group = $emp_data['group'];
				$reporting_to = $emp_data['reporting_to'];
			} else {
				$department = '';
				$unit = '';
				$group = '';
				$reporting_to = '';
			}

			$days = $this->GetDays($data['from'], $data['to']);
			$day = array();
			foreach ($days as $dkey => $dvalue) {
	        	$dates = explode('-', $dvalue);
	        	$day[$dkey]['day'] = $dates[2];
	        	$day[$dkey]['date'] = $dvalue;
	        }

	        if($data['multi_day'] == 1){
	        	$data['leave_amount'] = '';
	        	$data['type'] = '';
	        } else {
	        	$data['days'] = '';
	        	$data['encash'] = '';
	        }

	        $batch_id_sql = "SELECT `batch_id` FROM `oc_leave_transaction_temp` ORDER BY `batch_id` DESC LIMIT 1";
			$obatch_ids = $this->db->query($batch_id_sql);
			if($obatch_ids->num_rows == 0){
				$batch_id = 1;
			} else{
				$obatch_id = $obatch_ids->row['batch_id'];
				$batch_id = $obatch_id + 1;
			}

			if($data['enable_encash'] == 0){
				$data['encash'] = '';
			}
			//echo $batch_id;exit;
			foreach($day as $dkey => $dvalue){
				$sql = "INSERT INTO `oc_leave_transaction_temp` SET 
					`emp_id` = '".$data['e_name_id']."', 
					`leave_type` = '".$data['leave_type']."',
					`date` = '".$dvalue['date']."',
					`date_cof` = '".$data['date_cof']."',
					`leave_amount` = '".$data['leave_amount']."',
					`type` = '".$data['type']."',
					`days` = '".$data['days']."',
					`encash` = '".$data['encash']."',
					`leave_reason` = '".$this->db->escape($data['leave_reason'])."',
					`a_status` = '1',
					`unit` = '".$unit."',
					`group` = '".$group."',
					`year` = '".date('Y', strtotime($dvalue['date']))."',
					`dot` = '".date('Y-m-d')."',
					`dept_name` = '".$this->db->escape($department)."',
					`batch_id` = '".$batch_id."',
					`reporting_to` = '".$reporting_to."' ";
				//echo $sql;
				//echo '<br />';
				$this->db->query($sql);
			}

			if($data['enable_encash'] == 1 && $data['encash'] > 0 && $data['leave_type'] = 'PL'){
				//$pl_bal_sql = "SELECT `pl_bal` FROM `oc_leave` WHERE emp_id = '".$data['e_name_id']."' ";
				//$pl_bal_res = $this->db->query($pl_bal_sql);

				$total_bal = $this->model_transaction_transaction->gettotal_bal_ess($data['e_name_id']);
				$total_bal_pro = $this->model_transaction_transaction->gettotal_bal_pro_ess($data['e_name_id'], 'PL');
				$total_bal_pro1 = $this->model_transaction_transaction->gettotal_bal_pro1_ess($data['e_name_id'], 'PL');
				$total_bal_pl = 0;
				if(isset($total_bal['pl_acc'])){
					if(isset($total_bal_pro['bal_p'])){
						$total_bal_pl = $total_bal['pl_acc'] - ($total_bal_pro['bal_p'] + $total_bal_pro1['leave_amount']);
					} else {
						$total_bal_pl = $total_bal['pl_acc'];
					}
				}

				if($total_bal_pl > 0){
					$pl_balance = $total_bal_pl;
					$pl_balance = $pl_balance - $data['encash'];
					$upd_pl_bal_sql = "UPDATE `oc_leave` SET `pl_bal` = '".$pl_balance."' WHERE `emp_id` = '".$data['e_name_id']."' AND `close_status` = '0' ";
					//echo $upd_pl_bal_sql;
					//echo '<br />';
					$this->db->query($upd_pl_bal_sql);
				}
			}

			require(DIR_SYSTEM . 'library/Mailin.php');
			
			$dept_heads = $this->db->query("SELECT `reporting_to`, `department`, emp_code, `title`, `name`, `email` FROM `oc_employee` WHERE 1=1 AND `emp_code` = '".$reporting_to."' ")->row;
			//$message = 'You have a ' . $data['leave_type'] . ' Leave From '. date('d-m-Y', strtotime($data['from'])) . ' to '. date('d-m-Y', strtotime($data['to'])) .' Pending For Approval by Employee ' . $date['e_name'] . "\n\n";
			$message = 'Dear ' . $dept_heads['name'].', ' . "<br>";
			$message .= 'Please Approve the Leave / OD of '.$data['e_name'].' using following Link'."<br>";
			$message .= 'URL - http://172.16.10.58:81/attendance/admin'."<br>";
			$message .= '-- Regards'."<br>";
			$message .= 'Admin.';
			// $message = 'You have a Leave Pending For Approval'."<br>";
			// $message .= 'URL - http://172.16.10.58:81/attendance/admin';
			$subject = 'Leave Approval Reminder';
			$from_email = 'fargose.aaron@gmail.com';
			$to_email = $dept_heads['email'];
			//$to_email = 'eric.fargose@gmail.com';
			$to_name = $dept_heads['title'].' '.$dept_heads['name'];

            $mailin = new Mailin("https://api.sendinblue.com/v2.0","N12OPU6qLfTsxnK7");
            $mail_data = array( "to" => array($to_email=>$to_name),
                            "from" => array($from_email, "India Factoring & Finance Solutions Pvt.Ltd."),
                            "subject" => $subject,
                            "html" => html_entity_decode($message)
                         );
            $mailin->send_email($mail_data);

            $dept_heads = $this->db->query("SELECT `reporting_to`, `department`, emp_code, `title`, `name`, `email` FROM `oc_employee` WHERE 1=1 AND `emp_code` = '".$data['e_name_id']."' ")->row;
			//$message = 'You have a ' . $data['leave_type'] . ' Leave From '. date('d-m-Y', strtotime($data['from'])) . ' to '. date('d-m-Y', strtotime($data['to'])) .' Pending For Approval by Employee ' . $date['e_name'] . "\n\n";
			$message = 'You have added a Leave / OD and is Pending For Approval'."<br>";
			//$message .= 'URL - http://172.16.10.58:81/attendance/admin';
			$subject = 'Leave Added Successfully';
			$from_email = 'fargose.aaron@gmail.com';
			$to_email = $dept_heads['email'];
			//$to_email = 'eric.fargose@gmail.com';
			$to_name = $dept_heads['title'].' '.$dept_heads['name'];

            $mailin = new Mailin("https://api.sendinblue.com/v2.0","N12OPU6qLfTsxnK7");
            $mail_data = array( "to" => array($to_email=>$to_name),
                            "from" => array($from_email, "India Factoring & Finance Solutions Pvt.Ltd."),
                            "subject" => $subject,
                            "html" => html_entity_decode($message)
                         );
            $mailin->send_email($mail_data);

			//exit;
			$this->session->data['success'] = 'You have Inserted the Leave Transaction';	
			$url = '';
			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['filter_name_id_1'])) {
				$url .= '&filter_name_id_1=' . $this->request->get['filter_name_id_1'];
			}

			if (isset($this->request->get['filter_date'])) {
				$url .= '&filter_date=' . $this->request->get['filter_date'];
			}

			if (isset($this->request->get['filter_unit'])) {
				$url .= '&filter_unit=' . $this->request->get['filter_unit'];
			}

			if (isset($this->request->get['filter_approval_1'])) {
				$url .= '&filter_approval_1=' . $this->request->get['filter_approval_1'];
			}

			if (isset($this->request->get['filter_approval_1_by'])) {
				$url .= '&filter_approval_1_by=' . $this->request->get['filter_approval_1_by'];
			}

			if (isset($this->request->get['filter_approval_2'])) {
				$url .= '&filter_approval_2=' . $this->request->get['filter_approval_2'];
			}

			if (isset($this->request->get['filter_proc'])) {
				$url .= '&filter_proc=' . $this->request->get['filter_proc'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['filter_leavetype'])) {
				$url .= '&filter_leavetype=' . $this->request->get['filter_leavetype'];
			}

			$this->redirect($this->url->link('transaction/leave_ess', 'token=' . $this->session->data['token'].$url.'&batch_id='.$batch_id, 'SSL'));
		}

		$this->getForm();
	}

	public function delete() {
		$this->language->load('transaction/leave');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('transaction/transaction');
		$this->load->model('catalog/shift');
		$this->load->model('catalog/employee');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $batch_id) {
				// $leav_data_sql = "SELECT * FROM `oc_leave_transaction_temp` WHERE `batch_id` = '".$batch_id."' ";
				// $leav_data = $this->db->query($leav_data_sql)->row;
				// if(isset($leav_data['encash']) && $leav_data['encash'] != ''){
				// 	$encah_leave = $leav_data['encash'];
				// 	$sql = "SELECT * FROM `oc_leave` WHERE `emp_id` = '".$leav_data['emp_id']."' AND `close_status` = '0' ";
				// 	$query1 = $this->db->query($sql);
				// 	if($query1->num_rows > 0){
				// 		if($query->row['leave_type'] == 'PL'){
				// 			//$balance = $query1->row['pl_bal'];
				// 			$total_bal = $this->model_transaction_transaction->gettotal_bal_ess($leav_data['emp_id']);
				// 			$total_bal_pro = $this->model_transaction_transaction->gettotal_bal_pro_ess($leav_data['emp_id'], 'PL');
				// 			$total_bal_pro1 = $this->model_transaction_transaction->gettotal_bal_pro1_ess($leav_data['emp_id'], 'PL');
				// 			$total_bal_pl = 0;
				// 			if(isset($total_bal['pl_acc'])){
				// 				if(isset($total_bal_pro['bal_p'])){
				// 					$total_bal_pl = $total_bal['pl_acc'] - ($total_bal_pro['bal_p'] + $total_bal_pro1['leave_amount']);
				// 				} else {
				// 					$total_bal_pl = $total_bal['pl_acc'];
				// 				}
				// 			}
				// 			$balance = $total_bal_pl + $leav_data['encash'];
				// 			$upd_bal_sql = "UPDATE `oc_leave` SET `pl_bal` = '".$balance."' WHERE `emp_id` = '".$query1->row['emp_id']."' AND `close_status` = '0' ";
				// 			$this->db->query($upd_bal_sql);
				// 			$this->log->write($upd_bal_sql);
				// 		} 
				// 	}
				// }
				$can_leave_sql = "DELETE FROM `oc_leave_transaction_temp` WHERE `batch_id` = '".$batch_id."' ";
				$this->db->query($can_leave_sql);

				$can_leave_sql = "DELETE FROM `oc_requestform` WHERE `batch_id` = '".$batch_id."' ";
				$this->db->query($can_leave_sql);
			}

			$this->session->data['success'] = 'Leave Cancelled Successfully';

			$url = '';
			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['filter_name_id_1'])) {
				$url .= '&filter_name_id_1=' . $this->request->get['filter_name_id_1'];
			}

			if (isset($this->request->get['filter_date'])) {
				$url .= '&filter_date=' . $this->request->get['filter_date'];
			}

			if (isset($this->request->get['filter_unit'])) {
				$url .= '&filter_unit=' . $this->request->get['filter_unit'];
			}

			if (isset($this->request->get['filter_approval_1'])) {
				$url .= '&filter_approval_1=' . $this->request->get['filter_approval_1'];
			}

			if (isset($this->request->get['filter_approval_1_by'])) {
				$url .= '&filter_approval_1_by=' . $this->request->get['filter_approval_1_by'];
			}

			if (isset($this->request->get['filter_approval_2'])) {
				$url .= '&filter_approval_2=' . $this->request->get['filter_approval_2'];
			}

			if (isset($this->request->get['filter_proc'])) {
				$url .= '&filter_proc=' . $this->request->get['filter_proc'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['filter_leavetype'])) {
				$url .= '&filter_leavetype=' . $this->request->get['filter_leavetype'];
			}

			$this->redirect($this->url->link('transaction/leave_ess', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		} elseif(isset($this->request->get['batch_id']) && $this->validateDelete()){
			$batch_id = $this->request->get['batch_id'];
			// $leav_data_sql = "SELECT * FROM `oc_leave_transaction_temp` WHERE `batch_id` = '".$batch_id."' ";
			// $leav_data = $this->db->query($leav_data_sql)->row;
			// if(isset($leav_data['encash']) && $leav_data['encash'] != ''){
			// 	$encah_leave = $leav_data['encash'];
			// 	$sql = "SELECT * FROM `oc_leave` WHERE `emp_id` = '".$leav_data['emp_id']."' AND `close_status` = '0' ";
			// 	$query1 = $this->db->query($sql);
			// 	if($query1->num_rows > 0){
			// 		if($leav_data['leave_type'] == 'PL'){
			// 			//$balance = $query1->row['pl_bal'];
			// 			$total_bal = $this->model_transaction_transaction->gettotal_bal_ess($leav_data['emp_id']);
			// 			$total_bal_pro = $this->model_transaction_transaction->gettotal_bal_pro_ess($leav_data['emp_id'], 'PL');
			// 			$total_bal_pro1 = $this->model_transaction_transaction->gettotal_bal_pro1_ess($leav_data['emp_id'], 'PL');
			// 			$total_bal_pl = 0;
			// 			if(isset($total_bal['pl_acc'])){
			// 				if(isset($total_bal_pro['bal_p'])){
			// 					$total_bal_pl = $total_bal['pl_acc'] - ($total_bal_pro['bal_p'] + $total_bal_pro1['leave_amount']);
			// 				} else {
			// 					$total_bal_pl = $total_bal['pl_acc'];
			// 				}
			// 			}
			// 			$balance = $total_bal_pl + $leav_data['encash'];
			// 			$upd_bal_sql = "UPDATE `oc_leave` SET `pl_bal` = '".$balance."' WHERE `emp_id` = '".$query1->row['emp_id']."' AND `close_status` = '0' ";
			// 			$this->db->query($upd_bal_sql);
			// 			$this->log->write($upd_bal_sql);
			// 		} 
			// 	}
			// }
			$can_leave_sql = "DELETE FROM `oc_leave_transaction_temp` WHERE `batch_id` = '".$this->request->get['batch_id']."' ";
			$this->db->query($can_leave_sql);

			$can_leave_sql_1 = "DELETE FROM `oc_leave_transaction` WHERE `linked_batch_id` = '".$this->request->get['batch_id']."' ";
			$this->db->query($can_leave_sql_1);

			$can_leave_sql = "DELETE FROM `oc_requestform` WHERE `batch_id` = '".$batch_id."' ";
			$this->db->query($can_leave_sql);
			$this->session->data['success'] = 'Leave Cancelled Successfully';

			$url = '';
			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . $this->request->get['filter_name'];
			}

			if (isset($this->request->get['filter_name_id'])) {
				$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
			}

			if (isset($this->request->get['filter_name_id_1'])) {
				$url .= '&filter_name_id_1=' . $this->request->get['filter_name_id_1'];
			}

			if (isset($this->request->get['filter_date'])) {
				$url .= '&filter_date=' . $this->request->get['filter_date'];
			}

			if (isset($this->request->get['filter_unit'])) {
				$url .= '&filter_unit=' . $this->request->get['filter_unit'];
			}

			if (isset($this->request->get['filter_approval_1'])) {
				$url .= '&filter_approval_1=' . $this->request->get['filter_approval_1'];
			}

			if (isset($this->request->get['filter_approval_1_by'])) {
				$url .= '&filter_approval_1_by=' . $this->request->get['filter_approval_1_by'];
			}

			if (isset($this->request->get['filter_approval_2'])) {
				$url .= '&filter_approval_2=' . $this->request->get['filter_approval_2'];
			}

			if (isset($this->request->get['filter_proc'])) {
				$url .= '&filter_proc=' . $this->request->get['filter_proc'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['filter_leavetype'])) {
				$url .= '&filter_leavetype=' . $this->request->get['filter_leavetype'];
			}

			$this->redirect($this->url->link('transaction/leave_ess', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} elseif(isset($this->session->data['emp_code'])){
			$emp_name = $this->db->query("SELECT `name` FROM `oc_employee` WHERE `emp_code` = '".$this->session->data['emp_code']."' ")->row['name'];
			$filter_name = $emp_name;
		} elseif(isset($this->session->data['d_emp_id'])){
			$emp_name = $this->db->query("SELECT `name` FROM `oc_employee` WHERE `emp_code` = '".$this->session->data['d_emp_id']."' ")->row['name'];
			$filter_name = $emp_name;
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_name_id'])) {
			$filter_name_id = $this->request->get['filter_name_id'];
		} elseif(isset($this->session->data['emp_code'])){
			$filter_name_id = $this->session->data['emp_code'];
		} elseif(isset($this->session->data['d_emp_id'])){
			$filter_name_id = $this->session->data['d_emp_id'];
		}  else {
			$filter_name_id = '';
		}

		if (isset($this->request->get['filter_name_id_1'])) {
			$filter_name_id_1 = $this->request->get['filter_name_id_1'];
		} elseif(isset($this->session->data['emp_code'])){
			$filter_name_id_1 = $this->session->data['emp_code'];
		} else {
			$filter_name_id_1 = '';
		}

		if($filter_name_id != ''){
			$filter_dept = $this->db->query("SELECT `department` FROM `oc_employee` WHERE `emp_code` = '".$filter_name_id."' ")->row['department'];
		} elseif($filter_name_id_1 != ''){
			$filter_dept = $this->db->query("SELECT `department` FROM `oc_employee` WHERE `emp_code` = '".$filter_name_id_1."' ")->row['department'];
		} else {
			$filter_dept = '';
		}

		if($filter_name_id != ''){
			$filter_unit = $this->db->query("SELECT `unit` FROM `oc_employee` WHERE `emp_code` = '".$filter_name_id."' ")->row['unit'];
		} elseif($filter_name_id_1 != ''){
			$filter_unit = $this->db->query("SELECT `unit` FROM `oc_employee` WHERE `emp_code` = '".$filter_name_id_1."' ")->row['unit'];
		} else {
			$filter_unit = '';
		}

		//echo $filter_unit;exit;

		if (isset($this->request->get['filter_date'])) {
			$filter_date = $this->request->get['filter_date'];
		} else {
			$filter_date = '';
		}

		if (isset($this->request->get['filter_approval_1'])) {
			$filter_approval_1 = $this->request->get['filter_approval_1'];
		} else {
			$filter_approval_1 = '0';
		}

		if (isset($this->request->get['filter_approval_1_by'])) {
			$filter_approval_1_by = $this->request->get['filter_approval_1_by'];
		} else {
			$filter_approval_1_by = '0';
		}

		if (isset($this->request->get['filter_approval_2'])) {
			$filter_approval_2 = $this->request->get['filter_approval_2'];
		} else {
			$filter_approval_2 = '0';
		}

		if (isset($this->request->get['filter_proc'])) {
			$filter_proc = $this->request->get['filter_proc'];
		} else {
			$filter_proc = '0';
		}

		if (isset($this->request->get['filter_leavetype'])) {
			$filter_leavetype = $this->request->get['filter_leavetype'];
		} else {
			$filter_leavetype = '0';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_name_id_1'])) {
			$url .= '&filter_name_id_1=' . $this->request->get['filter_name_id_1'];
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . $this->request->get['filter_date'];
		}

		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}

		if (isset($this->request->get['filter_approval_1'])) {
			$url .= '&filter_approval_1=' . $this->request->get['filter_approval_1'];
		}

		if (isset($this->request->get['filter_approval_1_by'])) {
			$url .= '&filter_approval_1_by=' . $this->request->get['filter_approval_1_by'];
		}

		if (isset($this->request->get['filter_approval_2'])) {
			$url .= '&filter_approval_2=' . $this->request->get['filter_approval_2'];
		}

		if (isset($this->request->get['filter_proc'])) {
			$url .= '&filter_proc=' . $this->request->get['filter_proc'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['filter_leavetype'])) {
			$url .= '&filter_leavetype=' . $this->request->get['filter_leavetype'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('transaction/leave_ess', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$url_insert = '&filter_name_id='.$filter_name_id.'&filter_name_id_1='.$filter_name_id.'&filter_name='.$filter_name;

		$this->data['insert'] = $this->url->link('transaction/leave_ess/insert', 'token=' . $this->session->data['token'] . $url . $url_insert, 'SSL');
		$this->data['delete'] = $this->url->link('transaction/leave_ess/delete', 'token=' . $this->session->data['token'] . $url . $url_insert, 'SSL');
		$this->data['export'] = $this->url->link('transaction/leave_ess/export', 'token=' . $this->session->data['token'] . $url . $url_insert, 'SSL');
		$this->data['requests'] = $this->url->link('transaction/requestform/getForm1', 'token=' . $this->session->data['token'] . $url . $url_insert, 'SSL');
		
		$this->data['leaves'] = array();
		$employee_total = 0;

		$data = array(
			'filter_name' => $filter_name,
			'filter_name_id' => $filter_name_id,
			'filter_name_id_1' => $filter_name_id_1,
			'filter_date' => $filter_date,
			'filter_unit' => $filter_unit,
			'filter_approval_1' => $filter_approval_1,
			'filter_approval_1_by' => $filter_approval_1_by,
			'filter_approval_2' => $filter_approval_2,
			'filter_proc' => $filter_proc,
			'filter_leavetype' => $filter_leavetype,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);

		
		$employee_total = $this->model_transaction_transaction->getTotalleaveetransaction_ess($data);
		$results = $this->model_transaction_transaction->getleavetransaction_ess($data);
		foreach ($results as $result) {
			$action = array();
			if($result['reject_date'] == '0000-00-00'){
				$leave_sql = "SELECT `id` FROM `oc_leave_transaction_temp` WHERE `batch_id` = '".$result['batch_id']."' AND (`approval_1` = '1' || `approval_2` = '1') ";
				$date_res = $this->db->query($leave_sql);
				if ($date_res->num_rows == 0) {
					$leave_sql = "SELECT `id` FROM `oc_leave_transaction_temp` WHERE `batch_id` = '".$result['batch_id']."' AND `p_status` = '1' ";
					$date_res = $this->db->query($leave_sql);
					if($date_res->num_rows == 0){
						if($result['encash'] == ''){
							$action[] = array(
								'text' => 'Delete',
								'href' => $this->url->link('transaction/leave_ess/delete', 'token=' . $this->session->data['token'] . '&batch_id=' . $result['batch_id'] . $url, 'SSL')
							);
						} else {
							$action[] = array(
								'text' => 'Delete',
								'href' => $this->url->link('transaction/leave_ess/delete', 'token=' . $this->session->data['token'] . '&batch_id=' . $result['batch_id'] . $url, 'SSL')
							);
						}
					} else {
						$request_sql = "SELECT `id`, `approval_1` FROM `oc_requestform` WHERE `batch_id` = '".$result['batch_id']."' ";
						$request_data = $this->db->query($request_sql);
						if($request_data->num_rows == 0){
							$action[] = array(
								'text' => 'Revert Leave',
								'href' => $this->url->link('transaction/requestform/insert', 'token=' . $this->session->data['token'] . '&batch_id=' . $result['batch_id'] . $url.$url_insert, 'SSL')
							);
						} else {
							if($request_data->row['approval_1'] == '0'){
								$action[] = array(
									'text' => 'Revert Request In Process',
									'href' => ''
								);
							} elseif($request_data->row['approval_1'] == '1'){
								$action[] = array(
									'text' => 'Approved',
									'href' => ''
								);
							} elseif($request_data->row['approval_1'] == '2'){
								$action[] = array(
									'text' => 'Rejected',
									'href' => ''
								);
							}
						}
					}
				} else {
					$request_sql = "SELECT `id`, `approval_1` FROM `oc_requestform` WHERE `batch_id` = '".$result['batch_id']."' ";
					$request_data = $this->db->query($request_sql);
					if($request_data->num_rows == 0){
						$action[] = array(
							'text' => 'Delete',
							'href' => $this->url->link('transaction/leave_ess/delete', 'token=' . $this->session->data['token'] . '&batch_id=' . $result['batch_id'] . $url.$url_insert, 'SSL')
						);
					} else {
						if($request_data->row['approval_1'] == '0'){
							$action[] = array(
								'text' => 'Revert Request In Process',
								'href' => ''
							);
						} elseif($request_data->row['approval_1'] == '1'){
							$action[] = array(
								'text' => 'Approved',
								'href' => ''
							);
						} elseif($request_data->row['approval_1'] == '2'){
							$action[] = array(
								'text' => 'Rejected',
								'href' => ''
							);
						}
					}
				}
			} else {
				$action[] = array(
					'text' => 'Delete',
					'href' => $this->url->link('transaction/leave_ess/delete', 'token=' . $this->session->data['token'] . '&batch_id=' . $result['batch_id'] . $url, 'SSL')
				);
			}

			$total_leave_dayss = $this->model_transaction_transaction->gettotal_leave_days_ess($result['batch_id']);
			if($total_leave_dayss['days'] == ''){
				$total_leave_days = $total_leave_dayss['leave_amount'];
			} else {
				$total_leave_days = $total_leave_dayss['days'];
			}
			$leave_from = $this->model_transaction_transaction->getleave_from_ess($result['batch_id']);
			$leave_to = $this->model_transaction_transaction->getleave_to_ess($result['batch_id']);

			$emp_data = $this->model_transaction_transaction->getempdata($result['emp_id']);

			if($result['approval_1'] == '1'){
				if($result['approval_date_1'] != '0000-00-00 00:00:00'){
					$approval_1 = 'Approved on ' . date('d-m-y h:i:s', strtotime($result['approval_date_1']));
				} else {
					$approval_1 = 'Approved';
				}
			} else if($result['approval_1'] == '2'){
				$approval_1 = 'Rejected';
			} else {
				$approval_1 = 'Pending';
			}

			if($result['approval_2'] == '1'){
				if($result['approval_date_2'] != '0000-00-00 00:00:00'){
					$approval_2 = 'Approved on ' . date('d-m-y h:i:s', strtotime($result['approval_date_2']));
				} else {
					$approval_2 = 'Approved';
				}
			} else {
				$approval_2 = 'Pending';
			}


			if($result['p_status'] == '1'){
				$proc_stat = 'Processed';
			} else {
				$proc_stat = 'UnProcessed';
			}

			if($result['leave_type'] == 'COF'){
				$leave_type = $result['leave_type'].' / '.$result['date_cof'];
			} else {
				$leave_type = $result['leave_type'];
			}

			if($result['reject_date'] != '0000-00-00'){
				if($result['reject_reason'] != ''){
					$reject_reason = $result['reject_reason'].' - as on '.date('d-m-y', strtotime($result['reject_date']));
				} else {
					$reject_reason = 'as on '.date('d-m-y', strtotime($result['reject_date']));
				}
			} elseif($result['reject_date_1'] != '0000-00-00'){
				if($result['reject_reason_1'] != ''){
					$reject_reason = $result['reject_reason_1'].' - as on '.date('d-m-y', strtotime($result['reject_date_1']));
				} else {
					$reject_reason = 'as on '.date('d-m-y', strtotime($result['reject_date_1']));
				}
			} elseif($result['reject_date_2'] != '0000-00-00'){
				if($result['reject_reason_2'] != ''){
					$reject_reason = $result['reject_reason_2'].' - as on '.date('d-m-y', strtotime($result['reject_date_2']));
				} else {
					$reject_reason = 'as on '.date('d-m-y', strtotime($result['reject_date_2']));
				}
			} else {
				$reject_reason = '';
			}

			$this->data['leaves'][] = array(
				'batch_id' => $result['batch_id'],
				'dot' => date('d-m-y', strtotime($result['dot'])),
				'leave_reason' => $result['leave_reason'],
				'reject_reason' => $reject_reason,
				'encash' => $result['encash'],
				'id' => $result['id'],
				'emp_id' => $result['emp_id'],
				'name' => $emp_data['name'],
				'unit' => $emp_data['unit'],
				'leave_type'        => $leave_type,
				'total_leave_days' 	      => $total_leave_days,
				'leave_from' => date('d-m-y', strtotime($leave_from)),
				'leave_to' => date('d-m-y', strtotime($leave_to)),
				'approval_1' => $approval_1,
				'approval_2' => $approval_2,
				'approval_1_by' => $result['approved_1_by'],
				'proc_stat' => $proc_stat,
				'selected'        => isset($this->request->post['selected']) && in_array($result['id'], $this->request->post['selected']),
				'action'          => $action
			);
		}

		// echo '<pre>';
		// print_r($this->data['leaves']);
		// exit;

		$unit_data = array(
			'0' => 'All',
			'Mumbai' => 'Mumbai',
			'Pune' => 'Pune',
			'Delhi' => 'Delhi',
			'Chennai' => 'Chennai',
			'Bangalore' => 'Bangalore',
			'Ahmedabad' => 'Ahmedabad'
		);

		$this->data['unit_data'] = $unit_data;

		$approves = array(
			'0' => 'All',
			'1' => 'Pending',
			'2' => 'Approved',
			'3' => 'Rejected'
		);

		$this->data['approves'] = $approves;

		$process = array(
			'0' => 'All',
			'1' => 'UnProcessed',
			'2' => 'Processed',
		);

		$this->data['process'] = $process;

		$leavess = array(
			'0' => 'All',
			'PL' => 'PL',
			'BL' => 'BL',
			'SL' => 'SL',
			'LWP' => 'LWP',
			'ML' => 'ML',
			'MAL' => 'MAL',
			'PAL' => 'PAL',
			'OD' => 'OD'
			
		);
		$this->data['leavess'] = $leavess;



		$sql = "SELECT `emp_code`, `name`, `title` FROM `oc_employee` WHERE `is_dept` = '1' AND `is_super` = '0' AND `department` = '".$filter_dept."' ";
		$dept_headsss = $this->db->query($sql);
		$dept_heads['0'] = 'All';
		if($dept_headsss->num_rows > 0){
			$dept_headss = $dept_headsss->rows;
			foreach($dept_headss as $dkey => $dvalue){
				$dept_heads[$dvalue['emp_code']] = $dvalue['name'];
			}
		}
		$this->data['dept_heads'] = $dept_heads;

		$this->data['token'] = $this->session->data['token'];	

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_delete'] = $this->language->get('text_delete');

		$this->data['button_insert'] = $this->language->get('button_insert');

		$this->data['button_filter'] = $this->language->get('button_filter');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		// if (isset($this->request->get['page'])) {
		// 	$url .= '&page=' . $this->request->get['page'];
		// }

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_name_id_1'])) {
			$url .= '&filter_name_id_1=' . $this->request->get['filter_name_id_1'];
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . $this->request->get['filter_date'];
		}

		if (isset($this->request->get['filter_unit'])) {
			$url .= '&filter_unit=' . $this->request->get['filter_unit'];
		}

		if (isset($this->request->get['filter_approval_1'])) {
			$url .= '&filter_approval_1=' . $this->request->get['filter_approval_1'];
		}

		if (isset($this->request->get['filter_approval_1_by'])) {
			$url .= '&filter_approval_1_by=' . $this->request->get['filter_approval_1_by'];
		}

		if (isset($this->request->get['filter_approval_2'])) {
			$url .= '&filter_approval_2=' . $this->request->get['filter_approval_2'];
		}

		if (isset($this->request->get['filter_proc'])) {
			$url .= '&filter_proc=' . $this->request->get['filter_proc'];
		}

		if (isset($this->request->get['filter_leavetype'])) {
			$url .= '&filter_leavetype=' . $this->request->get['filter_leavetype'];
		}

		$pagination = new Pagination();
		$pagination->total = $employee_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('transaction/leave_ess', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		
		$this->data['filter_name'] = $filter_name;
		$this->data['filter_name_id'] = $filter_name_id;
		$this->data['filter_name_id_1'] = $filter_name_id_1;
		$this->data['filter_date'] = $filter_date;
		$this->data['filter_unit'] = $filter_unit;
		$this->data['filter_approval_1'] = $filter_approval_1;
		$this->data['filter_approval_1_by'] = $filter_approval_1_by;
		$this->data['filter_approval_2'] = $filter_approval_2;
		$this->data['filter_proc'] = $filter_proc;
		$this->data['filter_dept'] = $filter_dept;
		$this->data['filter_unit'] = $filter_unit;
		$this->data['filter_leavetype'] = $filter_leavetype;
		
		$this->template = 'transaction/leave_ess_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');

		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		$this->data['text_no_results'] = $this->language->get('text_no_results');

		$this->data['tab_general'] = $this->language->get('tab_general');

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {	
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['encash'])) {
			$this->data['error_encash'] = $this->error['encash'];
		} else {
			$this->data['error_encash'] = '';
		}

		if (isset($this->error['days'])) {
			$this->data['error_days'] = $this->error['days'];
		} else {
			$this->data['error_days'] = '';
		}

		if (isset($this->error['leave_type'])) {
			$this->data['error_leave_type'] = $this->error['leave_type'];
		} else {
			$this->data['error_leave_type'] = '';
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('transaction/leave_ess', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		
		$url = '';
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if (isset($this->request->get['filter_name_id'])) {
			$url .= '&filter_name_id=' . $this->request->get['filter_name_id'];
		}

		if (isset($this->request->get['filter_name_id_1'])) {
			$url .= '&filter_name_id_1=' . $this->request->get['filter_name_id_1'];
		}

		if (isset($this->request->get['filter_approval_1'])) {
			$url .= '&filter_approval_1=' . $this->request->get['filter_approval_1'];
		}

		if (isset($this->request->get['filter_approval_1_by'])) {
			$url .= '&filter_approval_1_by=' . $this->request->get['filter_approval_1_by'];
		}

		if (isset($this->request->get['filter_approval_2'])) {
			$url .= '&filter_approval_2=' . $this->request->get['filter_approval_2'];
		}

		if (isset($this->request->get['filter_proc'])) {
			$url .= '&filter_proc=' . $this->request->get['filter_proc'];
		}

		if (isset($this->request->get['filter_leavetype'])) {
			$url .= '&filter_leavetype=' . $this->request->get['filter_leavetype'];
		}

		$this->data['cancel'] = $this->url->link('transaction/leave_ess', 'token=' . $this->session->data['token'].$url, 'SSL');
		$this->data['action'] = $this->url->link('transaction/leave_ess/insert', 'token=' . $this->session->data['token'].$url, 'SSL');

		
		$transaction_data = array();
		$emp_data = array();
		if (isset($this->request->get['batch_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$transaction_data = $this->model_transaction_transaction->getleave_transaction_data_ess($this->request->get['batch_id']);
			$emp_data = $this->model_transaction_transaction->getEmployees_dat($this->request->get['filter_name_id']);
		} elseif(isset($this->request->get['filter_name_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')){
			$emp_data = $this->model_transaction_transaction->getEmployees_dat($this->request->get['filter_name_id']);
		} elseif(isset($this->request->get['filter_name_id_1']) && ($this->request->server['REQUEST_METHOD'] != 'POST')){
			$emp_data = $this->model_transaction_transaction->getEmployees_dat($this->request->get['filter_name_id_1']);
		}

		$this->data['token'] = $this->session->data['token'];

		$leaves = array(
			'0' => array(
				'leave_id' => 'PL',
				'leave_name' => 'PL'
			),
			'1' => array(
				'leave_id' => 'BL',
				'leave_name' => 'BL'
			),
			'2' => array(
				'leave_id' => 'SL',
				'leave_name' => 'SL'
			),
			'3' => array(
				'leave_id' => 'ML',
				'leave_name' => 'ML'
			),
			//'4' => array(
				//'leave_id' => 'PLE',
				//'leave_name' => 'PLE'
			//),
			'5' => array(
				'leave_id' => 'MAL',
				'leave_name' => 'MAL'
			),
			'6' => array(
				'leave_id' => 'PAL',
				'leave_name' => 'PAL'
			),
			'7' => array(
				'leave_id' => 'OD',
				'leave_name' => 'OD'
			),
			'8' => array(
				'leave_id' => 'LWP',
				'leave_name' => 'LWP'
			),
		);
		
		$this->data['leaves'] = $leaves;

		if (isset($this->request->post['insert'])) {
			$this->data['insert'] = $this->request->post['insert'];
		} elseif(isset($transaction_data[0]['transaction_id'])) {
			$this->data['insert'] = 0;
		} else {
			$this->data['insert'] = '';
		}

		if (isset($this->request->post['batch_id'])) {
			$this->data['batch_id'] = $this->request->post['batch_id'];
		} elseif(isset($transaction_data[0]['batch_id'])) {
			$this->data['batch_id'] = $transaction_data[0]['batch_id'];
		} else {	
			$this->data['batch_id'] = 0;
		}

		if (isset($this->request->post['leave_type'])) {
			$this->data['leave_type'] = $this->request->post['leave_type'];
		} elseif(isset($transaction_data[0]['leave_type'])) {
			$this->data['leave_type'] = $transaction_data[0]['leave_type'];
		} else {	
			$this->data['leave_type'] = 'PL';
		}

		if (isset($this->request->post['from'])) {
			$this->data['from'] = $this->request->post['from'];
		} elseif(isset($transaction_data[0]['from'])) {
			$this->data['from'] = $transaction_data[0]['from'];
		} else {	
			$this->data['from'] = date('Y-m-d');
		}

		if (isset($this->request->post['to'])) {
			$this->data['to'] = $this->request->post['to'];
		} elseif(isset($transaction_data[0]['to'])) {
			$this->data['to'] = $transaction_data[0]['to'];
		} else {	
			$this->data['to'] = date('Y-m-d');
		}

		if (isset($this->request->post['leave_amount'])) {
			$this->data['leave_amount'] = $this->request->post['leave_amount'];
		} elseif (isset($transaction_data[0]['leave_amount'])) {
			$this->data['leave_amount'] = $transaction_data[0]['leave_amount'];
		} else {	
			$this->data['leave_amount'] = '';//date('d-m-Y');
		}

		if (isset($this->request->post['type'])) {
			$this->data['type'] = $this->request->post['type'];
		} elseif (isset($transaction_data[0]['type'])) {
			$this->data['type'] = $transaction_data[0]['type'];
		} else {	
			$this->data['type'] = 'F';//date('d-m-Y');
		}

		if (isset($this->request->post['days'])) {
			$this->data['days'] = $this->request->post['days'];
		} elseif (isset($transaction_data[0]['days'])) {
			$this->data['days'] = $transaction_data[0]['days'];
		} else {	
			$this->data['days'] = '';//date('d-m-Y');
		}

		if (isset($this->request->post['encash'])) {
			$this->data['encash'] = $this->request->post['encash'];
		} elseif (isset($transaction_data[0]['encash'])) {
			$this->data['encash'] = $transaction_data[0]['encash'];
		} else {	
			$this->data['encash'] = '';//date('d-m-Y');
		}

		if (isset($this->request->post['e_name'])) {
			$this->data['e_name'] = $this->request->post['e_name'];
			$this->data['e_name_id'] = $this->request->post['e_name_id'];
			$this->data['emp_code'] = $this->request->post['e_name_id'];
			$this->data['group'] = $this->request->post['group'];
		} 
		// elseif (isset($transaction_data[0]['emp_id'])) {
		// 	$this->data['e_name'] = $transaction_data[0]['emp_name'];
		// 	$this->data['e_name_id'] = $transaction_data[0]['emp_id'];
		// 	$this->data['emp_code'] = $transaction_data[0]['emp_id'];
		// } 
		elseif (isset($emp_data['emp_code'])) {
			$this->data['e_name'] = $emp_data['name'];
			$this->data['e_name_id'] = $emp_data['emp_code'];
			$this->data['emp_code'] = $emp_data['emp_code'];
			$this->data['group'] = $emp_data['group'];
		} else {	
			$this->data['e_name'] = '';
			$this->data['e_name_id'] = '';
			$this->data['emp_code'] = '';
			$this->data['group'] = '';
		}

		if (isset($this->request->post['a_status'])) {
			$this->data['a_status'] = $this->request->post['a_status'];
		} elseif (isset($transaction_data[0]['a_status'])) {
			$this->data['a_status'] = $transaction_data[0]['a_status'];
		} else {	
			$this->data['a_status'] = 1;//date('d-m-Y');
		}

		if (isset($this->request->post['date_cof'])) {
			$this->data['date_cof'] = $this->request->post['date_cof'];
		} elseif (isset($transaction_data[0]['a_status'])) {
			$this->data['date_cof'] = $transaction_data[0]['date_cof'];
		} else {	
			$this->data['date_cof'] = '';//date('d-m-Y');
		}

		if (isset($this->request->post['dot'])) {
			$this->data['dot'] = $this->request->post['dot'];
		} elseif (isset($transaction_data[0]['dot'])) {
			$this->data['dot'] = $transaction_data[0]['dot'];
		} else {	
			$this->data['dot'] = '';//date('d-m-Y');
		}

		if (isset($this->request->post['leave_reason'])) {
			$this->data['leave_reason'] = $this->request->post['leave_reason'];
		} elseif (isset($transaction_data[0]['leave_reason'])) {
			$this->data['leave_reason'] = $transaction_data[0]['leave_reason'];
		} else {	
			$this->data['leave_reason'] = '';//date('d-m-Y');
		}

		if (isset($this->request->post['diffDays'])) {
			$this->data['diffDays'] = $this->request->post['diffDays'];
		} else {	
			$this->data['diffDays'] = 1;
		}

		if (isset($this->request->post['encash_done_year'])) {
			$this->data['encash_done_year'] = $this->request->post['encash_done_year'];
		} elseif(isset($emp_data['emp_code'])) {
			$emp_code = $emp_data['emp_code'];
			$is_exist = $this->model_transaction_transaction->getpltakenyear_ess($emp_code);
			if(isset($is_exist['id'])){
				$this->data['encash_done_year'] = 1;
			} else {
				$this->data['encash_done_year'] = 0;
			}
		} else {	
			$this->data['encash_done_year'] = 0;
		}

		$statuses = array(
			'0' => 'InActive',
			'1' => 'Active'
		);
		$this->data['statuses'] = $statuses;

		if(isset($this->request->post['multi_day'])){
			$this->data['multi_day'] = $this->request->post['multi_day'];
		} else { 
			$total_leave_dayss = $this->model_transaction_transaction->gettotal_leave_days_ess($this->data['batch_id']);
			if(isset($total_leave_dayss['days'])){
				if($total_leave_dayss['days'] == ''){
					$total_leave_days = $total_leave_dayss['leave_amount'];
				} else {
					$total_leave_days = $total_leave_dayss['days'];
				}
			} else {
				$total_leave_days = 0;
			}
			if($total_leave_days > 0){
				$this->data['multi_day'] = 1;
			} else {
				$this->data['multi_day'] = 0;
			}
		}

		if(isset($this->request->post['enable_encash'])){
			$this->data['enable_encash'] = $this->request->post['enable_encash'];
		} else { 
			$this->data['enable_encash'] = 0;
		}

		$this->data['total_bal_pl'] = 0;
		if($this->data['emp_code']){
			$total_bal = $this->model_transaction_transaction->gettotal_bal_ess($this->data['emp_code']);
			
			$total_bal_pro = $this->model_transaction_transaction->gettotal_bal_pro_ess($this->data['emp_code'], 'PL');
			$total_bal_pro1 = $this->model_transaction_transaction->gettotal_bal_pro1_ess($this->data['emp_code'], 'PL');
			$this->data['total_bal_pl'] = 0;
			if(isset($total_bal['pl_acc'])){
				if(isset($total_bal_pro['bal_p'])){
					$this->data['total_bal_pl'] = $total_bal['pl_acc'] - ($total_bal_pro['bal_p'] + $total_bal_pro['encash'] + $total_bal_pro1['leave_amount']);
				} else {
					$this->data['total_bal_pl'] = $total_bal['pl_acc'];
				}
			}

			//$total_bal_cl = $this->model_transaction_transaction->gettotal_bal($this->data['emp_code']);
			$total_bal_pro = $this->model_transaction_transaction->gettotal_bal_pro_ess($this->data['emp_code'], 'CL');
			$total_bal_pro1 = $this->model_transaction_transaction->gettotal_bal_pro1_ess($this->data['emp_code'], 'CL');
			$this->data['total_bal_cl'] = 0;
			if(isset($total_bal['cl_acc'])){
				if(isset($total_bal_pro['bal_p'])){
					$this->data['total_bal_cl'] = $total_bal['cl_acc'] - ($total_bal_pro['bal_p'] + $total_bal_pro1['leave_amount']);
				} else {
					$this->data['total_bal_cl'] = $total_bal['cl_acc'];
				}
			}

			//$total_bal_sl = $this->model_transaction_transaction->gettotal_bal($this->data['emp_code']);
			$total_bal_pro = $this->model_transaction_transaction->gettotal_bal_pro_ess($this->data['emp_code'], 'SL');
			$total_bal_pro1 = $this->model_transaction_transaction->gettotal_bal_pro1_ess($this->data['emp_code'], 'SL');
			$this->data['total_bal_sl'] = 0;
			if(isset($total_bal['sl_acc'])){
				if(isset($total_bal_pro['bal_p'])){
					$this->data['total_bal_sl'] = $total_bal['sl_acc'] - ($total_bal_pro['bal_p'] + $total_bal_pro1['leave_amount']);
				} else {
					$this->data['total_bal_sl'] = $total_bal['sl_acc'];
				}
			}

			$total_bal_pro = $this->model_transaction_transaction->gettotal_bal_pro_ess($this->data['emp_code'], 'LWP');
			$total_bal_pro1 = $this->model_transaction_transaction->gettotal_bal_pro1_ess($this->data['emp_code'], 'LWP');
			$this->data['total_bal_lwp'] = 0;
			if(isset($total_bal['lwp_acc'])){
				if(isset($total_bal_pro['bal_p'])){
					$this->data['total_bal_lwp'] = $total_bal['lwp_acc'] - ($total_bal_pro['bal_p'] + $total_bal_pro1['leave_amount']);
				} else {
					$this->data['total_bal_lwp'] = $total_bal['lwp_acc'];
				}
			}

			$total_bal_pro = $this->model_transaction_transaction->gettotal_bal_pro_ess($this->data['emp_code'], 'ML');
			$total_bal_pro1 = $this->model_transaction_transaction->gettotal_bal_pro1_ess($this->data['emp_code'], 'ML');
			$this->data['total_bal_ml'] = 0;
			if(isset($total_bal['ml_acc'])){
				if(isset($total_bal_pro['bal_p'])){
					$this->data['total_bal_ml'] = $total_bal['ml_acc'] - ($total_bal_pro['bal_p'] + $total_bal_pro1['leave_amount']);
				} else {
					$this->data['total_bal_ml'] = $total_bal['ml_acc'];
				}
			}

			$total_bal_pro = $this->model_transaction_transaction->gettotal_bal_pro_ess($this->data['emp_code'], 'PAL');
			$total_bal_pro1 = $this->model_transaction_transaction->gettotal_bal_pro1_ess($this->data['emp_code'], 'PAL');
			$this->data['total_bal_pal'] = 0;
			if(isset($total_bal['pal_acc'])){
				if(isset($total_bal_pro['bal_p'])){
					$this->data['total_bal_pal'] = $total_bal['pal_acc'] - ($total_bal_pro['bal_p'] + $total_bal_pro1['leave_amount']);
				} else {
					$this->data['total_bal_pal'] = $total_bal['pal_acc'];
				}
			}

			$total_bal_p = $this->model_transaction_transaction->gettotal_bal_p_ess($this->data['emp_code'], 'PL');
			$total_bal_p1 = $this->model_transaction_transaction->gettotal_bal_p1_ess($this->data['emp_code'], 'PL');
			$this->data['total_bal_pl_p'] = 0;
			if(isset($total_bal_p['bal_p'])){
				if(isset($total_bal['pl_acc'])){
					//$this->data['total_bal_pl_p'] = $total_bal['pl_acc'] - $total_bal_p['bal_p'];
					$this->data['total_bal_pl_p'] = $total_bal_p['bal_p'] + $total_bal_p['encash'] + $total_bal_p1['leave_amount'];
				}
			}

			$total_bal_p = $this->model_transaction_transaction->gettotal_bal_p_ess($this->data['emp_code'], 'CL');
			$total_bal_p1 = $this->model_transaction_transaction->gettotal_bal_p1_ess($this->data['emp_code'], 'CL');
			$this->data['total_bal_cl_p'] = 0;
			if(isset($total_bal_p['bal_p'])){
				if(isset($total_bal['cl_acc'])){
					//$this->data['total_bal_cl_p'] = $total_bal['cl_acc'] - $total_bal_p['bal_p'];
					$this->data['total_bal_cl_p'] = $total_bal_p['bal_p'] + $total_bal_p1['leave_amount'];
				}
			}

			$total_bal_p = $this->model_transaction_transaction->gettotal_bal_p_ess($this->data['emp_code'], 'SL');
			$total_bal_p1 = $this->model_transaction_transaction->gettotal_bal_p1_ess($this->data['emp_code'], 'SL');
			$this->data['total_bal_sl_p'] = 0;
			if(isset($total_bal_p['bal_p'])){
				if(isset($total_bal['sl_acc'])){
					//$this->data['total_bal_sl_p'] = $total_bal['sl_acc'] - $total_bal_p['bal_p'];
					$this->data['total_bal_sl_p'] = $total_bal_p['bal_p'] + $total_bal_p1['leave_amount'];
				}
			}

			$total_bal_p = $this->model_transaction_transaction->gettotal_bal_p_ess($this->data['emp_code'], 'LWP');
			$total_bal_p1 = $this->model_transaction_transaction->gettotal_bal_p1_ess($this->data['emp_code'], 'LWP');
			$this->data['total_bal_lwp_p'] = 0;
			if(isset($total_bal_p['bal_p'])){
				if(isset($total_bal['lwp_acc'])){
					//$this->data['total_bal_sl_p'] = $total_bal['sl_acc'] - $total_bal_p['bal_p'];
					$this->data['total_bal_lwp_p'] = $total_bal_p['bal_p'] + $total_bal_p1['leave_amount'];
				}
			}

			$total_bal_p = $this->model_transaction_transaction->gettotal_bal_p_ess($this->data['emp_code'], 'ML');
			$total_bal_p1 = $this->model_transaction_transaction->gettotal_bal_p1_ess($this->data['emp_code'], 'ML');
			$this->data['total_bal_ml_p'] = 0;
			if(isset($total_bal_p['bal_p'])){
				if(isset($total_bal['ml_acc'])){
					//$this->data['total_bal_sl_p'] = $total_bal['sl_acc'] - $total_bal_p['bal_p'];
					$this->data['total_bal_ml_p'] = $total_bal_p['bal_p'] + $total_bal_p1['leave_amount'];
				}
			}

			$total_bal_p = $this->model_transaction_transaction->gettotal_bal_p_ess($this->data['emp_code'], 'PAL');
			$total_bal_p1 = $this->model_transaction_transaction->gettotal_bal_p1_ess($this->data['emp_code'], 'PAL');
			$this->data['total_bal_pal_p'] = 0;
			if(isset($total_bal_p['bal_p'])){
				if(isset($total_bal['pal_acc'])){
					//$this->data['total_bal_sl_p'] = $total_bal['sl_acc'] - $total_bal_p['bal_p'];
					$this->data['total_bal_pal_p'] = $total_bal_p['bal_p'] + $total_bal_p1['leave_amount'];
				}
			}
			// echo '<pre>';
			// print_r($this->data['total_bal_sl']);
			// echo '<pre>';
			// print_r($this->data['total_bal_sl_p']);
			// exit;
		}

		if($this->request->server['REQUEST_METHOD'] == 'POST') {
			$this->data['post_status'] = 1;
		} else {
			$this->data['post_status'] = 0;
		}

		$types = array(
			'F' => 'Full Day',
			'1' => 'First Half',
			'2' => 'Second Half'
		);
		$this->data['types'] = $types;
		
		// echo '<pre>';
		// print_r($this->data);
		// exit;

		$this->template = 'transaction/leave_ess.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	} 

	public function GetDays($sStartDate, $sEndDate){  
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = date("Y-m-d", strtotime($sStartDate));  
		$sEndDate = date("Y-m-d", strtotime($sEndDate));  
		// Start the variable off with the start date  
		$aDays[] = $sStartDate;  
		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;  
		// While the current date is less than the end date  
		while($sCurrentDate < $sEndDate){  
		// Add a day to the current date  
		$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// Add this new day to the aDays array  
		$aDays[] = $sCurrentDate;  
		}
		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;  
	} 

	protected function validateForm() {
		$this->load->model('transaction/transaction');

		// if (!$this->user->hasPermission('modify', 'transaction/horse_wise')) {
		// 	$this->error['warning'] = $this->language->get('error_permission');
		// }
		$from = $this->request->post['from'];
		$to = $this->request->post['to'];
		$close_sql = "SELECT * FROM `oc_transaction` WHERE `date` = '".$from."' ";
		$close_res = $this->db->query($close_sql);
		if($close_res->num_rows > 0){
			$close_sql = "SELECT * FROM `oc_transaction` WHERE `date` = '".$from."' AND `month_close_status` = '0' ";
			$close_res = $this->db->query($close_sql);
			if($close_res->num_rows == 0){
				$this->error['warning'] = 'Month Already Closed';
			}
		}
		//echo '<pre>';
		//print_r($this->error);
		//exit;
		//if($this->request->post['diffDays'] > 1){
			if($this->request->post['leave_type'] == 'PL'){
				//$pl_bal_sql = "SELECT `pl_bal` FROM `oc_leave` WHERE emp_id = '".$this->request->post['e_name_id']."' ";
				//$pl_bal_res = $this->db->query($pl_bal_sql);
				
				$total_bal = $this->model_transaction_transaction->gettotal_bal_ess($this->request->post['e_name_id']);
				$total_bal_pro = $this->model_transaction_transaction->gettotal_bal_pro_ess($this->request->post['e_name_id'], 'PL');
				$total_bal_pro1 = $this->model_transaction_transaction->gettotal_bal_pro1_ess($this->request->post['e_name_id'], 'PL');
				$total_bal_pl = 0;
				if(isset($total_bal['pl_acc'])){
					if(isset($total_bal_pro['bal_p'])){
						$total_bal_pl = $total_bal['pl_acc'] - ($total_bal_pro['bal_p'] + $total_bal_pro['encash'] + $total_bal_pro1['leave_amount']);
					} else {
						$total_bal_pl = $total_bal['pl_acc'];
					}
				}
				

				$pl_bal_tran_sql = "SELECT count(*) as cnt FROM `oc_leave_transaction_temp` WHERE emp_id = '".$this->request->post['e_name_id']."' AND `leave_type` = 'PL' AND `p_status` = '0' AND `a_status` = '1' ";
				$pl_bal_tran_res = $this->db->query($pl_bal_tran_sql);
				$pl_bal_f = $total_bal_pl - $pl_bal_tran_res->row['cnt'];
				if($total_bal_pl > 0){
					if($this->request->post['enable_encash'] == 1){ 
						if($pl_bal_f >= 30){
							$pl_balance = $pl_bal_f;
							if($this->request->post['leave_amount'] != '0'){
								$total_added = $this->request->post['leave_amount'];
							} else {
								$total_added = $this->request->post['days'] + $this->request->post['encash'];	
							}
							//$total_pl_added = $this->request->post['days'] + $this->request->post['encash'];
							if($total_added > $pl_balance){
								$this->error['days'] = 'Only ' . $pl_balance . ' days PL leave available For Employee';	
							} else {
								if($this->request->post['enable_encash'] == 1){
									$days = $this->request->post['days'];
									$act_encash = $this->request->post['encash'];
									$exp_encash = $days * 3.29;
									$exp_encash = round($exp_encash);
									if($days < 7 && $this->request->post['group'] != 'OFFICIALS'){
										//if($days < 7){
										//$this->error['encash'] = 'Minimum PL Leave Days Should be 7 days'; 
									} else {
										if($act_encash > $exp_encash){
											$this->error['encash'] = 'Encash Should be less than or equal to ' . $exp_encash . ' days'; 
										} elseif($act_encash < 15){
											$this->error['encash'] = 'Minimum Encash Should be 15 days'; 
										}
									}
								}
							}
						} else {
							$this->error['leave_type'] = 'PL Balance Leave For Employee Less than 30';	
						}
					} else {
						$pl_balance = $pl_bal_f;
						if($this->request->post['leave_amount'] != '0'){
							$total_added = $this->request->post['leave_amount'];
						} else {
							$total_added = $this->request->post['days'];	
						}
						if($total_added > $pl_balance){
							$this->error['days'] = 'Only ' . $pl_balance . ' days PL leave available For Employee';	
						} else {
							$days = $this->request->post['days'];
							if($days < 7 && $this->request->post['group'] != 'OFFICIALS'){
								//$this->error['days'] = 'Minimum PL Leave Days Should be 7 days'; 
							}
						}
					}
				} else {
					//if($this->request->post['enable_encash'] == 1){
						$this->error['leave_type'] = 'PL Balance Leave For Employee Does Not Exist';
					//}
				}
			} elseif($this->request->post['leave_type'] == 'CL'){
				//$bal_sql = "SELECT `cl_bal` FROM `oc_leave` WHERE emp_id = '".$this->request->post['e_name_id']."' ";
				//$bal_res = $this->db->query($bal_sql);
				$from = $this->request->post['from'];
				$to = $this->request->post['to'];

				$date1 = new DateTime($from);
				$date2 = new DateTime($to);

				$diff = $date2->diff($date1)->format("%a");
				$diff_days = $diff + 1;
				if($diff_days > 4){
					$this->error['leave_type'] = 'Max CL Leave Period is 4 days';
				}


				$total_bal = $this->model_transaction_transaction->gettotal_bal_ess($this->request->post['e_name_id']);
				$total_bal_pro = $this->model_transaction_transaction->gettotal_bal_pro_ess($this->request->post['e_name_id'], 'CL');
				$total_bal_pro1 = $this->model_transaction_transaction->gettotal_bal_pro1_ess($this->request->post['e_name_id'], 'CL');
				$total_bal_cl = 0;
				if(isset($total_bal['cl_acc'])){
					if(isset($total_bal_pro['bal_p'])){
						$total_bal_cl = $total_bal['cl_acc'] - ($total_bal_pro['bal_p'] + $total_bal_pro1['leave_amount']);
					} else {
						$total_bal_cl = $total_bal['cl_acc'];
					}
				}

				$bal_tran_sql = "SELECT count(*) as cnt FROM `oc_leave_transaction_temp` WHERE emp_id = '".$this->request->post['e_name_id']."' AND `leave_type` = '".$this->request->post['leave_type']."' AND `p_status` = '0' AND `a_status` = '1' ";
				$bal_tran_res = $this->db->query($bal_tran_sql);
				$bal_f = $total_bal_cl - $bal_tran_res->row['cnt'];
				
				$balance = $bal_f;
				if($this->request->post['leave_amount'] != '0'){
					$total_added = $this->request->post['leave_amount'];
				} else {
					$total_added = $this->request->post['days'];	
				}
				if($total_added > $balance){
					if($balance < 0){
						$balance = 0;
					}
					$this->error['days'] = 'Only ' . $balance . ' days '. $this->request->post['leave_type'] .' leave available For Employee';	
				} 
			} elseif($this->request->post['leave_type'] == 'SL'){
				//$bal_sql = "SELECT `sl_bal` FROM `oc_leave` WHERE emp_id = '".$this->request->post['e_name_id']."' ";
				//$bal_res = $this->db->query($bal_sql);

				$total_bal = $this->model_transaction_transaction->gettotal_bal_ess($this->request->post['e_name_id']);
				$total_bal_pro = $this->model_transaction_transaction->gettotal_bal_pro_ess($this->request->post['e_name_id'], 'SL');
				$total_bal_pro1 = $this->model_transaction_transaction->gettotal_bal_pro1_ess($this->request->post['e_name_id'], 'SL');
				$total_bal_sl = 0;
				if(isset($total_bal['sl_acc'])){
					if(isset($total_bal_pro['bal_p'])){
						$total_bal_sl = $total_bal['sl_acc'] - ($total_bal_pro['bal_p'] + $total_bal_pro1['leave_amount']);
					} else {
						$total_bal_sl = $total_bal['sl_acc'];
					}
				}

				$bal_tran_sql = "SELECT count(*) as cnt FROM `oc_leave_transaction_temp` WHERE emp_id = '".$this->request->post['e_name_id']."' AND `leave_type` = '".$this->request->post['leave_type']."' AND `p_status` = '0' AND `a_status` = '1' ";
				$bal_tran_res = $this->db->query($bal_tran_sql);
				$bal_f = $total_bal_sl - $bal_tran_res->row['cnt'];
				$balance = $bal_f;
				if($this->request->post['leave_amount'] != '0'){
					$total_added = $this->request->post['leave_amount'];
				} else {
					$total_added = $this->request->post['days'];	
				}
				if($total_added > $balance){
					if($balance < 0){
						$balance = 0;
					}
					$this->error['days'] = 'Only ' . $balance . ' days '. $this->request->post['leave_type'] .' leave available For Employee';	
				} 
			} 

			// elseif($this->request->post['leave_type'] == 'COF'){
			// 	//echo 'aaaa';
			// 	//$bal_sql = "SELECT `cof_bal` FROM `oc_leave` WHERE emp_id = '".$this->request->post['e_name_id']."' ";
			// 	//$bal_res = $this->db->query($bal_sql);

			// 	$total_bal = $this->model_transaction_transaction->gettotal_bal($this->request->post['e_name_id']);
			// 	$total_bal_pro = $this->model_transaction_transaction->gettotal_bal_pro($this->request->post['e_name_id'], 'COF');
			// 	$total_bal_cof = 0;
			// 	if(isset($total_bal['cof_acc'])){
			// 		if(isset($total_bal_pro['bal_p'])){
			// 			$total_bal_cof = $total_bal['cof_acc'] - $total_bal_pro['bal_p'];
			// 		} else {
			// 			$total_bal_cof = $total_bal['cof_acc'];
			// 		}
			// 	}
				
			// 	$bal_tran_sql = "SELECT count(*) as cnt FROM `oc_leave_transaction` WHERE emp_id = '".$this->request->post['e_name_id']."' AND `leave_type` = '".$this->request->post['leave_type']."' AND `p_status` = '0' AND `a_status` = '1' ";
			// 	$bal_tran_res = $this->db->query($bal_tran_sql);
			// 	$bal_f = $total_bal_cof - $bal_tran_res->row['cnt'];
				
			// 	$balance = $bal_f;
			// 	$total_added = $this->request->post['days'];
			// 	if($total_added > $balance){
			// 		if($balance < 0){
			// 			$balance = 0;
			// 		}
			// 		$this->error['days'] = 'Only ' . $balance . ' days '. $this->request->post['leave_type'] .' leave available For Employee';	
			// 	} 
			// }

			// echo '<pre>';
			// print_r($this->error);
			// exit;

			$from = $this->request->post['from'];
			$to = $this->request->post['to'];

			$date_sql = "SELECT `date`, `leave_amount`, `type` FROM `oc_leave_transaction_temp` WHERE `date` BETWEEN '".$from."' AND '".$to."' AND `a_status` = '1' AND `emp_id`= '".$this->request->post['e_name_id']."' ";
			$date_res = $this->db->query($date_sql);
			if($date_res->num_rows > 0){
				if($date_res->row['type'] != ''){
					if($date_res->row['leave_amount'] == 0.5 && $this->request->post['leave_amount'] == 0.5){
						if($date_res->row['type'] == $this->request->post['type']){
							$this->error['warning'] = 'Leave For Dates Exist';
						}
					} else {
						$this->error['warning'] = 'Leave For Dates Exist';
					}
				} else {
					$this->error['warning'] = 'Leave For Dates Exist';	
				}
			}
		//}

		$day = array();
		$days = $this->GetDays($from, $to);
        foreach ($days as $dkey => $dvalue) {
        	$dates = explode('-', $dvalue);
        	$day[$dkey]['day'] = $dates[2];
        	$day[$dkey]['date'] = $dvalue;
        }

        foreach($day as $dkey => $dvalue){
        	$is_holidays = $this->db->query("SELECT `holiday_id` FROM `oc_holiday` WHERE `date` = '".$dvalue['date']."' ");
        	if($is_holidays->num_rows > 0){
        		if($is_holidays->num_rows > 0){
	        		$day_date = date('j', strtotime($dvalue['date']));
	        		$day_month = date('n', strtotime($dvalue['date']));
	        		$day_year = date('Y', strtotime($dvalue['date']));
	        		$sql = "SELECT `".$day_date."` FROM `oc_shift_schedule` WHERE `emp_code` = '".$this->request->post['e_name_id']."' AND `month` = '".$day_month."' AND `year` = '".$day_year."' ";
	        		$shift_datas = $this->db->query($sql)->row[$day_date];
	        		$shift_explode = explode('_', $shift_datas);
	        		// echo '<pre>';
	        		// print_r($shift_explode);
	        		// exit;
	        		if($shift_explode[0] == 'H'){
		        		$this->error['warning'] = 'Holiday Exist in Selected Date Period';
		        		break;
	        		}
	        	}
        		//$this->error['warning'] = 'Holiday Exist in Selected Date Period';
        		//break;
        	}
        	if(date('N', strtotime($dvalue['date'])) >= 6){
        		$this->error['warning'] = 'Weekend Exist in Selected Date Period';
        		break;	
        	}
        }

        // echo '<pre>';
        // print_r($this->error['warning']);
        // exit;

		$from_date = $this->request->post['from'];
		$to_date = $this->request->post['to'];
		$from_year = date('Y', strtotime($from_date));
		$to_year = date('Y', strtotime($to_date));
		if($from_year != $to_year){
			$this->error['warning'] = 'From And To Year should be same';
		}
		// echo '<pre>';
		// print_r($this->error);
		// exit;

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	protected function validateDelete(){
		// $this->load->model('transaction/transaction');
		// if(isset($this->request->post['selected'])){
		// 	foreach ($this->request->post['selected'] as $batch_id) {
		// 		$leave_sql = "SELECT `id` FROM `oc_leave_transaction` WHERE `batch_id` = '".$batch_id."' AND `p_status` = '1' ";
		// 		$date_res = $this->db->query($leave_sql);
		// 		if ($date_res->num_rows > 0) {
		// 			$this->error['warning'] = 'Leave With batch ' . $batch_id . ' already processed';
		// 		}
		// 	}	
		// } elseif(isset($this->request->get['batch_id'])){
		// 	$batch_id = $this->request->get['batch_id'];
		// 	$leave_sql = "SELECT `id` FROM `oc_leave_transaction` WHERE `batch_id` = '".$batch_id."' AND `p_status` = '1' ";
		// 	$date_res = $this->db->query($leave_sql);
		// 	if ($date_res->num_rows > 0) {
		// 		$this->error['warning'] = 'Leave With batch ' . $batch_id . ' already processed';
		// 	}
		// }

		if (!$this->error) {
			return true;
		} else {
			return false;
		}		
	}

	public function getshift_details() {
		$jsondata = array();
		$jsondata['status'] = 0;
		//$this->log->write(print_r($this->request->get,true));
		$shift_data = array();
		if(isset($this->request->get['emp_code'])){
			$jsondata['status'] = 1;
			$emp_code = $this->request->get['emp_code'];
			$from = $this->request->get['from'];
			$to = $this->request->get['to'];
			if(!isset($this->request->get['cof_stat'])){
				$from = date('Y-m-d', strtotime($from . ' -1 day'));
				$to = date('Y-m-d', strtotime($to . ' +1 day'));
			}
			$data = array(
				'filter_date_start' => $from,
				'filter_date_end' => $to
			);
	        $this->load->model('transaction/transaction');
			$trans_datas = $this->model_transaction_transaction->gettransaction_data_leave($emp_code, $data);
			$html = '';
			$html .= "<table border='1' style='margin:0px auto;'>";
				$html .= "<tr>";
					$tot = count($trans_datas) + 1;
					$html .= "<td style='padding:10px;text-align:center;font-weight:bold;' colspan = '".$tot."'>";
						$html .= "Attendance Status";
					$html .= "</td>";
				$html .= "</tr>";
				$html .= "<tr>";
					$html .= "<td style='padding:2px;'>";
						$html .= " ";
					$html .= "</td>";
					foreach($trans_datas as $tkey => $tvalue){
						$html .= "<td style='padding:2px;font-weight:bold;font-size:9px;'>";
							$html .= $tvalue['day'];
						$html .= "</td>";
					}
				$html .= "</tr>";
				$html .= "<tr>";
					$html .= "<td style='font-weight:bold;padding:2px;'>";
						$html .= "Act In Time";
					$html .= "</td>";
					foreach($trans_datas as $tkey => $tvalue){
						$html .= "<td style='padding:2px;font-size:9px;'>";
							if($tvalue['act_intime'] != '00:00:00' && $tvalue['act_intime'] != ''){
								$html .= $tvalue['act_intime'];
							} else {
								$html .= '00:00';
							}
						$html .= "</td>";
					}
				$html .= "</tr>";
				$html .= "<tr>";
					$html .= "<td style='font-weight:bold;padding:2px;'>";
						$html .= "Act Out Time";
					$html .= "</td>";
					foreach($trans_datas as $tkey => $tvalue){
						$html .= "<td style='padding:2px;font-size:9px;'>";
							if($tvalue['act_outtime'] != '00:00:00' && $tvalue['act_outtime'] != ''){
								$html .= $tvalue['act_outtime'];
							} else {
								$html .= '00:00';
							}
						$html .= "</td>";
					}
				$html .= "</tr>";
				$html .= "<tr>";
					$html .= "<td style='font-weight:bold;padding:2px;'>";
						$html .= "FH";
					$html .= "</td>";
					foreach($trans_datas as $tkey => $tvalue){
						$html .= "<td style='padding:2px;font-size:9px;'>";
							if($tvalue['firsthalf_status'] == '1'){
								$html .= 'P';	
							} elseif($tvalue['firsthalf_status'] == '0'){
								$html .= 'A';
							} else {
								$html .= $tvalue['firsthalf_status'];	
							}
						$html .= "</td>";
					}
				$html .= "</tr>";
				$html .= "<tr>";
					$html .= "<td style='font-weight:bold;padding:2px;'>";
						$html .= "SH";
					$html .= "</td>";
					foreach($trans_datas as $tkey => $tvalue){
						$html .= "<td style='padding:2px;font-size:9px;'>";
							if($tvalue['secondhalf_status'] == '1'){
								$html .= 'P';	
							} elseif($tvalue['secondhalf_status'] == '0'){
								$html .= 'A';
							} else {
								$html .= $tvalue['secondhalf_status'];	
							}
						$html .= "</td>";
					}
				$html .= "</tr>";
			$html .= "</table>";
			if(!isset($this->request->get['cof_stat'])){
				$close_sql = "SELECT * FROM `oc_transaction` WHERE `date` = '".$this->request->get['from']."' ";
				//$this->log->write(print_r($close_sql,true));				
				$close_res = $this->db->query($close_sql);
				//$this->log->write(print_r($close_res,true));
				if($close_res->num_rows > 0){
					$close_sql = "SELECT * FROM `oc_transaction` WHERE `date` = '".$this->request->get['from']."' AND `month_close_status` = '0' ";					
					//$this->log->write(print_r($close_sql,true));
					$close_res = $this->db->query($close_sql);
					//$this->log->write(print_r($close_res->num_rows,true));					
					if($close_res->num_rows == 0){
						$jsondata['status'] = 2;
						$jsondata['message'] = 'Month Already Closed';
					}
				}
			}
		}
		$jsondata['html'] = $html;
		//$this->log->write($html);
	    $this->response->setOutput(Json_encode($jsondata));
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/employee');

			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_employee->getemployees($data);

			foreach ($results as $result) {
				$json[] = array(
					'employee_id' => $result['employee_id'],
					'emp_code' => $result['emp_code'], 
					'name'            => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}
}
?>