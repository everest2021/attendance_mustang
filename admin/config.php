<?php
session_start();
date_default_timezone_set("Asia/Kolkata");
// HTTP
define('HTTP_SERVER', 'http://192.168.0.228:8012/attendance_mustang/admin/');
define('HTTP_CATALOG', 'http://192.168.0.228:8012/attendance_mustang/');

// HTTPS
define('HTTPS_SERVER', 'http://192.168.0.228:8012/attendance_mustang/admin/');
define('HTTPS_CATALOG', 'http://192.168.0.228:8012/attendance_mustang/');

// DIR
define('DIR_APPLICATION', 'C:/xampp/htdocs/attendance_mustang/admin/');
define('DIR_SYSTEM', 'C:/xampp/htdocs/attendance_mustang/system/');
define('DIR_DATABASE', 'C:/xampp/htdocs/attendance_mustang/system/database/');
define('DIR_LANGUAGE', 'C:/xampp/htdocs/attendance_mustang/admin/language/');
define('DIR_TEMPLATE', 'C:/xampp/htdocs/attendance_mustang/admin/view/template/');
define('DIR_CONFIG', 'C:/xampp/htdocs/attendance_mustang/system/config/');
define('DIR_IMAGE', 'C:/xampp/htdocs/attendance_mustang/image/');
define('DIR_CACHE', 'C:/xampp/htdocs/attendance_mustang/system/cache/');
define('DIR_DOWNLOAD', 'C:/xampp/htdocs/attendance_mustang/download/');
define('DIR_LOGS', 'C:/xampp/htdocs/attendance_mustang/system/logs/');
define('DIR_CATALOG', 'C:/xampp/htdocs/attendance_mustang/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'db_attendance_mustang');
define('DB_PREFIX', 'oc_');
//company Name
define('COMPANY_NAME', 'Attendance Mustang')
?>