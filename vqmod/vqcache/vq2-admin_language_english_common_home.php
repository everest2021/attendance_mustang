<?php
// Heading
$_['heading_title']                 = 'Dashboard';

// Text
$_['text_overview']                 = 'Overview';
$_['text_statistics']               = 'Statistics';
$_['text_latest_10_orders']         = 'Last 10 Horses Treated';
$_['text_total_sale']               = 'Total Sales:';
$_['text_total_sale_year']          = 'Total Sales This Year:';
$_['text_total_order']              = 'Total Treatment:';
$_['text_total_customer']           = 'No. of Horses:';
$_['text_total_customer_approval']  = 'Customers Awaiting Approval:';
$_['text_total_review_approval']    = 'Reviews Awaiting Approval:';
$_['text_total_affiliate']          = 'No. of Affiliates:';
$_['text_total_affiliate_approval'] = 'Affiliates Awaiting Approval:';
$_['text_day']                      = 'Today';
$_['text_week']                     = 'This Week';
$_['text_month']                    = 'This Month';
$_['text_year']                     = 'This Year';
$_['text_order']                    = 'Total Orders';
$_['text_customer']                 = 'Total Customers';

			$_['text_total_sales_previous_years']                 = 'Previous Sales:';
			$_['text_other_stats']                                = 'Other Stats';
			$_['text_total_review']                               = 'No. of Reviews:';
			


$_['column_sr_no']  = 'Sr';
$_['column_horse_name']  = 'Horse Name';
$_['column_trainer']  = 'Trainer';
$_['column_date']  = 'Date';
$_['column_medicine']  = 'Medicine';
$_['column_total']  = 'Total';
$_['text_view']  = 'view';
$_['text_detail']  = 'detail';

$_['text_total_horses'] = 'Total Horses';
$_['text_total_trainer'] = 'Total Trainer';
$_['text_total_owner'] = 'Total Owner';
$_['text_total_medicine'] = 'Total Medicine';

// Column 
$_['column_order']                  = 'Order ID';
$_['column_customer']               = 'Customer';
$_['column_status']                 = 'Status';
$_['column_date_added']             = 'Date Added';
$_['column_total']                  = 'Total';
$_['column_firstname']              = 'First Name';
$_['column_lastname']               = 'Last Name';
$_['column_action']                 = 'Action';

$_['button_search'] = 'Search';

// Entry
$_['entry_range']                   = 'Select Range:';

// Error
$_['error_install']                 = 'Warning: Install folder still exists and should be deleted for security reasons!';
$_['error_image']                   = 'Warning: Image directory %s not writeable!';
$_['error_image_cache']             = 'Warning: Image cache directory %s not writeable!';
$_['error_cache']                   = 'Warning: Cache directory %s not writeable!';
$_['error_download']                = 'Warning: Download directory %s not writeable!';
$_['error_logs']                    = 'Warning: Log directory %s not writeable!';
?>